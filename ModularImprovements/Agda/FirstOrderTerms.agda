module FirstOrderTerms where

open import Library
open import Categories
import Categories.Products
open import Functors
open import Categories.Sets
open import Monads
open import Relation.Binary.HeterogeneousEquality
open Cat
open Fun

-------------------------
-- Signatures
-------------------------

record Signature {a : Level} : Set (lsuc a) where
  constructor sig
  field  symbol : Set a
         parameter : symbol -> Set a
         arity     : symbol -> Set a

open Signature

--------------------------------------------------
module SigHelper where

    -- An example language
    data L : Set where
         op1 : L
         op2 : L

    -- Writing a signature directly can be quite cumbersome
    LSig : Signature
    LSig = record { symbol = L
                  ; parameter = λ { op1 → ℕ ; op2 → ⊤ }
                  ; arity = λ { op1 → ⊤ ; op2 → ℕ }}

    --That's why we use a signature constructor helper.
    data opType (S : Set) : S → Set₁ where
       _⇒_ : {s : S} → Set → Set → opType S s

    parType : {S : Set} → {s : S} → opType S s → Set
    parType (p ⇒ _) = p

    ariType : {S : Set} → {s : S} → opType S s → Set
    ariType (_ ⇒ a) = a

    mkSignature : (S : Set) -> ((s : S) → opType S s) -> Signature
    mkSignature S f = sig S (λ s → parType (f s)) (λ s → ariType (f s))

    -- Now, describing a signature is easier.

    LSig2 = mkSignature L Lmap
             where
              Lmap : (s : L) → opType L s
              Lmap op1 = ℕ ⇒ ⊤
              Lmap op2 = ⊤ ⇒ ℕ
--------------------------------------------------

-- Signature Semantics
-- Every signature determines a functor

-- The object mapping
OMapSig : Signature
         → Set
         → Set
OMapSig S A = Σ (symbol S) (λ s → parameter S s × (arity S s → A))

OMapSigL : {l : Level} -> Signature {l}
         -> Set l -> Set l
OMapSigL {l} S A = Σ (symbol S) λ s → parameter S s × (arity S s -> A)

---------
-- The Hom mapping
HMapSig : (S : Signature)
            → {X Y : Set}
            → (X → Y)
            → OMapSig S X → OMapSig S Y
HMapSig S f (n , p , h) = n , p , (f ∘ h)

-- The Functor definition
⟦_⟧sig : Signature → Fun Sets Sets
⟦ S ⟧sig = record{
            OMap  = OMapSig S;
            HMap  = HMapSig S;
            fid   = refl;
            fcomp = refl
          }

--------------------------------------------------
-- Open Terms
--------------------------------------------------

-- We define open terms as the free (algebraic) monad over a signature

data Term (S : Signature)(X : Set) : Set where
  var : X → Term S X
  op  : OMapSig S (Term S X) → Term S X

data TermL {l : Level}(S : Signature {l})(X : Set l) : Set l where
  varl : X -> TermL S X
  opl : OMapSigL S (TermL S X) -> TermL S X

foldTerm : {S : Signature}
      → ∀{X Y}
      → (X → Y)            -- what to do with variables
      → (OMapSig S Y → Y)  -- what to do with operations
      → Term S X → Y
foldTerm     v k (var x)      = v x
foldTerm {S} v k (op (s , p , h)) = k (s , p , (λ x → foldTerm v k (h x)))

-- Bounded Fold
boundedFold : {S : Signature}
            → ∀{X Y}
            → ℕ                  -- Gas
            → Y -- default value
            → (X → Y)            -- what to do with variables
            → (OMapSig S Y → Y)  -- what to do with operations
            → Term S X → Y
boundedFold zero def v k t = def
boundedFold (suc n) def v k (var x) = v x
boundedFold (suc n) def v k (op (s , p , h)) = k (s , p , (λ x → boundedFold n def v k (h x)))

----------------------------------------
-- Algebra Morphism to use Gas?
-- I need some sort of monadic fold or more structure... So I am going to
-- assume that the algebra is built knowing how to use gas.

-- But what indices do I have to check?? I mean, the idea is pretty easy, check
-- if an argumento is bottom (or Right ⊤) and return (Right ⊤)... But it seems
-- that I need some monadic effects, or a way to decide which arguments I have
-- to check.
-- bounded : { S : Signature}{A : Set}
--         -> (OMapSig S A -> A)
--         -> (OMapSig S (A *) -> A *)
-- bounded alg (s , p , ts ) = {!!}

_* : ∀ {a} -> Set a -> Set a
A * = A ⊎ ⊤

just : ∀ {a} -> {A : Set a} -> A -> A *
just = inj₁

none : ∀ {a} -> {A : Set a} -> A *
none = inj₂ ⊤.tt

demay : ∀ {a} -> {A B : Set a} -> B -> (A -> B) -> A * -> B
demay v f (inj₁ x) = f x
demay v f (inj₂ y) = v

boundedGas : {S : Signature}
            → ∀{X Y}
            → ℕ                  -- Gas
            → (X → Y)            -- what to do with variables
            → (OMapSig S (Y *) → Y *)  -- what to do with operations
            → Term S X → Y *
boundedGas g v k t = boundedFold g (inj₂ ⊤.tt) (inj₁ ∘ v) k t

----------------------------------------

-- In Agda, this is equivalent to pattern matching on a term
-- amd doing recursive calls on subterms.
inductionTerm : ∀{S l X}
           (P : Term S X → Set l) →
           ((x : X) → P (var x)) →
           ((s : symbol S)(p : parameter S s)(ts : arity S s → Term S X) →
            ((a : arity S s) → P (ts a)) → P (op (s , p , ts))
           ) →
           (t : Term S X) → P t
inductionTerm P v p (var x) = v x
inductionTerm P v p (op (s , r , t)) = p s r t (λ a → inductionTerm P v p (t a))


congS : ∀{S : Signature {lzero}}{X Y : Set}
         {s : symbol S}{p : parameter S s}
         {h h' : arity S s → X}
         → (f : Σ (symbol S) (λ s → parameter S s × (arity S s → X)) → Y)
         → ((a : arity S s) → h a ≅ h' a)
         → f (s , p , h) ≅ f (s , p , h')
congS {s = s} {p} {h} {h'} f i = cong (λ y → f (s , p , y)) (ext (λ a → i a))

foldTerm-id :  (S : Signature)(X : Set)
         → (x : Term S X)
         → foldTerm {Y = Term S X} var op x ≅ x
foldTerm-id S X (var x) = refl
foldTerm-id S X (op (s , p , h)) = congS op (λ a → foldTerm-id S X (h a))

--------------------------------------------------
-- Term S is a monad for any signature S
--------------------------------------------------

bindTerm : ∀ {S}{X Y : Set} → (X → Term S Y) → Term S X → Term S Y
bindTerm k = foldTerm k op

Term-law1 : ∀ {S}{X : Set} → (a : Term S X) → bindTerm {S} {X} var a ≅ a
Term-law1 (var x) = refl
Term-law1 (op (s , p , h)) = congS op (λ a → Term-law1 (h a))

Term-law3 : ∀ {S}{X Y Z : Set}
      →  {f : X → Term S Y}
      →  {g : Y → Term S Z }
      →  (a  : Term S X)
      → bindTerm {S} {X} (bindTerm {S} {Y} g ∘ f) a ≅ bindTerm {S} {Y} g (bindTerm {S} {X} f a)
Term-law3 (var x) = refl
Term-law3 {S} {f = f} {g = g} (op (s , p , h)) = congS op (λ a → Term-law3 (h a))

TermMonad : Signature → Monad Sets
TermMonad S = record {
  T    = Term S; 
  η    = var;
  bind = bindTerm;
  law1 = ext (Term-law1); 
  law2 = ext (λ _ → refl);
  law3 = ext Term-law3}

-------------------------------------------------
-- fold fusion lemmas.

--
foldFusion : ∀{X Y Z S}
          → {a : Z → X}
          → (h : X → Y)
          → {f : OMapSig S X → X}
          → {g : OMapSig S Y → Y}
          → h ∘ f ≅ g ∘ HMapSig S h
          → (t : Term S Z)
          → h (foldTerm a f t) ≅ foldTerm (h ∘ a) g t
foldFusion h p (var x) = refl
foldFusion {S = S} {a} h {f} {g} p (op (s , r , x)) = 
   proof
   h (foldTerm a f (op (s , r , x)))
  ≅⟨ refl ⟩
    (h ∘ f) (s , r , λ x₁ → foldTerm a f (x x₁))
  ≅⟨ cong-app p (s , r , (λ x₁ → foldTerm a f (x x₁))) ⟩
    (g ∘ HMapSig S h) (s , r , λ x₁ → foldTerm a f (x x₁))
  ≅⟨ refl ⟩
    g (s , r , h ∘ (λ x₁ → foldTerm a f (x x₁)))
    ≅⟨ congS g (λ x₁ → foldFusion h p (x x₁)) ⟩
    g (s , r , λ x₁ → foldTerm (h ∘ a) g (x x₁))
  ≅⟨ refl ⟩
    foldTerm (h ∘ a) g (op (s , r , x))
  ∎

--banana split
bananaSplit : ∀{S X Y Z}
          → (a : Z → X × Y)
          → (f : OMapSig S X → X)
          → (g : OMapSig S Y → Y)
          → (t : Term S Z)
          → foldTerm a (λ fp → f (HMapSig S fst fp) , g (HMapSig S snd fp)) t
           ≅ ( foldTerm (fst ∘ a) f t , foldTerm (snd ∘ a) g t)
bananaSplit a f g (var v) = refl
bananaSplit {S = S} a f g (op (s , p , x)) = cong₂ _,_
                 (congS f λ a₁ → foldFusion fst (ext (λ { (s , p , r) → refl})) (x a₁))
                 (congS g λ a₁ → foldFusion snd (ext (λ { (s , p , r) → refl})) (x a₁))

foldBindLemma : ∀{S X Y Z}
               → (f : Z → Y)
               → (h : OMapSig S Y → Y)
               → (k : X → Term S Z)
               → (t : Term S X)
               → foldTerm f h (bindTerm k t) ≅ foldTerm (foldTerm f h ∘ k) h t
foldBindLemma f h k (var x) = refl
foldBindLemma f h k (op (s , p , ts)) = congS h (λ a → foldBindLemma f h k (ts a))

--------------------------------------------------
-- Programs and Contexts
--------------------------------------------------

-- A program is a closed term
Program : Signature → Set
Program S = Term S ⊥

-- Since program are Ctx without holes we can fold them without
-- paying attention to (non-existing) holes.
foldProgram : ∀{S Y} -> (OMapSig S Y -> Y) -> Program S -> Y
foldProgram alg = foldTerm ⊥-elim alg

inductionProgram : ∀{S l}
           (P : Program S → Set l) →
           ((s : symbol S)(p : parameter S s)(ts : arity S s → Program S) →
            ((a : arity S s) → P (ts a)) → P (op (s , p , ts))
           ) →
           (t : Program S) → P t
inductionProgram P p t = inductionTerm P (λ ()) p t

-- A context is term with holes marked with tt : ⊤
Ctx : Signature → Set
Ctx S = Term S ⊤

-- We may plug any term in a context
infix 30 _[_]

_[_] : ∀{S X} → Ctx S → Term S X → Term S X
C [ T ] = bindTerm (λ ⊤ → T) C
