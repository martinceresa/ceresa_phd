open import FirstOrderTerms
open import Semantics

module Tick.Algebra (S : Signature)(E V : Set)(h : Semantics S E V)  where

open import Tick
open import Library

open import Data.Nat
open import Data.Nat.Properties -- (ℕ, ≤) is a PoSet

open import Improvement.Definition (✓sig S) E V (✓sem h)

open Semantics.Semantics (✓sem h)
open FirstOrderTerms.Signature

TS : Signature
TS = ✓sig S

-- | Helper function, 1 + n ≤ n is false.
Pnn : (n : ℕ) -> suc n ≤ n -> ⊥
Pnn zero ()
Pnn (suc n) (s≤s fal) = Pnn n fal

open import Relation.Binary.PropositionalEquality hiding ([_])
     renaming (cong to pcong)

mPlusSucn : (m n : ℕ) -> suc (m + n) ≡ m + suc n
mPlusSucn zero n = refl
mPlusSucn (suc m) n = pcong suc (mPlusSucn m n)

Pnn' : (m n : ℕ) -> suc m + n ≤ n -> ⊥
Pnn' m zero ()
Pnn' m (suc n) (s≤s t) = Pnn' m n (≤-trans (≤-reflexive (mPlusSucn m n)) t)

-- | Worsening a program with a tick
tickRef : (p : Program TS) -> p ⊴ ✓ p
tickRef p e = refl , n≤1+n (foldProgram cost p e)

-- | Is no possible that a program behaves worse than itself
noImp : (p : Program TS) -> ✓ p ⊴ p -> (e : E) -> ⊥
noImp p false e with false e
noImp p false e | _ , imp = Pnn (foldProgram cost p e) imp

noautoImprov : {e : E}(p : Program TS) -> p ⊴⊵ ✓ p -> ⊥
noautoImprov {e} p pequiv = noImp p (snd pequiv ) e

-- Omega?
omega : Program TS -> Set
omega p = ∀ (e : E) -> ⊥

noAuto : (p : Program TS) -> p ⊴⊵ ✓ p -> omega p
noAuto p equiv = noImp p (snd equiv)

tickElim : (p q : Program TS) -> ✓ p ⊴ q -> p ⊴ q
tickElim p q imp = ⊴-trans {p} {✓ p} {q} (tickRef p) imp

tickIntroLeq : (p q : Program TS) -> p ⊴ q -> ✓ p ⊴ ✓ q
tickIntroLeq p q lpeq e = (fst (lpeq e)) , s≤s (snd (lpeq e))

sucElim : {m n : ℕ} -> suc m ≤ suc n -> m ≤ n
sucElim (s≤s t) = t

tickEqElim : {p q : Program TS} -> ✓ p ⊴⊵ ✓ q -> p ⊴⊵ q
tickEqElim {p} {q} ( fImp , sImp ) =
               < fst ∘ fImp , sucElim ∘ snd ∘ fImp >
               , < fst ∘ sImp , sucElim ∘ snd ∘ sImp >

tickEqIntro : { p q : Program TS} -> p ⊴⊵ q -> ✓ p ⊴⊵ ✓ q
tickEqIntro {p} {q} ( fImp , sImp ) =
            < fst ∘ fImp , s≤s ∘ snd ∘ fImp >
            ,
            < fst ∘ sImp , s≤s ∘ snd ∘ sImp >

---------------------
-- Arbitrary worsening
✓-n : ℕ -> Program TS -> Program TS
✓-n zero T = T
✓-n (suc n) T = ✓ (✓-n n T)

