open import FirstOrderTerms
open import Semantics

module Improvement (S : Signature)(E V : Set)(h : Semantics S E V) where

-- | Unifiying module. The idea is to reexport all the improvement theory (but I dunno if it's working)

open import Improvement.Definition S E V h public
open import Improvement.CtxLemma S E V h public

-- Work in progress
open import Improvement.Induction S E V h
