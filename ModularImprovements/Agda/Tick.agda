
module Tick  where

open import Library
open import FirstOrderTerms
open import Semantics
open import Data.Nat
open import Data.Nat.Properties

{-
 In order to more comfortably manipulate costs expressions we want to extend 
 a language with a tick operator, which does not change semantics but adds a unit of cost.
-}


{-
 We provide the means to extend any signature with a tick operator
-}

data Tick (L : Set) : Set where
   tick : Tick L
   oper : L → Tick L

✓sig : Signature → Signature
✓sig (sig L pars ar) = record {
                 symbol = Tick L
               ; parameter = λ { tick → ⊤
                               ; (oper x) → pars x}
               ; arity = λ { tick → ⊤
                           ; (oper x) → ar x}
               } 

{- We also lift any semantics to a semantics for the tick-extended
 signature.  the evaluation algebra does not change (tick is a no-op),
 but adds a unit of cost.
-}
✓sem : ∀{S E V} → Semantics S E V → Semantics (✓sig S) E V
✓sem (sem ev cost monotonicity) = record {
              ev = λ { (tick , p , a) e → a ⊤.tt e
                     ; (oper x , p , a) e → ev (x , p , a) e}
            ; cost = λ { (tick , p , a) e → 1 + (a ⊤.tt e)
                       ; (oper x , p , a) e → cost (x , p , a) e}
            ; monotonicity = λ { {tick} x {e} →  s≤s (x ⊤.tt e)
                               -- +-monoˡ-≤ 1 (x ⊤.tt e)
                               ; {oper s} x → monotonicity x}
           }

{- Any extended algebra can use the following ✓ operator
   for adding a unit of cost
-}
✓ : ∀{S X} → Term (✓sig S) X → Term (✓sig S) X
✓ t = op (tick , ⊤.tt , (λ x → t))
