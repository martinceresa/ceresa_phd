open import FirstOrderTerms

module Semantics (S : Signature)(E V : Set) where

open import Library
open import Data.Nat
open import Data.Nat.Properties -- (ℕ, ≤) is a PoSet

{- We want to define improvements over languages such that
      - Syntax is given by a Signature
      - Semantics is given by initial algebra semantics over carriers of the form
            E → (V × ℕ)
         where E is an environment
               V are the normal forms
               ℕ is the cost
-}

Carrier : Set
Carrier = E → V × ℕ

{-
  However, an algebra for Carrier has too much freedom.  For example,
  the value of an operator might depend on the cost of the computation
  of its subterms.

  In order to avoid this we construct algebras S (E → V × ℕ) → E → V × ℕ, from
     - an evaluation algebra ev : S (E → V) → E → V
     - a cost algebra      cost : S (E → ℕ) → E → ℕ
     - monotonicity of the cost algebra.
      This basically means that optimising a subterm
      can't make the whole term slower.

  The separation of algebras in this way means that also the cost algebra can't depend
  on the values on which the operator operates. This may be problematic, as in operators
  such as if_then_else, the cost depends on the value of the first argument (true or false).

  A possible solution might be to make available the values for the cost algebra

     cost' : S (E → V × ℕ) → E → ℕ

  Constructing a joint carrier is still possible (in fact, easier).

  mkCarrier' : (OMapSig S (E → V) → E → V)
           → (OMapSig S (E → V × ℕ) → E → ℕ)
           → OMapSig S Carrier → Carrier
  mkCarrier' ev cost x e = (ev (HMapSig S (λ h → fst ∘ h) x) e , cost x e)

  But this change may affect, things like the context lemma.
-}

open Signature

record Semantics : Set where
  constructor sem
  field
    ev   : OMapSig S (E → V) → E → V
    cost : OMapSig S (E → ℕ) → E → ℕ
    monotonicity : {s : symbol S} →
                   {p : parameter S s} →
                   {t u : arity S s → E → ℕ} →
                   ((a : arity S s) → (e : E) → t a e ≤ u a e) →
                   {e : E} →
                   cost (s , p , t) e ≤ cost (s , p , u) e

open Semantics


{- From a Semantics record we construct the algebra on Carrier -}

mkCarrier : Semantics
         → OMapSig S Carrier → Carrier
mkCarrier s x e = (ev s (HMapSig S (λ h → fst ∘ h) x) e)
                       , cost s (HMapSig S (λ h → snd ∘ h) x) e



{- By the banana split theorem (slightly generalized to account for
   the environments) doing a fold on mkCarrier s is the same as doing
   a pair of folds on each of the algebras.
-}
carrierSplit : ∀{X}
             → (s : Semantics)
             → (varVal : X → E → V)
             → (varCost : X → E → ℕ)
             → (t : Term S X)
             → (e : E)
             → (foldTerm (λ x e' → (varVal x e' , varCost x e')) (mkCarrier s) t e) ≅
                (foldTerm varVal (ev s) t e , foldTerm varCost (cost s) t e)
carrierSplit s varVal varCost (var x) e = refl
carrierSplit s varVal varCost (op (σ , p , t)) e =
  cong₂ _,_
  (congS (λ x → ev s x e) (λ a → ext (λ b → cong fst (carrierSplit s varVal varCost (t a) b))))
  (congS (λ x → cost s x e) (λ a → ext (λ b → cong snd (carrierSplit s varVal varCost (t a) b))))

{-
  Therefore we don't need to calculate the unified Carrier, and we can simply
  work with the pair of algebras.
-}

--------------------------------------------------
-- We define a partial order over Carriers

open import Relation.Binary

_≼_ : Carrier  → Carrier  → Set
_≼_ p q = (e : E) → fst (p e) ≅ fst (q e) × snd (p e) ≤ snd (q e)

partialOrder : IsPartialOrder (λ x y → x ≅ y) (_≼_)
partialOrder =
              record
              { isPreorder = record
                  { isEquivalence = Library.isEquivalence
                  ; reflexive     = refl'
                  ; trans         = trans'
                  }
              ; antisym  = antisym'
              }
     where

     refl' : (λ x → _≅_ x) ⇒ _≼_
     refl' refl e = refl , ≤-refl

     trans' : ∀{x y z} → x ≼ y →  y ≼ z →  x ≼ z
     trans' a b = λ e → (trans (fst (a e)) (fst (b e))) , ≤-trans (snd (a e)) (snd (b e))

     antisym≤ :  ∀ {x y} → x ≤ y → y ≤ x → x ≅ y
     antisym≤ z≤n z≤n = refl
     antisym≤ (s≤s a) (s≤s b) = cong suc (antisym≤ a b)

     antisym' :  ∀ {x y} → x ≼ y → y ≼ x → x ≅ y
     antisym' a b = ext (λ e → cong₂ _,_ (fst (a e)) (antisym≤ (snd (a e)) (snd (b e))))
