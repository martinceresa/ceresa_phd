# Agda Implementation

* [x] Initial Algebra Semantics Approach
* [ ] If-Then-Else problem.
  Cost algebra needs to know which branch was taken into account.
* [ ] Improvement Theory
  + [x] Context Lemma
  + [ ] Context Properties.
	**Do we have unique fixpoints?**
    - [ ] Reduction Contexts
    - [x] Constant Contexts.
      Stronger notion. ∀ C ∈ Ctx Ts, ∀ (M N : Program TS), C [ M ] ⊴⊵ C [ N ]
      + [x] Improvement Induction
