open import FirstOrderTerms
open import Semantics

module Improvement.Induction (S : Signature)(E V : Set)(h : Semantics S E V) where

open import Library
open import Data.Nat
open import Data.Nat.Properties -- (ℕ, ≤) is a PoSet

open import Tick
open import Tick.Algebra S E V h

open import Improvement.Definition (✓sig S) E V (✓sem h)
open import Improvement.CtxLemma (✓sig S) E V (✓sem h)
open Semantics.Semantics (✓sem h)

open FirstOrderTerms.Signature

--------------------------------------------------
-- # Improvement Induction

-- ## Why tick is required.
-- In order to prove this "Imp Induction" theorem we need to
-- talk about *observable improvements*.

-- | **Fail**
-- In order to show that the fixpoint is useful we need the tick!
-- Here we can see that *p ⊴⊵ C [ p ]* does not give us anything useful...
-- Think what happens when *C = []*
-- uniqFPointsEq : (p q : Program S)(C : Ctx S)
--               -> p ⊴⊵ C [ p ] -- p ⊴⊵ p
--               -> q ⊴⊵ C [ q ] -- q ⊴⊵ q
--               -> (e : E) -> foldProgram ev p e ≅ foldProgram ev q e

--------------------------------------------------------------------------------
-- | Unused notation, a generalization of the tick relation.
--------------------------------------------------------------------------------
-- _⊴_⊵_ : Program TS -> ℕ -> Program TS -> Set
-- p ⊴ zero ⊵ q = p ⊴⊵ q
-- p ⊴ suc n ⊵ q =  p ⊴ n ⊵ ✓ q

-- _⊴_∣_ : Program TS -> ℕ -> Program TS -> Set
-- p ⊴ zero ∣ q = p ⊴ q
-- p ⊴ suc n ∣ q = ✓ p ⊴ n ∣ q
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- ## Constant Contexts
-- Given a context C we say that C is constant if C behaves exactly the same
-- filling the hole with any program.
constCtx : (C : Ctx TS) -> Set
constCtx C = ∀ (M N : Program TS) -> C [ M ] ⊴⊵ C [ N ]

strNotion : (C : Ctx TS) -> constCtx C
          -> (M : Program TS)
          -> C [ ✓ M ] ⊴ C [ M ]
strNotion C HCon M = fst (HCon (✓ M) M)

-- | For example, we can prove that the trivial context is not
-- constant.
trivNoConst : constCtx (var ⊤.tt) -> Program TS -> (e : E) -> ⊥
trivNoConst HTri N = noImp N (fst (HTri (✓ N) N ))

-- | Can I write a function to remove holes? (POC)
-- the hole idea here is that constant contexts do not
-- evaluates (or do not need its holes).
-- cnstCtx : (C : Ctx TS) -> constCtx C -> Program TS
-- cnstCtx C = {!!}
const≾≿ : (C : Ctx TS) -> Set
const≾≿ C = ∀ (p q : Program TS) -> C [ p ] ≾≿ C [ q ]
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- ## Reduction Contexts

-- Reduction contexts are contexts which have to compute whatever is placed
-- in the hole in order to continue evaluation.

-- Since we do not really know how to completely define them
-- here we have a few definitions to play around.

-- ### Moran & Sands Reduction Context prop

-- Why just one tick? *I guess* the intuition behind this reduction prop
-- is better understood thinking in small step semantics. If *M*
-- can make an step, so does C [ M ] since the step is the only thing that
-- *C* can (or should) do.
-- This is a very strong notion, which is not true in general, but we can use it
-- to explore its properties, and later on generalize it into something like
-- C [ ✓ M ] ⊴⊵ ✓ⁿ C [ M ]
reductionMS : (C : Ctx TS) -> Set
reductionMS C = ∀ (M : Program TS) -> C [ ✓ M ] ⊴⊵ ✓ (C [ M ])

-- For example:
trivRedMS : reductionMS (var ⊤.tt)
trivRedMS M = ⊴⊵-refl {✓ M}
--------------------------------------------------------------------------------
-- Another property that *I* think should have reduction contexts is

-- Here we should only compare costs... ≾
reductionCtxAlt : Ctx TS -- Given a context C ∈ Ctx TS
                -> Set -- such that
reductionCtxAlt C = ∀ (M : Program TS) -- forall program M,
                    -> M ⊴ C [ M ] -- that is, C computes at least M

-- Now we have a better def, a given context C is called 'reduction context'
-- if forall programs, C [ p ] costs at least p.
reduction≾ : Ctx TS -> Set
reduction≾ C = ∀ (p : Program TS) -> p ≾ C [ p ]

hipProof : (C : Ctx TS) -> const≾≿ C ⊎ reduction≾ C
hipProof (var x) = inj₂ λ p → ≾-refl {p}
hipProof (op (s , p , t)) = {!!}

-- For example, [-] is a reduction context
trivRedAlt : reductionCtxAlt (var ⊤.tt)
trivRedAlt M = ⊴-refl {M}

-- Every reduction context C is a /post-fixpoint/
prefixRed : (C : Ctx TS) -> ( M : Program TS) -> reductionCtxAlt C -> M ⊴ ✓ C [ M ]
prefixRed C M red = ⊴-trans {M}{C [ M ]}{✓ C [ M ]}
                                  (red M ) (tickRef (C [ M ]))

-- A reduction context *cannot* be a /pre-fixpoint/
-- ✓ C [ M ] ⊴ M ⊴ C [ M ] ⇒ ✓ C [ M ] ⊴ C [ M ] ⇒ ⊥
redNoPre : (C : Ctx TS)(M : Program TS)
              -> reductionCtxAlt C
              -> ✓ C [ M ] ⊴ M -> (e : E) -> ⊥
redNoPre C M red Hip = noImp (C [ M ])
                      (⊴-trans {✓ C [ M ]} {M} {C [ M ]} Hip (red M))
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- ### Contexts Universe
--------------------------------------------------------------------------------
ctxHip : Set
ctxHip = ∀ (C : Ctx TS) -- Every context C
         -> constCtx C -- is either a constant
         ⊎ reductionMS C -- or a reduction context

-- That reduction ctx definition is not enough.
-- Or probably, my brain is not enough to prove all the theorems

-- Fixpoint definition
fixpoint : Ctx TS -> Program TS -> Set
fixpoint C N = ✓ C [ N ] ⊴⊵ N

bigHipAlt : Set
bigHipAlt = ∀ (C : Ctx TS) -> constCtx C ⊎ reductionCtxAlt C

module AssumedCtxAlt
  (universe : bigHipAlt) -- | Every context is either constant or a reduction one.
  where
  preFixAreCnt : (C : Ctx TS)( N : Program TS) -> ✓ C [ N ] ⊴ N -> constCtx C
  preFixAreCnt C N PN with universe C
  preFixAreCnt C N PN | inj₁ cnst = cnst
  preFixAreCnt C N PN | inj₂ red = λ p q → (λ e -> case redNoPre C N red PN e of λ{ () })
                                           , λ e → case redNoPre C N red PN e of λ{ ()}

  -- Every Context that has a fixpoint (or is a fixpoint) must be a constant context
  fixpointAreCnt : (C : Ctx TS)(N : Program TS) -> fixpoint C N -> constCtx C
  fixpointAreCnt C N FN = preFixAreCnt C N (fst FN)

  uniqueness : (C : Ctx TS)(M N : Program TS) -> fixpoint C N -> fixpoint C M -> N ⊴⊵ M
  uniqueness C M N FN FM with fixpointAreCnt C N FN
  ... | cnst = ⊴⊵-trans {N} {✓ C [ N ]} {M}
                            (⊴⊵-sym {✓ C [ N ]} {N} FN)
               (⊴⊵-trans {✓ C [ N ]} {✓ C [ M ]} {M}
                            (tickEqIntro {C [ N ]}{C [ M ]} (cnst N M))
                            FM)
  -- | Improvement Induction given as Sands.
  impInduction : (M N : Program TS) -- Given two programs
               -> (C : Ctx TS) -- and a context
               -- such that
               -> fixpoint C N -- N is a fix-point
               -> ✓ (C [ M ]) ⊴ M -- M is a post-fix point
               -- Then M is improved by N
               -> N ⊴ M
  impInduction M N C FN PFM with fixpointAreCnt C N FN
  ... | cnst = ⊴-trans {N} {✓ C [ N ]} {M}
                       (snd FN)
               (⊴-trans {✓ C [ N ]} {✓ C [ M ]} {M}
                        (tickIntroLeq (C [ N ]) (C [ M ]) (fst (cnst N M)))
                        PFM)

  -- | Lesser hips
  impLessInd : ( M N : Program TS)
             -> (C : Ctx TS)
             -> N ⊴ ✓ C [ N ]
             -> ✓ C [ M ] ⊴ M
             -> N ⊴ M
  impLessInd M N C PN PM with preFixAreCnt C M PM
  ... | cnstC = ⊴-trans {N} {✓ C [ N ]} {M} PN
                (⊴-trans {✓ C [ N ]} {✓ C [ M ]} {M}
                         (tickIntroLeq (C [ N ]) (C [ M ]) (fst (cnstC N M)))
                         PM)
