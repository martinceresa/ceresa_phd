open import FirstOrderTerms
open import Semantics

module Improvement.CtxLemma (S : Signature)(E V : Set)(h : Semantics S E V) where

open import Improvement.Definition S E V h
open import Library

open Semantics.Semantics h

-- We now prove a "context lemma":
-- Any improvement p₁ ⊴ p₂ is valid under any context.
ctxLemma : (p₁ p₂ : Program S)
         → p₁ ⊴ p₂
         → (C : Ctx S)
         → C [ p₁ ] ⊴ C [ p₂ ]
ctxLemma p₁ p₂ i (var x) e = i e
ctxLemma p₁ p₂ i (op (s , r , t)) e =
         (congS (λ x → ev x e) (λ a → ext (fst ∘ ctxLemma p₁ p₂ i (t a))))
         , monotonicity (λ a → snd ∘ ctxLemma p₁ p₂ i (t a))

ctxLemmaD : (p q : Program S)
          -> p ⊴⊵ q
          -> (C : Ctx S)
          -> C [ p ] ⊴⊵ C [ q ]
ctxLemmaD p q (piq , qip) C = (ctxLemma p q piq C) , (ctxLemma q p qip C)

ctxCost : (p q : Program S)
        -> p ≾ q
        -> (C : Ctx S)
        -> C [ p ] ≾ C [ q ]
ctxCost p q imp (var x) = imp
ctxCost p q imp (op (s , r , t)) e = monotonicity (λ a → ctxCost p q imp (t a))
