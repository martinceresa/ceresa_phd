open import FirstOrderTerms
open import Semantics

module Improvement.Context (S : Signature)(E V : Set)(h : Semantics S E V) where

open import Library
open import Data.Nat
open import Data.Nat.Properties -- (ℕ, ≤) is a PoSet
open import Relation.Binary

open import Tick
open import Tick.Algebra S E V h
open import Improvement.Definition S E V h
open import Improvement.CtxLemma S E V h

open Semantics.Semantics (✓sem h)

open FirstOrderTerms.Signature

--------------------------------------------------------------------------------
-- # Context problems
--------------------------------------------------------------------------------

-- We have to take into our definitions the environment, since the environment
-- has information about the evaluation. Some contexts behave as reduction
-- context to a given environment and as constant context to another
-- environment.

-- Such as : C = if b then [-] else 42
-- where b is taken from[defined in] the environment.

-- So now I am going to follow closely the definitions in Moran&Sands's *Improvement
-- in a Lazy Context: An Operational Theory for Call-By-Need*. There they have proved a
-- theorem called *Open Uniform Computation* which says (briefly) that:
-- ∀ C ∈ Ctx, e ∈ Environment, eval (e , C) →ᵏ § ↛, then § has one of the following
-- forms:
--  + (Δ , V) where V ∈ Value and Δ ∈ Environment
--  + (Δ , [-]) where Δ ∈ Environment

-- Borrowing from that idea the main difference with our previous approach is that
-- we are going to parameterize the definition of each context by a given environment

--------------------------------------------------------------------------------
-- Constant Context
--------------------------------------------------------------------------------

constantCtx : Ctx TS -> (e : E) -> Set
constantCtx C e = ∀ (t₁ t₂ : Program TS)
              -> foldProgram cost (C [ t₁ ]) e
                ≅ foldProgram cost (C [ t₂ ]) e

costD : (p : Program TS) -> (C : Ctx TS)
      -> foldTerm (λ x -> foldProgram cost p) cost C
        ≅ foldProgram cost (C [ p ])
costD = {!!}
-- costD p (var x) = refl
-- costD p (op (s , pa , t)) =
--   proof
--    foldTerm (λ x → foldProgram cost p)
--      cost (op (s , pa , t))
--    ≅⟨ refl ⟩
--    cost (s , pa , λ a -> foldTerm (λ x -> foldProgram cost p) cost (t a))
--    ≅⟨ congS cost (λ a → costD p (t a)) ⟩
--    cost (s , pa , λ a -> foldProgram cost (t a [ p ] ))
--    ≅⟨ refl ⟩
--    foldProgram cost (op (s , pa , t) [ p ])
--    ∎

-- These things can represent more programs than we actually have.
-- Cost function doesn't have to be surjective.
-- ⟦_⟧ : (n : ℕ) -> E -> ℕ
-- ⟦ n ⟧ = λ _ -> n

-- Constant Programs
-- cnstP : (t : Program S) -> Set
-- cnstP t = ∀ (e : E) -> foldProgram cost t e ≅ 0
-- *Not so sure about this one*
-- Taking Moran&Sands's language and machine
-- id = λ x -> x ∈ V
-- But, takking e = (H , [ 1 ]).
--------------------------------------------
-- (H , λ x . x , 1 : ε)
-- -> Subst
-- (H , 1 , ε)

-- trivIsConst : ∀ (e : E) ->  constantCtx (var ⊤.tt) e -> ⊥
-- trivIsConst e false = {!!}

-- Programs
≤-cong : {m n : ℕ} -> m ≅ n -> m ≤ n
≤-cong {m} {.m} refl = ≤-refl

≅-sym : {A B : Set} -> {a : A} -> {b : B} -> a ≅ b -> b ≅ a
≅-sym refl = refl

-- const : (C : Ctx S)(p q : Program S)
--       -> foldProgram cost (C [ p ]) ≅ foldProgram cost (C [ q ])
--       -> C [ p ] ≾≿ C [ q ]
-- const C p q eqC = (λ e → ≤-cong (cong-app eqC e) )
--                   , λ e →  ≤-cong (cong-app (≅-sym eqC) e)

-- constantCtxPCtx : (C : Ctx S) -> constantCtx C -> constantPCtx C
-- constantCtxPCtx C cnst p q =
--   const C p q
--             (proof
--             foldProgram cost (C [ p ])
--             ≅⟨ ≅-sym (costD p C )⟩
--             foldTerm (λ x -> foldProgram cost p) cost C
--             ≅⟨ cnst (foldProgram cost p) (foldProgram cost q) ⟩
--             foldTerm (λ x -> foldProgram cost q) cost C
--             ≅⟨ costD q C ⟩
--             foldProgram cost (C [ q ])
--             ∎)


-- --------------------------------------------------------------------------------
-- -- Reduction Context -- Not so clear in my mind
-- --------------------------------------------------------------------------------

-- _⟪_ : (E -> ℕ) -> (E -> ℕ) -> Set
-- n ⟪ m = ∀ (e : E) -> n e < m e

-- _⟪=_ : (E -> ℕ) -> (E -> ℕ) -> Set
-- n ⟪= m = ∀ (e : E) -> n e ≤ m e

-- ⟪=-refl : {m : E -> ℕ} -> m ⟪= m
-- ⟪=-refl = λ e → ≤-refl

-- ⟪=-trans : Transitive _⟪=_
-- ⟪=-trans {i}{j}{k} ij jk = λ e → ≤-trans {i e}{j e}{k e} (ij e) (jk e)

-- ⟪=-cong : { p q : E -> ℕ} -> p ≅ q -> p ⟪= q
-- ⟪=-cong {p} {.p} refl = ⟪=-refl

-- _⋦_ : (p q : Program S) -> Set
-- p ⋦ q = ∀ (e : E) -> foldProgram cost p e < foldProgram cost q e

-- Ctx Monotonicity is the same as reduction contexts?
-- Those context that preserves strict improvements are reduction contexts?
-- Are they strict contexts? Reduction Ctx vs Strict Ctx, to be continued
-- Strict : C [ Ω ] = Ω
-- redContexts : Ctx S -> Set
-- redContexts C = ∀ (p q : Program S)
--             -> p ⋦ q -> C [ p ] ⋦ C [ q ]

--------------------------------------------------------------------------------
-- Lifting reduction cost notion to programs.
--------------------------------------------------------------------------------
subtCost : (p : Program TS)(C : Ctx TS) -> (e : E)
         -> foldTerm (λ _ -> foldProgram cost p) cost C e ≤ foldProgram cost (C [ p ]) e
subtCost p (var x) e = ≤-refl
subtCost p (op (s , P , t)) e = {!!} -- monotonicity (λ a → subtCost p (t a))

-- Reduction Ctx in a given Environment
-- C is a reduction contex iff ∀ p ∈ Program, p ≾ C [ p ]
reductionCtx : Ctx TS -> (e : E) -> Set
reductionCtx C e = ∀ (P : Program TS) -> foldProgram cost P e ≤ foldProgram cost (C [ P ]) e

-- Alternative definition, its useful since relates tick and ctxs.
altRed : Ctx (✓sig S) -> (e : E) -> Set
altRed C e = ∀ (P : Program (✓sig S)) -> foldProgram cost (✓ C [ P ]) e ≤ foldProgram cost (C [ ✓ P ]) e

-- redCtx : (C : Ctx S) -> reductionCtx C -> (p : Program S) -> p ≾ C [ p ]
-- redCtx C red p = λ e → ≤-trans {foldProgram cost p e} {foldTerm (λ _ -> foldProgram cost p) cost C e}
--                                 {foldProgram cost (C [ p ]) e}
--                                 (red (foldProgram cost p) e)
--                                 (subtCost p C e)

--------------------------------------------------------------------------------
-- # Context Composition Algebra
--------------------------------------------------------------------------------

-- ContextComposition

_⊛_ : Term TS ⊤ -> Term TS ⊤ -> Term TS ⊤
C ⊛ R = foldTerm (λ ⊤ -> R) op C

-- This property is a bit more general than we actually need
-- (since Program S ⊂ (E -> ℕ))
destructCtxComp : (n : E -> ℕ)
            (C R : Ctx TS) ->
            foldTerm (λ _ -> foldTerm (λ _ -> n ) cost R) cost C
            ≅
            foldTerm (λ y -> n) cost (C ⊛ R)
destructCtxComp n (var x) R = refl
destructCtxComp n (op (s , p , t)) R = {!!} --  congS cost λ a → destructCtxComp n (t a) R

subEq : (C R : Ctx TS) -> ( M : Program TS)
     -> foldProgram cost (C [ (R [ M ] ) ])
       ≅ foldProgram cost ((C ⊛ R) [ M ])
subEq C R M = proof
                foldProgram cost (C [ R [ M ] ])
                ≅⟨ ≅-sym (costD (R [ M ]) C) ⟩
                foldTerm (λ x -> foldProgram cost (R [ M ])) cost C
                ≅⟨ cong (λ p → foldTerm p cost C) (ext (λ _ -> ≅-sym (costD M R))) ⟩
                foldTerm (λ x -> foldTerm (λ y -> foldProgram cost M) cost R) cost C
                ≅⟨ destructCtxComp (foldProgram cost M) C R ⟩
                foldTerm (λ y -> foldProgram cost M) cost (C ⊛ R)
                ≅⟨ costD M (C ⊛ R) ⟩
                foldProgram cost ((C ⊛ R) [ M ])
                ∎

-- subEqC : (C R : Ctx TS) -> (M : Program TS)
--        -> (C ⊛ R) [ M ] ≾≿ C [ R [ M ] ]
-- subEqC C R M = (λ e → ≤-cong (≅-sym (cong-app (subEq C R M) e)))
--                , λ e → ≤-cong (cong-app (subEq C R M) e)

≾≿-eq : (C : Ctx S) -> (P Q : Program S)
      -> P ≾≿ Q -> C [ P ] ≾≿ C [ Q ]
≾≿-eq C p q peqq = ctxCost p q (fst peqq) C , ctxCost q p (snd peqq) C

≅-eq : (C : Ctx TS) -> ( P Q : Program TS) -> (e : E)
     -> foldProgram cost P e ≅ foldProgram cost Q e
     -> foldProgram cost (C [ P ]) e ≅ foldProgram cost (C [ Q ]) e
≅-eq (var x) P Q e eq = eq
≅-eq (op (c , ps , ts)) P Q e eq = {!!}


--------------------------------------------------------------------------------
-- Composition with a constant ctx is a constant ctx
--------------------------------------------------------------------------------

absIzq⊛ : (C R : Ctx TS) -> (e : E) -> constantCtx C e -> constantCtx (C ⊛ R) e
absIzq⊛ C R e cstC M N with cstC (R [ M ]) (R [ N ])
absIzq⊛ C R e cstC M N | eq = trans (sym (cong-app (subEq C R M) e))
                              (trans eq (cong-app (subEq C R N) e))

-- TODO : Almost same as absIzq
-- absDer⊛ : (C R : Ctx S) (e : E) -> constantCtx R e -> constantCtx (C ⊛ R) e
-- absDer⊛ C R e cstR M N with cstR M N
-- ... | eqR = trans (sym (cong-app (subEq C R M) e)) (trans {!≅-eq!} {!!})

--------------------------------------------------------------------------------
-- Composition between reduction ctxs is a reduction ctx
--------------------------------------------------------------------------------
-- Todo
-- redComp : (R1 R2 : Ctx S)
--             -> reductionCtx R1
--             -> reductionCtx R2
--             -> reductionCtx (R1 ⊛ R2)
-- redComp r t redr redt n p = ≤-trans {n p}
--                                      {foldTerm (λ _ → foldTerm (λ _ → n) cost t) cost r p}
--                                      {foldTerm (λ _ → n) cost (r ⊛ t) p}
--                             (≤-trans {n p}{foldTerm (λ _ → n) cost t p}
--                                          {foldTerm (λ _ → foldTerm (λ _ → n) cost t) cost r p}
--                                          (redt n p)
--                                          (redr (foldTerm (λ _ -> n) cost t) p))
--                             (≤-cong (cong-app (destructCtxComp n r t) p))

--------------------------------------------------------------------------------
-- Universe Dichotomy
--------------------------------------------------------------------------------

✓n : (n : ℕ) -> Program TS -> Program TS
✓n zero p = p
✓n (suc n) p = ✓ (✓n n p)

✓C : Ctx TS
✓C = ✓ (var ⊤.tt)

everyTerm : (P Q : Program TS)
          -> (e : E)
          -> foldProgram cost P e ≤ foldProgram cost Q e
          -> foldProgram cost (✓n (foldProgram cost Q e ∸ foldProgram cost P e) P) e
            ≅ foldProgram cost Q e
everyTerm p q e ls = {!ls!}

tickInv : (C : Ctx TS) (e : E ) -> Set
tickInv C e = ∀ (t : Program TS)
                -> foldProgram cost (C [ t ]) e ≅ foldProgram cost (C [ ✓ t ]) e

everNat : (C : Ctx TS)(e : E)
        -> tickInv C e
        -> (P : Program TS)
        -> (n : ℕ) -> foldProgram cost (C [ P ]) e ≅ foldProgram cost (C [ ✓n n P ]) e
everNat C e eq P zero = refl
everNat C e eq P (suc n) = trans (everNat C e eq P n) (eq (✓n n P))

cstDefs : (C : Ctx TS)(e : E)
        -> tickInv C e
        -> constantCtx C e
cstDefs C e tInv p q = {!!} -- The problem here is that the environment
-- is different in different places inside C.

isCstCtx : (C : Ctx TS)(e : E) (Test : Program TS) -> Dec (constantCtx C e)
isCstCtx C e t with foldProgram cost (C [ t ]) e ≟ foldProgram cost (C [ ✓ t ]) e
isCstCtx C e t | yes p = yes (cstDefs C e {!p!}) -- Todo: ≅ ? ≡
isCstCtx C e t | no np = {!!}

isRedCtx : (C : Ctx TS)(e : E) (Test : Program TS) -> Dec (altRed C e)
isRedCtx C e t with foldProgram cost (C [ t ]) e ≟ foldProgram cost (C [ ✓ t ]) e
... | test = {!!}

-- ∀ C ∈ Ctx, ∀ e ∈ E, C is either a reduction context or a constant context
-- under e.
dich : (C : Ctx TS) -> (e : E) -> (constantCtx C e) ⊎ (reductionCtx C e)
dich C e = {!!}

--------------------------------------------------------------------------------
-- Commented out some properties and definitions
--------------------------------------------------------------------------------

-- Reduction Ctx Characterization ?
-- p < q ⇒ C [ p ] < C [ q ]
-- C [ ✓ p ] ≤ ✓ C [ p ]
-- C [ p ] < C [ ✓ p ]

-- trivRed : reductionCtx (var ⊤.tt)
-- Do I have to prove the other prop? I would be wonderful if this one works
-- Since I proved it with this hyp.
