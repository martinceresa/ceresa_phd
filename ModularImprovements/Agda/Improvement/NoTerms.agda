module Improvement.NoTerms (E V : Set) where

open import Library
open import Data.Nat
open import Data.Nat.Properties -- (ℕ, ≤) is a PoSet
open import Relation.Binary
open import Relation.Nullary

--------------------------------------------------------------------------------
-- Summary
--------------------------------------------------------------------------------
-- Big Idea here: I wanted to be liberated from the structure of
-- contexts and programs. Hence, I ignored completely its structure
-- and just see a Term as its cost function (Term ≡ E -> ℕ) and
-- a Context just as a function that takes a Term and gives us back
-- a new one (Ctx ≡ Term -> Term)

-- But this is a really bad idea, since not every (n ∈ ℕ) has a program which
-- computes in such cost. Hence, taking Terms to be just functions (E → ℕ) is
-- not a very good idea.

----------------------------------------
-- ℕ things
----------------------------------------
≤-cong : {m n : ℕ} -> m ≅ n -> m ≤ n
≤-cong refl = ≤-refl

≥-cong : {m n : ℕ} -> m ≅ n -> n ≤ m
≥-cong refl = ≤-refl

--------------------------------------------------------------------------------
-- # Definitions
--------------------------------------------------------------------------------

-- Terms are just functions from E to V
-- E : environment
-- V : values
Term : Set
Term = (E -> ℕ)

----------------------------------------
-- ## Functions to create new terms based on
-- old ones.
----------------------------------------

-- Min/Max between two terms
maxN : ℕ -> ℕ -> ℕ
maxN zero n = n
maxN (suc m) zero = suc m
maxN (suc m) (suc n) = suc (maxN m n)

minN : ℕ -> ℕ -> ℕ
minN zero n = zero
minN (suc m) zero = zero
minN (suc m) (suc n) = suc (minN m n)

maxT : Term -> Term -> Term
maxT p q e = maxN (p e) (q e)

minT : Term -> Term -> Term
minT p q e = minN (p e) (q e)

-- Constants terms
⟦_⟧ : ℕ -> Term
⟦ n ⟧ = λ _ -> n

Zero : Term
Zero = ⟦ 0 ⟧

-- Sum terms
_⊕_ : Term -> Term -> Term
p ⊕ q = λ x → p x + q x

--------------------------------------------------------------------------------
-- Context Definition
--------------------------------------------------------------------------------
-- Contexts are transformations of programs, so they are
-- functions from Term to Term
Ctx : Set
Ctx = Term -> Term

--------
--- WTF!
--------

WTF1 : Ctx
WTF1 t e = case t e <? 10 of
                λ{ (yes p) → 5
                ; (no ¬p) → t e}

-- Id ctx. [-]
idCtx : Ctx
idCtx = id

-- There is an special context which just add a tick to a given program.
✓ : Ctx
✓ t = suc ∘ t

-- and we can apply it an arbitrary number of times.
n✓ : ℕ -> Ctx
n✓ zero = λ x x₁ → x x₁
n✓ (suc m) = ✓ ∘ n✓ m

--------------------------------------------------------------------------------
-- Relation (⟪=) between terms.
--------------------------------------------------------------------------------
-- ≤ is anty symmetric, it should be somewhere in the std-lib but I do
-- not know where...
antiSymNat : {p q : ℕ} -> p ≤ q -> q ≤ p -> p ≅ q
antiSymNat {zero} {zero} l g = refl
antiSymNat {zero} {(suc q)} l ()
antiSymNat {(suc p)} {zero} () g
antiSymNat {(suc p)} {(suc q)} (s≤s l) (s≤s g) = cong suc (antiSymNat l g)

-- Relation between terms
-- Lifting ≤ to ⟪=.
-- Note that they are uniformm that its forall environment one is
-- better/worse than the other.
_⟪=_ : Term -> Term -> Set
m ⟪= n = ∀ (e : E) -> m e ≤ n e
-- Note: there are some terms left behind with that definition.
-- Terms that for some enviroments switch behaviour.

-- Example:
-- t  : if b then ⟦ n ⟧ else ⟦ m ⟧
-- e₁ : b := true
-- e₂ : b := false
-- ⟦ n ⟧ ≮ t
-- ⟦ m ⟧ ≮ t

⟪=-refl : { p : Term} -> p ⟪= p
⟪=-refl = λ e → ≤-refl

⟪=-antisym : { p q : Term} -> p ⟪= q -> q ⟪= p -> p ≅ q
⟪=-antisym pq qp = ext (λ a → antiSymNat (pq a) (qp a))

⟪=-trans : Transitive _⟪=_
⟪=-trans {i}{j}{k} ij jk = λ e → ≤-trans {i e}{j e}{k e} (ij e) (jk e)

⟪=-cong : { p q : Term} -> p ≅ q -> p ⟪= q
⟪=-cong {p} {.p} refl = λ e → ≤-refl


----------------------------------------
-- Contradictions
----------------------------------------

----------------------------------------
-- ✓ p ⟪= p ⇒ ⊥
----------------------------------------
ipso : {p : Term} -> ✓ p ⟪= p -> (e : E) -> ⊥
ipso {p} false e with false e
... | cost = 1+n≰n cost

✓Matters : {n : ℕ}{p : Term}
        -> n✓ (suc n) p ⟪= n✓ n p -> (e : E) -> ⊥
✓Matters = ipso

----------------------------------------
--
----------------------------------------
n✓Least : {n : ℕ}{P : Term} -> ⟦ n ⟧ ⟪= (n✓ n P)
n✓Least {zero} {P} = λ e → z≤n
n✓Least {suc n} {P} e = s≤s (n✓Least e)

-- 'n✓ n [.]' is a reduction ctx
n✓PL : {n : ℕ}{P : Term} -> P ⟪= (n✓ n P)
n✓PL {zero} {P} = λ e → ≤-refl
n✓PL {suc n} {P} = ⟪=-trans {P}{n✓ n P}{n✓ (suc n) P}
                            (n✓PL {n}{P})
                            λ e → n≤1+n (n✓ n P e)

----------------------------------------
-- ∀ m ∈ ℕ , n✓ m monotonicity. (n✓ is a Ctx, so we already knew that)
----------------------------------------
n✓Mono : {m n : ℕ}{p : Term}
       -> m ≤ n -> n✓ m p ⟪= n✓ n p
n✓Mono {zero} {n} {p} hmn = n✓PL {n}{p}
n✓Mono {suc m} {zero} {p} ()
n✓Mono {suc m} {suc n} {p} (s≤s hmn) e = s≤s (n✓Mono hmn e)

n✓MonoP : {m : ℕ}{ P Q : Term}
        -> P ⟪= Q -> n✓ m P ⟪= n✓ m Q
n✓MonoP {zero} hip = hip
n✓MonoP {suc m} {p}{q} hip e = s≤s (n✓MonoP {m}{p}{q} hip e)

----------------------------------------
-- Zero props
----------------------------------------
-- Zero is the least element
⟪=-min : (t : Term) -> ⟦ 0 ⟧ ⟪= t
⟪=-min t e = z≤n

Zero! : {t : Term} -> t ⟪= ⟦ 0 ⟧ -> t ≅ ⟦ 0 ⟧
Zero! {t} tL = ⟪=-antisym tL (⟪=-min t)

-- n✓ m Zero ≅ m
n✓Z : {m : ℕ}{e : E} -> n✓ m ⟦ 0 ⟧ e ≅ m
n✓Z {zero} {e} = refl
n✓Z {suc m} {e} = cong suc (n✓Z {m}{e})

----------------------------------------
-- Terms as ticks from 0, nothing crazy here
-- but I wanted to relate all things to Zero
----------------------------------------
-- Term simulator
everyTerm : Term -> Term
everyTerm P = λ x → n✓ (P x) Zero x

everyTermPless : ( P : Term) -> P ⟪= everyTerm P
everyTermPless P e with P e
everyTermPless P e | zero = z≤n
everyTermPless P e | suc pcost = s≤s (≥-cong n✓Z)

everyTermPGreater : (P : Term) -> everyTerm P ⟪= P
everyTermPGreater P e with P e
everyTermPGreater P e | zero = z≤n
everyTermPGreater P e | suc pcost = s≤s (≤-cong n✓Z)

everyTermIs✓ : {P : Term} -> P ≅ everyTerm P
everyTermIs✓ {P} = ⟪=-antisym (everyTermPless P)
                              (everyTermPGreater P)

----------------------------------------

tR : { m n : ℕ} -> suc m ≤ suc n -> m ≤ n
tR {zero} {n} r = z≤n
tR {suc m} {zero} (s≤s ())
tR {suc m} {suc n} (s≤s r) = r

tickRem : (m : ℕ)(p q : Term)
        -> (n✓ m p  ⟪= (n✓ m q))
        -> p ⟪= q
tickRem zero p q hip e = hip e
tickRem (suc m) p q hip e with hip e
tickRem (suc m) p q hip e | s≤s res = tickRem m p q (tR ∘ hip) e

-- ✓ (n✓ m P) = n✓ m (✓ P)
inside : (m : ℕ)(n : Term)
       -> (e : E)
       -> ✓ (n✓ m n) e ≅ n✓ m (✓ n) e
inside zero n  e =  refl
inside (suc m) n e = cong suc (inside m n e)

--------------------------------------------------------------------------------
-- Relation between ctxs
--------------------------------------------------------------------------------

_≾_ : (P Q : Ctx) -> Set
p ≾ q = ∀ (t : Term)(e : E) -> p t e ≤ q t e
-- I wanted to expand our language, maybe if we abstract
-- away some behaviour we can see things more clear

-- The idea was to define a relation that does not depends
-- directly on terms, so I can write down some properties
-- about just Ctxs. I want something like a test.

propTick : (P Q : Ctx)
         -> P ≾ Q
         ->  (P ∘ ✓) ≾ (Q ∘ ✓)
propTick p q hip = λ t → hip (suc ∘ t)

_≾≿_ : Ctx -> Ctx -> Set
p ≾≿ q = p ≾ q × q ≾ p

≾≿-cong : (P Q : Ctx) -> P ≾≿ Q -> P ≅ Q
≾≿-cong p q hip = ext (λ t → ext (λ e → antiSymNat (fst hip t e) (snd hip t e)))

≾-refl : (P : Ctx ) -> P ≾ P
≾-refl p = λ t e → ≤-refl

postTicked : (P : Ctx) -> P ≾ (✓ ∘  P)
postTicked p t e = n≤1+n (p t e)

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Ctx mon. This is what we get from IAS
--------------------------------------------------------------------------------
monCtx : Set
monCtx = ∀ (c : Ctx)(m n : Term) -> m ⟪= n -> c m ⟪= c n

--------------------------------------------------------------------------------
-- Classics definitions
-- Reduction and Constant Contexts
--------------------------------------------------------------------------------
redCtx : Ctx -> Set
redCtx c = ∀ (n : Term) -> n ⟪= c n

cnstCtx : Ctx -> Set
cnstCtx c = ∀ (m n : Term)(e : E) ->  c m e ≅ c n e

----------------------------------------
-- ✓ is red
----------------------------------------

tickRed : redCtx ✓
tickRed m e = n≤1+n (m e)

----------------------------------------
-- [-] is red
----------------------------------------

idRed : redCtx id
idRed m e = ≤-refl

----------------------------------------
-- ⟦ n ⟧ and constant terms are constants
----------------------------------------

⟦⟧cnst : {n : ℕ} -> cnstCtx (λ _ -> ⟦ n ⟧)
⟦⟧cnst {n} = λ m n₁ e → refl

termsCnst : (t : Term) -> cnstCtx (λ _ -> t)
termsCnst t = λ m n e → refl

--------------------------------------------------------------------------------
-- Contexts manipulation
--------------------------------------------------------------------------------

-- Given that C [✓ n] ≾ C [ n ], we can exploit how many times we want
prop'' : (C : Ctx)
       -> (∀ (n : Term) -> C (✓ n) ⟪= C n)
       -> (∀ (n : Term)(m : ℕ) -> C ( n✓ m n ) ⟪= C n)
prop'' C cH n zero = λ e → ≤-refl
prop'' C cH n (suc zero) = cH n
prop'' C cH n (suc (suc m)) = ⟪=-trans {C (n✓ (suc (suc m)) n)}{C (✓ n)}{C n}
                              (⟪=-trans {C (n✓ (suc (suc m)) n)}
                                        {C ((n✓ (suc m) (✓ n)) )}
                                        {C (✓ n)}
                              (⟪=-cong (cong C (ext (λ a → inside (suc m) n a))))
                                (prop'' C cH (✓ n) (suc m)))
                                (cH n)

prop✓ : (C : Ctx)
      -> ((n : Term) -> C (✓ n) ⟪= C n)
      -> (m : ℕ)
      -> (P : Term)
      -> C (n✓ m P) ⟪= C P
prop✓ C hip zero P = λ e → ≤-refl
prop✓ C hip (suc m) P = ⟪=-trans {C (n✓ (suc m) P)}{ C (n✓ m P)}
                                 {C P}
                                 (hip (n✓ m P))
                                 (prop✓ C hip m P)

-- Since C can absorb ticks, I can remove them from the inside
propd✓ : (C : Ctx)
       -> ((n : Term) -> C (✓ n) ⟪= C n)
       -> (n : Term)(P : Term)
       -> C (λ x -> n✓ ( ✓ n  x ) P x)
       ⟪= C (λ x -> n✓ ( n x) P x)
propd✓ C hip n P = hip (λ z → n✓ (n z) P z)

module Univ
       (ctxMon : monCtx) -- Ctx Monotonicity
         where
  -- Note that in order to prove P ≾ (P ∘ ✓) we needed ctxMon (obviously)
  -- In fact here we may want to prove P ≺ ✓ P
  tickedCtx : (P : Ctx) -> P ≾ (P ∘ ✓)
  tickedCtx p t e = ctxMon p t (✓ t) (λ e₁ → n≤1+n (t e₁)) e

  lessToEq : (C : Ctx)(m : Term) -> C (✓ m) ⟪= C m -> C (✓ m) ≅ C m
  lessToEq C m hip = ⟪=-antisym hip (ctxMon C m (✓ m) λ e → n≤1+n (m e))

  -- I cannot prove this one...
  -- This is because I cannot see any real tick inside C.
  -- since (λ x -> n✓ (n x) ...) depends on [x], and that x
  -- depende where in the context the holes are I cannot remove it.
  propn✓ : (C : Ctx)
         -> ((n : Term) -> C (✓ n) ⟪= C n)
         -> (n : Term) (P : Term)
         -> C (λ x -> n✓ ( n  x ) P x)
           ⟪= C P
  propn✓ C hip n P e = {!!}

  -- Big Idea here. Since (∀ n ∈ Term, C [ ✓ n] ≾ C [ n ])
  -- I wanted to show that ∀ P ∈ Term, C [ P ] ≾ P Zero
  -- but since Zero ≾ P ⇒ C Zero ≾ C [P] ⇒ C Zero ≾≿ C P
  -- and then C is contstant.

  prop : (C : Ctx) ->
       ((n : Term) -> C (✓ n ) ⟪= C n)
       -> (P : Term) -> C P ⟪= C Zero
  prop C hip P = ⟪=-trans {C P}
                          {C (everyTerm P)}
                          {C Zero}
                          (ctxMon C P (everyTerm P) (everyTermPless P))
                 (⟪=-trans {C (everyTerm P)}
                           {C (λ x -> n✓ (Zero x) Zero x)}
                           {C Zero}
                           (propn✓ C hip P Zero)
                           ⟪=-refl)


  needed : (C : Ctx)(t : Term)
         -> ✓ (C t) ⟪= t
         -> cnstCtx C
  needed C t hip = {!!}

  dec : (C : Ctx)
      -> redCtx C ⊎ cnstCtx C
  dec C = {!!}
