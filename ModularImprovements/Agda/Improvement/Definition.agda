open import FirstOrderTerms
open import Semantics

module Improvement.Definition (S : Signature)(E V : Set)(h : Semantics S E V) where

open import Library
open import Data.Nat
open import Relation.Binary
open import Data.Nat.Properties -- (ℕ, ≤) is a PoSet

open Semantics.Semantics h

{- A term is an improvement over another when, at every environment,
they evaluate to the same value and it costs less than the other -}

_⊴_ :  Program S → Program S →  Set
_⊴_ t₁ t₂ = (e : E) →
                 foldProgram ev t₁ e ≅ foldProgram ev t₂ e
                      ×
                 foldProgram cost t₁ e ≤ foldProgram cost t₂ e

⊴-refl : Reflexive _⊴_
⊴-refl e = refl , ≤-refl

⊴-trans : Transitive _⊴_
⊴-trans x y e = (trans (fst (x e)) (fst (y e)))
              , ≤-trans (snd (x e)) (snd (y e))

-- antisymmetric with respect to which equality?
-- This is one option. It only compares the value (but not the cost).
⊴-antisym : Antisymmetric (λ x y → (e : E) → foldProgram ev x e ≅ foldProgram ev y e) _⊴_
⊴-antisym {x} {y} r s e = fst (r e)

{-
  A term is an improvement over another when it costs less than the other
-}

_≾_ : Program S -> Program S -> Set
p ≾ q = ∀ (e : E) -> foldProgram cost p e ≤ foldProgram cost q e

_≿_ : Program S -> Program S -> Set
p ≿ q = q ≾ p

≾-refl : Reflexive _≾_
≾-refl e = ≤-refl

≾-trans : Transitive _≾_
≾-trans x y e = ≤-trans (x e) (y e)

--------------------------------------------------------------------------------
-- Cost equivalence
--------------------------------------------------------------------------------

_≾≿_ : Program S -> Program S -> Set
p ≾≿ q = p ≾ q × q ≾ p

≾≿-refl : Reflexive _≾≿_
≾≿-refl {x} = (≾-refl {x}) , ≾-refl {x}

≾≿-trans : Transitive _≾≿_
≾≿-trans {i} {j} {k} (l , p) ( r , q) =
         (≾-trans {i }{j}{k} l r) , (≾-trans {k }{j}{i} q p )

≾≿-sym : Symmetric _≾≿_
≾≿-sym (ilj , jli) = jli , ilj

isEquivalence≾≿ : IsEquivalence _≾≿_
isEquivalence≾≿ = record {
                  refl  = λ {x} → ≾≿-refl {x}
                  ; sym   = λ {i} {j} → ≾≿-sym {i} {j}
                  ; trans = λ {i} {j} {k} → ≾≿-trans {i} {j} {k}
                }

--
--------------------------------------------------
-- Improvement Equivalence

_⊴⊵_ :  Program S → Program S →  Set
p ⊴⊵ q  = p ⊴ q × q ⊴ p

⊴⊵-refl : Reflexive _⊴⊵_
⊴⊵-refl {x} = ⊴-refl {x} , ⊴-refl {x}

⊴⊵-trans : Transitive _⊴⊵_
⊴⊵-trans {i} {j} {k} (r₁ , r₂) (s₁ , s₂) = (⊴-trans {i} {j} {k} r₁ s₁)
                                         , (⊴-trans {k} {j} {i} s₂ r₂)

⊴⊵-sym : Symmetric _⊴⊵_
⊴⊵-sym {i} {j} (r , s) = s , r

isEquivalence⊴⊵ : IsEquivalence _⊴⊵_
isEquivalence⊴⊵ = record {
                     refl  = λ {x} → ⊴⊵-refl {x}
                   ; sym   = λ {i} {j} → ⊴⊵-sym {i} {j}
                   ; trans = λ {i} {j} {k} → ⊴⊵-trans {i} {j} {k}
                  }

--------------------------------------------------
