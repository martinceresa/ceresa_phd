---
title     : "SKI : SKI combinatory semantics"
layout    : page
---

\begin{code}
module Examples.SKI where

open import Library
open import FirstOrderTerms
open SigHelper
\end{code}

We need to add another constructor...

\begin{code}
data SKI : Set where
  S : SKI
  K : SKI
  I : SKI
  App : SKI

data Two : Set where
  LT : Two
  RT : Two

SKISig : Signature
SKISig = mkSignature SKI SKImap
  where
    SKImap : (s : SKI) -> opType SKI s
    SKImap S = ⊤ ⇒ ⊤
    SKImap K = ⊤ ⇒ ⊤
    SKImap I = ⊤ ⇒ ⊤
    SKImap App = ⊤ ⇒ Two -- No entiendo bien esto.
\end{code}

The idea here is to see how all this theory works out computing improvement in
the SKI calculus.
