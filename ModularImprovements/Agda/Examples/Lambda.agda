module Examples.Lambda where

open import Library
open import FirstOrderTerms

----------------------------------------
-- Expressions
----------------------------------------

-- Easy to read signature functor of λCal.
-- ∀ (NameSet ∈ Set), λC = Fix (SimpLam NameSet)
data SimpLam (v : Set)(r : Set) : Set where
  SVar : v -> SimpLam v r
  SApp : r → r → SimpLam v r
  SAbs : (v → r) -> SimpLam v r

-- Define three symbols
data LamSym  : Set where
  Var : LamSym
  App : LamSym
  Abs : LamSym

open SigHelper

Two : Set
Two =  ⊤ ⊎ ⊤

destwo : {a : Set} -> (l r : a) -> Two -> a
destwo l r (inj₁ x) = l
destwo l r (inj₂ y) = r

⟨_,_⟩ : {a : Set} -> (l r : a) -> Two -> a
⟨_,_⟩ = destwo

first : {a : Set} -> (Two -> a) -> a
first f = f (inj₁ ⊤.tt)
second : {a : Set} -> (Two -> a) -> a
second f = f (inj₂ ⊤.tt)

-- ∀ (s ∈ Set), Fₛ X = s + X² + Xˢ
LamSig : Set -> Signature
LamSig v = mkSignature LamSym LamMap
          where
            LamMap : (s : LamSym) -> opType LamSym s
            -- Each variable (v ∈ Vars) is a constructor in
            -- our language.
            LamMap Var = v ⇒ ⊥
            -- App is simply X²
            LamMap App = ⊤ ⇒ Two
            -- Not so sure about this but I am representing
            -- abstraction as: Xⱽ
            LamMap Abs = ⊤ ⇒ v

LamSig₁ : Set -> Signature {lsuc lzero}
LamSig₁ v = sig
        (Lift LamSym)
        (λ{ (Lift.lift Var) → Lift v
          ; (Lift.lift App) → Lift ⊤
          ; (Lift.lift Abs) → Lift ⊤})
        λ{ (Lift.lift Var) → Lift ⊥
         ; (Lift.lift App) → Lift Two
         ; (Lift.lift Abs) → Lift v}

-- I guess somewhere in the code we are going to assume
-- that expressions are well-formed. So we can get a prototy running in no time.

-- Given a variable name set (s ∈ Set), we can regain |Fix (SimpLam s)| as
SLam : Set -> Set
SLam s = Term (LamSig s) ⊥

LTerm : Set -> Set₁
LTerm s = TermL {lsuc lzero} (LamSig₁ s) (Lift ⊥)

CLam : Set₁
CLam = ∀ {s : Set} -> TermL {lsuc lzero} (LamSig₁ s) (Lift ⊥)

----------------------------------------
-- Easier constructors
----------------------------------------
λvar : {s : Set} -> s -> SLam s
λvar v = op (Var , v , λ{ () })

λapp : {s : Set} -> SLam s -> SLam s -> SLam s
λapp f x = op (App , ⊤.tt , destwo f x)

λabs : {s : Set} -> (s -> SLam s) -> SLam s
λabs body = op (Abs , ⊤.tt , body)

----------------------------------------
-- Examples
----------------------------------------

-- lid : CLam
-- lid = op (Abs , (⊤.tt , (λ x → op (Var , (x , λ{ ()})))))

-- id
λid : {s : Set} -> SLam s
λid = λabs λ x → λvar x

-- Ω
λΩ : {s : Set} -> SLam s
λΩ = λapp (λabs (λ x → λapp (λvar x) (λvar x)))
          (λabs (λ x → λapp (λvar x) (λvar x)))

-- lΩ : CLam
-- lΩ = lapp
--         (op (Abs , (⊤.tt , (λ x →
--                             op (App , (⊤.tt
--                             , ⟨ op (Var , (x , λ{ ()}))
--                             , op (Var , (x , λ{ ()}))
--                               ⟩))))))
--         (op (Abs , (⊤.tt , (λ x →
--                               op (App , (⊤.tt
--                               , ⟨ op (Var , (x , λ{ ()}))
--                               , op (Var , (x , λ{ ()}))
--                               ⟩))))))
----------------------------------------
-- Substitution
----------------------------------------

subs : {s : Set} -> SLam (SLam s) -> SLam s
subs (var ())
subs (op (Var , pa , as)) = pa
subs (op (App , _ , as)) =
                     λapp f x
                       where
                         f = subs (first as)
                         x = subs (second as)
subs (op (Abs , _ , as)) = λabs (subs ∘ as ∘ λvar)


----------------------------------------
-- Evaluator
----------------------------------------

-- Since we cannot guarantee termination by construction
-- (λΩ for example), we are going to use |boundedGas|

-- So we are going to rewrite terms into * Λ

-- Note : Do I have to distinguish between wrong terms and well-form terms?

eval : (OMapSigL {!!} (∀ {s} -> LTerm s *)) -> ∀ {s} -> LTerm s *
eval (Lift.lift Var , v , _) = just (opl {!!})
eval (Lift.lift App , args) = {!!}
eval (Lift.lift Abs , args) = {!!}

absApp : {s : Set} -> (∀ (s' : Set) -> s' -> SLam s') -> SLam s -> SLam s
absApp {s} f t = subs (f (SLam s) t)

lamEval : {s : Set} -> (OMapSig (LamSig s) ( SLam s * )) -> SLam s *
lamEval (Var , p , as) = just (λvar p)
lamEval (App , ⊤.tt , args) =
        -- Lets compute f and see what happens
        case f of
          -- There was enough gas to compute it
          λ{ (inj₁ (var ())) -- closed terms remember?
          --
           ; (inj₁ (op (Var , v , snd₁)))
             → demay none
                     (just ∘ λapp (λvar v))
                     x
           ; (inj₁ (op (App , _ , as ))) →
             demay none
                   (just ∘ λapp (λapp (first as) (second as)))
                   x
           ; (inj₁ (op (Abs , _ , as ))) →
             demay none
                   -- {!!}
                   (just ∘ {!!} )
                   x
          -- No gas to compute f
           ; (inj₂ y) → none}
         where
           f = first args
           x = second args
lamEval (Abs , args) = {!!}
