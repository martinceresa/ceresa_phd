module Mendley where

open import Library
open import Categories
import Categories.Products

open import FirstOrderTerms
open Signature

-- Mendley Catamorphisms
foldMTerm : { S : Signature}
          → ∀{X Y : Set}
          → (X → Y)
          → (∀ (A : Set) -> (A -> Y)
          -> OMapSig S A → Y)
          → Term S X → Y
foldMTerm v o (var x) = v x
foldMTerm {Y = Y} v o (op (s , p , t)) = o Y (λ x -> x) ((s , p , λ x → foldMTerm v o (t x)))

-- Mendleys are the same as regular catas? Correct?
-- If they are, why are we doing this?

foldMProgram : ∀{S Y} -> (OMapSig S Y -> Y) -> Program S -> Y
foldMProgram {S} alg = foldMTerm ⊥-elim (λ A x x₁ → alg (HMapSig S x x₁))

bindMTerm : forall{S}{X Y : Set} → (X → Term S Y) → Term S X → Term S Y
bindMTerm {S} k = foldMTerm k λ A x x₁ → op (HMapSig S x x₁)

infix 30 _[_]ₘ

_[_]ₘ : ∀{S X} → Ctx S → Term S X → Term S X
C [ T ]ₘ = bindMTerm (λ ⊤ -> T) C
