Require Import Coq.Program.Basics . (* We get function composition from here *)
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Init.Specif.

Definition Algebra (f : Set -> Set)(a : Set) : Set := f a -> a.

Class Functor (F : Set -> Set) :=
  {
    fmap : forall {a b : Set}, (a -> b) -> F a -> F b;
    (* Functor laws *)
    fmap_ident : forall {a : Set}, id = @fmap a a id; (* FunctionalExtensionality *)
    fmap_comp : forall (a b c : Set) (g : b -> c) (f : a -> b),
        compose (fmap g) (fmap f) = fmap (compose g f)
  }.

(**
   Free monad definition.
   Existence of [Free F X] plus two operators
   + [inj_var] : introduces a variables, [X -> Free F X]
   + [inj_op]  : introduces an operator, [F (Free F X) -> Free F X]

   There is another operator which exposes the internal structure of [Free F X],
   called [outF] : [t : Free F X] -> { exists v, inj_var v = t} + {exists op, inj_op op = t}]
 *)
(* For every endofunctor exists its fixpoint Free. *)
Axiom Free : forall (F : Set -> Set)`{Functor F}(X : Set), Set.
(* There are two ways of building Free objects *)
Axiom inj_op : forall {F : Set -> Set}{X : Set}`{Functor F}, F (Free F X) -> Free F X.
Axiom inj_var : forall {F : Set -> Set}{X : Set}`{Functor F}, X -> Free F X.
Axiom outF : forall {F : Set -> Set}{X : Set}`{Functor F}(t : Free F X),
    {exists (v : X), t = inj_var v} + {exists (op : F (Free F X)), t = inj_op op}.

(* And we consume such objects using fold *)
Axiom foldC : forall {F : Set -> Set}{map : Functor F}{X A : Set}
                     (var : X -> A) (alg : Algebra F A)
                     (term : Free F X), A.

(* Fold Universal Law *)
(* If *)
Axiom fold_inj_var : forall {F : Set -> Set}`{Functor F}{X A : Set}
                            (var : X -> A) (alg : Algebra F A),
    compose (foldC var alg) inj_var = var.

Axiom fold_inj_op : forall {F : Set -> Set}`{Functor F}{X A : Set}
                            (var : X -> A) (alg : Algebra F A),
    compose (foldC var alg) inj_op = compose alg (fmap (foldC var alg)) .

(* Ext variations. *)
Theorem fold_inj_var_ext : forall {F : Set -> Set}`{Functor F}
                                  {X A : Set}(var : X -> A)(alg : Algebra F A)
                                  (v : X),
    foldC var alg (inj_var v) = var v.
Proof.
  intros.
  pose (@fold_inj_var F H X A var alg).
  eapply equal_f in e.
  unfold compose in e.
  apply e.
Qed.

Theorem fold_inj_op_ext : forall {F : Set -> Set}`{Functor F}
                                  {X A : Set}(var : X -> A)(alg : Algebra F A)
                                  (op : F (Free F X)),
    foldC var alg (inj_op op) = alg (fmap (foldC var alg) op).
Proof.
  intros.
  pose (@fold_inj_op F H X A var alg).
  eapply equal_f in e.
  unfold compose in e.
  apply e.
Qed.
(* Only if *)
Axiom foldUniv : forall {F : Set -> Set}`{Functor F}{X A : Set}
                        {f : X -> A}{h : Algebra F A}{g : Free F X -> A},
    (compose g inj_var = f) ->
    (compose g inj_op = compose h (fmap g )) -> g = foldC f h.

(* End *)

(** Induction Principle
    Section dedicated to the induction principle needed to perform
    inductive proofs.
 *)

(* Lemma FreeInd {F : Set -> Set}`{Functor F}{X : Set} : *)
(*   forall (P : Free F X -> Prop), (* Proposition to prove *) *)
(*     (forall (v : X), P (inj_var v)) -> (* Variables are P-provable *) *)
(* I've tried with dependen product but I do not know how to use it properly.
   Algebra F {x & P x}
 *)
(*     (forall (op : F (Free F X)), algP (map P op) -> P (inj_op op) )    -> (* Indutive Hypothesis as an Algebra. *) *)
(*     (forall (t : Free F X), P t). (* So every term is P-correct *) *)
(* Proof. *)
(*   intros P Hvar Hinj t. *)
(*   assert (Outt := outF t). *)
(*   destruct Outt as [ [v injt ] | [ op opt ] ]; (* Case analysis in t*) *)
(*     subst; *)
(*     [apply Hvar | apply Hinj]. *)
(* Qed. *)
(******************************)

Lemma compose_assoc : forall {A B C D}{h : A -> B}{g : B -> C}{f : C -> D},
    compose f (compose g h) = compose (compose f g) h.
Proof.
  intros.
  auto.
Qed.

Instance map_Free {F : Set -> Set}`{Functor F} : Functor (Free F) :=
  {
    fmap _ _ f := foldC (compose inj_var f) inj_op 
  }.
Proof.
  (* Identity Law *)
  +
    intros A.
    apply foldUniv.
    *
      auto.
    *
      rewrite <- fmap_ident.
      auto.
  +
    intros.
    apply foldUniv.
    * (* Vars *)
      rewrite <- compose_assoc.
      rewrite fold_inj_var.
      rewrite compose_assoc.
      rewrite fold_inj_var.
      rewrite compose_assoc.
      reflexivity.
    *
      rewrite <- compose_assoc.
      rewrite fold_inj_op.
      rewrite compose_assoc.
      rewrite fold_inj_op.
      rewrite <- compose_assoc.
      rewrite fmap_comp.
      reflexivity.
Qed.

Definition hole_Filling {F : Set -> Set}`{Functor F}{X Y: Set}:
  (Free F X) -> (X -> Free F Y) -> Free F Y :=
  fun C f => foldC f inj_op C.

Notation "C [ F ]" := (hole_Filling C F)
                      (at level 85, no associativity).
Notation "(| f , g |)" := (foldC f g).
