Require Import FoldAxioms.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Program.Basics . (* We get function composition from here *)

Inductive Empty : Set :=.
Definition Term (F : Set -> Set)`{Functor F}:= Free F Empty.

Class Preorder (D : Set) :=
  {
    bop : D -> D -> Prop;
    refl_bop : forall (a : D), bop a a;
    trans_bop : forall (a b c : D), bop a b -> bop b c -> bop a c
  }.

Definition fEmpty : forall {D : Set}, Empty -> D.
Proof.
  intros.
  destruct H.
Defined.

Theorem Exten {F : Set -> Set}`{Functor F}{D : Set}
        `{Preorder D}{dalg : Algebra F D} :
  forall (C : Free F unit) (*Given a context C with just one kind of hole*)
         (f g : Free F Empty) (* And two terms *),
    (* injective algebra? *)
    (*******)
    bop ( (| fEmpty , dalg |) f) ( (| fEmpty , dalg|) g) (*such that f <= g*)
    -> bop ( (| fEmpty , dalg |) (C[fun _ => f])) (* C[f] <= C[g]*)
           ( (| fEmpty , dalg |) (C[fun _ => g])).
Proof.
  intros C f g Hfg.
  (* induction C using FreeInd. *)
  assert (Hf := outF C).
  destruct Hf as [ [v CEq] | [op CEq] ] ; subst.
  + (* C = inj_var. *)
    unfold hole_Filling.
    repeat rewrite fold_inj_var_ext.
    apply Hfg.
  +
    unfold hole_Filling.
    repeat
      rewrite fold_inj_op_ext.
    (* Do I need induction? *)