Require Import PolFunctors.
Require Import Functor.

Definition Algebra (C : Code) (X : Set) := [[C]] X -> X.
Definition Mu' (C : Code) := forall A, Algebra C A -> A.
Definition Cata {C : Code}{A : Set} : Algebra C A -> Mu' C -> A :=
  fun alg fa => fa A alg.

Definition inC {C : Code} : [[C]] (Mu' C) -> Mu' C :=
  fun term A alg => alg (fmap (Cata alg) term).

Definition outC {C : Code} : Mu' C -> [[C]] (Mu' C).
Proof.
  intros t.
  apply (@Cata C ([[C]] (Mu' C)) (fmap inC) t).
Defined.

(* Definition outF  ???? *)
(* Definition Term (C : Code)(X : Set) := forall A, (X -> A) -> Algebra C A -> A. *)

(* Definition Fold {C : Code}{X : Set}{A : Set} : *)
(*   (X -> A) -> Algebra C A -> Term C X -> A := *)
(*   fun var alg term => term A var alg. *)

(* Definition inOp {C : Code}{X : Set} : [[C]] (Term C X) -> Term C X := *)
(*   fun term A var alg => alg (fmap (Fold var alg) term). *)
(* Definition inVar {C : Code}{X : Set} : X -> Term C X := *)
(*   fun x A var alg => var x. *)

(* Definition out_alg (C : Code)(X : Set) : Algebra C ( [[C]] (Term C X)):= *)
(*   fmap inOp. *)

(* Definition out_Alg (C : Code)(X : Set) : Algebra C (X + [[C]] (Term C X)). *)
(* Proof. *)
(*   unfold Algebra. *)
(*   intros t. *)
(*   apply (fmap (fun t => match t with *)
(*                         | inl x => inVar x *)
(*                         | inr op => inOp op *)
(*                         end )) in t. *)
(*   apply inr. *)
(*   apply t. *)
(* Defined. *)

(* (* No se porque no anda esto... *) *)
(* Definition OutF {C : Code}{X : Set} : Term C X -> X + [[C]] (Term C X) := *)
(* (*   Fold inl (out_Alg C X). *) *)

(* (* Propiedad universal *) *)
(* (* g = (| h, f |) <=> g . inVar = f /\ g . inOp = h . F g *) *)
(* (* => *) *)
Lemma foldVar :
  forall {C : Code}{X : Set}{A : Set}
         (varOp : X -> A)(termOp : Algebra C A)(x : X),
         Fold varOp termOp (inVar x) = varOp x.
Proof. reflexivity. Qed.

Lemma foldOp :
  forall {C : Code}{X : Set}{A : Set}
         (varOp : X -> A)(termOp : Algebra C A)(x : [[C]] (Term C X)),
         Fold varOp termOp (inOp x) = termOp (fmap (Fold varOp termOp) x).
Proof. reflexivity. Qed.
(* <= *)

Lemma UP : forall (C : Code)(X : Set)(A : Set)
                  (alg : Algebra C A)(var : X -> A)
                  (h : Term C X -> A),
    (forall x, alg (fmap h x) = h (inOp x)) ->
    (forall v, h (inVar v) = var v) ->
    forall t, h t = Fold var alg t.
Proof.
(*   intros C X A alg var h. *)
(*   intros D1 D2 t. *)

(* (**) *)

(* (* Check PolFun. *) *)

(* (* (* WTF! Esto no es lo qeu quiero claramente...*) *) *)
(* (* Inductive Term (X : Set) : Set := *) *)
(* (*   | Var : X -> Term X *) *)
(* (*   | Op :  PolFun (Term X ) -> Term X.  *) *)
(* (* Inductive Term (X : Set) (F : forall Y, PolFun Y) : Set := *) *)
(* (*   | Var : X -> Term X F *) *)
(* (*   | Op : F (Term X F ) -> Term X F. *) *)

(* (* Inductive Term  (F : PolFun) (X : Set) : Set := *) *)
(* (*   | Var : X -> Term F X *) *)
(* (*   | Op : F (Term F X) -> Term F X. *) *)

(* (* Inductive Term  (C : Code) (X : Set) : Set := *) *)
(* (*   | Var : X -> Term C X *) *)
(* (*   | Op : Intero C (Term C X) -> Term C X. *) *)

(* Fixpoint fold  *)