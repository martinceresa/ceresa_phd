Require Import Coq.Program.Basics .

Class Functor (F : Type -> Type) :=
  {
    fmap : forall {a b : Type}, (a -> b) -> F a -> F b;
    (* Functor laws *)
    fmap_ident : forall a (x : F a), fmap (@id a) x = x;
    fmap_comp : forall (a b c : Type)
                       (g : b -> c)(f : a -> b) (x : F a),
        fmap (compose g f) x = fmap (compose g f) x
  }.
