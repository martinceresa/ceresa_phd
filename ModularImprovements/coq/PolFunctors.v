Require Import Coq.Program.Basics .
Require Import Functor.

Inductive Code : Set :=
  | R0 : Code
  | R1 : Code
  | Sum : Code -> Code -> Code
  | Prod : Code -> Code -> Code
  | Rec : Code.

Check Empty_set.

Fixpoint Interp (t : Code) : Set -> Set :=
  fun X =>
  match t with
  | R0 => Empty_set
  | R1 => unit
  | Sum t1 t2 => sum (Interp t1 X) (Interp t2 X)
  | Prod t1 t2 => prod (Interp t1 X) (Interp t2 X)
  | Rec => X
  end.
Notation "[[ T ]]" := (Interp T).

Inductive PolFun (X : Set) : Set :=
  | F0 : PolFun X
  | F1 : unit -> PolFun X
  | FSum : PolFun X -> PolFun X -> PolFun X
  | FProd : PolFun X -> PolFun X -> PolFun X
  | FRec : PolFun X.

Check PolFun.
Fixpoint Intero (C : Code) (X : Set) : (PolFun X) :=
  match C with
  | R0 => F0 X
  | R1 => F1 X tt
  | Sum x x0 => FSum X (Intero x X) (Intero x0 X)
  | Prod x x0 => FProd X (Intero x X) (Intero x0 X)
  | Rec => FRec X
  end.
    
Check Intero.
 

(* Instance codeFunctor (C : Code) : Functor [[ C ]] := *)
(*   { *)
(*   }. *)
(* Proof. *)
(*   + (* Map definition *) *)
(*     intros X Y f t. *)
(*     induction C. *)
(*     {destruct t. } (* R0 *) *)
(*     {apply tt. } (* R1 *) *)
(*     {destruct t. (* Case analysis on t, Left or Right... *) *)
(*       + (* inl i *) *)
(*         apply IHC1 in i. *)
(*         apply (inl i). *)
(*       + *)
(*         apply IHC2 in i. *)
(*         apply (inr i). } *)
(*     {destruct t. *)
(*      apply IHC1 in i. *)
(*      apply IHC2 in i0. *)
(*      apply (pair i i0). *)
(*     } (* Prod *) *)
(*     { apply f; apply t. } (* Rec *) *)
(*   + (* Id law *) *)
(*     induction C; intros. *)
(*     * simpl in * ; destruct x. *)
(*     * *)
(*       simpl in *. *)
(*       destruct x. *)
(*       reflexivity. *)
(*     * *)
(*       simpl in *. *)
(*       destruct x. *)
(*       - (* inl *) *)
(*         rewrite IHC1. *)
(*         reflexivity. *)
(*       - rewrite IHC2; reflexivity. *)
(*     * *)
(*       destruct x. *)
(*       simpl. *)
(*       rewrite IHC1. *)
(*       rewrite IHC2. *)
(*       reflexivity. *)
(*     * *)
(*       simpl in *. *)
(*       unfold id. *)
(*       reflexivity. *)
(*   + (* Composition Law *) *)
(*     intros. *)
(*     induction C; intros; *)
(*       simpl in *; reflexivity. *)
(* Qed. *)
    
