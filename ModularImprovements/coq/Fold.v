Require Import Coq.Program.Basics .
(*
*** Local Variables: ***
*** coq-prog-args: ("-impredicative-set") ***
*** End: ***
*)
(* Following Benjamin Delaware in "Meta-Theory a la Carte"*)

(* ALGEBRAS AND FOLDS *)

(* Normal Algebras *)
Definition Algebras (f : Set -> Set)(a : Set) : Set := f a -> a.
(* Mixin Algebras *)
Definition Mixin (X : Set)(F : Set -> Set)(A : Set) : Set := (X -> A) -> F X -> A.
(* Mendley Algebras *)
(* We need impredicative set!!! *)
Definition MAlgebras (f : Set -> Set) (a : Set) : Set := forall (r : Set), Mixin r f a.

(* Church Encoding for Fix! *)
Definition Fix (f : Set -> Set) : Set := forall (a : Set), MAlgebras f a -> a.
Definition fold {f : Set -> Set}{a : Set} : MAlgebras f a -> Fix f -> a :=
  fun alg fa => fa _ alg.

(* Can I introduce the idea of fuel to this function? *)
(* In this case, we need to say something like that I need a tick to inyect f(fix f) into fix f. *)
(* Definition inF (f : Set -> Set ) : f (Fix f) -> Fix f := *)
(*   fun fterm _ alg => alg (Fix f) (fold alg) fterm. *)

Definition Free (f : Set -> Set) (X : Set) := forall (a : Set) , (X -> a) -> MAlgebras f a -> a.
Definition foldF {f : Set -> Set} {X a : Set} : (X -> a) -> MAlgebras f a  -> Free f X -> a :=
  fun var alg term => term _ var alg.

(* See ExampleLang.v *)

(* Universal Properties of Fold. Hutton Ideas? *)


(* Functors! *)
Class Functor (F : Set -> Set) :=
  {
    fmap : forall {a b : Set}, (a -> b) -> F a -> F b;
    (* Functor laws *)
    fmap_ident : forall a (x : F a), fmap id x = x; (* FunctionalExtensionality *)
    fmap_comp : forall (a b c : Set) (g : b -> c) (f : a -> b) (x : F a),
        fmap (compose g f) x = fmap (compose g f) x
  }.

(* Good old definitions... *)
Definition fold' {f : Set -> Set} {funct : Functor f} {a : Set} : Algebras f a -> Fix f -> a :=
  fun alg => fold (fun _ rec => compose alg (fmap rec)).

Definition foldF' {f : Set -> Set}{funct : Functor f}{X a : Set} :
  (X -> a) -> Algebras f a -> Free f X -> a :=
  fun var alg => foldF var (fun _ rec => compose alg (fmap rec)).