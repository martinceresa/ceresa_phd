{-# Language GADTs #-}
{-# Language EmptyCase #-}

module Algebra where

import Control.Monad

-- Empty Type
data Empty

-- Impossible arrow?
empty :: Empty -> a
empty e = case e of

instance Show Empty where
  show = const "undefined"

-- Fixpoint def.
newtype Fix f = Fix {unFix :: f (Fix f)}

type Alg f a = f a -> a
type Coalg f a = a -> f a

cata :: Functor f => Alg f a -> Fix f -> a
cata alg = alg . fmap (cata alg) . unFix

ana :: Functor f => (a -> f a) -> a -> Fix f
ana coalg = Fix . fmap (ana coalg) . coalg

-- Free Algebra definition.

data FreeAlg f a where
    Op :: f (FreeAlg f a) -> FreeAlg f a
    Ret :: a -> FreeAlg f a

instance Functor f => Functor (FreeAlg f) where
  fmap f (Ret a) = Ret $ f a
  fmap f (Op t) = Op $ fmap (fmap f) t

fold :: Functor f => Alg f a -> (x -> a) -> FreeAlg f x -> a
fold _ mu (Ret x) = mu x
fold alg mu (Op t) = alg ( fmap (fold alg mu) t)

join :: Functor f => FreeAlg f (FreeAlg f a) -> FreeAlg f a
join = fold Op id

instance Functor f => Applicative (FreeAlg f) where
  pure = Ret
  (<*>) = ap

instance Functor f => Monad (FreeAlg f) where
  return = Ret
  t >>= h = fold Op h t

getTerm :: FreeAlg f Empty -> f (FreeAlg f Empty)
getTerm (Ret e) = empty e
getTerm (Op t) = t

toX :: Functor f => FreeAlg f Empty -> FreeAlg f x
toX = fold Op empty

toOp :: Functor f => f x -> FreeAlg f x
toOp = Op . fmap Ret

-- Context Management
-- There is no need to do anything here. FreeAlg f x is a context with
-- as many holes as elements in x, and hole-filling is just fold.

-- Since we want to run elements we can only fill holes with closed terms.
