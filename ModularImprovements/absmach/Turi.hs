{-# Language RankNTypes #-}

module Turi where

import Algebra

import Control.Arrow hiding (app)
import qualified Control.Monad as M


-- Classic Turi

-- Structural Recursion
unique :: Functor f
       => (f (FreeAlg f x , y) -> y)
       -> (x -> y)
       -> FreeAlg f x -> y
unique h f x = snd $ fold ((Op . fmap fst ) &&& h )(Ret &&& f) x

fromMach :: Functor f
         => (forall y. f y -> b (FreeAlg f y))
         -> f (x , b x) -> b (FreeAlg f x)
fromMach e = e . fmap fst

fromMach' :: (Functor f, Functor m)
         => (forall y. f y -> m (b (FreeAlg f y)))
         -> f (x , m (b x)) -> m (b (FreeAlg f x))
fromMach' e = e . fmap fst

evs :: (Functor f , Functor b)
    => (forall y. f y -> b (FreeAlg f y))
    -> (x -> b x)
    -> FreeAlg f x -> b (FreeAlg f x)
evs h f = unique h' f'
   where
     h' = fmap join . fromMach h
     f' = fmap Ret . f

evs' :: (Functor f , Functor b, Functor m)
    => (forall y. f y -> m (b (FreeAlg f y)))
    -> (x -> m (b x))
    -> FreeAlg f x -> m (b (FreeAlg f x))
evs' h f = unique h' f'
   where
     h' = fmap (fmap join) . fromMach' h
     f' = fmap (fmap Ret) . f

rmk :: (Monad m) => Either v (m x) -> m (Either v x)
rmk (Left v) = return $ Left v
rmk (Right eff) = Right <$> eff

type D v = (Int , v)
type B v = Either v

algD:: Alg (B v) (D v)
algD (Left v) = (1 , v)
algD (Right (n ,v)) = (1 + n , v)

tZ :: (Functor f, Monad m)
   => (forall y. f y -> m (B v (FreeAlg f y)))
   -> FreeAlg f Empty -> m (B v (FreeAlg f Empty))
tZ f = evs' f empty

beta :: (Functor f, Monad m)
     => (B v (D v) -> D v)
     -> (forall x. B v (m x) -> m (B v x))
     -> (forall y. f y -> m (B v (FreeAlg f y)))
     -> FreeAlg f Empty -> m (D v)
beta alpha dist e = fmap alpha . M.join . fmap (dist . fmap (beta alpha dist e)) . tZ e

--unfold para coalgebras monádicas.
unfoldM :: (Functor b, Monad m)
        => (forall y. b (m y) -> m (b y))
        -> (x -> m (b x))
        -> x -> m (Fix b)
unfoldM dist k x = k x >>= fmap Fix . dist . fmap (unfoldM dist k)

--fusion con un fold
unfoldMfold :: (Functor b, Monad m)
        => (forall y. b (m y) -> m (b y))
        -> (x -> m (b x))
        -> Alg b d
        -> x -> m d
unfoldMfold dist k h x = k x >>= fmap h . dist . fmap (unfoldMfold dist k h)

run :: (Functor f, Monad m)
     => (forall y. f y -> m (B v (FreeAlg f y)))
     -> FreeAlg f Empty -> m (D v)
run = beta algD rmk

newtype K v x = K {runK :: (Int , v)}


-- ordF :: (Functor f, Eq v, Monad m)
--      => (forall y. f y -> m (B v (FreeAlg f y))) -- alpha
--      -> (forall a. m a -> a) -- run
--      -> FreeAlg f Empty -> FreeAlg f Empty
--      -> Ordering
-- ordF alp unbox t1 t2 =
--   let (s1, v1) = unbox $ run alp t1
--       (s2, v2) = unbox $ run alp t2
--   in case (ord _ , v1 == v2) 
