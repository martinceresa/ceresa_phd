module Lang where

import Algebra

type Name = String


data L x = Push Int x
         | Add x
         | LTrue x | LFalse x | Eq x
         | Fetch Name x | Store Name x
         | Fin
         deriving Show

instance Functor L where
  fmap _ Fin = Fin
  fmap f (Push i x) = Push i $ f x
  fmap f (Add x) = Add $ f x
  fmap f (LTrue x) = LTrue $ f x
  fmap f (LFalse x) = LFalse $ f x
  fmap f (Eq x) = Eq $ f x
  fmap f (Fetch nm x) = Fetch nm $ f x
  fmap f (Store nm x) = Store nm $ f x

type T = FreeAlg L
-- Helpers

push :: Int -> T x -> T x
push i t = Op $ Push i t

add :: T x -> T x
add = Op . Add

lTrue :: T x -> T x
lTrue = Op . LTrue

lfalse :: T x -> T x
lfalse = Op . LFalse

eq :: T x -> T x
eq = Op . Eq

fetch :: Name -> T x -> T x
fetch nm l = Op $ Fetch nm l

store :: Name -> T x -> T x
store nm l = Op $ Store nm l

fin :: T x
fin = Op Fin
