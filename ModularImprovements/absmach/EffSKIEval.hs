{-# Language LambdaCase #-}
{-# Language UndecidableInstances #-}
{-# Language RankNTypes #-}
module EffSKIEval where

import Algebra
import SKI
import Empty

import Control.Monad.State.Lazy

import Data.Functor.Identity

-- Instrumented Evaluator

newtype DM m = DM {runDM :: [DM m] -> m SKI}

instance Show (m SKI) => Show (DM m) where
  show (DM t) = show $ t []

type Tick m a = m a -> m a

evDM :: DM m -> m SKI
evDM = flip runDM []

appM :: DM m -> DM m -> DM m
appM (DM f) x = DM $ \xs -> f (x : xs)

-- Application inside m
comb :: Applicative m => m SKI -> m SKI -> m SKI
comb f x = app <$> f <*> x

evalM :: Applicative m
      => (forall a. Tick m a) -- Some eff -- This is the tick context ?? <--
      -> Alg Sig (DM m)
evalM eff I = DM $ \case
                  [] -> pure i
  -- | No cost added.
                  (DM x:xs) -> eff (x xs)
  -- | (I x) xs ~> x xs.
evalM eff K = DM $ \case
                  [] -> pure k
                  [x] -> comb (pure k) (evDM x)
  -- | ^ Note: This is no weak head reduction!
                  (DM x : _ : as) -> eff (x as)
  -- | (K x _) as ~> x as
evalM eff S = DM $ \case
                  [] -> pure s
                  [f] -> comb (pure s) (evDM f)
                  [f , g] -> comb (comb (pure s) (evDM f)) (evDM g)
                  (f : g : x : as) -> eff $ runDM f (x : appM g x : as)
evalM _ (App f x) = appM f x

runA :: Applicative m => (forall a. m a -> m a) -> SKI -> m SKI
runA eff t = flip runDM [] $ fold (evalM eff) empty t

pass :: Monad m => m b -> m a -> m a
pass eff e = eff >> e

runEnv :: Applicative m => SKI -> [SKI] -> (forall a. m a -> m a) -> m SKI
runEnv t env step = runDM (fold (evalM step) empty t)
                        (map (fold (evalM step) empty) env)

-- Acá depende de m
-- tickContext :: m SKI -> m SKI
-- tickContext = _

-- Directly Nat

newtype C a b = C {runC :: a }
  deriving Show

instance Functor (C a) where
  fmap _ (C a) = C a

instance Monoid a => Applicative (C a) where
  pure _ = C mempty
  (C x) <*> (C y) = C $ mappend x y

instance Monoid Int where
  mempty = 0
  mappend = (+)

step :: C Int a -> C Int b
step (C i) = C $ i + 1

runMonoidDC :: SKI -> [DM (C Int)] -> Int
runMonoidDC t dm = runC $ runDM (fold (evalM step) empty t) dm

runMonoid :: SKI -> Int
runMonoid t = runC $ runDM (fold (evalM step) empty t) []


-- Simple Evaluation

runEval :: SKI -> SKI
runEval = runIdentity . runA id

-- Timed Evaluation

-- DC is our spiritual type guide
-- newtype DC = DC {unDC :: [DC] -> (SKI , Int)}
tick :: State Int ()
tick = get >>= put . (+1)

runTimed :: SKI -> (SKI , Int)
runTimed = flip runState 0 . runA (pass tick) --  runEnv t [] (pass tick)

-- Gas Dep Evaluation

ifo :: Int -> StateT Int Maybe ()
ifo n = if n >=1 then put (n - 1) else lift Nothing

quanto :: StateT Int Maybe ()
quanto = get >>= ifo

runWithGas :: SKI -> Int -> Maybe (SKI , Int)
runWithGas = runStateT . runA (pass quanto) -- runEnv t [] (pass quanto)
