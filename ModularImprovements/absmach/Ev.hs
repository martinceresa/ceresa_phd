module Ev where

import qualified Data.Map as M
import Control.Monad.State hiding (join)

import Control.Monad.Except

import Algebra
import Lang
import Turi (B, D, run)

data Err = BadAdd -- Add instr needs two Nat in the stack
         | BadEq  -- Eq  instr needs two Nat in the stack
         | BadStore -- Store instr needs a Nat in the stack
         | BadFin -- Fin needs at least one value in the stack
         deriving Show

type Heap = M.Map Name Int
type Value = Either Int Bool
type Stack = [Value]

type M = State (Stack , Heap)
type ME = ExceptT Err M

injval :: Monad m => a -> m (Either a b)
injval = return . Left

injvar :: Monad m => b -> m (Either a b)
injvar = return . Right

injtx :: (Functor f, Monad m) => b -> m (Either a (FreeAlg f b))
injtx = injvar . Ret

alpha :: L x -> ME (B Value (FreeAlg L x))
alpha Fin = get >>= \(st, _ ) -> case st of
                                   (v:_) -> injval v
                                   _ -> throwError BadFin
alpha (Push i r) = get >>= \(st , m) ->
                  put (Left i : st, m) >> injtx r
alpha (Add x) = get >>= \(st , m) -> case st of
                                      (Left p : Left q : rs) ->
                                        put (Left (p + q) : rs, m) >> injtx x
                                      _ -> throwError BadAdd
                                        -- fail "Bad Stack: Add needs two Nats."
alpha (Eq x) = get >>= \(st , m) -> case st of
                                      (Left p : Left q : rs) ->
                                        put (Right (p == q) : rs, m) >> injtx x
                                      _ -> throwError BadEq
                                        -- fail "Bad Stack: Eq needs two Nats."
alpha (LTrue x) = get >>= \(st, m) -> put (Right True : st, m) >> injtx x
alpha (LFalse x) = get >>= \(st, m) -> put (Right False : st, m) >> injtx x
alpha (Fetch nm x) = get >>= \(st, m) -> put (Left (m M.! nm) : st , m)  >> injtx x
alpha (Store nm x) = get >>= \(st, m) -> case st of
                                           (Left i : xs) -> put (xs, M.insert nm i m) >> injtx x
                                           _ -> throwError BadStore
                                             -- fail "Bad Stack: Store needs (expects) a Nat."

runT :: FreeAlg L Empty -> Either Err (D (Either Int Bool))
runT =  flip evalState ([], M.empty) . runExceptT .  run alpha

metaTest :: FreeAlg L x -> FreeAlg L x
metaTest = push 1 . push 2

test1 :: FreeAlg L Empty
test1 = metaTest (add fin)

testn :: FreeAlg L Empty
testn = metaTest $ metaTest $ metaTest test1

badTest :: FreeAlg L Empty
badTest = push 1 $ add fin

test2 :: FreeAlg L Empty
test2 = push 2 $ store "x" $ push 3 $ push 5 $ fetch "x" $ add fin
