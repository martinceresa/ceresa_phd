{-# Language TypeSynonymInstances #-}
{-# Language FlexibleInstances #-}
{-# Language RankNTypes #-}
{-# Language LambdaCase #-}
{-# Language TupleSections #-}

module Ev where

import Algebra
import SKI
import qualified Data.Map as M
-- import State

import Control.Monad.State hiding (join)

import Control.Arrow hiding (app)


-- B X = V + X
-- In general:

-- injvar :: b -> FreeAlg f (Either a b)
-- injvar = Ret . Right

-- injval :: a -> FreeAlg f (Either a b)
-- injval = Ret . Left

----------------------------------------
-- B X = SKI + X
type B x = Either SKI (x, Maybe x)

type M = State [SKI]

-- Given B X = V + X, forall M Monad, BM => MB.
lambda :: Monad m => Either v (m x) -> m (Either v x)
lambda = either (return . Left) (fmap Right)

injval :: SKI -> M (B a)
injval = return . Left

injvar :: b -> M (B b)
injvar = return . Right . (,Nothing)

injapp :: x -> x -> M (B x)
injapp f x = return (Right (f, Just x))

alpha :: Sig x -> M (B (T x))
alpha I = get >>= \case
  [] -> injval i
  (x:xs) -> put xs >> injvar (toX x)
alpha K = get >>= \case
  [] -> injval k
  (x:[]) -> injval (app k x)
  (x:y:xs) -> put xs >> injvar (toX x)
alpha S = get >>= \case
  [] -> injval s
  [f] -> injval $ app s f
  [f,g] -> injval $ app (app s f) g
  (f:g:y:ys) -> put (y : app g y : ys) >> injvar (toX f)
alpha (App f x) = injapp (Ret f) (Ret x)

-- eval :: SKI ->
opMonad :: (Functor b, Functor c)
        => (forall y. Sig y -> c (b (T y))) -> (x -> c (b x)) -> T x -> c (b (T x))
opMonad _ f (Ret x) = (fmap Ret) <$> f x
opMonad sos _ (Op t) = fmap (fmap join) (sos t)
