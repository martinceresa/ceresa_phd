{-# Language FlexibleInstances #-}
{-# Language MultiParamTypeClasses #-}
module Spec where

-- import Algebra
-- import SKI
-- import Eval
-- import QTest

-- import QuickSpec hiding (Sig)

-- instance Observe Int (Maybe Int) SKI where
--   observe c t =  snd <$> runWithGas t c

-- sig = [
--   con "IP" (IP :: SKI),
--   con "KP" (KP :: SKI),
--   con "SP" (SP :: SKI),
--   con "AppP" (AppP :: SKI -> SKI -> SKI),
--   predicate "safeTest" (safeTest :: SKI -> SKI -> FreeAlg Sig Hole -> Int -> Bool)
--   ]
