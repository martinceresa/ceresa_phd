module Class.HigherClasses where

import           Test.QuickCheck

-- HShow
class HShow f where
    hshow :: f String -> String

-- HEq
class EqF f where
  (=!=) :: Eq a => f a -> f a -> Bool

-- Arbitrary
class HArbitrary f where
    harbitrary :: Arbitrary a => Gen (f a)

