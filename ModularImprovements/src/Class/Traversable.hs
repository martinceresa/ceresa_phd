module Class.Traversable where

class Functor f => Traversable f where
  dist :: Applicative m => f (m a) -> m (f a)
