{-# LANGUAGE ExplicitForAll    #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE RankNTypes        #-}
module Improvement.InstrAlgebra where

import           Prelude             hiding (Traversable)

import           Class.Traversable

import           Control.Monad       (ap)
import           Control.Monad.State hiding (join)
import           Test.QuickCheck

import           Class.HigherClasses
import           Data.Const
import           Data.Empty
import           Improvement.Algebra hiding (fold, join)
import qualified Improvement.Algebra as Alg

-- Instrumented Semantics

data InstrAlg f a where
  Var  :: a -> InstrAlg f a
  Con  :: f (InstrAlg f a) -> InstrAlg f a
  -- | ^ Free Algebra Structure
  Tick :: InstrAlg f a -> InstrAlg f a
  -- | ^ Plus Tick

-- Show Instance

instance (HShow f, Functor f, Show a) => Show (InstrAlg f a) where
  show = fold (\t -> "(Tick " ++ t ++ ")") hshow show

instance (EqF f , Eq a) => Eq (InstrAlg f a) where
  (==) (Tick p) (Tick q) = p == q
  (==) (Var x) (Var y)   = x == y
  (==) (Con t) (Con s)   = t =!= s
  (==) _ _               = False

-- Orders?

ordEn :: (EqF f , Functor f, Ord a) => InstrAlg f a -> InstrAlg f a -> Bool
ordEn (Tick p) (Tick q) = ordEn p q
ordEn (Tick p) q        = ordEn p q
ordEn p q               = p == q

lessEq :: InstrAlg (K c) a -> InstrAlg (K c) b -> Bool
lessEq (Tick x) (Tick y) = lessEq x y
lessEq (Tick _) _        = False
lessEq _ _               = True

--

fold :: Functor f
     => (b -> b)
  -- | ^ Tick
     -> (f b -> b) -> (a -> b) -> InstrAlg f a
  -- | ^ Fold
     -> b
fold _ _ mu (Var x)      = mu x
fold tck alg mu (Con t)  = alg ( fmap (fold tck alg mu) t)
fold tck alg mu (Tick t) = tck (fold tck alg mu t)

join :: Functor f => InstrAlg f (InstrAlg f a) -> InstrAlg f a
join = fold Tick Con id

instance Functor f => Functor (InstrAlg f) where
  fmap f = fold Tick Con (Var . f)

instance Functor f => Applicative (InstrAlg f) where
    pure = Var
    (<*>) = ap

instance Functor f => Monad (InstrAlg f) where
    return = Var
    t >>= h = join $ fmap h t

-- Effects

-- Types
runEff :: (Functor f, Traversable f, Applicative m )
       => (m (FreeAlg f a) -> m (FreeAlg f a))
       -> (a -> m (FreeAlg f a))
       -> InstrAlg f a -> m (FreeAlg f a)
runEff t = fold t (fmap Op . dist)

-- Get Ticks
tick :: State Int ()
tick = get >>= put . (+1)

splitT :: (Functor f , Traversable f)
          => (a -> State Int (FreeAlg f a))
          -> InstrAlg f a -> State Int (FreeAlg f a)
splitT = runEff (tick >>)

splitTime :: (Functor f, Traversable f)
          => (a -> State Int (FreeAlg f a))
          -> InstrAlg f a -> ( FreeAlg f a , Int)
splitTime var = flip runState 0 . splitT var

runTimed :: (Functor f, Traversable f)
          => (a -> State Int (FreeAlg f a))
          -> InstrAlg f a -> Int
runTimed v = snd . splitTime v

-- Run with Gas

ifo :: Int -> StateT Int Maybe ()
ifo n = if n >=1 then put (n - 1) else lift Nothing

quanto :: StateT Int Maybe ()
quanto = get >>= ifo

runGas :: (Functor f, Traversable f)
       => (a -> StateT Int Maybe (FreeAlg f a))
       -> InstrAlg f a -> StateT Int Maybe (FreeAlg f a)
runGas = runEff (quanto >>)

runWithGas :: (Functor f , Traversable f)
           => (a -> StateT Int Maybe (FreeAlg f a))
           -> InstrAlg f a -> Int -> Maybe (FreeAlg f a, Int)
runWithGas var t = runStateT (runGas var t)

-- POCs

toInstr :: Functor f => FreeAlg f a -> InstrAlg f a
toInstr = Alg.fold Con Var

noTick :: Functor f => InstrAlg f a -> FreeAlg f a
noTick = fold id Op Ret

-- Cómo cuento los ticks?
count :: Functor f
      => InstrAlg f Empty
      -> (f Int -> Int) -> Int
count t intalg = fold (+1) intalg empty t

subst :: Functor f => (a -> InstrAlg f b) -> InstrAlg f a -> InstrAlg f b
subst = fold Tick Con

-- Tick Context
tickContext :: InstrAlg f a -> InstrAlg f a
tickContext = Tick

-- Arbitrary

instance {-# OVERLAPPING #-} ( HArbitrary f, Functor f)
         => Arbitrary (InstrAlg f Empty) where
  arbitrary = frequency [(2 , Con <$> harbitrary), (1 , Tick <$> arbitrary)]

instance {-# OVERLAPPABLE#-} (HArbitrary f, Functor f, Arbitrary a) => Arbitrary (InstrAlg f a) where
  arbitrary = frequency [ (2, Con <$> harbitrary)
                        , (1, Var <$> arbitrary)
                        , (1, Tick <$> arbitrary)] -- Esto es medio al pedo...
