module Improvement.CostMonad where

newtype M x = M (Int , x)

instance Functor M where
  fmap f (M (c, x)) = M (c, f x)

instance Applicative M where
  pure v = M (0, v)
  (M (cf, f)) <*> (M (cx, x)) = M (cf + cx , f x)

instance Monad M where
  return = pure
  (M (ca, a)) >>= f = let (M (cb, b)) = f a in M (ca + cb , b)
