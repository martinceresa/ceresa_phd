{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE RankNTypes           #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Improvement.Improvement where

import           Control.Monad.State
import           Prelude                  hiding (Traversable)

import           Class.HigherClasses
import           Data.Empty
import           Data.Hole
import           Improvement.Algebra      (FreeAlg)
import           Improvement.InstrAlgebra

import           Class.Traversable

-- Obs eq.
testOEQ :: (Functor f)
        => FreeAlg f Empty -> FreeAlg f Empty
        -> FreeAlg f Hole
        -> Bool
testOEQ _ = error "Not implemented. Use the safe one"

-- Ord Maybe a. But since we do not have to compare the result values
-- we do not compare the Just Values
checkMB :: Maybe a -> Maybe b -> Bool
checkMB Nothing _         = True
checkMB (Just _) (Just _) = True
checkMB _ _               = False


checkImprov :: Ord a => Maybe a -> Maybe a -> Bool
checkImprov Nothing _         = True
checkImprov (Just _) Nothing  = False
checkImprov (Just m) (Just n) = m >= n

-- We take our definition of Improvement to be
-- t1,t2 : InstrAlg f Empty [Closed terms]
-- ∀ C : InstrAlg f Hole [Context with just one hole]
-- ∀ n : Nat
-- C[t1]⇓ⁿ ⇒ C[t2]⇓ⁿ

-- Where C[t]⇓ⁿ = ∃ m ∈ Nat. runwithGas alg empty (C[t]) n = Just m

testImprov :: (Functor f, HArbitrary f, Traversable f)
            => InstrAlg f Empty -> InstrAlg f Empty
            -- | Two terms to compare
            -> InstrAlg f Hole
            -- | Arbitrary Context
            -> Int
            -- | Arbitrary Gas
            -> Bool
testImprov p q c j =
  let
    ct1 = subst (const p) c
    ct2 = subst (const q) c
    i = abs j -- i ≥ 0
  in
    checkMB
    (runWithGas empty ct1 i)
    (runWithGas empty ct2 i)


-- The difference here is that we compare how much gas was consumed.
-- Since the return (V , Int) inside Maybe is the gas unused
-- we take it less is more consumption.
-- Henceforth mt1 <= mt2 indicates that everytime
-- mt1 finishes with kt1 gas left, mt2 finishes with kt2 gas left
-- and kt1 <= kt2 which means that mt2 used less gas in its evaluation.
testStrictImprov :: (Functor f, HArbitrary f, Traversable f)
         => InstrAlg f Empty -> InstrAlg f Empty
         -- | Two terms to compare, t1 t2
         -> InstrAlg f Hole
         -- | Arbitrary Context C
         -> Int
         -- | Arbitrary Gas i
         -> Bool -- Checks that  cost (C[t1]) < cost (C[t2]) with gas i, if gas was enough
testStrictImprov p q c j =
  let i = abs j
      ct1 = subst (const p) c
      ct2 = subst (const q) c
  in
    (snd <$> runWithGas empty ct1 i)
    <=
    (snd <$> runWithGas empty ct2 i)

-- I must show something like this:
-- ∀ t ∈ FreeAlg Σ ∅. ∀ n ∈ Nat. ∀ runT :: Σ ? → ? | (| runT |) t = n
-- ⇒ ∀ m ∈ Nat, m >= n. runWithGas t m = Just (_ , m - n)

-- I think this must be enough to show that t1 <= t2 ⇒ t1 improves t2.
