module PlusSigma where

newtype PSigma f x = PS {unPS :: Either (f x) x}

instance Functor f => Functor (PSigma f) where
    fmap f (PS (Left t))  = PS $ Left $ fmap f t
    fmap f (PS (Right t)) = PS $ Right $ f t

newtype Fix f = Fix {unFix :: f (Fix f)}

-- instance Functor f => Functor (Fix f) where

cata :: Functor f => (f a -> a) -> Fix f -> a
cata alg = alg . fmap (cata alg) . unFix

ticks :: Functor f => (f a -> a) -> (a -> a) -> Fix (PSigma f) -> a
ticks cAlg tick = cata newAlg
  where
    newAlg (PS (Left t)) = cAlg t
    newAlg (PS (Right t)) = tick t

prodCata :: Functor f => (f a -> a) -> (f b -> b) -> f (a , b) -> (a , b)
prodCata alg1 alg2 t = (alg1 (fmap fst t), alg2 (fmap snd t))
