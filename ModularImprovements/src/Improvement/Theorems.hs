module Improvement.Theorems where

import           Prelude                  hiding (Traversable)

import           Improvement.Algebra
import           Improvement.InstrAlgebra

import           Data.Empty
import           Data.Maybe

import           Class.Traversable

-- Just notes about theorems that I think I should prove
--
-- Definition of runWithGas ?
-- ∀ t in InstrFree Σ empty, v in FreeAlg Σ Empty , n in ℕ such that
-- splitTime t = (v , n) implies ∀ m in ℕ,
--  m < n -> runWithGas t m = Nothing
--  m >= n -> runWithGas t m = Just (v , m - n)
--
--
-- Split is runWithGas
-- splitTime t = (v , n) <=> runWithGas t n = Just (v , 0)
--
-- runWithGas mono en n
-- t, v , m, n such that
-- runWithGas t m = Just (v , n)
-- => ∀ r > m, runWithGas t r = Just (v , n + (r - m))
--
-- So now we have:
-- t1 < t2 <=> runTimed t1 < runTimed t2
--  Our definition
--
-- t1 < t2 => ∀ C in Ctx, C[t1] < C[t2]
--  Thm substition is monotonic. This is an old result, right?
--
-- t1 < t2 => ∀ C in Ctx, n in ℕ, runWithGas C[t2] n < runWithGas C[t1]
--  expand Maybe (<) basically give us the implication we wanted.
--  Here I have some doubts... Mainly in the implication part, where do I show
--  that the programs behave the same way...
--
-- t1 < t2 => t1 improves t2.
-- Summing up all the previuos work.

gasisrun :: (Functor f, Traversable f) => InstrAlg f Empty -> Int -> Bool
gasisrun t j = let
             n = snd $ splitTime empty t
             rG = (runWithGas empty t i)
             i = abs j
             in
                if i < n
                then isNothing rG
                else (snd $ fromJust rG) == i - n
