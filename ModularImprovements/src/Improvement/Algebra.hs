{-# LANGUAGE EmptyCase         #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE RankNTypes        #-}

module Improvement.Algebra where

import           Control.Monad

import           Class.HigherClasses
import           Data.Empty

import           Test.QuickCheck

type Alg f a = f a -> a
type Coalg f a = a -> f a

-- Free Algebra definition.

data FreeAlg f a where
    Op :: f (FreeAlg f a) -> FreeAlg f a
    Ret :: a -> FreeAlg f a

instance Functor f => Functor (FreeAlg f) where
  fmap f (Ret a) = Ret $ f a
  fmap f (Op t)  = Op $ fmap (fmap f) t

fold :: Functor f => Alg f a -> (x -> a) -> FreeAlg f x -> a
fold _ mu (Ret x)  = mu x
fold alg mu (Op t) = alg ( fmap (fold alg mu) t)

join :: Functor f => FreeAlg f (FreeAlg f a) -> FreeAlg f a
join = fold Op id

subst :: Functor f => FreeAlg f a -> (a -> FreeAlg f b) -> FreeAlg f b
subst = flip (fold Op)

instance Functor f => Applicative (FreeAlg f) where
  pure = Ret
  (<*>) = ap

instance Functor f => Monad (FreeAlg f) where
  return = Ret
  t >>= h = fold Op h t

getTerm :: FreeAlg f Empty -> f (FreeAlg f Empty)
getTerm (Ret e) = empty e
getTerm (Op t)  = t

toX :: Functor f => FreeAlg f Empty -> FreeAlg f x
toX = fold Op empty

toOp :: Functor f => f x -> FreeAlg f x
toOp = Op . fmap Ret

-- Class Instances

instance {-# OVERLAPPING #-} (HArbitrary f) => Arbitrary (FreeAlg f Empty) where
  arbitrary = Op <$> harbitrary

instance {-# OVERLAPPABLE#-}  (Arbitrary a , HArbitrary f) => Arbitrary (FreeAlg f a) where
  arbitrary = frequency [ (2, Op <$> harbitrary)
                        , (1, Ret <$> arbitrary)]

----
instance (HShow f, Functor f, Show a) => Show (FreeAlg f a) where
  show (Ret x) = show x
  show (Op t)  = hshow $ show <$> t

instance (EqF f , Eq a) => Eq (FreeAlg f a) where
  (==) (Ret x) (Ret y) = x == y
  (==) (Op p) (Op q)   = p =!= q
  (==) _ _             = False
