module Data.Empty where

import Test.QuickCheck

data Empty

instance Eq Empty where
  (==) _ _ = True

instance Show Empty where
  show _ = "undefined"

instance Arbitrary Empty where
  arbitrary = pure tempty

empty :: Empty -> a
empty = undefined

tempty :: Empty
tempty = undefined
