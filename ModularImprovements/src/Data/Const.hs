module Data.Const where

import           Prelude             hiding (Traversable)

import           Class.HigherClasses
import           Class.Traversable

newtype K a c = K {unK :: a}

instance Functor (K a) where
  fmap _ (K a) = K a

instance Show a => HShow (K a) where
  hshow (K a) = "K " ++ show a

instance Traversable (K a) where
  dist (K a) = pure $ K a
