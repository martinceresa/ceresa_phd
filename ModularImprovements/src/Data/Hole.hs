{-# LANGUAGE PatternSynonyms #-}

module Data.Hole where

import           Improvement.Algebra
import           Test.QuickCheck

data Hole = Hole
          deriving (Show, Eq)

pattern HoleP :: FreeAlg f Hole
pattern HoleP <- Ret Hole
  where HoleP = Ret Hole

instance Arbitrary Hole where
  arbitrary = pure Hole
