{-# LANGUAGE FlexibleInstances #-}
module Naturals.Tests where

import           Naturals.Eval
import           Naturals.Plus

import           Test.QuickCheck

import           Data.Empty
import           Data.Hole
import           Improvement.Algebra
import           Improvement.InstrAlgebra as Instr

import           Class.HigherClasses
import           Improvement.Improvement


-- Left assoc
izq :: InstrAlg FPlus Empty
    -> InstrAlg FPlus Empty
    -> InstrAlg FPlus Empty
    -> InstrAlg Nat Empty
izq x y z = toNat $ add (add x y) z

-- Right assoc
der :: InstrAlg FPlus Empty
    -> InstrAlg FPlus Empty
    -> InstrAlg FPlus Empty
    -> InstrAlg Nat Empty
der x y z = toNat $ add x (add y z)

-- Left assoc is improved by right assoc
imProv :: InstrAlg FPlus Empty
       -> InstrAlg FPlus Empty
       -> InstrAlg FPlus Empty
       -> InstrAlg Nat Hole
       -> Int
       -> Bool
imProv x y z  = testImprov (izq x y z) (der x y z)

-- Right assoc is not improved by left assoc
notProv :: InstrAlg FPlus Empty
        -> InstrAlg FPlus Empty
        -> InstrAlg FPlus Empty
        -> InstrAlg Nat Hole
        -> Int
        -> Bool
notProv x y z = testImprov (der x y z) (izq x y z)

-- Write simpler checks with Holes
data Three = O | T | TH -- Three different holes
  deriving Show

instance CoArbitrary Three where
  coarbitrary O  = variant (0 :: Integer)
  coarbitrary T  = variant (1 :: Integer)
  coarbitrary TH = variant (2 :: Integer)

-- Here we need this instance to show counter examples found by QuickCheck
instance (HShow f, Functor f) => Show (Three -> InstrAlg f Empty) where
  show f = "{ O ~> " ++ show (f O) ++ "; T ~> " ++ show (f T) ++ "; TH ~> " ++ show (f TH) ++ "}"

-- same as izq
izqV :: InstrAlg FPlus Three
izqV = add
          (add
            (Var O)
            (Var T))
          (Var TH)

-- same as der
derV :: InstrAlg FPlus Three
derV = add (Var O)
           $ add (Var T)
                 (Var TH)

testIzqV :: (Three -> InstrAlg FPlus Empty)
         -> InstrAlg Nat Hole
         -> Int
         -> Bool
testIzqV f  = testImprov (toNat $ Instr.subst f izqV) (toNat $ Instr.subst f derV)

testDerV :: (Three -> InstrAlg FPlus Empty)
          -> InstrAlg Nat Hole
          -> Int
          -> Bool
testDerV f = testImprov (toNat $ Instr.subst f derV) (toNat $ Instr.subst f izqV)
