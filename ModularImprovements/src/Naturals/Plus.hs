{-# LANGUAGE FlexibleInstances #-}
module Naturals.Plus where

import           Prelude                  hiding (Traversable)

import           Class.HigherClasses
import           Improvement.InstrAlgebra (InstrAlg (..))

import           Class.Traversable

import           Test.QuickCheck

data FPlus x
  = Z
  -- | ^ Zero
  | S x
  -- | ^ Succ
  | Add x x
  -- | ^ Addition

data FBool x
  = TT
  | FF

data FIF x = IF x x x

-- Functor

instance Functor FPlus where
  fmap _ Z         = Z
  fmap f (S x)     = S $ f x
  fmap f (Add x y) = Add (f x) (f y)

instance Functor FBool where
  fmap _ TT = TT
  fmap _ FF = FF

instance Functor FIF where
  fmap f (IF c tt ff) = IF (f c) (f tt) (f ff)

-- Traversable

instance Traversable FBool where
  dist TT = pure TT
  dist FF = pure FF

instance Traversable FPlus where
  dist Z         = pure Z
  dist (S x)     = S <$> x
  dist (Add x y) = Add <$> x <*> y

instance Traversable FIF where
  dist (IF c tt ff) = IF <$> c <*> tt <*> ff

-- Show

instance HShow FBool where
  hshow TT = "TT"
  hshow FF = "FF"

instance HShow FPlus where
  hshow Z         = "Z"
  hshow (S x)     = "S " ++ x
  hshow (Add x y) = "(Add " ++ x ++ y ++ ")"

instance HShow FIF where
  hshow (IF c tt ff) = "("++ c ++ "?" ++ tt ++ ":" ++ ff ++")"

instance HArbitrary FPlus where
  harbitrary = oneof [ pure Z , S <$> arbitrary , Add <$> arbitrary <*> arbitrary]

add :: InstrAlg FPlus a
    -> InstrAlg FPlus a
    -> InstrAlg FPlus a
add x y = Con $ Add x y

zero :: InstrAlg FPlus a
zero = Con Z

suc :: InstrAlg FPlus a -> InstrAlg FPlus a
suc = Con . S


