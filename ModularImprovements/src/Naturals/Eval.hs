{-# LANGUAGE LambdaCase #-}
module Naturals.Eval where

import           Prelude                  hiding (Traversable)

import           Naturals.Plus

import           Class.HigherClasses
import           Class.Traversable

import           Data.Const
import           Data.Empty
import           Improvement.InstrAlgebra as Instr

import           Test.QuickCheck


-- Semantic Domain of FPlus
data Nat x = C | N x -- Basically natural numbers.

instance Functor Nat where
  fmap _ C     = C
  fmap f (N x) = N $ f x

instance Traversable Nat where
  dist C     = pure C
  dist (N x) = N <$> x

instance HShow Nat where
  hshow C     = "C"
  hshow (N x) = "N " ++ x

instance HArbitrary Nat where
  harbitrary = frequency [(2 , pure C), (1 , N <$> arbitrary)]

type R = InstrAlg (K Int) Empty -- Ticked Int

sucR :: R -> R
sucR = Instr.fold Tick alg empty
  where
    alg (K x) = Con $ K (x + 1)

addR :: R -> R -> R
addR x y = Instr.fold (Tick . Tick) alg empty x -- These tick should be doubled
  where
    alg (K x') = Instr.fold Tick algy empty y
      where
        algy (K y') = Con $ K $ x' + y'

toRAlg :: FPlus R -> R
toRAlg Z         = Con (K 0)
toRAlg (S x)     = Tick $ sucR x
toRAlg (Add x y) = addR x y

evalNat :: FPlus (InstrAlg Nat a) -> InstrAlg Nat a
evalNat Z         = Con C
evalNat (S x)     = Tick $ Con $ N x
evalNat (Add x y) = plus x y

plus :: InstrAlg Nat a
     -> InstrAlg Nat a
     -> InstrAlg Nat a
plus x y = Instr.fold Tick alg Var x
  where
    alg C     = y
    alg (N z) = Tick $ Con $ N z

toNat :: InstrAlg FPlus a -> InstrAlg Nat a
toNat = Instr.fold Tick evalNat Var

-- Back to Reality

evalTNat :: FPlus (Int, Int) -> (Int , Int)
evalTNat Z                      = (0 , 0)
evalTNat (S (n , v))            = (n + 1, v + 1)
evalTNat (Add (xc, xv) (yc,yv)) = (2 * xc + yc, xv + yv)

newtype Fix f = Fix {unFix :: f (Fix f)}

cata :: Functor f => (f a -> a) -> Fix f -> a
cata falg = falg . (fmap (cata falg)) . unFix

addTNat :: Nat (Fix Nat -> Fix Nat) -> (Fix Nat -> Fix Nat)
addTNat C     = id
addTNat (N n) = (Fix . N) . n

evalBool :: FBool (Bool, Int) -> (Bool, Int)
evalBool TT = (True, 1)
evalBool FF = (False, 1)

data Sum f g h x = InL (f x) | InM (g x) | InR (h x)

instance (Functor f, Functor g, Functor h) => Functor (Sum f g h) where
  fmap p (InL f) = InL $ fmap p f
  fmap p (InM g) = InM $ fmap p g
  fmap p (InR h) = InR $ fmap p h

threeAlg :: (f a -> a) -> (g a -> a) -> (h a -> a) -> Sum f g h a -> a
threeAlg falg _ _ (InL t) = falg t
threeAlg _ galg _ (InM t) = galg t
threeAlg _ _ halg (InR t) = halg t

type NatIf = Sum FPlus FIF FBool

type Lang = Fix NatIf

type SDom = Maybe (Either Int Bool)

evalNatDom :: FPlus SDom -> SDom
evalNatDom Z            = Just $Left 0
evalNatDom (S mbn) = mbn >>= \case
  Left n -> Just $ Left (n + 1)
  _ -> Nothing
evalNatDom (Add mx my) = do
  x <- mx
  y <- my
  case (x,y) of
    (Left x' ,Left y') -> return (Left (x' + y'))
    _                  -> Nothing

evalSBool :: FBool SDom -> SDom
evalSBool TT = Just $ Right True
evalSBool FF = Just $ Right False

evalSIF :: FIF SDom -> SDom
evalSIF (IF b tt ff)  = b >>= \case (Right True) -> tt
                                    (Right False) -> ff
                                    _ -> Nothing

evalSDom :: NatIf SDom -> SDom
evalSDom = threeAlg evalNatDom evalSIF evalSBool

interp :: Lang -> SDom
interp = cata evalSDom

-- Cost Analysis.

type CDom = Maybe (Either Int Bool , Int)

evalCNat :: FPlus CDom -> CDom
evalCNat Z = Just (Left 0 , 1)
evalCNat (S mn) = mn >>= \case
  (Left n , cn) -> Just (Left (n+1) , cn + 1)
  _ -> Nothing
evalCNat (Add mx my) = do
  (x', cx) <- mx
  (y', cy) <- my
  case (x',y') of
    (Left x , Left y) -> Just (Left (x + y), 2*cx + cy)
    _                 -> Nothing

evalCBool :: FBool CDom -> CDom
evalCBool TT = Just (Right True, 1)
evalCBool FF = Just (Right False, 1)

evalCIF :: FIF CDom -> CDom
evalCIF (IF b tt ff) = b >>= \(bv,bc) -> case bv of
                                           (Right True) -> tt >>= \(tv,tc) -> Just (tv, tc + bc)
                                           (Right False) -> ff >>= (\(fv,fc) -> Just (fv, fc + bc))
                                           _ -> Nothing

evalCDom :: NatIf CDom -> CDom
evalCDom = threeAlg evalCNat evalCIF evalCBool

costNatIf :: Lang -> CDom
costNatIf = cata evalCDom

-- Artificial costs

