module SKI.Test where

import           Improvement.Algebra

import           SKI.SKI

infixl 4 $$

($$) :: SKI -> SKI -> SKI
($$) = app

test :: SKI
test = app i i

-- I as SKK
skk :: SKI
skk = s $$ k $$ k

-- I as SKS
sks :: SKI
sks = s $$ k $$ skk

-- I as SSKK
sskk = s $$ s $$ k $$ k

test2 :: SKI
test2 = app (app (app s k) k) i

test3 :: SKI
test3 = app s (app (app k i) i)

rever :: SKI
rever = app (app s (app k (app s i))) k

self :: SKI
self = app (app s i) i

bot :: SKI
bot = self $$ self

recu :: SKI -> SKI -> SKI -- recu a b = a (bb)
recu a b = (s $$ (k $$ a) $$ self) $$ b

b :: SKI
b = s $$ (k $$ s) $$ k
c :: SKI
c = s $$ (s $$ (k $$ (s $$ (k $$ s) $$ k)) $$ s) $$ (k $$ k)
w :: SKI
w = s $$ s $$ (s $$ k)

nI :: Int -> SKI
nI 0 = i
nI n = app (nI (n-1)) i
