module SKI.Turner where

-- Turner's optimizations

import           Data.Empty
import           Data.Hole
import           Improvement.Algebra
import           Improvement.Improvement
import           Improvement.InstrAlgebra

import           SKI.SKI

-- Two extra combinators:

b :: FreeAlg Sig a
  -> FreeAlg Sig a
  -> FreeAlg Sig a
  -> FreeAlg Sig a
b f g x = app f $ app g x

c :: FreeAlg Sig a
  -> FreeAlg Sig a
  -> FreeAlg Sig a
  -> FreeAlg Sig a
c f g x = app (app f x) g

-- S (K e1) (K e2)
exp1 :: FreeAlg Sig a -> FreeAlg Sig a -> FreeAlg Sig a
exp1 e1 e2 = app (app s (app k e1)) (app k e2)

--  K (e1 e2)
exp2 :: FreeAlg Sig a -> FreeAlg Sig a -> FreeAlg Sig a
exp2 e1 e2 = app k (app e1 e2)

-- S (K e1) (K e2) >= K (e1 e2)
opt1 :: FreeAlg Sig Empty
     -- | ^ e1
     -> FreeAlg Sig Empty
     -- | ^ e2
     -> InstrAlg Sig Hole
     -- | ^ Context
     -> Int
     -- | ^ Gas
     -> Bool
opt1 e1 e2 =
  let
    t1 = toInstr $ exp1 e1 e2
    t2 = toInstr $ exp2 e1 e2
  in
  testImprov t1 t2

opt1' :: FreeAlg Sig Empty
     -- | ^ e1
     -> FreeAlg Sig Empty
     -- | ^ e2
     -> FreeAlg Sig Empty
     -- | ^ e3
     -> InstrAlg Sig Hole
     -- | ^ Context
     -> Int
     -- | ^ Gas
     -> Bool
opt1' e1 e2 e3 =
  let
    t1 = toInstr $ app (exp1 e1 e2) e3
    t2 = toInstr $ app (exp2 e1 e2) e3
  in
  testImprov t1 t2


-- S (K e1) I >= e1
opt2 :: FreeAlg Sig Empty
     -- | ^ e1
     -> InstrAlg Sig Hole
     -- | ^ Context
     -> Int
     -- | ^ Gas
     -> Bool
opt2 e =
  let
    t1 = toInstr $  app (app s (app k e)) i
    t2 = toInstr $ e
  in testImprov t1 t2

-- S (K e1) e2 >= B e1 e2
opt3 :: FreeAlg Sig Empty
     -- | ^ e1
     -> FreeAlg Sig Empty
     -- | ^ e2
     -> FreeAlg Sig Empty
     -- | ^ e3
     -> InstrAlg Sig Hole
     -- | ^ Context
     -> Int
     -- | ^ Gas
     -> Bool
opt3 e1 e2 e3 =
  let
    t1 = toInstr $ app (app (app s (app k e1)) e2) e3
    t2 = toInstr $ b e1 e2 e3
  in
    testImprov t1 t2

-- S e1 (K e2) >= C e1 e2
opt4 :: FreeAlg Sig Empty
     -- | ^ e1
     -> FreeAlg Sig Empty
     -- | ^ e2
     -> FreeAlg Sig Empty
     -- | ^ e3
     -> InstrAlg Sig Hole
     -- | ^ Context
     -> Int
     -- | ^ Gas
     -> Bool
opt4 e1 e2 e3 =
  let
    t1 = toInstr $ app (app (app s e1) (app k e2)) e3
    t2 = toInstr $ c e1 e2 e3
  in
    testImprov t1 t2
