{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase        #-}
module SKI.Eval where

import           Data.Empty
import           Improvement.Algebra      as Alg
import           Improvement.InstrAlgebra as IAlg

import           SKI.SKI                  hiding (app, i, k, s)

app :: InstrAlg Sig a -> InstrAlg Sig a -> InstrAlg Sig a
app f x = Con $ App f x

s :: InstrAlg Sig a
s = Con S

k :: InstrAlg Sig a
k = Con K

i :: InstrAlg Sig a
i = Con I

newtype Do a = Do {runDo :: [Do a] -> a}
type D = Do (InstrAlg Sig Empty)

appD :: Do a -> Do a -> Do a
appD (Do f) x = Do $ \xs -> f (x : xs)

eval :: Sig D -> D
eval I = Do $ \case
  [] -> i
  (Do x : xs) -> Tick $ x xs
eval K = Do $ \case
    [] -> k
    [Do x] -> app k (x [])
    (Do x : _ : xs) -> Tick $ x xs
eval S = Do $ \case
    [] -> s
    [Do x] -> app s (x [])
    [Do f , Do g] -> app (app s (f [])) (g [])
    (Do f : g : x : xs) -> Tick $ f (x : appD g x : xs)
eval (App f x) = appD f x

--------------------------------------------------------------------------------
-- Cost Domain?
--------------------------------------------------------------------------------

type Dc = Do Int

cost :: Sig Dc -> Dc
cost I = Do $ \case
  [] -> 0
  (Do x : xs) -> 1 + x xs
cost K = Do $ \case
    [] -> 0
    [Do x] -> x [] -- app k (x [])
    (Do x : _ : xs) -> 1 + x xs
cost S = Do $ \case
    [] -> 0
    [Do x] -> x []
    [Do f , Do g] -> f [] + g []
    (Do f : g : x : xs) -> 1 + f (x : appD g x : xs)
cost (App f x) = appD f x

-- Run

runTimed :: FreeAlg Sig Empty -> InstrAlg Sig Empty
runTimed = flip runDo [] . Alg.fold eval empty
