{-# LANGUAGE ExplicitForAll       #-}
{-# LANGUAGE FlexibleInstances    #-}
{-# LANGUAGE GADTs                #-}
{-# LANGUAGE PatternSynonyms      #-}
{-# LANGUAGE TypeSynonymInstances #-}

module SKI.SKI where

import           Prelude             hiding (Traversable)

import           Data.Empty
import           Improvement.Algebra

import           Class.HigherClasses
import           Class.Traversable

import           Test.QuickCheck

-- SKI signature. Sig X = 3 + X²
data Sig x = S | K | I | App x x
  deriving Eq

instance HArbitrary Sig where
    harbitrary = frequency
                   [ (1, pure I)
                   , (1, pure K)
                   , (1, pure S)
                   , (2, App <$> arbitrary <*> arbitrary)
                   ]

instance Functor Sig where
    fmap _ I         = I
    fmap _ K         = K
    fmap _ S         = S
    fmap f (App g x) = App (f g) (f x)

instance Traversable Sig where
  dist I         = pure I
  dist K         = pure K
  dist S         = pure S
  dist (App f x) = (\f x -> (App f x)) <$> f <*> x

instance EqF Sig where
  (=!=) I I                 = True
  (=!=) K K                 = True
  (=!=) S S                 = True
  (=!=) (App x y) (App p q) = (x == p) && (y == q)
  (=!=) _ _                 = False

instance HShow Sig where
  hshow I           = "I"
  hshow K           = "K"
  hshow S           = "S"
  hshow (App fs xs) = "("++ fs ++ " " ++ xs ++")"

type SKI = FreeAlg Sig Empty

getTTerm :: FreeAlg Sig SKI -> Sig SKI
getTTerm (Ret x) = getTerm x
getTTerm (Op t)  = fmap join t

app :: FreeAlg Sig x -> FreeAlg Sig x -> FreeAlg Sig x
app f x = Op (App f x)

i :: FreeAlg Sig x
i = Op I

k :: FreeAlg Sig x
k = Op K

s :: FreeAlg Sig x
s = Op S

pattern IP :: forall a. FreeAlg Sig a
pattern IP <- Op I
  where IP = Op I

pattern KP :: forall a. FreeAlg Sig a
pattern KP <- Op K
  where KP = Op K

pattern SP :: forall a. FreeAlg Sig a
pattern SP <- Op S
  where SP = Op S

pattern AppP :: forall a. FreeAlg Sig a -> FreeAlg Sig a -> FreeAlg Sig a
pattern AppP f x <- Op (App f x)
  where AppP f x = Op $ App f x

showTerm :: SKI -> String
showTerm = fold showAlg empty
  where
    showAlg I           = "I"
    showAlg K           = "K"
    showAlg S           = "S"
    showAlg (App fs xs) = "("++ fs ++ " " ++ xs ++")"
