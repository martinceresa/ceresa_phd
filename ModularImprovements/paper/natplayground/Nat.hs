{-# LANGUAGE GADTs #-}

module Nat where

import           Prelude hiding (succ)

data TickedFix f where
  Fix :: f (TickedFix f) -> TickedFix f
  Tick :: TickedFix f -> TickedFix f

data NatF x = O | S x

instance Functor NatF where
  fmap _ O     = O
  fmap f (S x) = S $ f x

type Nat = TickedFix NatF

cataTick :: (NatF a -> a) -> (a -> a) -> TickedFix NatF -> a
cataTick alg s (Fix t)  = alg (fmap (cataTick alg s) t)
cataTick alg s (Tick t) = s (cataTick alg s t)

addTicked :: NatF (TickedFix NatF -> TickedFix NatF) -> (TickedFix NatF -> TickedFix NatF)
addTicked O     = id
addTicked (S n) = Tick . (Fix . S) . n

zero :: TickedFix NatF
zero = Fix O

succ :: TickedFix NatF -> TickedFix NatF
succ = Fix . S

add :: Nat -> Nat -> Nat
add = cataTick addTicked id

toNat :: Int -> Nat
toNat 0 = zero
toNat n = succ $ toNat $ n - 1

evalNCost :: NatF (Int , Int) -> (Int , Int)
evalNCost O          = (0 , 0)
evalNCost (S (v, c)) = (1 + v , 1 + c)

evalCost :: TickedFix NatF -> (Int , Int)
evalCost = cataTick evalNCost ( \ (v,c) -> (v, 1 + c) )
