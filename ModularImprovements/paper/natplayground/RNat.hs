module RNat where

import Control.Arrow

newtype Fix f = Fix {unF :: f (Fix f)}

-- Nat X = 1 + X
data Nat x = O | S x

-- PTicked F X = F X + X
data PTick f x = Simpl (f x) | Ticked x

type Naturals = Fix Nat
type TickedNaturals = Fix (PTick Nat)

instance Functor Nat where
  fmap _ O     = O
  fmap f (S x) = S $ f x

instance Functor f => Functor (PTick f) where
  fmap f (Simpl t) = Simpl $ fmap f t
  fmap f (Ticked x) = Ticked $ f x

cata :: Functor f => (f a -> a) -> Fix f -> a
cata alg = alg . fmap (cata alg) . unF

cataTK :: Functor f => (f a -> a) -> (a -> a) -> Fix (PTick f) -> a
cataTK alg tk = cata nalg
  where
    nalg (Simpl t) = alg t
    nalg (Ticked x) = tk x

prodAlg :: Functor f => (f a -> a) -> (f b -> b) -> f (a , b) -> (a , b)
prodAlg alg blg t = ( alg $ fmap fst t , blg $ fmap snd t )

naturals :: Nat Int -> Int
naturals O = 0
naturals (S x) = 1 + x

-- Just do not care about ticks?
lift :: (f a -> a) -> PTick f a -> a
lift alg (Simpl t) = alg t
lift _ (Ticked t) = t

-- Cost
toCost :: Functor f => (f (a , Int) -> (a, Int)) -> PTick f (a , Int) -> (a , Int)
toCost alg (Simpl t) = alg t
toCost _ (Ticked (v, c)) = (v , 1 + c)

evalTKs :: Fix (PTick Nat) -> (Int , Int)
evalTKs = cataTK (prodAlg naturals naturals) (second (+1))
