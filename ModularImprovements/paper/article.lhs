\documentclass[sigplan, review]{acmart}

%include polycode.fmt
\usepackage{booktabs} % For formal tables
\usepackage[utf8]{inputenc}

%format MU = "\mu"
%format banana (a) = "\llparenthesis" a "\rrparenthesis"
%format Nat = "\Nat"
%% \usepackage{tikz-cd} % Commutative diagrams
%% \usepackage{tikz}
%% \tikzset{node distance=2cm, auto}

\usepackage[all]{xy}

% Copyright
\setcopyright{none}

% DOI
\acmDOI{10.475/123_4}

% ISBN
\acmISBN{123-4567-24-567/08/06}

%Conference
\acmConference[NeverLandConf'19]{ACM NeverLand conference}{Never 2019}{ NeverStop , NeverStopping}
\acmYear{2019}
\copyrightyear{2019}

\acmPrice{-90.00-}

%% LHS
%% include definitions.tex
\input{definitions.tex}

\begin{document}

\title{From Initital Algebra Semantics to Proof Principles}
\titlenote{Yep}
\subtitle{An Operational Metatheory for something}
\subtitlenote{The full version of the author's guide is available as
  \texttt{acmart.pdf} document}
\author{Martín Ceresa}
\affiliation{\institution{CIFASIS-CONICET}
            \country{Argentina}}
\email{ceresa@@cifasis-conicet.gov.ar}
%
\author{Mauro Jaskelioff}
\affiliation{\institution{CIFASIS-CONICET}
            \country{Argentina}}
\email{jaskelioff@@cifasis-conicet.gov.ar}

%\newcommand{\cifasis}{\ensuremath{\textrm \textsection}}

%include abstract.tex

% This is just an example
\begin{CCSXML}
<ccs2012>
  <concept>
    <concept_id>10003752.10003777.10003787</concept_id>
    <concept_desc>Theory of computation~Complexity theory and logic</concept_desc>
    <concept_significance>100</concept_significance>
  </concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[100]{Theory of computation~Complexity theory and logic}

\maketitle

% \keywords

%include introduction.tex
%
%include motivation.tex
\input{initialalg.tex}
%
%include improvement.tex
%
%include initialimprovement.tex
%
% %include relatedwork.tex
%
%include conclusions.tex

\bibliographystyle{ACM-Reference-Format}
\bibliography{bib}
\end{document}
