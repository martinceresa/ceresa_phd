{-# LANGUAGE GADTs         #-}
{-# LANGUAGE TypeFamilies  #-}
{-# LANGUAGE TypeOperators #-}
module POC where

import           Control.Arrow (second)

data (+) a b = Inl a | Inr b
newtype Fix f = Fix {unFix :: f (Fix f)}
type Alg f a = f a -> a

data Zero
type One = ()
type Two = One + One

data Free f a where
  Pure :: a -> Free f a
  Op :: f (Free f a) -> Free f a

fold :: Functor f => Alg f b -> (a -> b) -> Free f a -> b
fold _ var (Pure x) = var x
fold alg var (Op t) = alg (fmap (fold alg var) t)

join :: Functor f => Free f (Free f a) -> Free f a
join = fold Op id

instance Functor f => Functor (Free f) where
  fmap f = fold Op (Pure . f)

subst :: Functor f => Free f a -> (a -> Free f ()) -> Free f ()
subst t sts = join $ fmap sts t

cata :: Functor f => Alg f a -> Fix f -> a
cata alg = alg . fmap (cata alg) . unFix

-- Fixpoint Operator. Thank to Al-lãh Haskell is lazy.
-- Suck it Agda.
fixop :: (a -> a) -> a
fixop f = f (fixop f)

data V where
  -- Lattice Structure
  VBot :: V
  VTop :: V
  -- Injection Functions
  -- Why do we have integers and booleans? We never produce neither of them.
  VI :: Int -> V
  VB :: Bool -> V
  --
  VF :: (V -> V) -> V

data D where
  -- Lattice Structure
  Bot :: D
  Top :: D
  -- Injection Functions
  I :: (Int, Int) -> D
  B :: (Bool, Int) -> D
  F :: ((D , Int) -> (D , Int)) -> D

addC :: D -> Int -> D
addC Top   _ = Top
addC Bot   _ = Bot
addC (I v) a = I $ second (+ a) v
addC (B b) a = B $ second (+ a) b
addC (F f) a = F $ second (+ a) . f

data SAL x r where
  Var :: x -> SAL x r
  Cond :: (r , r , r) -> SAL x r
  App :: (r , r) -> SAL x r
  Abs :: (x , r) -> SAL x r
  Let :: (x , r , r) -> SAL x r
  LetR :: (x, r , r ) -> SAL x r

instance Functor (SAL x) where
  fmap _ (Var x)            = Var x
  fmap f (Cond (c,tt,ff))   = Cond (f c, f tt, f ff)
  fmap f (App (g, x))       = App (f g, f x)
  fmap f (Abs (x, b))       = Abs (x, f b)
  fmap f (Let (x , e , b))  = Let (x , f e , f b)
  fmap f (LetR (x , e , b)) = Let (x , f e , f b)

-- Real SAL expressions.
type Exp x = Fix (SAL x)

type CD =
  -- Values are represented as D
  (D
  ,
  -- Overall cost.
   Int)

-- least CD element?
leastCD :: CD
leastCD = (Bot, 0)

-- greatest CD
-- gCD :: CD, gCD = (Top, Inf)

type E x = x -> CD

-- Semantic Domain.
type M x = E x -> CD

access :: x -> E x -> CD
access x e = e x

cond :: (CD, CD, CD) -> CD
cond (c, tt, ff) = case c of
  (Top          , d) -> (Top, d)
  (Bot          , d) -> (Bot, d)
  (B (True , tc), d) -> second (+ (tc + d)) tt
  (B (False, fc), d) -> second (+ (fc + d)) ff
  _                  -> leastCD

castF :: (D, Int) -> (D, Int) -> (D, Int)
castF (d, c) = case d of
  (F f) -> second (+ c) . f
  _     -> const leastCD

app :: (CD, CD) -> CD
app (f, x) = castF f x

assign :: Eq x => x -> (E x, CD) -> E x
assign x (f, v) y | x == y    = v
                  | otherwise = f y

eval :: Eq x => Alg (SAL x) (M x)
eval (Var  x          ) env = access x env
eval (Cond (c, tt, ff)) env = cond (c env, tt env, ff env)
eval (App  (f, x)     ) env = app (f env, x env)
eval (Abs  (x, b)     ) env = (F (curry (b . assign x) env), 0)
eval (Let  (x, e, b)  ) env = b $ assign x (env, e env)
eval (LetR (x, e, b)) env = eval (Let (x, fixop . curry (e . assign x), b)) env

evaluator :: Eq x => Exp x -> M x
evaluator = cata eval
