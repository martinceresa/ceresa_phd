================================================================================
+++++++++++++++++++++++++++++++++++++++PHD++++++++++++++++++++++++++++++++++++++
================================================================================
+ Write, write and write
  * Monotonicity. From Algebras to Contexts Lemmas (or Extensionality)
    Relation Lifting through Algebras
  * Theoretical Chapters.
+ There is a more general theorem of Knaster-Tarski? Remember that
  a complete lattice is nonsense in CS, but here I do have a structure with
  some limits... Coherently Complete (or something like that).
+ Resultados como Imp Induction?
  Seguir pensando como veníamos: Cómo comparo handlers?
  Tick sirve para algo?
+ Mecanizar algo de esto? Buscar teoremas inductivos?
  * Abella/Beluga
  * Coq [Ott/LNgen]/SMTCoq

---- Ugo
+ M ≥ N ⇔ {Mₙ}ω ≥ {Nₙ}ω ?

--------------------------------------------------------------------------------
