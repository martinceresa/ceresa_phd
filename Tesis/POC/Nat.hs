{-# Language GADTs #-}
module Nat where

import Cata

-- Nat polinomial Functor
data FNat f where
  Z :: FNat f
  S :: f -> FNat f

-- Functor instance
instance Functor FNat where
  fmap _ Z = Z
  fmap f (S x) = S $ f x

-- Nat is the fixpoint of Nat Functor
type Nat = Fix FNat

-- Here is the __bijection__
-- Not exactly since Int is not Nat!
seman :: Nat -> Int
seman = cata alg
  where
    alg Z = 0
    alg (S n) = 1 + n

names :: Int -> Nat
names 0 = Fix Z
names n = Fix $ S (names (n - 1))

-- Well, now I want to count costs...

type SeCo = (Int, Int)

secos :: Nat -> SeCo
secos = cata alg
  where
    alg Z = (0 , 0)
    alg (S (s, c)) = (s + 1 , c + 1)

---------
suc :: Nat -> Nat
suc = cata (Fix . alg)
  where
    alg Z = S $ Fix Z
    alg (S n ) = S n

add :: Nat -> Nat -> Nat
add = cata alg
  where
    alg Z = id
    alg (S n) = suc . n

{-
But now there is no difference between add |2| |2| and |4|. Which is something
we do not wanted to lose. But here is nothing to do here... If we want to take
|add| into the equation we need to add the add operation into our algebra (or
dictionary). It has no sense to add structure to |Nat| since it should be
general enough (or pure enough) to have natural as its initial algebra.

-}


---------
