module DiFunctorFold where

import           Prelude hiding (succ)

newtype Comp f g a = Comp {unComp :: f ( g a )}

instance ( Functor f, Functor g ) => Functor (Comp f g) where
  fmap f (Comp x) = Comp $ fmap (fmap f) x

data Empty
data Nat x = Z | S x

instance Functor Nat where
  fmap _ Z     = Z
  fmap f (S x) = S (f x)

newtype FCost a = C {unC :: (a , Int)}
  deriving Show

cost :: FCost a -> Int
cost = snd . unC

class CoFunctor f where
  comap :: (b -> a) -> f a -> f b

class MixFunctor f where
  codmap :: (b -> a) -> f a c -> f b c
  dmap :: (a -> b) -> f c a -> f c b
  dimap :: (a -> b) -> (d -> c) -> f c a -> f d b
  dimap va co = dmap va . codmap co

instance Functor FCost where
  fmap f (C (a , c)) = C (f a , c)

instance Applicative FCost where
  pure a = C (a , 0) -- a given value from heaven has no cost.
  (C (f , fc)) <*> (C (x , c)) = C (f x, fc + c)

join :: FCost (FCost x) -> FCost x
join (C (C (v,c1), c2)) = C (v , c1 + c2)


type Alg f a = (f a -> a)

newtype Fix f = Fix {unFix :: f (Fix f)}

newtype Rec f = In {out :: f (Rec f) (Rec f)}

cata :: Functor f => (f a -> a) -> Fix f -> a
cata alg = alg . fmap (cata alg) . unFix

anaCleta :: Functor f => (a -> f a) -> a -> Fix f
anaCleta coalg = Fix . fmap (anaCleta coalg ) . coalg

type DiAlg f a b = ( f b a -> a, b -> f a b)

dicata :: MixFunctor f => DiAlg f a b -> Rec f -> a
dicata di@(alg, _) = alg . dimap (dicata di) (diana di) . out
diana :: MixFunctor f => DiAlg f a b -> b -> Rec f
diana di@(_ , coalg) = In . dimap (diana di) (dicata di)  . coalg

forAlgNat :: a -> (a -> a) -> Alg Nat a
forAlgNat  z _ Z = z
forAlgNat  _ s (S n ) = s n

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- General fold form. \forall \tau \in Types, x \in \micro Nat.
-- fold_\tau z s x \in \tau
data NatFold cv v
  = NatFold v (cv -> v) v
  | Zero
  | Succ v

instance MixFunctor NatFold where
  codmap g (NatFold x f n) = NatFold x (f . g) n
  codmap _ Zero = Zero
  codmap _ (Succ v) = Succ v
  dmap f (NatFold x g n) = NatFold (f x) (f . g) (f n)
  dmap _ Zero = Zero
  dmap f (Succ v) = Succ (f v)

type NatFolds = Rec NatFold

type TFold a = NatFold a a

succTF :: TFold SNat -> TFold SNat
succTF n = Succ $ fst natAlgebra n

anaNat :: Alg Nat (TFold SNat)
anaNat Z = Zero
anaNat (S n) = succTF n

natAlgebra :: DiAlg NatFold SNat SNat
natAlgebra = ( cat , an )
  where
    cat (NatFold z s n) = cata (forAlgNat z s) n
    cat Zero = zero
    cat (Succ n) = succ n
    an = cata anaNat

tick :: FCost a -> FCost a
tick = flip addCost 1
natCostAlgebra :: DiAlg NatFold (FCost SNat) (FCost SNat)
natCostAlgebra = ( cat , an )
  where
    cat Zero = pure zero
    cat (Succ n) = succ <$> n -- Why this is so important?
    -- cat (Succ n) = tick (succ <$> n) -- Why this is so important?
    cat (NatFold z s (C (v, c))) = cata (forAlgNat z s) v `addCost` norm v `addCost` c `addCost` cost z
    an (C (n , _)) =  cata alg n
    alg Z = Zero
    alg (S n) = Succ $ cat n

eval :: NatFolds -> SNat
eval = dicata natAlgebra

evalCost :: NatFolds -> FCost SNat
evalCost = dicata natCostAlgebra

fromSNat :: SNat -> NatFolds
fromSNat = diana natAlgebra
-- fromSNat = cata alg
--   where
--     alg Z = zeroR
--     alg (S n) = succR n

-- evalCost :: DiAlg NatFold (SNat, Int) (SNat, Int)
-- evalCost = ( _ , _)

zeroR :: NatFolds
zeroR = In Zero
succR :: NatFolds -> NatFolds
succR = In . Succ

fromIntToNats :: Int -> NatFolds
fromIntToNats = fromSNat . fromInt

addEx1 :: NatFolds
addEx1 = addNats (addNats (fromIntToNats 42) (fromIntToNats 84)) (fromIntToNats 1)

addEx2' :: NatFolds
addEx2' = addNats (fromIntToNats 84) (fromIntToNats 1)
addEx2 :: NatFolds
addEx2 = addNats (fromIntToNats 42) (addNats (fromIntToNats 84) (fromIntToNats 1))

addNats :: NatFolds -> NatFolds -> NatFolds
addNats x y = In $ NatFold y succR x

multNats :: NatFolds -> NatFolds -> NatFolds
multNats x y = In $ NatFold zeroR (addNats y) x

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- Nats
type SNat = Fix Nat

canAlg :: Alg Nat SNat
canAlg Z = zero
canAlg (S n) = succ n

zero :: SNat
zero = Fix Z

succ :: SNat -> SNat
succ = Fix . S

costAlg :: Alg Nat x -> Alg Nat (FCost x)
costAlg alg Z = pure (alg Z)
costAlg alg (S (C (v, c))) = C (alg (S v), c + 1)

addCost :: FCost x -> Int -> FCost x
addCost ( C (v, c) ) x = C (v , c + x)

normAlg :: Alg Nat Int
normAlg Z = 0
normAlg (S n) = n + 1
norm :: SNat -> Int
norm = cata normAlg

cataCost :: Alg Nat (FCost x) -> SNat -> FCost x
cataCost alg n = cata alg n `addCost` norm n

-- toFix :: NatFold SNat
-- toFix = NatFold zero succ

addN :: Alg Nat (SNat -> SNat)
addN Z = id
addN (S n) = succ . n

addAlgs :: Alg Nat (Alg Nat SNat)
addAlgs Z = canAlg
addAlgs (S n) = succ . n

addWCost :: SNat -> SNat -> FCost SNat
addWCost x y = cataCost (costAlg addN) x <*> pure y

fromInt :: Int -> SNat
fromInt 0 = zero
fromInt n = succ (fromInt (n - 1))

t1 = join $ addWCost (fromInt 42) <$> (addWCost (fromInt 84) (fromInt 1))
t2 = join $ addWCost <$> (addWCost (fromInt 42) (fromInt 84)) <*>  pure (fromInt 1)
