{-# Language GADTs #-}
module AddNat where

import Cata

-- Nats plus |add| op
data FNat f where
  Z :: FNat f
  S :: f -> FNat f
  A :: f -> f -> FNat f

instance Functor FNat where
  fmap _ Z = Z
  fmap f (S x) = S $ f x
  fmap f (A x y) = A (f x) (f y)

type Nat = Fix FNat

seman :: Nat -> Int
seman = cata alg
    where
      alg Z = 0
      alg (S n) = 1 + n
      alg (A x y) = x + y

-- I guess this is somewhat the minimal into (?).
-- Any other into causes more cost??
into :: Int -> FNat Nat
into 0 = Z
into n = S (Fix $ into n)

cost :: Nat -> (Int , Int)
cost = cata alg
  where
    alg Z = (0,0)
    alg (S (v,c)) = (1 + v, 1 + c)
    -- Not really sure about this, or at least I should have a good explanation.
    -- Which I clearly do not have.
    alg (A (x,xc) (y, yc)) = ( x + y , 2*xc + yc )

-- toCost :: Nat -> OldNats and compute costs there? Taht would be a good
-- idea... To move throw algebras, like from higher algebras with a lot of
-- operations into like the essential one which only have just the required
-- values... Like Nat+LotsOfOps into just Nats, but with a lot of cost
-- information

-- Now we can use the contexts to write down somethings like macros and compare
-- them!! :D

c1 :: Free FNat Three
c1 = Op (A (Pure T1) (Op (A (Pure T2) (Pure T3))))

c2 :: Free FNat Three
c2 = Op (A (Op (A (Pure T1) (Pure T2))) (Pure T3))
