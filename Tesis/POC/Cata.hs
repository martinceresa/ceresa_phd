{-# Language GADTs #-}
module Cata where

----------------------------------------
-- Fix definitions
newtype Fix f = Fix {unF :: f (Fix f)}

-- Catamorphisms
cata :: Functor f => (f a -> a) -> Fix f -> a
cata h = h . fmap (cata h) . unF

----------------------------------------
-- Free constructions, aka Contexts

data Free f a where
  Pure :: a -> Free f a
  Op :: f (Free f a) -> Free f a

fold :: Functor f => (f a -> a) -> (x -> a) -> Free f x -> a
fold _ v (Pure x) = v x
fold f v (Op t) = f $ fmap ( fold f v ) t

----------------------------------------

data Zero
data One = O1
data Two = D1 | D2
data Three = T1 | T2 | T3
