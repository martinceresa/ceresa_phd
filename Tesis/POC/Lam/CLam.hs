module CLam (cnormalize, cost) where

import           Lam                 (Term (..))
import           Names

import           Control.Monad.State

newtype Cost a = C { unC :: (a , Int) }
  deriving Show

distC :: Monad m => Cost (m a) -> m (Cost a)
distC (C (ma , i)) = ma >>= \a -> return $ C ( a , i)

instance Functor Cost where
  fmap f (C (a, n )) = C (f a , n)

instance Applicative Cost where
  pure = freeC
  f <*> x = zipC ($) f x

instance Monad Cost where
  return = pure
  (C (x, i)) >>= h = addC i (h x)

addC :: Int -> Cost a -> Cost a
addC i (C (a, j)) = C (a , i + j)

sucC :: Cost a -> Cost a
sucC (C (a, n)) = C (a , n + 1)

zipC :: (a -> b -> c) -> Cost a -> Cost b -> Cost c
zipC comb (C (a , ca)) (C (b,cb)) = C (comb a b, ca + cb)

freeC :: a -> Cost a
freeC x = C (x , 0)

drop :: Cost a -> a
drop (C (a , _)) = a

----------------------------------------
-- Types?
----------------------------------------
data Val = Res Atom Int
         | Fu (Val -> Val)
data Atom = Va Ident
          | Ap Atom Val

data Atm = V Ident | A Atm Norm
  deriving Show
data Norm = Ato Atm Int | LamN Ident Norm
  deriving Show

type Env = GEnv Val

idEnv :: Env
idEnv n = Res (Va n) 0

sucVal :: Val -> Val
sucVal (Res a i) = Res a (i + 1)
sucVal (Fu f) = Fu $ sucVal . f

----------------------------------------
-- Evaluation
----------------------------------------

-- First step, Term -> Val

apply :: Val -> Val -> Val
apply (Res a i) v = Res (Ap a v) i
apply (Fu f) v   = sucVal $ f v -- Function application

eval :: Term -> Env -> Val
eval (Var i) env   = env i
eval (Lam x b) env = Fu $ \ v -> eval b (upt env x v)
eval (App f x) env = apply (eval f env) (eval x env)

atto :: Atom -> State Names Atm
atto (Va i) = return (V i)
atto (Ap a v) = A <$> atto a <*> reify v

getNm :: State Names Ident
getNm = do
  (n, ns) <- gets dec
  put ns
  return n

-- Second Step, reifycation Val -> Norm
reify :: Val -> State Names Norm
reify (Res a i) = Ato <$> atto a <*> pure i
reify (Fu f) =
  do
    x <- getNm
    f' <- reify (f (Res (Va x) 0))
    return $ LamN x f'

-- cost just accumulates ints but do not add anything
costA :: Atm -> Int
costA (V _) = 0
costA (A f v) = costA f + cost v

cost :: Norm -> Int
cost (Ato a i) = i + costA a
cost (LamN _ n) = cost n

cnormalize :: Term -> Norm
cnormalize m = flip evalState initNames $ reify $ eval m idEnv
