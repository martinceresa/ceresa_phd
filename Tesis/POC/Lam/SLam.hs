module SLam (snormalize) where

import           Control.Monad.State
import           Names

import           Lam                 (Term (..))

-- Here the idea is to get a working example of cost analysis following the
-- evaluation on Lam

-- As first approximation I simplify the evaluation a bit trying to reify just
-- at the end.

data Atom n = VarN Ident | AppA (Atom n) n
  deriving Show

dist :: Monad m => Atom (m a) -> m (Atom a)
dist (VarN i)   = pure (VarN i)
dist (AppA f v) = AppA <$> dist f <*> v

data Val a = Res a | Fun (Val a -> Val a)

instance Functor Atom where
  fmap _ (VarN i)   = VarN i
  fmap f (AppA l r) = AppA (fmap f l) (f r)

-- reified version of Val!
data Norm = A (Atom Norm) | LamN Ident Norm
  deriving Show

newtype Fix f = Fix{unF :: f (Fix f)}
----------------------------------------
-- Val -> Val?
----------------------------------------
-- I want to normalize terms just rewriting vals, and reify at the end
-- eval :: Val -> Val
-- reify :: Val -> Norm
-- normlize = reify . eval
newtype NVal1 a = I {unI :: Val (Atom a) }
type Val1 = Fix NVal1

type Env = GEnv Val1

idEnv :: Env
idEnv = inG . Res . VarN

unG :: Val1 -> Val (Atom Val1)
unG = unI . unF

inG :: Val (Atom Val1) -> Val1
inG = Fix . I

-- unG . inG = id
-- inG . unG = id

----------------------------------------
-- Evaluation !
----------------------------------------
-- Patten Syn should be an improvement here ejeje
apply :: Val1 -> Val1 -> Val1
apply (Fix (I (Res a))) x = inG $ Res $ AppA a x
apply (Fix (I (Fun f))) x = inG $ f (unG x)

eval :: Term -> Env -> Val1
eval (Var i) env   = env i
eval (Lam x b) env = inG $ Fun $ \ v -> unG $ eval b (upt env x (inG v))
eval (App f x) env = apply (eval f env) (eval x env)

----------------------------------------
-- Reify !
----------------------------------------
getNm :: State Names Ident
getNm = do
  (n, ns) <- gets dec
  put ns
  return n

reify :: Val (Atom Val1)-> State Names Norm
reify (Fun f) =
  do
    x <- getNm
    f' <- reify (f (Res $ VarN x))
    return $ LamN x f'
reify (Res a) = A <$> dist (fmap ( reify . unG) a)

----------------------------------------
-- Normalize !
----------------------------------------
snormalize :: Term -> Norm
snormalize m = flip evalState initNames $ reify $ unG $ eval m idEnv
