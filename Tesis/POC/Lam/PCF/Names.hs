module Names where

import           Prelude             hiding (drop, zipWith)

type Ident = String

-- New names generator
data Stream a = Cons a (Stream a)

instance Functor Stream where
  fmap f (Cons x rest) = Cons (f x) (fmap f rest)

dec :: Stream a -> (a , Stream a)
dec (Cons r rs) = (r,rs)


nats :: Stream Int
nats = Cons 0 (fmap (1+) nats)

xes :: Stream Char
xes  = Cons 'x' xes

zipWith :: (a -> b -> c) -> Stream a -> Stream b -> Stream c
zipWith f (Cons a as) (Cons b bs) = Cons (f a b) (zipWith f as bs)

type Names = Stream Ident

initNames :: Stream Ident
initNames = zipWith (\ x i -> x : show i)  xes nats

-- Environment
type GEnv a = Ident -> a

upt :: GEnv a -> Ident -> a -> GEnv a
upt env x v y = if x == y then v else env y
