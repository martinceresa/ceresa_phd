{-# Language GADTs #-}

module Terms where

import Cata
import Names

-- Language Constructions as a Fixpoint of a Signature Functor

data PCF a where
  Var :: Ident -> PCF a

data Pro x
  = FVar Ident
  | FAbs Ident x
  | FApp x x
  | FY x
  | FZero
  | FSucc x
  | FPred x
  | FIFZ x x x

instance Functor Pro where
  fmap _ FZero = FZero
  fmap _ (FVar x ) = FVar x
  fmap f (FAbs n b) = FAbs n $ f b
  fmap g (FApp f x) = FApp (g f) (g x)
  fmap g (FY x) = FY $ g x
  fmap g (FSucc x) = FSucc $ g x
  fmap g (FPred x) = FPred $ g x
  fmap g (FIFZ c tt ff) = FIFZ (g c) (g tt) (g ff)

data HPro x a where
  HVar :: Ident -> HPro x a
  HAbs :: Ident -> x a -> HPro x a

type Terms = Fix Pro
----------------------------------------

data Val
  = At Atom
  | Fn (Val -> Val)

data Atom
  = I Ident
  | App Atom Val
  | Y Atom
  | Zero
  | Succ Val
  | Pred Val
  | Ifz Val Val Val

type Env = GEnv Val

plusOne :: Val -> Val
plusOne (At )

eval :: Pro (Env -> Val) -> Env -> Val
-- 0 ▹ 0
eval FZero _ = At Zero
-- Succ n ▹ n + 1
eval (Succ m) env = 

