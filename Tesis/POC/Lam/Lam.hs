module Lam (normalize , Term (..)) where

import           Prelude             hiding (drop, zipWith)

import           Control.Monad.State

import           Names

-- Classic Lambda Calculus terms
data Term = Var Ident | Lam Ident Term | App Term Term
  deriving (Show , Eq)

-- -- So its fold is
-- foldL ::
--       -- Variables
--          (Ident -> a)
--       -- Abstractions
--       -> (Ident -> a -> a)
--       -- Application
--       -> (a -> a -> a)
--       -> Term -> a
-- foldL vr _ _ (Var x)     = vr x
-- foldL vr lm ap (Lam x b) = lm x (foldL vr lm ap b)
-- foldL vr lm ap (App f x) = ap (foldL vr lm ap f) (foldL vr lm ap x)

--------------------------------------------------------------------------------
-- Evaluation ?
--------------------------------------------------------------------------------

-- Again, complex type to represent either atom values or functions
data Val =
  Res Atom
  | Fun (Val -> Val)

-- Atomic values, either variables or applications
data Atom = VarN Ident | AppA Atom Norm
  deriving Show
-- Normalize values are either atoms or lambda abstractions (but there is no
-- beta-redexes)
data Norm = At Atom | LamN Ident Norm
  deriving Show
-- State Names
getNm :: State Names Ident
getNm = gets (fst . dec)

type Env = GEnv Val

idEnv :: Env
idEnv = Res . VarN -- Free terms are cost free?

 -- These are free because we are just changing representations, no computations
 -- are performed
reify :: Val -> State Names Norm -- Names -> Val -> (Norm , Names)
reify (Res a) = return $ At a
reify (Fun f) =
  do
    nm <- getNm
    f' <- reify $ f (Res (VarN nm))
    return $ LamN nm f'

apply :: Val -> Val -> State Names Val
apply (Res a) v =
  do
    n <- reify v
    return $ Res (AppA a n)
apply (Fun f) v = return $ f v

-- Explicit State monad!
eval :: Term -> Env -> State Names Val
eval (Var x) env = return $ env x
eval (Lam x b) env =
  do
    nms <- gets (snd . dec)
    return $ Fun $ \v -> flip evalState nms $ eval b (upt env x v)
eval (App f x) env =
  do
    f' <- eval f env
    x' <- eval x env
    apply f' x'

normalize :: Term -> Norm
normalize m = flip evalState initNames $ eval m idEnv >>= reify
