module Ex where

import Lam

----------------------------------------
-- Test
----------------------------------------
-- data Term = Var Ident | Lam Ident Term | App Term Term

-- \x . x
idTerm :: Term
idTerm = Lam "y" (Var "y")

-- \ f v . v
zeroTerm :: Term
zeroTerm = Lam "f" $ Lam "v" $ Var "v"

-- \ n . \ f v . f (n f v)
sucTerm :: Term
sucTerm = Lam "n"
          $ Lam "f"
          $ Lam "v"
          $ App
              (Var "f")
              (App
                (App
                  (Var "n")
                  (Var "f"))
                (Var "v"))
-- \n . \ m . \ f v . n f (m f v)
addTerm :: Term
addTerm = Lam "n"
          $ Lam "m"
          $ Lam "f" $ Lam "v"
          $ App
            (App (Var "n") (Var "f"))
            (App (App (Var "m") (Var "f")) (Var "v"))

ltwo,lone,one,two,four, lfour :: Term
one = App sucTerm zeroTerm
two = App sucTerm one
four = App (App addTerm two) two
-- They should have 0-cost
lfour = Lam "f" $ Lam "v"
        $ App (Var "f") $ App (Var "f") $ App (Var "f") $ App (Var "f") (Var "v")
lone = Lam "f" $ Lam "v" $ App (Var "f") (Var "v")
ltwo = Lam "f" $ Lam "v" $ App (Var "f") (App (Var "f") (Var "v"))

-- (n + m) + o
-- + (+ n m) o
leftAdd,rightAdd :: Term -> Term -> Term -> Term
leftAdd n m o = App (App addTerm (App (App addTerm n) m)) o
-- n + ( m + o )
-- + n (+ m o)
rightAdd n m o = App (App addTerm n) (App (App addTerm m) o)

add :: Term -> Term -> Term
add m n = App (App addTerm m) n
