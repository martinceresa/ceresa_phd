@incollection{Sands:HOOTS,
  author          = {David Sands},
  title           = {Improvement Theory and Its Applications},
  pages           = {275--306},
  editor          = {A. D. Gordon and A. M. Pitts},
  booktitle       = {Higher {O}rder {O}perational {T}echniques in {S}emantics},
  publisher       = {Cambridge University Press},
  series          = {Publications of the Newton Institute},
  year            = 1998,
  abstract        = {An improvement theory is a variant of the standard theories
                  of observational approximation (or equivalence) in which the
                  basic observations made of a functional program's execution
                  include some intensional information about, for example, the
                  program's computational cost. One program is an improvement of
                  another if its execution is more efficient in any program
                  context. In this article we give an overview of our work on
                  the theory and applications of improvement. Applications
                  include reasoning about time properties of functional
                  programs, and proving the correctness of program
                  transformation methods. We also introduce a new application,
                  in the form of some bisimulation-like proof techniques for
                  equivalence, with something of the flavour of Sangiorgi's
                  ``bisimulation up-to expansion and context''.},
  pdf             = {http://www.cse.chalmers.se/~dave/papers/hoots97.pdf}
}
@phdthesis{Sands:PhDthesis,
  author = {David Sands},
  title = {Calculi for Time Analysis of Functional Programs},
  school = {Department of Computing, Imperial College},
  year = {1990},
  address = {University of London},
  month = {09},
  abstract = {Techniques for reasoning about extensional properties of
functional programs are well-understood, but methods for analysing  the
underlying intensional, or operational properties have been much neglected.
This thesis presents the  development of several calculi for time analysis
of functional programs.

We focus on two features, higher-order functions and lazy evaluation, which
contribute much to the expressive power and semantic elegance of
functional languages, but serve to make operational properties more
opaque.

Analysing higher-order functions is problematic because complexity is
dependent not only on the cost of computing, but also on the cost of {\em
applying}\ function-valued expressions.  Techniques for statically deriving
programs which compute time-cost in the presence of arbitrary higher-order
functions are developed. The key to this process is the introduction of
syntactic structures called {\em cost-closures}, which enable intensional
properties to be carried by functions. The approach is formalised by the
construction of an appropriate cost-model, against which the correctness of
the derivation is proved. A specific factorisation tactic for reasoning
about higher-order functions out of context is illustrated.

Reasoning about lazy evaluation (ie call-by-name, or more usually,
call-by-need) is problematic because the cost of evaluating an expression
cannot be understood simply from the costs of its sub-expressions.  A
direct calculus for reasoning about a call-by-name language with lazy lists
is derived from a simple operational model. In order to extend this
calculus with a restricted form of equational reasoning, a nonstandard
notion of operational approximation called {\em cost-simulation} is
developed, by analogy with {\em (bi)simulation} in CCS.

The problem with calculi of the above form, based directly on an
operational model, is that they do not yield a {\em compositional}
description of cost, and cannot model {\em lazy evaluation}
(graph-reduction) easily. We show how a description of the {\em context} in
which a function is evaluated can be used to parameterise two types of
time-equation: {\em sufficient-time} equations and {\em necessary-time}
equations, which together provide bounds on the exact time-cost of lazy
evaluation.  This approach is extended to higher-order functions using a
modification of the cost-closure technique.},
  pdf = {http://www.cse.chalmers.se/~dave/papers/PhDthesis.pdf}
}

@inproceedings{Sands:Skye,
  author          = {David Sands},
  title           = {Operational Theories of Improvement in Functional Languages
                  (Extended Abstract)},
  booktitle       = {Proceedings of the Fourth {G}lasgow Workshop on Functional
                  Programming},
  year            = 1991,
  series          = {Workshops in Computing Series},
  publisher       = {{S}pringer-Verlag },
  address         = {Skye},
  pages           = {298--311},
  month           = 08,
  abstract        = {In this paper we address the technical foundations
                  essential to the aim of providing a semantic basis for the
                  formal treatment of relative efficiency in functional
                  languages. For a general class of {``functional''} computation
                  systems, we define a family of improvement preorderings which
                  express, in a variety of ways, when one expression is more
                  efficient than another. The main results of this paper build
                  on Howe's study of equality in lazy computation systems, and
                  are concerned with the question of when a given improvement
                  relation is subject to the usual forms of (in)equational
                  reasoning (so that, for example, we can improve an expression
                  by improving any sub-expression). For a general class of
                  computation systems we establish conditions on the operators
                  of the language which guarantee that an improvement relation
                  is a precongruence. In addition, for a particular higher-order
                  nonstrict functional language, we show that any improvement
                  relation which satisfies a simple monotonicity condition with
                  respect to the rules of the operational semantics has the
                  desired congruence property. },
  pdf             = {http://www.cse.chalmers.se/~dave/papers/Sands:Skye.pdf}
}

@article{Sands:TOPLAS,
  author          = {David Sands},
  title           = {Total Correctness by Local Improvement in the
                  Transformation of Functional Programs},
  journal         = {ACM Transactions on Programming Languages and Systems
                  (TOPLAS)},
  year            = 1996,
  volume          = 18,
  number          = 2,
  month           = {March},
  pages           = {175--234},
  note            = {Extended version of \cite{Sands:POPL}},
  summary         = {The goal of program transformation is to improve efficiency
                  while preserving meaning. One of the best-known transformation
                  techniques is Burstall and Darlington's unfold-fold method.
                  Unfortunately the unfold-fold method itself guarantees neither
                  improvement in efficiency nor total correctness. The
                  correctness problem for unfold-fold is an instance of a
                  strictly more general problem: transformation by locally
                  equivalence-preserving steps does not necessarily preserve
                  (global) equivalence. This article presents a condition for
                  the total correctness of transformations on recursive
                  programs, which, for the first time, deals with higher-order
                  functional languages (both strict and nonstrict) including
                  lazy data structures. The main technical result is an
                  improvement theorem which says that if the local
                  transformation steps are guided by certain optimization
                  concerns (a fairly natural condition for a transformation),
                  then correctness of the transformation follows. The
                  improvement theorem makes essential use of a formalized
                  improvement theory; as a rather pleasing corollary it also
                  guarantees that the transformed program is a formal
                  improvement over the original. The theorem has immediate
                  practical consequences: it is a powerful tool for proving the
                  correctness of existing transformation methods for
                  higher-order functional programs, without having to ignore
                  crucial factors such as memoization or folding, and it yields
                  a simple syntactic method for guiding and constraining the
                  unfold-fold method in the general case so that total
                  correctness (and improvement) is always guaranteed. },
  pdf             = {http://www.cse.chalmers.se/~dave/papers/sands-TOPLAS96.pdf}
}

@inproceedings{Howe:Lazy.Eq,
  author          = {Douglas J. Howe},
  title           = {Equality In Lazy Computation Systems},
  booktitle       = {Proceedings of the Fourth Annual Symposium on Logic in
                  Computer Science {(LICS} '89), Pacific Grove, California, USA,
                  June 5-8, 1989},
  pages           = {198--203},
  year            = 1989,
  url             = {https://doi.org/10.1109/LICS.1989.39174},
  doi             = {10.1109/LICS.1989.39174},
  timestamp       = {Thu, 25 May 2017 00:42:40 +0200},
  biburl          = {https://dblp.org/rec/bib/conf/lics/Howe89},
  bibsource       = {dblp computer science bibliography, https://dblp.org}
}

@article{Ceresa.2022.EffectfulImp,
  abstract        = {Optimizing programs is hard. Not only one must preserve
                  semantics, but one also needs to ensure that an optimization
                  truly makes the program better. The first part, preserving
                  program semantics, has been, and still is, the subject of much
                  research. We follow a line of work that starts with Morris'
                  observational equivalence, continues with Abramsky's
                  applicative bisimilarity and Howe's method, and concludes in a
                  recent abstract formalization of applicative bisimilarity in
                  the presence of algebraic effects by Dal Lago, Gavazzo and
                  Levy. The second part, ensuring that an optimization truly
                  makes the program better, is a path less traveled, with the
                  improvement theory of Sands being the most prominent example.
                  In this work, we connect these two parts by obtaining an
                  abstract theory of improvements based on effectful applicative
                  bisimilarity that extends Sands' notion of improvement to
                  effectful languages.},
  author          = {Martin A. Ceresa and Mauro J. Jaskelioff},
  doi             = {https://doi.org/10.1016/j.scico.2022.102792},
  issn            = {0167-6423},
  journal         = {Science of Computer Programming},
  keywords        = {Relational cost analysis, Functional programming
                  optimization, Improvement theory, Algebraic effect},
  pages           = 102792,
  title           = {Effectful improvement theory},
  url             =
                  {https://www.sciencedirect.com/science/article/pii/S0167642322000259},
  volume          = 217,
  year            = 2022,
  Bdsk-Url-1      =
                  {https://www.sciencedirect.com/science/article/pii/S0167642322000259},
  Bdsk-Url-2      = {https://doi.org/10.1016/j.scico.2022.102792}
}

@inproceedings{Moran.1999.ImprovementLazyContext,
  author          = {Moran, Andrew and Sands, David},
  title           = {Improvement in a Lazy Context: An Operational Theory for
                  Call-by-Need},
  year            = 1999,
  isbn            = 1581130953,
  publisher       = {Association for Computing Machinery},
  address         = {New York, NY, USA},
  url             = {https://doi.org/10.1145/292540.292547},
  doi             = {10.1145/292540.292547},
  abstract        = {The standard implementation technique for lazy functional
                  languages is call-by-need, which ensures that an argument to a
                  function in any given call is evaluated at most once. A
                  significant problem with call-by-need is that it is difficult
                  -- even for compiler writers -- to predict the effects of
                  program transformations. The traditional theories for lazy
                  functional languages are based on call-by-name models, and
                  offer no help in determining which transformations do indeed
                  optimize a program.In this article we present an operational
                  theory for call-by-need, based upon an improvement ordering on
                  programs: M is improved by N if in all program-contexts C,
                  when C[M] terminates then C[N] terminates at least as
                  cheaply.We show that this improvement relation satisfies a
                  "context lemma", and supports a rich inequational theory,
                  subsuming the call-by-need lambda calculi of Ariola et al.
                  [AFM+95]. The reduction-based call-by-need calculi are
                  inadequate as a theory of lazy-program transformation since
                  they only permit transformations which speed up programs by at
                  most a constant factor (a claim we substantiate); we go beyond
                  the various reduction-based calculi for call-by-need by
                  providing powerful proof rules for recursion, including
                  syntactic continuity -- the basis of fixed-point-induction
                  style reasoning, and an improvement theorem, suitable for
                  arguing the correctness and safety of recursion-based program
                  transformations.},
  booktitle       = {Proceedings of the 26th ACM SIGPLAN-SIGACT Symposium on
                  Principles of Programming Languages},
  pages           = {43–56},
  numpages        = 14,
  location        = {San Antonio, Texas, USA},
  series          = {POPL '99}
}

@article{Howe.1996.HowesMethod,
  title           = {Proving Congruence of Bisimulation in Functional
                  Programming Languages},
  journal         = {Information and Computation},
  volume          = 124,
  number          = 2,
  pages           = {103-112},
  year            = 1996,
  issn            = {0890-5401},
  doi             = {https://doi.org/10.1006/inco.1996.0008},
  url             =
                  {https://www.sciencedirect.com/science/article/pii/S0890540196900085},
  author          = {Douglas J. Howe},
  abstract        = {We give a method for proving congruence of
                  bisimulation-like equivalences in functional programming
                  languages. The method applies to languages that can be
                  presented as a set of expressions together with an evaluation
                  relation. We use this method to show that some generalizations
                  of Abramsky's applicative bisimulation are congruences
                  whenever evaluation can be specified by a certain natural form
                  of structured operational semantics. One of the
                  generalizations handles nondeterminism and diverging
                  computations.}
}
