README
========================================

Spellchecking
++++++++++++++++++++++++++++++++++++++++

.. code:: bash spellcheck:
 	aspell -d es --mode=tex -c Filename.tex

.. code:: fish spellcheckall:
 	for file in tex/*.tex; aspell -d es --mode=tex -c $file; end

Notation
++++++++++++++++++++++++++++++++++++++++

+ Better, scott ord (\better*) :math:`\sqsupseteq`
+ Observational Approximation (\obsaprox*) :math:`\overset{\sqsubset}{\sim}`
+ Holes \hole* :math:`[-]`
+ Improvement (\improv*) :math:`\succ`
+ Nat (\Nat*) :math:`\mathbb{N}`
+ Tick (\tick*) :math:`\checkmark`


Commands:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

+ \note
+ substs

Environments:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

+ definition
+ theorem
+ lemma
+ notation
+ example
+ convention
+ mejora
+ proof

----------------------------------------

Tools?
++++++++++++++++++++++++++++++++++++++++

+ Memoir
+ Biber
+ latexmk


Wild Ideas
----------------------------------------

+ SAT/SMT

  Is it a good idea to use SAT/SMT solvers to solve the equations? Suppose I
  have two Programs/Ctxs to compare can I derive the equations somehow and throw
  them into some solver to say if its an improvement?

  This task seems an easy one... I've implemented a similar idea (I guess) when
  I was playing around with QC. I will leave it to when I do not want to do
  anything else... Or... It might be some student final project... :P

+ Galois Connections

  More crazy is to think about Galois Connections? I am sure there was a
  reference in some of the textbooks that I've read but I cannot find it.
  Suppose that I have an injection function :math:`into : V \to \mu \Sigma
  \emptyset` so I can compose it with the evaluation function :math:`eval : \mu
  \Sigma \emptyset \to V`, and the following properties must hold:

  .. Wild guess?
  .. math:
     \text{Here we should have something like that eval plus into is worse than nothing?}
     \forall t \in \mu \Sigma \emptyset, into \circ eval \sqsubset id

     eval \circ into = id_V

------------------------------------------------------------
I am going to leave this as is just coz of legacy.

* [ ] Write algebra monotonicity in a formal and generic way, since we are
  going to use it twice (or as many times necessary). We shall also justify
  why we do that, shan't we?.

* [ ] I need to define how to treat things like :math:`f \in \mu\Sigma X`. For
  example, there is a semantic value :math:`\limg \gamma , _ \rimg f : (X
  \to V) \to V`. Another choice would be something like :math:`f,g \in \mu
  \Sigma X, f \sqsubseteq g \trinagleq \forall (s : X \to \mu \Sigma
  \emptyset), \limg \gamma, s \rimg f \sqsubseteq \limg \gamma , s \rimg g`.
  But I do not know if I have the same questions on the cost-semantic side...

* [ ] Since there are three theoretical sections (Imp Theory, IAS, Domains), you
  can start there writing something down. There are going to be at least 2
  examples that may be connected. Think about them and how to connect them, the
  less examples the better.
