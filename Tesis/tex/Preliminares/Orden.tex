\section{Teoría de Orden}\label{sec:teoria.orden}

En esta sección se introducen los conceptos básicos de teoría de orden para
luego introducir teoría de dominios y la semántica denotacional en ellos.

Al momento de analizar el comportamiento de lenguajes de programación
funcionales nos encontramos con que éstos describen o declaran computaciones
centradas en \emph{qué} valor se espera.
%
Éste enfoque contrasta directamente con lenguajes no funcionales donde el
objetivo se centra en \emph{cómo} se obtienen dichos valores, a través de
accesos a memoria u otras formas.
%
Un enfoque natural es entonces asignarle a cada programa una función que
describa su comportamiento.
%
Debemos entonces sortear un problema que es el de la no terminación:
¿qué función denota un programa que no termina?
%
Nos dedicamos entonces a presentar primero una noción de aproximación al
comportamiento de programas y definir así la denotación de un programa como su
aproximación al infinito, en otras palabras, el límite de una cadena de
aproximaciones.

\begin{definition}[Orden Parcial]\label{def:orden.parcial}
  Un orden parcial~(\emph{poset}) sobre un conjunto \(D\) es una relación
binaria \((\poOrd{D}) \subseteq D \times D\) tal que cumple con las siguientes propiedades:
  \begin{description}
  \item[Reflexiva:] Para todo \(d \in D, d \poOrd{D} d\)
  \item[Transitiva:] Dados \(x,y,z \in D\), \(x \poOrd{D} y \land y
    \poOrd{D} z \implies x \poOrd{D} z\)
  \item[Anti-simétrica:] Dados \(x, y \in D\), \(x \poOrd{D} y \land y \poOrd{D}
    x \implies x = y\)
  \end{description}
\end{definition}

En particular, en el caso que la relación \((\poOrd{D})\) solo sea reflexiva y
transitiva, el par \((D, \poOrd{D})\) se llama \emph{preorden}.
%
En el caso que el orden esté totalmente definido en base al contexto, se
omitirá el subíndice de la relación.

\begin{definition}[Cadena y \(\omega\)-Cadena]\label{def:cadena}
Sea \((P, \poOrd{P})\) un orden parcial, un subconjunto \(X \subseteq P\) es
una cadena si para todo \(x,y \in X\),
\(x \poOrd{P} y\) o \(y \poOrd{P} x\).
%
Una \(\omega\)-cadena es una secuencia \emph{infinita} \(\{x_n\}_{n < \omega} \)
de elementos en \(P\), tales que, \(x_i \poOrd{P} x_j\) con \(i \leq j\).
\end{definition}

Definimos a una función \(f : (P, \poOrd{P}) \to (Q, \poOrd{Q})\) entre los
órdenes parciales \((P, \poOrd{P}) \text{ y }(Q, \poOrd{Q}) \) como una función
entre los conjuntos de cada uno de ellos.

\begin{definition}[Monotonía]
  Sean \((P_1, \poOrd{P_1})\) y \((P_2, \poOrd{P_2})\) dos órdenes parciales.
  %
Una función \(f : P_1 -> P_2\) es \emph{monótona} si y sólo si para todo par de
valores \(x,y \in P_{1}\) tales que \( x \poOrd{P_{1}} y\), entonces
\(f(x) \poOrd{P_2} f(y)\).
  \end{definition}

\begin{definition}[Conjunto Dirigido]
  Sea \((A, \poOrd{A} )\) un poset.
  %
  Un subconjunto \(X \subseteq A\) es llamado dirigido si y sólo si todo
  subconjunto \emph{finito} \(X_0 \subseteq X\) tiene una cota superior en
  \(X\).
  %
  % \[
  %   \forall X_0 \subseteq X, \exists y \in X, \forall x \in X_0, x \poOrd{A} y
  % \]
  \end{definition}

De la definición se desprende que un conjunto dirigido no puede ser vacío.
%
Dado un conjunto dirigido \(X\), tenemos que el conjunto vacío está contenido en
\(X\), y por lo tanto la cota superior del conjunto vacío debe estar en \(X\).
% Ya que el conjunto vacío está siempre contenido en cualquier conjunto,
% Por lo que, un conjunto dirigido \(X\) no puede ser vacío.
%
% Ya que \(\emptyset \subseteq X \) para cualquiera sea \(X\), y \(X\) al ser dirigido
% tiene que existir un \(y \in X\).

Un orden parcial es llamado \emph{predominio} u \emph{orden parcial
completo}~(CPO) si y sólo si todo conjunto dirigido tiene un supremo.
%
El supremo de un conjunto dirigido \(X\) lo notamos como \((\lub X)\).
%
Además, un predominio es llamado \emph{dominio} u \emph{orden parcial completo
con punta}~(PCPO) si y sólo si tiene un elemento mínimo, notado generalmente con
\(\bot\).

\begin{definition}[Función Continua]
  Sean  \(\ADom{1}, \ADom{2}\) dos predominios.
  %
  Una función \(f : \ADom{1} -> \ADom{2}\) es continua si y sólo si preserva los
  supremos de los conjuntos dirigidos:
\[
  f (\lub X) = \lub \{ f(x) \mid x \in X\}
\]
%
Para todo conjunto dirigido \(X \subseteq A_1\).
  \end{definition}

A medida que avancemos en la interpretación de lenguajes de programación
funcionales la noción de continuidad será \textbf{fundamental}.
%
Las funciones continuas son aquellas que mantienen la estructura y dan sentido a
transformaciones entre programas manipulando directamente los supremos pero
preservando la estructura.
%
Recordar que denotaremos a los programas con el límite de su cadena de
aproximaciones, por lo que, funciones continuas representan manipulaciones de
programas.

Nos dedicamos entonces a introducir las herramientas necesarias para poder
denotar términos del cálculo lambda mediante el uso de dominios.

\subsection{Productos}

El resultado de realizar el producto de predominios es un predominio y sus
funciones de proyección son continuas.

\begin{theorem}
Dada una familia de predominios \((A_i \, | \, i \in I)\).
%
Su producto \(\prod_{i \in I} A_i\) es un predominio bajo el orden componente a
componente y las funciones de proyección \(\pi_i : \prod_{i \in I} A_i -> A_i\) son
continuas.
%
Más aún, si todos los predominios \(A_i\) son dominios, también lo
es \(\prod_{i \in I} A_i\).
%
Sea \((f_i : B -> A_i \, | \, i \in I)\) una familia de funciones continuas,
entonces existe una única función continua \(f : B -> \prod_{i \in I} A_i\)
tal que: \[ \pi_{i} \circ f = f_i \]
\end{theorem}

\begin{lemma}\label{lemma:argcont}
  Sean \(A_1,A_2\) y \(A_3\) predominios. Entonces, una función \(f : A_1 \times
  A_2 -> A_3\) es continua si y sólo si lo es en ambos argumentos.
  \end{lemma}

\subsection{Exponenciales}

Finalmente, definimos los exponenciales para poder interpretar las abstracciones
de cálculo lambda dentro de los dominios.
%
En otras palabras, tenemos que garantizar que los espacios de funciones o
\emph{exponenciales} estén bien definidos.

\begin{theorem}
Sean \(A_1\) y \(A_2\) dos predominios.
%
El conjunto \(A_2^{A_1} = [A_1 -> A_2]\) definido por todas las funciones
\emph{continuas} de \(A_1\) a \(A_2\) es un predominio ordenado de forma
\emph{extensiva}.

Sean \(f,g : A_1 -> A_2\) funciones continuas:
\[
  f \sqsubseteq g \iff \forall a \in A_1, f(a) \poOrd{A_2} g(a)
\]
  \end{theorem}

Definimos entonces la función de aplicación de una función en un argumento.
%
Dada una función continua entre dos predominios \(f : A_{1} -> A_{2}\), y un
elemento de \(a \in A_{1}\), definimos la función \(ev(f,a) = f(a)\).
%
Podemos ver que \(ev\) es continua en sus argumentos, y por
Lema~\ref{lemma:argcont}, la función \(ev\) es continua.

% \martin{Idk why I wrote this}

Finalmente, vemos que podemos conectar el producto con funciones continuas de la
siguiente forma.

\begin{theorem}
  Sean \(A_1, A_2\) y \(A_3\) predominios.
  %
  Para toda función continua \(f : A_1 \times A_2 -> A_3\) existe una única función
continua \(g : A_1 -> [A_2 -> A_3]\) tal que: \[ f(x,y) = g(x)(y) \]
  \end{theorem}

\subsection{Puntos Fijos}

En esta sección exploramos la noción de punto fijo, que es la que utilizamos
para definir funciones recursivas.
%
Durante el resto de la sección, asumimos que \(D\) es un dominio.

\begin{theorem}
  Sea \(f : [D -> D]\) una función continua.
  %
  Entonces, existe el supremo de la cadena \(\{f^{n}(\bot)\}_{n < \omega}\):
  \[ \mu(f) = \lub_{n \in \Nat*} f^n(\bot) \]
  %
  y además satisface las siguiente dos condiciones:
  \begin{itemize}
    \item \(\mu(f) = f(\mu(f))\)
    \item \(\mu(f) \sqsubseteq d\), cuando \(f(d) \sqsubseteq d\)
  \end{itemize}
  %
  En particular, \(\mu(f)\) es el menor punto fijo de \(f\).
  \end{theorem}

Necesitamos que \(D\) sea un dominio para comenzar la cadena desde \(f(\bot)\).
%
Además, es fácil ver que \(\{f^{n}(\bot)\}_{n < \omega}\) es una cadena por
continuidad de \(f\).
% Notar la necesidad de que sea un domino, y no un predominio.

Por el teorema anterior, podemos ver que existe una función \(\mu\) de
\([D -> D]\) en \(D\) que toma funciones continuas \(f\) y las envía a su punto
fijo.
%
Se podría probar que \(\mu\) es continua.
%
Pero vamos a hacer un prueba un poco más general.

\begin{theorem}
  Sea \(\phi : [ [D -> D] -> D] -> [ [D -> D] -> D]\) el operador continuo
  definido como:
  \[
    \phi(F)(f) = f(F(f))
  \]
  con \(F : [ [D -> D] -> D]\) y \(f : [D -> D]\).

  Los puntos fijos de \(\phi\) son entonces los operadores continuos de punto
  fijo en \(D\), por lo que \(\mu\) es el menor punto fijo de \(\phi\).
  \end{theorem}

Como resultado obtenemos además que \(\mu\) es continuo.

Finalmente, cerramos el capítulo con un resultado general del álgebra.

\begin{theorem}[Teorema Knaster-Tarski]\label{thm:knaster:tarski}
  Dado un retículo completo \(L\) y una función que preserve el orden
\(f : L -> L\), el conjunto de puntos fijos de \(f\) en \(L\) es a su vez un
retículo completo.
\end{theorem}

El teorema de Knaster-Tarski nos presenta con un ínfimo y un supremo en el
conjunto de puntos fijos de una función.
%
Esto nos presenta entonces un principio de inducción y un principio de
coinducción.
%
El ínfimo de los puntos fijo es el menor de los pre-puntos fijos:
\[
  \<lfp>(f) = \bigcap \{ x \in L \mid f(x) \leq x\}
\]
Mientras que el supremo de los puntos fijo es el mayor de los post-puntos fijos:
\[
  \<gfp>(f) = \bigcup  \{ x \in L \mid x \leq f(x) \}
\]

%%% Local Variables:
%%% TeX-master: "../../Thesis.tex"
%%% TeX-PDF-mode: t
%%% ispell-local-dictionary: "es"
%%% End:
