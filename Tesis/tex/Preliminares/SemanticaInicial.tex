\section{Semántica Inicial}

La semántica inicial busca equipar con una noción de significado a estructuras
mediante el uso del objeto inicial.
%
La idea principal del enfoque es que mediante la propiedad de inicialidad
podemos caracterizar fácilmente la sintaxis de los lenguajes.
%
Más aún, podemos definir a las funciones de evaluación de términos en dominios
semánticos que \emph{respeten} los constructores del lenguaje.

El árbol de sintaxis abstracto de un término es simplemente una construcción
detallada de los diferentes operadores del lenguaje y sus términos.
%
Dicho árbol solo nos presenta con un forma de estructurar un término donde un
constructor utiliza otros términos del lenguaje, pero no tiene ningún
significado.
%
Por ejemplo, dada una expresión sintáctica como \(1 + 2\), podemos representarla
como un árbol con un nodo y dos hojas:
\[
\begin{array}{c}
\begin{tikzcd}
  & + \arrow[dl] \arrow[dr] & \\
  1 & & 2 \\
\end{tikzcd}
  \end{array}
\]

De esta manera, para obtener una función de evaluación del árbol sintáctico de
un término en un dominio semántico \(D\), basta con dar una función de
interpretación o evaluación de cada operador del lenguaje en el dominio \(D\).
%
En otras palabras, podemos recorrer el árbol de sintaxis abstracto, a cada nodo
representando un operador del lenguaje lo interpretamos con su función de
evaluación, y hacemos lo mismo con sus argumentos, de forma recursiva.

En esta sección, repasamos los conceptos que utilizamos al momento de
definir los operadores algebraicos y cómo utilizar los árboles de sintaxis
abstractos para definir la noción de contexto.
%
Finalmente, vemos como obtener funciones de costos analizando los
árboles de sintaxis abstracta como una primer aproximación a un análisis de
costos.

Dado un conjunto \(A\) notamos como \(A^{*}\) a el conjunto resultante de
multiplicar \(A\)  consigo mismo una cantidad finita de veces, \(A \times \ldots \times A\),
pudiendo incluso ser una cantidad nula \(A^{0} = \emptyset\).
%
Además, utilizamos a \(\lambda\) para notar a la cadena vacía.

\paragraph*{Álgebra Inicial}
%
Dentro de una categoría \(C\), intuitivamente, un álgebra \(S\) es inicial en
una clase \(\mathbb{A}\) de álgebras si y solo si para toda \(A \in \mathbb{A}\)
existe un único homomorfismo \({h}_{A} : S -> A \).
%
El árbol de sintaxis abstracto define \emph{un} álgebra inicial, pero como
veremos, eso en realidad no es importante ya que las álgebras iniciales son
isomorfas, por lo que con caracterizar una nos es suficiente.

Dentro de los lenguajes de programación tiene sentido de hablar de
\emph{many-sorted} álgebras, ya que los lenguajes de programación suelen ser
poblados de elementos básicos como \emph{enteros, booleanos, cadenas de
caracteres, etc},
un álgebra multi-sorted consiste entonces en una familia indexada de conjuntos,
llamados \emph{portadores} y una familia de operadores
definidos en el producto cartesiano de esos conjuntos.
%
Por ejemplo, un álgebra \(A\) con los tres tipos básicos antes mencionados
tendría como conjuntos portadores a \(\{A_{int},A_{bool},A_{string}\}\) con
operaciones entre ellos como ser: \( (\to_{string}) : A_{bool} -> A_{string} ->
A_{string} -> A_{string}\), \((\times) : A_{int} -> A_{int} -> A_{int}\), \( (=_{int}) : A_{int}
-> A_{int} -> A_{bool}\), que se pueden interpretar como el condicional en
\emph{strings}, la multiplicación e igualdad de enteros respectivamente.

\begin{definition}[Signatura \(\Sigma\)]\label{def:signatura}
  Sea \(S\) un conjunto cuyos elementos llamaremos \emph{sorts}.
  %
  Una signatura \(\Sigma\) es una familia de conjuntos disjuntos \(\langle
  \Sigma_{w,s} \rangle\) indexados por \(S^{*} \times S\).
  %
  El conjunto \(\Sigma_{w,s}\) contiene los símbolos de los operadores cuyo tipo es
\(\langle w , s \rangle\), aridad \(w\), sort \(s\), y rango longitud de \(w\).
  \end{definition}

\begin{definition}[\(\Sigma\)-Álgebra]
  Una \(\Sigma\)-álgebra indexada por sorts \(S\), \(A\), consiste entonces en una
familia de conjuntos \(\langle A_s \rangle_{s \in S}\) llamados portadores de \(A\), con
\(A_s\) siendo el portador del sort \(s \in S\).
  %
  Donde para cada \(\langle w,s \rangle \in S^{*} \times S\) y para cada \(\sigma
  \in \Sigma_{w,s}\), define una operación \(\sigma_{A}\) de tipo \(\langle w, s
  \rangle\), es decir, \(\sigma_{A} : A_{w_1} \times \cdots \times A_{w_n} ->
  A_{s}\) con \(w = w_1 \cdots w_n\) y \(w_i \in S\) con \(i \in \{ 1, \ldots, n\}\).
  \end{definition}

Una operación \(\sigma_A\) de tipo \(\langle \lambda, s \rangle\) es una constante de
sort \(s\), es decir, \(\sigma_A \in A_{s}\).

Definimos entonces los homomorfismos entre álgebras como familia de
homomorfismos entre los conjuntos portadores respetando la estructura de las
álgebras.

\begin{definition}[\(\Sigma\)-Álgebra Homomorfismo]
  Sean \(A, A'\) dos \(\Sigma\)-álgebras.
  %
  Un homomorfismo \(h : A -> A'\) es una familia de funciones
  \(\langle h_s : A_s -> {A'}_s \rangle_{s \in S}\) tales que
  respetan los operadores:
  \begin{itemize}
    \item Para cada constante \(\sigma \in \Sigma_{\lambda, s}\), tenemos que \(h_s(\sigma_{A}) = \sigma_{A'}\)
    \item Para cada operador \(\sigma \in \Sigma_{w_1 \ldots w_n, s}\) y elementos \(\langle a_1
      \cdots a_n\rangle \in A_{s_1} \times \cdots A_{s_n} \), tenemos que
      \(h_s(\sigma_A(a_1, \ldots, a_n)) = \sigma_{A'}(h_{s_1}(a_1), \ldots,
      h_{s_n}(a_n))\)
    \end{itemize}
  \end{definition}

Generalizar esto a álgebras con \(S\) sorts es sencillo, aunque para esto
primero definimos notación para simplificar la escritura.
\begin{description}
  \item[Familia Indexada] Ahora \(A\) es una familia de conjuntos indexada por
    \(S\), \(A = \langle A_s \rangle_{s \in S}\), y con \(w = s_1 \ldots s_n \in
    S^{*}\), podemos generalizar \(A^{n}\) como \(A^{w}\) de forma tal que
    \(A^{w} = A_{s_1} \times \ldots A_{s_n}\).
  \item[Familia de Funciones Indexadas] De forma similar podemos extrapolar una familia de
    funciones \(h : A -> A', \langle h_s : A_s -> {A'}_s \rangle_{s \in S}\), y
    definir, una función \(h^w : A^w -> {A'}^w\) con
    \(h^w(a_1, \ldots, a_n)= \langle h_{s_1}(a_1), \ldots, h_{s_n}(a_{s_n})\rangle\)
\end{description}

Para el caso especial de familias indexadas con \(w = \lambda, (n = 0)\) se
interpreta como \(A^{\lambda} = A^0 = \{\lambda\}\), y una función
\(\sigma_{A}\) con dominio \(A^{\lambda}\) la identificamos con una constante
cuya valor es \(\sigma_{A^{\lambda}}\).

Ahora entonces podemos definir homomorfismo entre álgebras multi-sorted.

\begin{definition}[\(S\)-sorted Álgebra]
  Dado un sort \(S\), una \(\Sigma\)-álgebra \(A\) con sort \(S\) consiste en:
  \begin{itemize}
    \item una familia indexada en \(S\) de conjuntos \(A\), denominada portador
    \item un mapa que a cada símbolo \(\sigma \in \Sigma_{w,s}\) le asigna una función \(\sigma_{A} : A^{w} -> A_{s}\)
  \end{itemize}
\end{definition}

\begin{definition}[\(S\)-sorted álgebra homomorfismo]
  Un homomorfismos \(h\) de \(A\) a \(B\) consiste en una familia de funciones
indexadas en \(S\), \(\langle h_s : A_s -> B_s \rangle_{s \in S} \), de forma tal que para
todo símbolo \(\sigma \in \Sigma_{w,s}\) con aridad \(w \in S^{*}, s \in S\) tenemos que:
  \(h_s(\sigma_{A}(a)) = \sigma_{B}(h^{w}(a))\).
  \end{definition}

En otras palabras que el siguiente diagrama conmute.
  \[
  \begin{tikzcd}
    {A^{w}} \arrow[r, "\sigma_{A}"] \arrow[d,"h^{w}"'] & {A_{s}} \arrow[d,"h_{s}"] \\
    {B^{w}} \arrow[r,"\sigma_{B}"'] & {B_{s}} \\
    \end{tikzcd}
  \]

Lo interesante de este enfoque, dado que utilizamos teoría de conjuntos, es
que nos permite construir el árbol de sintaxis abstracta y una función para
consumirlo~\footnote{Es posible obtener el mismo resultado utilizando otras
teorías, pero para esta tesis basta con tener conjuntos.}.
%
\begin{lemma}[Existencia de Álgebra Inicial]
  La clase de \(\Sigma\)-álgebras con \(\Sigma\)-álgebra homomorfismos tiene
  una \(\Sigma\)-álgebra inicial denominada \(T_{\Sigma}\).
  \end{lemma}
%
Obviaremos la demostración en esta tesis, ya que es un resultado conocido del
área y se pueden encontrar en libros de álgebras
universales~\parencite{Bergman.2011.UniversalAlgebra}.

El conjunto \(T_{\Sigma,s}\), el elemento portador de sort \(S\), lo podemos
interpretar como el conjunto de expresiones bien formadas, o más concretamente
como árboles de sintaxis abstractos (AST), utilizando los operadores \(\sigma \in \Sigma\).
%
% Además, como mencionamos antes, no nos interesa exactamente cual es el álgebra
% inicial, sino los elementos con la propiedad de inicialidad, ya que son todos
% isomorfos.

Podemos introducir la noción de álgebra libre de \(\Sigma\)-álgebras  y así obtener
una definición de operadores derivados.
% para obtener una
% noción concreta de lo que sería un operador derivado.

\begin{definition}[\(\Sigma\)-álgebra libre]\label{def:free:algebra}
  Sea \(X\) una familia indexada en \(S\) disjunto respecto a \(\Sigma\), cuyos
  valores llamaremos \emph{variables}.
  %
  Cada conjunto indexado \(X_{s}, s \in S\) representa variables de sort \(s\).

  Podemos entonces formar una jerarquía de conjuntos de la siguiente forma:
  para cada \(s \in S\), \({\Sigma_{\lambda , s}(X)}_{0} = \Sigma_{s} \cup X_{s}\);
  para cada \(k > 0\), \({\Sigma_{w, s}(X)}_{k} = \Sigma_{w, s}\).
  \end{definition}

Podemos ver que \(T_{\Sigma(X)}\) es el álgebra inicial de \(\Sigma(X)\).
%
La principal diferencia es que los árboles en el portador pueden tener variables
en las hojas además de las constantes que teníamos originalmente.
%
El álgebra \(T_{\Sigma(X)}\) tiene como signatura \(\Sigma(X)\) pero lo que queremos es
obtener una \(\Sigma\)-álgebra, y los podemos lograr definiendo una nueva
\(\Sigma\)-álgebra llamada \(T_{\Sigma}(X)\) con portador \(T_{\Sigma(X)}\) y símbolos de
operaciones definidos en \(\Sigma\).

% Podemos entonces definir una álgebra libre en el sentido de:
\begin{definition}[\(\Sigma\)-álgebra libre generada]
  Sea \(h : X -> A\) una familia de funciones indexada en \(S\) tal que
\(h_s : X_s -> A_s\), entonces existe un único \(\Sigma\)-homomorfismo
\(\overline{h} : T_{\Sigma}(X)  \to A\)
  \end{definition}

\section[Semántica Inicial Cálculo Lambda]{Semántica Inicial para Términos del Cálculo Lambda}

Veamos como introducir semántica de álgebra inicial para un lenguaje mínimo con
variables y expresiones ``let--in''.
%
Dado un conjunto numerable de variables \(X\), tenemos dos categorías
sintácticas:
\begin{itemize}
  \item Conjunto de Valores: \(V,W \mathrel{::=} x \mid (\lAbs{x}{M}) \)
  \item Conjunto de Términos:
        \(M,N \mathrel{::=} \lRet{V}
        \mid (\lApp{V}{W})
        \mid \letIn{x}{M}{N}\)
 \end{itemize}

Entonces tenemos que para el conjunto de valores podemos definir la signatura
\[
  \Sigma^{V}_{w,s} =
  \begin{dcases}
    {var_{x} \mid x \in X} & \text{si } w,s = \lambda, V \\
    \{\lambda\} & \text{si } w,s = \langle X, T \rangle, V\\
    \emptyset & \text{sino}
    \end{dcases}
\]
% \(\Sigma^{V}_{\lambda,V} = X
% , \Sigma^{V}_{\langle X V \rangle,V} = \{\lambda\}
% , \Sigma^{V}_{w,s} = \emptyset\).
%
Donde \(T\) es el conjunto de términos definido por la siguiente signatura.

En palabras, tenemos que \(\Sigma^{V}\) representa el conjunto de valores que
contiene: las constantes, un elemento por cada elemento de \(X\), y un constructor
que toma dos argumentos, una variable y un término, retornando un valor que
representa la abstracción.

Para el conjunto de términos tenemos la siguiente signatura:
\[
  \Sigma^{T}_{w,s} =
  \begin{dcases}
    \{ret\} & \text{si } w,s = \langle V \rangle , T \\
    \{app\} & \text{si } w,s = \langle V, V \rangle, T \\
    \{let_{x} \mid x \in X \} & \text{si } w,s = \langle T, T\rangle, T\\
    \emptyset & \text{sino}
    \end{dcases}
\]
%
Tenemos entonces que la signatura \(\Sigma^{T}\) representa tres constructores: uno
para retornar valores, otro para construir la aplicación de un valor a un
término, y finalmente la construcción del operador de substitución explícita.

El lector habrá notado una cierta circularidad en la definición de las dos
categorías sintácticas.
%
Los términos dependen de los valores y los valores de los términos.
%
La circularidad es fácilmente evitable definiendo un tipo inductivo de los
términos, utilizando álgebras iniciales.


Podemos entonces construir las álgebras iniciales de ambas signaturas,
\(T_{\Sigma^{V}},T_{\Sigma^{T}}\), y de esta manera definir funciones que toman
elementos sintácticos del lenguaje.
% que utilizamos de ejemplo en algún dominio
% semántico donde son interpretados dichos elementos.
%
Veamos entonces como podemos darle semántica a los términos utilizando el
siguiente dominio:
\(D_{V} \cong I + [ D_{V} \to D_{V}]\)~\parencite{Goguen.IAS,Scott.DataTypesAsLattices},
con \(I\) siendo el conjunto de los números enteros\footnote{En nuestro caso
tendría más sentido diferenciar el dominio semántico de los valores y términos.
Esto es debido a que la evaluación de términos puede divergir. Por simplicidad
en esta sección hacemos caso omiso éste planteo, aunque lo retomamos en las
siguientes secciones.}
% primero el caso que se encuentra en la
% bibliografíadonde sabemos que
% existe un dominio:
%
Debido a la presencia de variables en el lenguaje llevaremos un entorno de
variables definido simplemente como un mapa entre variables y valores: \(E = D_{V}^{X}\).
% es decir, el conjunto de funciones de
% variables a valores.
%
Definimos entonces el dominio semántico de los términos y valores como
\(D \equiv [ E \to D_{V} ]\), es decir, es el dominio de funciones continuas de
entornos a \(D_{V}\).

De la definición de \(D_{V}\) tenemos las siguientes funciones de inyección:
\begin{align*}
  inj_{I} &: I \to D_{V}\\
  inj_{D_{V} \to D_{V}} &: [D_{V} \to D_{V}] \to D_{V}
\end{align*}

Veamos primero como construir un álgebra para los valores en el dominio semántico
\(D\) presentando la interpretación de las variables y las abstracciones.
%
En el caso de las variables, simplemente buscaremos su valor en el entorno,
mientras que en el caso de las abstracciones utilizaremos una función que nos permite mapear
funciones sintácticas en semánticas.
%
Por ejemplo, mapear la función ``\( \lambda_{x} var_{x}\)'' a la función matemática
identidad.
%
\begin{align*}
  x_{D} &= access_{x} \\
  \lambda_{D}(x,m) &= inj_{D \to D} \circ abstract ( m \circ assign_{x} )
  \end{align*}

Donde definimos las siguientes funciones auxiliares:
\[
  \begin{array}{lcl}
  access_{x} &:& E \to D_{V} \\
  access_{x}(e) &=& e(x)\\
  assign_{x} &:& E \times D_{V} \to E\\
  assign_{x}(e, v) &=& \lambda y \to
                     \begin{dcases}
                       v & \text{si } y = x \\
                       e(y) & \text{sino}
                     \end{dcases}\\
  abstract &:& (D_{1} \times D_{2} \to D_{3}) \to (D_{1} \to (D_{2} \to D_{3}))\\
  abstract(f)(x)(y) &=& f(x,y)
    \end{array}
\]

Para interpretar los términos definimos funciones interpretando cada uno de los
constructores del lenguaje:
%
\begin{align*}
  ret_{D}(v) &= v\\
  app_{D}(v, t) &= ap \circ [v , t] \\
  let_{D}(x, m, t) &= t \circ assign_{x} \circ [ m , 1_{e} ] \\
  \end{align*}

Utilizando la función auxiliar:
\begin{align*}
  ap &: D_{V} \times D_{V} \to D_{V}\\
  ap(f,x) &= \pi_{[D_{V} \to D_{V}]}(f)(x) \\
  % rec &: [D_{V} \to D_{V}] \to D_{V} \\
  % rec &= Y
  \end{align*}

Ahora que tenemos un álgebra que da semántica a la signatura \((\Sigma^{V}, \Sigma^{T})\),
por inicialidad de \(T_{\Sigma^{V}},T_{\Sigma^{T}}\) sabemos que existe un único
\(\Sigma\)-álgebra homomorfismo \(h_{V} : T_{\Sigma^{V}} \to D, h_{T} : T_{\Sigma^{T}} \to D\) tal que
los diagramas conmutan.
%
Notar que en este caso, tal que los diagramas conmutan, nos da la garantía que
las
funciones de interpretación son mutuamente recursivas y que la interpretación de
los términos utiliza la interpretación de sus subtérminos.
%
De esta manera llevamos términos y valores a un mismo dominio semántico, \(D_{V}\).
% aunque
% surgen otras preguntas, por ejemplo, podríamos definir funciones que nos
% permitan evaluar un término \(t \in T_{\Sigma^{T}}\) en un valor \(v \in T_{\Sigma^{V}}\)?

Lo que nos permite definir la equivalencia de programas como:
%
\begin{definition}[Equivalencia (Semántica) de Programas]
  Sean \(p_{1},p_{2} \in T_{\Sigma^{T}}\), definimos a \(p_{1} \equiv p_{2}\) si y solo si
la interpretación de \(p_{1}\) es igual a la interpretación de \(p_{2}\) en el
dominio semántico \(D_{V}\).
  % \([[ p_{1} ]] =_{D} [[ p_{2} ]]\).
  %
  \end{definition}

En palabras, dos términos son equivalentes si sus interpretaciones en el dominio
semántico son iguales.
%
Por ejemplo, construcciones sintácticas diferentes como
\((\lambda_{x}. var_{x}), (\lambda_{y}. var_{y}) \in T_{\Sigma^{V}}\) son mapeadas a la misma
función, en este caso la identidad.
%
% Notar que esto lo podemos hacer siempre que tengamos una forma de mapear
% estructuras sintácticas en valores interpretaciones semánticas.
%
Estamos simplemente moviendo la discusión de un dominio a otro, lo interesante
es que haciendo esto, podemos comparar programas no por su construcción
sintáctica sino por su \emph{interpretación semántica}.
%
En este caso, los valores cerrados de nuestro lenguaje son interpretados en
funciones matemáticas continuas, y estás pueden ser comparadas siguiendo una
noción de equivalencia como ser la de \emph{extensionalidad}, es decir, dos
funciones son equivalentes si aplicadas al mismo argumento se obtiene la misma
respuesta.

El objetivo de este trabajo es distinguir propiedades intensivas sobre la
ejecución de programas, por lo que si quisiéramos utilizar este enfoque
deberíamos equipar las interpretaciones semánticas con información acerca de su
evaluación.
%
Es decir, queremos distinguir propiedades de cómo se realiza la evaluación de
términos, y más aún, nuestro objetivo es construir una relación entre términos
no una noción absoluta de costos.
%
Veamos primero cómo obtener información a través del árbol sintáctico.

\section{Costos como Álgebras}

Como primer aproximación podemos introducir una noción de costo de
evaluación dentro de la teoría del álgebra semántica inicial.
%
En otras palabras, deducir el costo de un programa a partir del proceso de
interpretación de un término sintáctico en su denotación.
%
Para esto podemos pensar que el análisis de costo de la evaluación es totalmente
independiente de los valores que son computados y sólo dependen de la
construcción sintáctica de los términos, en otras palabras, dada una signatura
\(\Sigma\): \(Cost \in Alg_{\Sigma}\).

Un álgebra de costos es simplemente una forma de asignarle costos a los términos
del lenguaje y hay diferentes formas de asignar costos, por ejemplo, computando
el tamaño del árbol sintáctico o contando las veces que aparece cierto
constructor.
% nociones más cercanas a la evaluación de
% términos.
%
Nuestro interés está en analizar propiedades intensivas de la evaluación de
términos por lo que nos concentraremos en caracterizar álgebras que expongan
propiedades de la evaluación, aunque lamentablemente, estas propiedades no son
fáciles de estudiar por sí solas, y no vamos a caracterizar completamente dichas
álgebras de costos en esta sección, sino que nos concentraremos en definirlas
cuando tengamos la posibilidad de \emph{observar} la evaluación de los términos.

% \begin{definition}[Álgebra Costos]
%   Sea \(\Sigma\) una signatura.
%   %
%   Diremos que un álgebra \(\mathbb{N} \in Alg_{\Sigma}\) es monótona cuando,
%   \begin{itemize}
%   \item \(\forall \sigma \in \Sigma_{\lambda, s}, \sigma_{\mathbb{N}} > 0 \)
%   \item \(\forall \sigma \in \Sigma_{w, s}, t_1 \ldots t_n \in {\mathbb{N}}^{|w|}, \sigma_{\mathbb{N}}(t_{1} \ldots t_{n}) > t_i, i \leq n \)
%     \end{itemize}
%   \end{definition}

% Pero esto todavía no se ajusta correctamente a lo que queremos estudiar
% nosotros, si bien el costo crece a medida que vamos evaluando los términos,
% todavía no tenemos ninguna garantía que los costos de las computaciones que se
% utilizan se agreguen al costo final.
% %
% Un ejemplo concreto es que si durante la evaluación de una expresión utilizamos
% todos sus argumentos pero solo contamos uno de todos ellos.

% En álgebras de costos podemos definir algunas álgebras de costos \emph{razonables}.
%

Veamos de todas maneras como podemos trabajar una noción de costo sintácticas
utilizando la maquinaria definida hasta el momento.

\begin{definition}[Altura del árbol sintáctico]
  Sea \(\Sigma\) una signatura.
  %
  Definimos el costo de un término como la altura sintáctica del mismo.
  %
  \begin{itemize}
  \item \(\forall \sigma \in \Sigma_{\lambda, s}, \sigma_{\mathbb{N}} = 1\)
  \item \(\displaystyle \forall \sigma \in \Sigma_{w, s}, t_1 \ldots t_n \in {\mathbb{N}}^{|w|},
    \sigma_{\mathbb{N}} = \max_{i \in n} t_i + 1\)
    \end{itemize}
  \end{definition}
  %
% En este caso la noción de costo es simplemente la altura del árbol sintáctico.
%

\begin{definition}[Cantidad de constantes utilizadas en el árbol sintáctico]
  Sea \(\Sigma\) una signatura.
  %
  Definimos el costo de un término como la cantidad de constantes que aparecen en su árbol sintáctico.
  \begin{itemize}
  \item \(\forall \sigma \in \Sigma_{\lambda, s}, \sigma_{\mathbb{N}} = 1\)
  \item \(\displaystyle \forall \sigma \in \Sigma_{w, s}, t_1 \ldots t_n \in {\mathbb{N}}^{|w|},
    \sigma_{\mathbb{N}} = \sum_{i \in n} t_i\)
    \end{itemize}

  \end{definition}
  %
% Mientras que en esta noción contamos la cantidad de constantes del árbol.

Ambas nociones de costos si bien válidas o \emph{razonables}, no se
ajustan a lo que consideraríamos una noción real del costo de la evaluación de
términos, de hecho no tienen relación alguna con la evaluación de los términos.
%
Todavía no hay una conexión real entre el cómputo de los costos de la evaluación
y el álgebra de costos.
%
Las nociones definidas hasta el momento son nociones estáticas, resultado de
\emph{recorrer} y analizar el árbol sintáctico de un término, mientras que
el análisis de la evaluación de términos es de carácter dinámico.

Este enfoque tiene el problema que el costo no tiene conexión alguna con la
evaluación de los términos, sino que simplemente extraen información del árbol
sintáctico.
%
Por ejemplo, podríamos describir sintácticamente un valor con un árbol
sintáctico muy grande, cuya evaluación no sería costosa, mientras que un término
cuya evaluación podría ser costosa con un árbol sintáctico pequeño.

% \begin{itemize}
%  \item el costo no tiene ninguna conexión con la evaluación real de los términos
%  \item homomorfismos de álgebras puede incluso romper con la noción de orden establecida.\martin{explain this a bit more? or at all?}
% \end{itemize}

Peor aún si llevamos la idea más básica de costos al dominio semántico antes
presentado estaremos en problemas, ya que no hay una noción clara para acumular
los costos de los valores.
%
En otras palabras, no es fácil definir el dominio semántico de nuestros
programas, recordemos que asumíamos el dominio de Scott:
\[
  D \equiv I + [D \to D]
\]
%
Pero por ejemplo, no está tan claro que exista un dominio tal que a cada valor
se le pueda asignar el valor que se utilizó al computarlo, en principio porque
depende del término de partida, es decir, depende del contexto.
%
Por ejemplo:
\[
  D \times \mathbb{N} \equiv I \times \mathbb{N} + [ D \to D ] \times \mathbb{N}
\]
%
Pero los elementos de \( [D \to D] \times \mathbb{N}\) no terminan de caracterizar el
costo requerido para computar un valor, en el sentido que se acumula el costo
utilizado para computarlo pero dependiendo del contexto también un valor puede
ser utilizado para continuar computando, y por ende, tendría más sentido en
pensar que son funciones de valores con costos en valores con costos.

Finalmente, nuestro objetivo no es establecer el costo de evaluar un término,
sino que queremos saber cómo es posible mejorarlo, y para esto, queremos obtener
una relación entre términos.
%
Para esto definiremos relaciones entre álgebras en la siguiente sección.

\section{Congruencia y Relaciones entre Álgebras}

Las preguntas de esta sección son: ¿qué podemos \emph{observar} de relaciones sobre
los términos sintácticos?, ¿es posible construir una relación entre
términos que observe su evaluación y relacione aquellos términos que presentan
una mejora?

La noción de congruencia está definida como \emph{relación algebraica} entre
términos construidos a partir de la signatura \(\Sigma\).

\begin{definition}[\(\Sigma\)-congruencia]
  Sea \(\Sigma\) una signatura y \(A,B\) dos \(\Sigma\)-álgebras.
  % una \(\Sigma\)-congruencia entre álgebras
  Definimos una \(\Sigma\)-congruencia entre \(A\) y \(B\) como una familia de
relaciones \(\langle R_{s} \subseteq A_{s} \times B_{s}\rangle_{s \in S}\) tal que se puede extender a una
sub-álgebra del producto de \(A \times B\).
%
Es decir, la familia de relaciones \(\langle R_{s} \rangle_{s \in S} \in \textit{Alg}_{\Sigma}\) define una
\(\Sigma\)-álgebra tal que las proyecciones
\(\pi_{s,A} : R_{s} \to A_{s}, \pi_{s,B} : R_{s} \to B_{s}\) son \(\Sigma\)-homomorfismos a
\(A\) y \(B\) respectivamente.
  %
  \end{definition}

%%%% A^w <-(Pi^w_A)- R^w -(Pi^w_B)-> B^w
%%%% |                |               |
%%%% \sigma_a              \sigma_r            \sigma_b
%%%% \/               \/             \/
%%%% A_s <-(Pi_A)-    R_s  -(Pi_B)-> B_s
\[
\begin{tikzcd}
  {A^{w}} \arrow[d,"\sigma_{A}"']
  & {R^{w}} \arrow[l,"\pi^{w}_{A}"'] \arrow[r,"\pi^{w}_{B}"] \arrow[d,"\sigma_{R}"]
  & B^{w} \arrow[d,"\sigma_{B}"] \\
  A
  & R \arrow[l,"\pi_{A}"] \arrow[r,"\pi_{B}"']
  & B \\
\end{tikzcd}
\]

Sea \(A \in \textit{Alg}_{\Sigma}\) una \(\Sigma\)-álgebra inicial y \(R\) una
\(\Sigma\)-congruencia en \(A\) con proyecciones \((\pi_{1},\pi_{2} : R \to A)\).
%
Por inicialidad de \(A\) existe un único homomorfismo \((i : A \to R)\) tal que:
\[\pi_{1} \circ i = 1_{A} = \pi_{2} \circ i\]
%
En otras palabras, esto implica que para todo \(a \in A, i(a) = (a,a) \in R\), por
lo que \(R\) es una relación reflexiva, (\(=_{A} \subseteq R\)).
%
Adicionalmente la relación \(=_{A}\) es una \(\Sigma\)-congruencia en \(A\), por lo
que hemos demostrado el siguiente teorema.

\begin{theorem}
  Sea \(A\) una álgebra inicial de \(\Sigma\), la relación de igualdad \(=_{A}\) en
\(A\) es la menor \(\Sigma\)-congruencia:
  \[
    =_{A} = \bigcap \{ R \subseteq A \times A | R \text{ es una \(\Sigma\)-congruencia en } A \}
  \]
  \end{theorem}

Sea una \(\Sigma\)-álgebra \(A \in \textit{Alg}_{\Sigma}\) y \(O \subseteq A \times A\) una familia de
preórdenes en \(S\), \((\pi_{1},\pi_{2} : O \to A)\), de forma tal que \(O\) sea una
\(\Sigma\)-álgebra:
\begin{itemize}
  \item Para cada \(\sigma \in \Sigma_{w,s}\), dados pares de valores relacionados
\(p_{1} , \cdots, p_{n} \in O_{s_{1}}, \cdots, O_{s_{n}}\) tenemos que
\(\sigma_{O} (p_{1}, \cdots, p_{n}) \in O_{s}\subseteq A_{s} \times A_{s}\).
  \item Para cada \(\sigma \in \Sigma_{\lambda, s}\), \(\sigma_{O} \in O_{s} \subseteq A_{s} \times A_{s}\)
\end{itemize}

% \subsection{Costos dentro de Álgebras}

% \martin{Esto no debería estar acá, pero lo podemos mover después.}

% Finalmente pare cerrar el cápitulo, podemos explorar de forma abstracta la idea
% de introducir costos dentro de la relaciones entre álgebras.

% Podemos entonces obtener una relación entre términos pero que además se comparen
% los costos.
% %
% Sea \((A \times \Nat*) \in Alg_{\Sigma}\) una álgebra de costos, puedo definir
% entonces una relación entre términos tales que podamos obtener una definición de
% mejora o \emph{improvement}.
% %
% Un álgebra de costos nos permite adicionar una noción de costo a cada uno de los
% elementos de la familia de conjuntos.

% \begin{definition}[Mejora]
%   Sea \(A, B\) dos conjuntos equipados con costos,
% \(cost_{A} : A -> \Nat*, cost_{B} : B -> \Nat*\).
%   %
%   Decimos entonces que \(a \improv* b\) con \(a \in A, b \in B\) si y solo si
% \(cost_{A}(a) \geq cost_{B}(b)\).
%   \end{definition}

% Respetar operadores es si para cada \(\tau (\overline{a}) \sim \tau (\overline{b})\) si
% \(\overline{a} \overline{\sim} \overline{b}\).

% Veamos entonces como asignar una noción de costos a la evaluación que definimos
% con anterioridad.

% Veamos primero una noción sencilla donde además de computar el valor computamos
% el costo requerido para llegar a él.
% %
% Es decir que nuestro dominio de evaluación es entonces el producto (conjunto)
% de los dominios \(D_{V}\) y \(\mathbb{N}\):
% \[
%   D_{c} = D_{v} \times \mathbb{N} \equiv I \times \mathbb{N} + [ D_{c} \to D_{c} ] \times \mathbb{N}
% \]
% Donde le asignamos a cada elemento del dominio semántico \(D_{V}\) un costo,
% aquel utilizado para computarlo a partir de un término.

% Podemos entonces definir algunas funciones auxiliares:
% \begin{align*}
%   cost &: D_{c} \to \mathbb{N} \\
%   cost &= \pi_{2} \\
%   value &: D_{c} \to D_{V} \\
%   value &= \pi_{1} \\
%   appSum &: (D_{V} \times D_{V} \to D_{V}) \to D_{c} \times D_{c} \to D_{c} \\
%   appSum(f) &= [ f \circ \langle value, value \rangle , (+) \circ \langle cost , cost \rangle ] \\
%   tick &: D_{c} \to D_{c} \\
%   tick &= <1_{D_{V}}, suc >
%   addCost &: D_{c} \times \mathbb{N} \to D_{c} \\
%   addCost((v,m), n) &= (v, n + m)
%   \end{align*}

% Podemos entonces definir la siguiente álgebra de evaluación que cuenta el costo
% de evaluar un término en un valor.
% %
% Las valores no llevaran costos, ya que computarlos no nos cuenta nada.
% \martin{todo esto es una verga, los valores tienen términos suspendidos, que hacemos con eso?}

% Y para términos:
% \begin{align*}
%   ret_{D}(v) &= tick(v) \\
%   app_{D}(v, t) &= tick \circ appSum(ap) \circ [v , t] \\
%   let_{D}(x, m, t) &=  addCost \circ <f \circ assign_{x}, 1 + cost> \circ [ [m,1_{e}] , m ] \\
%   \end{align*}

% De esta manera sumamos un tick por cada operador analizado, y además acumulamos
% los costs  requeridos por cada uno de los términos.

% Vamos algunos ejemplos

% Veamos el costo de la evaluación de un término que es simplemente un valor:
% \begin{align*}
%   [[ ret(\lambda_{x}(x)) ]]
%     &\equiv tick \circ [[ \lambda_{x}(x) ]] \\
%     &\equiv tick \circ tick \circ inj_{D \to D} \circ abstract( (tick \circ access_{x}) \circ assign_{x})\\
%   \end{align*}
% Notar que podemos definir contextos muy fácilmente dentro de la teoría de
% álgebra inicial como aquellos términos construidos a partir del álgebra libre.
% \martin{There are still some details left out, p and q can have free variables}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \martin{This is not required in this part, maybe it is better to introduce it latter with effects}

\subsection{Álgebras Continuas}

Interpretamos la evaluación de términos como una secuencia de aproximaciones.
%
Dado que los términos están representados como un constructor y sus argumentos,
la evaluación se basa en interpretar sus argumentos y luego el constructor en
sí.
%
El proceso de evaluación de un constructor y sus argumentos se repite una
cantidad finita de veces, primero interpretando hasta un constructor, luego
hasta dos, luego hasta tres, y así sucesivamente.
%
Dicho proceso define entonces una secuencia de evaluaciones aproximadas.
%
Por lo que definimos a la evaluación como el supremo de su cadena de
aproximaciones y por lo tanto utilizamos álgebras  que respeten el orden
de dichas cadenas.
%
Es decir, utilizamos conjuntos parcialmente ordenados y restringimos nuestro
universo de álgebras a aquellas que respetan el orden de los conjuntos ordenados
con los que trabajemos.

Dado que mezclamos la semántica de los programas con propiedades
intensivas de la evaluación utilizamos órdenes parciales completos.
%
Recordemos que un CPO es un orden parcial de forma tal que todo conjunto
dirigido tiene supremo.

En esta sección nos concentramos en definir álgebras cuyos portadores son CPO.

\begin{definition}[\(\Sigma\)-Álgebra Ordenada]
  Sea \(\Sigma\) una signatura indexada por sorts \(S\).
  %
  Una \(\Sigma\)-álgebra ordenada consiste entonces en una familia de CPOs
\(\langle A_{S} \rangle_{s \in S}\) donde \(A_{s}\) es el portador del sort \(s \in S\).
%
Adicionalmente vamos a requerir que la interpretación de los símbolos de los
operadores sean funciones continuas, es decir, que mantengan el sentido de las
cadenas.

Para todo \(\langle w , s \rangle \in S^{*} \times S\) y \(\sigma \in \Sigma_{w, s}\), define una función
\emph{continua} en todos sus argumentos
\(\sigma_{A} : A_{w_{1}} \times \cdots \times A_{w_{n}} \to A_{s}\) con \(w = w_{1} \ldots w_{n}\) y
\(w_{i} \in S\) con \(i = 1 \ldots n\).
  \end{definition}

En otras palabras, un álgebra es ordenada si interpreta cada uno de los
operadores como funciones continuas, es decir, como funciones que respetan el
\emph{sentido} de sus operandos.

% Podemos entonces también definir la evaluación de términos como cadenas de
% aproximaciones en su evaluación.

% \martin{Why am I doing this? Why is this part here?}

% \section{Semántica de Álgebra Final}

% Por otro lado, la noción de Coálgebras finales están más conectadas con la
% noción de \emph{bisimulación} y de relaciones en general.
% %
% Podemos conectar directamente la idea de equipar con costos el conjunto de
% términos y valores: \(T_{\Sigma^{T+V}} \times \Nat*\), y utilizar una coálgebra en la
% evaluación tal que: \(\gamma : T_{\Sigma^{T+V}} \to T_{\Sigma^{T+V}} \times \Nat*\).
% %
% De esa manera podemos anotar en cada paso de la evaluación cuanto nos costó
% hacerlo.

% \martin{Multi sorted co-algebras? I don't think it makes sense... Or maybe it
% does, but I would use partial functions though}
% % Las coalgebras son la idea dual de las álgebras:
% % \begin{definition}[CoAlgebra]
% %   Dada una signatura \(\Sigma\), una \(\Sigma\)-coalgebra es un una familia de conjuntos
% %   \( \langle A_{s} \rangle_{s \in S}\).
% %   %
% %   Donde para cada \(\langle w, s \rangle \in S^{*} \times S\) y para cada \(\sigma \in \Sigma_{w,s}\), define una operación
% %   \end{definition}
% %
% \begin{definition}[\(\Sigma\)-coalgebra]
%   Sea \(\Sigma\) una signatura, definimos una coalgebra como el par formado por un
% conjunto \(A\) y una función \(\gamma\),\(\langle A , \gamma \rangle\), de forma tal que para cada
% \(a \in A\), existe \(\sigma \in \Sigma\) con aridad \(\alpha(\sigma) = n \in \Nat*\) y
% \(a_{1}, \ldots, a_{n} \in A^{n}\) tales que \(\gamma(a) = \sigma(a_{1}, \ldots, a_{n})\).
% \end{definition}

% Lo que necesitamos es conectar la noción de \emph{observaciones} sobre la
% evaluación de los programas, y para eso, es mejor introducir o definir la noción
% de semántica de álgebra final.
% %

% Más aún lo que nosotros estamos buscando no es una noción de costos total sobre
% los programas sino lo que buscamos es definir una noción relativa de costos, una
% relación que nos permita establecer mejoras sobre programas.


% La verdad que no se bien lo que estoy explicando.

%%% Local Variables:
%%% TeX-master: "../../Thesis.tex"
%%% TeX-PDF-mode: t
%%% ispell-local-dictionary: "es"
%%% End:
