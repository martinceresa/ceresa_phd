\chapter[Optimizaciones con Efectos]{Optimizaciones como Mejoras con Efectos}\label{ch:examples}

En este capítulo, revisamos dos ejemplos de optimizaciones como mejoras en
lenguajes con efectos algebraicos.
%
Un ejemplo general donde hipotetizamos sobre como se observan los efectos en el
lenguaje y mostramos que  la eliminación de código muerto es una optimización en
la presencia de efectos.
%
Otro ejemplo donde mostramos que la eliminación de sub-expresiones comunes es
una optimización bajo el efecto de no-terminación.

\section{Eliminación de Código Inobservable}
%
El objetivo de este ejemplo es presentar al lector una optimización general en
presencia de efectos y utilizamos este resultado para mostrar una simple
optimización dentro del área de computaciones probabilísticas.
%
Hasta el momento que se realizaron los
estudios~\parencite{Ceresa.2022.EffectfulImp}, y hasta donde el autor conoce,
representa la primer mejora probada formalmente dentro del área de computaciones
probabilísticas, por lo que, este ejemplo muestra progreso sobre el estudio de
lenguajes probabilísticos.

Definimos la \emph{eliminación de código inobservable} como una generalización
de eliminación de código muerto en la presencia de efectos algebraicos.
%
La eliminación de código muerto buscar eliminar código que no modifica el
resultado final de la computación.
%
En presencia de efectos, podemos estar eliminando código que genera efectos
observables, aunque no modifique el resultado final de la computación.
%
% Mientras que la eliminación de código muerto elimina código que no cambia el
% resultado final de la computación, en la presencia de efectos, tenemos que ser
% más cuidados para no remover código que puede llegar a producir efectos
% observables.
%
Un ejemplo sencillo de código que no influye en la computación de un valor, pero
que produce un efecto, es un fragmento de código que simplemente muestra un
mensaje por pantalla.
%
En caso de eliminar dicho fragmento de código, estaría eliminando el efecto de
mostrar el mensaje por pantalla, y por lo tanto, estaría cambiando el
comportamiento observable del programa aunque no su resultado.

A lo largo de la sección asumimos que tenemos una signatura \(\Sigma\) y un
\(\Sigma\)-sistema \((T, \Gamma)\).
% donde la signatura \(\Sigma\)
% contiene los operadores que introducen efectos y la mónada \(T\) es capaz que
% interpretar dichos efectos.
% Para simplificar la presentación, y sin perder generalidad, asumimos que tenemos
% una signatura \(\Sigma\) que describe las operaciones que generan los efectos
% deseados y una mónada \(T\), capaz de interpretar los efectos de cada operación,
% equipada con un relacionador mónadico \(\Gamma\), tal que, \((T, \Gamma)\) forma
% un sistema \(\Sigma\).
%
Dado el sistema \((T, \Gamma)\) tenemos una relación de similaridad
\((\sqsubseteq)\)~\parencite{Lago2017EffectfulApplicativeBisimilarity} y una
relación de mejoras derivada \((\succeq)\)~(Sección~\ref{sec:dev:cost}).
%
% Gracias al trabajo de~\citeauthor{Lago2017EffectfulApplicativeBisimilarity},
% tenemos una relación de similaridad \(\sqsubseteq\), y más aún, una noción de mejoras
% \(\succeq\)~.

\begin{theorem}[Eliminación de Código Inobservable]
  Sean \(M,N\) dos términos cerrados tales que \(\letIn{x}{M}{N} \sqsubseteq N\),
  entonces \(\letIn{x}{M}{N} \succeq N\).
  \end{theorem}

\begin{proof}
Sean \(M,N \in \T_{0}\), tales que \(\letIn{x}{M}{N} \sqsubseteq N\).
%
La prueba se basa en dos hechos: primero, \(N\) es cerrado, entonces, la
variable \(x\) no aparece libre en \(N\); y segundo, el hecho que \(N\) aproxime
con efectos a \(\letIn{x}{M}{N}\) significa que la evaluación de \(M\) no agrega
una diferencia observable en los efectos generados por la evaluación de \(N\).

Desde el punto de vista de aproximación de términos tenemos que, por ser
\(\sqsubseteq\) una relación preadecuada:
% En símbolos, por ser \(\sqsubseteq\) una relación preadecuada, tenemos que:
\[
  [[ \letIn{x}{M}{N} ]] \mathrel{\Gamma \mathcal{U}} [[ N ]]
\]
%
Más aún, dado que la evaluación es determinística y que \(N\) es un término cerrado:
\[
  [[ \letIn{x}{M}{N} ]] \mathrel{\Gamma \bm{1}_{\V_{0}}} [[ N ]]
\]
%
En otras palabras, el relacionador mónadico \(\Gamma\) no puede observar ninguna
diferencia entre la evaluación de \(\letIn{x}{M}{N}\) y \(N\), son equivalentes
modulo \(\Gamma\).

Mientras que al momento de agregar costos y utilizar la evaluación
instrumentada, además de evaluar el término, se acumulan los ticks generados por
la evaluación del término \(M\), antes de continuar con la evaluación del
término \(N\).
%
En el caso que la evaluación de \(M\) diverja, tenemos que independientemente
de que término sea \(N\), es una mejora ya que \(\Gamma\) es un relacionador
inductivo.
%
Dado que la evaluación es equivalente en términos semánticos por hipótesis,
podemos restar los ticks generados por la computación de un valor que no se
utiliza, y así probar que la eliminación de código inobservable es una mejora.
%
% Finalmente, podemos olvidar los ticks generados por la evaluación del
% término \(M\) y así probar que la eliminación de código inobservable es una
% mejora.

Formalmente, dado que los términos \(M\) y \(N\) son cerrados, podemos construir
una \(\lambda\)-relación que relaciona términos idénticos y además
\(\letIn{x}{M}{N}\) con \(N\), y la relación identidad para valores, en
símbolos:
\[
  (\T_{0} \cup \{(\letIn{x}{M}{N}, N)\}, \V_{0})
\]
%
Dicha relación es una simulación aplicativa de mejoras.
%
Además de la identidad de términos (que es una simulación aplicativa de
mejoras), tenemos un solo caso adicional:
%
\begin{align*}
  & {{[[ \letIn{x}{M}{N} ]]}_{\mathbb{N}_{\infty}}} \\
  {\equiv} & \langle \text{Evaluación} \rangle \\
  & {\checkmark {[[M]]}_{\mathbb{N}_{\infty}} \cost{>>=} \lambda \_ \mapsto \cost{[[N]]}} \\
  {\equiv} & \inrangles{ \text{Mónada de Costos}} \\
  & {\checkmark \cost{[[ M ]]} >>= \lambda (\_, vc) \mapsto \checkmark^{vc} \cost{[[N]]}} \\
  {\cost{\Gamma} \bm{1}_{\V_{0}}} & \inrangles{\text{Aritmética}} \\
  & {\checkmark \cost{[[M]]} >>= \lambda (\_, \_) \mapsto \cost{[[N]]}} \\
  {\cost{\Gamma} \bm{1}_{\V_{0}}} & \inrangles{\text{Hipótesis código inobservable }} \\
  & {\checkmark \cost{[[N]]}} \\
  \end{align*}
\end{proof}

Dependiendo de los efectos y las observaciones que se hagan sobre los mismos,
algunos sistemas aceptan como una transformación válida la eliminación de código
muerto, es decir, aquellos sistemas que para todos dos términos cerrados
\(M,N \in \T_{0}, \letIn{x}{M}{N} \sqsubseteq N\).
%
Para aquellos sistemas, por el lema que acabamos de mostrar, podemos probar que
es efectivamente una mejora sujeta a la definición derivada por la noción de
equivalencia y el relacionador de costos internos.

A modo de ejemplo concreto de eliminación de código inobservable, mostramos que
el lenguaje probabilístico equipado con el sistema de sub-distribuciones para
interpretar los efectos acepta dicha optimización.
%
Utilizamos la evaluación instrumentada~(Definición~\ref{def:eval:cost})
donde se suma un tick por cada constructor del lenguaje y el operador binario
durante la evaluación de un término.

\begin{lemma}
  Dados dos términos probabilísticos cerrados \(M\) y \(N\), entonces para toda
variable \(x\), \(\letIn{x}{M}{N} \sqsubseteq N\).
  \end{lemma}
\begin{proof}
  Sean \(M\) y \(N\) dos términos cerrados y \(x\) una variable.
  %
  La prueba consiste en mostrar que el relacionador \(\Gamma^{\mathcal{D}}\) relaciona las
sub-distribuciones generadas por la evaluación de los términos
\(\letIn{x}{M}{N}\) y
\(N\).
%
En símbolos podemos evaluar ambos términos para obtener las sub-distribuciones
que se generan y ver que son equivalentes modulo \(\Gamma^{\mathcal{D}}\):
\begin{align*}
  & [[ \letIn{x}{M}{N} ]] & \equiv \\
  & [[ M  ]] >>= \lambda \, V \mapsto {[[ N [ x := V ] ]]} & \equiv \\
  & [[ M  ]] >>= \lambda \, {V} \mapsto {[[ N  ]]} & \equiv \\
  & \lambda \, {U} \mapsto { \Sigma_{W \in \V_{0}} [[ M ]](W) \times (\lambda \,  {V} \mapsto {[[ N ]]})(W)(U)} & \equiv \\
  & \lambda \, {U} \mapsto { \Sigma_{W \in \V_{0}} [[ M ]](W) \times [[ N ]](U)} &
\end{align*}
%
Por definición de la mónada de sub-distribuciones, la evaluación de un término
en todos los valores posibles es menor o igual que 1,
y por lo tanto:
%
\[
  \forall V \in \V_{0}, \Sigma_{W \in \V_{0}} [[ M ]] (W) \times [[ N  ]](V) \leq 1 \times [[ N ]] (V) \leq [[ N ]](V)
\]
%
Aplicando la definición de \(\Gamma^{\mathcal{D}}\):
\[
  [[ \letIn{x}{M}{N} ]] \mathrel{\Gamma^{\mathcal{D}} \bm{1}_{\V_{0}}} [[ N ]]
\]
%
Y por lo tanto podemos concluir que \(\letIn{x}{M}{N} \sqsubseteq N\).
  \end{proof}

La transformación de código inobservable es una mejora para el lenguaje
probabilístico, ya que aplica el teorema de eliminación de código inobservable.
%
En palabras, lo que estamos mostrando es que eliminar código que no contribuye a
la construcción del resultado, pero que puede introducir mayores posibilidades
de fallar (o divergir), es una optimización.
%
Es decir, que eliminar código que mejora la probabilidad de convergencia de la
evaluación y que \emph{no} modifica el resultado es una optimización.
%
Podemos replicar un lema similar para el lenguaje no-determinista, pero no
podríamos hacerlo con otros efectos como mensajes de salida, donde eliminar
operaciones que imprimen mensajes llevarían a diferentes observaciones sobre el
sistema.

\section{Eliminación de Sub-expresiones Comunes}\label{sec:CSE}
%
La eliminación de sub-expresiones comunes, CSE de su siglas en
inglés~\footnote{\emph{Common Sub-expression Elimination}}, es una
transformación que busca eliminar múltiples ocurrencias de una sub-expresión en
un término.
%
Consiste en identificar una sub-expresión común \(M\), y utilizar una nueva
variables fresca \(x\), de forma tal que podamos guardar en \(x\) el resultado
de la evaluación de \(M\) y así consultarlo siempre que sea necesario \emph{sin
tener que re-evaluar la expresión \(M\)}.
%
La transformación CSE es considerada una optimización ya que evita tener que
evaluar múltiples veces una misma expresión, y por lo tanto, el costo total de
la evaluación se reduce.
%
Lamentablemente, no siempre es el caso cuando hay efectos computacionales.
%
Por ejemplo, en la presencia de entrada y salida, al eliminar la múltiple
evaluación de una sub-expresión que genera un mensaje en pantalla, elimina
también que dicho mensaje aparezca múltiples veces.

En esta sección, primero exploramos cómo definir la optimización CSE como una
mejora trabajando desde la intuición sin tener en cuenta los efectos, y segundo,
probamos que CSE es una optimización cuando el único efecto observable es la
no-terminación.

Dada una signatura \(\Sigma\), y un sistema compatible con \(\Sigma\),
\((T,\Gamma)\).
%
Enunciamos \emph{intuitivamente} CSE de la siguiente forma: para todo contexto
\(\mathbb{C}\), término cerrado \(M\), y una variable fresca \(x\), el término
\(\checkmark\mathbb{C}[\checkmark M]\) es mejorado por el término
\(\letIn{x}{M}{\mathbb{C}[\lRet{x}]}\)~\parencite{Hackett.2019.Clairvoyant}.
%
Los ticks que se agregan son necesarios para pagar por adelantado la
transformación: un tick por guardar el valor de \(M\) en \(x\),
\(\letIn{x}{M}{\ldots}\), y un tick adicional por cada vez que se busca el valor
de \(x\).
%
Esta manera de agregar ticks necesarios dependen de la  definición de la función
de evaluación instrumentada de términos con costos.
%
En particular, en la Definición~\ref{def:eval:cost}, la evaluación de términos
\(\lRet{x}\) y \(\letIn{x}{M}{N}\) agregan un tick al costo de evaluación de un
término, y por eso, agregamos un tick por cada construcción agregada al término
original.

Si bien la transformación de CSE antes planteada codifica la intuición detrás de
CSE, es muy general para ser una mejora ya que incluye contextos en los cuales
dicha transformación \emph{empeora} el término, incrementando el costo total de
la evaluación.
%
Por ejemplo, si el contexto no requiere la evaluación de su agujero, CSE
evaluará la expresión prematuramente de forma innecesaria.

\begin{example}\label{ex:no:CSE}
  Sean \(N,M\) dos términos cerrados y \(\mathbb{C} \doteq N\).

El contexto \(\mathbb{C}\) es un contexto sin agujeros.
%
Si realizamos la transformación que describe CSE intuitivamente tendríamos que:
\[
  \checkmark \mathbb{C}[\checkmark M] \equiv \checkmark N[\checkmark M] \succeq_{\Gamma} \letIn{x}{M}{N[\lRet{x}]}
\]
%
Aplicando la definición de llenado de agujeros, tenemos que:
\[
  \checkmark N \succeq_{\Gamma} \letIn{x}{M}{N}
\]

Dicha formula sólo se cumple si la evaluación del término \(N\) no termina, pero
es falsa en cualquier otro caso.
  \end{example}

Una forma de resolver el problema, y así refinar la definición de CSE como una
mejora, es introducir la noción de que un contexto utiliza su agujero durante la
evaluación.
%
Seguimos la literatura e introducimos el concepto de \emph{contextos de
reducción}, o \emph{contextos de
evaluación}~\parencite{Felleisen.1992.EvaluationContexts}.
%
Siguiendo la función de evaluación podemos definir contextos que usan sus
agujeros durante la evaluación, y así definir correctamente CSE como una mejora.

\begin{definition}[Contextos de Reducción]
  Sea \(\Sigma\) una signatura.
  %
  Definimos los contextos de reducción utilizando la siguiente gramática.
  \[
    \mathbb{R} \Coloneqq [ - ] \mid (\letIn{x}{\mathbb{R}}{\mathbb{C}})
    \mid \lApp{(\lAbs{x}{\mathbb{R}})}{\mathbb{D}}
    \mid \sigma(\mathbb{C}_{0},\ldots, \mathbb{C}_{n-1})
  \]

  Donde \(\sigma \in \Sigma, n = \alpha(\sigma)\), y al menos uno de los contextos \(\mathbb{C}_{i}\)
con \(i < n\) es de reducción, y el contexto \(\mathbb{D}\) no necesariamente
es de reducción.
  \end{definition}

Los contextos de reducción son contextos que usan o requieren que sus agujeros
sean llenados para completar su evaluación, en otras palabras, la evaluación de
un contexto de reducción sin llenar sus agujeros quedaría bloqueada.
%
Es fácil ver que el contexto del Ejemplo~\ref{ex:no:CSE} no es un contexto de
reducción ya que no tiene ningún agujero en él.
%
Los contextos de reducción garantizan que la expresión con la que llenaremos el
agujero será evaluada, exactamente lo que necesitamos para definir CSE.

\begin{definition}[Eliminación de Sub-expresiones Comunes]
Sea \(\Sigma\) una signatura, \((\Gamma,T)\) un \(\Sigma\)-sistema, y
\(\mathbb{R}\) un contexto de reducción.

Definimos a CSE tal que para todo término cerrado \(M \in \T_{0}\) y variable
fresca \(x\) vale que:
\[
  \checkmark \mathbb{R}[\checkmark M] \succeq_{\Gamma} \letIn{x}{M}{\mathbb{R}[\lRet{x}]}
\]
  \end{definition}

La transformación CSE es una simple optimización de compilador en lenguajes sin
efectos, sin embargo, fue probada correcta por primera vez para un cálculo
lambda utilizando una estrategia de reducción \emph{call-by-need} utilizando
teorías de mejoras~\parencite{Moran.1999.Need}, y desde entonces es el principal
ejemplo de las teorías
desarrolladas~\parencite{Schmidt.2015.ImpFunctionalCore,Hackett.2019.Clairvoyant}.
%
Cuando se agregan efectos algebraicos al lenguaje, la transformación CSE puede
no ser correcta, ya que puede cambiar el comportamiento observable de los
programas.
%
Por ejemplo, cambiar el orden de dos llamadas a una operación para imprimir un
mensaje en pantalla, cambia el orden en que estos mensajes aparecen en pantalla.

Teniendo una definición de CSE, presentamos una prueba de CSE cuando el
\emph{único efecto observable es la no-terminación}, es decir, para el sistema
parcial, siendo éste el caso de estudio del área.
% como se mencionó en el párrafo anterior.
%
Primero mostramos un lema más simple que conlleva la misma idea y luego lo
utilizamos para mostrar que CSE es una mejora.

Desde ahora hasta el final de la sección asumiremos que estamos trabajando con
el sistema parcial.

\begin{lemma}[CSE Directo]
  Sea \(M\) un término cerrado, \(\mathbb{C}\) un contexto tal que
\(\mathbb{C}[M] \in \T_{0}\), y \(x\) una variable fresca, tenemos que:
\[
  \letIn{x}{M}{\mathbb{C}[\checkmark M]} \succeq_{\Gamma_{\bot}} \letIn{x}{M}{\mathbb{C}[\lRet{x}]}
\]
  \end{lemma}

El lema CSE Directo establece que, ya que vamos a evaluar la expresión \(M\) y
guardarla en \(x\), podemos utilizar directamente el valor que está en \(x\) y
así evitar evaluar \(M\) de nuevo.
%
Pero además, establece como se tendrían que reordenar los efectos producidos por
la evaluación de los términos.
%
En otras palabras, encapsula además como se tienen que reestructurar los efectos
en el caso que quisiéramos evitar tener que evaluar la expresión \(M\),
asumiendo que ya hemos computado su valor.
%
Se puede ver que CSE Directo es un caso particular de CSE donde se utiliza el
contexto de reducción \(\letIn{x}{[-]}{\mathbb{C}}\).
%
La prueba procede por inducción en la estructura del contexto \(\mathbb{C}\) y
se puede encontrar en el Apéndice~\ref{appendix:CSE:Directo}.

Volviendo a la prueba de CSE, esta consiste en la observación de que la
evaluación de contextos de reducción llevan a la evaluación de una expresión
\emph{let}~(donde CSE directo se puede aplicar) o a su agujero~(donde la mejora
es evidente).
%
Esta observación es similar a lo
que~\citeauthor{Moran.1999.Need}~\parencite*{Moran.1999.Need} llaman \emph{open
uniform computation lemma} donde los autores muestran que (en su sistema) la
evaluación de sus contextos lleva a una variable libre, un valor, o un agujero.

El sistema parcial no tiene ninguna operación, \(\Sigma = \emptyset\).
%
Esto reduce los posibles contextos de reducción, y en particular, no hay
contextos de reducción de la forma \(\sigma(\ldots)\).
% y en tal caso la
% propiedad de CSE se cumple por vacuidad.
%
Eliminar el caso particular del contexto de reducción, y reordenar los efectos
en la prueba de CSE directo~(Apéndice~\ref{appendix:CSE:Directo}), son los
únicos momentos donde utilizamos que trabajamos con el sistema parcial.

\begin{lemma}[CSE para el sistema parcial]
  Sea \(M\) un término cerrado, \(\mathbb{R}\) un contexto de reducción tal que
  \(\mathbb{R}[M] \in \T_{0}\), y \(x\) una variable fresca, entonces:
  \[
    \checkmark \mathbb{R}[\checkmark M] \succeq_{\Gamma_{\bot}} \letIn{x}{M}{\mathbb{R}[\lRet{x}]}
  \]
  \end{lemma}

  \begin{proof}
    La prueba consiste en construir una simulación aplicativa que exponga la mejora.
    %
    Sea \(M\) un término cerrado, y \(\mathbb{R}\) un contexto, tal que \(\mathbb{R}[M] \in \T_{0}\).
    %
    Definimos la siguiente relación entre \(\lambda\)-términos:
  \[
    \begin{array}{lcl}
    \mathbb{V}_{\T} & \hspace{-0.75em} \doteq & \T_{0} \\
                    & \hspace{-0.75em} \cup &  \{ (\checkmark \mathbb{R}[\checkmark M] , \letIn{x}{M}{\mathbb{R}[\lRet{x}]}) \\
                    & & \mid \mathbb{R} \text{ es un contexto de reducción}, x \text{ es fresca}\} \\
                    & \hspace{-0.75em} \cup & \{ (\letIn{x}{M}{\mathbb{C}[\checkmark M]}, \letIn{x}{M}{ \mathbb{C}[\lRet{x}]}) \\
                    & & \mid \mathbb{C} \text{ es un contexto}, x \text{ es fresca}\} \\
    \mathbb{V}_{\V} & \hspace{-0.75em} \doteq & \bm{1}_{\V_{0}}
      \end{array}
  \]
  %
  La relación \(\mathbb{V}\) describe la transformación CSE, y además, la
transformación CSE directo que utilizaremos como lema dentro de la demostración.
%
Utilizaremos los principios de coinducción y análisis por casos para probar
que la relación \(\mathbb{V}\) es una simulación aplicativa, y nos
concentramos en probar los casos relacionados a los de la transformación de
CSE.
%
La transformación CSE directo es probada en el
apéndice~(Apéndice~\ref{appendix:CSE:Directo}).

La relación \(\mathbb{V}_{\V}\) es la identidad sobre valores cerrados, y por lo tanto, respeta valores.

Lo que nos queda por probar es que la evaluación de términos cerrados nos lleva
a valores relacionados.
%
En símbolos, dado \(\mathbb{R}\) un contexto de reducción y \(x\) una variable fresca,
nos queda probar que:
\[
  {[[ \checkmark \mathbb{R}[\checkmark M]  ]]}_{\mathbb{N}_{\infty}}
  \mathrel{\Gamma_{\bot_{\mathbb{N}_{\infty}}} \mathbb{V}_{\V}}
  {[[ \letIn{x}{M}{\mathbb{R}[\lRet{x}]} ]]}_{\mathbb{N}_{\infty}}
\]

Procedemos haciendo análisis por casos en la estructura de \(\mathbb{R}\).
%
En el caso de que el contexto de reducción \(\mathbb{R}\) es el contexto trivial,
\(\mathbb{R} = [-]\), el resultado se obtiene directamente.
%
Además ya que estamos en el sistema parcial, el conjunto de operadores es vacío,
por lo que no existen contextos de reducción con operadores algebraicos.
%
Como resultado solo tenemos dos casos: el caso que \(\mathbb{R}\) sea un let-binding o
una aplicación.
%
% En ambos casos vamos a llenar los agujeros con el objetivo de encontrar
% contextos de reducción donde podamos aplicar la hipótesis inductiva o estar en
% un caso particular del CSE directo.

\begin{itemize}
  \item El caso en que el contexto de reducción sea una expresión let-binding:
sea \(\mathbb{R} = \letIn{y}{\mathbb{S}}{\mathbb{C}}\), para algún contexto de
reducción \(\mathbb{S}\) y contexto (no necesariamente de reducción)
\(\mathbb{C}\).
  \[\begin{array}{rl}
& {\cost{[[ \checkmark (\letIn{y}{\mathbb{S}}{\mathbb{C}})[\checkmark M] ]]}} \\
{\equiv} & \inrangles{\text{Llenado de agujeros, \(M\) es cerrado}} \\
& {\cost{[[ \checkmark (\letIn{y}{\mathbb{S}[\checkmark M]}{\mathbb{C}[\checkmark M]}) ]]}} \\
{\equiv} & \inrangles{\text{Evaluación}} \\
& { \checkmark^{2} (\cost{[[ \mathbb{S}[\checkmark M] ]]} \cost{>>=} V \mapsto \cost{[[ \substTVal{\mathbb{C}[\checkmark M]}{y}{V} ]]})} \\
{\equiv} & \inrangles{\text{Asociatividad de la mónada}} \\
& { \checkmark^{2} \cost{[[ \mathbb{S}[\checkmark M] ]]} \cost{>>=} V \mapsto \cost{[[ \substTVal{\mathbb{C}[\checkmark M]}{y}{V} ]]}} \\
{\equiv} & \inrangles{\text{Evaluación}}\\
&{ \checkmark \cost{[[ \checkmark \mathbb{S}[\checkmark M] ]]} \cost{>>=} V \mapsto \cost{[[ \substTVal{\mathbb{C}[\checkmark M]}{y}{V} ]]}} \\
{\cost{\Gamma_{\bot}}} & \inrangles{\text{Hipótesis coinductiva, \(x\) es una variable fresca}} \\
& {\checkmark \cost{[[ \letIn{x}{M}{\mathbb{S}[\lRet{x}]} ]]} \cost{>>=} V \mapsto \cost{[[ \substTVal{\mathbb{C}[\checkmark M]}{y}{V} ]]}} \\
{\equiv} & \inrangles{\text{Evaluación}} \\
& {
  \begin{array}{l}
  \checkmark( \checkmark \cost{[[ M ]]} \cost{>>=} W \mapsto \cost{[[ \substTVal{\mathbb{S}[\lRet{x}]}{x}{W} ]]} ) \\
  \cost{>>=} \\
  V \mapsto \cost{[[ \substTVal{\mathbb{C}[\checkmark M]}{y}{V} ]]}
  \end{array}
} \\
{\equiv} & \inrangles{\text{Asociatividad de las mónadas}} \\
    & { \begin{array}{l}
        \checkmark^{2} \cost{[[ M ]]} \cost{>>=} W \mapsto \cost{[[ \substTVal{\mathbb{S}[\lRet{x}]}{x}{W} ]]}\\
        \cost{>>=}\\
        V \mapsto \cost{[[ \substTVal{\mathbb{C}[\checkmark M]}{y}{V} ]]}
        \end{array}
        } \\
{\equiv} & \inrangles{\text{Varios pasos en la evaluación}} \\
& {\cost{[[ \letIn{x}{M}{\letIn{y}{\mathbb{S}[\lRet{x}]}{\mathbb{C}[\checkmark M]}} ]]}} \\
{\cost{\Gamma_{\bot}}} & \inrangles{\text{CSE directo, \(x\) es fresca}} \\
& {\cost{[[ \letIn{x}{M}{\letIn{y}{\mathbb{S}[\lRet{x}]}{\mathbb{C}[\lRet{x}]}} ]]}}
  \end{array}\]

\item El caso en el que el contexto \(\mathbb{R}\) sea un reducción aplicación:
\(\mathbb{R} = \lApp{(\lAbs{y}{\mathbb{S}})}{\mathbb{V}}\), para algún contexto de
reducción \(\mathbb{S}\), contexto valor \(\mathbb{V}\) y variable \(y\).

\begin{longtable}{RL}
& {\cost{[[ \checkmark (\lApp{(\lAbs{y}{\mathbb{S}})}{\mathbb{V}})[\checkmark M] ]]}}\\
{\equiv} & \inrangles{\text{Llenado de agujeros, \(M\) es un término cerrado}} \\
& {\cost{[[ \checkmark (\lApp{(\lAbs{y}{\mathbb{S}[\checkmark M]})}{(\mathbb{V}[\checkmark M])}) ]]}} \\
{\equiv} & \inrangles{\text{Evaluación}} \\
& {\checkmark^{2} \cost{[[ \substTVal{\mathbb{S}[\checkmark M]}{y}{\mathbb{V}[\checkmark M]} ]]}} \\
{\equiv} & \inrangles{\text{Llenado de agujeros, \(M\) es un término cerrado}} \\
& {\checkmark^{2} \cost{[[ (\substTVal{\mathbb{S}}{y}{\mathbb{V}})[\checkmark M] ]]}} \\
{\equiv} & \inrangles{\text{Evaluación}} \\
& {\checkmark \cost{[[ \checkmark (\substTVal{\mathbb{S}}{y}{\mathbb{V}})[\checkmark M] ]]}} \\
{\cost{\Gamma_{\bot}}} & \inrangles{\text{Hipótesis coinductiva, \(x\) es una variables fresca}} \\
& {\checkmark \cost{[[ \letIn{x}{M}{(\substTVal{\mathbb{S}}{y}{\mathbb{V}})[\lRet{x}] }  ]]}} \\
{\equiv} & \inrangles{\text{Llenado de agujeros, \(M\) es un término cerrado}} \\
& {\checkmark \cost{[[ \letIn{x}{M}{(\substTVal{(\mathbb{S}[\lRet{x}])}{y}{(\mathbb{V}[\lRet{x}])}) }  ]]}} \\
{\equiv} & \inrangles{\text{Evaluación}} \\
& {\checkmark \cost{[[ M ]]} \cost{>>=} W |-> \cost{[[ \substTVal{(\substTVal{(\mathbb{S}[\lRet{x}])}{y}{(\mathbb{V}[\lRet{x}])})}{x}{W} ]]}} \\
{\equiv} & \inrangles{\text{Substitución, \(x \notin FV(\mathbb{S},\mathbb{V}), x \neq y\)}} \\
& {\checkmark \cost{[[ M ]]} \cost{>>=} W |-> \cost{[[ (\substTVal{(\mathbb{S}[\lRet{W}])}{y}{(\mathbb{V}[\lRet{W}])}) ]]}} \\
{\equiv} & \inrangles{\text{Tick es una operación pura mapeada}} \\
& {\cost{[[ M ]]} \cost{>>=} W |-> \checkmark \cost{[[ (\substTVal{(\mathbb{S}[\lRet{W}])}{y}{(\mathbb{V}[\lRet{W}])}) ]]}} \\
{\equiv} & \inrangles{\text{Evaluación}} \\
& {\cost{[[ M ]]} \cost{>>=} W |-> \cost{ [[ \lApp{(\lAbs{y}{\mathbb{S}[\lRet{W}]})}{(\mathbb{V}[\lRet{W}])} ]] }} \\
{\equiv} & \inrangles{\text{Substitución, \(x\) es una variable fresca}} \\
& {\cost{[ M ]} \cost{>>=} W |-> \cost{ [[ \substTVal{((\lApp{\lAbs{y}{\mathbb{S}[\lRet{x}]}}){(\mathbb{V}[\lRet{x}])})}{x}{W} ]] }} \\
{\equiv} & \inrangles{\text{Evaluación}} \\
& {\cost{[[ \letIn{x}{M}{\lApp{(\lAbs{y}{\mathbb{S}[\lRet{x}]})}{(\mathbb{V}[\lRet{x}])}} ]]}} \\
{\equiv} & \inrangles{\text{Llenado de agujeros, \(x\) es una variable fresca}} \\
& {\cost{[[ \letIn{x}{M}{(\lApp{(\lAbs{y}{\mathbb{S}})}{\mathbb{V}})[\lRet{x}]} ]]}}
\end{longtable}
\end{itemize}
    \end{proof}

De esta manera podemos ver que la teoría de mejoras derivada de la aproximación
de programas con el efecto observable de que la evaluación es la no-terminación
nos permite probar que la CSE es una optimización.

\subsection{Introducir efectos en CSE}
%
En la sección anterior mostramos una prueba de CSE donde el único efecto
observable es no-terminación, posicionando la teoría de mejoras derivada de la
equivalencia al mismo nivel que sus predecesores, pero nos queda una pregunta en
el tintero: ¿CSE es una mejora en presencia de otros efectos?
%
En nuestra teoría, hay dos entidades que participan al momento de comparar dos
programas con efectos: relacionadores y mónadas.
%
Esto hace que tengamos dos maneras de probar que una transformación sobre
un programa es efectivamente una mejora:
\begin{itemize}
  \item mapear propiedades de la mónada que interpreta los efectos algebraicos generados por los operadores
  \item ajustar las observaciones realizadas sobre los efectos generados,
estableciendo propiedades sobre los relacionadores.
\end{itemize}
%
La transformación CSE reemplaza múltiples evaluaciones de un sub-término
compartido por una sola evaluación.
%
Al transformar un término siguiendo CSE, los efectos producidos por
la evaluación de una sub-expresión compartida pueden ser \emph{eliminados} o
\emph{ignorados}.
%
Podemos eliminar efectos si la mónada que interpreta los efectos tiene alguna
propiedad (e.g. idempotencia) que asegure la equivalencia entre términos.
%
Mientras que podemos ignorar efectos si la observación realizada por el relacionador
simplemente no los distingue, e.g., si las cadenas de caracteres a mostrar en
pantalla son ignorados.
%
En otras palabras, los efectos pueden ser ignorados si los relacionadores olvidan
estructura.

Todavía al momento de realizar la tesis, no hemos encontrado condiciones
necesarias y suficientes para establecer cuando CSE es una mejora en presencia
de efectos, pero sabemos que una noción de idempotencia es necesaria.
%
Sea \(\Sigma\) una signatura y \((\Gamma,T)\) un sistema compatible tal que CSE es una
mejora.
%
Entonces, podemos mostrar que el sistema \((\Gamma,T)\) es (observacionalmente)
idempotente si para todo término cerrado \(M \in \T_{0}\):
\[
  \letIn{x}{M}{\checkmark M} \succeq \letIn{x}{M}{\lRet{x}} \succeq M
\]
%
Por lo que podemos concluir que: efectos que no sean \emph{observacionalmente}
idempotentes no aceptan CSE como una mejora.
%
Además, pudimos notar que una noción débil de commutatividad es necesaria, pero
estos estudios han quedado inconclusos.

La mónada parcial es conmutativa e idempotente, y por lo tanto, podemos
reordenar o incluso remover múltiples evaluaciones del mismo término, pero
lamentablemente, no hay tantas mónadas que tengan ambas propiedades.
%
Sin embargo, podemos aprender algo de la prueba de CSE utilizada en esta
sección, necesitamos dichas propiedades en dos lugares particulares de la
prueba: en CSE directo, donde los efectos son reordenados; y en los contextos de
reducción de operaciones.
%
El resto de la prueba es genérica en los efectos.
%
Esto indica que puede llegar a ser posible ajustar más las definiciones de
manera tal que sea posible obtener una noción aunque sea observacional de cómo
reordenar los efectos generados, y además estudiar cómo la interpretaciones de
los operadores consumen los efectos ya generados.

En este capítulo vimos dos ejemplos concretos de cómo utilizar la teoría de
mejoras para mostrar que ciertas transformaciones de programas son
optimizaciones.
%
En el caso de eliminación de sub-expresiones comunes, primero definimos la
transformación utilizando contexto de reducción y luego utilizamos una simulación
de mejoras para mostrar que CSE es efectivamente una mejora.
%
Mientras que en el caso de eliminación de código inobservable primero
establecimos los requerimientos del sistema y lo probamos de forma general.
%
Luego mostramos que el sistema probabilístico cumple con el requerimiento, y por
ende, la eliminación de código inobservable es una mejora en dicho sistema.

%%% Local Variables:
%%% TeX-master: "../Thesis.tex"
%%% TeX-PDF-mode: t
%%% ispell-local-dictionary: "es"
%%% End:
