\chapter{Prueba CSE Directo}\label{appendix:CSE:Directo}

En este apéndice mostramos una prueba del lema CSE directo utilizado en la prueba
de CSE~(Sección~\ref{sec:CSE}).
%
Dividimos la prueba en dos partes: primero construimos una simulación que
utilizaremos como un lema auxiliar, y luego definiremos una simulación de mejora
que evidencia que CSE Directo es una mejora.
%
Asumimos que estamos trabajando dentro del sistema parcial donde el
único efecto observable es la divergencia en la evaluación de términos.

Llamamos a un contexto \emph{cerrado de términos} si es un contexto sin
variables libres que al llenar su agujero con un término retorna un término,
mientras que llamamos a un contexto \emph{cerrado de valores} si es un contexto
sin variables libres que al llenar su agujero con un término nos retorna un
valor.

\begin{applemm}[CSE Directo]
  Sea \(M\) un término cerrado, \(\mathbb{C}\) un contexto tal que
\(\mathbb{C}[M] \in \T_{0}\), y \(x\) una variable
  fresca.
  %
  Tenemos que:
\[
  \letIn{x}{M}{\mathbb{C}[\checkmark M]} \succeq_{\Gamma_{\bot}} \letIn{x}{M}{\mathbb{C}[\lRet{x}]}
\]
\end{applemm}
%
\begin{proof}
  Dado \(M\) un término cerrado, \(\mathbb{C}\) un contexto cerrado de términos tal que
y \(x\) una variable fresca.

Si observamos los términos \(\letIn{x}{M}{\mathbb{C}[\checkmark M]}\) y
\(\letIn{x}{M}{\mathbb{C}[\lRet{x}]}\), podemos ver que ambos términos evalúan
el término \(M\).
%
Por lo que procedemos por análisis por casos en el resultado de la
evaluación del término cerrado \(M\).
%
O bien \(\cost{[[ M ]]}\) alcanza un valor con un costo o bien la evaluación
diverge.

\begin{itemize}
  \item La evaluación del término \(M\) diverge:
        \( \cost{[[ M  ]]} = \iota_{r}(\bot)\).
    %
    Ya que ambas expresiones evalúan la expresión \(M\), y por definición de la
    mónada de parcialidad, tenemos que ambas expresiones evalúan a \(\iota_{r}(\bot)\).
    %
    Para evidenciar que es una mejora en este caso nos basta con definir la
siguiente relación entre \(\lambda\)-términos y valores.
    %
    \begin{align*}
      {DCSE_\bot}_{\T} & \triangleq Id_{\T_{0}} \\
                    & \cup \{
                      ({\letIn{x}{M}{\mathbb{C}[ \checkmark M]}}
                      ,{
                      \letIn{x}{M}{\mathbb{C}[\lRet{x}]}
                      }) \\
                    & \mid
                      \mathbb{C} \text{ es cerrado de términos}
                      , x \text{ es una variable fresca} \} \\
      {DCSE_\bot}_\V &\triangleq Id_{\V_{0}}
    \end{align*}

    La relación entre \(\lambda\)-términos y valores \(({DCSE_{\bot}}_{\T}, {DCSE_{\bot}}_{\V})\)
    es una simulación de mejoras.

  \item La evaluación del término \(M\) alcanza un valor \(V \in \V_{0}\) con un
costo \(m \in \mathbb{N}\): \( \cost{[[ M ]]} = \iota_{l}(V, m)\).

    Definimos un par de relaciones entre \(\lambda\)-términos y valores en la cual se
relacionan todos los posibles términos en donde puede aparecer \(M\) con el
mismo término pero se utiliza el resultado de la evaluación de \(M\)
directamente.
    %
    \begin{align*}
      \mathcal{R}_{\T} &\triangleq Id_{\KT} \cup \{ (\mathbb{C}[ \checkmark M ], \mathbb{C}[ \lRet{V} ])
               \mid
               \mathbb{C} \text{ es cerrado de términos }
               \} \\
      \mathcal{R}_{\V} &\triangleq Id_{\KV} \cup \{ (\mathbb{V}[ \checkmark M ], \mathbb{V}[ \lRet{V} ])
               \mid \mathbb{V}
               \text{ contexto cerrado de valores}
               \}
    \end{align*}
    %
    Nuestra tarea ahora es mostrar que el par de relaciones \((\mathcal{R}_{\T},\mathcal{R}_{\V})\)
forman una simulación de mejoras.
%
    Concretamente tenemos que mostrar que respetan valores y que relacionan
términos que evalúan a valores relacionados respectando la observación definida
por el relacionador \(\Gamma_{\bot}\).

    Sea \(\mathbb{C}\) un contexto cerrado de términos , veamos que su
evaluación está relacionada por \(\Gamma_{\bot} \mathcal{R_{\V}}\):
    \[
\cost{[[ \mathbb{C}[\checkmark M] ]]}
\mathrel{\Gamma_{\bot} \mathcal{R}_{\V}}
\cost{[[ \mathbb{C}[\lRet{V}] ]]}
    \]
    %

   Ya que la evaluación es guiada por la sintaxis, procedemos entonces a mostrar
que es una simulación aplicativa utilizando el principio coinductivo y análisis
por casos sobre el contexto \(\mathbb{C}\).
    %
    A continuación mostramos el caso más informativo donde
\(\mathbb{C} = \letIn{x}{\mathbb{D}}{\mathbb{E}}\) para contextos \(\mathbb{D}\)
y \(\mathbb{E}\).
%
El resto de los casos se procede de forma similar.

De las hipótesis tenemos que \(M \in \T_{0}\) y que \(\mathbb{C}\) es un
contexto cerrado de términos.
%
Por lo que tenemos que \(\mathbb{D}\) es cerrado de términos y pero que
\(\mathbb{E}\) no lo es, sino que \(\mathsf{FV}(\mathbb{E}[M]) \subseteq \{ x \}\).

Nuestro objetivo es mostrar que:
\begin{displaymath}
  \cost{[[ (\letIn{x}{\mathbb{D}}{\mathbb{E}})[ \checkmark M ] ]]}
  \mathrel{\cost{\Gamma_\bot} \mathcal{R}_\V}
  \cost{[[ (\letIn{x}{\mathbb{D}}{\mathbb{E}})[ \lRet{V} ] ]]}
\end{displaymath}
%
% Luego de reemplazar a \(\mathbb{C}\) por \((\letIn{x}{\mathbb{D}}{\mathbb{E}})\),
Por definición de la operación de llenado de agujero, y dado que \(M \in \T_{0}\)
y \(V \in \V_{0}\), podemos dar un paso con la evaluación de los términos:
%
\begin{displaymath}
  \begin{array}{c}
    \checkmark \cost{[[ \mathbb{D}[\checkmark M ] ]]} \cost{>>=} W \mapsto \cost{[[ \substTVal{\mathbb{E}[\checkmark M ]}{x}{W} ]]}\\
    {\cost{\Gamma_\bot} \mathcal{R}_\V}\\
    \checkmark  \cost{[[ \mathbb{D}[ \lRet{V} ] ]]} \cost{>>=} W \mapsto \cost{[[ \substTVal{\mathbb{E}[ \lRet{V} ]}{x}{W} ]]}\\
  \end{array}
\end{displaymath}

Podemos eliminar los ticks al comienzo de ambos términos ya que el operador de
tick es inyectivo.
        %
    Además, por definición de T-relacionador~(Definición~\ref{def:monad:relacionador})
podemos dividir la prueba en dos.
    \begin{itemize}
    \item Por un lado \(\cost{[[ \mathbb{D}[\checkmark M ] ]]} \mathrel{\cost{\Gamma_\bot}
            \mathcal{R}_\V} \cost{[[ \mathbb{D}[ \lRet{V} ] ]]}\) se sigue del hecho que
\(\mathbb{D}\) es un contexto cerrado de términos y de las hipótesis
coinductivas.
    %
    \item Por el otro lado tenemos que para cuales quiera \( W_0, W_1 \in \KV \)
tales que \( W_0 \mathrel{\mathcal{R}_{\V}} W_1 \), tenemos que mostrar que:
      \begin{displaymath}
      \cost{[[ \substTVal{\mathbb{E}[\checkmark M]}{x}{W_0} ]]}
      \mathrel{\cost{\Gamma} \mathcal{R}_{\V}}
      \cost{[[ \substTVal{\mathbb{E}[\lRet{V}]}{x}{W_1} ]]}
      \end{displaymath}

    En éste caso \textbf{no} podemos aplicar la hipótesis coinductiva directamente dado
que \(\mathbb{E}\) puede tener como variable libre a \(x\).
    %
    % Esto nos lleva a seguir manipulando la expresión.
    % %
    %   Note that we cannot apply the co-inductive hypothesis here since \(\EE\)
    %   may not be a closed term context, and thus, our goal is to get a closed
    %   term context to apply the co-inductive hypothesis.
      %
    Por definición de \(\mathcal{R_{\V}}\) tenemos que si
\(W_{0} \mathrel{\mathcal{R_{\V}}} W_{1}\) implica dos posibles casos: o bien
\(W_{0} = W_{1}\) o existe un contexto cerrado de valores \(\mathbb{V}\) tal que
\(W_{0} = \mathbb{V}[\checkmark M]\) y \(W_{1} = \mathbb{V}[ \lRet{V} ]\).
      %
      % From the definition of \(\mathcal{R}_\V\), \( d_0 \mathrel{\mathcal{R}_{\V}} d_1 \)
      % implies two possible cases: either \(d_0 = d_1\) or there is a value
      % context \(\VV\) such that \(d_0 = \VV[\checkmark[] M]\) and \(d_1
      % = \VV[ \LRet{m} ]\).
      %
      Ambos casos tienen la misma estructura por lo que mostramos uno de ellos,
dejando el otro como ejercicio para el lector.
      %
      Sea \(\mathbb{V}\) un contexto cerrado de valores y \(W_{0}, W_{1}\) tales
que \(W_{0} = \mathbb{V}[\checkmark M]\) y \(W_{1} = \mathbb{V}[ \lRet{V} ]\).
      %
    \[
      \begin{array}{rl}
       & {\cost{[[ \substTVal{\mathbb{E}[\checkmark M]}{x}{W_{0}} ]]}} \\
      {\equiv} & \langle \text{Definición de \(W_0\)} \rangle \\
      & {\cost{[[ \substTVal{\mathbb{E}[\checkmark M]}{x}{\mathbb{V}[\checkmark M]} ]]}} \\
      {\equiv} & \langle \text{\(M \in \KT\) y definición de llenado de agujeros} \rangle\\
      & {\cost{[[ \substTVal{\mathbb{E}}{x}{\mathbb{V}}[\checkmark M] ]]}} \\
        {\cost{\Gamma_{\bot}} \mathcal{R}_{\V}} &
                                \langle
                                \substTVal{\mathbb{E}}{x}{\mathbb{V}}
         \text{ es cerrado de valores e hipótesis coinductiva} \rangle \\
      & {\cost{[[ \substTVal{\mathbb{E}}{x}{\mathbb{V}}[ \lRet{V}] ]]}} \\
      {\equiv} & \langle V \in \V_{0} \text{ y definición de llenado de agujeros} \rangle \\
      & {\cost{[[ \substTVal{\mathbb{E}[ \lRet{V} ]}{x}{\mathbb{V}[ \lRet{V} ]} ]]}} \\
      {\equiv} & \langle \text{ definición de \(W_1\)} \rangle \\
      & {\cost{[[ \substTVal{\mathbb{E}[ \lRet{V} ]}{x}{W_1} ]]}}
        \end{array}
    \]
    \end{itemize}

Como resultado tenemos que:
\[
  \begin{array}{c}
    \cost{[[ \mathbb{D}[\checkmark M ] ]]}
    >>= W \mapsto \cost{[[ \substTVal{\mathbb{E}[\checkmark M ]}{x}{W} ]]}\\
    {\cost{\Gamma_\bot} \mathcal{R}_\V}\\
    \cost{[[ \mathbb{D}[ \lRet{V} ] ]]} >>=
    W \mapsto \cost{[[ \substTVal{\mathbb{E}[ \lRet{V} ]}{x}{W} ]]}\\
  \end{array}
\]

Los demás casos siguen una estructura similar:
intercambiando la substitución con el llenado de agujeros utilizando el hecho que
\(M\) es cerrado y aplicando la hipótesis coinductiva.
% The other cases follow a similar structure, swapping substitution with
% hole-filling taking advantage of the fact that \(M\) is a closed term and
% applying the co-inductive hypothesis as necessary.
\end{itemize}

Finalmente mostramos que la relación \(\mathcal{R}_{\V}\) respeta valores.
%
Sea \(\mathbb{V}\) un contexto cerrado de valores tal que
\(\mathbb{V}[ \checkmark M ] \mathcal{R}_{\V} \mathbb{V}[ \lRet{V} ] \), y \(W \in \KV\), veamos que:
%
\begin{displaymath}
  (\lApp{\mathbb{V}[ \checkmark M ]}{W}) \mathrel{\mathcal{R}_{\T}} (\lApp{\mathbb{V}[\lRet{V} ]}{W})
\end{displaymath}
%
Que se desprende del hecho que al ser \(W \in \KV\) y \(\mathbb{V}\) es un
contexto cerrado de valores, tenemos que \(\lApp{\mathbb{V}}{W}\) es un contexto
cerrado de valores.
%
Por hipótesis coinductiva y definición de la operación de llenado de agujeros,
tenemos que:
%
\begin{displaymath}
  (\lApp{\mathbb{V}}{W})[ \checkmark M ] \mathrel{\mathcal{R}_{\T}} (\lApp{\mathbb{V}}{W})[\lRet{V} ]
\end{displaymath}
%
Como consecuencia de la prueba anterior podemos concluir que la relación entre
\(\lambda\)-términos y valores cerrados \((\mathcal{R}_{\T},\mathcal{R}_{\V})\) es una simulación de
mejoras.

Dando un paso hacía atrás, todavía nos queda por probar que CSE directo es una
mejora cuando la evaluación del término cerrado \(M\) alcanza un valor.
%
Para esto, definimos la siguiente relación entre términos y valores del
lenguaje:
%
\begin{align*}
  {DCSE}_\T & \triangleq \mathcal{R}_\T \\
            & \cup \{ ({\letIn{x}{M}{\mathbb{C}[\checkmark M]}},{\letIn{x}{M}{\mathbb{C}[\lRet{x}]}}) \\
            & \mid \mathbb{C} \text{ es cerrado de términos y } x \text{ es una variable fresca} \} \\
  {DCSE}_\V & \triangleq \mathcal{R}_\V
\end{align*}
%
Para ver por que el par de relaciones recién definidas son una simulación de
mejoras, nos concentramos en el nuevo caso que agregamos, ya que el resto es el
lema auxiliar ya probado.
%
Sea \(\mathbb{C}\) un contexto cerrado de términos y \(x\) una variable fresca.
%
Recordemos además la hipótesis sobre \(M\):\(\cost{[[ M ]]} =
\iota_l(V,m)\).
%
Veamos que:
%
\begin{displaymath}
  \cost{[[ \letIn{x}{M}{\mathbb{C}[\checkmark M]} ]]}
  \mathrel{\cost{\Gamma_\bot} {DCSE}_\V}
  \cost{[[ {\letIn{x}{M}{\mathbb{C}[\lRet{x}]}} ]]}
\end{displaymath}
Aplicando la definición de la evaluación, y por transitividad, es equivalente a
mostrar que:
\begin{displaymath}
  \checkmark^{m+1} \cost{[[ \mathbb{C}[\checkmark M] ]]}
  \mathrel{\cost{\Gamma_\bot} {DCSE}_\V}
  \checkmark^{m+1} \cost{[[ \mathbb{C}[\lRet{V}] ]]}
\end{displaymath}

Dado que \(\mathbb{C}[\checkmark M ] \mathrel{\mathcal{R}_\T} \mathbb{C}[\lRet{V}]\)
y por definición de simulación de mejoras, tenemos que:
\begin{displaymath}
  \cost{[[ \mathbb{C}[\checkmark M ] ]]}
  \mathrel{\cost{\Gamma_\bot} \mathcal{R}_\V}
  \cost{[[ \mathbb{C}[\lRet{V}] ]]}
\end{displaymath}
%
Ya que \(\mathcal{R}_{\V} \subseteq {DCSE}_{\V}\) y \(\cost{\Gamma_{\bot}}\) es un relacionador
tenemos que:
%
\begin{displaymath}
  \cost{[[ \mathbb{C}[\checkmark M ] ]]}
  \mathrel{\cost{\Gamma_\bot} {DCSE}_\V}
  \cost{[[ \mathbb{C}[\lRet{V}] ]]}
\end{displaymath}
%
Agregamos los ticks que faltan \((\checkmark^{m + 1})\) ya que la operación de tick es
monotónica, probando que \(({DCSE}_\T,{DCSE_\V})\) es una simulación de mejoras.

De esta manera, acumulando los resultados, mostramos que CSE Directo es una
mejora proveyendo una simulación de mejora para cada contexto en el sistema
parcial.
\end{proof}

%%% Local Variables:
%%% TeX-master: "../Thesis.tex"
%%% TeX-PDF-mode: t
%%% ispell-local-dictionary: "es"
%%% End:
