\section{Efectos Algebraicos}\label{sec:effectful:algeffs}

La idea de esta sección es introducir al lector a los conocidos efectos
algebraicos y dar una presentación técnica de los mismos.

Moggi introduce una forma general de efectos computacionales encapsulados vía el
concepto de \emph{mónadas} \(T : \pmb{C} -> \pmb{C}\), donde el \(T(x)\)
representa el tipo de computaciones con elementos en \(x\).

Las operaciones que generan los efectos algebraicos vienen, entonces,
presentadas por una signatura  \emph{algebraica} de un solo \emph{sort}
\(\Sigma\), donde una operación de aridad \(n\) \(f \in \Sigma\) denota
semánticamente la familia de morfismos:
\[
  f_{x} : {T(x)}^{n} -> T(x)
\]


Sintácticamente podemos introducir las operaciones de \(\Sigma\) en nuestro
pequeño lenguaje de juguete:
\begin{definition}[Lenguaje con efectos algébricos]\label{def:effectful:lang}
  Dado un conjunto numerable de variables \(\Var\) y \((\Sigma,\alpha)\) una
  álgebra.

  Definimos el conjunto de valores \(\V_{\Sigma}\):
  \[ V, W  \mathref{::=} x \mid (\lAbs{x}{M}) \]

  Definimos el conjunto de términos \(\T_{\Sigma}\) con \(\sigma \in \Sigma\) y
  \(\alpha(\sigma) = n\):
  \[
    M,N \mathrel{::=} \lRet{V}
    \mid (\lApp{V}{W})
    \mid \letIn{x}{M}{N}
    \mid \sigma(M_{1}, \ldots, M_{n})
  \]
  \end{definition}

Lo que haremos ahora es entonces darle contenido semánticos a las nuevas
operaciones, es decir, cómo evaluaremos las operaciones recién introducidas?

Vamos a respetar las mismas convenciones ya establecidas en la
Sección~\ref{sec:prelim:lenguaje}.
%
Ya que introducimos nuevas construcciones sintácticas debemos entonces definir
las mismas operaciones que ya definimos anteriormente.

Recordar entonces que introducimos operaciones sin contenido semántico ni
sintáctico más allá que sabemos que son tokens que representan operadores con
una cierta aridad.

% Definimos entonces un algoritmo para definir las variables ligadas en términos y
% valores de la siguiente forma:
% \[
%   \begin{array}{lcl}
%     BV_{{\V}_{Sigma}} &:& {\V}_{Sigma} -> \Var \\
%     BV_{{\V}_{Sigma}}(x) & =& \emptyset \\
%     BV_{{\V}_{Sigma}}(\lAbs{x}{M}) &=& \{x\} \cup BV_{{\T}_{Sigma}}(M) \\
%     \\
%     BV_{{\T}_{Sigma}} &:& {\T}_{Sigma} -> \Var \\
%     BV_{{\T}_{Sigma}}(v) &=& BV_{{\V}_{Sigma}}(v) \\
%     BV_{{\T}_{Sigma}}(\lApp{V}{W}) &=& BV_{{\V}_{Sigma}}(V) \cup BV_{V}(W) \\
%     BV_{{\T}_{Sigma}}(\letIn{x}{M}{N}) &=& BV_{{\T}_{Sigma}}(M) \cup BV_{{\T}_{Sigma}}(N) \cup \{x\}\\
%     BV_{{\T}_{Sigma}}(\sigma(M_{1}, \ldots, M_{n})) = \bigcup_{i \in n} BV_{{\T}_{Sigma}}M_{i}
%   \end{array}
% \]

Definimos el conjunto de variables libre en\tonces como:
\[
  \begin{array}{lcl}
    FV_{{\V}_{\Sigma}} &:& {\V}_{\Sigma} -> \Var \\
    FV_{{\V}_{\Sigma}}(x) & =& \{x\} \\
    FV_{{\V}_{\Sigma}}(\lAbs{x}{M}) &=& FV_{{\T}_{\Sigma}}(M) \setminus \{x\} \\
    \\
    FV_{{\T}_{\Sigma}} &:& {\T}_{\Sigma} -> \Var \\
    FV_{{\T}_{\Sigma}}(v) &=& FV_{{\V}_{\Sigma}}(v) \\
    FV_{{\T}_{\Sigma}}(\lApp{V}{W}) &=& FV_{{\V}_{\Sigma}}(V) \cup FV_{V}(W) \\
    FV_{{\T}_{\Sigma}}(\letIn{x}{M}{N}) &=& FV_{{\T}_{\Sigma}}(M) \cup
                                           (FV_{{\T}_{\Sigma}}(N) \setminus
                                           \{x\}) \\
    FV_{{\T}_{\Sigma}}(\sigma(M_{1}, \ldots, M_{n})) &=& \bigcup_{i \in n} {FV_{{\T}_{\Sigma}}(M_i)}
  \end{array}
\]

Definir además la substitución como:
\[
  \begin{array}{lcl}
    \substTVal{V}{v}{W} & = & \substVVal{V}{v}{W}\\
    \substTVal{(\lApp{V}{V'})}{v}{W} & = & \lApp{(\substVVal{V}{v}{W})}{(\substVVal{V'}{v}{W})} \\
    \substTVal{(\letIn{x}{M}{N})}{v}{V} & = & \letIn{x}{\substTVal{M}{v}{W}}{\substTVal{N}{v}{W}}\\
    \substTVal{\sigma(M_{1}, \ldots, M_{n})} & = &
                                                   \sigma(\substTVal{M_{1}}{v}{W},
                                                   \ldots,\substTVal{M_{n}}{v}{W})
    \end{array}
\]

Y por supollo que el lema de substitución se mantiene.

Lo que nos queda entonces definir la evaluación de los términos, y para eso es
necesario saber como evaluar los operadores definidos en \(\Sigma\).
%
Para esto entonces mezclaremos dos bloques fundamentales las mónadas, utilizadas
para interpretar los efectos, y los dominios, utilizados para denotar los programas.

La construcción básica de los dominios es la noción de orden, comenzaremos por
definir una noción de mónadas ordenadas.

\begin{definition}[Orden sobre una Mónada, Mónada Ordenada]
  Sea \(T\) una mónada.
  %
  Un orden sobre \(T\) es una familia de relaciones \(\sqsubseteq\) y de elementos
  \(\bot\) tal que para cada conjunto \(X\), \({\bot}_{X} \in T(X)\) ,
  \(({\sqsubseteq}_{T(x)}) \subseteq T(X) \times T(X)\),
  \((T(X),{\sqsubseteq}_{T(x)}, {\bot}_{X})\) forma un \(\omega\)\textbf{CPPO} y
  el operador bind de la mónada es continuo en ambos argumentos:
  \[
    \bigsqcup_{n < \omega} (x >>= f_n) = x >>= ( \bigsqcup_{n < \omega} f_n )
  \]
  \[
    \bigsqcup_{n < \omega} (a_n >>= f) = (\bigsqcup_{n < \omega} a_n) >>= f
  \]

  Donde \(\{f_n\}_{n < \omega}\) es una cadena de funciones \(f_n : X -> T(Y)\)
  y \(\{a_n\}_{n < \omega}\) es una cadena de elementos \(a_n \in T(X)\).
  \end{definition}

\martin{Nota, definir cadena de funciones... \(X\) ahí arriba no tiene por que
  tener orden, mientras que \(T(X)\) sí.}

Definimos entonces lo que es un álgebra para una mónada.
%
Recordemos que la idea de tener un álgebra es la de otorgarle una función que
interpreta cada uno de los operadores introducidos en la signatura.

\begin{definition}[Mónada para \(\Sigma\)-Álgebra]
  Dada una signatura \(\Sigma\) y una mónada \(T\).
  %
  Una \(\Sigma\)-Álgebra para la mónada \(T\) es una familia de
  \(\Sigma\)-Álgebras \(\Phi\) de forma tal que para todo conjunto \(X\),
  \(\Phi_{X}\) es una \(\Sigma\)-Álgebra para \(T(X)\).
  \end{definition}