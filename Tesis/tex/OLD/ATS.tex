
%% Transition System.
\subsection{Sistemas de Transición Aplicativos}
\martin{Sacar STAs}

La teoría desarrollada resulta un caso particular de un teoría operacional
denominada \emph{Transition Systems}.
%
Primero identificaremos un evaluador genérico de términos del cálculo lambda de
forma abstracta, como simplemente un conjunto y una función parcial, que dado un
término construye una función entre ellos.
%
De esta manera tenemos una noción de evaluación parcial, donde los valores son
denotados como funciones.
%
\begin{definition}
  Un \emph{quasi-applicative transition system}~(quasi-ats) es un par,
\((A, ev)\), tales que \(A\) es un conjunto y \(ev\) es una función parcial, que
toma elementos en \(A\) y los interpreta como una función de \(A\) en \(A\).
  \[ ev : A \rightharpoonup (A \to A) \]
  \end{definition}

% Notar que la noción de función parcial podemos interpretarla también como una
% función \(evT : A \to (A \to A) + \bot \), tal que:
% \begin{align*}
%   \forall x \in A, x \notin dom(ev), evT(x) & \doteq & \iota_{r}(\bot) \\
%   \forall x \in A, x \in dom(ev), evT(x) & \doteq & ev(x)
%   \end{align*}
% \martin{maybe we can make this analysis at the end of this section}

Para simplificar la presentación, durante esta sección, utilizaremos las
siguientes notaciones, principalmente ya que nos interesa observar la
convergencia de la evaluación de un valor.
%
\begin{notation}
  Notaci\'on para simplificar definiciones posteriores.
  \begin{itemize}
    \item \(a \Downarrow f \equiv  a \in dom(ev) \land ev(a) = f \)
    \item \(a \Downarrow \equiv a \in dom(ev)\)
    \item \(a \Uparrow \equiv a \notin dom(ev)\)
    \item Dada una relación \(R \subseteq A \times A\), \(f,g \colon B \to A\) dos funciones,
notamos a \({\overrightarrow{R}} \subseteq B \times B\) como el predicado:
\(\forall b \in B, f(b) \mathrel{R} g(b)\)
  \end{itemize}
  \end{notation}
\martin{Check that everything is alright}

Siguiendo entonces la intuición presentada en la sección anterior, podemos
caracterizar la idea de \emph{experimentar} con un término para poder observar
si su evaluación converge como un operador entre relaciones.
%
Podemos entonces definir un operador de simulación aplicativa de la siguiente
manera:
\begin{definition}[Operador de Simulación Aplicativa]
  Sea \((A, ev)\) un quasi-ats.
  %
  Definimos un operador \(F\), \(F \colon Rel(A) \to Rel(A)\), de la siguiente
manera:
  \[
    F(R) = \{ (a,b) : a \Downarrow f \implies b \Downarrow g \land f \mathrel{\overrightarrow{R}} g \}
  \]
  \end{definition}
%
Dada una relación \(R\), podemos agregarle un paso al experimento \(F(R)\),
donde se relacionan los términos en base a poder relacionar la observación de
que una evaluación converge entonces la otra también, y sus valores están
relacionados por \(R\).

Definimos entonces una relación es una simulación aplicativa si cumple con la
propiedad del operador de simulación aplicativa.
%
\begin{definition}[Simulación Aplicativa]
  Sea \((A,ev)\) un quasi-ats, y \(R \subseteq Rel(A)\) una relación en \(A\).
  %
  La relación \(R\) es una simulación aplicativa si \(R \subseteq F(R)\).
  \end{definition}

Definimos la aproximación aplicativa directamente como la existencia de una
simulación aplicativa que relacione a los términos:
\[
  a {\sqsubseteq}^{B} b \doteq a \mathrel{R} b \land R \text{ es una bisimulación aplicativa}
\]
%
O de forma más concisa:
%
\[
  ({\sqsubseteq}^{B}) \equiv \bigcup \{ R \in Rel(A) : R \subseteq F(R) \}
\]
%
Podemos caracterizar la relación de aproximación aplicativa coinductivamente.
%
La aproximación aplicativa, \({\sqsubseteq}^{B}\), está bien definida porque es el punto
fijo dado por el teorema de Tarski-Banach~\ref{thm:knaster:tarski}, y además
el operador de bisimulación aplicativa es mon\'otono en el reticulado de conjuntos
ordenados por la inclusión de conjuntos.
%
\begin{lemma}
  El operador \(F\) es mon\'otono.

  \begin{proof}
    Sea \(R,S \subseteq A \times A\) tales que \(R \subseteq S\).
    %
    Queremos probar que \(F(R) \subseteq F(S)\).
    %
    Sean \(a,b \in A, a \mathrel{F(R)} b\), que por definición de \(F\) es equivalente a
    \[
      a \Downarrow f \implies b \Downarrow g \land f \overrightarrow{R} g
    \]

    Queremos probar entonces que \(a \mathrel{F(S)} b\).
    %
    Supongamos entonces que \(a \Downarrow f\), y por la definición anterior, tenemos que
\(b \Downarrow g\) y que además, \(f \overrightarrow{R} g\).
%
Dado que \(R \subseteq S\), tenemos que \(\overrightarrow{R} \subseteq \overrightarrow{S}\) para
todo par de funciones \(f,g\).
%
Por lo que tenemos que \(a \mathrel{F(S)} b\).
    \end{proof}
  \end{lemma}

La relación \(({\sqsubseteq}^{B})\) está bien definida ya que es el mayor punto fijo
del operador \(F\).
%
Resultado que además nos provee con sus lemas de inducción y coinducción.
%%%

Definimos entonces la noción de simulación aplicativa como un sistema quasi-ats
donde se incluye un operador de aplicación cuya evaluación respeta además la
relación de simulación aplicativa.
%
Dicho de otra manera, los valores resultantes de la evaluación, interpretados
como funciones, son monótonos respecto a \(({\sqsubseteq}^{B})\).
%
\begin{definition}[Applicative Transition System]
  Sea \((A,ev)\) un quasi-ats.
  %
  Decimos que \((A,ev)\) es un \emph{applicative transistion system}~(ats) si
además la interpretación de elementos de \(A\) respeta la aproximación
aplicativa.
  \[
    \forall a, b, c \in A, a \Downarrow f \land b {\sqsubseteq}^{B} c \implies f(b) \mathrel{{\sqsubseteq}^{B}} g(c)
  \]
  \end{definition}

Ahora entonces procederemos a conectar la noción de aplicación dentro del
lenguaje con la noción de divergencia y la de aplicación de funciones por fuera
del lenguaje.
%
\begin{definition}[Quasi-estructura aplicativa con divergencia]
  Una quasi-estructura aplicativa con divergencia es una tripla \((A, \cdot, \Uparrow)\)
tales que \((A, \cdot)\) es una estructura aplicativa, y \((\Uparrow) \subseteq A\) es un predicado
de divergencia que satisface:
  \[
    x \Uparrow \implies (x \cdot y ) \Uparrow
  \]
  \end{definition}

Dada una quasi-estructura aplicativa con divergencia \((A, \cdot, \Uparrow)\), podemos
definir una relación entre elementos de \(A\) de la siguiente forma:
%
\[
  a {\sqsubseteq}^{A} b \doteq a \Downarrow \implies b \Downarrow \land \forall c \in A, {a \cdot c} \mathrel{{\sqsubseteq}^{A}} {b \cdot c}
\]
%
La relación entre valores de \(A\) \({\sqsubseteq}^{A}\) se caracteriza mediante el mayor
punto fijo de un operador monótono entre relaciones, similar a la simulación
aplicativa.

Finalmente, podemos utilizar la noción de quasi-estructuras aplicativas con
divergencia para caracterizar la simulación aplicativa.
%
\begin{lemma}
  Sea \(\mathbb{B} = (A, ev)\) un ats, definimos entonces a \(\mathbb{A} = (A, \cdot, \Uparrow)\)
  de la siguiente forma:
  \[
    a \cdot b \doteq \begin{dcases}
      a & a \Uparrow \\
      f(b) & a \Downarrow f
              \end{dcases}
  \]

  Entonces,
  \[
    \mathrel{a {\sqsubseteq}^{A}} b \iff a \mathrel{{\sqsubseteq}^{B}} b
  \]

  También es posible recuperar a \(\mathbb{B}\) a partir de \(\mathbb{A}\) de la
siguiente forma:
\[
  ev(a) \doteq \begin{dcases}
    b \mapsto a \cdot b & a \Downarrow \\
    undefined & a \Uparrow
    \end{dcases}
  \]

  Finalmente, el operador \((\cdot)\) es compatible con \({\sqsubseteq}^{B}\):
  \[
    a \mathrel{{\sqsubseteq}^{B}} b \land c \mathrel{{\sqsubseteq}^{B}} d \implies a \cdot c \mathrel{{\sqsubseteq}^{B}} b \cdot d
  \]
  \end{lemma}

Aquellos lectores interesados en esta l\'inea de trabajo, como a su vez en un
tratado a fondo de los resultados presentados en esta sección, están invitados a
visitar la fuente original~\parencite{Abramsky.1990.LazyLambdaCalculus}.
