\chapter{Teoría de Mejoras con Efectos}\label{ch:improvement}

En este capítulo vemos el aporte principal de la tesis, donde presentamos
\emph{una forma} de realizar análisis de propiedades intensivas sobre la
evaluación de términos en presencia de efectos algebraicos.
%
Esto lo logramos introduciendo primero un nuevo efecto: la computación del costo
de la evaluación de un término acumulando \emph{ticks}.
%
Luego utilizamos un nuevo relacionador para comparar los costos de la evaluación de
un término con otro, y así, obtener una relación de mejoras entre programas con
efectos.
%
Esta simple idea nos permite derivar desde la definición de equivalencia entre
programas la definición de mejoras tal cual dada por
Sands~\parencite*{Sands:HOOTS}.

% Lamentablemente al derivar la definición de otros lenguajes con efectos nos
% encontramos con que la definición no es tan \emph{granular} como se espera.
% %
% Es decir, hay elementos que se los observa como del mismo costo pero que en
% realidad podrían tener diferentes costos.
% %
% Por ejemplo, no es lo mismo fallar luego de utilizar \(100\) unidades de cómputo
% que luego de utilizar \(10000\).
% %
% Para subsanar esto vemos como podemos adaptar la evaluación de términos para
% obtener una definición de mejoras más granular y así poder discernir cuando se
% alcanza el mismo fallo con menor costo.

\section{Análisis Intensivo Derivado}\label{sec:dev:cost}

Exploramos las definiciones derivadas a partir de introducir el análisis de
costos de la evaluación de términos dentro de las relaciones de aproximación
contextual y de la simulación aplicativa.
%
Lo que hacemos es adicionar como un efecto el cómputo de costo de la evaluación,
y luego, introducimos la observación del mismo para obtener dos nuevas
relaciones: \emph{similaridad aplicativa con efectos y costos} y \emph{mejoras
con efectos}.
%
Para esto, agregamos una nueva operación llamada \emph{tick} (\(\checkmark\)) que agrega
una unidad de costo a la evaluación de un término.
%
De esta manera llevamos a cabo el análisis de costo simplemente
acumulando los ticks necesarios por la evaluación de los términos y poder así
compararlos al finalizar la evaluación.

Para simplificar la presentación de los resultados, asumiremos que tenemos una
signatura \(\Sigma\) equipada con un sistema \((T,\Gamma)\), que quedará fijo durante toda
la sección.

\subsection{Costos en la evaluación de Términos}\label{sec:cost:interno}

Para agregar el efecto de acumular el costo de evaluar un término del lenguaje,
lo que hacemos es seguir la idea intuitiva de simplemente equipar a cada
elemento con un número natural que representa el costo requerido para
computarlo.

Para simplificar la notación primero presentamos las operaciones sobre costo.
%
Notamos con \(\mathbb{N}_{\infty}\) al conjunto de los números naturales con un elemento
adicional.
%
Este elemento adicional, \(\infty\), denota al supremo del conjunto \(\mathbb{N}_{\infty}\).

\begin{definition}\label{def:fam:add}
  Sea \(X\) un conjunto, definimos una familia de funciones \(add_{n}\) indexada
por \(n \in \mathbb{N}\) de la siguiente forma:
%
\begin{align*}
  add_{n} & : (X \times \mathbb{N}_{\infty}) \to (X \times \mathbb{N}_{\infty}) \\
  add_{n}(t,c) & = (t, c + n)
\end{align*}
  \end{definition}

Definimos la familia de funciones que dentro de la mónada \(T\)
acumula los ticks que se han encontrado hasta el momento:
\begin{align*}
  {\checkmark}^{n} & : T(X \times \mathbb{N}_{\infty}) \to T (X \times \mathbb{N}_{\infty}) \\
  {\checkmark}^{n} & = T \, add_{n}
  \end{align*}

Finalmente, definimos una mónada basándonos en la mónada \(T\) de forma tal
que equipe a todo elemento dentro de la mónada con un número natural
representando el costo requerido para computarlo.
%
Valores puros (sin efectos) son introducidos con un costo \(0\), pero al
utilizar el operador \(\<bind>\), sumamos el costo de computar su primer
argumento con el costo de computar el segundo.

\begin{definition}[Mónada de Costo Interno]\label{def:cost:monad}
  Definimos la mónada \(T_{\mathbb{N}_{\infty}} = (T_{\mathbb{N}_{\infty}}, \eta, >>=)\) de la siguiente forma:
  %
  \begin{align*}
    T_{\mathbb{N}_{\infty}}(X) &= T(X \times \mathbb{N}_{\infty})\\%~\label{def:int:monad}\\
    \eta_{T_{\mathbb{N}_{\infty}}}(x) &= \eta_{T}(x, 0)\\%~\label{def:int:monad:eta}\\
    m \mathrel{{>>=}_{\mathbb{N}_{\infty}}} f &= m \mathrel{{>>=}_{T}} (v,c) \mapsto {\checkmark}^{c}(f(v))%~\label{def:int:monad:nu}
    \end{align*}
  \end{definition}

% Dentro de la definición, tenemos que \(T_{\mathbb{N}_{\infty}}\) es morfismo
% dentro de la categoría de conjuntos, mientras que las
% segundo~\ref{def:int:monad:eta} y el tercero~\ref{def:int:monad:nu}, definen los
% operadores de la mónada utilizando los operadores mónadicos de la mónada \(T\).
%
Esta forma de \emph{transformar} una mónada en otra es un método muy conocido
dentro de la comunidad del lenguaje de programación Haskell, y se la puede
encontrar bajo el nombre de \emph{transformador de mónada writer con el monoide aditivo
de los números naturales}~\parencite{Liang:MTrans,JM:TCS:2010}.
%
Otras combinaciones de transformadores y mónadas podrán dar lugar a otros
análisis de costos, estudiaremos diferentes nociones en la
Sección~\ref{sec:alt:cost}.
%
De todas maneras, el transformador de mónadas writer tiene la ventaja, como
veremos, que nos permite introducir análisis de costo de programas \emph{sin
tener que modificar el sistema}.
%
En otras palabras, de esta manera derivamos de la definición de aproximación de
programas un análisis de costos simplemente equipando cada elemento con el costo
necesario para computarlo.

Debido a que utilizamos un transformador de mónadas, que lo que nos permite es
componer dos mónadas, primero vamos a estudiar el comportamiento de forma local,
introduciendo la noción de costos utilizando un monoide aditivo y luego las
compondremos utilizando el transformador.

\begin{lemma}~\label{lmm:product:relacionador}
  Dado un monoide \((M, \cdot, e)\) aditivo y un preorden \((\sqsubseteq) \subseteq M \times M\) compatible
con la operación de \(M\).
  %
  La relación obtenida del producto punto a punto con \((\sqsubseteq)\), \(({-} \, \times \sqsubseteq)\), es
un relacionador monádico para la mónada producto con \(M\), \((- \times M)\).
%
  \end{lemma}
\begin{proof}
La prueba consiste de dos partes: la primera es mostrar que es efectivamente un
relacionador para el funtor subyacente a la mónada producto, y luego que es un
relacionador mónadico.

La relación producto con \((\sqsubseteq)\) define un relacionador para el funtor
subyacente de la mónada producto con \(M\).
%
Para esto debemos mostrar que se cumplen las \(4\) propiedades de los relacionadores.
%
Sean \(X,Y,Z\) tres conjuntos.
\begin{itemize}
  \item Contención de la identidad con efectos: \(1_{X \times M} \subseteq 1_{X} \times \sqsubseteq\).
  %
  Sea \((x,c) \in X \times M\), dado que la relación \((\sqsubseteq)\) es un preorden, entonces es
reflexiva, y por lo tanto, \((x,c) \mathrel{1_{X} \times \sqsubseteq} (x, c)\).
  \item Para cualquier relación \(R \subseteq X \times Y\) y \(S \subseteq Y \times Z\),
\((R \, \times \sqsubseteq) \circ (S \, \times \sqsubseteq) \subseteq ((R \circ S) \, \times \sqsubseteq)\).

    Sea \((x,p) \in X \times M\) y \((z,r) \in Z \times M \) tales que
\[(x,p) \mathrel{(R \, \times \sqsubseteq) \circ (S \, \times \sqsubseteq)} (z,r)\]
%
Por definición de composición de relaciones tenemos que existen
\(y \in Y\) y \(q \in M\) tal que:
        \[
        (x,p) \mathrel{(R \, \times \sqsubseteq)} (y,q) \land (y,q) \mathrel{(S \, \times \sqsubseteq)} (z,r)
        \]
        %
        Aplicando la definición del producto de relaciones tenemos que:
        \[
        x \mathrel{R} y \land (p \sqsubseteq q)
        \land y \mathrel{S} z \land (q \sqsubseteq r)
        \]
        Por commutatividad del operador lógico \((\land)\), transitividad de \((\sqsubseteq)\) y definición de composición de relaciones, tenemos finalmente que:
        \[
        (x,p) \mathrel{((R \circ S) \, \times \sqsubseteq)} (z,r)
        \]
  \item Para cualquier conjunto \(W,Z\) y función \(f : W \to X, g : Z \to Y\),
        tenemos que:
        %
        \[ ((f \times g)^{-1}R \times \sqsubseteq) \subseteq ((\_ \times 1_{M}) \, f \times (\_ \times 1_{M}) \, g)^{-1} (R \, \times \sqsubseteq)\]
        %
        Sean además \(w \in W\),\(z \in Z\), \(p,q \in M\), tales que:
        %
        \[
        (w,p) \mathrel{((f \times g)^{-1}  R \, \times \sqsubseteq)} (z,q)
        \]
        %
        Aplicamos la definición de producto de relaciones
        \[
        w \mathrel{((f \times g)^{-1} R)} z \land p \sqsubseteq q
        \]
        Por definición de aplicación inversa de funciones, tenemos que
existen \(x \in X, y \in Y\) tales que \(f(w) = x\) y \(g(z) = y\) y además:
        \[
        f(w) \mathrel{R} g(z) \land p \sqsubseteq q
        \]
        Más aún, por definición de funtor de la mónada de producto, tenemos que
\((\_ \times 1_{M}) \, f (w,p) = (f(w), p)\) y  \((\_ \times 1_{M}) \, g (z,q) = (g(z), q)\), y por lo tanto:
        \[
        ((\_ \times 1_{M}) \, f \, (w,p)) \mathrel{R \, \times \sqsubseteq} ((\_ \times 1_{M}) \, g \, (z,q))
        \]
        Finalmente aplicamos la definición las inversas de las funciones
mapeadas por el funtor de la mónada producto:
        \[
        (w,p) \mathrel{( {(\_ \times 1_{M}) f} \times {(\_ \times 1_{M}) g} )^{-1} (R \, \times \sqsubseteq)} (z,q)
        \]
  \item Por último, para toda relación \(T \subseteq X \times Y\) tal que
\(S \subseteq T\), \((S \, \times \sqsubseteq) \subseteq (T \, \times \sqsubseteq)\).
%
        Asumiendo que \(S \subseteq T\), sean \(x \in X\), \(y \in Y\), y \(p, q \in M\), tales que:
        \[
        (x,p) \mathrel{S \, \times \sqsubseteq } (y,q)
        \]
        Por definición de producto de relaciones, tenemos que:
        \[
        x \mathrel{S} y \land p \sqsubseteq q
        \]
        Más aún, dado que \(S \subseteq T\), tenemos que:
        \[
        x \mathrel{T} y \land p \sqsubseteq q
        \]
        Finalmente por definición de producto de relaciones, tenemos que:
        \[
        (x,p) \mathrel{(T \, \times \sqsubseteq)} (y,q)
        \]
        %
        Lo que nos permite concluir que \((S \, \times \sqsubseteq) \subseteq (T \, \times \sqsubseteq)\).
  \end{itemize}

Lo que nos queda por mostrar es que el relacionador producto con \((\sqsubseteq)\) además
es mónadico.
%
Es decir, que extiende el comportamiento de las operaciones de la mónada
resultante de hacer el producto con el monoide \(M\).
%
Sean \(X', Y'\) conjuntos, \(f : X \to (X' \, \times M)\) y \(g : Y \to (Y' \, \times M)\) dos
funciones, y \(R \subseteq X \times Y\) y \(S \subseteq X' \times Y'\) dos relaciones.
%
\begin{itemize}
   \item La inyección de valores respeta relaciones mediante el
relacionador.
%
        Sean \(x \in X\) e \(y \in Y\) tales que \(x \mathrel{R} y\), tenemos que \(\eta_{X \, \times M}(x) = (x, e)\) y \(\eta_{Y \, \times M}(y) = (y, e)\).
        %
        Al ser la relación \((\sqsubseteq)\) un preorden, es reflexiva, y por lo tanto \(e \sqsubseteq e\).
        %
        Concluimos que \((x,e) \mathrel{R \, \times \sqsubseteq}(y,e)\).
  \item Sea \((x,p) \in (X \times M)\) y \((y,q) \in (Y \times M)\) tales que
\((x,p) \mathrel{R \, \times \sqsubseteq} (y,q)\), o equivalentemente,
\(x \mathrel{R} y \land p \sqsubseteq q\).
%
        Asumimos que para todos \(x \in X\) e \(y \in Y\) tales
que \(x \mathrel{R} y\), tenemos que \(f(x) \mathrel{S \, \times \sqsubseteq} g(y)\).
%
        Tenemos que mostrar que \(((x,p) >>= f) \mathrel{S \, \times \sqsubseteq} ((y,q) >>= g)\).
        %
        Por definición del operador \(\<bind>\) de la mónada, esto es equivalente a probar que:
        \[
        ((x,p) >>= f) \mathrel{S \, \times \sqsubseteq} ((y,q) >>= g)
        \]
        %
        aplicando la definición del operador \(\<bind>\) de la mónada de
producto tenemos
        \[
        (1_{X}, p \cdot)(f(x)) \mathrel{(S \, \times \sqsubseteq)} (1_{Y}, q \cdot) (g(y))
        \]
        %
        Sean \((x', p') \in (X' \times M)\) y \((y', q') \in (Y' \times M)\) tales que
\(f(x) = (x',p')\) y \(g(y) = (y', q')\), y además, dado que
\(x \mathrel{R} y\), \((x', p') \mathrel{(S \, \times \sqsubseteq)} (y', q')\), tenemos que
probar entonces:
        %
        \[
        (x', p \cdot p') \mathrel{(S \, \times \sqsubseteq)} (y', q \cdot q')
        \]
        equivalentemente
        \[
        x' \mathrel{S} y' \land (p \cdot p') \sqsubseteq (q \cdot q')
        \]
        Por un lado tenemos que \(x' \mathrel{S} y'\) por ser el resultado de la
aplicación de las funciones \(f\) y \(g\).
%
        Por el otro tenemos \(p \sqsubseteq q\) y además \(p' \sqsubseteq q'\), y por ser el
preorden \(\sqsubseteq\) compatible con la operación de \(M\) tenemos que
\(p \cdot p' \sqsubseteq q \cdot q'\).\qedhere
\end{itemize}
  \end{proof}

Para el resto de la sección, asumimos que tenemos un monoide aditivo
\((M, \cdot, e)\) con un preorden que respete la operación del monoide.
%
Notaremos además a la mónada \(T \circ (\_ \, \times M)\) como \(\mathbb{W}_{T,M}\).

Lo que haremos entonces en lo que queda de la sección es probar que
\((\mathbb{W}_{T,M} , \Gamma \circ (\_ \, \times \sqsubseteq))\) forma un sistema válido para la signatura
\(\Sigma\).
%
En otras palabras, queremos ver que la mónada \(\mathbb{W}_{T,M}\) está ordenada
y además que \(\Gamma \circ (\_ \, \times \sqsubseteq)\) es un relacionador mónadico inductivo para
\(\mathbb{W}_{T,M}\).

\begin{lemma}
  Para toda mónada ordenada \(T\), la mónada resultante \(\mathbb{W}_{T,M}\) es
una mónada ordenada.
  \end{lemma}
\begin{proof}
  Sea \((\preceq)\) un orden de la mónada \(T\).
  %
  Por definición de mónadas ordenadas, tenemos que para cada conjunto \(X\) hay
un \(\omega\)\textbf{CPPO} \((T(X), {\preceq}_{X}, \bot_{X})\).
%
En particular, para todo conjunto \(X\), \(X \times M\) es también un conjunto, y por
lo tanto, podemos utilizar el orden definido por \((\preceq)\) para el conjunto
\(X \times M\), y así obtener un \(\omega\)\textbf{CPPO}
\((T(X  \times M), \preceq_{X \times M}, \bot_{X \times M})\).
\end{proof}

\begin{lemma}\label{lmm:cost:rel}
  El relacionador definido como la composición de \(\Gamma\) y el relacionador definido en el
Lema~\ref{lmm:product:relacionador}, \(\Gamma \circ (\_ \, \times \sqsubseteq)\), es un relacionador mónadico
para la mónada \(\mathbb{W}_{T,M}\).
  \end{lemma}
\begin{proof}
  Dado que la composición de relacionadores definen un relacionador por la
composición de funtores, tenemos que el relacionador \(\Gamma \circ (\_ \, \times \sqsubseteq)\) es un
relacionador para el funtor subyacente de la mónada \(\mathbb{W}_{T,M}\).
%

El operador de relaciones \(\Gamma \circ (\_ \, \times \sqsubseteq)\) respeta las operaciones de la
mónada \(\mathbb{W}_{T,M}\).
%
Sean \(X,X',Y,Y'\) conjuntos,
\(f : X \to \mathbb{W}_{T,M}(X')\) y \(g : Y \to \mathbb{W}_{T,M}(Y')\) dos funciones, y
\(R \subseteq X \times Y, S \subseteq X' \times Y'\) dos relaciones.

\begin{itemize}
  \item El relacionador \(\Gamma \circ (\_ \times \sqsubseteq)\) respeta la transformación natural \(\eta_{TM}\).
  %
  Sean \(x \in X\) e \(y \in Y\) dos valores tales que \(x \mathrel{R} y\), queremos
ver que \(\eta_{TM}(x) \mathrel{(\Gamma (R \, \times \sqsubseteq))} \eta_{TM}(y)\).
  \begin{align*}
    & \eta_{TM}(x) \\
    \equiv & \langle \text{definición del transformador de mónadas Writer} \rangle \\
    & \eta_{T}(x, e) \\
    \Gamma (R \, \times \sqsubseteq) & \langle \Gamma \text{ es un \(T\)-relacionador para la mónada \(T\) y }
(x,e) \mathrel{(R \, \times \sqsubseteq)}(y, e)\\
    & \eta_{T}(y,e) \\
    \equiv& \langle \text{definición del transformador de mónadas Writer} \rangle \\
    & \eta_{TM}(y) \\
    \end{align*}

  \item El relacionador \(\Gamma \circ (\_ \, \times \sqsubseteq)\) respeta el operador
\(\<bind>\) de la mónada \(\mathbb{W}_{T,M}\).
%
        Sean \(u \in \mathbb{W}_{T,M}(X), v \in \mathbb{W}_{T,M}(Y)\) tales que
\(u \mathrel{\Gamma (R \, \times \sqsubseteq)} v\) y \(f,g\) forman un homomorfismo entre \(R\) y
\(\Gamma(S \, \times \sqsubseteq)\).
    \begin{align*}
      &
        \begin{array}{l}
        (u {>>=}_{\mathbb{W}_{T,M}} f) \mathrel{\Gamma (S \, \times \sqsubseteq)} (v {>>=}_{\mathbb{W}_{T,M}} g)
        \end{array} \\
      \equiv& \langle \text{definición del transformador de mónadas Writer}\rangle \\
      &
        \begin{array}{l}
        (u {>>=}_{T} (u_{X},u_{M}) \mapsto T \, (u_{m} \cdot  \_) \, f(u_{X})) \\
        \quad \mathrel{\Gamma(S \, \times \sqsubseteq)} \\
        (v {>>=}_{T} (v_{X},v_{M}) \mapsto T \, (v_{m} \cdot  \_) \, g(v_{X}))
        \end{array}
      \end{align*}
      %
        Notamos a las funciones derivadas de \(f\) y \(g\) que además acumulan los costos computados como:
        \begin{align}
          f'(u,m) \doteq T \, (u_{m} \cdot  \_) \, f(u_{X}) \\
          g'(u,m) \doteq T \, (v_{m} \cdot  \_) \, g(v_{X})
          \end{align}
      %
        Dado que \(\Gamma\) es un \(T\)-relacionador para la mónada \(T\), y por hipótesis
tenemos que \(u \mathrel{\Gamma (R \, \times \sqsubseteq)} v\), nos queda por mostrar que
para todo \((x,m_{X}) \in (X \times M)\) e \((y,m_{Y}) \in (Y \times M)\)
tales que \((x, m_{X}) \mathrel{R \, \times \sqsubseteq} (y, m_{Y})\), tenemos que
\(f'(x,m_{X}) \mathrel{\Gamma (S \, \times \sqsubseteq)} g'(y,m_{Y})\).
%
Sea \((x,m_{X}) \in (X \times M)\) y \((y,m_{Y}) \in (Y \times M)\) tales que
\((x, m_{X}) \mathrel{R \, \times \sqsubseteq} (y, m_{Y})\).
%
El hecho que  \(f'(x,m_{X}) \mathrel{\Gamma (S \, \times \sqsubseteq)} g'(y,m_{Y})\) se desprende de
que \(\Gamma\) es un \(T\)-relacionador para la mónada \(T\), \(m_{X} \sqsubseteq m_{Y}\), y
que \(f,g\) forman un homomorfismo entre las relaciones \(R\) y \(\Gamma(S \, \times \sqsubseteq)\).
%
En símbolos tenemos que:
\begin{align*}
  & (x, m_{X}) \mathrel{R \, \times \sqsubseteq} (y, m_{Y}) \\
  \equiv& \langle \text{definición producto de relaciones} \rangle \\
  & x \mathrel{R} y \land m_{X} \sqsubseteq m_{Y} \\
  \implies & \langle \text{hipótesis} \rangle \\
  & f(x) \mathrel{\Gamma(S \, \times \sqsubseteq)} g(y) \land m_{X} \sqsubseteq m_{Y} \\
  \implies & \langle \Gamma \text{ es un \(T\)-relacionador de } T \rangle  \\
  & T \, (m_{X} \cdot \_) \, f(x) \mathrel{\Gamma(S\, \times \sqsubseteq)} T \, (m_{y} \cdot \_) \, g(y) \\
  \equiv & \langle \text{definiciones de } f',g' \rangle \\
  & f'((x, m_{x})) \mathrel{\Gamma(S \, \times \sqsubseteq)} g'((y,m_{y}))
  \end{align*}
      %
  \end{itemize}
\end{proof}

% En el lema que acabamos de mostrar, hay un paso que mostraremos más adelante, que
% para todos \(m_{X} \sqsubseteq m_{Y}\) y cuales quiera valores \(v,u\) tales que
% \(v \mathrel{\Gamma (R \, \times \sqsubseteq)} u\), tenemos que
% \(T \, (m_{X} \cdot \_) v \mathrel{\Gamma (R \, \times \sqsubseteq)} T \, (m_{Y} \cdot \_) u\).
%

La mónada \(\mathbb{W}_{T,M}\) es \(\Sigma\)-continua.
%
Se desprende directamente de que \(T\) sea \(\Sigma\)-continua.
%
Más aún, la transformación que estamos realizando \emph{restringe} los
conjuntos posibles (los valores de la categoría Set).
%
Como resultado de esta sección, tenemos que para un monoide \((M, \cdot, e)\) y un
preorden \((\sqsubseteq) \subseteq M \times M\), que respete al monoide, la mónada \(\mathbb{W}_{T,M}\)
y el relacionador mónadico \( \Gamma \circ (\_ \, \times \sqsubseteq))\) forman un \(\Sigma\)-sistema.

Finalmente, concluimos que dado un \(\Sigma\)-sistema derivamos otro sistema en
el cual se puede razonar sobre los costos de la evaluación de términos.

\subsection{Mapear Relaciones con nociones de Costo}

En esta sección definimos cómo mapear relaciones entre valores a relaciones
que además comparen los costos, con el objetivo de utilizar relaciones que
razonen sobre el costo de la evaluación.
%
Dado que equipamos valores con el costo requerido para computarlos, utilizamos
el relacionador definido en el Lema~\ref{lmm:cost:rel}, resultante de la composición
del relacionador del sistema con el orden del monoide aditivo.
%
Como a lo largo de la tesis utilizamos el mismo monoide aditivo, aquel de los
números naturales más infinito, \(\infty\), a continuación mostramos la definición
de su relacionador.

\begin{definition}[Relacionador de Costo]\label{def:relacionador:costos}
  Sean \(X,Y\) dos conjuntos y sea \(R\) una relación entre \(X\) e \(Y\).
  %
  Definimos la relación \(R_{\mathbb{N}_{\infty}} \subseteq (X \times \mathbb{N}_{\infty}) \times (Y \times \mathbb{N}_{\infty})\) como el
producto de la relación \(R\) y \((\geq_{\infty})\).
%
  En símbolos:
  \[
    (x , m) \mathrel{R_{\mathbb{\infty}}} (y , n) \iff x \mathrel{R} y \land m \geq_{\infty} n
  \]
  \end{definition}

Podemos ver en la definición anterior cómo son comparados los costos de los
valores equipados con sus costos.
%
De esta manera, dada una relación \(R\), el relacionador de costos de \(R\) lo que
hace es refinar la relación para \emph{además} comparar los costos.
%
Este relacionador fue presentado
por~\citeauthor{DalLago.2019.NormalForm}~\parencite*{DalLago.2019.NormalForm}
que codifica la definición de mejoras propuesta por
Sands~\parencite*{Sands:HOOTS}.
%
Notamos al nuevo sistema,
\((\mathbb{W}_{T,\mathbb{N}_{\infty}}, \Gamma \, \circ R_{\mathbb{N}_{T,\mathbb{N}_{\infty}}})\),
simplemente como \((T_{\mathbb{N}_{\infty}}, \Gamma_{\mathbb{N}_{\infty}})\).

Teniendo un sistema de costos, nos concentraremos en introducir un nuevo
operador a nivel del lenguaje para marcar computaciones costosas.
%
De esta manera le permitimos a los usuarios del lenguaje agregar notaciones
sobre el costo de los programas.

A continuación, probamos que no cambiamos la semántica del lenguaje al agregar
un nuevo operador para adicionar costos a nivel del lenguaje, y así poder
razonar sobre programas.
%
Es decir, introducimos un nuevo operador \(\checkmark\) a la signatura que notaremos
\(\Sigma_{\checkmark}\), y mostramos que tenemos un \(\Sigma_{\checkmark}\)-sistema.
%
El nuevo operador unario del lenguaje lo podemos interpretar como un tick
dentro de la mónada de costos, es decir directamente como la función \(\checkmark^{1}\)
definida como el mapeo de la función \(add_{1}\)~(Definición~\ref{def:fam:add}).

\begin{lemma}\label{lemma:sigmaplus:cont}
  La mónada de costo interno \(T_{\mathbb{N}_{\infty}}\) es \(\Sigma_{\checkmark}\)-continua.
  \end{lemma}
\begin{proof}
Dado que por definición \(\Sigma_{\checkmark} \equiv \Sigma \cup \{\checkmark\}\), podemos dividir la prueba en dos:
%
\begin{itemize}
  \item La mónada \(T_{\mathbb{N}_{\infty}}\) es \(\Sigma\)-continua.
  %
        Sigue de que la mónada \(T\) es \(\Sigma\)-continua.

  \item La interpretación del operador unario \(\checkmark\) es continua.
  %
        La función de interpretación \(\checkmark^{1}\) se define como el mapeo de
una función sobre valores y costos \(add_{1}\), y sumado a que las operaciones
de la mónada son continuas por definición de mónada continua, tenemos que la
función \(\checkmark^{1}\) también lo es.
  \end{itemize}
\end{proof}

En la prueba anterior se puede ver un procedimiento que utilizaremos siempre que
trabajemos con la mónada de costos interna.
%
La mónada \(\mathbb{W}_{T,\mathbb{N}_{\infty}}\) es el resultado de apilar los
efectos, pero en particular, introducir un nuevo efecto dentro de los de \(T\).
%
Por lo que al momento de realizar pruebas, en general, aplicamos la definición
de la mónada \(\mathbb{W}_{T,\mathbb{N}_{\infty}}\), exponiendo el hecho que sean efectos
apilados, y así obtenemos elementos dentro de \(T\).
%
En otras palabras, tenemos que para resultados universales dentro de \(T\),
podemos mapear dicho resultado a \(\mathbb{W}_{T,\mathbb{N}_{\infty}}\).

\begin{lemma}\label{lemma:sigmaplus:inductivo}
  El relacionador \(\Gamma_{\mathbb{N}_{\infty}}\) es un T-relacionador inductivo para la mónada
\(\Sigma_{\checkmark}\)-continua \(T_{\mathbb{N}_{\infty}}\).
  \end{lemma}
\begin{proof}
  Seguimos el mismo razonamiento que en el Lema~\ref{lemma:sigmaplus:cont}.
\end{proof}

\begin{theorem}
  El par \((T_{\mathbb{N}_{\infty}}, \Gamma_{\mathbb{N}_{\infty}})\) es un \(\Sigma_{\checkmark}\)-sistema.
  \end{theorem}
\begin{proof}
  Por Lema~\ref{lemma:sigmaplus:cont} y Lema~\ref{lemma:sigmaplus:inductivo}.
  \end{proof}

\subsection{Evaluación con Costos}\label{sec:cost:eval}

Contamos con todas las herramientas para definir la relación de
\(\Sigma_{\mathbb{N}_{\infty}}\)-similaridad aplicativa con efectos y aproximación observacional
con costos para los términos del lenguaje \((\T,\V)\).
%
A modo de ejemplo, introducimos una relación de evaluación instrumentada que
será la encargada de ir introduciendo ticks a medida que vaya evaluando los
términos en valores mónadicos, simplemente acumulando el costo de consumir un
constructor del lenguaje.
%
Una alternativa a contar reducciones sobre los términos, es contar solamente la
cantidad de aplicaciones de funciones que fueron
realizadas~\parencite{Sands:Skye} o incluso asignarles diferentes costos a
operaciones que son computacionalmente pesadas, como ser: comunicarse por red o
consultas a oráculos.
%
Diferentes formas de instrumentar la relación de evaluación posiblemente lleve
a diferentes relaciones de mejoras.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Pagano
\begin{definition}[Relación de Evaluación Instrumentada Aproximada]
  Definimos una familia de relaciones indexadas por un número natural
\( n \in \mathbb{N}\) entre términos cerrados y valores con
efectos \(({\Downarrow}^{\mathbb{N}T}_{n})\) donde instrumentamos con tick la evaluación de cada uno de los constructores del lenguaje.
  %% Operational Approx Semantics
  \begingroup
  \addtolength{\jot}{1em}
  \begin{align*}
    %% Bot Rule
    &\inference[\<bot>]{}{M {\Downarrow}^{\mathbb{N}T}_0 \bot}
    \quad
    \inference[(\<ret>)]{}{\lRet{V} {\Downarrow}^{\mathbb{N}T}_{n+1} \checkmark\eta_{\mathbb{N}_{\infty}}(V)} \\
    &\inference[(\<seq>)]
      {M {\Downarrow}^{\mathbb{N}T}_n X & \substTVal{N}{x}{V} {\Downarrow}^{\mathbb{N}T}_n Y_V}%
      {\letIn{x}{M}{N} {\Downarrow}^{\mathbb{N}T}_{n+1} \checkmark X {>>=}_{\mathbb{N}_{\infty}} (V \leadsto Y_V) } \\
    &\inference[(\<app>)]{\substTVal{M}{x}{W} {\Downarrow}^{\mathbb{N}T}_n X}
          {\lApp{(\lAbs{x}{M})}{W} {\Downarrow}^{\mathbb{N}T}_{n+1} \checkmark X} \\
    &\inference[(\(\checkmark\)-\<op>)]{M {\Downarrow}^{\mathbb{N}T}_n X }
              {\checkmark M {\Downarrow}^{\mathbb{N}T}_{n+1} \checkmark X} \quad
    \inference[(\(\sigma\)-\<op>)]{M_1 {\Downarrow}^{\mathbb{N}T}_n X_1 & \ldots & M_k {\Downarrow}^{\mathbb{N}T}_n X_k}
              {\sigma(M_1, \ldots, M_k) {\Downarrow}^{\mathbb{N}T}_{n+1} \checkmark \sigma^{T} (X_1, \ldots, X_k)}
  \end{align*}
  \endgroup
Donde la regla (\(\sigma\)-\<op>) es en realidad un conjunto de reglas, una
para cada operador \(\sigma \in \Sigma\) y \(k = \alpha(\sigma)\).
  \end{definition}

Siguiendo los pasos realizados en los Capítulos~\ref{ch:lenguaje}
y~\ref{ch:Effects} podemos derivar una relación de evaluación tal que para todo
término cerrado \(M \in \T^{\Sigma}_{0}\), \({[[ M ]]}_{\mathbb{N}_{\infty}} \in T_{\mathbb{N}_{\infty}}(\V_{0})\).
%
Para evitar la repetición dejamos las pruebas necesarias fuera del presente
documento.

\begin{lemma}[Ecuaciones de la Relación de Evaluación Instrumentada]\label{def:eval:cost}
  La relación de evaluación mónadica instrumentada del \(\Sigma\)-sistema  \((T, \Gamma)\)
respeta las siguientes ecuaciones:
  % Instrumentamos la relación de evaluación mónadica del \(\Sigma\)-sistema
% utilizando la función tick definida al principio de esta sección.
\begin{align*}
  {[[ \lRet{V} ]]}_{\mathbb{N}_{\infty}} &= \checkmark \eta_{\mathbb{N}_{\infty}}(V) \\
  {[[ \lApp{(\lAbs{x}{M})}{W} ]]}_{\mathbb{N}_{\infty}} &= \checkmark {[[ \substTVal{M}{x}{W} ]]}_{\mathbb{N}_{\infty}} \\
  {[[ \letIn{x}{M}{N}]]}_{\mathbb{N}_{\infty}} &= \checkmark {[[ M ]]}_{\mathbb{N}_{\infty}} {>>=}_{\mathbb{N}_{\infty}} (v \mapsto {[[ \substTVal{N}{x}{v} ]]}_{\mathbb{N}_{\infty}}) \\
  { [[ \checkmark M ]] }_{\mathbb{N}_{\infty}} &= \checkmark {[[ M ]]}_{\mathbb{N}_{\infty}} \\
  { [[ \sigma(M_{1}, \ldots, M_{n}) ]]}_{\mathbb{N}_{\infty}} &= \checkmark \sigma^{T_{\mathbb{N}_{\infty}}}
                                                 ({[[ M_{1} ]]}_{\mathbb{N}_{\infty}},
                                                 \ldots,
                                                 {[[ M_{n} ]]}_{\mathbb{N}_{\infty}}) \\
\end{align*}
  \end{lemma}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Las principales diferencias con la relación de evaluación dada en la
Sección~\ref{sec:eff:eval} son:
  \begin{itemize}
    \item la evaluación de un tick en el lenguaje se interpreta como adicionar
un tick en la acumulación de costos, de esta manera evitamos que se cuenten dos
veces.
    \item se suma un tick por cada vez que se consume un constructor del término
  \end{itemize}

Siguiendo los Capítulos~\ref{ch:equiv} y~\ref{ch:equiv:eff}, definimos una
familia de relaciones de aproximación sobre la evaluación de términos, aunque en
este caso tendremos la observación extra sobre el costo de las computaciones.

En lo que queda del capítulo reproducimos los resultados obtenidos en el
Capítulo~\ref{ch:Effectless} teniendo en cuenta los efectos con el objetivo de
obtener una teoría de mejoras en lenguajes con efectos.

\section{Simulación de Mejoras}

La relación de similaridad aplicativa sigue la idea presentada por Abramsky,
donde define dos términos similares si lo son modulo evaluación, y la hemos
caracterizado coinductivamente para términos sin efectos en la
Sección~\ref{sec:aprox_app} y con efectos en la Sección~\ref{sec:aprox:eff}.
%
En esta sección lo que haremos es observar además de los efectos producidos por
las operaciones algebraicas del lenguaje, el costo resultante de la evaluación
de los términos.
%
Llamamos \emph{simulación de mejoras} a la relación de simulación aplicativa
resultante de utilizar un sistema que lleve cuenta de los costos de las
computaciones.
%
En este sentido, la simulación de mejoras está compuesta de dos observaciones
(independientemente del sistema): la comparación de términos como la simulación de
Abramsky y la comparación de costos.

\begin{definition}[Simulación de Mejoras]\label{def:sim:mejoras}
  Decimos que una relación cerrada entre \(\lambda\)-términos y valores
\((R_{\T}, R_{\V})\) es una simulación de mejoras si y solo si respeta valores y
además:
\[
  \forall M, N \in \T_{0}, M \mathrel{R_{\T}} N \implies {[[ M ]]}_{\mathbb{N}_{\infty}} \mathrel{\Gamma_{\mathbb{N}_{\infty}} R_{\V}} { [[ N ]] }_{\mathbb{N}_{\infty}}
\]
  \end{definition}

Sea \((R_{\T},R_{\V})\) una simulación de mejoras, \(M\) y \(N\) dos términos
cerrados, tales que \(M \mathrel{R_{\T}} N\).
%
Desenvolviendo la definición de simulación de mejoras, tenemos que la evaluación
de ambos términos están relacionados de forma tal que:
\({[[ M ]]}_{\mathbb{N}_{\infty}} \mathrel{\Gamma_{\mathbb{N}_{\infty}}R_{\V}} {[[ N ]]}_{\mathbb{N}_{\infty}}\).
%
Más aún, por la definición del relacionador de costos (Definición~\ref{lmm:cost:rel})
tenemos que
\({[[ M ]]}_{\mathbb{N}_{\infty}} \mathrel{{(\Gamma R_{\V})}_{\mathbb{N}_{\infty}}} {[[ N ]]}_{\mathbb{N}_{\infty}}\), en
palabras:
\begin{itemize}
   \item el resultado de evaluar el término \(M\) y \(N\), \({[[ M ]]}_{\mathbb{N}_{\infty}}\) y
\({[[ N ]]}_{\mathbb{N}_{\infty}}\), tiene los efectos relacionados mediante la
definición del relacionador \(\Gamma\) de \(T\).  %\(T_{\mathbb{N}_{\infty}}\)
   \item el costo de la evaluación de \(M\), encapsulado en
\({[[ M ]]}_{\mathbb{N}_{\infty}}\) es mayor o igual que el costo de la evaluación de
\(N\), encapsulado en \({[[ N ]]}_{\mathbb{N}_{\infty}}\).
        \end{itemize}

\subsubsection{Mónada de Parcialidad}

La mónada de parcialidad presenta, en esta teoría, un ejemplo muy importante.
%
Es el cálculo lambda tal cuál fue presentado al principio de la
tesis~(Capítulo~\ref{ch:lenguaje}) y su teoría de
mejoras~(Capítulo~\ref{ch:Effectless}), donde el único efecto que se observa
es el de la convergencia en la evaluación de un término.
%
Concretamente, la mónada de parcialidad instancia la teoría en el caso que el
único efecto observable es la divergencia de la evaluación de los términos,
efecto necesario para poder establecer la noción de aproximación observacional.
%
Para ver que el sistema parcial es el mismo que la noción clásica de mejoras,
instanciamos las definiciones y las desenvolvemos, en particular la del
relacionador \(\Gamma_{\bot}\) definido en la Sección~\ref{sec:aprox:eff}.
%
Sea \((R_{\T},R_{\V})\) una relación entre \(\lambda\)-términos tal que sea una
simulación de mejoras en el sistema parcial.
%
Por la definición de simulación de mejoras~(Definición~\ref{def:sim:mejoras})
tenemos que para cuales quiera \(\lambda\)-términos cerrados \(M\) y \(N\), si
\(M \mathrel{R_{\T}} N \) entonces:
%
\[
  \begin{array}{rl}
    \bullet  & {[[ M ]]}_{\mathbb{N}_{\infty}} = \iota_{r}(\bot_{\V \times \mathbb{N}_{\infty}})\text{, o} \\
    \bullet  & {[[ M ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(V,n)
      \implies
      {[[ N ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(W,m) \land n \geq_{\mathbb{N}_{\infty}}m \land V \mathrel{R_{\V}} W
    \end{array}
\]

La definición resultante de instanciar la simulación de mejoras al sistema
parcial es equivalente a la definición clásica de teoría de
mejoras~\parencite{Sands:Skye}.
%
La diferencia principal yace en que esta última utiliza evaluación a \emph{weak
head normal form}, en lugar de la evaluación \emph{eager} que seguimos en este
trabajo.

\section{Mejoras}

La relación de aproximación observacional relaciona términos basándose en cómo
estos se comportan en todo contexto.
%
En otras palabras, un término \(A\) aproxima observacionalmente un término \(B\)
si no hay un contexto capaz de \emph{observar} una diferencia en el
comportamiento de \(A\) y \(B\).
%
En los capítulos anteriores hemos explorado cuales eran las observaciones que
podían realizarse, en nuestro caso sobre los efectos generados por la evaluación
de operadores de efectos algebraicos.
%
La noción de observación la encapsulamos directamente en el uso de
los relacionadores, codificando primero la observación de no divergencia en la
evaluación, y luego, dependiendo del sistema, los efectos generados por la
evaluación.
%
En esta sección, vamos a ver cómo al mezclar los efectos generados con la
información de costos podemos derivar una noción de \emph{mejoras} para poder
hacer análisis intensivo sobre términos.

\begin{definition}[Mejora]
  Sean \(M\) y \(N\) dos términos.
  %
  Decimos que \(M\) es mejorado por \(N\) si y solo si \(M \geq_{\Gamma_{\mathbb{N}_{\infty}}} N\).
  \end{definition}

Notamos la relación de mejoras \((\geq_{\Gamma_{\mathbb{N}_{\infty}}})\) como \(\succeq_{\Gamma}\),
donde nos evitamos escribir el subíndice de costo.

\subsection{Sistema Parcial}

De la misma forma que utilizamos el sistema parcial para mostrar que al
instanciar la definición de simulación de mejoras con efectos obtenemos la
definición clásica, en éste caso vemos que al instanciar la definición de
mejoras al sistema parcial obtenemos la definición de mejoras clásica.
%
Como resultado del Lema~\ref{lmm:obs:comp:pre} tenemos que la relación
\(\succeq_{\Gamma_{\bot}}\) es un preorden compatible y preadecuado.
%
Por lo que, para todo término \(M\) y \(N\) y para todo contexto
\(\mathbb{C}\) que cierre a \(M\) y \(N\), si
\(\emptyset \vdash \mathbb{C}[M] \succeq_{\Gamma_{\bot}} \mathbb{C}[N]\), tenemos que:
\[ [[ \mathbb{C}[M] ]]_{\mathbb{N}_{\infty}} \mathrel{\Gamma_{\bot\mathbb{N}_{\infty}} \mathcal{U}}
[[ \mathbb{C}[N] ]]_{\mathbb{N}_{\infty}}
\]
%
Más aún, por definición del relacionador de costos~(Lema~\ref{lmm:cost:rel}) y
de \(\Gamma_{\bot}\), dados términos \(M\) y \(N\), \(M \succeq_{\Gamma_{\bot}} N\) si y solo si para
todo contexto \(\mathbb{C}\) que cierre a \(M\) y \(N\) hay dos posibles casos:
\[
  \begin{array}{rl}
    \bullet & \text{la evaluación de } \mathbb{C}[M] \text{ diverge: } {[[ \mathbb{C}[M] ]]}_{\mathbb{N}_{\infty}} = \iota_{r}(\bot_{\V \times \mathbb{N}_{\infty}}) \\
    \bullet & \text{la evaluación de  } \mathbb{C}[M] \text{ converge y es mejorada por la evaluación de \(\mathbb{C}[N]\):}
        \\
    & {[[ \mathbb{C}[M] ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(V,c_{1}) \implies
        {[[ \mathbb{C}[N] ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(W,c_{2}) \land c_{1} \geq_{\infty} c_{2}
    \end{array}
\]

En palabras, para dos términos \(M\) y \(N\) cualesquiera, tales
que \(M \succeq_{\Gamma_{\bot}} N\), para cualquier contexto \(\mathbb{C}\) que
los cierre, siempre que la evaluación de \(\mathbb{C}[M]\) converja con costo
\(c_{1}\), la evaluación de \(\mathbb{C}[N]\) también debe converger con un
costo \(c_{2}\), tal que \(c_{1}\) es mayor o igual que \(c_{2}\).
%
Esta es la misma noción de mejoras dada para el cálculo lambda sin
efectos~\parencite{Sands:HOOTS} como se puede observar al compararla con la
definición de mejora~(Definición~\ref{def:improvement}) del
Capítulo~\ref{ch:Effectless}.

Finalmente por el Teorema~\ref{thm:sim:obs}, es suficiente con obtener una
simulación de mejoras para mostrar mejoras.
%
En otras palabras, nos provee un método para probar mejoras entre términos
cerrados.
%
Para probar que un término cerrado \(M\) es mejorado por otro término
cerrado \(N\), \(M \succeq_{\Gamma} N\), basta con probar que \(M\) es similar a \(N\).
%
Más aún, probar que \(M\) es similar a \(N\) consiste en encontrar una
simulación que evidencie la mejora.
%
De esta manera es posible simplificar las pruebas, aunque dependiendo del
sistema, como vimos al momento de introducir
efectos~(Sección~\ref{sec:app:obs}), las simulaciones no siempre son suficientes
para mostrar propiedades observacionales.

En el siguiente capítulo vemos cómo utilizar la teoría de mejoras con efectos
para derivar \emph{nuevas teorías de mejoras}.
%
En particular, una teoría para cada uno de los efectos utilizados como ejemplo a
lo largo de la tesis.

%%% Local Variables:
%%% TeX-master: "../Thesis.tex"
%%% TeX-PDF-mode: t
%%% ispell-local-dictionary: "es"
%%% End:
