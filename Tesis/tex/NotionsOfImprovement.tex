\chapter{Nociones de Mejoras}\label{ch:notionsofimprovement}

Utilizando las definiciones vistas en el capítulo
anterior~(Capítulo~\ref{ch:improvement}), en este capítulo derivamos una
\emph{nueva} noción de mejoras para cada uno de los sistemas presentados como
ejemplo a lo largo de la tesis.
%
En particular derivamos nociones de mejoras y simulación de mejoras para
los diferentes lenguajes presentados: excepciones, no-determinismo y
operadores probabilísticos.

Primero adicionamos al sistema una noción de costos \emph{sin tener que
  modificar} la definición del sistema.
%
De esta manera reutilizamos la definición de aproximación de términos dada por
el relacionador para además comparar costos sin tener que realizar pruebas
adicionales.
%
Luego, sobre el final del capítulo, analizamos mediante un ejemplo concreto
presentando los límites del enfoque utilizado.
%
Es decir, mostramos cómo la reutilización del relacionador podría llevar a una
noción de mejoras poco granular.
%
Esto nos lleva entonces a explorar una nueva alternativa a introducir costos en
el lenguaje modificando el relacionador, y así, tener una observación sobre los
costos más expresiva.

\section{Excepciones}

Sea \(E\) un conjunto de símbolos, \(((X + E)_{\bot}, \Gamma_{E})\) es un sistema
\(\Sigma_{E}\) compatible.
%
Más aún, siguiendo la definición de simulación de
mejoras~(Definición~\ref{def:sim:mejoras}), una relación entre \(\lambda\)-términos
\((R_{\T},R_{\V})\) es una simulación de mejoras para el sistema de excepciones
si y solo si \(R_{\V}\) respeta valores y para los términos cerrados \(M,N\)
tenemos que, \(M \mathrel{R_{\T}} N \) si y solo uno de los siguientes casos es
verdadero:
\begin{itemize}
  \item La evaluación de \(M\) diverge: \({[[ M ]]}_{\mathbb{N}_{\infty}} = \iota_{r}(\bot) \)
  \item La evaluación de \(M\) converge a una excepción \(e\), y también lo hace
la evaluación de \(N\):
        \({ [[ M ]] }_{\mathbb{N}_{\infty}} =   \iota_{l}(\iota_{r}(e)) \implies {[[ N ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(\iota_{r}(e')) \land e = e'\)
  \item La evaluación de \(M\) converge, entonces la evaluación de \(N\) también
converge a un valor relacionado con el resultante de la evaluación de \(M\) con
menor o igual costo:
%         a un valor \(v\) con costo \(n\), y
% también la evaluación de \(N\) converge a un valor \(w\) con costo \(m\), tal
% que \((n \geq m)\) y \((v \mathrel{R_{\V}} w)\):
\[
    { [[ M ]] }_{\mathbb{N}_{\infty}} = \iota_{l}(\iota_{l}(V,n)) \implies
      {[[ N ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(\iota_{l}(W,m))
      \land (n \geq m)
      \land (V \mathrel{R_{\V}} W)
\]
  \end{itemize}

En palabras, dos términos cerrados \(M\) y \(N\) son similares si y solo si sus
evaluaciones retornan valores extensionalmente similares y el costo de evaluar a
\(M\) es mayor que el de \(N\) o ambas evaluaciones producen la \emph{misma
excepción}.
%
Sin embargo, notar que cuando la evaluación lanza una excepción \emph{no se
comparan los costos}.
%
Esto es una consecuencia directa del uso de la mónada interna de costos
\emph{sin modificar el sistema}.
%
Al utilizar la mónada interna de costos se equipan valores con costos, pero
las excepciones no son valores, sino que son elementos que representan
el efecto de lanzar dicha excepción.
%
Exploramos una alternativa de llevar cuenta del costo de las evaluaciones en
la Sección~\ref{sec:alt:cost}.

De la misma manera, decimos que un \(\lambda\)-término \(M\) es mejorado por un
\(\lambda\)-término \(N\), \(M \succeq_{E} N\), si y solo si para todo contexto
\(\mathbb{C}\) que cierre a \(M\) y \(N\), tenemos a lo sumo uno de los
siguientes casos:
\begin{itemize}
        \item La evaluación de \(\mathbb{C}[M]\) converge, y por ende, la
evaluación de \(\mathbb{C}[N]\) también pero con menos costo:
        \begin{align*}
          & \forall v \in \V_{0}, n \in \mathbb{N}_{\infty}, { [[ \mathbb{C}[M] ]] }_{\mathbb{N}_{\infty}} = \iota_{l}(\iota_{l}(v,n)) \implies \\
          & \exists w \in \V_{0}, m \in \mathbb{N}_{\infty}, { [[ \mathbb{C}[N] ]] }_{\mathbb{N}_{\infty}} = \iota_{l}(\iota_{l}(w,m)) \land (n \geq_{\infty} m)
          \end{align*}
  \item La evaluación de \(\mathbb{C}[M]\) genera una excepción \(e\), y por lo tanto, la evaluación de \(\mathbb{C}[N]\) genera la misma excepción:
        \[
        \forall e \in E, { [[ \mathbb{C}[M] ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(\iota_{r}(e)) \implies
        {[[ \mathbb{C}[ N ] ]]}_{\mathbb{N}_{\infty}} = \iota_{l}(\iota_{r}(e))
       \]
  \item La evaluación de \(\mathbb{C}[M]\) diverge:
        \[
        {[[ \mathbb{C}[M] ]]}_{\mathbb{N}_{\infty}} = \iota_{r}(\bot)
        \]
\end{itemize}

\section{No-Determinismo}
%
El sistema no-determinístico introduce un operador \((\oplus)\) binario cuya
evaluación retorna el valor de alguno de sus argumentos.
%
Para interpretar este efecto utilizamos la mónada de partes donde vamos
acumulando los posibles resultados que puede tomar la evaluación de un término.
%
Existen al menos dos formas de definir la aproximación de computaciones
no-determinístico que podemos codificarlas como dos relacionadores diferentes, y
utilizando estas dos aproximaciones definir una tercera.
%
Cada uno de estos relacionadores están inspirados por las diferentes relaciones
sobre \emph{powerdomains} presentadas
por~\citeauthor{Sondergaard.1992.NonDetFunLang}~\parencite*{Sondergaard.1992.NonDetFunLang}.

Un trabajo más reciente que estudia la noción de similaridad en lenguajes con
no-determinismo puede ser el
de~\citeauthor{Lassen.1997.SimBisimNonDet}~\parencite*{Lassen.1997.SimBisimNonDet}
donde presentan dos relaciones: \emph{may-converge} y \emph{may-diverge}.
%
En otras palabras, definen dos observaciones sobre la evaluación
no-determinística de términos.
%
Estas observaciones siguen un enfoque similar al que seguimos en esta
tesis~\parencite[Definición~3.1]{Lassen.1997.SimBisimNonDet}, pero combinan las
observaciones de forma tal que obtienen una definición más granular.
%
Pueden obtener definiciones más granulares ya que trabajan con una semántica
operacional concreta para interpretar no-determinismo.
%
En este trabajo, nos concentramos en cómo es observado el no-determinismo
mediante el uso de relacionadores, y por lo tanto, nos basamos en las
observaciones más básicas realizadas
por~\citeauthor{Sondergaard.1992.NonDetFunLang}, que se pueden comparar a las
utilizadas en la relación de
may-converge~\parencite{Lassen.1997.SimBisimNonDet}.

En esta sección instanciamos la definición del sistema no-determinístico con
el objetivo de obtener una noción de simulación de mejoras y una de mejoras con
efectos sobre \emph{tres} diferentes relacionadores mónadicos para la mónada de
no-determinismo.

\subsection{Relación de costos de Hoare}
%
El relacionador de Hoare representa una forma intuitiva de mapear una relación de
valores a una relación entre conjunto de valores.
%
Sea \(R \subseteq X \times Y\) una relación, mapeamos la relación a conjuntos de valores,
\(A \subseteq X\) y \(B \subseteq Y\), decimos que \(A\) está \(R\)-relacionado con \(B\), si
para cada elemento de \(A\) se relaciona con al menos uno de \(B\).
%
Cuando utilizamos este mapeo de relaciones para comparar la evaluación de
términos no-determinístico, el relacionador de Hoare nos dice que un término \(M\)
está relacionado con un término \(N\) si y solo si todo valor \emph{alcanzable} por la
evaluación de \(M\) es también alcanzable por la evaluación de \(N\).

\begin{definition}[Relacionador de
Hoare~\parencite{Lago2017EffectfulApplicativeBisimilarity}]
  Sean \(X,Y\) dos conjuntos y \(R\) una relación entre ellos.
  %
  Definimos el relacionador \(\Gamma^{H}R \subseteq \mathcal{P}(X) \times \mathcal{P}(Y)\) donde:
  \[
    S \mathrel{\Gamma^{H}R} T \iff \forall x \in S , \exists y \in T, x \mathrel{R} y
  \]
  \end{definition}


Agregamos costos como un efecto observable sobre la evaluación de términos y
equipamos el relacionador para poder comparar el costo de los valores.
%
Utilizando las definiciones de la sección anterior podemos derivar una noción de
mejora y de simulación de mejoras.
%
Obtenemos que una relación entre \(\lambda\)-términos cerrados \((R_{\T},R_{\V})\) es
una simulación de mejoras de Hoare si y solo si respeta valores y además para
todo \(M,N\) términos cerrados tenemos que:
\[
  M \mathrel{R_{\T}} N \implies { [[ M ]] }_{\mathbb{N}_{\infty}} \mathrel{\Gamma^{H}_{\mathbb{N}_{\infty}} R_{\V}} {[[ N ]]}_{\mathbb{N}_{\infty}}
\]
%
Aplicando la definición del relacionador de costos, tenemos que:
\[
  M \mathrel{R_{\T}} N \implies {[[ M ]]}_{\mathbb{N}_{\infty}} \mathrel{\Gamma^{H} R_{\V\mathbb{N}_{\infty}}} {[[ N ]]}_{\mathbb{N}_{\infty}}
\]
%
Ahora, desenvolviendo la definición del relacionador de Hoare tenemos que:
\[
  M \mathrel{R_{\T}} N \implies
  \forall (V,m) \in { [[ M ]] }_{\mathbb{N}_{\infty}},
  \exists (W,n) \in {[[ N ]]}_{\mathbb{N}_{\infty}},
  V \mathrel{R_{\V}} W \land m \geq_{\infty} n
\]

En palabras, un término \(N\) es similar a un término \(M\) si y
solo si siempre que la evaluación de \(M\) termine entonces también lo hace la de
\(N\), y además, para cada posible valor \(V\) que pueda tomar la evaluación de
\(M\), existe un valor \(W\) resultante de la evaluación de \(N\), tales que
\(W\) es similar a \(V\) y el costo \(n\) es menor o igual que \(m\).

La definición de mejoras de Hoare se obtiene de instanciar la definición de
mejoras con el relacionador de Hoare:
\[
  M \succeq_{\Gamma^{H}} N \iff \forall \mathbb{C} \in Ctx, \mathbb{C}[M,N] \in \T_{0},
  { [[ \mathbb{C}[M] ]] }_{\mathbb{N}_{\infty}}
  \mathrel{\Gamma^{H}_{\mathbb{N}_{\infty}} \mathcal{U}}
  { [[ \mathbb{C}[N] ]] }_{\mathbb{N}_{\infty}}
\]

En palabras, un término \(N\) mejora a un término \(M\) si y solo si
para todo contexto \(\mathbb{C}\) que los cierre, para todo valor \(V\) en la
evaluación de \(\mathbb{C}[M]\), hay un valor \(W\) en la evaluación de
\(\mathbb{C}[N]\), tal que el costo requerido para obtener \(W\) es menor o
igual al necesario para obtener \(V\).
%
En este caso solo se comparan los costos y los efectos generados por la
evaluación de los términos, ya que los valores resultantes de la evaluación se
comparan por la relación \(\mathcal{U}\) que relaciona todos los valores con todos los
valores.

\begin{example}
  Sea \(M \triangleq \tick*^{5} \lRet{I}\) un término de costo \(6\) que retorna la
identidad y \(N \triangleq (\tick*^{7} \lRet{I}) \oplus \lRet{I}\) un término alcanza la
identidad con un costo \(9\) o \(2\).
%
Definimos la identidad como \(I \triangleq \lAbs{x}{\lRet{x}}\).
%
La evaluación del término \(M\) tiene costo \(6\) ya que consume \(5\) ticks y
luego retorna un valor, lo cual adiciona una unidad de costo.
%
Mientras que la evaluación del término \(N\) tiene dos valores posibles donde a
ambas posibilidades se les suma uno por consumir el operador \(\oplus\), y siguiendo
la misma cuenta que en el término \(M\) tenemos que una rama consume \(8\)
unidades mientras que la otra solo una.
  %
  Dado que \(M\) y \(N\) son términos cerrados, nos basta con encontrar una
simulación de mejoras que evidencie la mejora de forma local.
%
% Formalmente, para probar que \(N\) mejora a \(M\), basta con construir una
% simulación de mejora que muestre dicha mejora,
%
Afortunadamente con la relación de identidad nos basta.
%
Intuitivamente, el término \(M\) evalúa siempre a la función identidad con un
costo de \(6\) unidades, mientras
% (\(5\) ticks más uno que retorna el valor)
que el término \(N\) evalúa a la función identidad con \(9\) o \(2\) unidades de
costo.
% (\(7\) ticks más uno del retorno del valor y otro del \(\oplus\), o bien el
% operador y el retorno).
%
Dado que es posible obtener el mismo valor utilizando menos de \(6\) unidades de
costo evaluando el término \(N\), el relacionador de Hoare nos dice que \(M\) es
mejorado por \(N\).
  \end{example}

El relacionador de Hoare considera que un término \(M\) es mejorado por un
término \(N\) si el mejor caso de \(N\) tiene menor costo que el mejor caso de
\(M\).
%Podemos ver como hacer una relación entre la noción de mejoras que define el
%relacionador de Hoare, tenemos que dados términos \(M,N\), \(M\) es mejorado por
%\(N\) si el mejor caso de la evaluación de \(N\) es mejor o igual al mejor caso
%de la evaluación de \(M\).

\subsection{Relación de costos de Smyth}
%
El relacionador de Smyth, comparado con el relacionador de Hoare, mapea relaciones pero en
el sentido opuesto.
%
Sea \(R \subseteq X \times Y\), entonces \(A \subseteq X, B \subseteq Y\) están \(R\)-relacionados siempre
que todo elemento de \(B\) esté relacionado con un elemento de \(A\).

\begin{definition}[Relacionador de Smyth~\parencite{Lago2017EffectfulApplicativeBisimilarity}]
  Sean \(X,Y\) dos conjuntos, y \(R \subseteq X \times Y\) una relación.
  %
  Definimos el relacionador \(\Gamma^{S}R \subseteq \mathcal{P}(X) \times \mathcal{P}(Y)\) de la siguiente forma:
  \[
    S \mathrel{\Gamma^{S}R} T \iff \forall y \in T, \exists x \in S, x \mathrel{R} y
  \]
  \end{definition}

Una relación de \(\lambda\)-términos cerrados \((R_{\T},R_{\V})\) es una simulación de
mejoras de Smyth si y solo si respeta valores y para todos términos cerrados
\(M,N\), tenemos que:
\[
  M \mathrel{R_{\T}} N \implies { [[ M ]] }_{\mathbb{N}_{\infty}} \mathrel{\Gamma^{S}_{\mathbb{N}_{\infty}}R_{\V}} { [[ N ]] }_{\mathbb{N}_{\infty}}
\]
Al igual que antes, desenvolvemos la definición del relacionador, y obtenemos que:
\[
  M \mathrel{R_{\T}} N \implies \forall (W,n_{c}) \in {[[ N ]]}_{\mathbb{N}_{\infty}},
  \exists (V,m_{c}) \in {[[ M ]]}_{\mathbb{N}_{\infty}}, W \mathrel{R_{\V}} V \land m_{c} \geq_{\infty} n_{c}
\]

Para obtener la definición de mejoras de Smyth lo que hacemos es instanciar la
definición de mejoras al relacionador de Smyth, de la misma manera que hicimos con el
de Hoare.
%
Dados dos términos cuales quiera \(M,N\), \(M \succeq_{\Gamma^{S}} N\) si y solo si:
\[
  \forall \mathbb{C} \in Ctx, \mathbb{C}[M], \mathbb{C}[N] \in \T_{0},
  { [[ \mathbb{C}[M] ]]}_{\mathbb{N}_{\infty}}
  \mathrel{\Gamma^{S}_{\mathbb{N}_{\infty}} \mathcal{U}}
  { [[ \mathbb{C}[N] ]]}_{\mathbb{N}_{\infty}}
\]

En palabras, para cada posible valor alcanzable por la evaluación de
\(\mathbb{C}[N]\), hay un valor alcanzable por la evaluación de
\(\mathbb{C}[M]\) que requiere más o igual costo para ser computado.

Se puede ver la dualidad entre las definiciones de mejoras de Smyth y Hoare.
%
Dados dos términos \(M,N\), comparando las nociones de mejoras \(M \succeq_{\Gamma^{S}} N\)
y \(M \succeq_{\Gamma^{H}} N\).
%
Por un lado, la mejora de Hoare solo pide que exista un valor alcanzable en la
evaluación de \(N\) que tenga un menor costo.
%
Mientras que por el otro, la mejora de Smyth se fija que exista un valor
alcanzable en la evaluación de \(M\) que tenga un costo mayor.
%
En otras palabras, la mejora de Hoare busca mejorar el mejor caso mientras que
con la mejora de Smyth se busca mejorar el peor caso.

\begin{example}
  Retomamos el mismo ejemplo con los términos \(M \triangleq \tick*^{5} \lRet{I}\) y
\(N \triangleq (\tick*^{7} \lRet{I}) \oplus \lRet{I}\), con \(I\) definiendo la identidad de
la misma manera que en el ejemplo anterior.
%
El término \(M\) no es (Smyth) mejorado por el término \(N\) ya que \(M\) evalúa
a la función identidad en \(6\) unidades de costos, mientras que hay una posible
ejecución en \(N\) que puede requerir \(9\) unidades.
%
Sin embargo, podemos probar que \(N \succeq_{\Gamma^{S}} M\) siguiendo el mismo
procedimiento que en el ejemplo anterior.
  \end{example}

Intuitivamente, un término \(M\) es (Smyth) mejorado por un término \(N\) si y
solo si en todo contexto \(\mathbb{C}\) el peor caso en la evaluación de
\(\mathbb{C}[N]\) tiene que ser mejor (o igual) que el peor caso en la
evaluación de \(\mathbb{C}[M]\)

\subsection{Relación de costos de Plotkin}
%
La relación de costos de Plotkin es una conjunción entre las dos relaciones
antes vistas.
%
En particular, se obtiene una relación donde un término \(M\) es mejorado por un
término \(N\) siempre que para todo contexto \(\mathbb{C}\):
\begin{itemize}
        \item para todo valor alcanzable por la evaluación de \(\mathbb{C}[M]\)
hay un valor en la evaluación de \(\mathbb{C}[N]\) que requiere menos (o igual) costo,
        \item para todo posible valor en la evaluación de \(\mathbb{C}[N]\) hay
un valor en la evaluación de \(\mathbb{C}[M]\) que requiere más (o igual) costo.
  \end{itemize}

\begin{definition}[Relacionador de Plotkin~\parencite{Lago2017EffectfulApplicativeBisimilarity}]
  Sean \(X,Y\) dos conjuntos y \(R \subseteq X \times Y\) una relación.
  %
  Definimos el relacionador de Plotkin, \(\Gamma^{P}R \subseteq \mathcal{P}(X) \times \mathcal{P}(Y)\)
de la siguiente forma:
\[
  A \mathrel{\Gamma^{P}R} B \iff A \mathrel{\Gamma^{H}R} B \land A \mathrel{\Gamma^{S}R} B
\]

Una relación entre \(\lambda\)-términos cerrados \((R_{\T},R_{\V})\) es una simulación
de mejoras de Plotkin si y solo si respeta valores y además:
\[
  M \mathrel{R_{\T}} N \implies
  \begin{array}{rl}
    & (\forall V_{m} \in {[[ M ]]}_{\mathbb{N}_{\infty}}, \exists V_{n} \in {[[ N ]]}_{\mathbb{N}_{\infty}}, V_{m}\mathrel{R_{\V\mathbb{N}_{\infty}}} V_{n}) \\
    \land & \\
    & (\forall V_{n} \in {[[ N ]]}_{\mathbb{N}_{\infty}}, \exists V_{m} \in {[[ M ]]}_{\mathbb{N}_{\infty}}, V_{m}\mathrel{R_{\V\mathbb{N}_{\infty}}} V_{n}) \\
    \end{array}
\]
  \end{definition}

Mientras que la noción de mejoras de Plotkin, para dos términos cuales quiera \(M,N\),
\(M \succeq_{\Gamma^{P}} N\) si y solo si, para todo contexto \(\mathbb{C}\) que cierre a \(M,N\):
\[
  \begin{array}{rl}
    & (\forall (V_{m},c_{m}) \in {[[ M ]]}_{\mathbb{N}_{\infty}}, \exists (V_{n},v_{c}) \in {[[ N ]]}_{\mathbb{N}_{\infty}},
      c_{m} \geq_{\infty} c_{n}) \\
    \land & \\
    & (\forall (V_{n},c_{n}) \in {[[ N ]]}_{\mathbb{N}_{\infty}}, \exists (V_{m}, v_{c}) \in {[[ M ]]}_{\mathbb{N}_{\infty}},
     c_{m} \geq_{\infty} c_{n} )\\
    \end{array}
\]

La relación de costos de Plotkin es la conjunción de la de Hoare y Smyth.
%
En otras palabras, un término \(N\) es (Plotkin) mejorado por un término \(M\)
si y solo si en cualquier contexto \(\mathbb{C}\), el mejor caso de la
evaluación de \(\mathbb{C}[N]\) es mejor que el mejor caso de la evaluación de
\(\mathbb{C}[M]\), mientras que el peor caso posible de la evaluación de
\(\mathbb{C}[M]\) es peor que el peor caso de la evaluación de
\(\mathbb{C}[N]\).

El ejemplo del sistema no-determinista demuestra un caso interesante del cual
podemos extraer diferentes nociones de mejoras utilizando diferentes
relacionadores.
%
En este caso se derivaron tres teorías de mejoras con diferentes relacionadores
para un mismo efecto algebraico, el de no determinismo, obteniendo diferentes
nociones de mejoras.
%
Derivar diferentes nociones dependiendo el relacionar utilizado nos muestra que
la noción de mejora depende de la observaciones realizadas sobre el sistema.

\section{Probabilístico}
%
Los lenguajes funcionales probabilísticos son un campo activo de investigación
donde se pueden encontrar diferentes maneras de interpretar el efecto generados
por términos probabilísticos~\parencite{Ramsey.2002.ProbMonad}.
%
En esta sección, derivamos la noción de mejora para la mónada de
sub-distribuciones utilizada para interpretar el operador probabilístico
introducido en la Sección~\ref{sssec:prob}.
%
Dependiendo de cómo se interpreten el efecto de operadores probabilísticos es
posible que se puedan obtener diferentes nociones de mejoras.
%
En lo que queda de la sección derivamos una teoría de mejoras
para un lenguaje probabilístico.
% siguiendo el enfoque presentado hasta el momento
% obtenido a partir del uso del sistema probabilístico.

A modo de recordatorio, la evaluación de términos probabilísticos la llevamos a
cabo dentro de la mónada de sub-distribuciones.
%
La mónada de sub-distribuciones de un conjunto \(X\) es el conjunto de todas las
sub-distribuciones con conjunto soporte numerable en \(X\).
%
La evaluación de un término es interpretada como la sub-distribución
generada a partir del término donde un sub-conjunto numerable de términos tiene una
probabilidad mayor a \(0\) de ser el resultado de la evaluación, cuya suma es
menor o igual a \(1\).
%
De esta manera interpretamos que la suma de las probabilidades de los valores
del conjunto soporte es la probabilidad de la evaluación de converger a un
valor.

Agregamos costos al lenguaje aplicando la mónada de costo
interno~(Definición~\ref{def:cost:monad}) a la mónada de sub-distribuciones, y
como resultado, obtenemos una nueva mónada capaz de que interpretar los términos
probabilísticos pero con un sentido semántico ligeramente diferente.
%
Dado un término cerrado \(M\), la evaluación de \(M\) es interpretada como una
sub-distribución \(\mu : \V_{0} \times \mathbb{N}_{\infty} \to [0,1]\) indicando la
probabilidad de que un valor \(v\) sea el resultado de la evaluación con un
costo de \(n\) como \(\mu(v,n)\).

Una relación entre \(\lambda\)-términos cerrados \((R_{\T},R_{\V})\) es una simulación
de mejoras para el sistema probabilístico si y solo si respeta valores y además:
\[
  M \mathrel{R_{\T}} N \implies \forall U \subseteq (\V_{0} \times \mathbb{N}_{\infty}),
  ({[[ M ]]}_{\mathbb{N}_{\infty}}(U)) \leq
  ({[[ N ]]}_{\mathbb{N}_{\infty}}(R_{\V\mathbb{N}_{\infty}}(U)))
\]
%
Definimos la aplicación de una sub-distribución a un conjunto como la suma de
las probabilidades de su conjunto soporte.

Mientras que la relación de mejoras en el sistema de sub-distribuciones es el
resultado de instanciar la definición de mejoras al sistema de
sub-distribuciones, es decir, que dado un término \(M\) es mejorado por un
término \(N\) si y solo si para cualquier contexto \(\mathbb{C}\) que los
cierre, y conjunto \(U \subseteq (\V \times \mathbb{N})\):
\[
  {[[ \mathbb{C}[M] ]]}_{\mathbb{N}_{\infty}}(U)
  \leq
  {[[ \mathbb{C}[N] ]]}_{\mathbb{N}_{\infty}}(\mathcal{U}_{\mathbb{N}_{\infty}}(U))
\]

Donde \(\mathcal{U}_{\mathbb{N}_{\infty}}\) relaciona todo valor equipado con un
costo con cualquier otro valor que tenga un costo menor.
%
En símbolos podemos describirlo como:
\[
  \mathcal{U}_{\mathbb{N}_{\infty}} = \{ ((V,v_{c}),(W,w_{c})) \mid V,W \in \V, v_{c} \geq_{\infty} w_{c} \}
\]

Sea \(U \subseteq (\V_{0} \times \mathbb{N}_{\infty})\) un conjunto de valores equipados con sus costos.
%
Cuando aplicamos una sub-distribución de probabilidades \(\mu\) obtenida de la
evaluación de un término \(M\) a \(\mathcal{U}_{\mathbb{N}_{\infty}}\),
\(\mu(\mathcal{U}_{\mathbb{N}_{\infty}}(U))\), obtenemos la probabilidad de que la
evaluación de \(M\) converja a un valor con un costo menor o igual que al costo
supremo en \(U\).
%
Por ejemplo, si quisiéramos saber cual es la probabilidad de que la evaluación
de un término converja a un valor con un costo menor a \(42\), bastaría con
tomar a \(U = \{ (\lRet{I}, 42)\}\) con \(I\) la función identidad.

Un término \(M\) es mejorado en el sistema probabilístico por un
término \(N\) si y solo si, para todo contexto \(\mathbb{C}\) que los cierre, si
para todo conjunto de valores \(V\), la probabilidad que la evaluación de
\(\mathbb{C}[M]\), \({[[ \mathbb{C}[M] ]]}_{\mathbb{N}_{\infty}}\), alcance un valor
en \(V\) con costo menor a \(n\) es menor o igual que la probabilidad de que la
evaluación de \(\mathbb{C}[N]\), \({ [[ \mathbb{C}[N] ]]}_{\mathbb{N}_{\infty}}\),
converja con costo menor o igual a \(n\).
%
Notar que al igual que en los casos anteriores los valores alcanzados son
ignorados por el uso de la relación \(\mathcal{U}\), ya que lo que observamos
son los efectos, probabilidad de convergencia, y el costo utilizado por la
evaluación.


\section{Análisis de Costos Alternativos}\label{sec:alt:cost}

El análisis de costos presentado en la Sección~\ref{sec:dev:cost} introduce una
manera de interpretar costos en lenguajes con efectos: equipar los resultados de
computaciones con su correspondiente costo.
%
Sin embargo, también podríamos estar interesados en tener en cuenta el costo de
los efectos de otra manera, especialmente cuando hay resultados de
computaciones que pueden no tomar forma de valores, y por lo tanto, no llevar
consigo ninguna información de costo.

Un ejemplo claro es el caso del sistema de excepciones donde las excepciones
son interpretadas como valores distinguidos que no llevan ninguna
información de costos (son simplemente etiquetas).
%
Por lo tanto, la noción de mejoras derivada en la Sección~\ref{sec:cost:interno}
no compara los costos de las computaciones que terminan en una excepción.
%
Esto es causado por el uso de la mónada de costo
interno~(Definición~\ref{def:cost:monad}), donde para una mónada \(T\), la
mónada de costo interno equipa a valores con costos dentro de \(T\),
\(\cost{T} \doteq T \circ ( \_ \times \mathbb{N}_{\infty})\).
%
Más aún, derivamos el análisis de costos guiados por un relacionador mónadico para
\(T\) donde se comparan valores y costos; sin embargo, si la mónada \(T\)
interpreta algunos efectos como elementos sin información, como es el caso de la
excepciones, entonces va a haber elementos en \(\cost{T}\) sin costo asignado.

\begin{example}\label{ex:except:bad}
  Sea \(E\) un conjunto no vacío de excepciones y \(e \in E\).
  %
  Podemos probar que \(\<raise>_{e} \succeq_{\Gamma^{E}} \checkmark \<raise>_{e}\) dado que la
relación entre \(\lambda\)-términos
\((\bm{1}_{\T} \cup \{(\<raise>_{e}, \checkmark \<raise>_{e})\}, \bm{1}_{\V} )\) es una
simulación de mejoras.
  \end{example}

El problema se origina por equipar los valores con un costo.
%
Por lo que planteamos que en vez de llevar una noción de costo local a cada
valor, llevar un registro del costo total requerido para evaluar un
término.
%
En otras palabras, cambiar el foco a llevar un contador global de los ticks que
se produjeron durante la evaluación para eventualmente compararlo al final de la
computación.
%
A diferencia del enfoque anterior, para obtener una noción de mejoras,
necesitamos proveer al sistema con una mónada equipada con un relacionador mónadico
que formen un sistema compatible.
%
Comenzamos por definir un funtor que simplemente agregue un número natural al
sistema.

\begin{definition}[Funtor de Costo Externo]
  Sea \(T\) un endo-funtor.
  %
  Definimos el funtor de costo externo como:
  \[
    O_{T}(X) \doteq (T(X) \times \mathbb{N}_{\infty})
  \]
  \end{definition}
  %
En el caso que \(T\) sea un funtor, el funtor de costo externo también es un funtor.
%
Pero lamentablemente, no es un transformador de mónadas: dada una mónada \(T\),
\(O_{T}\) no es necesariamente una mónada.
%
De todas formas, para el caso de la mónada de excepciones, sí obtenemos una
mónada.
%
Esto es equivalente a utilizar el transformador mónadico de excepciones en la
mónada de costos, no al revés como hicimos en el Capítulo~\ref{ch:improvement}.

\begin{definition}[Transformador mónadico de Excepciones]
  Sea \(E\) un conjunto de símbolos sin interpretar representando excepciones y
\(M\) una mónada.
%
La siguiente construcción es una mónada capaz de interpretar excepciones y
posiblemente otros efectos dependiendo de la definición de \(M\):
\[
  \<ExceptT>_{M}(X) \doteq M (E + X)_{\bot}
\]
Donde para cada \(e \in E\), definimos el operador \(\<raise>_{e}\) interpretando
de la siguiente manera:
\[
  \<raise>_{e} \doteq \eta(\iota_{l}(e))
\]

Finalmente, la función \(\<lift>\) a continuación promueve computaciones
mónadicas de \(M\) a computaciones en \(\<ExceptT>_{M}\):
\[
  \<lift> \doteq M \, \iota_{r}
\]
  \end{definition}

Podemos entonces utilizar la mónada de costos,
\(\mathbb{D}(A) \doteq (A \times \mathbb{N}_{\infty})\)~\parencite{DalLago.2019.NormalForm},
para incorporar costos en el sistemas de excepciones,
\(\<ExceptT>_{\mathbb{D}}\), de forma tal que podemos interpretar excepciones
directamente con \(\<raise>\) y el operador de tick lo interpretamos simplemente
como \(\<lift> \, add_{1}\).
%
Si desenvolvemos la definición del transformador mónadico de excepciones vemos
que la definición es equivalente a la del funtor de costo externo.
%
Para todo conjunto \(E\) de excepciones:
\[
  \<ExceptT>_{\mathbb{D}}(X) \doteq (X + E)_{\bot} \times \mathbb{N}_{\infty}
\]
%
Por lo tanto, un relacionador válido para dicho sistema es el resultado de la
composición del relacionador de costos y el relacionador de excepciones:
\[
  \Gamma_{EC} \doteq \cost{\Gamma} \circ \Gamma_{E}
\]
%
Finalmente, el par \((\<ExceptT>_{\mathbb{D}},\Gamma_{EC})\) forma un sistema
compatible de excepciones, y más aún, ya tenemos todos los ingredientes para
implementar una función de evaluación con costos~(Sección~\ref{sec:cost:eval}).
%
Esta nueva definición que obtenemos de mejoras es más granular dado que puede
diferenciar computaciones que terminan en la misma excepción, pero que necesitan
diferentes costos.

\begin{example}[Ejemplo~\ref{ex:except:bad} revisado]
  Sea \(E\) un conjunto no vacío de excepciones y \(e \in E\).
  %
  Podemos ver que \(\<raise>_{e} \not\succeq_{\Gamma_{EC}} \checkmark \<raise>_{e}\) simplemente
ejecutándolos.
%
Asumamos que \(\<raise>_{e} \succeq_{\Gamma_{EC}} \checkmark \<raise>_{e}\), podemos evaluarlos
dentro del contexto trivial:
\[
  {[[ \<raise>_{e} ]]}_{EC} \equiv \<lift> \, add_{1} \, (\iota_{l}(e),0) \equiv (\iota_{l}(e), 1)
\]
mientras que
\[
  {[[ \checkmark \<raise>_{e} ]]}_{EC} \equiv \<lift> \, add_{1} \, {[[ \<raise>_{E} ]]}_{EC} \equiv
  \<lift> \, add_{1} \, (\iota_{l}(e),1) \equiv (\iota_{l}(e),2)
\]
%
Finalmente, no existe una relación \(R \subseteq X \times Y\) tal que al mapearla con el
relacionador \(\Gamma_{EC}\) se cumpla que
\((\iota_{l}(e),1) \mathrel{\Gamma_{EC}(E)} (\iota_{l}(e),2)\), por definición de \(\Gamma_{EC}\)
y el relacionador de costos.
  \end{example}

Parecería ser que podríamos obtener una \emph{mónada de costo externo}
simplemente apilando un transformador de mónada, con los efectos esperados,
sobre la mónada de costos.
%
Sin embargo, no es tan claro como obtener un sistema compatible a partir del
transformador de mónadas.
%
El transformador mónadico de excepciones está definido simplemente como la
composición de dos funtores, \(\<ExceptT>_{M} \doteq M \circ E\), y por ende, podemos
obtener un sistema compatible por la composición de funtores.
%
Sin embargo, la mayoría de los transformadores no encajan en este patrón.

Una característica esencial de los transformadores de mónadas es su habilidad de
mapear
computaciones~\parencite{Jaskelioff.2010.MonadTrans,Jaskelioff.2009.ModularMonadTrans},
y por lo tanto, deberíamos construir el sistema de costos alrededor de la
operación de mapeo de computaciones \(\<lift>\).
%
En otras palabras, deberíamos requerir que la operación sea un operador continuo.
%
Podríamos, entonces, ir un paso más y construir nuestro sistema comenzando con
los dos efectos básicos: divergencia y costos, para luego agregar nuevos efectos
simplemente apilando transformadores de mónadas.
%
El problema se encuentra cuando tratamos de definir un relacionador para dicho sistema.
%
Por lo que deberíamos entonces producir una nueva (en principio) noción de
\emph{transformador de relacionadores} que nos permita comparar elementos con efectos,
pero no esta claro como forzarlos a tener en cuenta la restricción sobre los
costos impuesta por el relacionador de costos.
%
Un candidato podría ser la siguiente definición.

\begin{definition}[Transformador de relacionadores mónadicos]
  Sea \(M\) una mónada y \(\hat{T}\) un transformador de mónadas.
  %
  Definimos un transformador de relacionadores mónadicos \(\Theta\) como un operador que
toma un relacionador mónadico \(\Gamma\) para \(M\), y retorna un relacionador mónadico para
\(\hat{T}M\), de forma tal que el operador \(\<lift>\) mapea valores
relacionados en valores con efectos relacionados.
%
Formalmente, dados dos conjunto \(X,Y\) y una relación \(R \subseteq X \times Y\), tenemos que:
\[
  \forall a \in M(X), b \in M(Y), a \mathrel{\Gamma R} b \implies \<lift>(a) \mathrel{\Theta \Gamma R} \<lift>(b)
\]
  \end{definition}

Lamentablemente, esta definición si bien es prometedora, deberíamos ser capaces
de adicionar una propiedad de forma tal que nos permita comparar costos.
%
En símbolos, que si tenemos que \(p \mathrel{\Theta \Gamma} q\) nos garantice que el costo
en \(q\) es menor o igual que el costo en \(p\).
%
Notar que al nivel semántico estamos cubiertos: si podemos aproximar términos
dentro de la mónada \(M\), podemos aproximar los mismo términos en la mónada
\(\hat{T}M\) dado que el operador \(\<lift>\) es continuo.
%
El problema está en que la noción de costos no es parte de
\(\hat{T}\) y por ende el transformador puede ignorarla totalmente.
%
Por ejemplo, el relacionador \(\Theta \Gamma\), puede olvidar estructura, y así, relacionar
términos ignorando su costo.

Finalmente, otro problema con este enfoque, es que algunas mónadas no tienen un
transformador mónadico asociado.
%
Un ejemplo concreto es el de la mónada de sub-probabilidades.
%
En cuyo caso se deberían estudiar como introducir nuevos efectos al sistema.

El caso de estudio de la excepciones es interesante ya que nos presenta con una
limitación y la necesidad de introducir un costo global de la evaluación de
términos.
%
Si bien pudimos introducir una noción de costo externo en el sistema de
excepciones, lamentablemente no queda claro como hacerlo para el caso general y
por lo tanto debemos analizar el caso de cada efecto por separado.

%%% Local Variables:
%%% TeX-master: "../Thesis.tex"
%%% TeX-PDF-mode: t
%%% ispell-local-dictionary: "es"
%%% End:

% LocalWords:  extensionalmente determinístico relacionador
