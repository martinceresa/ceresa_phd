%%Introduction to the general idea of my thesis.
\chapter{Introducción}\label{ch:Intro}

%%%%%%%%%%%%%%%%%%%% Capítulo Introducción Overview
%% Introducción: Pone la la escena y define el problema. Introduce la estructura de la tesis.
%% Intentional analysis of programs, and a relational approach.
%% + Punto departida: Qué es una optimización?
%% + Motivación : Por qué querríamos mejorar los programas?
%% + Justificación : Antes el hardware mejoraba y los programas también,
%% ahora no. Peor, tenemos lugares donde deberíamos explorar el hardware lo más posible.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Optimización, y la idea de probar propiedades extensionales e intensivas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\emph{Optimizar} un programa consiste en modificar un programa para que sea más
\emph{eficiente} o utilice menos recursos.
%
Es decir, dado un programa, este se transforma de manera tal que se obtiene otro
programa que es \emph{mejor} en aspectos \emph{intensivos} pero que es
\emph{equivalente} al original de forma \emph{extensional}.
%
Esto hace que probar que una transformación es efectivamente una optimización
sea \textbf{difícil}, ya que requiere realizar dos pruebas: por un lado hay que
probar que el sentido semántico se mantiene, es decir que nuestro nuevo programa
se comporta de forma \emph{similar} al original; mientras que además es
necesario probar que la ejecución es mejor que la ejecución del programa
original de alguna manera observable y cuantificable.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Por qué queremos optimizar? Contar que en algunas áreas de la computación esto
%% es relevante.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Si bien el estudio de los costos y la complejidad de los programas es muy
relevante para el desarrollo y estudio de algoritmos, durante una época la
industria del software se benefició del avance en los componentes electrónicos
de la computadora.
%
Mientras la famosa \emph{Ley de Moore}~\parencite{Moore.1965.Law} estaba en auge
y los procesadores duplicaban la cantidad de transistores cada dos años, los
\emph{mismos programas} se volvían más y más rápidos \emph{sin tener que
modificarlos}, ya que el hardware subyacente mejoraba.
%
No era necesario transformar el programa de manera alguna para obtener una
mejora en el tiempo de ejecución, simplemente se cambiaba el procesador o la
memoria y todos los procesos eran más rápidos.
%
Esta historia tiene su fin, ya que la perdida de energía en forma
de temperatura vuelve inadmisible continuar aumentando el poder de cómputo de
los procesadores, obligando a los fabricantes de procesadores a optar por
introducir múltiples unidades de procesamiento.
%
Esto obligó entonces a los programadores y diseñadores de software a cambiar la
forma de pensar el software y los obligó escribir software que fuese más rápido,
es decir, presentar soluciones algorítmicas más rápidas.
%
Incluso se introduce un nuevo requisito dentro del desarrollo de software:
escribir software previendo la inclusión de futuras optimizaciones con el
objetivo de obtener el mayor provecho del hardware subyacente.

%% Hardware embebido
No siempre se cuenta con unidades de procesamiento avanzadas o con la capacidad
de renovar el hardware subyacente.
%
Por ejemplo, en unidades de procesamiento embebidas utilizadas en satélites
geoestacionarios.
%
A su vez el poder computacional del hardware puede ser extremadamente limitado o
incluso el acceso al hardware puede ser limitado, haciendo que los
desarrolladores deban escribir programas pensando en el uso correcto y eficiente
del hardware subyacente.

%% Compiladores
Diferentes áreas de la computación se han dedicado a estudiar el concepto de
optimización de programas, donde posiblemente la más prominente sea el área de
\emph{compiladores}.
%
Los compiladores son programas que transforman un programa escrito en un
lenguaje de alto nivel en un programa \emph{equivalente} en un lenguaje objeto que
puede ser ejecutado por una unidad de
procesamiento~\parencite{Muchnick.1998.AdvancedCompiler}.
%
De forma amplia también podemos pensar a los compiladores como traductores de un
lenguaje a otro, y que para realizar dicha tarea de traducción, utilizan varias
etapas de procesamiento en las cuales se pueden aplicar transformaciones que no
sólo mantienen el sentido semántico del programa sino que además mejoran su
rendimiento.
%
Donde mejorar el rendimiento puede ser, por ejemplo, menos instrucciones de
código ensamblador, o eliminando fragmentos de código que nunca serán accedidos
en la ejecución del programa.
%
Por ejemplo, podemos transformar programas recursivos de cola en bucles
\<for>~\parencite[Capítulo~15]{Muchnick.1998.AdvancedCompiler}, a los cuales se les
pueden aplicar técnicas conocidas de optimización de
bucles~\parencite[Capítulo~14]{Muchnick.1998.AdvancedCompiler}.
%
Una optimización aún más agresiva es la eliminación de sub-expresiones
compartidas~\parencite[Capítulo~13]{Muchnick.1998.AdvancedCompiler}, donde se busca
reemplazar la evaluación de una misma sub-expresión múltiples veces por una
sola.

Ya que los compiladores son una secuencia de transformaciones de programas,
basan su correctitud en probar que cada transformación aplicada es correcta.
%
Pero para saber si una optimización es correcta además deberíamos probar que
realmente el programa que se obtiene es mejor, lo cual introduce la necesidad de
comparar propiedades intensivas de la ejecución de programas.

\section{Análisis de Costos y Análisis Asintótico}

El análisis de un algoritmo consiste en predecir el consumo de recursos necesarios
por el algoritmo~\parencite[Capítulo~2.2]{Cormen.2009.IntroductionAlgorithms}.
%
En general se suelen utilizar como medida el espacio, el tiempo, o la cantidad de
instrucciones, que se necesitan para su evaluación.
%
En el caso de tener varios algoritmos que resuelvan el mismo problema, se pueden
realizar análisis para determinar cuál es el que mejor se adapta a los
requerimientos del sistema.

%% Costos
El \emph{costo} de un programa consiste entonces en realizar una medición
precisa de la cantidad de unidades básicas que toma su ejecución.
%
Las unidades básicas es una medición que se realiza contando alguna operación
básica como ser acceso a disco, llamadas a función, instrucciones del CPU, entre
otras.
%
Podemos diferenciar entonces dos niveles de abstracción: el más abstracto, macro
costos, donde se cuentan operaciones de alto nivel como llamadas a función; y un
nivel más bajo, micro costos, donde se cuentan operaciones de bajo nivel como
ser instrucciones del CPU.

%% Complejidad
La \emph{complejidad} de un algoritmo se refiere entonces al análisis asintótico
de la función de costos que describe el uso de recursos necesarios para la
evaluación algoritmo en base al tamaño de la entrada.
%
El tamaño suele considerarse un valor en el dominio de los enteros positivos que
codifica el tamaño real de las diferentes entradas del algoritmo, por ejemplo,
en bits.
%
Por ejemplo, un algoritmo que toma una lista de elementos y los reordena de
forma tal que quedan en orden inverso, en general se dice que tiene una
complejidad de orden \(O(n)\), ya que la función de costos del algoritmo es
lineal respecto a la longitud de la lista \(n\) que toma como entrada.

La complejidad es muy útil para obtener una \emph{intuición} sobre propiedades
intensivas de nuestros programas y es muy utilizado al momento de hacer
análisis sobre programas y algoritmos.
%
En particular, nos permite además encontrar límites teóricos a ciertos
algoritmos, por ejemplo, que no es posible ordenar una lista de elementos en
menos de \(O(n\log(n))\) utilizando algoritmos de ordenamiento basados en la
comparación de elementos.
%
Pero, al momento de comparar la ejecución de programas, estos no se ejecutan
de forma asintótica, sino que por tiempo finito con restricciones físicas
reales, haciendo que el comportamiento asintótico no sea el mejor análisis.
%
Esto se agrava por el hecho que el análisis asintótico oculta constantes y
factores dentro de la complejidad de los programas que juegan un papel
importante al momento de comparar la ejecución de dos programas.
%
Otro punto débil de la notación \(O\)-grande es cuando se requiere comparar la
ejecución de dos programas, por ejemplo donde uno es un \emph{cuanto} más
rápido que el otro.
%
Siguiendo el análisis asintótico estaríamos presos a decir que ambos programas
comparten su comportamiento, pero sabemos que no comparten el mismo costo.
%
Por lo que el análisis de costos de los programas es el enfoque preferido para
el estudio de optimizaciones de programas.

En esta tesis nos concentraremos en el análisis de costos de programas para
establecer optimizaciones reales sobre los mismos.
%
A su vez nos concentraremos en unidad de costos macro, relativas al lenguaje, ya
que sino estaríamos estableciendo unidades de costos relativas al hardware y
perderíamos generalidad.
%
Trabajamos basándonos en la posibilidad de observar \emph{una unidad de costo}
abstracta que llamaremos \emph{tick}, que nos permite observar cuando un
programa utiliza más recursos que otro, simplemente comparando los ticks
necesarios para la evaluación de un programa con los del otro.

\section{Lenguajes Funcionales}

En un principio los lenguajes de programación se desarrollaron partiendo de las
instrucciones de las unidades de procesamiento.
%
Esto facilitaba la construcción de compiladores, ya que la traducción era casi
directa o bien muy sencilla, pero con la evolución del hardware también vino la
necesidad de diseñar nuevos lenguajes que sean capaces de expresar mejor los
algoritmos y programas con \emph{mayor abstracción} que estén más cerca del
humano que de la unidad de procesamiento.
%% %

%%  Lenguajes funcionales y teoría de costos en ellos
Los lenguajes funcionales son una respuesta a la necesidad de los programadores
de expresar más fácilmente los programas, alejándose de las unidades de
procesamiento y del \emph{cómo} se deben de ejecutar los programas, para que los
programadores se concentren en \emph{qué} se debe computar.
%
Los lenguajes de programación funcional son descendientes del modelo
computacional del \emph{lambda calculus}.
%
Son lenguajes de programación de un alto nivel de abstracción, es decir, se
sitúan lejos de las instrucciones que se ejecutan en las unidades de
procesamiento.
%
La propiedad fundamental de los lenguajes de programación funcionales es que el
mecanismo que acciona la computación es la aplicación de
funciones~\parencite{Hutton.2016.HaskellProgramming}, a diferencia de otros
paradigmas que se basan en accesos a memoria como son los lenguajes imperativos.
%
Esto permite que los programadores desarrollen nuevas técnicas para facilitar la
construcción de nuevos programas más complejos, y debido a que se encuentran más
cerca del lenguaje de la matemáticas, se pueden expresar propiedades sobre los
programas de forma más sencilla.
%
A su vez los lenguajes funcionales pueden incorporar otros mecanismos como ser:
%
\begin{description}
  \item [Funciones Recursivas.] Permiten (y fomentan) la definición de funciones
    recursivas, funciones que se definen utilizando otra instancia de sí mismas.
  \item [Funciones de alto orden.] Las funciones son ciudadanos de primer orden,
    pueden ser argumentos o resultados de otras funciones.
  \item [Tipado Estático.] El código fuente es tipado previamente a la ejecución,
    eliminando errores de ejecución.
  \item [Evaluación Lazy.] Los términos se evalúan a medida que van siendo
    necesarios para la computación.
  \end{description}

% Los lenguajes funcionales, al basar su modelo de cómputo en la aplicación de
% funciones, relegan una parte importante al compilador/intérprete del lenguaje.
%
Al igual que las matemáticas, los lenguajes funcionales son un excelente medio
para explicitar la semántica de los programas relegando decisiones al compilador
quien eventualmente deberá emitir código en un lenguaje de menor nivel de
abstracción.
%
Por lo que, por definición, los compiladores de lenguajes funcionales realizan
transformaciones radicales al código fuente para obtener una secuencia de
instrucciones de bajo nivel.
%
Esto permite al compilador realizar varios tipos de transformaciones, de alto
nivel, transformando fragmentos de código funcional por otro, transformando
fragmentos de código funcional en secuencia de instrucciones, y finalmente,
transformando secuencias de instrucciones en otras.

% Difícil al momento de computar costos o hacer análisis
La separación que proponen estos lenguajes entre la definición del programa y la
logística de su evaluación, impone una cierta complejidad al momento de realizar
el análisis de programas funcionales.
%
Una función es la descripción de un valor en base a varios valores de entrada.
%
Es decir, no establece cómo computar esos valores sino que intenta describir el
valor resultante en base a las diferentes observaciones de los valores de
entrada.
%
Estos tipos de programas no establecen una correlación directa entre el programa
que uno escribe y, por ejemplo, cómo se guardan los datos en memoria, si es
necesario el uso de memoria dinámica o memoria estática, entre otras decisiones.
%
Estas tareas son delegadas al compilador, haciendo que el razonamiento sobre el
uso de recursos en lenguajes de programación funcional sea complejo, en
particular cuando el lenguaje es lazy~\parencite{Okasaki.1998.PurelyFunct}.

Sin embargo, debido a que, en general, los lenguajes funcionales tienen una
semántica clara, el desarrollo de transformaciones resulta fácil e intuitivo.
%
Esto se da principalmente en lenguajes de programación funcional denominados
puros con \emph{transparencia referencial}.
%
La transparencia referencial permite realizar un razonamiento ecuacional sobre
los programas, permitiendo reemplazar un fragmento de código por otro
\emph{equivalente}.
%
En otras palabras, podemos probar transformaciones de programas de forma muy
elegante, pero probar optimizaciones sobre programas en lenguajes funcionales es
más complicado, ya que no existe una clara conexión entre un programa y las
propiedades de su evaluación.
%
Para esto, tenemos que concebir el programa en conjunto con su función de
evaluación, es decir, con una semántica dada y no solo una mera transformación
de símbolos.
%
Afortunadamente, existe una teoría que se adapta muy bien a los lenguajes
funcionales, y que permite expresar optimizaciones sobre la ejecución
de programas, la \emph{teoría de mejoras}.

La Teoría de Mejoras~(\emph{Improvement Theory}), desarrollada en los 90s por el
Profesor \emph{David Sands}~\parencite*{Sands:PhDthesis,Sands:Skye}, busca
mezclar conceptos intensivos y extensionales sobre los programas ya que
queremos que una transformación sea correcta (extensional) pero también buscamos
mejorar nuestros programas (de forma intensiva).
%
Para obtener una teoría de mejoras es necesario introducir conceptos cercanos a
las unidades de procesamiento, como memoria o tiempo computacional, a los
evaluadores o intérpretes que son quienes definen \emph{cómo} se computan los
programas.
%
Por esta misma razón utilizaremos conceptos de costos macros antes
mencionados.

Como veremos más adelante, nuestro objeto de estudio son los lenguajes de
programación funcionales pero que además introducen  \emph{efectos
computacionales}.
%
Es decir, no son lenguajes \emph{puros} sino que son lenguajes que introducen
operaciones que generan un cierto \emph{efecto computacional}, como ser, entrada
y salida, estado global, manejo de errores, no determinismo, operadores
probabilísticos, entre otros.
%
El objetivo de esta tesis es el de obtener una teoría de mejoras que nos
permitan establecer optimizaciones en lenguajes con efectos.

\section{Efectos Computacionales y Algebraicos}

Los efectos computaciones son aquellos efectos producidos por la
evaluación de un término de un lenguaje de programación.
%
Un ejemplo clásico es la entrada/salida en un programa, hay programas que pueden
depender de la entrada, o bien, pueden mostrar por pantalla información de la
ejecución o incluso el resultado del mismo.
%
Esto rompe la ilusión de que los programas son funciones matemáticas, o al
menos, no lo son directamente.
%
Introducir este tipo de nociones a los lenguajes funcionales nos permiten, entre
otras cosas, interactuar con el entorno, como además introducir operaciones que
permiten describir procesos del área de aplicación.
%
En otras palabras, los programas ya no serán funciones sino que tendrán un
comportamiento adicional.
% la evaluación o la identificación de programas que toman una
% entrada de tipo \emph{A} y devuelven una salida de tipo \emph{B} no son
% funciones matemáticas \(A \to B\) sino que pueden tener efectos adicionales.

El estudio de efectos computacionales es el foco de atención desde hace varias
décadas, comenzando con la visión unificadora de
Moggi~\parencite{Moggi1989ComputationalLambdaCalculus}, donde los efectos son
observables desde el punto de vista del sistema, e incluso estudios más
recientes explorando diferentes estructuras
matemáticas~\parencite{Plotkin.2004.ComputationalEffects}.
%
% continuando la brillante idea del Prof.~Moggi
En el lenguaje de programación Haskell, las computaciones que involucran un
efecto son marcadas al nivel de tipo con lo que se denominan
\emph{mónadas}~\parencite{Moggi.1991.NotionsCompMonads,Moggi1989ComputationalLambdaCalculus}.
%
Por lo que resulta muy común encontrarse con funciones puras, i.e. \(f : A \to B\),
y además funciones que tienen algún efecto computacional \(mv : A \to M(B)\),
donde el efecto se encapsula en \(M\).
%
Un ejemplo es la mónada \emph{IO} en Haskell, que es la que se encarga de toda
la interacción entre programas Haskell y el mundo exterior.

Los efectos algebraicos~\parencite{Plotkin.2004.ComputationalEffects}
presentan un enfoque diferente desde una perspectiva del estudio de la
teoría de lenguajes.
%
Los efectos computacionales dentro de la evaluación de un programa se presentan
mediante el uso de ciertos \emph{operadores} que introducen efectos impuros.
%
Por ejemplo, \emph{set}/\emph{get} para el manejo de memoria mutable,
\emph{read}/\emph{print} para obtener interacción con la entrada y salida, etc.
%
De esta manera, los efectos computacionales quedan restringidos a aquellos
introducidos por los operadores.
%
Esto involucra entonces una modificación al lenguaje, relegando la
interpretación de los efectos al momento de la evaluación.

\section{Semántica Operacional y Semántica Denotacional}

La semántica operacional se dedica a estudiar los programas en base a cómo
se evalúan los operadores del lenguaje, mientras que la denotacional se encarga
de encontrarle un sentido al programa definiendo su comportamiento en un dominio
matemático que modela su comportamiento.
%
Debido a que el objeto de estudio de la tesis es una teoría de costos con
efectos algebraicos, deberemos relacionar conceptos operacionales (costos) y
denotacionales (interpretación de los efectos).
%
Es decir, necesitamos mezclar  propiedades intensivas con propiedades
extensionales, propiedades sobre la ejecución de un programa funcional con el
comportamiento del mismo.

\section{Teoría de Mejoras sin y con Efectos}

La teoría de mejoras busca introducir conceptos operacionales dentro de la
denotación de programas.
%
Se equipa la relación de evaluación con el costo necesario para evaluar un
término en un valor, y luego se utiliza estos costos para establecer
relaciones entre programas.
%
Al tener una relación de evaluación exponiendo el costo de términos podemos
definir una nueva relación más refinada de equivalencia observacional, obteniendo
entonces la noción de mejora.
%
Un programa mejora a otro si no hay contexto que observe un empeoramiento en la
evaluación del programa completo.
%
Por lo que la relación de mejora entre programas es un refinamiento de la
aproximación observacional entre programas.

Las definiciones observacionales, si bien son muy intuitivas y expresivas, son
en general complicadas para desarrollar teorías.
%
Esto es debido a que utilizan cuantificadores universales ``no hay contexto que
observe\ldots''.
%
Por lo que en general, estas definiciones vienen equipadas con un cuerpo teórico
que facilita las demostraciones, acotando el espacio de búsqueda, como ser los
``lemas de contexto'', o derivando teoremas que permitan mejorar programas de
forma modular como ser el ``álgebra de ticks''.

La definición de aproximación observacional deja a libre interpretación qué es
observar.
%
Esto permitió que, al exponer información de la evaluación, el costo, Sands
pueda observar una mejora, y así, poder definir formalmente mejoras entre
programas.
%
En esta tesis utilizaremos la misma idea, pero con la presencia de efectos
algebraicos.
%
Partiendo de una noción de equivalencia observacional de programa con efectos,
en esta tesis, definimos una noción de mejoras que observe además efectos
algebraicos, siguiendo los pasos de la teoría de mejoras tradicional.
%
Esto lo logramos en dos pasos:
primero, agregamos a la relación de evaluación de programas a valores con
efectos el costo necesario para computar dicho valor;
%
segundo, introducimos un nuevo operador algebraico que representa el consumo de
un cómputo de costo, llamado tick.
%
En otras palabras, exploraremos como agregamos el efecto de computar el costo de
la evaluación a los efectos presentes en el lenguaje, y además, modificaremos la
observación para además tener en cuenta el costo de la evaluación de programas.

Por ejemplo, asumamos un cálculo lambda con una familia operadores binarios
\(\oplus_{r}\) que representa la elección probabilística, \(p \oplus_{r} q\) evalúa a
\(p\) con probabilidad \(r\) o a \(q\) con probabilidad \(1 - r\).
%
Nos preguntamos entonces como es posible optimizar dicho término, más aún, si
podríamos optimizar \(p\) obteniendo otro término \(p'\), ¿es \(p' \oplus q\) una
optimización de \(p \oplus q\)?
%
Peor aún, todavía no contamos con una definición de lo que es mejorar un
programa probabilístico.

Introducir una noción de mejoras con efectos no es tan claro como parece, más
aún cuando se busca obtener un cuerpo teórico general que nos permita introducir
diferentes efectos.
%
Durante la tesis estudiamos además el efecto de no-determinismo y excepciones.
%
Concentramos nuestros esfuerzos en encontrar una teoría \emph{general} que nos
permita caracterizar optimizaciones.

\section{Trabajo Relacionado}

Desde el comienzo de la teoría de mejoras en los '90, el estudio de análisis de
costos ha avanzado aunque no se puede decir que mucho.
%
La teoría de mejoras tuvo un gran desarrollo mientras fue el foco de estudio del
Prof.~Sands en los '90, pero se desarrolló muy poco hasta recientemente que ha
cobrado relevancia nuevamente en el área~\parencite{Hackett.2019.Clairvoyant,
Handley.2020.LiquidateAssets}.
%
A diferencia del trabajo de Sands, estos trabajos han consistido en buscar
definir teorías de mejoras para lenguajes específicos con el objetivo de obtener
una relación más refinada que la equivalencia y probar optimizaciones sobre los
mismos.
%
Es decir, no se han desarrollado nuevas técnicas en el uso de teorías de
mejoras, sino que simplemente se ha aplicado la teoría a diferentes lenguajes
con el objetivo de mostrar optimizaciones.

\paragraph{Erratic Fudgets}
Se ha estudiado agregar operadores, \emph{erratic fudgets}, a un
lenguaje funcional reducido con el objetivo de introducir no-determinismo en los
programas~\parencite{Moran.1999.ErraticFudgets} obteniendo además una teoría de
mejoras.
%
Sin ser el objetivo del artículo, los
autores~\citeauthor{Moran.1999.ErraticFudgets} presentan la primer teoría de
mejoras en un lenguaje con efectos algebraicos: no determinismo introducido por
operadores llamados erratic fudgets.
%
En esta tesis estudiaremos la posibilidad de introducir el efecto de
no-determinismo al lenguaje, pero a diferencia de los erratic fudgets,
buscaremos hacer observaciones más refinadas sobre las posibles observaciones
del sistema.
%
Además, estudiamos lenguajes con efectos algebraicos de forma general, donde
primero desarrollamos la teoría de forma paramétrica en los efectos algebraicos.

\paragraph{Un enfoque denotacional para medir complejidad computacional en Lenguajes Funcionales}
%
\emph{Katheryn Van
Stone}~\parencite{Katheryn2003DenotationalMeasuringComplexity} presenta en su
tesis doctoral, como bien dice el título, un análisis de complejidad dentro de
lenguajes funcionales con un punto de vista denotacional.
%
Es decir, se introducen nociones de costos dentro de la interpretación de
programas funcionales como funciones matemáticas, introduciendo además efectos
siguiendo las ideas de
Moggi~\parencite{Moggi1989ComputationalLambdaCalculus}.
%
Nuestro enfoque es diferente ya que introducimos efectos mediante operadores,
permitiéndonos hacer una análisis más específico a cada lenguaje manteniendo un
framework general.
%
Además, nuestro análisis gira entorno al análisis de costo de programas,
mediante la adición de un nuevo operador tick.
% :a la noción de tick y la introducción del
% mismo como un nuevo operador, es decir, de costos de programas, y no de análisis
% asintótico.

\paragraph{Espacio Métricos}
%
Otra noción posible es interpretar programas con costos dentro de un espacio
métrico, y construir una teoría de mejoras alrededor de dicha
construcción~\parencite{Hackett.2019.Clairvoyant}.
%
De esta manera se puede formalizar de forma elegante la noción de que la
evaluación de un programa es menor que la del otro, y además permite demostrar
propiedades muy útiles mapeando propiedades de los espacio métricos.
%
Nuestro enfoque en cambio es diferente, buscamos obtener teorías de mejoras para
diferentes lenguajes, introduciendo de forma modular los diferentes operadores
que introducen efectos.

\paragraph{Análisis de Costo Relacional}
%
Otro trabajo reciente es la definición de relaciones de costos, como ser
RelCost~\parencite{Cicek2017RelCost}, donde los autores definen un sistema de
tipos refinados y un sistema de efectos donde se agrega el análisis de costos.
%
El objetivo, y resultado, de dicho análisis es obtener una cota estimada del
costo de la ejecución para así compararla entre diferentes programas.
%
Difiere a nuestro enfoque en varios aspectos: 1) en esta tesis se deriva una
noción de mejora partiendo de la noción de equivalencia, y por lo tanto,
las optimizaciones son transformaciones correctas entre programas; 2) en este trabajo
es sencillo introducir nuevos efectos ya que 3) el manejo de efectos algebraicos
es explícito; 4) no es necesario conocer los costos de ejecución de los
programas, simplemente mostrar que uno es menor que el otro.

% \begin{description}
  % \item[Moran] Time-space fudgets
  % \item[Katheryn Van Stone] A denotational approach to Measuring Complexity in FP
  % \item[Hacket\&Hutton] Space metrics
  % \item[RelCost] Relantional cost analysis
  % \end{description}
% So if the theory is already there, why am I writing this?

Un problema que tienen las teorías de mejoras, es que, para cada nuevo lenguaje
uno debe desarrollar toda la teoría de nuevo.
%
Si bien se han realizados avances en el área, y es posible obtener teorías de
mejoras de algunos lenguajes, no se han encontrado trabajos en lenguajes
con efectos algebraicos, por ejemplo, que sean capaces de incorporar efectos con
simplicidad sin tener que computar toda una teoría de análisis de costos o
complejidad.
%
Aún peor, no siempre es posible establecer una métrica clara al momento de
definir cómo se computan los costos de la ejecución de un programa, por ende, no
siempre es posible definir una \emph{mejora} directamente.
%
En este trabajo obtendremos una forma sencilla de derivar una teoría de mejoras
partiendo de la noción de aproximación observacional.
%
Dicha teoría derivada no siempre es tan refinada, por lo que exploraremos otras
métodos para obtener una teoría de mejoras con efectos.
%
La comparación entre teorías de mejoras queda fuera de este trabajo.

\section{Organización y Enfoque de la Tesis}

La tesis se basa en el uso de diferentes técnicas y teorías de las ciencias de
la computación:
\begin{description}
\item [Aproximación Observacional] Nos permite definir de forma clara cuando un
programa se comporta observacionalmente similar a otro.
\item [Teorías de Mejoras] Nos permite definir una relación capaz de probar
optimizaciones sobre programas.
\item [Semánticas de Álgebra Inicial y Álgebras Continuas] Nos permiten
introducir operadores y definir los lenguajes de forma sencilla.
\item [Semántica Formal y Teoría de Dominios] Utilizamos la teoría de dominios
para darle sentido semántico a los programas, conectando con álgebras continuas,
al momento de definir operadores.
\end{description}

La tesis está dividida principalmente en dos partes:
\begin{description}
        \item[Teoría de Mejoras sin Efectos:] En la primer parte introduciremos
los conceptos fundamentales de teorías de mejoras dentro de un lenguaje sin
efectos, donde esbozaremos la mayoría de las técnicas que utilizaremos en la
segunda parte.
        \item[Teoría de Mejoras con Efectos:] Se introducirán los efectos
algebraicos al lenguaje, y veremos como arribar a una definición de mejoras
partiendo de la aproximación observacional.
\end{description}

La segunda parte de la tesis buscará recorrer el mismo camino que la primera,
pero agregando efectos algebraicos.
%
De esta manera primero introducimos técnicas generales, y luego nos enfocamos de
lleno en los conceptos relacionados a los efectos algebraicos.

El resto de la tesis está dividida en capítulos.
%
En el Capítulo~\ref{ch:PrevTheo} vemos los conceptos básicos que se utilizan a
lo largo de la tesis, pero sólo los necesarios para poder desarrollar toda la
primer parte de la tesis, es decir, no se introducen los efectos hasta que
sean necesarios.
%
En el Capítulo~\ref{ch:lenguaje} introducimos el lenguaje
que utilizamos como vehículo para introducir las nociones de equivalencia en
el Capítulo~\ref{ch:equiv} y la teoría de mejoras en el
Capítulo~\ref{ch:Effectless}.
          %
Luego comenzamos con la segunda parte de la tesis, se introducen efectos en
el Capítulo~\ref{ch:Effects}, se define la noción de aproximaciones en el
Capítulo~\ref{ch:equiv:eff}, y se deriva una teoría de mejoras con efectos en
el Capítulo~\ref{ch:improvement}.
          %
En el Capítulo~\ref{ch:notionsofimprovement} derivamos la respectiva teoría de
mejoras para cada uno de los efectos algebraicos utilizados como ejemplo a lo
largo de la tesis y vemos además un límite del enfoque y cómo
solucionarlo.
          %
En el Capítulo~\ref{ch:examples} mostramos dos optimizaciones utilizando teorías
de mejoras definidas en la tesis.
          %
Finalmente concluimos el trabajo con una sección de conclusiones y trabajos
futuros en el Capítulo~\ref{ch:conclusions}.

\section{Publicaciones}

El trabajo de esta tesis está publicado en la
revista internacional~\citefield{Ceresa.2022.EffectfulImp}{journaltitle} bajo el
título~\citetitle{Ceresa.2022.EffectfulImp}.
%
A diferencia de la tesis el artículo hace un desarrollo resumido sobre el camino
realizado durante la investigación, es decir, se muestra directamente los
resultados obtenidos.
%
En este documento nos centraremos en hacer una presentación desarrollando la
intuición detrás de la investigación y posibles extensiones al trabajo.

\fullcite{Ceresa.2022.EffectfulImp}

\subsection{Otros trabajos}

Durante el doctorado, producto de la colaboración con otros investigadores, se
obtuvieron las siguientes publicaciones.
          %
Estos trabajos no forman parte del presente documento, pero forman parte de mi
desarrollo como investigador.

\paragraph*{HLola}
          %
Lola es un lenguaje que permite especificar monitores para la monitorización en
tiempo de ejecución de propiedades temporales en sistema síncronos.
          %
Una de las ideas fundamentales en la que se basa es en una separación clara
entre los datos (teorías) y las propiedades temporales de los mismos~\parencite{Lola}.
%
Dado que los datos utilizados en la mayoría de los monitores son observables, es
posible utilizar el potente sistema de tipos de Haskell para finalmente dar una
implementación que cumpla con dicha separación.
%
A dicha implementación la denominamos HLola.

\fullcite{HLola}

\paragraph*{QuickFuzz}
          %
QuickFuzz es una herramienta escrita en Haskell que permite reutilizar las
librerías para escritas por la comunidad para la generación de casos de prueba.
          %
Rápidamente, la idea principal del proyecto consiste sacarle provecho a las
representaciones internas que de tipos de datos como ser imágenes, fragmentos de
código, archivos comprimidos, documentos (pdfs, docx, odt, entre otros) y
archivos multimedia como Ogg, Midi y TTF.
%
En Haskell, en general, los programadores definen una estructura para
representar los diferentes tipos de datos, en QuickFuzz definimos técnicas
agresivas y automáticas para la generación de elementos arbitrarios de que
cumplan con la estructura definida.
          %
A estos elementos generados de forma arbitraria, y sin sentido semántico a
priori, luego son mutados utilizando herramientas clásicas del área para generar
fallos en otras herramientas.
%
Las publicaciones obtenidas fueron:

\fullcite{GriecoCMB17}

\fullcite{GriecoCB16}


%%% Local Variables:
%%% TeX-master: "../Thesis.tex"
%%% TeX-PDF-mode: t
%%% ispell-local-dictionary: "es"
%%% End:
