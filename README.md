# Intentando Doctorar a Martin Ceresa

## Progreso:

* Año 1: Failed
* Año 2: Failed
* Año 3: Failed
    + ~~Empezar con el paper.~~
    + ~~Modularizar la libreria.~~
    + ~~Re-hacer lo de modelica.~~
    + Cerrar lo que se hizo (Vacuously Done).
* Año 4: Failed.
    + ~~ Improvement Theory through Turi. ~~ Discarded
    + Improvement Theory meaning?
    + ~~Abs. Mach.~~ Improvement Theory.
        * Hutton's Razor
        * SKI
        * Lambda Calc
    + Generalized Theory of Improvements based on ~~Operational Semantics.~~
      Initial Algebra Semantics.
      * ~~Programar algo en Coq~~ Failed.
      * ~~Llegar a algún resultado~~.
      * Intentar Publicar algo Failed
      * Comenzar a escribir algo. Failed
* Año 5: Failure in Progress
    +  ~~Terminar Manes. Terminar el estudio en Enero.~~
      * [x] Conectar terminacion (e implicancia logica) con Scott ordering.
        Esto es conectar aproximación observacional con Scott Ordering, donde claramente
        tiene sentido hacerlo en lenguajes funcionales, si son valores se espera el
        mismo resultado. Pensar que (exists v. C[t]-*> v) ==> (exists v'. C[t']-*> v') es un Scott Order.
      * [ ] Sumar estructura a los monoides aditivos para el manejo de costos.
        En realidad esto es agregar estructura para el manejo de costos. Pasar a trabajar con categorias
        enriquecidad, sin necesariamente toda esa teoría.
      * [ ] Operadores con Kleene Semantics y costos.
        Creo que por acá viene el teorema de Improvement Induction, es básicamente cómo
        podemos manejar los costos con puntos fijos, cuando y donde tengo estos valores,
        básicamente cuando trabajo con funciones recursivas o cuando estoy trabajando con
        operadores dentro del dominio semantico (diferencia entre constructores y operadores)

--------------------------------------------------------------------------------

## Estado Actual

**Más perdido que el Chavo del Ocho.**

Si quiero probar y manipular nociones de puntos fijos (para probar Induction Improvement),
necesito tener en mi dominio semántico dichas nociones. Parece que para que esto esté bien
relacionado necesito lo que Goguen llama *Continuous Algebras*. Esto es lo que estoy viendo ahora.

Set -> Lattice à la Dana Scott. Goguen prepara una IAS para un lenguaje con dominio semántico
V = I + B + [V -> V] , podemos meter los costos ahí?

## Plan de Trabajo

Habl'e con Mauro, implementar como ejemplo de la teoría el Lambda Calculus a lo
Morans & Sands. La idea de mecanización de estos resultados resultó ser medio un fiaso,
no estoy tan seguro que me sirva hacer esto si no puedo llegar a entender bien o demostrar
los resultados primero en papel, rápidamente me encuentro con limitaciones mías usando agda y
parece que me estoy pegando un tiro en el pie.

Papers relevantes (y no):

+ [x] Initial Algebra Semantics for Lambda Calculi. J. W. Gray
  Teóricamente relevante, nos da pauta que todo andaría perfecto utilizando como
  categoría base a $bSet^{\bullet}$. No es novedad, pero me da tranquilidad(?).
  Lamentablemente implementar esto en *Agda* es mucho trabajo, con dificultades
  intrínsecas que no estoy en capacidad de predecir (no se nada).
+ [x] Type-and-Scope Safe Programs and Their Proofs/Triangulating Context Lemmas
  Relevante pero inservible, trabajan siempre con computaciones que terminan y
  por ende teoremas interesantes como Induction Improvement carecen de sentido.
+ [ ] Nail Ghani sugirió *Induction&Recursion*.
  Todavía no exploré esta sugerencia. Leí un poquitito pero ni idea.
  *look up induction recursion which is a powerful meta-language for giving
  initial algebra semantics to typed languages*
  

Volviendo...

+ [ ] Bounded λ-Calculus execution. 
  Acá no la noción de gas no está conectada con la de runtime execution. Esto lo
  podemos discutir más adelante pero no me termina de cerrar...
+ [x] Delay Monad.
  Danielsson implementó otra forma usando Delay Monad, viene con implementación
  en Agda donde sostiene que anda todo... Además una pequeña discusión de como
  hacerlo con tick y todo... Estuve leyendo el paper y es interesante. Usa
  coinducción y otras pavadas de Agda. No tengo puta idea si esto me sirve, está
  levemente relacionado y viene con una implementación.
  [Trabajo](http://www.cse.chalmers.se/~nad/publications/danielsson-definitional-interpreters-complexity.html)

  Este resultó ser bastante interesante. Usa la delay monad (muy relacionado
      con coalgebras.) y logra definir una noción de costos, de hecho habla
  sobre Teoría de Mejoras.

+ [x] Trinagulating Context Lemmas tiene una buena definición de lo que sería
  una propiedad observacional á la Moran&Sands escrita en Agda. El problema de
  este enfoque es que trabaja directamente sobre un lenguaje sencillo sin
  recursión y con programas que siempre terminan... Pero por ahí nos sirve para
  dar un punta pie inicial a toda la implementación... Parece que Danielsson
  implementó algo un poquito más general..V
 

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
Ya tenemos implementado un sistema basado en *Initial Algebra Semantics* en
**Set**, es decir, todos los programas terminan, tenemos una definición concreta
de Contextos (programas con agujeros) y de substitución con capturación de
variables.

Definimos un modelo semántico como:

+ Álgebra de Evaluación
+ Álgebra de Costos
+ Monotonía sobre el álgebra de costos [2: 3.2].

Con esto llegamos a tener un cálculo composicional de costos. David Sands tiene
dos papers hasta este lugar[1, 2]. El enfoque es distinto pero creo que es algo
parecido.

Lo que nunca pude probar es **Improvement Induction**, donde relaciona
términos recursivos con mejoras de programas. *Por qu'e tenemos puesta la vara
ac'a?*

#### Posibles Temas a Desarrollar:

+ 1) Aumentar de **Set** a otra estructura para tener programar que pueden no
  terminar.
+ 2) Tick Algebra. Si dotamos al Lenguaje con una noción de Tick, esto nos habilita
  a trabajar con un poco más de estructura. Recordemos que siempre que
  trabajemos de forma abstracta en realidad no conocemos absolutamente nada
  sobre el lenguaje en el cual trabajamos. Por eso, en general no podemos asumir
  que tengamos términos constantes, o valores, etc. Al agregar el operador de
  Tick, podemos analizar cómo se comportan ciertos contextos y establecer
  propiedades sobre estos. Por ejemplo, podríamos en principio definir contextos
  de Reducción en base a la noción de tick, y además tengo cierta intuición
  sobre que es necesario para mostrar el Teorema de Improvement Induction donde
  la habilidad de **observar** costos es (creo) fundamental.
+ 3) Abstraer el dominio semántico. Acá es estudiar que propiedades tendría que
  tener el dominio semántico que permita tener un análisis de costos. Pj:
  Functor FS, (FS V , FS Nat) donde tenga un preorden/orden parcial y que admita
  una precongruencia sobre los contextos.
+ 4) Calculo de *costos precisos*. Por ejemplo, calcular bien el costo del If, el
  álgebra de costos debería saber cual fue el resultado de verdad de computar la
  condición y sumar el costo correspondiente (no ambos, ni el máximo).
  Acá se plantearon algunas soluciones:

    * Álgebras de Mendler: solucionan la parte semántica (explícitamente) pero
      no los costos (a priori).
    * Aumentar el álgebra de Costos a manipular (Valor, Nat)
    * Utilizar evaluadores mónadicos, donde el costo sea un efecto lateral y por
      ende todo argumento que se utilice se acumula su costo en el efecto.

+ 5) Ejemplo concreto de lenguaje. (Posibles cosas utiles: [5],[4],[1], SKI (de
 Haskell), Hutton's Razor)

    * Mostrar mejoras simples para aumentar confianza en el modelo.
    * Implementar lenguaje recursivo (empujando punto 1).
    * Obtener un *Improvement Induction Theorem** especializado pero correcto
      para generalizar.
      
~~**Propuesta concreta**: Haría un ejemplo concreto para entender mejor como funcionan
las cosas, y así también para tener un prototipo para probar lemas. Después
revisaría la teoría para ver que se puede generalizar a partir de los ejemplos.
Seguramente al implementar un ejemplo, ya que queremos que tenga recursión, lleve
a salirnos de la cat Set. Y posiblemente a implementar costos de forma precisa.

Podemos implementar SKI, [Lambda Calculus](https://plfa.github.io/Lambda/) o algo similar.
Meter PCF sin recursión y luego agregársela seria un ejercicio interesante.~~

(Al final Mauro me dijo que tenía que implementar en Agda, obtener un ejemplo y ver.)

--------------------------------------------------------------------------------
  
#### Preguntas (al aire?):

+ Capaz es mejor pensar todo esto en Semántica de CoAlgebra Final, donde la
 semántica operacional se refleja mejor (??), y el modelo de costos de Sands es
 más directo? Los costos podrían verse en términos de sucesión y convergencia? A
 su vez también se podría reflejar la recursión de forma más sencilla?
+ Sands siempre trabaja con semánticas operacionales, al juntar esto con
  Contextos llega a demostrar que un Contexto evalúa a un (contexto) Valor ó a
  un agujero (sin contar los casos donde no termine o variables libres, etc).
  Esto lo puede hacer porque de alguna manera siempre puede observar el proceso
  de evaluación de las computaciones, porque justamente está en una semántica
  operacional. Nosotros al trabajar con IAS solo podemos trabajar con
  observaciones sobre la evaluación y por ende nunca pude llegar a describir un
  concepto como este. Nota bibliográfica: Sands lo llama Open Uniform
  Computation.
+ **Gas**. Podemos desarrollar la teoría al rededor de una noción de Gas, donde
  computamos hasta donde nos dé, y establecer cierto criterio en base a eso.
  Capaz que esto nos permite trabajar con lenguajes que tengas términos que
  puedan no terminar. Siguiendo esta idea podemos utilizar la noción de *Gas*
  como la cantidad necesaria de ticks para que el programa termine y definir la
  noción de improvement como la noción de menor necesidad de *gas*. Podemos
  pensar como a => b, si \forall C, n \in Nat, runG (C[a]) n termina entonces
  runG (C[b]) n termina. Y un refinamiento de esto sería pedir que (C[b])
  termine con menos gas.

  El problema con Gas es que no está necesariamente conectado a los costos semánticos.
  Es decir, esto deberíamos escribirlo bien, pero creo que acá necesito una mónada para
  establecer bien los costos, capaz incluso relacionarlo con la Delay Monad.

+ Formas de Evaluación. Sands siempre plante'o c'omo se evalúan cada uno de los
  operadores del lenguaje, incluso termina demostrando todo para operadores que
  a lo sumo son estrictos en el primer argumento. Esto le permite asumir c'omo
  la evaluaci'on va progresando, definiendo contextos de reducci'on y mostrando
  ciertos lemas sobre estos. En nuestra teor'ia el problema se hizo relevante
  varias veces, asumo que yo siendo poco inteligente quise hacer mímica de las
  pruebas de Sands. Hay formas de ver o extraer dicha informaci'on del algebra
  de evaluaci'on? Algo que me indique qu'e argumentos son utilizados (ya que en
  principio todos son evaluados).
 
### Notas para tener en cuenta

#### [Q]
+ [ ] Simplificar el lenguaje a operadores entre valores: eff(V_1, ..., V_n).
  De manera similar a tener aplicación entre valores (en vez de términos) para
  tener un control explícito sobre el órden de evaluación.
  
#### Intenté:

+ Hacer andar la teoría en **Coq** y fallé. D'i un montón de vueltas con varios
  papers, pero no terminó andando la cosa bien...
+ Mauro implementó en **Agda** la parte de Semántica de Álgebra inicial y
  estableció la separación semántica. Trabajando sobre eso intenté probar algo
  sobre la relación entre términos, contextos y costos. No llegué a nada (O
  llegué a nada).
  
#### Grandes errores que cometí:

+ Pensar que entendía algo
+ Abstraerme de los términos del lenguaje. Esto termina introduciendo
  abstracciones que pueden no estar representadas por programas en el lenguaje.
  Es decir, la función de abstracción puede no ser sobreyectiva.
+ Dado que pensamos que nuestro dominio semántico son las funciones de (E ->
  (V,N)), me induce a pensar extensionalmente y lamentablemente caí en la
  tentación de pensar que ciertas propiedades eran más regulares de lo que
  realmente son. Acá vendría bien pensar en un lema de Contexto acorde, en este
  caso sería un *Lema de Entornos*.
+ Pensar que los contextos son uniformemente de Reducción **o** Constantes. En
  realidad esto depende del Entorno en el que se esté. Por ejemplo, sean C, R
  contextos constante y de reducción respectivamente, entonces *D = if e then C
  else R* depende claramente del valor que tome *e* que proviene del entorno y
  define si *D* es de reducción o constante.

  
[1] : A Naive Tieme Analysis and its Theory of Cost Equivalence. David Sands,
'95

[2] : Operational Theories of Improvement in Functional Languages. David Sands,
'91

[3] : Towards Operational Semantics of Contexts in functional Languages. David
Sands

[4] : Meta-Theory a la Carte.

[5] : From Algebra to abstract machine: a verified generic construction
