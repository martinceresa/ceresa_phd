{-# Language MultiParamTypeClasses #-}
module SigmaMonad where

class Monad m => SigmaMonad s m where
  ot :: s (m a) -> m a

class Monad m => TickMonad m where
  tick :: m a -> m a
