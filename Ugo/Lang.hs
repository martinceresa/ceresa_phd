{-# LANGUAGE GADTs          #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE RankNTypes     #-}

module Lang where

{-|
Module : Lang

Present the Calculus presented in the paper. Lambda calculus with explicit
(effectful) let binding. Presents two syntactic cats, values and terms.

For some reason (beyond comprehension) I use PHOAS. Obviously is overkill the
approach and I might be shooting myself in the foot, but the original idea was
to implement it in Agda/Coq too.
-}

-- import ProFunctor
----------------------------------------
-- Algebras
type Alg sig a = sig a -> a
----------------------------------------

-- This awful PHOAS definition is caused by my fucking damaged brain. But easier to
-- translate to Coq/Agda
-- Given a Σ (represented here as (* → *)) and a set of variables a (of kind *)
-- we get \Sigma_{\tick}
data TermD ( sig :: * -> * ) (a :: *) where
  Ret :: ValueD sig a -> TermD sig a
  Tick :: TermD sig a -> TermD sig a
  App :: ValueD sig b -> ValueD sig a -> TermD sig a
  To  :: TermD sig a -> (forall b . b -> TermD sig b) -> TermD sig a
  Op  :: sig (TermD sig a) -> TermD sig a

data ValueD sig a where
  Var :: a -> ValueD sig a
  Abs :: (forall b . b -> TermD sig b) -> ValueD sig a

-- High Order Definitions
-- Closed term
type Term sig = (forall a. TermD sig a)
-- Terms with a free variable
type Term1 sig = (forall a. a -> TermD sig a)
-- Closed Value
type Value sig = (forall a. ValueD sig a)
-- Values with a free variable
type Value1 sig = (forall a. a -> ValueD sig a)

----------------------------------------
-- Term application
appTo :: Term sig -> Term sig -> Term sig
appTo m n = To m (\f -> To n (App (Var f) . Var))

app :: Value sig -> Value sig -> Term sig
app = App

ret :: Value sig -> Term sig
ret = Ret

abs :: Term1 sig -> Value sig
abs = Abs

var :: Value1 sig
var = Var
----------------------------------------
----------------------------------------
-- Examples:
-- \x. x
idV :: ValueD sig x
idV = Abs (Ret . Var )

idT :: TermD sig x
idT = Ret idV

-- Omega, (\x.xx)(\x.xx)
omega :: TermD sig a
omega = App t t
  where
    t = Abs (\x -> App (Var x) (Var x))

-- --------------------------------------------------------------------------------
-- -- Rewritten as Profunctor
-- -- But this is different, App takes two Terms now...
-- data TermDF (sig :: * -> *) (a :: *) (b :: *) where
--   -- Std Lambda
--   AppF :: b -> b -> TermDF sig a b
--   AbsF :: (a -> b) -> TermDF sig a b
--   -- Explicit Let/Seq
--   ToF :: b -> (a -> b) -> TermDF sig a b
--   -- Operations
--   OpF :: sig b -> TermDF sig a b
--   -- Tick
--   TickF :: b -> TermDF sig a b

-- type Terms sig = Rec (TermDF sig)

-- app :: Terms sig a b -> Terms sig a b -> Terms sig a b
-- app m n = Roll $ AppF m n
-- abs :: (a -> Terms sig a b) -> Terms sig a b
-- abs = Roll . AbsF
-- to :: Terms sig a b -> (a -> Terms sig a b) -> Terms sig a b
-- to m n = Roll $ ToF m n
-- op :: sig (Terms sig a b) -> Terms sig a b
-- op = Roll . OpF
-- tickOp :: Terms sig a b -> Terms sig a b
-- tickOp = Roll . TickF
-- -- If This path is followed, Smart Cons should be a good idea.
-- var :: b -> Terms sig a b
-- var = Place
-- appTo :: Terms sig a b -> Terms sig a b -> Terms sig a b
-- appTo m n = to m (\f -> to n (\x -> _))
--------------------------------------------------------------------------------
-- Outer ticks does not always work. We need some sort of distributivity between
-- the computational monad and the product with nat.
--------------------------------------------------------------------------------
-- data OTicked m a = T {cost :: Int, comp:: m a}
-- type Iso a b = (a -> b , b -> a)

-- addC :: Int -> OTicked m a -> OTicked m a
-- addC n (T m a) = T (n + m) a

-- instance Functor m => Functor (OTicked m) where
--   fmap f (T ts c) = T ts (fmap f c)
-- instance Applicative m => Applicative (OTicked m) where
--   pure x = T 0 (pure x)
--   (T fn f) <*> (T xn x) = T (fn + xn) (f <*> x)

-- bindIso :: Monad m => (forall a. Iso (m (a , Int)) (OTicked m a)) -> OTicked m x -> (x -> OTicked m y) -> OTicked m y
-- bindIso iso (T xc x)  f = addC xc ( fst iso $ x >>= (snd iso . f) )

-- data Excp x = Either Err x
-- type Err = Either Int Int

-- exIso :: Iso (Excp (x , Int)) (OTicked Excp x)
-- exIso = ( f , g )
--   where
--     f (Left (Left c1)) = _ -- The thing here is that the cost is all over the place!
-- instance Monad m => Monad (OTicked m) where
--   return = pure
--   (T xc x) >>= f = addC xc (x >>= _)
--------------------------------------------------------------------------------

