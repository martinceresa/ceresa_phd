{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE RankNTypes        #-}

module CostEval where

import           IntMonoid
import           Lang
import           SigmaMonad
import           State
import           Writer

--------------------------------------------------------------------------------
-- Introducing the notion of cost through Writer monad transformer?
----------
-- Given a monad M, Ticked M is able to count ticks,
type Ticked = Writer Int

-- Given a monad M, GTicked M is able to keep a global counter of ticks.
type GTicked = State Int


-- defined just as succ
wtick :: Monad m => Ticked m a -> Ticked m a
wtick = censor succ

instance Monad m => TickMonad (Ticked m) where
  tick = censor succ

instance Monad m => TickMonad (GTicked m) where
  tick c = modifyT succ >> c
  --
  -- tick c = modifyT succ >> c,

-- Now we can do the very same evalM but this time with ticks all over the place..
-- [[_]]_c : Term -> m (Value x Nat)
evalC :: ( Functor sig , Monad m )
      => (forall b. Alg sig (Ticked m b) ) -- Algebraic Effects evaluation
      -> TermD sig a -- Given a term M
      -> Ticked m (ValueD sig a) -- [[ M ]] \in m (Value x Nat)
evalC _ (Ret v)     = return v
evalC alg (Tick c)  = wtick $ evalC alg c
evalC alg (App f x) = wtick $ evalC alg ( appVal f x)
evalC alg (To m n)  = evalC alg m >>=  evalC alg . joinVT . n
evalC alg (Op t)    = alg ( fmap ( evalC alg  ) t)

-- Note that we are just ticking everything... as this is just a POC I do not
-- really care to do something intelligent. Everything is should remain obvious.

-- Eventually when we are more obviously correct, we can just take into account
-- App and see what happens

--------------------------------------------------------------------------------
-- Examples
--------------------------------------------------------------------------------
-- Effects ?
-- Empty Functor | No operation allowed
data Empty a
instance Functor Empty

-- With its empty algebra
algEmpty :: Empty a -> a
algEmpty = undefined

-- Maybe!
type MaybeLam = TermD Empty
type MaybeVLam = ValueD Empty

evalCst :: MaybeLam a -> Maybe (MaybeVLam a , Int)
evalCst = unWrt . evalC algEmpty
----------------------------------------
-- We can introduce an explicit divergent computation
----------------------------------------
-- Introduce _|_ : () -> TMB X
type MExplicit = TermD Maybe
type MVExplicit = ValueD Maybe

malg :: Alg Maybe (Ticked Maybe b)
malg Nothing  = Wrt Nothing
malg (Just x) = x

explicitEvalCst :: MExplicit a -> Maybe (MVExplicit a, Int)
explicitEvalCst = unWrt . evalC malg

-- return (\_ -> _|_ )
exampleExplicit :: MExplicit a
exampleExplicit = Ret $ Abs (const $ Op Nothing)

----------------------------------------
-- Delayed Monad
-- As another Partiality Example
data Delayed a where
  Now :: a -> Delayed a
  Later :: Delayed a -> Delayed a

instance Functor Delayed where
  fmap f (Now x)   = Now (f x)
  fmap f (Later x) = Later (fmap f x)

instance Applicative Delayed where
  pure x = Now x
  (Now f) <*> xs = fmap f xs
  (Later lf) <*> xs = Later $ lf <*> xs

instance Monad Delayed where
  return = pure
  (Now x) >>= f = f x
  (Later x) >>= f = Later $ x >>= f

-- Delay monad is commutative.
-- do x <- fx; y <- fy; comp x y
-- \equiv
-- do y <- fy ; x <- fx; comp x y
-- And it seems to be idempotent too.
-- Again I do not see clearly the operation which produce effects.

-- _|_
never :: Delayed a
never = Later never

malg' :: Alg Maybe (Ticked Delayed b)
malg' Nothing = Wrt $ never
malg' (Just x) = x

delayedEvalCst :: MExplicit a -> Delayed (MVExplicit a, Int)
delayedEvalCst = unWrt . evalC malg'
