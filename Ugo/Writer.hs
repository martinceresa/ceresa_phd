module Writer where

-- newtype Writer w a = Write {print :: (a , w)}

-- instance Functor (Writer w) where
--   fmap f (Write (a , w)) = Write (f a , w)

-- instance (Monoid w) => Applicative (Writer w) where
--   pure x = Write (x , mempty)
--   (Write (f, w)) <*> (Write (x , w')) = Write (f x, mappend w w')

-- kliArr :: (Monoid w) => (a -> Writer w b) -> Writer w a -> Writer w b
-- kliArr f (Write (a , w)) = let (Write (b , w')) = f a in Write (b , mappend w w')

-- instance (Monoid w) => Monad (Writer w) where
--   return = pure
--   a >>= f = kliArr f a
newtype Writer w m a = Wrt {unWrt :: m (a , w)}

instance Functor m => Functor (Writer w m) where
  fmap f (Wrt m) = Wrt $ fmap (\(a,w) -> (f a , w)) m

idR :: (Applicative m, Monoid w) => m (a -> b, w) -> m ( (a , w) -> (b , w) )
idR f = (\(g,w)(a,w') -> (g a, w `mappend` w') ) <$> f

instance (Applicative m, Monoid w) => Applicative (Writer w m) where
  pure x = Wrt (pure (x , mempty))
  (Wrt f) <*> (Wrt x) = Wrt $ idR f <*> x

instance (Monad m, Monoid w) => Monad (Writer w m) where
  return = pure
  (Wrt ma) >>= f = Wrt $ ma >>= \(a,w) -> (\(b , w') -> (b , mappend w w')) <$> unWrt (f a)

censor :: Monad m => (w -> w) -> Writer w m a -> Writer w m a
censor f = Wrt . fmap (\(a, w) -> (a , f w)) . unWrt

mp :: (Monad m, Monoid w) => w -> Writer w m a -> Writer w m a
mp = censor . mappend

