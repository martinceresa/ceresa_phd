module CPO where

import Control.Arrow
-- import Prelude hiding (iterate)

-- | Domain (D, ⊑ , ⊥)
class Dom a where
  bot :: a -- ⊥
  lub :: [a] -> a -- Shouldn't be streams instead of list?

-- Classic lifting
instance Dom a => Dom (b -> a) where
  bot = const bot
  lub fs b = lub (map ($ b) fs)

-- Joint product of Domains
instance (Dom a, Dom b) => Dom (a,b) where
  bot = (bot,bot)
  lub = (lub *** lub) . unzip

-- iterate :: (a -> a) -> a -> [a]
-- iterate f a = a : iterate f (f a)

-- Kleene's characterization of recursive functions, given (f : a -> a) mono
lfp :: Dom a => (a -> a) -> a
lfp = lub . flip iterate bot
