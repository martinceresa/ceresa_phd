{-# Language TupleSections #-}
{-# Language RankNTypes #-}
module Output where

{-|
Module : Output

The idea here is to start writing examples with effects.
Signature here is a function 'print_msg : a -> a' such that
prints message 'msg' somewhere on (hopefully) earth.

The idea is to give an interpretation to print_msg through the writer monad with
identity. -}

import Lang
import SigmaMonad

import Prelude hiding (abs)

----------------------------------------
-- | Signature
newtype Print a = Print {unP :: (a , String) }

instance Functor Print where
  fmap f (Print (x, os)) = Print (f x, os)
----------------------------------------
----------------------------------------
-- | Writers Monad |OutputM a| just carries around a value and the accumulated
-- /output channel/.
newtype OutputM a = O { rO :: (a , [String])}

instance Functor OutputM where
  fmap f (O (x, os)) = O (f x, os)

instance Applicative OutputM where
  pure = O . (,[])
  (O (f, os)) <*> (O(x,os')) = O (f x, os' ++ os) -- it might be wrong

instance Monad OutputM where
  return = pure
  (O (x, os)) >>= f = let O (x', os') = f x
                      in O (x', os' ++ os)

instance TickMonad OutputM where
  tick = undefined

runOutput :: OutputM a -> (a , [String])
runOutput (O (r, os)) = (r, os)

getOutput :: OutputM a -> [String]
getOutput = snd . runOutput
----------------------------------------
----------------------------------------
-- | Interpretation of /print_msg/
algOutput :: Print (OutputM a) -> OutputM a
algOutput (Print (O (a, o) , sr)) = O (a, sr : o)

-- | Helpers
-- Explicit type carrying primitive
printer' :: String -> TermD Print a -> TermD Print a
printer' s t = Op (Print (t, s))

-- Existencially quantified Term
printer :: String -> Term Print -> Term Print
printer = printer'

----------------------------------------
----------------------------------------
-- Examples?
helloWorld :: Term Print
helloWorld = printer "Hello World!"
            $ printer "Hi man"
            $ printer "What's up?"
  -- Greetings, done
            $ App
            (Abs (\b -> printer' "Application" (Ret $ Var b)))
            (Abs (Ret . Var))
