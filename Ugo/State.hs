{-# Language TupleSections #-}
module State where

import Control.Monad

newtype State s m a = St {runSt :: s -> m (a , s)}

first :: (a -> c) -> (a , b) -> (c, b)
first f (x , b) = (f x, b)
second :: (b -> c) -> (a , b) -> (a , c)
second f (a, x) = (a , f x)

instance Functor m => Functor (State s m) where
  fmap f (St c) = St $ fmap (first f) . c

instance Monad m => Applicative (State s m) where
  pure = return
  (<*>) = ap

instance Monad m => Monad (State s m) where
  return x = St $ \s -> return (x , s)
  (St x) >>= f = St $ \s ->
      x s >>= \(x', s') -> runSt (f x') s'

modifyT :: Monad m => (s -> s) -> State s m ()
modifyT f = St $ return . second f . ((),)
