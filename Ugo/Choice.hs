{-# LANGUAGE RankNTypes #-}
module Choice where

{-|
Module : Choice

The idea here is to start writing examples with effects.

Signature here is a function 'choice : a x a -> a' such that
choice one of its arguments with the same probability.

The idea is to give an interpretation to |choice| using a non-deterministic
monad. So |choice^L : [a] x [a] -> [a]|
-}

import           Lang
import           SigmaMonad

-- POC
import           Evaluation

import           Prelude    hiding (abs)

----------------------------------------
-- | Signature
newtype Choice a = Choice {unC :: (a, a)}
-- Choice is Two

instance Functor Choice where
  fmap f (Choice (x,y)) = Choice (f x, f y)
----------------------------------------
----------------------------------------
{-|
To model non determinism in Haskell the standard way is to simulate it through
the List Monad!

I guess I should explain here what/how list monad works, but I am going to skip
it.
-}

-- Maybe next time
instance TickMonad [] where
  tick = undefined

-- Concrete Evaluation function to simply test evaluation.
compute :: TermD Choice a -> [ValueD Choice a]
compute = evalM algChoice
----------------------------------------
-- | /choice/ interpretation
-- To set of possibilities arise, so we simply concat them out.
algChoice :: Choice [a] -> [a]
algChoice (Choice (xs,ys))
  -- My guess here is that a Union should happen
  = xs ++ ys

-- | Helpers
-- Explicit type carrying
choice' :: TermD Choice a -> TermD Choice a -> TermD Choice a
choice' m n = Op (Choice (m , n))

-- Existencially quantified Term
choice :: Term Choice -> Term Choice -> Term Choice
choice m n = Op (Choice (m , n))

----------------------------------------
--  Optimistic Rnd Sampling. Modulo my brain incapacity.

-- First part of optimistic Sampling.
-- do
--   a <- m (+) n
--   y <- f(a)
--   g(y)
optSamplingW
  :: Value Choice -- f, accepting an inverse
  -> Value Choice -- g normal function
  -> Term Choice -> Term Choice -- M, N \in Terms
  -> Term Choice
optSamplingW f g m n =
  appTo (choice m n)
  $ ret $ abs
  $ \x -> To (App f (Var x)) -- Type preserving primitives
  $ App g . Var

-- Second part of optimistic Sampling.
-- do
--   a <- m (+) n
--   g(a)
optSamplingB
  :: Value Choice -- g normal function
  -> Term Choice -> Term Choice -- M, N \in Terms
  -> Term Choice
optSamplingB g m n =
  appTo (choice m n)
  $ ret $ abs
  $ App g . Var -- Type Preserving Primitives.

-- The whole point here is to prove that optSamplingW >= optSamplingB
----------------------------------------
-- This part is going to be more hacky than the rest.
-- Idea: __QuickCheck this mother fucker!__
-- The idea is as follows, the relation here is 'modulo f'.
-- optSamplingW generates a distribution of possible values/executions, same as optSamplingB.
-- The whole idea is that such distribution are the same modulo f.
-- optSamplingW = [f x1, f x2, ...]
-- optSamplingB = [x1 , x2 , ...]
-- Such that probabilities remains the same,
-- prob(xi,SB) = prob(f xi, SW) and prob(yi, SW) = prob (f^{-1}(yi), SB)
