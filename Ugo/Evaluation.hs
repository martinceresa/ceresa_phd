{-# LANGUAGE RankNTypes #-}
module Evaluation where

import           Lang
import           SigmaMonad

----------------------------------------
-- Helpers to define substitution. JoinV gives ValueD monadic behavior.
joinV :: Functor sig => ValueD sig (ValueD sig a) -> ValueD sig a
joinV (Var t) = t
joinV (Abs c) = Abs ( joinVT . c . Var)

-- This is a mix function between our two realms,
joinVT :: Functor sig => TermD sig (ValueD sig a) -> TermD sig a
joinVT (Ret v)   = Ret $ joinV v
joinVT (Tick t)  = Tick $ joinVT t
joinVT (App f x) = App  f --( joinV _ )
                   (joinV x)
joinVT (To m n)  = To (joinVT m )
                   (joinVT . n . Var )
joinVT (Op t)    = Op (fmap joinVT t)


-- We only have (need) Value substitution, that is
-- Forall M \in Term1, M[x := V]
termSubs :: Functor sig => Term1 sig -> Value sig-> Term sig
termSubs t v = joinVT (t v)

-- Forall V \in Value_1, V[W/x]
valueSubs :: Functor sig => Value1 sig -> Value sig -> Value sig
valueSubs v w = joinV (v w)

-- We are working with closed expressions
appVal :: Functor sig => ValueD sig b -> ValueD sig a -> TermD sig a
appVal (Var _) _ = error "Open case?"
appVal (Abs c) x = joinVT (c x)

--------------------------------------------------------------------------------
-- Monadic evaluation. [[-]] : Term -> m (Value)
----------
evalM :: ( Functor sig , TickMonad m ) => (forall b. Alg sig (m b) ) -> TermD sig a
      -> m (ValueD sig a)
evalM _ (Ret v)     = return v
evalM alg (Tick t)  = tick $ evalM alg t
evalM alg (App f x) = evalM alg (appVal f x)
evalM alg (To m n)  = evalM alg m >>= evalM alg . joinVT . n
evalM alg (Op t)    = alg ( fmap ( evalM alg  ) t)
