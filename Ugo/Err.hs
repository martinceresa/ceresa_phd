module Err where

import           Lang
import           Writer
import           State

-- Error Handler?
data T = T
-- type Comp x = Maybe (Either T x)
newtype Comp x = Comp{unComp :: Maybe (Either T x)}

instance Functor Comp where
  fmap f (Comp x) = Comp $ fmap (fmap f) x

instance Applicative Comp where
  pure = Comp . Just . Right
  (Comp (Just (Right f))) <*> (Comp(Just(Right x))) = pure (f x)
  (Comp (Just (Left T))) <*> _ = Comp $ Just $ Left T
  _ <*> (Comp (Just (Left T))) = Comp $ Just $ Left T
  _ <*> _ = Comp Nothing

instance Monad Comp where
  return = pure
  (Comp (Just (Right x))) >>= f = f x
  (Comp (Just (Left T))) >>= _ = Comp $ Just $ Left T
  (Comp Nothing) >>= _ = Comp Nothing


raise :: Comp x
raise = Comp $ Just $ Left T

catch :: (Comp x , Comp x) -> Comp x
catch (Comp (Just (Left _)), a)       = a
catch (r@( Comp (Just (Right _))), _) = r
catch (Comp Nothing , _)              = Comp Nothing

data ErrFun x =  R | C x x

instance Functor ErrFun where
  fmap _ R     = R
  fmap f (C x y) = C (f x) (f y)

alg :: ErrFun (Comp x) -> Comp x
alg R       = raise
alg (C c d) = catch(c,d)

algC :: ErrFun (Ticked Comp x) -> Ticked Comp x
algC R = Wrt raise
algC (C c d) = Wrt $ catch (unWrt c, unWrt d)

  -- Since Catch is not algebraic I shouldn't be able to write it using just monads ops.
algSt :: ErrFun (GTicked Comp x) -> GTicked Comp x
algSt R = St $ \_ -> raise -- Check this one out
algSt (C c d) = St $ \s ->  catch ((runSt c s), runSt d s)

-- Term -> (Value + Errors) + Bottom
-- Only flawless computations have observable costs... But it seems reasonable
-- since we are excluding cost evaluation when we say there are some special
-- values (distinguishable errors). Using WriterT cost is available only when we
-- compare Successful Value Computations?
  -- Lan
-- evaluation :: TermD ErrFun a -> Ticked Comp (ValueD ErrFun a)
-- evaluation = evalM alg

evaluation :: TermD ErrFun a -> Ticked Comp (ValueD ErrFun a)
evaluation = evalM algC

gevaluation :: TermD ErrFun a -> GTicked Comp (ValueD ErrFun a)
gevaluation = evalM algSt

-- cevaluation :: TermD ErrFun  a -> Comp (ValueD ErrFun a)
