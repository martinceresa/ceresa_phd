Require Import Effects.
Import Effect.
Require Import Terms.
Import Term.

Module SStep.

(* There is something missing... Do I need more polymorphism in Ocs?
   It works, but I think it must be a better way to do this shit.
 *)
Fixpoint getOcs var (eff : Effect) (op : Operation eff) (e : Expr) (k : Expr) (os : ocs (expr var)) : comp var :=
  match os with
    (* Empty / Not Found, just keep it going up. Unhandled effect *)
  | Dempty' _ _ => (callop eff op e (fun V y => app' V (k V) (var' V y))) var
  | Dcons' _ eff' op' c res =>
    if (Operation_Dec eff eff' op op')
    then (flattenC _ (c (e _) (k _) )) (* inlined simultaneous substitution*)
    else getOcs var eff op e k res
  end.

Reserved Notation "t1 '-->' t2" (at level 40).

Inductive step {ESig : Signature } : Comp -> Comp -> Type :=
| ST_IfTT : forall (c1 c2 : Comp),
      ( ifThenElse tru c1 c2 )
        -->
      c1
| ST_IfFF : forall (c1 c2 : Comp ),
    ifThenElse fls c1 c2 --> c2
| ST_MatchZ : forall (c1 : Comp ) (c2 : Comp1),
    natMatch zero c1 c2 --> c1
| ST_MatchS : forall (e : Expr) (c1 : Comp ) (c2 : Comp1),
    natMatch (succ e) c1 c2 -->  c2 [ e ]c
| ST_App : forall T c e,
    app (abs T c) e --> SubstComp e c
| ST_LetCS : forall c1 c1' c2,
      c1  --> c1'
    -> (*--------------*)
      letC c1 c2 --> letC c1' c2
| ST_LetCV : forall (e : Expr) (c : Comp1 ),
    letC (val e) c --> SubstComp e c
| ST_LetCOp : forall (eff : Effect ) (i : Inst eff ) (op : OpSym eff) (e : Expr) (c1 c2 : Comp1) ,
    letC (cop eff (insta eff i) op e c1) c2
        (* let x = (i#op e (y. c1)) in c2 *)
     --> (**************************)
        (* i#op e (y . let x = c1 in c2) *)
    cop eff (insta eff i) op e (fun Y y => letC' Y (c1 Y y) (c2 Y))
        (* A little polymorphism plumbing was required *)
| ST_LetRec : forall T D c1 c2,
              letRec T D c1 c2 -->
               c2 [ abs T (fun V v => letRec' _ T D (c1 V) (c1 V v) ) ]c
               (* Not so sure about this one... I still do not completely
                understand the scope of this recursive definitions... Because here
                I will be shadowing x binding.*)
| ST_WithHandleSeq : forall e c c',
                 c --> c'
      -> (*--------------------*)
      withHandle e c --> withHandle e c'
| ST_WithHandleVal : forall T c res e, withHandle (hndlr T c res) (val e) --> c [ e ]c
| ST_WithHandleOcs : forall A B T eff e i op cv c res,
    let h := (hndlr T cv res) in
     Sig eff ESig op A B (* Getting types from Context *)
     -> (*************************************)
     withHandle h (cop eff (insta eff i) op e c)
       -->
       fun V =>
         getOcs V eff ( i , op) e (abs B (fun Y y => withHandle' Y (h Y)
                                                                 (c Y y) (* *)
                                  )) (res (expr V))
where "t1 '-->' t2" := (step t1 t2).

Reserved Notation "t1 '-->*' t2" (at level 40).

Inductive ClauStep {ESig : Signature } : Comp -> Comp -> Type :=
| STRefl : forall c, c -->* c
| STTrans : forall c c' c'', @step ESig c c' -> c' -->* c'' -> c -->* c''
where "t1 '-->*' t2" := (ClauStep t1 t2).

Inductive ClauNSteps {ESig : Signature} : nat -> Comp -> Comp -> Type :=
| CNRefl : forall (c : Comp), ClauNSteps 0 c c
| CNSucc : forall (n : nat) (c c' c'' : Comp), (@step ESig c c') -> ClauNSteps n c' c'' -> ClauNSteps (S n) c c''.

Lemma Refinament : forall {ESig : Signature} (c c' : Comp) (n : nat),
    @ClauNSteps ESig n c c' -> @ClauStep ESig c c'.
Proof.
  intros.
  induction X.
  + constructor.
  + econstructor.
    apply s.
    apply IHX.
Qed.

(* Restriction to just values *)
Inductive BigNStep {ESig : Signature} : Comp -> nat -> Comp -> Prop :=
| JVal : forall (c : Comp) (n : nat) (e : Expr),
    @ClauNSteps ESig n c (val e) -> BigNStep c n (val e)
| JOp : forall (c : Comp) (n : nat) (eff : Effect) (op : Operation eff) (e : Expr) (c' : Comp1), @ClauNSteps ESig n c (callop eff op e c') -> BigNStep c n (callop eff op e c').

Definition BigStep {ESig : Signature} : Comp -> Comp -> Prop :=
  fun c c' =>
  exists (n : nat) , @BigNStep ESig c n c'.

Definition Approx {ESig : Signature} : Comp -> Comp -> Prop :=
  fun c c' => exists (n  : nat) ,  @BigNStep ESig c n c'.

Definition Improv {ESig: Signature} (c c' : Comp) : Type :=
  forall (n : nat) (v : Comp), @BigNStep ESig c n v -> exists (m : nat)(v' : Comp),
      (m <= n) ->
     (@BigNStep ESig c' m v').

End SStep.
