Require Import SmallStep.
Import SStep.
Require Import Effects.
Import Effect.
Require Import Terms.
Import Term.

(* Reference example ? *)
Module ReferenceExample.
  Import Lists.List.ListNotations.
  Open Scope list_scope.

  (* Just one kind of effects *)
  Example refEff : Effect := 0.
  (* Operations in that effect*)
  Example refSig : Signature :=
    [ (* Just one efect, refEff  *)
      [     (Unit , Nat) (* lookup : Unit -> Nat *)
          ; (Nat , Unit) (* update : Nat -> Unit *)
      ]
    ].
  Example lookup : OpSym refEff := osym refEff 0 .
  Example update : OpSym refEff := osym refEff 1 .
  Example i : Inst refEff := ins refEff 0.

  Example iEff : Expr := insta refEff i.

  Example lookUp : Expr := (genericEff refEff iEff lookup Unit).
  Example upDate : Expr := (genericEff refEff (insta refEff i) update Unit).

  Example hEffect : Expr :=
    hndlr Unit
          (* val x -> i#update x*)
          (fun V x => app' V (upDate V) (var' V x))
          (
            (* i#lookup x k -> k 1 *)
            Dcons refEff ( i , lookup) (fun V x k => app' V (var' V k) (succ zero V))
            (* i#update x k -> k () *)
            (Dcons refEff ( i , update) (fun V x k => app' V (var' V k) (unit V))
                         (Dempty ( Dty refEff Unit [])))
          ) .
  Example c : Comp :=
    (* let x1 = lookUp () *)
    letC (app lookUp unit)
    (* in *)
         (fun V x1 =>
            (* let *)
            letC' V
                  (* x2 = update x1*)
                  (app' V (upDate V) (var' V x1))
                  (* in *)
                  (fun x2 =>
                     (* x1 + 1 *)
                     val' V (succ' V (var' V x1)))) .
  Lemma Deriv : @ClauStep refSig (withHandle hEffect c) (app upDate (succ (succ zero))).
  Proof.
    eapply STTrans.
    {
      apply ST_WithHandleSeq. apply ST_LetCS. apply ST_App.
    }

    eapply STTrans.
    {
      apply ST_WithHandleSeq. apply ST_LetCOp.
    }
    simpl.
    eapply STTrans.
    {
      eapply ST_WithHandleOcs. apply Sig_here. apply Eff_here.
    }
    simpl.
    destruct Operation_Dec.
    2: inversion o; contradiction. (* Contradiction case: lookup is not update *)
    (* So, we have triggered an effect: lookup *)
    compute. (* Just to see the result *)
    eapply STTrans.
      {
        apply ST_App...
      }
    eapply STTrans.
      {
        apply ST_WithHandleSeq. apply ST_LetCV.
      }
    simpl.
    eapply STTrans.
      {
        apply ST_WithHandleSeq. apply ST_LetCS. apply ST_App.
      }
    eapply STTrans.
      {
        apply ST_WithHandleSeq. apply ST_LetCOp.
      }
    simpl.
    eapply STTrans.
      { (* So, now we have to look the effect up in the context...*)
        eapply ST_WithHandleOcs. apply Sig_here. apply Eff_there. apply Eff_here.
      }
    simpl.
    destruct Operation_Dec.
    { (* Another contradiction *)
      inversion o0. discriminate H5.
    }

    destruct Operation_Dec.
    2: {
      inversion o1; contradiction.
    }
    eapply STTrans.
    {
      apply ST_App.
    }
    simpl.

    eapply STTrans.
    {
      apply ST_WithHandleSeq. apply ST_LetCV.
    }

    eapply STTrans.
    {
      apply ST_WithHandleVal.
    }

    simpl.
    apply STRefl.
  Qed.
End ReferenceExample.
