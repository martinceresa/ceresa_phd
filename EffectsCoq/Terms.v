Require Import Effects.
From Coq Require Import Strings.String.
Module Term.

Import Effect.

(* I am not using any fancy thing to encode anything... Since I still
do not know what I am doing I'll just do (as always) mediocre  things.
 Well... PHOAS is really easy, so lets give it a try.
Language definition: *)

Section terms.
  Variable varSet : Set.

  Inductive expr : Type :=
    | tru' : expr
    | fls' : expr
    | zero' : expr
    | succ' : expr -> expr
    | unit' : expr
    | abs'  : ty -> (varSet -> comp) -> expr
    | var' : varSet -> expr
    | insta' : forall eff, Inst eff -> expr
    | hndlr' : ty (* A *)
              -> (varSet -> comp) (* handler val x : A -> cv *)
              -> ocs (* | ocs *)
              -> expr
  with
  ocs : Type :=
  | Dempty' : dty -> ocs
  | Dcons' : forall (eff : Effect),
      Operation eff (* i#op *)
      -> (varSet (* x *)
          -> varSet (* k *)
          -> comp) (* x k -> c*)
      -> ocs (* rest *)
      -> ocs
  with
  comp : Type :=
    | val' : expr -> comp
    | cop' : forall (eff : Effect),
        expr                  (* e1     *)
        -> OpSym eff          (* #op    *)
        -> expr               (* e2     *)
        -> (varSet -> comp)   (* (y. c) *)
        -> comp
    | withHandle' : expr -> comp -> comp
    | ifThenElse' : expr -> comp -> comp -> comp
    | absurdC' : dty -> expr -> comp
    | app' : expr -> expr -> comp
    | natMatch' : expr                  (* match e with *)
                 -> comp               (* | zero -> c1 *)
                 -> ( varSet -> comp ) (* | suc x -> c2 *)
                 -> comp
    | letC' : comp (* let x = c1 *)
             -> (varSet -> comp) (* in c2*)
             -> comp
    | letRec' : ty (* A *)
               -> dty (* C *)
               -> (varSet -> varSet -> comp) (* let rec f x : A -> C = c1 *)
               -> (varSet -> comp ) (* in c2 *)
               -> comp.
End terms.

Definition Expr := forall var, expr var.
Definition Expr1 := forall ( var : Set), var -> expr var.
Definition Comp := forall (var : Set) , comp var.
Definition Comp1 := forall (var : Set) , var -> comp var.
Definition Comp2 := forall (var : Set) , var -> var -> comp var.

Lemma extComp' : forall (c c' : Comp) var , c = c' -> c var = c' var.
Proof.
  intros.
  rewrite H.
  auto.
Qed.

Axiom extComp : forall (c c' : Comp) var, c var = c' var -> c  = c'.


Definition Ocs := forall (var : Set), ocs var.
Definition Dempty : dty -> Ocs := fun dt _ => Dempty' _ dt.
Definition Dcons : forall (eff : Effect), Operation eff -> Comp2 -> Ocs -> Ocs :=
  fun eff ops c res V => Dcons' V eff ops (c V) (res V).

(* Remove explicit polymorphism *)
Definition tru : Expr := fun _ => tru' _ .
Definition fls : Expr := fun _ => fls' _ .
Definition zero : Expr := fun _ => zero' _ .
Definition succ (e : Expr) : Expr := fun _ => succ' _ (e _) .
Definition unit : Expr := fun _ => unit' _ .

Definition abs (t : ty) (bd : Comp1 ) : Expr :=  fun _ => abs' _ t ( bd _ ).

Definition insta : forall (eff : Effect), Inst eff -> Expr := fun eff i _ => insta' _ eff i .
Definition hndlr (typ : ty) ( cv : Comp1 ) (os : Ocs) : Expr :=
  fun _ => hndlr' _ typ (cv _) (os _).

Definition val (e : Expr) : Comp := fun _ => val' _ (e _).
Definition cop (eff : Effect) (e1 : Expr) (op : OpSym eff) (e2 : Expr) (c : Comp1) : Comp :=
  fun _ => cop' _ eff (e1 _ ) op (e2 _) (c _).

Definition callop (eff : Effect) (op : Operation eff) (e : Expr) (c : Comp1) : Comp :=
  cop eff (insta eff (fst op)) (snd op) e c.

Definition genericEff (eff : Effect) (e : Expr) (op : OpSym eff) : forall T, Expr :=
  fun T =>
  abs T (fun V x => cop' V eff (e V) op (var' _ x) (fun y => val' _ (var' _ y))).

Definition genEff (eff : Effect) (e : Expr) (op : OpSym eff) : forall (T : ty) , Comp1 :=
  fun T V x => app' V (genericEff eff e op T V) (var' V x) .

Definition withHandle (e : Expr) (c : Comp) : Comp :=
  fun _ => withHandle' _ (e _ ) (c _).

Lemma withHandleInj : forall {var : Set} (e : Expr ) (c c' : Comp) ,
    withHandle e c = withHandle e c' -> c = c'.
Proof.
  intros var e c c' H.
  eapply extComp' in H.
  inversion H.
  apply (extComp c c' var).
  apply H1.
Qed.

Definition ifThenElse (e : Expr) (tb fb : Comp) : Comp :=
  fun _ => ifThenElse' _ (e _ ) (tb _) (fb _ ).
Definition absurdC (dtyp : dty) (e : Expr) : Comp :=
  fun _ => absurdC' _ dtyp (e _).
Definition app (e1 e2 : Expr) : Comp :=
  fun _ => app' _ (e1 _ ) (e2 _).
Definition natMatch (e : Expr) (czero : Comp) (csucc : Comp1) : Comp :=
  fun _ => natMatch' _ (e _) (czero _) (csucc _) .
Definition letC (c1 : Comp) (c2 : Comp1) : Comp :=
  fun _ => letC' _ (c1 _) (c2 _).

Lemma letCInj1 : forall {var : Set} (c c' : Comp) (q : Comp1) , letC c q = letC c' q -> c = c'.
Proof.
  intros.
  eapply extComp' in H.
  inversion H.
  apply ( extComp c c' var ).
  apply H1.
Qed.

Definition letRec (typ : ty) (dtyp : dty) (c1 : Comp2) (c2 : Comp1) : Comp :=
  fun _ => letRec' _ typ dtyp (c1 _ ) (c2 _).

Section Flatten.
  Variable varSet : Set.

  Fixpoint flattenE (e : expr (expr varSet)) : expr varSet :=
  match e with
  | tru' _ => tru' _
  | fls' _  => fls' _
  | zero' _ => zero' _
  | succ' _ x => succ' _ (flattenE x)
  | unit' _ => unit' _
  | abs' _ t f => abs' varSet t (fun x => flattenC (f (var' _ x)) )
  | var' _ x => x
  | insta' _ eff i => insta' _ eff i
  | hndlr' _ t cv os => hndlr' _ t (fun x => flattenC (cv (var' _ x))) (flattenOcs os)
  end
  with flattenOcs (os : ocs (expr varSet)) : ocs varSet :=
         match os with
         | Dempty' _ x => Dempty' _ x
         | Dcons' _ eff ops com res => Dcons' _ eff ops (fun x k => flattenC (com (var' _ x) (var' _ k))) (flattenOcs res)
end
  with flattenC (c : comp (expr varSet)) : comp _ :=
         match c with
         | val' _ x => val' _ (flattenE x)
         | cop' _ eff x x0 x1 x2 => cop' _ eff (flattenE x) x0 (flattenE x1) (fun x => flattenC (x2 (var' _ x)))
         | withHandle' _ x x0 => withHandle' _ (flattenE x) (flattenC x0)
         | ifThenElse' _ x x0 x1 => ifThenElse' _ (flattenE x) (flattenC x0)(flattenC x1)
         | absurdC' _ x x0 => absurdC' _ x (flattenE x0)
         | app' _ x x0 => app' _ (flattenE x) (flattenE x0)
         | natMatch' _ x x0 x1 => natMatch' _ (flattenE x) (flattenC x0) (fun x => flattenC (x1 (var' _ x)))
         | letC' _ x x0 => letC' _  (flattenC x) (fun y => flattenC (x0 (var' _ y)))
         | letRec' _ x x0 x1 x2 => letRec' _ x x0 (fun f y => flattenC (x1 (var' _ f) (var' _ y)))
                                        (fun y => flattenC (x2 (var' _ y)))
         end.

  Check flattenE.
  Check flattenC.
End Flatten.

Reserved Notation "e '[' x ']e'" (at level 20).
Reserved Notation "c '[' x ']c'" (at level 20).

Definition Subst (E1 : Expr ) (E2 : Expr1 ) : Expr := fun _ =>
  flattenE _ (E2 _ (E1 _)).
Notation "e '[' x ']e'" := (Subst x e).

Definition SubstComp (E : Expr) (C : Comp1) : Comp :=
  fun _ => flattenC _ (C _ (E _)).
Notation "c '[' x ']c'" := (SubstComp x c).

(* Sequential substitution...*)
Definition SubstComp2 (E : Expr) (C : Comp2) : Comp1 :=
  fun _ v => flattenC _ (C _ (E _) (var' _ v)).

Definition SimultaneosSubst (E1 E2 : Expr) (C : Comp2) : Comp :=
  fun _ => flattenC _ ( C _ (E1 _) (E2 _)).

(****************************************)
(* Lets see some examples*)
(** idB = \x:Bool . x **)
Example idBool : Expr := abs Bool (fun x y => val' x (var' x y) ) .
Example idBool' : Expr := fun _ => abs' _ Bool (fun x => val' _ (var' _ x)).

(****************************************)

(* Reserved Notation "'[' x ':=c' s ']' t" (at level 20). *)
(* Reserved Notation "'[' x ':=e' s ']' t" (at level 20). *)

(* Open Scope string_scope. *)

(* Fixpoint substExpr (x : string) (s : expr) (t : expr) : expr := *)
(*   match t with *)
(*     | tru => tru *)
(*     | fls => fls *)
(*     | zero => zero *)
(*     | succ t' => succ (substExpr x s t') *)
(*     | unit => unit *)
(*     | var x' => if eqb_string x x' then s else var x' *)
(*     | abs x' T1 c => abs x' T1 (if eqb_string x x' then c else (substComp x s c)) *)
(*     | insta i => insta i (* TODO *) *)
(*     | hndlr h => hndlr h (* TODO *) *)
(*     end *)
(* with substComp (x : string) (s : expr) (c : comp) : comp := *)
(*        match c with *)
(*        | val e => val (substExpr x s e) *)
(*        | cop e1 op e2 x' c => cop (substExpr x s e1) op (substExpr x s e2) *)
(*                                  x' (if eqb_string x x' then c else substComp x s c) *)
(*        | withHandle e c => withHandle (substExpr x s e) (substComp x s c) *)
(*        | ifThenElse e tb fb => ifThenElse (substExpr x s e) (substComp x s tb) (substComp x s fb) *)
(*        | absurdC e => absurdC (substExpr x s e) *)
(*        | app e1 e2 => app (substExpr x s e1) (substExpr x s e2) *)
(*        | natMatch e czero x' csuc => natMatch e czero x' (if eqb_string x x' then csuc else (substComp x s csuc)) *)
(*        | letC x' c1 c2 => letC x' (substComp x s c1) (if eqb_string x x' then c2 else substComp x s c2) *)
(*        | letRec f x' T1 T2 c1 c2 => letRec f x' T1 T2 c1 c2 (* TODO *) *)
(*                       end. *)

(* Notation "'[' x ':=e' e ']' c" := (substExpr x e c). *)
(* Notation "'[' x ':=c' e ']' c" := (substComp x e c). *)

(* (* This is a pretty blunt substitution, since we are going to define an step relation on *)
(*  closed terms it should be enough. *) *)

End Term.
