Require Import Effects.
Import Effect.
Require Import Terms.
Import Term.

Require Import SmallStep.
Import SStep.

Module Reductions.

(*
Well here the idea is to give a reduction context operational semantic. It is
pretty much (and I want to prove that) the same as define in the other file
[SmallStep.v]
 *)

  (* Since we do not give a fuck about types... *)

Inductive ReductionCtx : Type :=
  | RTop : ReductionCtx (* [ ] *)
  | RLet : ReductionCtx -> Comp1 -> ReductionCtx (* let x = Red in c *)
  | RHnd : Expr -> ReductionCtx -> ReductionCtx (* with e handle Red *)
.

(*
  CC := [-] | val EC | EC # EC (y . CC) | with EC handle CC
  | if EC then CC else CC | EC EC | (match EC with 0 -> CC | succ x -> CC)
  | let x = CC in CC
  EC := x | tt | ff | 0 | succ EC | () | abs x CC | i
      | (handler val x -> CC | OC )
  OC := nil | ((i,op) x k -> CC | OC)
 *)

(* Inductive CompContext : Type := *)
(*   | CHole : CompContext *)
(*   | CVal : ExprContext -> CompContext *)
(*   | ECOp : ExprContext -> ExprContext ->  *)
(* with ExprContext : Type := *)
(*      | EAbs : ... *)

Reserved Notation " R '[[' c ']]' " (at level 35).

Fixpoint plug (R : ReductionCtx) (c : Comp) : Comp :=
  match R with
  | RTop => c
  | RLet R' c' => letC (plug R' c) c'
  | RHnd e R' => withHandle e (plug R' c)
  end.

Notation "R '[[' c ']]' " := ( plug R c ).

Theorem InjRed : forall {var : Set} (R : ReductionCtx) (c c' : Comp),
    R [[ c ]] = R [[ c' ]] -> c = c'.
Proof.
  intros var R.
  induction R.
  + (* hole *)
    intros; auto.
  + (* Let *)
    intros.
    inversion H.
    apply (@letCInj1 var) in H1.
    apply IHR.
    assumption.
  +
    intros.
    inversion H.
    apply (@withHandleInj var) in H1.
    apply IHR.
    apply H1.
Qed.

(* This is the place where I have to make steps inside reduction ctx...
Is basically c ~> c' then R[[c]] ~> R[[c']], which is pretty obvious, since
reduction contexts are the ones that by definition need to evaluate its hole.
 *)
Reserved Notation " c '~>' d " (at level 40).
(* Every Small Step its okay inside Reduction Steps*)

Inductive RedStep {ESig : Signature } : Comp -> Comp -> Type :=
| RIffTT : forall c1 c2 R,
    R [[ ifThenElse tru c1 c2]] ~> R [[ c1 ]]
| RIffFF : forall c1 c2 R,
    R [[ ifThenElse fls c1 c2]] ~> R [[ c2 ]]
| RMatchZ : forall R c1 c2, R [[ natMatch zero c1 c2 ]] ~> R [[ c1 ]]
| RMatchS : forall R e c1 c2, R [[ natMatch (succ e) c1 c2 ]] ~> R [[ c2 [ e ]c ]]
| RApp : forall (R : ReductionCtx) (A : ty ) (c : Comp1) (e : Expr),
    R [[ app (abs A c) e ]] ~> R [[ ( c [ e ]c ) ]]
| RLetCV : forall R e c,
    R [[ letC (val e) c ]] ~> R [[ c [ e ]c ]]
| RLetOp : forall R eff i op e c1 c2,
    let instance := insta eff i in
    R [[ letC (cop eff instance op e c1) c2 ]]
      ~>
    R [[ cop eff instance op e (fun Y y => letC' Y (c1 Y y) (c2 Y)) ]]
| RRec : forall R T D c1 c2,
    R [[ letRec T D c1 c2 ]] ~> R [[ c2 [ abs T (fun V v => letRec' _ T D (c1 V) (c1 V v)) ]c ]]
| RHnldVal : forall R A c e res,
    R [[ withHandle (hndlr A c res) (val e) ]] ~> R [[ c [ e ]c ]]
| RHnldOcs : forall R A B T eff e i op cv c res,
    let h := hndlr T cv res in
    Sig eff ESig op A B
    ->
    R [[ withHandle h (cop eff (insta eff i) op e c) ]]
      ~> R [[ fun V => getOcs V eff ( i , op ) e
           ( abs B (fun Y y => withHandle' Y (h Y) (c Y y) ) ) (res (expr V)) ]]
where "c '~>' d" := (RedStep c d).

Lemma SmallRed : forall {ESig : Signature } (c c' : Comp), (@step ESig c c') ->
                                       (exists (R : ReductionCtx) (c'' : Comp),
                                       c = R [[ c'' ]]).
Proof.
  intros ES c1 c2 stp.
  inversion stp.
  + exists RTop .
    exists (ifThenElse tru c2 c3).
    auto.
  + exists RTop .
    exists (ifThenElse fls c0 c2).
    auto.
  + exists RTop .
    exists (natMatch zero c2 c3).
    auto.
  + exists RTop.
    exists (natMatch (succ e) c0 c3).
    auto.
  + exists RTop.
    exists (app (abs T c) e).
    auto.
  + exists (RLet RTop c3).
    exists c0.
    auto.
  + exists RTop.
    exists (letC (val e) c).
    auto.
  + exists RTop.
    exists (letC (cop eff (insta eff i) op e c0) c3).
    auto.
  + exists RTop.
    exists (letRec T D c0 c3).
    auto.
  + exists (RHnd e RTop).
    exists c.
    auto.
  + exists RTop.
    exists (withHandle (hndlr T c res) (val e)).
    auto.
  + exists RTop.
    exists (withHandle h (cop eff (insta eff i) op e c)).
    auto.
Qed.

Inductive Steps {ESig : Signature} : Comp -> Comp -> Type :=
  | RRefl : forall c, Steps c c
  | TR : forall R c c' c'' , @RedStep ESig ( R [[ c ]] ) c' -> Steps c' c'' -> Steps c c''.

Theorem Deterministic {ESig : Signature} : forall (c r r' : Comp),
    @RedStep ESig c r -> @RedStep ESig c r' -> r = r'.
Proof.
  (* TODO *)

Inductive NSteps {ESig : Signature} : nat -> Comp -> Comp -> Type :=
| NRefl : forall (c : Comp), NSteps 0 c c
| NSucc : forall (n : nat) (c c' c'' : Comp), (@RedStep ESig c c') -> NSteps n c' c'' -> NSteps (S n) c c''.

Inductive RValues {ESig : Signature} : Comp -> nat -> Comp -> Type :=
  | RVal  : forall (c : Comp) (n : nat) (e : Expr), @NSteps ESig n c (val e) -> RValues c n (val e)
  | RCop  : forall (c : Comp) (n : nat)
                   (eff : Effect) (op : Operation eff) (e : Expr) (c' : Comp1),
      @NSteps ESig n c (callop eff op e c') -> RValues c n (callop eff op e c').

Inductive IRE {ESig : Signature} : Expr -> Expr -> Type :=
| Z_IRE : IRE zero zero
| TT_IRE : IRE tru tru
| FF_IRE : IRE fls fls
| U_IRE : IRE unit unit
| S_IRE : forall e e', IRE e e' -> IRE (succ e) (succ e')
| Abs_IRE : forall (T : ty) (c c' : Comp1),
    (forall (e : Expr), IRC ( c [ e ]c ) (c' [ e ]c) ) -> IRE (abs T c) (abs T c')
| I_IRE : forall e i i', i = i' -> IRE (insta e i) (insta e i')
(* | Hld_IRE : forall cv cv' ocs ocs'  *)
with IRC {ESig : Signature } : Comp -> Comp -> Type :=
.
