(*
Let's try (and surely fail) to make some proofs in Coq about
improved algebraic effects?
 *)

(*
I am going to follow closely "An Effect System for Algebraic Effects and Handlers",
which already has a mechanized implementation in Twelf.

 *)

From Coq Require Import Strings.String.
From Coq Require Import Arith.PeanoNat.
From Coq Require Import Lists.List.

Module Effect.
(******************************************)
(*
            Effects Representation
            Operations, Signature, Instances, Etc
 *)

(* Effects are a parameter for this module,
 not so sure if this is the correct abstraction.*)
(* Parameter Effects : Type. *)
(*  Scratch that, effects are just names!  *)
(*  Scratch that, effects are just numbers!!  *)
Definition Effect := nat.

(*Instances are Indexed Nats*)
Inductive Inst : Effect -> Type :=
  | ins : forall (e : Effect)(n : nat), Inst e.
(* Search "eq_nat". *)

Definition eqI (e : Effect) := @Logic.eq (Inst e).

Inductive Inst_eq : forall (e1 e2 : Effect), Inst e1 -> Inst e2 -> Prop :=
| Irefl : forall e i j, i = j -> Inst_eq e e (ins e i) (ins e j).

Inductive Inst_diff : forall (e1 e2 : Effect), Inst e1 -> Inst e2 -> Prop :=
| IdiffEff : forall e1 e2 i j, e1 <> e2 -> Inst_diff e1 e2 i j
| IdiffInst : forall e1 e2 i j, i <> j -> Inst_diff e1 e2 (ins e1 i) (ins e2 j).

Lemma inst_dec : forall (e1 e2 : Effect) (i : Inst e1) (j : Inst e2),
    {Inst_eq e1 e2 i j} + {Inst_diff e1 e2 i j}.
Proof.
  intros e1 e2.
  destruct (e1 =? e2) eqn:HEq.
  + intros.
    destruct i; destruct j.
    destruct (n =? n0) eqn:Inseq.
    * left.
      apply EqNat.beq_nat_true in HEq.
      rewrite HEq.
      apply Irefl.
      apply EqNat.beq_nat_true in Inseq.
      auto.
    * right.
      apply IdiffInst.
      apply EqNat.beq_nat_false in Inseq.
      apply Inseq.
  + intros.
    right.
    apply IdiffEff.
    apply EqNat.beq_nat_false in HEq.
    apply HEq.
Qed.

(*Same for op syms*)
Inductive OpSym : Effect -> Type :=
  | osym : forall (e : Effect)(n : nat), OpSym e.

Definition Operation (e : Effect) := ( Inst e * OpSym e)%type.

(* Two operations are different if either their are about different effects
 , or diff instances
 , or diff operations *)
Inductive Op_Diff : forall (eff  eff1 : Effect), Operation eff -> Operation eff1 -> Set :=
| Diff_Eff : forall (e1 e2 : Effect) (i1 : Inst e1) (i2 : Inst e2)(o1 : OpSym e1)(o2 : OpSym e2),
    e1 <> e2 -> Op_Diff e1 e2 ( i1 , o1 ) ( i2 , o2 )
| Op_DiffIns : forall (e1 e2 : Effect) (n m : nat) (o1 : OpSym e1)(o2 : OpSym e2),
    n <> m -> Op_Diff e1 e2 (ins e1 n , o1) (ins e2 m , o2)
| Op_DiffOpSym : forall (e1 e2 : Effect) (n m : nat) (i1 : Inst e1)(i2 : Inst e2),
    n <> m -> Op_Diff e1 e2 (i1 , osym e1 n) (i2 , osym e2 m).

(*  *)
Inductive Op_Eq : forall (e1 e2 : Effect), Operation e1 -> Operation e2 -> Set :=
| equal : forall (e : Effect) (i1 i2 : nat ) (o1 o2 : nat ),
    i1 = i2 -> o1 = o2 -> Op_Eq e e (ins e i1 , osym e o1) ( ins e i2 , osym e o2).

(* | diff : forall (e1 e2 : Effect) (op1 : Operation e1) (op2 : Operation e2), *)
(*     Diff e1 e2 op1 op2 -> Decidable e1 e2 op1 op2. *)

(* I'll prove it if we need it... But It should be obviously true...*)
(* Lemma Operation_Dec : forall (e1 e2 : Effect) (o1 : Operation e1) (o2 : Operation e2), *)
(*     {Op_Eq e1 e2 o1 o2} + {Op_Diff e1 e2 o1 o2}. *)
Theorem Operation_Dec : forall (e1 e2 : Effect) (o1 : Operation e1) (o2 : Operation e2),
    Op_Eq e1 e2 o1 o2 + Op_Diff e1 e2 o1 o2.
Proof.
  intros eff1 eff2.
  destruct (eff1 =? eff2) eqn:He; intros Op1 Op2;
    destruct Op1 as [ I1 O1 ]; destruct Op2 as [ I2 O2 ].
  {
      apply  Nat.eqb_eq in He.
      destruct He.
      destruct (inst_dec eff1 eff1  I1 I2 ) eqn:HInst.

      destruct I1 .
      destruct I2 .
      destruct (n =? n0) eqn:NEq.
      apply Nat.eqb_eq in NEq.
      destruct NEq.
      destruct O1.
      destruct O2.
      destruct (n0 =? n1) eqn:NEq.
      apply Nat.eqb_eq in NEq.
      destruct NEq.
      left.
      apply equal.
      auto.

      auto.

      apply Nat.eqb_neq in NEq.
      right.
      apply Op_DiffOpSym.
      auto.

      apply Nat.eqb_neq in NEq.
      right.
      apply Op_DiffIns; auto.

      right.

      destruct I1. destruct I2.
      apply Op_DiffIns.
      inversion i; auto.
  }

  right.
  apply Diff_Eff.
  apply  Nat.eqb_neq in He.
  auto.
Qed.

(******************************************)
(* Regions *)

(* Given an Effect a region is just a list of instances of that Effect.
This could be replace by a Fin type..
 *)
Definition Region (e : Effect) := list ( Inst e ).

Import Lists.List.ListNotations.

Inductive InList (a : Set) : a -> list a -> Type :=
| IL_here : forall (x : a) (xs : list a), InList a x (x :: xs)
| IL_there : forall (x y : a) ( xs : list a), InList a x xs -> InList a x (y :: xs).

Inductive SubList (a : Set) : list a -> list a -> Type :=
| SL_empty : forall (xs : list a), SubList a [] xs
| SL_refl : forall (xs : list a), SubList a xs xs
| SL_trns : forall (xs xs' ys : list a),
    SubList a xs xs' -> SubList a xs' ys
    -> (**************************)
    SubList a xs ys.

Definition InRegion (e : Effect) := InList ( Inst e ).

(*Regions subsets*)
Definition SubRegions (e : Effect ) := SubList ( Inst e ).

(******************************************)
(* DIRTTTTT *)
Definition Dirt (e : Effect) := list ( Operation e ).

Definition InDirt (e : Effect) := InList ( Operation e ).
Definition SubDirt (e : Effect) := SubList ( Operation e ).

(******************************************)

(* Simple Types *)
Inductive ty : Type :=
  | Bool : ty
  | Nat : ty
  | Unit : ty
  | Empty : ty
  (******************************************)
              (* Usual function type, A -> C *)
  | Arrow : ty -> dty -> ty
              (* Handler types, C => D *)
  | Carrow : dty -> dty -> ty
              (* Effect types, E^R *)
  | EffTy : forall (e : Effect), Region e -> ty
with
dty : Type :=
  | Dty : forall (e : Effect), ty -> Dirt e -> dty.

(******************************************)

(* For a given effect E, signature of E = {op_1 : A -> B, op_2 : A' -> B' , ...} *)
(* Definition Signature (eff : Effect) := list ( OpSym eff * ty * ty). *)
(* As far as I understand Twelf, they define a nameless list of Effects maybe
 I have to follow their decision... *)

Definition EffSignature := list ( ty * ty ).
Definition Signature := list EffSignature.
(* (* From Coq Require Import Vectors.Fin. *) *)
(* (* So... I think I should use Fin n, but there is a drawback... That I'll be *)
(* lifting a natural number to the type lvl... And I dont know if it would be *)
(* useful or painful xD*) *)
Inductive EffSig : EffSignature -> nat -> ty -> ty -> Type :=
| Eff_here : forall (A B : ty) (res : EffSignature), EffSig (( A , B ) :: res) 0 A B
| Eff_there : forall (efs : EffSignature) (nm : nat ) (A B A' B' : ty),
    EffSig efs nm A B -> EffSig ((A' , B') :: efs) (S nm) A B.

Inductive Sig : forall (eff : Effect), Signature -> OpSym eff -> ty -> ty -> Type :=
| Sig_here : forall (esig : EffSignature) (rest : Signature) (A B : ty) (n : nat),
    EffSig esig n A B -> Sig 0 (esig :: rest) (osym 0 n) A B
| Sig_there : forall (eff : Effect) (s : EffSignature) (sig : Signature) (op : nat ) (A B : ty),
      Sig eff sig (osym eff op) A B -> Sig (S eff) (s :: sig) (osym (S eff) op) A B.
End Effect.
