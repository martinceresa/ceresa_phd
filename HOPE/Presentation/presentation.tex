\documentclass[english]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Semantics
\usepackage[ligature,reserved,inference]{semantic}
\usepackage{stmaryrd} %% Scott's brackets
\usepackage{mathtools}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Drawing 
\usepackage{tikz}
\usetikzlibrary{arrows,shapes.misc,shadows}

\usepackage[english]{babel}

\input{commands.tex}

\newcommand\probPic{%
    \begin{tikzpicture}[node distance=4em,auto]
      \node[draw] (PT) at (0,0) {\(t \oplus_{p} s\)};
      \node[draw] (A) at (2,1) {\(t\)};
      \node[draw] (B) at (2,-1) {\(s\)};
%
      \draw[->] (PT) to [out=90, in=180] node [above] {\(p\)} (A.west);
      \draw[->] (PT.south) to [out=270,in=180] node [below] {\(1-p\)} (B.west) ;
    \end{tikzpicture}%
}

\usetheme{CambridgeUS}
\usecolortheme{lily}

\title{Effectful Improvement Theory}
\author
  {M. Ceresa\inst{1,2}}
\institute[CONICET \and UNR]{
  \inst{1}%
  CIFASIS - CONICET
  \and
  \inst{2}%
  Departamento de Cs de la Computación\newline
  Universidad Nacional de Rosario
}
\date[HOPE'20]%
  {Workshop on Higher-Order Programming with Effects, 2020}

\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Title Frame and Content
\begin{frame}[plain]
  \titlepage
\end{frame}

% \begin{frame}
%   \frametitle{Table of Content}
%   \tableofcontents[currentsection]
% \end{frame}
\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Table of Content}
    \tableofcontents[currentsection]
  \end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Introduction]{Observation Approximation \& Improvement}
\note{Introduce the classical notions of Morris' Contextual
  Equivalente, and Abramsky's applicative simulation, and show what
did Sands.
  Ugo Dal Lago implemented both notions when algebraic effects are
taken into account, and how we also introduce costs.}

\note{Morris equiv definition and Sands improvement definition.
  + Non-termination plays a fundamental part in these definition.
  + Remark that we have an instrumented evaluation function.
}

\subsection{Language \& Evaluation}

\begin{frame}{shrink}
  \frametitle{Pure \(\lambda\)-Calculus}
  \begin{block}{Syntax:}
    \vspace{-1em}
    \begin{align*}
      M,N &::= (\LRet{V}) \mid (\LApp{V}{W}) \mid (\LTo{M}{x \mathrel{.} N}) \\
      V,W &::= x \mid (\LAbs{x}{M})
    \end{align*}
  \end{block}
  \begin{block}{Operational Semantic:}
    \vspace{-1em}
    \begin{align*}
      [[ \cdot ]] &: Term -> {Value}_{\bot} \\
      [[ \LRet{V} ]] &= in_l(V) \\
      [[ \LApp{(\lambda x . M)}{W} ]] &= [[ \substT{M}{x}{W} ]] \\
      [[ \LTo{M}{x.N} ]]  &=
            \begin{dcases}
              [[ \substT{N}{x}{V} ]] & \text{if} \ [[ M ]] = in_l(V) \\
              in_r(\bot) & \text{otherwise}
            \end{dcases}
    \end{align*}
  \end{block}
\end{frame}

\subsection{Observational Approximation}

\begin{frame}{shrink}
  \frametitle{Observational Approximation}
  \begin{block}{Observational Approximation}
    % Two terms are equivalent if there is no context that distinguishes them
    \begin{displaymath}
      t \sqsubseteq u
      \iff
      \forall \mathcal{C}[\cdot], [[ \mathcal{C}[t] ]] = in_l(\_) 
      \implies [[ \mathcal{C}[u] ]] = in_l(\_)
    \end{displaymath}
  \end{block}
  Where the observation is convergence.
  \begin{block}{Abramsky's Applicative Simulation}
    % Two programs are applicative equivalent if the values obtained
    % from their evaluation are extensional equivalent.
    \begin{displaymath}
    s \sim t \iff [[ s ]] = in_l(u) \implies [[ t ]] = in_l(v) \land u \sim_{v} v
    \end{displaymath}
    % Two values are extensional equivalent if and only if they form
    % applicative similar programs when applied to the same argument.
    \begin{displaymath}
      u \sim_{v}  v \iff \forall w, (u \ w) \sim (v \ w)
    \end{displaymath}
  \end{block}
\end{frame}

\subsection{Improvement}
  \begin{frame}
    \frametitle{Adding Costs}
    \begin{block}{Tick's Family}
      \begin{align*}
        add^m &: (X \times \Nat) -> (X \times \Nat) \\
        add^m (x, c) &= (x, m + c)
      \end{align*}
    \end{block}
    \begin{block}{Instrumented Operational Semantic:}
      \vspace{-1em}
      \begin{align*}
        [[ \cdot ]]_{\Nat} &: Term -> {(Value \times \Nat)}_{\bot} \\
        [[ \LRet{V} ]]_{\Nat} &= in_l(V, 1) \\
        [[ \LApp{(\lambda x . M)}{W} ]]_{\Nat} &= add^1 [[ \substT{M}{x}{W} ]]_{\Nat} \\
        [[ \LTo{M}{x.N} ]]_{\Nat}  &=
              \begin{dcases}
                add^{V_c + 1} [[ \substT{N}{x}{V} ]]_{\Nat} & \text{if} \ [[ M ]]_{\Nat} = in_l(V,V_c) \\
                in_r(\bot) & \text{otherwise}
              \end{dcases}
      \end{align*}
  \end{block}
    
  \end{frame}

  \begin{frame}{shrink}
    \frametitle{Improvement}
    \begin{block}{Improvement}
      Let \(t,u\) be two terms. We say \(t\) is improved by \(u\) if
      in any context \(t\) takes longer to evaluate than \(u\):
      \begin{displaymath}
      t \preceq u \iff \forall \mathcal{C}[\cdot],
      [[ \mathcal{C}[t] ]]_{\Nat} = in_l(\_,n) \implies [[ \mathcal{C}[u] ]]_{\Nat}
      = in_l(\_,m)\land n \geq m
      \end{displaymath}
    \end{block}
    Where now the observation consists in first checking convergence
    and then the cost of the evaluation.
    \begin{block}{Improvement Simulation}
      One closed term improves another if the values obtained from their
      evaluation are extensional improvement and respect costs.
      \begin{displaymath}
        s \sim^c t \iff [[ s ]]_{\Nat} = in_l(u, n)
        \implies [[ t ]]_{\Nat} = in_l(v,m)
        \land n \geq m \land u \sim^c_{v} v
      \end{displaymath}
    \end{block}
  \end{frame}

  \note{Introduce an example to motivate the problem or what we have done}
  \begin{frame}
    \frametitle{Motivational Example}
    Let \(\oplus_{p}\) be a probabilistic choice operation, such that,
    when evaluated \(M \oplus_{p} N\) behaves like \(M\) with
    probability \(p\) or like \(N\) with probability \(1-p\).

    \begin{columns}
      \begin{column}{0.3\textwidth}%
        \probPic\
      \end{column}
      \begin{column}{0.7\textwidth}
        \begin{itemize}
        \item When can we say that a probabilistic term improves
          another? What is an improvement in the presence of
          probabilistic effects?
        \item Let \(M,N\) be closed terms, is \( \LTo{M}{x.T}
          \sqsubseteq T\) a valid improvement?
        \end{itemize}
      \end{column}
    \end{columns}

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    More generally, what is an improvement in the presence of
    algebraic effects?
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%     \begin{exampleblock}{Common Expression Improvement}
%       Let \(T\) be a program that evaluates to a natural number.
%       It is easy to see that: \( T + T \geq let \ t = T \ in \ t + t\)
%     \end{exampleblock}
%     While it is not so easy to see if a probabilistic program is
%     improved by another.
%     %
%     \textbf{I need a good motivational example.}
%     \begin{exampleblock}{Effectful Common Expression}
%      Let \(S\) be a program that evaluates to a natural number.
%      \( (S \oplus_{\frac{1}{2}} T) + (S \oplus_{\frac{1}{2}} T)
%      ??? let \ st = S \oplus_{\frac{1}{2}} T \ in \ st + st \)
%     \end{exampleblock}
%     The above transformation is not even a valid one.

% When can we say that a probabilistic program improves another?
% More generally, how do we prove improvements in presence of probabilistic effects?
  \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Algebraic Effects]{Adding Effects}
\note{Introducing Algebraic Effects.
  + Operations are introduced by uninterpeted symbols given by a signature.
  }
\subsection{Algebraic Effects}

\begin{frame}
  \frametitle{Algebraic Effects}
  Effects symbols are introduced by signatures, and interpreted as
  operations in a monad.
  % \begin{block}{Signature}
  %   A \emph{signature} is a pair \((\Sigma, \alpha)\) of a non-empty
  %   set of symbols \(\Sigma\) and an arity function,
  %   \(\alpha : \Sigma \to \mathbb{N}\).
  % \end{block}
  \begin{block}{\(\Sigma\)-Algebra}
   Let \((\Sigma,\alpha)\) be a signature, a \emph{\(\Sigma\)-algebra} is
   a pair composed by:
   \begin{itemize}
   \item A carrier set \(A\)
   \item A family of maps \((\cdot)^A\)
   \end{itemize}
   Such that:
   \( \forall \sigma \in \Sigma, \sigma^A : A^{\alpha(\sigma)} \to A \)
  \end{block}
  \begin{block}{\(\Sigma\)-algebraic Monad}
    Let \(T\) be a monad such that for each set \(X\), \(T(X)\) has
    a \(\Sigma\)-algebra
  \end{block}
  \end{frame}

\begin{frame}
  \frametitle{Effectful Language and Monadic Operational Semantic}
  % Effects are interpreted by mondas. 
  \vspace{-1em}
  \begin{block}{Effectful Language}
  Let \(\Sigma\) be a signature, and \(\sigma \in \Sigma\):
  \begin{align*}
    M,N &::= (\LRet{V}) \mid (\LApp{V}{W}) \mid (\LTo{M}{x \mathrel{.} N})
          \mid \bText{\sigma(M_1 , \ldots, M_n)} \\
    V,W &::= x \mid (\LAbs{x}{M})
  \end{align*}
  \end{block}
  \begin{block}{Monadic Operational Semantic}
    Let \(T\) be a \(\Sigma\)-algebraic monad.
    \begin{displaymath}
    \begin{array}{rrcl}
      &[[ \cdot ]] &:& Term -> T(Value) \\
      &[[ \LRet{V} ]] &=& \eta(V) \\
      &[[ \LApp{(\lambda x . M)}{W} ]] &=& [[ \substT{M}{x}{W} ]] \\
      &[[ \LTo{M}{x.N} ]]  &=& [[ M ]] >>= (V |-> [[ \substT{N}{x}{V} ]]) \\
      \blacktriangleright &[[ \sigma(M_1 , \ldots, M_n) ]] &=& \sigma^T([[ M_1 ]], \ldots, [[ M_n ]])
    \end{array}
  \end{displaymath}
  \end{block}
  \end{frame}

\subsection{Relator}
\note{Not sure about this slide... I think it is better to avoid
  technical definitions as much as possible}
\begin{frame}
  \frametitle{Relator}
  \begin{block}{Functor Relator} Monotonic Functor in cat Rel
    % \vspace{-1em}
  \begin{align*}
    Id_{FX} &\subseteq \Gamma (Id_X)  \tag{Rel-1}\\
    \Gamma R \circ \Gamma S &\subseteq \Gamma (R \circ S) \tag{Rel-2} \\
    \Gamma ({(f \times g)}^{-1} R) &\subseteq {(F f \times F g)}^{-1}\Gamma R \tag{Rel-3} \\
    R \subseteq S &\implies \Gamma R \subseteq \Gamma S \tag{Rel-4}
  \end{align*}
  \end{block}
  \begin{block}{Monad Relator}
    A monad relator is a functor relator such that:
  % We define a monad relator \(\Gamma\) for \(T\) if and only if
  % \(\Gamma\) is a functor relator for \(T\) regarded as a functor, and:
  \begin{align*}
    \forall x \in X, y \in Y, {x}\mathrel{R}{y} & \implies {\eta_X(x)}\mathrel{\Gamma R}{\eta_Y(y)} \\
    \forall  u \in T(X), v \in T(Y), {u}\mathrel{\Gamma R}{v}
             & \implies {(u >>= f)}\mathrel{\Gamma S}{(v >>= g)}
  \end{align*}
  whenever \(\forall x \in X, y \in Y, {x}\mathrel{R}{y} \implies {f(x)}\mathrel{\Gamma S}{g(y)}\)
  \end{block}
  \end{frame}
  \begin{frame}
    \frametitle{Relator}
\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        

\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
%uncomment if require: \path (0,300); %set diagram left start at 0, and has height of 300

%Shape: Circle [id:dp22461860643186526] 
\draw   (85,145) .. controls (85,107.44) and (115.44,77) .. (153,77) .. controls (190.56,77) and (221,107.44) .. (221,145) .. controls (221,182.56) and (190.56,213) .. (153,213) .. controls (115.44,213) and (85,182.56) .. (85,145) -- cycle ;
%Shape: Circle [id:dp6251717812198404] 
\draw   (346,143) .. controls (346,105.44) and (376.44,75) .. (414,75) .. controls (451.56,75) and (482,105.44) .. (482,143) .. controls (482,180.56) and (451.56,211) .. (414,211) .. controls (376.44,211) and (346,180.56) .. (346,143) -- cycle ;
%Curve Lines [id:da12507629128428588] 
\draw    (173.5,81) .. controls (214.72,46.99) and (327.84,46.65) .. (366,89.34) ;
\draw [shift={(367.13,90.65)}, rotate = 229.94] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Shape: Free Drawing [id:dp1583107546415614] 
\draw  [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=6] [line join = round][line cap = round] (161.13,163.65) .. controls (161.66,163.12) and (162.13,162.4) .. (162.13,161.65) ;
%Shape: Free Drawing [id:dp8978237093640393] 
\draw  [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=6] [line join = round][line cap = round] (417.13,165.65) .. controls (417.66,165.12) and (418.13,164.4) .. (418.13,163.65) ;
%Curve Lines [id:da04601178440313858] 
\draw    (163.13,164.65) .. controls (184.91,173.56) and (367.43,192.27) .. (413.77,168.39) ;
\draw [shift={(415.13,167.65)}, rotate = 510.4] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

% Text Node
\draw (155.5,61.75) node   [align=left] {\begin{minipage}[lt]{33.32pt}\setlength\topsep{0pt}
X x Y
\end{minipage}};
% Text Node
\draw (428,61.75) node   [align=left] {\begin{minipage}[lt]{70.72pt}\setlength\topsep{0pt}
T(X) x T(Y)
\end{minipage}};
% Text Node
\draw (263.5,37.25) node   [align=left] {\begin{minipage}[lt]{19.720000000000002pt}\setlength\topsep{0pt}
$\displaystyle \Gamma $
\end{minipage}};
% Text Node
\draw (162.57,140.15) node   [align=left] {\begin{minipage}[lt]{14.37064453125pt}\setlength\topsep{0pt}
R\\
\end{minipage}};
% Text Node
\draw (434.07,139.15) node   [align=left] {\begin{minipage}[lt]{32.73064453125pt}\setlength\topsep{0pt}
$\displaystyle \Gamma $ R\\
\end{minipage}};


\end{tikzpicture}
  \end{frame}

\subsection{Effectful Observational Approximation}
  \begin{frame}
    \frametitle{Effectful Observational Approximation}
    Let \(\Sigma\) be a signature, \(T\) a monad, \(\Gamma\) a
    monad relator for \(T\), and \(\mathcal{U} = Values \times Values\).
    \begin{block}{Effectful Observational Approximation}
      \begin{displaymath}
        M \sqsubseteq_{\Gamma} N \iff \forall C[\cdot],
        [[ C[M] ]] \Gamma \mathcal{U} [[ C[N] ]]
      \end{displaymath}
    \end{block}
    \begin{block}{Effectful Applicative Simulation}
      \begin{align*}
       s \sim_{\Gamma} t & \iff [[ s ]] \mathrel{\Gamma (\sim_v)} [[ t ]] \\
       v \sim_v u & \iff \forall w, (\LApp{v}{w}) \sim_{\Gamma} (\LApp{u}{w})
      \end{align*}
      \end{block}
      TODO : Algebraic Effects implies that Effectful Simulations implies
      Observational Approximation.
      TODO : Say something about the co-inductive definitions?
  \end{frame}

  \begin{frame}
    \frametitle{Observing Non-Termination}
    We can go back to the classic Morris definition if we take
    termination as the only observable effect with the help of the
    lifting monad.
    \begin{block}{Lifting Relator}
     Let \(X, Y\) be set, and \(R \subseteq X \times Y\).
     \begin{displaymath}
       x \mathrel{R_{\bot}} y \iff
       \begin{dcases}
         \forall v, x = in_l(v) \implies \exists u, y = in_l(u) \land v
         \mathrel{R} u \\
         x = in_r(\bot)
       \end{dcases}
     \end{displaymath}
    \end{block}
    \begin{block}{Lifting Observational Approximation}
      \begin{displaymath}
        M \sqsubseteq_{\bot} N \iff \forall C[\cdot],
        % C[M],C[N] \in ClosedTerms,
      [[ C[M] ]] = in_l(\_) \implies [[ C[N] ]] = in_l(\_)
    \end{displaymath}
    \end{block}
  \end{frame}

\subsection{Adding Costs to Effectful Terms}
  \begin{frame}
    \frametitle{Cost as an Effect}
    % Let \(\Sigma\) be a signature,
    % We add a new symbol \(\checkmark\) representing an unary operation
    % that increases the cost of the evaluation.
    Let \(T\) be a monad, we can lift the family of ticks operations:
    \begin{align*}
      \checkmark^n &: T(X \times \Nat) -> T(X \times \Nat) \\
      \checkmark^n &= T \ add^n
    \end{align*}
    such that equips each element in \(X\) with a natural number, and
    adds a new operation enabling us to add costs.
    % \begin{block}{Tick}
    %   \vspace{-1em}
    %   \begin{align*}
    %     add^n & : (X \times \Nat) -> (X \times \Nat)\\
    %     add^n (x, c) & \triangleq (x, n + c)\\
    %     \checkmark^n & \triangleq T \ add^n
    %   \end{align*}
    %   \end{block}
    \begin{block}{Inner Cost Monad}
      % Monad Transformer with \((\Nat, (+), 0)\)
      \vspace{-1em}
      \begin{align*}
       T_C (X) & \triangleq T(X \times \Nat) \\
       \eta_C (x) & \triangleq \eta(x, 0) \\
       m >>=_C f & \triangleq m >>= (v,c) |-> \checkmark^c \ f(v)
      \end{align*}
    \end{block}
  \end{frame}
  
  \begin{frame}
    \frametitle{Cost Relator}
    Let \(\Gamma\) be a monad relator for monad \(T\).
    \begin{block}{Product Cost Relator}
      Let \(R \subseteq X \times Y\), define \(R^C \subseteq (X\times
      \Nat) \times (Y \times \Nat)\):
      \begin{displaymath}
       (x,m) \mathrel{R^C} (y, n)  \iff x \mathrel{R} y \land m \geq n
      \end{displaymath}
    \end{block}
    % Define a cost relator \(\Gamma^C\) simply as the composition of
    % \(\Gamma\) with \((\cdot)^C\).
    \begin{block}{Cost Relator}
     Let \(\Gamma\) be a relator for monad \(T\), \(\Gamma^C \triangleq
     \Gamma \circ (\cdot)^C\) is a monad relator for the inner cost
     monad \(T_C\)
    \end{block}
    % \(\equiv \Gamma(R^C)\).

    For example, the cost relator for the lifting relator is:
    Let \(R \subseteq X \times Y\)
    \begin{displaymath}
     x \mathrel{R^C_{\bot}} y \iff
     \begin{dcases}
       y = in_l(u,u_c) \land v \mathrel{R} u \land v_c \geq u_c & x = in_l(v,v_c) \\
       x = in_r(\bot)
     \end{dcases}
    \end{displaymath}
  \end{frame}

  \begin{frame}
    \frametitle{Instrumented Monadic Operational Semantics}
    We can add a new unary operation symbol \(\Sigma_{\checkmark}
    \triangleq \Sigma \cup \{\checkmark\}\)
    \begin{block}{Instrumented Monadic Operational Semantics}
      \vspace{-1em}
      \begin{align*}
        {[[ \LRet{V} ]]}_{\Nat} &= \checkmark \eta_{\Nat}(V) \\
        {[[ \LApp{(\lambda x . M)}{W} ]]}_{\Nat} &= \checkmark {[[ M[x:=W] ]]}_{\Nat} \\
        {[[ \LTo{M}{x.N} ]]}_{\Nat}  &= \checkmark {[[ M ]]}_{\Nat} {>>=}_{\Nat} (V |-> {[[ N[x:=V] ]]}_{\Nat}) \\
        {[[ \sigma(M_1 , \ldots, M_n) ]]}_{\Nat} &=
              \begin{dcases}
                \checkmark [[ M_1 ]] & \text{if} \ \sigma = \checkmark \\
                \checkmark \sigma^T({[[M_1]]}_{\Nat}, \ldots, {[[M_n]]}_{\Nat}) & \text{otherwise}
              \end{dcases}
        % {[[ \checkmark M ]]}_{\Nat} &= \checkmark [[ M ]]_{\Nat} \\
        % {[[ \sigma(M_1 , \ldots, M_n) ]]}_{\Nat} &= \checkmark \sigma^T({[[M_1]]}_{\Nat}, \ldots,{[[M_n]]}_{\Nat})
      \end{align*}
    \end{block}
  \end{frame}

  \begin{frame}{shrink}
    \frametitle{Effectful Improvement}
    % Let \(\Sigma\) be a signature, \(T\) a monad, \(\Gamma\) a
    % monad relator for \(T\), and
    \begin{block}{Effectful Observational Improvement}
      Let \(\mathcal{U} = Values \times Values\).
      % \vspace{-1em}
      \begin{displaymath}
        M \preceq_{\Gamma} N \iff \forall C[\cdot],
        [[ C[M] ]]_{\Nat} \Gamma^C \mathcal{U} [[ C[N] ]]_{\Nat}
      \end{displaymath}
    \end{block}
    Same as Sands, effectful improvement simply restricts the
    observations made by approximation checking that the cost does
    not increase.
    \begin{block}{Effectful Improvement Simulation}
      \begin{align*}
       s \sim_{\Gamma^C} t & \iff [[ s ]] \mathrel{\Gamma^C (\sim_v)} [[ t ]] \\
       v \sim_v u & \iff \forall w, (\LApp{v}{w}) \sim_{\Gamma^C} (\LApp{u}{w})
      \end{align*}
      
    \end{block}
  \end{frame}
  \begin{frame}
    \frametitle{Lifiting Improvement and Simulation}
    
    \begin{block}{Lifting Improvement}
      \begin{displaymath}
        M \preceq_{\bot} N \iff \forall C[\cdot],
      [[ C[M] ]] = in_l(\_,m) \implies [[ C[N] ]] = in_l(\_, n)
      \land m \geq n
    \end{displaymath}
    \end{block}
    \begin{block}{Lifting Improvement Simulation}
      \begin{displaymath}
        s \sim^C_{\bot} t \iff [[ s ]]_{\Nat} = in_l(u, n)
        \implies [[ t ]]_{\Nat} = in_l(v,m)
        \land n \geq m \land u \sim^c_{v} v
      \end{displaymath}
    \end{block}
  \end{frame}
% \begin{frame}
% \frametitle{Applicative Simulation}
% Assuming an evaluation function \(\Downarrow\) from programs to values
% \begin{block}{Applicative Simulation}
%   Two programs are applicative equivalent if the values obtained
%   from their evaluation are extensional equivalent.
%   \begin{displaymath}
%   s \sim t \iff s \Downarrow u \land t \Downarrow v \land u \sim_{v} v
%   \end{displaymath}
%   Two values are extensional equivalent if and only if they form
%   applicative similar programs when applied to the same argument.
%   \begin{displaymath}
%     u \sim_{v}  v \iff \forall w, (u \ w) \sim (v \ w)
%   \end{displaymath}
%   \end{block}
% \end{frame}
% \begin{frame}
%   \frametitle{Improvement Simulation}
%   \begin{block}{Improvement Simulation}
%     One program improves another if the values obtained from their
%     evaluation are extensional improvement and respect costs.
%     \begin{displaymath}
%     s \sim^c t \iff s \Downarrow^n u \land t \Downarrow^m v \land u
%     \sim^c_{v} v \land n \geq m
%     \end{displaymath}
%     Two values are extensional improvement ...
%     \begin{displaymath}
%      u \sim^c_{v}  v \iff \forall w, (u \ w) \sim^c (v \ w)
%     \end{displaymath}
%     \end{block}
%   \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Examples and applications}
\note{Examples and applications: CSE, Tailor-Made monad, Probabilistic
example}

\subsection{Probabilistic Effect}

\begin{frame}
  \frametitle{Example: Probabilistic Effect}
  Let \({\oplus}_{p}\) represent the binary probabilistic operator
  such that the term \(t \oplus_p u\) behaves like \(t\) with
  probability \(p\) and behaves like \(u\) with probability \(1 - p\).
  %
  \begin{align*}
    \mathcal{D}(X) &\triangleq \{ \mu : X -> [0,1] | \sum(\mu(x)) \leq 1, \text{finite support} \} \\
    \eta (v) &\triangleq u |-> \begin{dcases}
      1 & \text{if} \ v = u \\
      0 & otherwise
      \end{dcases}\\
    \mu >>= f &\triangleq u |-> \sum_{x \in X}(\mu(x) * f(x)(u))
  \end{align*}

  % Interpreted by the sub-distribution finite monad.
  \begin{columns}
    \begin{column}{0.3\textwidth}%
      \probPic\
    \end{column}
    \begin{column}{0.7\textwidth}
      \begin{displaymath}
       \oplus^{\mathcal{D}}_p (\mu, \nu) \triangleq v |-> p * \mu(v) +
       (1 - p) * \nu(v) 
      \end{displaymath}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Example: Probabilistic Relator}
  \begin{block}{Probabilistic Relator}
   Let \(R \subseteq X \times Y\).
   \begin{displaymath}
    \mu \mathrel{\Gamma_{\mathcal{D}} R} \nu \iff \forall S \subseteq X, \mu(S)
    \leq \nu(R(S))
   \end{displaymath}
  \end{block}
  \begin{block}{Probabilistic Improvement}
    Given two probabilistic terms \(t,u\),
    \begin{displaymath}
      t \preceq_{\mathcal{D}} u \iff \forall C[\cdot], S \subseteq
      (Value \times \Nat), [[ C[t] ]]_{\Nat}(S) \leq [[ C[u] ]]_{\Nat}(\mathcal{U}^C)
    \end{displaymath}
  \end{block}
  
\end{frame}

\begin{frame}{shrink}
  \frametitle{Dead Sampling Elimination}
  \begin{block}{Uniform Sampling}
    \vspace{-1em}
    \begin{displaymath}
      [[ uniformSampling_n ]](m) = 
      \begin{dcases}
        \frac{1}{n+1} & m \in \{0, 1, \ldots, n\} \\
        0 & \text{otherwise}
      \end{dcases}
    \end{displaymath}
   Note that \(uniformSampling_n\) generates a probability distribution:
   \( \sum_{v \in Values} [[ uniformSampling_n ]](v) = 1 \)
  \end{block}
   % We can use this machinery to remove unnecessary samplings
   % Assume \( uniformSampling_n \) samples a natural value between
   % \(0\) and \(n\) with uniform probability, \(\frac{1}{n+1}\).
  \begin{block}{Dead Sampling}
   Let \(M\) be a closed term: \( \LTo{uniformSampling_n}{x . M}
   \preceq_{\mathcal{D}} \checkmark M\).
   \vspace{-1em}
  \begin{align*}
    &[[ \LTo{uniformSampling_n}{x . M} ]]_{\Nat} \equiv \\ 
    &\checkmark [[ uniformSampling_n ]]_{\Nat} >>=_{\Nat} \lambda m. [[ M[x := m] ]]_{\Nat} \equiv \\
    &\checkmark [[ uniformSampling_n ]]_{\Nat} >>=_{\Nat} \lambda m. [[ M ]]_{\Nat} \preceq_{\mathcal{D}}\\
    &\checkmark [[ M ]]_{\Nat}
  \end{align*}
  \end{block}
  
\end{frame}

% \begin{frame}
%   \frametitle{Examples: Exceptions}
%   Let \(E\) be a non-empty set of exception symbols.
%   Let \(\Sigma_E \triangleq \{ raise_e \mid e \in E\}\) be a set of
%   unary operation symbols, interpreted by the monad \(E(X) \triangleq
%   (X + E)_{\bot}\), and relator \(\Gamma_E\) defined as follows, let
%   \(R \subseteq X \times Y\):
%   \begin{displaymath}
%     p \mathrel{\Gamma_E R} q \iff
%     \begin{dcases}
%       p = in_l(in_l(x)) \implies q = in_l(in_l(y)) \land x \mathrel{R}
%       y \\
%       p = in_l(in_r(e_1)) \implies q = in_l(in_r(e_2)) \land e_1 = e_2 \\
%       p = in_r(\bot)
%     \end{dcases}
%   \end{displaymath}
%     Therefore,
%   \begin{displaymath}
%     p \mathrel{\Gamma^C_E R} q \iff
%     \begin{dcases}
%       p = in_l(in_l((x,x_c))) \implies q = in_l(in_l(y,y_c)) \land x \mathrel{R}
%       y \land x_c \geq y_c \\
%       p = in_l(in_r(e_1)) \implies q = in_l(in_r(e_2)) \land e_1 = e_2 \\
%       p = in_r(\bot)
%     \end{dcases}
%   \end{displaymath}
% \end{frame}

%% Example Non-determinism
% \begin{frame}
%   \frametitle{Example: Non-determinism}
%   Let \(\oplus\) represent the non-deterministic choice operator.
%   Interpreted by the monad of all subsets.
%   \begin{align*}
%     \eta (v) & \triangleq \{ v \} \\
%     U >>= f & \triangleq \bigcup_{u \in U}f(u)
%   \end{align*}
%   Let \(R \subseteq X \times Y\), define \(\Gamma_{\mathcal{P}}\):
%   \begin{displaymath}
%    P \mathrel{\Gamma_{\mathcal{P}} R} Q \iff \forall p \in P, \exists
%    q \in Q, p \mathrel{R} q
%   \end{displaymath}
% \end{frame}

\subsection{Other Examples}
\begin{frame}
  \frametitle{Other Examples}
  We can also interpret other effects as:
  \begin{itemize}
    \item Non-Deterministic effect.
    \item Exceptions
    \item Input-Output
    \item State
  \end{itemize}
  And we can also mix them.

  Working with contextual definitions is hard.
  There is also a notion of \emph{Applicative Simulation} derived
  following the same principles and the use of relators.
  %
  However, it is required algebraic effects.

  Here we proposed one method which employs the inner monad, but other
  ways can be employed to generate an improvement theory. For example
  we have a tailor-made monad for exceptions.
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Future Work}
\begin{frame}
  \frametitle{Future Work}
  \begin{itemize}
    \item Crypto? Game-based proofs checking properties as how many
      times an oracle was consulted.
    \item Other thingies?
    \item Study the relation between the observations made by the
      relator and possible optimizations
    \item General tick algebra?
    \item Generic Optimizations? Common Sub-Expression Elimination
      with effects?
    \item Explore other notions of costs. Sands' idea of just counting
applications
  \end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Summary}

\begin{frame}
 \frametitle{Summary \& Contact} 
 Short summary and contact information.
 Future work? Crypto Approach, Security, etc?
 Mail: mceresa@dcc.fceia.unr.edu.ar
\end{frame}
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%