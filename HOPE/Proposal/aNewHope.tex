\documentclass{article}

\input{packages}

\newcommand\CC{\mathbb{C}}
\newcommand\cimproved{\geq}
\newcommand\N{\mathbb{N}}

\newtheorem{definition}{Definition}

\title{Effectful Improvement Theory}
\author{Martín Ceresa %\inst{1}%
  \\
  \small{joint work with Mauro Jaskelioff}%\inst{1,2}%
}
% \institute{
%   National University of Rosario, Argentina \\
%   \email{martin@dcc.fceia.unr.edu.ar}
%   \and
%   CIFASIS -- CONICET
% }
\date{HOPE 2020 Talk Proposal}

\begin{document}
\maketitle

%% Small introduction to program optimization.
Program optimization is a difficult task, since we have to take into account two
different aspects of computation:
\begin{itemize}
  \item  \emph{what} we compute and
  \item \emph{how} we compute.
\end{itemize}
We want to improve \emph{how} computation is performed \textbf{without} changing
\emph{what} we compute, i.e.\ preserving the meaning of programs.
%
%% What are we going to do?
Our task is then to \emph{observe} intentional properties of execution, such as
computing time, energy consumption, memory usage, etc, while preserving the
meaning of programs.
% In other words, we need to establish a relation between programs that relate
% safe program transformation in a way that the observed intentional properties
% are improved.

\emph{Improvement Theory}~\cite{Sands:Op:Theories} does precisely this for pure
functional languages with divergence. It presents a relation called
\emph{improvement} as a stricter notion of equivalence which establishes which
transformations are safe while also enabling the comparison of program
performance. This theory is obtained as a refinement of notions of program
equivalence, as shown below.

Observational equivalence~\cite{morrisphd} states that two programs are
equivalent if there is no context that distinguishes them. Assuming a language
where programs might not terminate, it is enough to check whether contexts
distinguish termination, where we note $t \downarrow$ when program $t$
terminates.
\[
t\cong u \quad\triangleq\quad \forall \CC, ~\CC[t]\downarrow ~\Leftrightarrow~ \CC[u]\downarrow
\]

A proof of observational equivalence has the complication of having to deal with
a universal quantification over all contexts.
Abramsky~\cite{Abramsky:1990:LLC:119830.119834} introduced the notion of
applicative bisimilarity as a simpler notion of equivalence and proved
denotationally that it implied observational equivalence by showing that
bisimulation is a congruence. Howe showed how to obtain the same result but
operationally~\cite{Howe:Eq}, thus opening the door for Sands operational theory
of improvements~\cite{Sands:Op:Theories}.


Sands starts with a notion of improvement which, like observational equivalence,
quantifies over all contexts.
\begin{displaymath}
  t \cimproved u \quad{\triangleq}\quad \forall \CC, n \in \N,~ \CC[t]\downarrow_n
  ~\implies~ \forall m \in \N, \CC[U] \downarrow_m \land~ n \geq m
\end{displaymath}

Improvement has the same downside as observational equivalence: to prove an
improvement we need to show that evaluating a term costs more/less than the
other \emph{for every context}. However, the solution is analogous: Sands
defines a notion of improvement simulation and shows, adapting Howe's method,
that it implies improvement.

Recently, \citeauthor{Eff:App:Bisim}~\cite{Eff:App:Bisim} extended Howe's method
to deal with computational effects, where effects are modelled by a monad. The
simulation relation is built around the concept of relator. A relator, given a
way to compare pure values, yields a way to compare effectful values.

The notion of relator give us an opportunity: If we consider cost of evaluation
as a side effect, then we can compare the cost of pure values and obtain an
improvement theory~\cite{Ugo:Norm:Bisim}. Furthermore, if we combine this notion
of effect with other effects, we can compare the cost of effectful programs.
Thus, we obtain an improvement theory for effectful languages.

%%% Talk related stuff
We introduce a simple notion of cost with the writer monad
transformer~\cite{MonadTrans} and the additive monoid of natural numbers. The
writer monad transformer equips terms and values with costs independently of the
particular monad used to modelled effects. Moreover, we lift the inverse natural
order of the natural numbers through relator's composition enabling us to
compare evaluation costs with ease. The result of this transformation is a cost
aware monad and relator. Furthermore, the derived notions of observational
approximation and applicative simulation~\cite{Eff:App:Bisim} of the cost aware
monad and relator, reassemble the notions of improvement and improvement
simulation respectively~\cite{Sands:Op:Theories}.

Following this idea, I will introduce a general theory of improvement for
effectful languages. More concretely, I will show a way to derive notions of
improvement for languages with different concrete effects, such as exceptions,
probability and non-determinism.

\printbibliography%

\end{document}
