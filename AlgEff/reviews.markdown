# Reviews Sc of Programming

Deadline: The due date for revision is Oct 03, 2021.

TODO: We promise to show a non-trivial probabilistic optimization, but are we?

## Reviewer 1

The article focuses on the development of an effectful improvement theory to
provide foundations to program transformations that aims to optimize some
effectful aspects, such as time complexity, i.e., number of steps, store usage,
energy consuming, etc. The proposed theory is based in previous works, as Sands
improvement notion on effectful languages. The subject is very interesting and
relevant. It is hard too. Optimization is hard anyway. The main purpose of the
improvement is to have a better computation without modifying what is computed.
So the improvement is made on the basis of intentional properties of execution
that are observable, such those above mentioned, but preserving the semantics.
The article is aware of some other work where the improvement theory is based on
low level approach such as abstract machines allowing some granularity and
reasoning of the side-effects at this level.

[Martin: General introduction, nothing to be done here.]

The article is well-written in general. I am not a native English speaker, as
far as I can judge, the paper may have typos, I have not found them, indeed. The
article has a very good motivation (section 1). Many examples that help to
understand how the theory can express the improvements regarding many different
side-effects, besides time/energy/storage usage. The idea to use Common
Sub-expression Elimination (CSE) as an improvement when the only observable
effect is non-termination seems to work very well. However, as the authors
report, CSE is a general solution when other effects are present. Finaly, the
article extends the improvement to probabilistic languages and non-trivial
optimizations, including time optimization. The authors claim that they define a
general theory of improvement for effectful languages. I wonder if they can
extend this approach to Quantum Programming Languages, as QPL, initially
proposed by Selinger and afterwards developed by Feng and ali.

[Martin: The reviewer is proposing to mix this approach with Quantum PLs (QPL) proposed
by Selinger, Feng and Ali. We can mention that this technique can be applied to QPL.
I won't say a thing about this...
]
[Mauro: Maybe we can mention that the paper intends to lay the groundwork to start stuying specific instances in depth.
QPL could be one instance, probabilistic computations another. However, studying these instances in depth requires devoting a whole paper to each one.
]

The article shows how to deal with has effectful relational theory of costs that
the authors say that is effective and easy to implement for some simple effects.
However they do not show any practical, or empirical evidence of this
effectiveness.

[Martin: As an answer we can cite more about Sands' paper where CSE is first proved
correct. Or other uses?]
[Mauro: I don't understand what he means. What does it mean to show that they are effective?]

They of course follow the mainstream in semantics when they considers the
difference between "effects" and "values" by the data type itself. not by any
abstraction we use to manipulate it. Taking into account the Maybe monad, the
"effect" is always that a computation can be interrupted even when one uses
Applicative or other Monad interfacing. Monads can express features that, in
some PLs, are side-effects. Thus effectful features are adequately expressed by
these constructions. As a matter of curiosity, a question that might be raised
is why the use of Eilenberg-Moore final solution for monads is not considered
when dealing with effecful monads. The Eilenberg-Moore is closely related to
final semantics that might be used in Final Semantics of side-effects. This is
only a minor suggestion and perhaps it is only curiosity.

[Martin: I don't really know what exactly is the approach taken by Eilenberg-Moore.
Talk to Mauro about this.

Eilenberg-Moore cat is the category of T-Algebras and T-Alg morphisms.
I don't have the intuition about why this approach would be useful, are monads
and algebraic operations better described in this framework?

We just followed the approach taken by Ugo Dal Lago :shrug:.]
[Mauro: I don't get the suggestion either. ]

I have to consider that the text does not touch any other approach, however,
than the Monadic based. There are some investigations on effectful declarative
programming with an emphasis on non-determinism as an effect, that does not
follows the monadic approach, in the context of the Curry language. This might
be mentioned and minimally compared. See for example the work of Oleg Kiselyov.

[Martin: Write something comparing this approach with Oleg's ideas?]
[Mauro: What we want is to write is a generic approach, so we do not want to put emphasis on any particular effect.
Wuld like to know which work by Oleg is he reviewer talking about. Martin, do you know?]
[
Martín: The paper goes into a different line of work. Adding (or hidding) an
accumulative cost mechanism won't work (as well as we did here).
We need the general Monad mechanism (albeit losing some effects), that's why
adding cost (as in the inner cost monad) is so easy.

Also his approach is not operational, we may need to state a tick algebra
to make it work. We do not know anything about how programs are computed.
]

This article discussed a very hard subject, a lot of work has been made in cost
analysis and complexity theory since 1990's. But these works are mainly based
onan automatic/static reasoning about cost embedding the cost into the type
system. Having so, a cost analysis or complexity. The merit of this article is
to report a diverse way by using a relational cost theory, where the global cost
is irrelevant, since what is important is the cost comparison between the
original and improved programs.

Although the text is well-written, I found it too long. Some of the main
results, like theorem 50 and its generalization as theorem 60, might be
explained more intuitively, maybe avoiding many formal defitions that are used
frequently once in the text. Section 2, can be reduced too, but of course this
is a matter of style. Anyway, the paper is 52 pages long. This is almost a book.
We must think how to report results without going into all details. This is not
a complain with the paper , but with our way to reporting results.

[Martin: Shrink sections 2. Thm 50 (and its generalization Thm 60) explain them intuitively.]
[Mauro: Agree with section 2. Maybe agree with Thm 50.]

Finally, I must say that I found the results technically sound and relevant to
be published. I uses Grammarly in some parts of the text and there are some
improvements that can be made in the text. The authors can consider to revise
once more the text regarding the english language.

[Martin: Grammarly?]

## Reviewer 2

Relatively few general results in the paper, and the paper is rather long for
the size of its novel contributions. A number of general constructions are
given, for example for lifting a monad into a cost monad, but they do not always
do the right thing. The proofs of specific improvements are very simple
properties and these are for specific effect monads and not general. These
limitations are however noted by the authors themselves.

[Mauro: The combination of effects is a difficult problem. Even for one given combination there are many ways to
address cost, as shown by the three relators in our non-determinism example. ...
]

The proof methods provided by the paper do not seem to be very useful in
practice - the proof of common subexpression elimination is relegated to the
appendix, and even then it is only a "sketch".

[ Martin: This one exepcts more results. ]
[ Mauro: This is the first paper we know that show
how to do improvement in effectful settings such as probabilistic systems and non-determinism.
Even formally posing the question is difficult. While we do not develop long examples (as pointed out, the paper is already long as it is)
we provide evidence that our methods are enough to prove some interesting improvements. For example, proving that avoiding unnecessary smapling is an improvement may seem like a trivial result, but it has never been done before.
]

Suggested improvements

1. A lot of the focus of preliminaries is on the CPO semantics but I got the
   impression that very little of this is actually used in the paper. The
   authors should review whether all this material is really needed, or whether
   the semantics can be summarised more succinctly.

[Martin: shrink section 2.]

2. In the development of the earlier parts of the paper the authors should
   clearly and systematically cite definitions that are taken from earlier works
   (mostly Dal Lago et al). Again, parts of this development which are not
   needed in detail for the rest of the paper should be shortened and
   summarised.

[Martin: shrink section 3.]

3. The probabilistic improvement example needs better explanation and a more
   relevant example (the current one is just dead code elimination).

[Martin: Okay, I guess I can improve that part... I don't know about a better example though.]
[ Mauro : "Just dead elimination". Indeed, but remarkably in a probabilistic setting, which has never been done before.]

More detailed comments

Sec 3 could benefit from a high level overview - perhaps starting with (cbv)
applicative bisimulation a la Abramsky and use this to explain what needs to be
proven (precongruence) and how it needs to be generalised.

[Added on tex]

p10 Is M to x.N what is usually written as a (non-recursive) let expression? It
would be useful to mention this, as well as the odd syntax which makes values
and terms disjoint.

[Added on tex, after the definition of the language.Explained a bit more why
such a definition is useful, it simplifies the evaluation.
]
[Mauro: values and term disjoint is standard for reviewer 1, but odd for reviewer 2.]

p11 Why a different notation for substitution into a value?? This (gain)
probably comes from using definitions verbatim from earlier work, but in that
case the work should be cited. Explain the M to x.N case - it suggests that x is
not a binding occurrence (not alpha convertible)? I thought you planned to use
the Baradregt convention (in which case x and y are distinct since x is bound
and y is free). The def should be consistent with the def of lambda.

[He's right, added the reference and removed the unnecesary case]

What does V |-> ... mean? You use the notation x |-> ... to denote a semantic
function, where x is a variable denoting the input, but here it seems to be
something different since V metavariable ranging over values.
I can sort of see what you mean here but there must be a nicer way to express
it. Certainly it is not a very "operational" rule, as it has infinitary
branching (we need to quantify over the values V). Since you are basing the cost
semantics on this definition, should you not argue that it is sufficiently
operational for this to make sense?

[Added something to the tex, but I need to talk this one over with Mauro
Martin: I don't think we can do something better here.
]

On p12 you are using V as a regular variable now? Confusing...

[Changed it on tex]

p13 Explain what you mean by similarity being too weak, and what do you do about
it (ignore the problem?)

[Noted on tex]

p14 Existential quantifier not needed in def of R(U)

[Done]

p18 "presented a method to build a precongruence candidate relation starting
from the notion of compatibility clauses."

This is not properly explained, since the concept of "precongruence candidate"
refers to the details of Howe's proof method and does not exist outside the
context of Howe's proof.

[Noted on tex]

p18 of variable -> of variables

[Done]

Example 49

The quantification "for any term context C" needs to be inside the formula
(writing it on the next line makes it look like a top level quantification).
Should it not be restricted to contexts which make the terms closed?

[Done]

The case of non-determinism seems too simple, and does not discuss
nontermination other than in the special case where nontermination is the only
behaviour. This merits some discussion in the paper, with reference to the work
of Lassen, Pitcher or Moran, for example (Sestoft's work is semantically too
outdated to be useful I think).

Lemma 62 Is \sigma^T correct? I guess you need to lift into the cost monad

[Done]

p29

To be clear: the choice not to compare the cost of an exceptional computation is
a feature of the chosen relator. If the language has operations to catch
exceptions then this would not be a good thing to do as you can hide cost. So
what is the story in that case? You treat exceptions as first-class values and
not as effects? What worries me here is that you have no way to know whether you
have a sane definition of a realtor or not. Sondergaard and Sestoft does not
seem like the right reference for discussing the powerdomains. In the context of
observational orders, papers by Lassen or Pitcher would be more appropriate.
Again, the problem here is that you consider nondeterminism without
nontermination (so they do not really match the powerdomain orderings).

Update to comment: the issue with exceptions is discussed much later in the
paper, but without considering the problem caused by introducing exception
handlers. At the very least you should mention this issue upfront in this
section, and consider the implications of having handlers.

[ Martin: Add part of this discussion when presenting the cost of exceptional
computations.
Check out more recent papers Lassen/Pitcher.
Exception handlers are not discussed in this paper :shrug:. ]

5.3 needs some examples and a discussion as to whether the notion of
probabilistic improvement is intuitively useful/interesting in terms of what is
guarantees about improvements. The last paragraph before section 6 tries to do
this but is not so clear ("the sum of its supporting set"?).
I am sure that if I spent more time I could unravel your definition, but I think
there must be a simple intuitive way to present it by unwinding the definitions
a bit more. Perhaps you need to make some basic assumptions about the ability of
contexts to distinguish values in order to be able to simplify the description?

The needless sampling example is not very illustrative as it is just a dead code
elimination. (since x is not free in pb). Maybe this is because the definition
of probabilistic improvement is very limited?

In the common subexpression example, is it possible to characterise a class of
effects for which this is sound? It is a sort of idempotence property. This
would make the example more interesting (assuming that there are more examples
than just termination).

Comment update: I see that you have thoughts along the same lines on p38 - and
that you have not looked at this (a bit disappointing).

[ Martin: We talked about this, maybe we can add some discussion about properties
or possible future work on this. ]

36 pay-head -> pay ahead

[Done]

Statement 72 states an unsound improvement, so should not be written as an
improvement!

Definition 74 - this is the definition of a strict context (c.f. strictness
analysis), which is more general than a reduction context. The comments which
follow your definition are correct for real reduction contexts but are incorrect
for these ones.

Consider the context
C = fix (\f \n if n == 0 then [ - ] else f (n - 1) ) z

This satisfies Def 74, but never "uses" the hole when z is bound to a negative
value. Note that when viewed as a lazy function f y = C[y] then a strictness
analysis will detect that it is strict in y (and GHC would convert it to a CBV
function).

Similarly we can indeed prove that M improved by tick M, precisely when M never
terminates.

[Martin: I think he is right about that... Characterizing reduction contexts in
this way is not the best way...]

[Comment] Disappointing that very simple properties are not so easy to prove -
e.g. lemma 77 where the proof is relegated to an appendix and still only
"sketched".

[Martin: I can add the rest of the cases and removed the word 'sketeched']

Regarding future work, one key point is reasoning about recursively defined
functions. The applicative simulation proofs are limited. It might be worth
mentioning the state of the art in bisimulation proof techniques for effectual
languages (various forms of up-to proofs - see Pous and Sangiorgi 2019 for an
overview. The other (related) direction is to look at the improvement theorem (a
fixed point theorem which makes use of cost to help with the proof) - see e.g.
works of Hutton et al for modern examples.

Ensure that names appearing in paper titles in the bibliography are capitalized.
("howe's method")

[Done?]

## Reviewer 3:

The paper develops a theory of effectful improvement, which is the extension of
Sands' improvement theory to higher-order (sequential) languages with
computational effects. Such an extension is based on the work on effectful
applicative (bi)similarity by Dal Lago, Gavazzo, and Levy and, as such, it
builds upon the notion of a relator for a monad. In a nutshell, effectful
improvement for a computational effect modelled by a monad T is modelled by T(-
x Nat^{\infty}) applicative similarity, where T(- x Nat^{\infty}) is the monad
obtained by post-composing T with the so-called cost monad - x Nat^{\infty}.
The relator for T(- x Nat^{\infty}) is obtained by the one for T together with a
suitable relator for the cost monad, which is essentially given by Sands'
original definition of improvement. Effectful improvement being a form of
effectful applicative similarity, it satisfies many nice structural properties,
viz. it is a precongruence. Moreover, it can be used to reason about program
program behaviours in an intensional fashion. This is witnessed by two examples
studied by the authors: needless sampling remove and common sub-expression
elimination. Finally, another form of effectful improvement based on the outer
cost monad Nat^{\inft} x T(-) is studded, but only in the case of T equals to
the exception monad.

GENERAL COMMENT

The paper is well-written and easy to follow. However, I have some major
concerns about the actual contributions of the work. The form of effectful
improvement presented (to which I will refer to as inner cost improvement) seems
to be obtained as a (not surprising) specific instance of effectful applicative
similarity. In fact, once one observes that the composition of a monad T with
the cost monad is itself a monad (which is well known) and that the same holds
for relators (which, I think, should be quite easy to prove), then one can
simply instantiate effectful applicative similarity to obtain inner cost
improvement. Additionally, such an improvement works fine for some effects, but
not so for others. For instance, inner cost improvement seems to be of little
interest in a probabilistic setting, where one would like to reason in terms of
the expected cost of a computation. That form of improvement is given by what
the authors call the outer cost monad which, however, is treated only with
respect to the exception monad. Defining an effectful improvement of that kind
may require a distributive law of T over the cost monad (is that right?) which
may not exist when T is, e.g., the distribution monad. In my opinion, a theory
of effectful program improvement should give a proper account of outer cost
improvement. For that reasons, I think the authors need to revise their
manuscript by developing the theory of outer cost improvement deeper, as well by
taking into account the technical comments in the next section.

[ Martin: This reviewer suggests to explore the idea of /Outer Cost/ Improvement.
They think that every reasonable notion of cost/improvement should support this notion. ]
[ Mauro: Outer cost monad could be developed more or less generically if we assume that the effect posseses a monad transformer
(note that this is asking for less than a distributive law).]

TECHNICAL COMMENT

- Page 4/Page 1 (counting the first 'almost empty pages', or page 1 otherwise),
  line 49: I think "intentional" should be "intensional". Check that also in the
  rest of the manuscript.

  [Done]

- Page 5/Page 2, line 17: t diverges ----> t converges

  [Done]

- References Lago et al. (2017) and Lago and Gavazzo (2019) should be Dal Lago
  et al. (2017) and Dal Lago and Gavazzo (2019).

  [Done]

- Page 10/Page 7, line 9/10. The position of the comma does not look quite right
  to me

  [Martin: There is a weird sentence 'Otherwise, noted we work in Set' at the beginning of Page 7, is that it?]
  [Mauro: I guess it should be 'Otherwise noted, we work in Set']

- Page 13/Page 10, line 54. I think the right expression is 'variable
  convention' (i.e. singular and without an explicit reference to Barendregt, as
  such a convention was introduced by Thomas Ottmann, see Addenda for the sixth
  imprinting of Barendregt's book).

  [Done]

- Page 14/Page 11, definition of (M to x.N)[y := W]. If you assume variable
  convention, you do not have to do case analysis: (M to x.N)[y := W] = M[y :=
  W] to x.N[y := W] works fine.

- Page 16/Page 13, Definition 26. The expression 'Functor relator' does not
  sound good to me. Relators has been introduced by Kawahara in his 1972 paper
  'Notes on the universality of relational functors', and subsequently studied
  in the Algebra of Programming school (especially by Backhouse and
  collaborators), up to the point in which they entered in the world of
  coalgebra with the PhD by Thijs (1996). In all those works, relators are
  extensions of SET functors to the category REL of sets and relations, hence
  the terminology FUNCT-OR (for functions) ---> RELAT-OR (for relations). Dal
  Lago, Gavazzo, and Levy use the expression relators referring to relational
  extensions of monads, for which one should be more precise and use expressions
  such as T-relators. Relational extension of monads (and functors) have also
  been introduced by Barr in 1970 in the context of categorical topology where
  they go under the name of "lax extension" (shorthand for "lax extension of a
  SET-functor/monad/... to REL"). Since then, lax extensions have been
  extensively studied in categorical topology and nowadays they are a
  cornerstone notion in the filed of Monoidal Topology.

  [Martin: So functor relators are just relators, and monadic extentions should
  be 'T-relators'?
  Discuss this one with Mauro, but it makes sense...
  ]

- Page 17/Page 14. Definition 28. Why coalgebras? These were not introduced
  previously, nor they seem to be relevant for the rest of the paper. If you
  want to prove structural properties of similarity in the abstract (something
  that is not needed, in my opinion), then maybe it would be better to use ATSs.

  [Martín: He is right, I shoudn't mention coalgebras, that is a borrowed mistake.]

- Page 17/Page 14, Line 28. "The proofs are left to the reader". Proving the
  candidate relator for the (sub)distribution monad to be indeed a relator is
  highly nontrivial and usually proofs crucially builds upon Strassen's theorem.

  [Martín: Noted on tex, I think we need to cite someone.]

- Page 21/Page 18, line 10. You have not introduced the notation R([[M]]).

  [Martín: Actually, the definition I gave is wrong! We should thank the reviewer for that too.
  Also, the other definition, \( A \Gamma R B \doteq R(A) \subseteq B\) may be a relator as well.
  ]

- Page 21/Page 18, Definition 42. That is always the case (see, e.g., Remark 6
  in https://arxiv.org/pdf/1704.04647.pdf)

  [Martin: He's right, I should rephrase this part.]

- Page 22/Page 19. I think the standard terminology is "adequate" and not
  "adecuate".

  [Done]

- Page 26/Page 23. Definition 54. The cost relator has been introduced at least
  in [Dal Lago and Gavazzo 2019], although I suspect others introduced it
  previously (but I do not have any reference for that).

  [Added on tex]

- Page 26/Page 23. Lemma 56 to Lemma 61. I think it would be better to have a
  more modular development. Let C be the cost relator and Γ be a relator for a
  monad T. We know that if Φ is a relator for a _functor_ F and Ξ is a relator
  for a functor G, then ΞΦ is a relator for the functor GF. As a consequence, ΓC
  is a relator for the inner cost functor (from which Lemma 56 follows). It
  remains to prove that it is a relator for the inner cost monad as well. Is
  there some general result stating that if two monads compose, then also their
  associated relators do (that should be the case if, for instance, monad
  composition follows from a distributive law)? If so, then Lemma 58 would
  follow from this more general result. Moreover, as previously written, Lemma
  57 follows from Lemma 58.

  [Move it to soft mods]

- Page 29/Page 26. Lemma 62. Fix parentheses in the definition of [[M to x.N]].
  Also, the constructors "return" and "- to x.-" are in italic, whereas
  previously they were in bold.

  [What parentheses? Italics removed]

- Page 37/Page 34. The remove needless sampling transformation seems to have
  little to do with sampling. To me, it seems an instance of the transformation
  M to x.N ≅ N, for x ∉ FV(N) This is sound for some effects (such as
  probabilistic nondeterminism) and unsound for others (such as output). Do we
  have a general theorem stating that if the above transformation is sound for a
  monad T, then it gives an improvement for the inner cost monad with respect to
  T?

## To Discuss

- Page 16/Page 13, Definition 26. The expression 'Functor relator' does not
  sound good to me. Relators has been introduced by Kawahara in his 1972 paper
  'Notes on the universality of relational functors', and subsequently studied
  in the Algebra of Programming school (especially by Backhouse and
  collaborators), up to the point in which they entered in the world of
  coalgebra with the PhD by Thijs (1996). In all those works, relators are
  extensions of SET functors to the category REL of sets and relations, hence
  the terminology FUNCT-OR (for functions) ---> RELAT-OR (for relations). Dal
  Lago, Gavazzo, and Levy use the expression relators referring to relational
  extensions of monads, for which one should be more precise and use expressions
  such as T-relators. Relational extension of monads (and functors) have also
  been introduced by Barr in 1970 in the context of categorical topology where
  they go under the name of "lax extension" (shorthand for "lax extension of a
  SET-functor/monad/... to REL"). Since then, lax extensions have been
  extensively studied in categorical topology and nowadays they are a
  cornerstone notion in the filed of Monoidal Topology.

  [Martin: So functor relators are just relators, and monadic extentions should
  be 'T-relators'?
  Discuss this one with Mauro, but it makes sense...

  Done it
  ]

- Page 37/Page 34. The remove needless sampling transformation seems to have
  little to do with sampling. To me, it seems an instance of the transformation
  M to x.N ≅ N, for x ∉ FV(N) This is sound for some effects (such as
  probabilistic nondeterminism) and unsound for others (such as output). Do we
  have a general theorem stating that if the above transformation is sound for a
  monad T, then it gives an improvement for the inner cost monad with respect to
  T?

  [
  As mentioned by the other review, needless sampling is just dead code elimination...
  Maybe we should do something more interesting and try to prove /unobservable code elimination/.
  If M to x.N ≅ N not in FV(N), that means that the effects produced by the evaluation of M are not observed
  ]

## Not so hard things:

- Page 21/Page 18, Definition 42. That is always the case (see, e.g., Remark 6
  in https://arxiv.org/pdf/1704.04647.pdf)

  [Martin: He's right, I should rephrase this part.]

  [Noted in tex]

- Page 26/Page 23. Lemma 56 to Lemma 61. I think it would be better to have a
  more modular development. Let C be the cost relator and Γ be a relator for a
  monad T. We know that if Φ is a relator for a _functor_ F and Ξ is a relator
  for a functor G, then ΞΦ is a relator for the functor GF. As a consequence, ΓC
  is a relator for the inner cost functor (from which Lemma 56 follows). It
  remai ns to prove that it is a relator for the inner cost monad as well. Is
  there some general result stating that if two monads compose, then also their
  associated relators do (that should be the case if, for instance, monad
  composition follows from a distributive law)? If so, then Lemma 58 would
  follow from this more general result. Moreover, as previously written, Lemma
  57 follows from Lemma 58.

  [
  Monad composition does not follow a modular approach, so it doesn't make sense
  to do that here, I think the reviewer knows that and tries to solve it by
  suggesting we derive composition from a distributive law?

  Lemma 57 follows from Lemma 58, but Lemma 58 uses lemma 57 (that's why is proven there)...

  ***

  Changed a bit the presentation, showed the same proofs in a generic monoid with a preorder.
  Check it out.
  ]

- Page 37/Page 34. The remove needless sampling transformation seems to have
  little to do with sampling. To me, it seems an instance of the transformation
  M to x.N ≅ N, for x ∉ FV(N) This is sound for some effects (such as
  probabilistic nondeterminism) and unsound for others (such as output). Do we
  have a general theorem stating that if the above transformation is sound for a
  monad T, then it gives an improvement for the inner cost monad with respect to
  T?

  [
  As mentioned by the other review, needless sampling is just dead code elimination...
  Maybe we should do something more interesting and try to prove /unobservable code elimination/.
  If M to x.N ≅ N not in FV(N), that means that the effects produced by the evaluation of M are not observed.

  I think it should be doable, I still need some more properties (and intelligence) to come up with those proofs.
  Observational properties (or lemmas) are bad hypothesis.

  ***

  Prove that \((M to x . N) \sqsubseteq N \implies (M to x.N) \succeq N\)
  And that in the probabilistic setting it is always the case that \(M to x. N \sqsubseteq N\)
  ]

## Harder things:

- The case of non-determinism seems too simple, and does not discuss
  nontermination other than in the special case where nontermination is the only
  behaviour. This merits some discussion in the paper, with reference to the work
  of Lassen, Pitcher or Moran, for example (Sestoft's work is semantically too
  outdated to be useful I think).

  [
  This is only the beginning, and this method is general enough to work. But there are some particular cases when this method can be improved.
  ]

- Sondergaard and Sestoft does not seem like the right reference for discussing
  the powerdomains. In the context of observational orders, papers by Lassen or
  Pitcher would be more appropriate. Again, the problem here is that you
  consider nondeterminism without nontermination (so they do not really match
  the powerdomain orderings).

[
Martín: They are right, it is old but it is the only one following our approach.
We interpret non-determinism as the set of all possible values.
One reviewer say it right: we consider nondet without nondet.
However, the notions stated by Lassen and Pitcher are exactly the same:

Take M,N : P(\tau), that means that M,N are both effectful expressions that when run they give you a \(\tau\) value.

Lower notion M<R>N <-> \forall M1, M \Downarrow return M1 \implies \exists N1, N \Downarrow return N1 /\ M1 R N1
Upper notion M<R>N <-> \forall N1, not(M may diverge) /\ not(N may diverge) => N \Downarrow return N1 \implies \exists M1, M \Downarrow return M1 /\ M1 R N1

The big difference with their language is that monadic terms are introduced a
bit differently, and we have a generic way of introducing new effects.

Here we can say that when treating just one kind of effect you can get more specific proofs, or properties, or theory :shrug:.

]

- Exception system: consider the implications of having handlers ??
  [
  Martín: Having handlers is not our subject here.
  ]

- 5.3 needs some examples and a discussion as to whether the notion of
  probabilistic improvement is intuitively useful/interesting in terms of what is
  guarantees about improvements. The last paragraph before section 6 tries to do
  this but is not so clear ("the sum of its supporting set"?).
  I am sure that if I spent more time I could unravel your definition, but I think
  there must be a simple intuitive way to present it by unwinding the definitions
  a bit more. Perhaps you need to make some basic assumptions about the ability of
  contexts to distinguish values in order to be able to simplify the description?

The needless sampling example is not very illustrative as it is just a dead code
elimination. (since x is not free in pb). Maybe this is because the definition
of probabilistic improvement is very limited?

[
Martín: Solved this one adding unobservable code elimination
]

In the common subexpression example, is it possible to characterise a class of
effects for which this is sound? It is a sort of idempotence property. This
would make the example more interesting (assuming that there are more examples
than just termination).
[
Martín:
-- We leave these ones out of our example.
-- Maybe we can re-do the other trick but I don't think it makes any sense here.
]

- Definition 74 - this is the definition of a strict context (c.f. strictness
  analysis), which is more general than a reduction context. The comments which
  follow your definition are correct for real reduction contexts but are incorrect
  for these ones.

Consider the context
C = fix (\f \n if n == 0 then [ - ] else f (n - 1) ) z

This satisfies Def 74, but never "uses" the hole when z is bound to a negative
value. Note that when viewed as a lazy function f y = C[y] then a strictness
analysis will detect that it is strict in y (and GHC would convert it to a CBV
function).

Similarly we can indeed prove that M improved by tick M, precisely when M never
terminates.

[
They are right, I changed this definition on text.
]

- Regarding future work, one key point is reasoning about recursively defined
  functions. The applicative simulation proofs are limited. It might be worth
  mentioning the state of the art in bisimulation proof techniques for effectual
  languages (various forms of up-to proofs - see Pous and Sangiorgi 2019 for an
  overview.
  [
  Martin: I tried to applied some Up-To Proofs, but I need to revisit that
  I can expand the future work section with this.
  ]

- The other (related) direction is to look at the improvement theorem (a
  fixed point theorem which makes use of cost to help with the proof) - see e.g.
  works of Hutton et al for modern examples.

  [Martin: Yeah, I've tried that but I am not smart enough... I think one
  solution would be to have a fix operator, or maybe some fixed set of recursive
  definitions.
  Or something else, idk.
  I should revisit other theories too.
  It would be much easier to have a monadic fix operator (Lassen and Pitchet
  have one, I don't know if Moran has one too).
  We can add types and expand the language a bit to prove interesting things on
  it, but that is again outside of our scope.
  ]

- In my opinion, a theory of effectful program improvement should give a proper
  account of outer cost improvement. For that reasons, I think the authors need
  to revise their manuscript by developing the theory of outer cost improvement
  deeper, as well by taking into account the technical comments in the next
  section.

  [
  It is not so clear for me... For example, an outer notion of cost mixed with
  probabilistic programs doesn't make so much sense, they even treat divergent
  computations differently.
  Remember that we need an ordered monad, and I don't know how to do that.

  ]
