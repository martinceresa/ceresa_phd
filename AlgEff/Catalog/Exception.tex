\section{Exception Calculus}

As more complex but small example it is possible to add one kind of
distinguishable effect which introduces exceptions to the language.

Assuming there is a set \(E \in \Set \) of labels signaling different
exceptions. Basically \(E\) is a set of tokens.

Similar to the pure calculus, since non termination is an observable effect we
need to add another token to signal non termination.
%%
But, since we can combine effects we are to develop the theory needed for
exceptions and combine it latter with the partiality monad used before.

\begin{definition}[Exceptions]
  \(
  \Sigma_{E} \triangleq  E; \alpha_E = 1
  \)
\end{definition}

Now we have a language with exceptions defined as follows:

\begin{definition}[Exceptional Calculus]
  Lambda Calculus with Exceptions grammar
\begin{bnf*}
  \bnfprod{M,N}
  {
    \bnfts{\textbf{return}} \bnfsp \bnfpn{V}
    \bnfor \bnfpn{V} \bnfsp \bnfpn{W}
    \bnfor \bnfpn{M} \bnfsp \bnfts{\textbf{to} x.} \bnfpn{N}
    \bnfor \bnfts{\(e\)} % \bnfts{\(, e \in E\)}
  } \\
  \bnfprod{V,W}
  {
    \bnfts{x}
    \bnfor \bnfts{\(\lambda x.\)} \bnfpn{M}%
  }
\end{bnf*}
Where \(e \in E\), and variables in \(Var\).
\end{definition}

Exceptions by themselves form a functor, has monadic structure and posses a
rather simple relator.

\begin{definition}[Exceptions Math]
  \begin{align*}
    T_E X &\triangleq X + E \\
    \eta &: X -> T_E X \\
    \eta(x) &= in_l (x) \\
    f^E &: T_E X -> T_E Y\\
    f^E (x) &=
              \begin{cases}
                f(y) & if \; x = in_l(y) \\
                x & if \; x = in_r(e) \\
                \end{cases} \\
    [[e]] &= in_r(e), e \in E
    \end{align*}
  \end{definition}

Now we can compose Exception Monad with Partiality monad used before.
But here a known limitation of monad composition arises, clearly monadic
composition is not commutative and here it seems that we have two options.
%%
Since it is required to deal with non-termination explicitly at an outer level,
we need to check first if a program terminates and then check if it is an
exception or a value. Following this reasoning what we need is to compose
partiality monad with exception monad:
\[T X \triangleq (X + E)_{\bot} \equiv ((\__{\bot}) \circ (\_ + E)) X \]
Therefore define a monadic evaluation relation, \([[ - ]] : \T -> T \V\) as
two monads composition. Such definition reuse the computational model
implemented before but it also enable the use of exception in the evaluation.

\begin{note}
  There is another option which is to add a different exception which represents
  a non-terminating program, \(E_\bot \triangleq E \cup \{\bot\}\). But
  following the processes mentioned above we are not required to derive the
  computational model again.
  \end{note}

\begin{definition}[Exception Relator]
  Given a relation \(R \subseteq X \times Y\), define \(\Gamma_E R \subseteq T_E
  X \times T_E Y\) as follows:
  \[
    \mkRel{x}{\Gamma_E R}{y} \iff
    \begin{cases}
      x = in_l(x')  \implies y = in_l(y') \land \mkRel{x'}{R}{y'} \\
      x = in_r(e_x) \implies y = in_r(e_y) \land e_x = e_y
    \end{cases}
  \]
  \end{definition}

\begin{note}
  Exceptions are just tokens and do not carry any information, for now. There may be
  cases when they carry some information and may be compared by some relation.
  It is something to point out since I may relate exceptions later.
  \end{note}

Relators are closed under composition, therefore is possible to compose
partiality relator with exception relator and obtain a relator for monad \(T\).
% Since we compose partiality with exception, we are going to do the same with
% theirs relators.

\begin{definition}[Relator for \(T\)]
  \(\Gamma_T R \triangleq (\Gamma_{\bot} \circ \Gamma_E) R \equiv
  \Gamma_{\bot}(\Gamma_E R)\), which can be expanded as:
  \[
    \mkRel{x}{\Gamma_T R}{y} \iff
    \begin{cases}
      x = \bot \\
      x = in_l(x') \implies y = in_l(y') \land \mkRel{x'}{R}{y'} \\
      x = in_r(e_x) \implies y = in_r(e_y) \land e_x = e_y
    \end{cases}
  \]
  \end{definition}

Such a relator indicates that first it is observed if a given computation
terminates and then if it is an exception or are related values. Clearly this is
exactly what is going to observe/happen when applicative simulation is defined.

\subsection{Applicative Simulation with Exceptions}

Applicative simulation instantiated for this calculus give us back the following
equations, \(S = (S_{\T}, S_{\V})\) is a simulation if:
\[
  \mkRel{M}{S_{\T}}{N} \implies
  \begin{cases}
    [[M]] = \bot_{T_E V} \\
    [[M]] = v \implies [[N]] = w \land \mkRel{v}{\Gamma_E S_{V}}{w}
  \end{cases}
\]
Which by definition of \(\Gamma_E\) is expanded to:
\[
  \mkRel{M}{S_{\T}}{N} \implies
  \begin{cases}
    [[M]] = \bot_{T_E V} \\
    [[M]] = in_l(v)  \implies [[N]] = in_l(w) \land \mkRel{v}{S_{V}}{w} \\
    [[M]] = in_r(e)  \implies [[N]] = in_r(e') \land e = e'
  \end{cases}
\]

Expand it completely here does not give any particular insight, so I leave it as
it is. One thing to remember is that \(\mkRel{V}{S_{V}}{W} \implies \forall U
\in V, \mkRel{V U}{S_{\T}}{W U}\)

\subsection{Contextual Approximation}

Based on our observations dictated by the relators we have that:
\[\mkRel{M}{\rctxImp[E]}{N} \implies (\forall C \in Ctx,
\begin{cases}
  [[ C[M] ]] = in_l(\_) \implies [[ C[N] ]] = in_l(\_) \\
  [[ C[M] ]] = in_r(e) \implies [[ C[N] ]] = in_r(e) \\
  [[ C[M] ]] = \bot_{T_E V} \\
\end{cases}
)
\]

Given two terms \(M,N\), \(M\) is contextually approximated by \(N\) if for
every context \(C\), when \(C[M]\) reaches a value it also does \(C[N]\) and
when \(C[M]\) reaches an exception \(C[N]\) reaches the same one.

It is saying that in every context, \(N\) behaves similar to \(M\) but taking
specially care when an exception is raised. Since they both have two raise the
same exception.

\subsection{Introducing Costs}

Following the methodology used before one way is to use the \emph{Writers monad transformer}.

Define a new signature \(\Sigma^C_E \equiv \Sigma_E \cup \Sigma_{\tick}\).
And \(Cost \; T \; X \equiv T (X \times \N) \equiv ((X \times \N) + E)_{\bot}\).
%%
Such monad made it possible to observe costs whenever a computation is perform
without producing an exception.
%%
In other words, we cannot compare the cost of
computations that produce an exception.
%%

For example: Given \(e \in E\), \[ [[ \LTo{\tick[25]}{x.e} ]]^c = in_r(e)
  = [[ e ]]^c \]
Where clearly one expression consumes more computational cost than the other.
%%

\begin{note}
Another solution left for further research could be to explore some kind of \emph{Strong}
notion and combine it \(((T X) \times \N)_{\bot} \equiv (T (X \times
\N))_{\bot}\).
%%
In particular \( ((X + E)\times \N)_{\bot} \equiv ((X \times \N) + (E \times \N))_{\bot}\)
\end{note}

Applicative Cost Simulation is a relation \(S =(S_\T, S_\V)\):
\[
  \mkRel{M}{S_{\T}}{N} \implies
  \begin{cases}
    [[M]]^c = \bot_{T_E V} \\
    [[M]]^c = in_l(v,n)  \implies [[N]]^c = in_l(w,m)
    \land (n \geq m)
    \land \mkRel{v}{S_{V}}{w} \\
    [[M]]^c = in_r(e)  \implies [[N]]^c = in_r(e') \land e = e'
  \end{cases}
\]

And the proposed contextual cost approximation derived is:
\[\mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
\begin{cases}
  [[ C[M] ]]^c = in_l(\_, n) \implies [[ C[N] ]]^c = in_l(\_,m)
             \land (n \geqInf m) \\
  [[ C[M] ]]^c = in_r(e) \implies [[ C[N] ]]^c = in_r(e) \\
  [[ C[M] ]]^c = \bot_{T V} \\
\end{cases}
)
\]

Both relations compare the cost of computations who reach values without
producing exceptions.
And of course we have that, applicative cost simulation implies observational
cost approximation.

\subsection{Observable Cost Exceptions}

As mentioned above there is another way to introduce costs in computations with
exceptions. It is based composing the other way around Exception Monad with
Writer's Monad.

Define \(CE X \triangleq (X + E) \times \N\). In this particular case such
composition is in fact a monad and also has a relator.
Given \(R \subseteq X \times Y\), define \(\Gamma_{CE} R \subseteq CE \; X \times
CE \; Y\) as:
\[
  \mkRel{(x,n)}{\Gamma_{CE} R}{(y,m)} \iff \mkRel{x}{\Gamma_E R}{y} \land n
  \geqInf m
\]

As result an applicative cost relation is \(S = (S_\T, S_\V)\):
\[
  \mkRel{M}{S_\T}{N} \implies \mkRel{[[ M ]]}{( \Gamma_E S_\V \times (\geqInf))}{ [[ N ]] }
\]

More important the new improvement definition is:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies \forall C \in Ctx, [[ C [M] ]] = (v_m, c_m)
  \implies [[ C[N] ]] = (v_n,c_n) \land \mkRel{v_m}{\Gamma_E \UU}{v_n} \land c_m
  \geqInf c_n
\]
which is equivalent to
\[
  \mkRel{M}{\rctxImp[]}{N} \implies
  \forall C \in Ctx,
  \begin{cases}
    [[ C[M] ]] = (in_l(\_), c_m) \implies [[ C[N] ]] = (in_l(\_), c_n)
    \land c_m \geqInf c_n \\
    [[ C[M] ]] = (in_r(e_m), c_m) \implies [[ C[N] ]] = (in_r(e_n), c_n)
    \land e_m = e_n \land c_m \geqInf c_n
    \end{cases}
\]

Term \(M\) is improved by term \(N\) if for any given context whenever \(C[M]\)
reaches a value \(C[N]\) also does with no greater cost, or whenever \(C[M]\)
reaches an exception again also does \(C[N]\) with no greater cost.

That means that \(N\) follows closely how behave \(M\) without involving
greater costs.