\section{No Deterministic Model}

In this section an operator modeling non-determinism is introduced.

\begin{definition}[Non-deterministic Op]
  \(\Sigma_{\oplus} \triangleq \{\oplus\}, \alpha(\oplus) = 2\).
  \end{definition}

Given two terms \(M,N \in \T\), \((M \oplus N)\) may behave as \(M\) or as
\(N\). As this section develops I am going to explain that in fact we may make
some guesses to build a more uniform non determinism where we would like to
choose some behaviors above others.

\begin{definition}[Lambda Calculus with Non-deterministic choice]
\begin{bnf*}
  \bnfprod{M,N}
  {
    \bnfts{\textbf{return}} \bnfsp \bnfpn{V}
    \bnfor \bnfpn{V} \bnfsp \bnfpn{W}
    \bnfor \bnfpn{M} \bnfsp \bnfts{\textbf{to} x.} \bnfpn{N}
    \bnfor \bnfpn{M} \bnfsp \bnfts{\(\oplus\)} \bnfsp \bnfpn{N}
  } \\
  \bnfprod{V,W}
  {
    \bnfts{x}
    \bnfor \bnfts{\(\lambda x.\)} \bnfpn{M}%
  }
\end{bnf*}
\end{definition}

A well establish monad to model non-deterministic behavior is the
\emph{power set} monad. Here I implement a \emph{finite power set monad} to stay
safe since its coalgebraic construction may be needed.
The ideas is every possible behavior is accumulated
in a set. Henceforth, evaluation does not reach a single value but every
possible value that could had been evaluated depending on the use of the
internal choice operator described above. An intuitive way to describe such
evaluation at runtime is as a \emph{tree} where every time a choice is made a node is
created with some outgoing paths assuming each of the choices made. Following
this idea at the end of a given evaluation all leafs are collected and a set of
values is given as result.

A simple model is
\begin{definition}[Non determinism Math]
  \begin{align*}
    T_P X &\triangleq \PP_{fin}(X) \\
    &\\
    T_P(f) &: T_P(X) -> T_P(Y) \\
    T_P(S) &=  \bigcup_{x \in S} \{f(x)\} \\
    &\\
    \eta &: X -> T_P X \\
    \eta(x) &= {x} \\
    &\\
    f^{\dagger} &: T_P X -> T_P Y\\
    f^{\dagger}(S) &= \bigcup_{x \in S} f(x) \\
    &\\
    [[M \oplus N]] &= [[M]] \cup [[N]]
    \end{align*}
  \end{definition}

Such model introduces non determinism simply as a set of possible reachable
values, but it has a caveat related to non termination. Presented simply as the
power set of terms, the only way to indicate that a program do not evaluate to a
value is by assigning it the empty set. In other words every non terminating
program \emph{evaluates} to the empty set. The problem with assigning the empty
set to those programs is that since we are using the union to interpret
\(\oplus\) implicitly I am ignoring non terminating programs along the way. In
other words I am taking place in heaven with Angelic evaluation. I can ignore
this (as Ugo dal Lago did) and try to develop some kind of analysis deriving
lemmas and improvement notions with different relators but I am not sure I am
doing the right thing.

Which gives place to define the following three relators:
\begin{definition}[Hoare's Non determinism Relator]\label{def:non-det-rel}
  Given a relation \(R \subseteq X \times Y\), \(\Gamma_HR \subseteq T_P X
  \times T_P Y\) is defined as follows:
  \[\mkRel{S}{\Gamma_HR}{T} \iff \forall x \in S, \exists y \in T, \mkRel{x}{R}{y} \]
  \end{definition}
Such definition could be rewritten as
  \[\mkRel{S}{\Gamma_HR}{T} \iff S \subseteq R^{-1}(T) \]
  
\begin{definition}[Smyth's Non determinism Relator]\label{def:S:non-det-rel}
  Given a relation \(R \subseteq X \times Y\), \(\Gamma_SR \subseteq T_P X
  \times T_P Y\) is defined as follows:
  \[\mkRel{S}{\Gamma_PR}{T} \iff \forall y \in Y, \exists x \in S, \mkRel{x}{R}{y} \]
  \end{definition}
And a more readable version is
  \[\mkRel{S}{\Gamma_PR}{T} \iff T \subseteq R(S) \]

\begin{definition}[Plotkin's Non determinism Relator]\label{def:P:non-det-rel}
  Given a relation \(R \subseteq X \times Y\), \(\Gamma_PR \subseteq T_P X
  \times T_P Y\) is defined as follows:
  \[\mkRel{S}{\Gamma_PR}{T} \iff \mkRel{S}{\Gamma_HR}{T} \land \mkRel{S}{\Gamma_SR}{T}\]
  \end{definition}
Following set definitions presented above we get
  \[\mkRel{S}{\Gamma_PR}{T} \iff (S \subseteq R^{-1}(T)) \land (T \subseteq R(S)) \]

This is the first case where there are three different relators for the very
same monad. Each one give different way to compare programs and even better, to
compare costs.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Fun section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Applicative Simulation with Non deterministic choice}

From applicative simulation definition a relation \(S = (S_\T , S_\V)\) is a
simulation if satisfies:
\begin{align*}
  \mkRel{M}{S_\T}{N} &\implies \mkRel{[[M]]}{\Gamma S_\V}{[[N]]} \\
  \mkRel{V}{S_\V}{U} &\implies \forall W \in \V, \mkRel{(V W)}{S_\T}{(U W)}
  \end{align*}

Since we have three relators we have three different definitions.

\begin{definition}[Hoare's Applicative Simulation]
  An applicative simulation is a Hoare's applicative simulation if
  \begin{align*}
    \mkRel{M}{S_\T}{N} &\implies \mkRel{[[M]]}{\Gamma_H S_\V}{[[N]]} \\
                       &\implies [[M]] \subseteq S_\V^{-1} ([[N]])
    \end{align*}
  \end{definition}
A term \(M\) is simulated by a term \(N\) if every value reachable by \(M\) is
related to some value reachable by \(N\).

\begin{definition}[Smyth's Applicative Simulation]
  An applicative simulation is a Smyth's applicative simulation if
  \begin{align*}
    \mkRel{M}{S_\T}{N} &\implies \mkRel{[[M]]}{\Gamma_S S_\V}{[[N]]} \\
                       &\implies [[N]] \subseteq S_\V([[M]])
    \end{align*}
  \end{definition}
Smyth relation goes in the opposite sense of Hoare relation. That is \(M\) is
simulated by \(N\) if every value reachable by \(N\) is related to some value
reachable by \(M\).

\begin{definition}[Plotkin's Applicative Simulation]
  An applicative simulation is a Plotkin's applicative simulation if
  \begin{align*}
    \mkRel{M}{S_\T}{N} &\implies \mkRel{[[M]]}{\Gamma_P S_\V}{[[N]]} \\
                       &\implies ([[M]] \subseteq S_\V^{-1} ([[N]])) \land ([[N]] \subseteq S_\V([[M]]))
    \end{align*}
  \end{definition}
Lastly, Plotkin simulation tight the gap asking for both senses, Smyth's and
Hoare's. Saying that \(M\) is simulated by \(N\) if every value reachable by
\(M\) is related to a value reachable by \(N\) and the other way around.
  
% \begin{align*}
%   \mkRel{M}{S_\T}{N} &\implies \forall v_m \in [[M]], \exists v_n \in [[N]],
%                        \mkRel{v_m}{S_\V}{v_n} \\
%   \mkRel{V}{S_\V}{U} &\implies \forall W \in \V, \mkRel{(V W)}{S_\T}{(U W)}
%   \end{align*}

% Which follows the tradition of applicative simulation, which says that if \(M\)
% evaluates to a value \(v_m\), then it also does \(N\) to a value \(v_n\) and
% both values are related, \(\mkRel{v_m}{S_\V}{v_n}\).

\subsection{Contextual Approximation}

Same as before I will develop three definitions of contextual approximation,
each one derived from each relator presented above.

Remember that contextual approximation is defined as the biggest compatible
adequate relation over terms, which give the following definition.
\[\mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \mkRel{[[C[M]]]}(\Gamma_*\UU){[[N]]} ) \]

In the rest of this subsection, \(\Gamma_*\) will be replaced by Hoare's
(\(\Gamma_H\)), Smyth's(\(\Gamma_S\)) and Plotkin's(\(\Gamma_P\)) relator, each
one presenting a different notion of Contextual Approximation.

Each relator is going to present us with a different notion of observational
approximation, in fact we are going to have two different notions, Hoare's and
Smyth's, while Plotkin's is just a conjunction of the other two.

\subsubsection{Hoare's Observational Approximation}
Hoare's observational approximation is defined in terms of its relator \(\Gamma_H\)
Starting from observational approximation definition we get that:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \mkRel{[[C[M]]]}{\Gamma_H\UU}{[[C[N]]]})
\]
Unfolding \(\Gamma_H\) definition we get:
\[\mkRel{M}{\rctxImp[]}{N} \implies
  (\forall C \in Ctx, [[C[M]]] \subseteq \UU^{-1}([[C[N]]]))
\]

As a hint here, there is an intermediate step which is in fact what is derived
directly from the equation above, which says if \([[C[N]]]\) does not
terminate then neither does \([[C[M]]]\):
\[
  \mkRel{M}{\rctxImp[]}{N} \implies
  (\forall C \in Ctx, [[C[N]]] = \emptyset \implies [[C[M]]] = \emptyset)
\]
So by transposition we get the following rule.

And finally since the relation \(\UU\) relates every value with every value,
we can only ask for:
\[\mkRel{M}{\rctxImp[]}{N} \implies
  (\forall C \in Ctx, [[C[M]]] \neq \emptyset \implies [[C[N]]] \neq \emptyset)
\]

As result we can say a term \(M\) is approximated by another term \(N\) whenever
in every context \(C\), if \(C[M]\) terminates, has a non-empty set of possible
result values, \(C[N]\) also terminates.

\subsubsection{Smyth's Observational Approximation}
Instead of using Hoare's relator in this section Smyth's relator is used instead.
Remember that Smyth's relator place the dependency in the other direction of
Hoare's relator. Hoare's relator says that for every element of the parting set
there is a related one in the destination set, while Smyth's relator says that
for every element in the destination set, there is one in the parting set.

Starting from observational approximation definition we get that:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \mkRel{[[C[M]]]}{\Gamma_S\UU}{[[C[N]]]})
\]
And applying \(\Gamma_S\) definition we get:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  [[C[N]]] \subseteq \UU([[C[M]]])
  )
\]
Following the same reasoning as before we have then:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  [[C[M]]] = \emptyset \implies [[C[N]]] = \emptyset
  )
\]

Instead of asking for termination, Smyth's observational approximation compare
terms based on its non termination behavior. A term \(M\) is
approximated by another term \(N\) if for any given context \(C\), when \(C[M]\)
does not terminates, neither does \(C[N]\).

\subsubsection{Plotkin's Observational Approximation}

Plotkin's observational approximation, defined by its relator, is simply the
conjunction between Hoare's and Smyth's observational approximation. A term
\(M\) is approximated by another term \(N\) if for any context \(C\), \(C[M]\)
behaves exactly has \(C[N]\), either by terminating or non terminating. This is
in some sense related to the observational equivalence definition of Felleisen.

Starting from observational approximation definition we get that:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \mkRel{[[C[M]]]}{\Gamma_P\UU}{[[C[N]]]})
\]
Which by definition is
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \mkRel{[[C[M]]]}{\Gamma_H\UU}{[[C[N]]]}
  \land
  \mkRel{[[C[M]]]}{\Gamma_S\UU}{[[C[N]]]}
  )
\]
Following the definitions obtained above
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  [[C[N]]] = \emptyset \implies [[C[M]]] = \emptyset
  \land
  [[C[M]]] = \emptyset \implies [[C[N]]] = \emptyset
  )
\]
Which we can rewrite it as
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  [[C[N]]] = \emptyset \iff [[C[M]]] = \emptyset
  )
\]
Explicitly saying that \(C[N]\) does not reach any value if and only if \(C[N]\)
does not reach any value either.
And also by transposition
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  [[C[N]]] \neq \emptyset \iff [[C[M]]] \neq \emptyset
  )
\]
Saying that \(C[N]\) terminates if and only if \(C[M]\) also does.

\subsection{Introducing Costs}

Following the previous sections, the time has come to introduce the notion of
cost. Non determinism presents me with a wonderful example to show the power of
the idea, since it is the only case where three different relators could be
applied. Same as before, I am going to derive notions of Cost Applicative
Simulation and Improvement for each one of the possible relators.

First, I redefine our model of computation. Instead of computing inside \(\PP\),
the new cost model is \(Cost T_P X \equiv T_P(X \times \N) \equiv \PP(X \times \N)\).
In this model, each term \(M\) evaluates to a set \([[M]] \in \PP(\V \times
\N)\) containing possible values reachable by \(M\) and the cost needed to reach them.

Introducing cost following this ideas enable us to reuse all the machinery
elaborated in the previous sections. I am entitle to use \([[ \_ ]]^c : \T ->
\PP(\V \times \N)\) directly.

For each relator \(\Gamma_{\mathcal{R}}\) with \(\mathcal{R} \in \{ H, S, P\}\),
defined the following cost relator:
\[
  \Gamma^c_{\mathcal{R}} R = \Gamma_{\mathcal{R}}(R \otimes (\geqInf))
\]

For each relator I am going to derive a notion of cost. Following the scheme
presented in the previous section, and since Plotkin's relator is the
conjunction of the other two, I am going to start with Hoare's relator,
following with Smyth's relator and join them at the end to explain Plotkin's
relator just as the conjunction of the other two notions.

\subsubsection{Hoare's Cost Relations}
Applicative Cost simulation is then a relation \(S = (S_\T, S_\T)\) satifying
the following:
\begin{align*}
  \mkRel{M}{S_\T}{N} &\implies \mkRel{[[M]]^c}{\Gamma^c_{H}S_\V}{[[N]]^c} \\
  \mkRel{V}{S_\V}{U} &\implies \forall W \in \V, \mkRel{VW}{S_\T}{UW}
  \end{align*}
Since the only place where the relator comes into play is in the term part of
the relation I am going to concentrate there.
Again applying \(\Gamma^c_{H}\) definition we get:
\[
  \mkRel{M}{S_\T}{N} \implies \mkRel{[[M]]^c}{\Gamma_{H}(S_\V \otimes (\geqInf))}{[[N]]^c}
\]
And by \(\Gamma_H\) definition and going back to a more explicit version:
\[
  \mkRel{M}{S_\T}{N} \implies \forall (m,m_c) \in [[M]]^c, \exists (n,n_c) \in
  [[N]]^c, \mkRel{(m,m_c)}{(S_\V \otimes (\geqInf))}{(n,n_c)}
\]
And finally unfolding relation product:
\[
  \mkRel{M}{S_\T}{N} \implies \forall (m,m_c) \in [[M]]^c, \exists (n,n_c) \in
  [[N]]^c, \mkRel{m}{S_\V}{n} \land \mkRel{m_c}{\geqInf}{n_c}
\]
Which, as I wanted, says same as before \emph{but} adding a constraint about
cost. In words the idea is that not only it terminates but there is at least a
related value in \(N\) with a better (no worse) cost.

Improvement is defined through the notion of observational approximation which
in this case is:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \mkRel{[[C[M]]]^c}{\Gamma_H\UU}{[[C[N]]]^c})
\]
Which translates to
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \forall (m,m_c) \in [[C[M]]]^c,
  \exists (n,n_c) \in [[C[N]]]^c, \mkRel{m}{\UU}{n} \land m_c \geqInf n_c
  )
\]
And since \(\UU\) relates every value with any other value it leave us with the
notion of that \(M\) is improved by \(N\) whenever there is a better \emph{case}
in \(N\). In other words, \(N\) reaches at least one value in lesser or equal
cost than \(M\).

\begin{example}
It can be seen that \(M \triangleq \tick[5] I\) is improved by \(N \triangleq
\tick[7] I \oplus I\), \(M \geq N\). Since we can build a applicative cost
simulation relation \(S \triangleq ( \{ (M, N)\} \cup Id_\T , Id_\V )\).

As an example of terms that are not an optimization we have.
\(M' \triangleq \tick[7] \oplus \tick[5]; N' \triangleq \tick[9] \). Clearly
\(M'\) is not improved by \(N'\)
  \end{example}

Which is a bit weird but follows the idea that with just one possible value for
\(N\) which is not worse than the ones reachable by \(M\) is enough to perform
an optimization.

In other words, the best case of \(N\) should be better (not worse) than the
best case of \(M\).

\subsubsection{Smyth's Cost Relations}
Smyth's Relator, same as before, is going to turn the relation the other way
around but this time is going to piggy back the cost too.

Applicative Cost simulation is then a relation \(S = (S_\T, S_\T)\) satifying
the following:
\begin{align*}
  \mkRel{M}{S_\T}{N} &\implies \mkRel{[[M]]^c}{\Gamma^c_{S}S_\V}{[[N]]^c} \\
  \mkRel{V}{S_\V}{U} &\implies \forall W \in \V, \mkRel{VW}{S_\T}{UW}
  \end{align*}

And applying Smyth's definition
\[
  \mkRel{M}{S_\T}{N} \implies
  \forall (n,n_c) \in [[N]]^c,
  \exists (m,m_c) \in [[M]]^c,
  \mkRel{m}{S_\V}{n}\land m_c \geqInf n_c
\]

Improvement is defined through the notion of observational approximation which
in this case is:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \mkRel{[[C[M]]]^c}{\Gamma_S\UU}{[[C[N]]]^c})
\]
And translates to
\[
  \mkRel{M}{\rctxImp[]}{N} \iff (\forall C \in Ctx,
  \forall (n,n_c) \in [[C[N]]]^c,
  \exists (m,m_c) \in [[C[M]]]^c,
  m_c \geqInf n_c
  )
\]

Which says that for every value reachable by \(N\), there is a worse one in
\(M\). It can be easily seen the duality between Hoare's and Smyth's
definitions. While Hoare's is satisfied with just one way to improve a term,
Smyth's wants to be sure that for every path taken in \(N\), there is one at
least as bad that could be taken in \(M\).

\begin{example}
It can be seen that \(M \triangleq \tick[5] I\) is not (Smyth) improved by \(N \triangleq
\tick[7] I \oplus I\). Since there is no case in \(M\) such that is worse than \(\tick[7] \in N\).

But! We can prove that \(N \geq M\). Since again every value reachable by \(M\)
has no grater cost than the value \(\tick[7]\) reachable by \(N\).

  \end{example}

In other words, the worst case of \(N\) should be better (not worse) than the
worst case of \(M\).

\subsubsection{Plotkin's Cost Relations}

Following the idea that Plotkin's relators basically ask for both Hoare's
\textbf{and} Smyth's relations between programs and costs. As result we obtained
a relation where a term \(M\) is improved by a term \(N\) whenever for every
possible value reachable by \(M\) there is (at least) one reachable by \(N\) with
better performance, and for every value reachable by \(N\) there is one
reachable by \(M\) with worse performance.

Following Plotkin's relator definition we get:
\begin{align*}
  \mkRel{M}{S_\T}{N} &\implies
                       (\forall v_m \in [[M]], \exists v_n \in [[N]], \mkRel{v_m}{S_\V}{v_n})
                       \land(\forall v_n \in [[N]], \exists v_m \in [[M]],  \mkRel{v_m}{S_\V}{v_n}) \\
  \mkRel{V}{S_\V}{W} &\implies \forall U \in \V, \mkRel{VU}{S_\T}{WU}
  \end{align*}

And

Which also give us an improvement definition which seems like:
\begin{align*}
  \mkRel{M}{\geq}{N} \implies & \forall C \in Ctx, \\
                       & (\forall (v_m,c_m) \in [[C[M]]]^c, \exists (v_n,c_n) \in
                       [[C[N]]]^c, (c_m \geqInf c_n)) \\
  \land & \\
                       & (\forall (v_n,c_n) \in [[C[N]]]^c, \exists (v_m, c_m)
                       \in [[C[M]]]^c, (c_n \geqInf c_m))
\end{align*}

As I was saying at the beginning of this section. We have both requirements made
by Hoare and Smyth. The base case of \(N\) should be better than the best case
of \(M\), while the worst case of \(M\) is worse than the worst case of \(N\).

\subsection{Completely Off-Topic}

Following the paper \emph{Non-determinism in Functional Programming}, there are
3 kinds of non-determinism:
\begin{itemize}
  \item \emph{Angelic} Non-determinism: choose terminating programs over non
    terminating ones. For example: \(\Omega \oplus I \mapsto I \).
  \item \emph{Demonic} Non-determinism: choose non-terminating programs over
    terminating ones. For example: \(\Omega \oplus I \mapsto \Omega \).
  \item \emph{Erratic} Non-determinism: real non-determinism.
  \end{itemize}

Angelic and Demonic comes in two flavors: \emph{Local} where the decision is
made at each operator, and \emph{Global} where the evaluation process prefers
some kind of behavior, in order words, it may choose locally the value that
makes the hole program behave as wanted.

Since \emph{non-termination} is explicit, I am going to introduce an element
\(\bot\) such that \([[ \Omega ]] = \bot \). Following this intuition I am going
to use another monad:

\newcommand\TEP{T_{EP}}

\begin{definition}[Explicit Non-Deterministic Monad]
  \begin{align*}
    \TEP X &\triangleq \PP_{fin}(X_\bot) \setminus \{\emptyset\}
    \end{align*}
  \end{definition}

In this way it is possible to identify easily programs that do not terminate. It
also introduces new technical procedures to carry out this bottom element. This
technicalities can be easily avoided composing the \emph{Maybe Transformer}. I
am not going to write it down (at least for now), but I will use the maybe lift \(f^{m} :
X_\bot -> Y_\bot\) directly.

There is three different monad definitions for \(\TEP\) which I am refer to as
\emph{D,A,E} for \emph{D}emonic, \emph{A}ngelic, \emph{E}rratic. I am trying to
figure this out, but each behavior could be identified at two levels local and
global. Global behavior seems to match with what monadic's bind definition is used
and local behavior seems to be defined as how the operator is interpreted.

\subsubsection{Angelic}

Angelic prefers termination over non-termination.

Global Angelic seems to use the following monad definition:
\begin{align*}
  \eta_A (x) &\triangleq \{ x \} \\
  f^A &:  \TEP (X) -> \TEP (Y) \\
  f^A (X) &= \begin{cases}
                 \{ \bot \} & X = \{ \bot \} \\
                 \bigcup_{x \in X \setminus \{\bot\}} f(x) & otherwise 
             \end{cases}
  \end{align*}

It can easily be seen how \(\bot\) is ignored as possible. It is not perfect
since it could be the case that \(\bot \in f(x)\) and I am not sure that is ok.
I am not so sure about this kind of Angelic Global behavior. \note{Talk to
  Mauro, I am sure he will tell me to erase all this nonsense.}

Local Angelic definition is achieved as:
\[ [[M \oplus N]] \triangleq [[M]] \cup [[N]] \setminus \{\bot\} \]
As the union of possible reachable values, removing non-termination if necessary.

Finally in order to derive an observational approximation and
applicative simulation definitions we need the following relator.

\begin{definition}[Hoare's Relator]
  Given \(R \subseteq X \times Y\), \(\Gamma_H R \subseteq \TEP(X) \times \TEP(Y)
  \) defined as follow:
  \[ \mkRel{P}{\Gamma_H}{Q} \iff \forall p \in P, \exists q \in Q, \mkRel{p}{R_{\bot}}{q}\]
  \end{definition}
Hoare relator is exactly definition~\ref{def:non-det-rel}. It is supposed to
denote \emph{Global angelic non-determinism}. Applying this relator we get an
improvement notion based on the \emph{best case scenario}.

\subsubsection{Demonic}

Global Demonic seems to use the following monad definition:
\begin{align*}
  \eta_A (x) &\triangleq \{ x \} \\
  f^A &:  \TEP (X) -> \TEP (Y) \\
  f^A (X) &= \begin{cases}
                 \{ \bot \} & \{ \bot \} \in X \\
                 \bigcup_{x \in X} f(x) & otherwise 
             \end{cases}
  \end{align*}
Following closely the idea of choose a non-termination path above other
possibilities.

Local Demonic definition is achieved as:
\[
  [[M \oplus N]] \triangleq
  \begin{cases}
      \{ \bot \} &  \bot \in [[M]] \cup [[N]] \\
      [[M]] \cup [[N]] & otherwise
    \end{cases}
  \]
Again choosing \(\bot\) whenever is possible.

There is a relator representing Global Demonic too.

\begin{definition}[Smyth Relator]
  Given \(R \subseteq X \times Y\), \(\Gamma_S R \subseteq \TEP(X) \times \TEP(Y)
  \) defined as follow:
  \[
    \mkRel{P}{\Gamma_S}{Q} \iff \forall q \in Q, \exists p \in P, \mkRel{p}{R_\bot}{q}
  \]
  % \[ \mkRel{P}{\Gamma_H R}{Q} \iff Q \subseteq R(P)\]
  \end{definition}
It is supposed to denote \emph{Global demonic non-determinism}. Applying this
relator we get an improvement notion based on the \emph{worst case scenario}.

\subsubsection{Erratic}

Erratic non determinism seems to use the following monad definition:
\begin{align*}
  \eta_A (x) &\triangleq \{ x \} \\
  f^A &:  \TEP (X) -> \TEP (Y) \\
  f^A (X) &= \bigcup_{x \in X} f^m(x) 
  \end{align*}

Interpreting non deterministic choice as:
\[
  [[M \oplus N]] \triangleq [[M]] \cup [[N]]
  \]

And getting the following relator:

\begin{definition}[Plotkin Relator]
  Given \(R \subseteq X \times Y\), \(\Gamma_H R \subseteq \PP(X) \times \PP(Y)
  \) defined as follow:
  \[ \mkRel{P}{\Gamma_H R}{Q} \iff Q \subseteq R(P) \land P \subseteq R^{-1}(Q) \]
  \end{definition}
It is supposed to denote \emph{Erratic non-determinism}. Applying this relator,
I ended up with something that I do not understand completely. Something as
programs that are not better nor worse of each other.
  