Catalog
==========

Short catalog of Concrete Examples of our Theory.

So far I thought about:
+ Maybe Monad that should derive into Sands' framework.
+ Assertion/Exception model.
+ Probabilistic Model.
