\section{Pure Calculus}

The simpler example is to no introduce any new additional operator.
% As first example a language without additional operators is presented.
Since there is no additional operators, \emph{no distinguishable algebraic
effect} is required. That means that we just compute pure and simple lambda
calculus in a \emph{call-by-value} fashion with one small caveat.

% Since there is no operator, an \emph{Empty Signature} is required:
\begin{definition}[Empty Signature]
  \( \Sigma_{\emptyset} \triangleq \emptyset ; \alpha_{\emptyset} = \emptyset \).
\end{definition}

Giving us back the \emph{classic lambda calculus
  % without additional
  % operators
} definition.
\begin{definition}[Calculus Definition]\label{def:lambda:calculus}
  Lambda Calculus syntactic grammar:
\begin{bnf*}
  \bnfprod{M,N}
  {
    \bnfts{\textbf{return}} \bnfsp \bnfpn{V}
    \bnfor \bnfpn{V} \bnfsp \bnfpn{W}
    \bnfor \bnfpn{M} \bnfsp \bnfts{\textbf{to} x.} \bnfpn{N}
  } \\
  \bnfprod{V,W}
  {
    \bnfts{x}
    \bnfor \bnfts{\(\lambda x.\)} \bnfpn{M}%
  }
\end{bnf*}
Defined as a two syntactic categories, lambda terms noted as \(\T\) and
value terms noted as \(\V\).
\end{definition}

Note that the language defined above presents lambda calculus plus an explicit
sequential operation also known as \emph{explicit eager let}. Such operator
present us with the need to introduce a monadic evaluator as mechanism to
sequentially evaluate expressions.

A monadic evaluation is a function \( [[ - ]] \subset \T \times T(\V) \) that
takes a term and evaluate it inside monad \(T\). Monads give us a way to
interpret effectful operators in it. To be sure that a monad is capable
of interpret such operators, such a monad \(T\) needs to carry a continuous
\(\Sigma_{\emptyset}\text{-algebra}\). Since \(\Sigma_{\emptyset}\) is empty,
every \wCPPO\ ordered monad \(T\) should do the trick.

The first monad that comes to our mind could be the \emph{Identity} monad but
sadly such an useful and simply monad \emph{does not} fulfill our requirements.
We require a monad that posses a \(\bot_X \in T X\) for every \(X \in \Set\ \).

\begin{remember}
  Since this work is based on a theory of approximations and fix-point theory,
  non-termination is explicit and (above all) \textbf{an observable effect!}
\end{remember}

Another simple but useful monad is the \emph{Lifting} or \emph{Maybe} monad that
equip a set with a new element signaling \emph{failure} or \emph{no-value}
computations.

\begin{definition}[Lifting]
  For every set \(X\), given an element \(\bot_X \notin X\), define \(X_{\bot} \triangleq X \cup \{\bot_X\}\).
\end{definition}

For each set in \( X \in \Set\), \(X_{\bot}\) has a discrete (or trivial) order,
and therefore an \wCPPO\ structure \((X_\bot, \sqsubseteq_X, \bot_X)\).
And also has monadic structure, the so called partiality monad:
\begin{align*}
  f_{\bot} &: X_{\bot} -> Y_{\bot} \\
  f_\bot(x) &=
              \begin{cases}
                \bot_Y & if \; x = \bot_X\\
                f(x)   & if \; x \neq \bot_X
              \end{cases}
  &\\
  \eta &: X -> X_{\bot} \\
  \eta (x) &= x \\
  % &\\
  % f^{\dagger} &: X_{\bot} -> Y_{\bot} \\
  % f^{\dagger}(x) &=
  %                  \begin{cases}
  %                    \bot_Y & if \; x = \bot_X \\
  %                    f(x)   & if \; x \neq \bot_X\\
  %                  \end{cases}
\end{align*}

A monadic evaluation function for the language defined at
\ref{def:lambda:calculus} is defined as follows:
\begin{definition}[Monadic Evaluation Function]
\begin{align*}
  [[ - ]] & : \T -> T(\V) \\
  [[ \LRet{V} ]] &= \eta(V) \\
  [[ \LApp{(\lambda x . M)}{W} ]] &= [[ M[x:=W] ]] \\
  [[ \LTo{M}{x.N} ]]  &= [[ M ]] >>= (V |-> [[ N[x:=V] ]]) \\
\end{align*}
% We can define a monadic operational semantics for the calculus defined
% above,
Where each value is introduced into the monad with \(\eta\), every application
is evaluated simply as substitution, and the sequential operator evaluating each
argument through the binding operation of the monad.
\end{definition}

Note that since the language has no additional operators defined by \(\Sigma_\emptyset\).
Therefore, the monadic evaluation function is the starting point where the
following examples are going to be built upon.

Following the method proposed by Ugo dal Lago, in order to derive an applicative
simulation and an observational approximation definition, a relator is required.
Remember that we use relator to lift relations from base objects \(X, Y \in
\Set\) to their effectful ones, in this case: \(X_{\bot}, Y_{\bot}\).

\begin{definition}[Partiality Relator]
  Given two sets \(X,Y \in \Set\ \), a relation \(R \subseteq X \times Y\),
  define \(\Gamma_{\bot} R \subseteq X_{\bot} \times Y_{\bot}\) as follows:
  \[
    \forall x \in X_{\bot}, y \in Y_{\bot}, \mkRel{x}{\Gamma_{\bot} R}{y} \iff 
    \begin{cases}
      x = \bot_{X} \\
      x \neq \bot_X \implies y \neq \bot_Y \land \mkRel{x}{R}{y}
    \end{cases}
  \]
  \end{definition}

This is enough to derive applicative simulation as well as contextual
approximation for the simple lambda calculus without effects.

\subsection{Applicative Simulation}

Applicative simulation is a relation 
% For applicative simulation we get the following equations,
\(S = (S_{\T},S_{\V})\) such that:
\begin{align*}
 \mkRel{M}{S_{\T}}{N} &\implies \mkRel{[[ M ]]}{\Gamma_{\bot} S_{\V}}{[[N]]} \\
 \mkRel{V}{S_{\V}}{W} &\implies \forall U \in V, \mkRel{V U}{S_{\T}}{W U}
\end{align*}

Since \(S_\V\) does not interact with neither \(\Gamma_\bot\) nor monadic
evaluation, only the term relation is expanded. Unfolding \(\Gamma_\bot\)
we get:
\[
  \mkRel{M}{S_{\T}}{N} \implies
  \begin{cases}
    [[M]] = \bot_{V} \\
    [[M]] = v \implies [[N]] = w \land \mkRel{v}{S_{\V}}{w}
  \end{cases}
\]
Relation \(S\) is defined for closed terms and closed values, and expanding
\(S_\V\) definition:
\[
  \mkRel{M}{S_{\T}}{N} \implies
  \begin{cases}
    [[M]] = \bot_{\V} \\
    [[M]] = (\func{x}{M'}) \implies [[N]] = (\func{x}{N'})
    \land (\forall w \in V, \mkRel{M'\{w/x\}}{S_{\T}}{N'\{w/x\}} ) 
  \end{cases}
\]

A relation \(S_\T \subseteq \T \times \T\) is an applicative simulation if for
every related terms \(M,N\), \(\mkRel{M}{S_\T}{N}\), either evaluating \(M\)
does not terminate or whenever \(M\) does terminate, also does \(N\), and their
values are extensively related.

Such applicative simulation is the classic definition used by Abramsky, Pitts,
etc. The idea is to evaluates until a value is found, and given that values are
functions, compare them extensively with the same \emph{relation}.

The only problem is that such a relation may not exists. The wise reader might
have noticed the circularity in the definition (or requirement) of \(S_\T\).
Since the computational model is defined inside a complete lattice, sets with
inclusion, we only need to guarantee that a monotonic operator could be built.
Which is exactly what relators give.

\subsection{Contextual Approximation}

Contextual approximation is defined to be the biggest \emph{compatible} and
\emph{adequate} relation. Since we are working in a complete lattice we have
that \((\geq_{\bot}) = \bigcup \mathbb{C}\mathbb{A}\).

Compatibility says that a relation is modular, so given two related terms they
behavior is related within bigger terms:
\(\mkRel{M}{\rctxImp[\bot]}{N} \implies \forall C \in Ctx, \mkRel{C[M]}{\rctxImp[\bot]}{C[N]}\).

Adequacy says that evaluating two related terms the same effects are observed:
\(\mkRel{M}{\rctxImp[\bot]}{N} \implies \mkRel{[[M]]}{\Gamma_{\bot} \UU }{[[N]]}\).

Mixing the two definitions above a compatible and adequate relation is one that:
\(\mkRel{M}{\rctxImp[\bot]}{N} \implies
\forall C \in Ctx, \mkRel{[[ C[M] ]]}{\Gamma_{\bot} \UU }{[[ C[N] ]]}
\).
\begin{note}
  Does the converse hold?.
  \end{note}
Such definition is more close to the definitions given by Pitts, Sands, Howe,
Abramsky, etc. therefore, is the one we are going to use to derive new notions of
contextual approximation and improvement notions.

Applying \(\Gamma_{\bot}\) definition we get that:
\[\mkRel{M}{\rctxImp[\bot]}{N} \implies \forall C \in Ctx,
\begin{cases}
  [[ C[M] ]] = \bot_{\V} \\
  [[ C[M] ]] = v \implies [[ C[N] ]] = w \land \mkRel{v}{\UU}{w} \\
\end{cases}
\]

And by definition of \(\UU\):
\[\mkRel{M}{\rctxImp[\bot]}{N} \implies (\forall C \in Ctx,
\begin{cases}
  [[ C[M] ]] = \bot_{\V} \\
  [[ C[M] ]] \in \V \implies [[ C[N] ]] \in \V  \\
\end{cases}
)
\]

Which is the classic observational approximation relation. Saying for every
context \(C\), when \(C [M]\) reaches a value implies that \(C [N]\) also does.
From recent contributions: applicative simulation is enough (and
not always necessary) to prove observational approximation.

\subsection{Introducing Costs}

In this section an intensional relational analysis is developed joining the
ideas of Ugo Dal Lago and Sands. Sands did something similar, once he saw the
work of Howe, and the main goal here is to use this example to get the same cost
notions as Sands. Sands's work is therefore used to check and confirm the
validity of the notions derived.

The easiest way to keep record of the resources needed to
perform a computation is to just implement some sort of shared counter across
evaluation. That it is not always possible, since it could be the case a
computation could take multiple paths with different costs. Therefore, a simpler
model is proposed where a natural number is attached to each value inside the
monadic evaluator.
Assuming that resources come in packages (energy to thanks
quantum mechanics) it is enough to have natural number keeping track of how many
\emph{quantos} were spent.

Keeping count in such counter is an observable effect, after all we want to
observe and compare cost between computations. Therefore, it is helpful to
define a signature with one operation called \emph{tick}.

\begin{definition}[Sigma Tick]
\(\Sigma_{\tick} \triangleq \{\tick\}, \alpha_{\tick}(\tick) = 1\).
\end{definition}

Instead of define a new model of computation we are going to reuse the work done
before, and since we want to attach cost to values inside monadic computation we
can use a known idea on the field:
\begin{definition}[Cost Transformer]
  Given a monad \(T\), define the cost transformer as \( Cost \; T \; X \triangleq T(X
  \times \N)\).
  \end{definition}

Cost transformer is nothing more than an instance of the \emph{Writer's
Transformer Monad}.
Following with the example in this section, the computational cost model is
\(Cost (\__{\bot}) X = (X \times \N)_{\bot}\).

In order to keep notation clear, instead of evaluating with the general monadic
evaluator, a specific cost evaluator is employed: \( [[ - ]]^c : \T -> T(\V
\times \N)\).

Where tick is interpreted as follows
\begin{align*}
  \tick &: (X \times \N) -> (X \times \N) \\
  \tick (x,n) &= (x , 1 + n)\\
  [[\tick(M)]]^c &= \tick[{\bot}]([[M]]^c)
\end{align*}
There are fancier ways to define tick's interpretation. We need to decide which
one is the most useful one. This at least express directly what we want from tick.

\begin{definition}[Cost Relator Transformer]
  Given a relator \(\Gamma\), define the cost relator as: for any relation
  \(R\), \(\Gamma^c R \triangleq \Gamma (R \times (\geqInf))\).
\end{definition}

The Cost applicative simulation for the partiality monad is then a relation \(R
= (R_\T,R_\V)\) such that:
\begin{align*}
 &\mkRel{M}{S_{\T}}{N} \implies \mkRel{[[ M ]]^c}{\Gamma^c_{\bot} S_{V}}{[[N]]^c} \\
 &\mkRel{V}{S_{V}}{W} \implies \forall U \in V, \mkRel{V U}{S_{\T}}{W U}
\end{align*}
And translates to:
\[
  \mkRel{M}{S_{\T}}{N} \implies
  \begin{cases}
    [[M]]^c = \bot_{V \times \N} \\
    [[M]]^c = (\func{x}{M'}, n) \implies [[N]]^c = (\func{x}{N'}, m)
    \land (n \geqInf m)
    \land (\forall w \in V, \mkRel{M'\{w/x\}}{S_{\T}}{N'\{w/x\}} ) 
  \end{cases}
\]
which is exactly the very same relation called \emph{improvement simulation} by
David Sands.

Finally we also can instantiate what an observational approximation relation is
when taking costs into account.
\[
\mkRel{M}{\rctxImp[]}{N} \implies \forall C \in Ctx,
\mkRel{[[ C[M] ]]^c}{\Gamma^c_{\bot} \UU }{[[ C[N] ]]^c}.
\]
Same as before this give us:
\[
  \mkRel{M}{\rctxImp[]}{N} \implies (\forall C \in Ctx,
  \begin{cases}
    [[ C[M] ]]^c = (\_, n) \implies [[ C[N] ]]^c = (\_, m))
    \land (n \geqInf m) \\
    [[ C[M] ]]^c = \bot_{V \times \N}
  \end{cases}
) \]

which is actually the definition of improvement a la Sand.

As a conclusion of this section we \emph{derive} from our theory the same
definitions made by Prof. Sands. This give us a some sort of confidence that we
are doing the right thing.

But! And extremely important, we have a very useful lemma which give us a simple
method to prove improvements.
\begin{lemma}
  Applicative Simulations implies Contextual Observational Approximation!
\end{lemma}

\subsection{Notes}
We can also think about lifting \(\bot\) as an operation which describes a
\emph{failing} term. That is define \(\Sigma_\bot \triangleq \{ fail \};
\alpha(fail) = 0\). Definitions are similar and the notions
derived from the framework are the same but now there is an operator aborting evaluation.