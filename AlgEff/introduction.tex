\section{Introduction}
%% Introduce the reader to both Improvement Theory and Algebraic Effects?

Program optimization is a difficult task, since we have to take into account two
different aspects of computation:
\begin{itemize}
  \item \emph{what} we compute and
  \item \emph{how} we compute.
\end{itemize}
We want to improve \emph{how} computation is performed \textbf{without} changing
\emph{what} is computed, i.e.\ preserving the meaning of programs.
%
%% What are we going to do?
Our task is then to \emph{observe} intensional properties of execution, such as
computing time, energy consumption, memory usage, etc, while preserving the
meaning of programs.
% In other words, we need to establish a relation between programs that relate
% safe program transformation in a way that the observed intensional properties
% are improved.

\emph{Improvement Theory}~\citep{Sands:Op:Theories} does precisely this for pure
functional languages with non-termination.
%
It defines a relation called \emph{improvement} as a stricter notion of
equivalence establishing which transformations are safe while also enabling the
comparison of program performance.
%
Such theory is obtained as a refinement of program equivalence, as shown
below.

\subsection{From Equivalence to Improvement}

Observational equivalence~\citep{morrisphd} states that two programs are
equivalent if no context distinguishes them.
%
Assuming a language where programs might not terminate, it is enough to check
whether contexts distinguish non-termination, where we note \(t \downarrow\) when
program \(t\) converges.
%
\[
t\cong u \quad\triangleq\quad \forall \CC, ~\CC[t]\downarrow ~\Leftrightarrow~ \CC[u]\downarrow
\]

Proofs of observational equivalence have the complication of having to deal with
universal quantification over all contexts.
%
\citet{Abramsky:1990:LLC:119830.119834} introduced the notion of
applicative bisimilarity as a simpler notion of equivalence and proved
denotationally that it implies observational equivalence since bisimulation is a
congruence.
%
\citet{Howe:Eq} showed how to obtain the same result but operationally,
thus opening the door for \citet{Sands:Op:Theories} operational theory of
improvements.

Improvement works much like observational equivalence but using an operational
semantics indexed by an ordered set of elements representing costs.
%
For example, we are not only interested in the termination relation
\((t\downarrow)\), but also in the cost \(n\) of evaluation \((t\downarrow_n)\).
%
Sands starts with a notion of improvement which, like observational equivalence,
quantifies over all contexts.

\begin{definition}[Improvement]
  Given programs \(t\) and \(u\), \(t\) is improved by \(u\) when in every program
  \(t\) can be replaced by \(u\) without incrementing the cost:
  \[
  t \cimproved u \quad\triangleq\quad \forall\CC, n \in \N,~ \CC[t]\downarrow_n
  ~\implies~ \exists m \in \N,~ \CC[u] \downarrow_m
   \land~  n \geq m
   \]
\end{definition}

This definition has the same downside as observational equivalence: to prove an
improvement, we need to show that evaluating a term costs more/less than another
\emph{in every context}.
%
However, the solution is analogous: Sands defines a notion of improvement
simulation and shows, adapting Howe's method, that it implies improvement.

Improvement is more expressive than observational equivalence since it
distinguishes between otherwise observably equivalent terms.
% , as we can see in
% the following example.
%
% \begin{example}
%   For example, assuming that the evaluation relation \((\downarrow_m)\) counts
%   the number of \(\beta\)-reduction steps in the evaluation of terms.
%   %
%   For any \(m,n\) \(\lambda\)-terms, we can prove that \( (\lambda x . m) n \cimproved m[ x:= n] \).
%   %
%   However, it cannot be proved that \( m[ x:= n] \cimproved (\lambda x . m) n \).
%   \end{example}
%
From the definition of improvement, we can define a new observable equivalence:
two terms are equivalent if we can prove improvement both ways.
%
Such new \textit{improvement equivalence} is stricter than observational
equivalence since two terms are improvement equivalent if they are observable
equivalent and require the same amount of resources to be computed.

\subsection{Taking effects into account}

Recently, \citet{Eff:App:Bisim} extended Howe's method
to deal with computational effects, where effects are modelled by a monad.
%
The simulation relation is built around the concept of relator that lifts
relations between pure values into relations between effectful values.
%
%% A relator, given a way to compare pure values, yields a way to compare effectful
%% values.

The notion of relator gives us an opportunity: we can consider the cost of
evaluation as a side effect and lift pure values as zero-cost computations.
%
Furthermore, if we combine the cost effect with other effects, we can compare
the cost of effectful programs, and thus obtain an improvement theory for
effectful languages.

%%% Motivation moved here
\input{Motivation}
%%% End Motivation

More concretely, the contributions of this article are:
\begin{itemize}
\item We define a general theory of improvement for effectful languages.
  %
\item We introduce a way to derive an improvement notion for several specific
  effectful languages.
  %
  In particular, we obtain for the first time improvement relations for a
  probabilistic language.
  %
% \item We show how to use the improvement relation on probabilistic languages to
%   obtain non-trivial optimizations.
\item We prove that unobservable code elimination (a generalization of dead-code
elimination) yields an improvement.
%
\item We show a non-trivial optimization on probabilistic languages.
%
\item We define \emph{Common Subexpression Elimination} (CSE) as an improvement
and prove it for the case when the only observable effect is non-termination.
  %
We also explore why CSE may be hard to prove in the presence of other effects.
\end{itemize}
