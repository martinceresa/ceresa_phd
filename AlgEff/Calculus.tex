\subsection{Language and Calculus}\label{sec:Calculus}

In this section, we present the language used as vehicle to show different
notions of improvement.
%% , based on the previous mathematical constructions, and
%% following~\citep{Eff:App:Bisim}.
%
Following~\citet{Eff:App:Bisim}, we define a classic lambda calculus with two
additional constructions:
\begin{itemize}
\item an explicit binding term constructor \((\LTo{}{})\), and
\item a way to introduce symbols from the signature into the language.
\end{itemize}

\begin{definition}[Language]\label{def:lang}
  Let \(\Sigma\) be a signature.
  %
  We define a set of terms, \(\T_{\Sigma}\), and a set of values,
  \(\V_{\Sigma}\).
  % 
  The language is defined using two \emph{BNF} syntactic categories:
\begin{align*}
  M,N &::= (\LRet{V}) ||| (\LApp{V}{W}) ||| (\LTo{M}{x \mathrel{.} N})
        ||| \sigma(M_1 , \ldots, M_n) \\
  V,W &::= x ||| (\LAbs{x}{M})
\end{align*}

Where variable \(x\) ranges over a countable set of variables \(Var\) and \(\sigma\)
ranges over the symbols defined in \(\Sigma\) with \(n = \alpha(\sigma)\).
\end{definition}

% \revNote
% {
%   The term \((\LTo{M}{x.N})\) represents a sequential binding operator, and each
%   symbol \(\sigma\) in \(\Sigma\) is represented by a term \(\sigma(M_1, \ldots ,
%   M_n)\) with \(\alpha(\sigma) = n\) and \(M_1, \ldots, M_n\) terms.
%   %
%   As result, we obtain a calculus with an explicit separation between
%   terms and value.
% }
% {
% p10 Is M to x.N what is usually written as a (non-recursive) let expression? It
% would be useful to mention this, as well as the odd syntax which makes values
% and terms disjoint.
% }
Consequently, we obtain a language with a clean explicit separation between
values and terms.
%Moreover, the language forces terms to explicitly guide evaluation in a call-by-value manner.
%\mauro{The above sentence is commented because it doesn't make sense.}
%
Each symbol \(\sigma\) in \(\Sigma\) is added to the language as a term
\(\sigma(M_{1}, \ldots, M_{n})\) with \(\alpha(\sigma) = n\) and \(M_{1}, \ldots, M_{n}\) terms.
  %
  The non-recursive let expression, noted as \(\LTo{M}{x.N}\), not only represents
a let-binding construction but also states that the term \(M\) should be
evaluated before the evaluation of \(N\), and thus, the effects produced
by \(M\) should happen before the effects produced by \(N\).
%
For example, in classic presentations of the lambda calculus application is
typically defined (syntactically) between two terms \(\LApp{M}{N}\), and thus, it is up
to the semantics to decide an evaluation that determines which term is
evaluated first: the function \(M\) or the argument \(N\).
%
The language presented here only admits the application of values, and thus,
forces the order of evaluation to be made explicit using let bindings.
% This is different from the language used here, which only admits applications of values.
% The syntax forces the order of evaluation to be made explicit using let bindings.
%
% since we are working with effectful computations,
% having a syntactically sequential order of evaluation is extremely useful
% because
In our case, the use of non-recursive let-bindings defines the order of the
effects produced at evaluation.
% effects are sequentially ordered following the non-recursive lets.

In the rest of the section, we work with a fixed arbitrary signature \(\Sigma\)
and note the set of terms and values as \(\T\) and \(\V\), respectively.
%
Moreover, we consider terms and values modulo
\(\alpha\)-equivalence and follow the so-called Barendregt variable
convention: if \(M_1, \ldots, M_n\) appear in the same
mathematical context (e.g. definition, proof), then all of their bound
variables are different from the free variables.

We note \(FV(M)\) as the set of free variables of term \(M\), and we use the
same notation for the set of free variables of values. Given a finite
set \(X\) of variables, we define the set of terms and values with free
variables in \(X\) as follows:
\begin{align*}
  \T(X) &= \{ M \in \T ||| FV(M) \subseteq X\}\\
  \V(X) &= \{ V \; \in \V ||| FV(V) \; \subseteq X\}
\end{align*}

The set of closed terms and values defined by \(\T(\emptyset)\) and
\(\V(\emptyset)\) are noted as \(\KT\) and \(\KV\), respectively.
%
We note the substitution of variable \(x\) with value \(V\) in term \(N\) as
\(N [x:= V]\)~\citep{Eff:App:Bisim}.

% \begin{definition}[Value Substitution~\citep{Eff:App:Bisim}]
%   Let \(V,W\) be values and \(M\) be a term. The value
%   \(\substV{V}{y}{W}\) is the result of the (simultaneous) substitution of every
% free appearance of variable \(y\) with \(W\) in \(V\), and the term
% \(\substT{M}{y}{W}\) is the term defined by (simultaneous) substitution of every
% free appearance of \(y\) with \(W\) in \(M\).
%   \begin{align*}
%     x[W/y] &=
%              \begin{dcases}
%                W & \text{if } x = y \\
%                x & \text{if } x \neq y
%              \end{dcases} \\
%     (\lambda x . M)[W/y] &= \lambda x. M[y := W] \\
%     & \\
%     (\LRet{V})[y := W] &= \LRet{(V[W/y])} \\
%     (\LApp{V}{V'})[y := W] &= \LApp{(V[W/y])}{(V'[W/y])} \\
%     (\LTo{M}{x \mathrel{.} N})[y := W] &= \LTo{(M[y:=W])}{x\mathrel{.}(N[y:=W])} \\
%     \sigma({M_1, \ldots, M_n})[y:=W] &= \sigma({{M_1}[y:=W], \ldots, {M_n}[y:=W]})
%   \end{align*}
% \end{definition}

% \martin{Maybe we can just remove the definition entirely and just mention that
% we use value substitution as defined in somewhere else}

% \subsubsection*{Evaluation.}
% For the rest of the section we fix an arbitrary \(\Sigma\text{-continuous}\)
% monad \(T\).

For a given \(\Sigma\)-continuous monad \(T\), we define a family
of approximation relations indexed by a natural number \(n\)
between closed terms and effectful closed values \(( ||_n )\),  as follows:
%% Operational Approx Semantics
\begingroup
\addtolength{\jot}{1em}
\begin{align*}
  %% Bot Rule
  &\inference[\<bot>]{}{M ||_0 \bot}
  \quad
  \inference[(\<ret>)]{}{\<return> V ||_{n+1} \eta(V)} \\
  &\inference[(\<seq>)]{M ||_n X  & \substT{N}{x}{V} ||_n Y_V}%
                {\LTo{M}{x.N} ||_{n+1} X >>= (V |-> Y_V) } \\
  &\inference[(\<app>)]{\substT{M}{x}{W} ||_n X}
        {\LApp{(\lambda x. M)}{W} ||_{n+1} X} \\
  &\inference[(\(\sigma\)-\<op>)]{M_1 ||_n X_1 & \ldots & M_k ||_n X_k}
            {\sigma(M_1, \ldots, M_k) ||_{n+1} \sigma^T (X_1, \ldots, X_k)}
\end{align*}
\endgroup
\noindent where the rule (\(\sigma\)-\<op>) is a set of rules one for each \(\sigma\)
in \(\Sigma\) and \(k = \alpha(\sigma)\).
%
Rule (\<seq>) follows sequential monadic evaluation: for term
\(\LTo{M}{x . N}\), we first approximate \(M\) to a monadic value
\(X \in T(\KV)\), and then we approximate \(N\) where variable \(x\)
is replaced by a parameterized value \(V\).
%
The value \(V\) represents the ground value in \(X\).
%
Other monadic evaluation relations where effects are fixed and known may not
require such general rule, as in the case of the works of~\cite{Lassen.1997.SimBisimNonDet,DalLago.2012.ProbOpSeman,Danielsson.2012.OpeSemanPartiality}.
%
However, in this work, we lose some specificity to gain generality.

Judgments are of the form \(M ||_n X\), where \(M \in \KT\), \(X \in T(\KV)\),
and \(n \geq 0\).
%
Intuitively \(M ||_n X\) means that \(X\) is the \(n\)-th
approximation of the evaluation of \(M\) following a
\emph{call-by-value} strategy.
%
Each judgment has a unique derivation, and moreover, for a given closed term \(
M\), the set of its approximation forms a chain \({\{M \mathrel{||_n} \}}_{n <
  \omega}\)~\citep{Eff:App:Bisim}.
% Together with the fact that \(T\) is \wCPPO\ ordered,
% evaluation is defined as:
\begin{definition}[Evaluation]\label{def:evaluation}
  Let \(T\) be a \(\Sigma\)-continuous monad and \(M\) be a closed term.
  Define the \emph{evaluation} of \(M\),  \([[M]]\) in
  \(T(\KV)\), as the lub of the chain of its approximations:
  \[ [[ M ]] = \LUB{(M \mathrel{||_n})}{n}\]
\end{definition}

The evaluation definition enables us to explicitly handle non-termination since
\(\Sigma\)-continuous monads are ordered monads.
%
Let \(T\) be a \(\Sigma\)-continuous monad, for each set \(X\), \(T(X)\) is an
\wCPPO\ with \(\bot_X\) as bottom element, and thus, we associate
divergent computations with \(\bot_X\).

Finally, since we employ \(\Sigma\)-continuous monads, the bind operator
\((>>=)\) and \(\sigma^T\) are continuous for each \(\sigma\) in \(\Sigma\),
%% is continuous, and for each  the algebra is continuous
and every least upper bound is preserved, enabling us to define evaluation
as the following equations~\citep{Eff:App:Bisim}:
\begin{align*}
  [[ \LRet{V} ]] &= \eta(V) \\
  [[ \LApp{(\lambda x . M)}{W} ]] &= [[ \substT{M}{x}{W} ]] \\
  [[ \LTo{M}{x.N} ]]  &= [[ M ]] >>= (y |-> [[ \substT{N}{x}{y} ]]) \\
  [[ \sigma(M_1 , \ldots, M_n) ]] &= \sigma^T([[M_1]], \ldots, [[M_n]])
\end{align*}
%
The evaluation function \([[ \cdot ]]\) is the least solution to the equations above.

% \martin{improve closing}
In this section, we defined a concrete evaluation relation for terms.
% the reader with a concrete evaluation relation for terms.
%
Evaluation is well-defined following terms structure and non-terminating
programs are handled explicitly as the bottom element, \(\bot\), of a given
\wCPPO{}.
%
All this work has been previously developed by~\citet{Eff:App:Bisim}
and~\citet{Ugo:Norm:Bisim} where a more detailed account is given.
