Exceptions
=========================

Exceptions are introduced
```
module Exceptions where

open import Level renaming (suc to lsuc; zero to lzero)
open import Relation.Binary
open import Relation.Binary.PropositionalEquality

open import Relators
open import Functors
open import Monads

open import Data.Sum
open import Data.Product

open import Data.Nat
open import Data.Nat.Properties
```

Exception functor:

Objects mapping:
```
Except₀ : Set -> Set -> Set
Except₀ E X = X ⊎ E
```
Functor definition
```
Except : Set -> Functor {lzero}
Except E = record
             { F₀ = Except₀ E
             ; F₁ = λ{ f (inj₁ x) →  inj₁ (f x)
                    ; f (inj₂ y) → inj₂ y }
             }
ExceptM : (E : Set) -> Monad (Except E)
ExceptM E = record { η = inj₁ ; _† = λ { f (inj₁ x) → (f x) ; f (inj₂ y) → inj₂ y } }

```
Properties
```
ExceptProp : (E : Set) -> FunctorProp (Except E)
ExceptProp E = record
             { ident = ext λ{ (inj₁ x) → refl ; (inj₂ y) → refl}
             ; comp = λ f g → ext λ{ (inj₁ x) → refl ; (inj₂ y) → refl }}

```
Exception relator definition
```
data RExcept E {A B : Set} (R : REL A B lzero)
             : (REL (Except₀ E A) (Except₀ E B) lzero) where
     REx : (e : E) -> RExcept E R (inj₂ e) (inj₂ e)
     RLift : (a : A) -> (b : B) -> R a b -> RExcept E R (inj₁ a) (inj₁ b)

exceptRelator : (E : Set) -> RelatorFProp {lzero} (Except E) (RExcept E)
exceptRelator E = record
              { ident = λ{ (inj₁ x) .(inj₁ x) refl → RLift x x refl
                        ; (inj₂ e) .(inj₂ e) refl → REx e}
              ; comp = λ{ (inj₁ x) .(inj₁ b) (.(inj₁ a)
                        , RLift a b x₁
                        , RLift .x .a x₂)
                        → RLift x b (a , x₁ , x₂)
                        ; (inj₂ e) .(inj₂ e) (.(inj₂ e) , REx .e , REx .e)
                        → REx e}
              ; inv = λ{ R f g (inj₁ x) .(inj₁ b) (RLift .x b x₁)
                         → RLift (f x) (g b) x₁
                      ; R f g (inj₂ e) .(inj₂ e) (REx .e) → REx e}
              ; mono = λ{ R S x (inj₁ x₂) .(inj₁ b) (RLift .x₂ b x₁)
                          → RLift x₂ b (x x₂ b x₁)
                       ; R S x (inj₂ e) .(inj₂ e) (REx .e) → REx e}
              }
exceptLaxExt : (E : Set) -> RelatorMonadProp (Except E) (ExceptM E) (RExcept E)
exceptLaxExt E = {!!}
```
Takking cost into exceptional values too.
```
module CostExceptionStr
       (E : Set) where
  CostExcept : Set -> Set
  CostExcept X = (X × ℕ) ⊎ (E × ℕ)

  CostExcept' : Set -> Set
  CostExcept' X = (X ⊎ E) × ℕ

  addCost : {X : Set} -> ℕ -> CostExcept' X -> CostExcept' X
  addCost n (fst , snd) = fst , snd + n

  CostExcept'₁ : {X Y : Set} -> (X -> Y) -> CostExcept' X -> CostExcept' Y
  CostExcept'₁ f (inj₁ x , snd) = inj₁ (f x) , snd
  CostExcept'₁ f (inj₂ y , snd) = inj₂ y , snd

  CE : Functor
  CE = record { F₀ = CostExcept' ; F₁ = CostExcept'₁ }
  open Functor CE

  CEM : Monad CE
  CEM = record { η = λ x → inj₁ x , 0 ; _† = λ{ f (inj₁ x₁ , snd) → addCost snd (f x₁) ; x (inj₂ y , snd) → (inj₂ y) , snd} }


  CEProp : FunctorProp CE
  CEProp = record
         { ident = ext λ{ (inj₁ x , snd) → refl ; (inj₂ y , snd) → refl}
         ; comp = λ f g → ext λ{ (inj₁ x , snd) → refl ; (inj₂ y , snd) → refl}
         }

  CERelator : {A B : Set} (R : REL A B lzero) -> REL (CostExcept' A) (CostExcept' B) lzero
  CERelator R (fst , snd) (fst₁ , snd₁) =  RExcept E R fst fst₁ × snd ≥ snd₁

  CERelatorProp : RelatorFProp CE CERelator
  CERelatorProp = let open RelatorFProp (exceptRelator E) renaming (ident to Eident; comp to Ecomp; mono to Emono) in
                record
                { ident = λ{ x .x refl → Eident (proj₁ x) (proj₁ x) refl , ≤-refl}
                ; comp = λ{ x z (fst , fst₁ , snd) → Ecomp (proj₁ x) (proj₁ z) (proj₁ fst , proj₁ fst₁ , proj₁ snd) , ≤-trans (proj₂ fst₁) (proj₂ snd)}
                ; inv = λ{ R f g (.(inj₂ e) , snd₂) (.(inj₂ e) , snd₁) (REx e , snd) → REx e , snd
                        ; R f g (.(inj₁ a) , snd₂) (.(inj₁ b) , snd₁) (RLift a b x , snd) → RLift (f a) (g b) x , snd}
                ; mono = λ { R S x fa fb (fst , snd) → Emono R S x (proj₁ fa) (proj₁ fb) fst , snd }
                }

  CEMonadRelatorProp : RelatorMonadProp CE CEM CERelator
  CEMonadRelatorProp = let open RelatorMonadProp (exceptLaxExt E) renaming (lax-bind to Elax) in
                     record
                     { lax-unit = λ R x y x₁ → RLift x y x₁ , ≤-refl
                     ; lax-bind =
                     λ{ R S f g x (.(inj₂ e) , snd₂) (.(inj₂ e) , snd₁) (REx e , snd) → REx e , snd
                       ; R S f g x (.(inj₁ a) , snd₂) (.(inj₁ b) , snd₁) (RLift a b x₁ , snd) -> {!!} }
                     }

  from : {X : Set} -> CostExcept X -> CostExcept' X
  from (inj₁ (fst , snd)) = inj₁ fst , snd
  from (inj₂ (fst , snd)) = (inj₂ fst) , snd

  to : {X : Set} -> CostExcept' X -> CostExcept X
  to (inj₁ x , snd) = inj₁ (x , snd)
  to (inj₂ y , snd) = inj₂ (y , snd)

  from→to : {X : Set} -> (e : CostExcept X) -> to (from e) ≡ e
  from→to (inj₁ x) = refl
  from→to (inj₂ y) = refl

  to→from : {X : Set} -> (e : CostExcept' X) -> from (to e) ≡ e
  to→from (inj₁ x , snd) = refl
  to→from (inj₂ y , snd) = refl
```
