Outer Cost Relator Module
========================================

**CALL THIS OUTER COST IS SOMEWHAT MISLEADING. THINK ABOUT IT**


```
module OuterCostRelator where
```

Fist import the required libraries

```
open import Level renaming (suc to lsuc)

open import Relators renaming (_∘_ to _●_)
open import Functors

open import Data.Product
open import Function

-- Needed for proofs
open import Relation.Binary
open import Relation.Binary.PropositionalEquality hiding (Extensionality)
```

Define State's Monad Transformer given a functor F (Should be a monad but that
has not been implemented yet).

```
module StateT
       {l : Level}
       (F : Functor {l}){FProp : FunctorProp F} -- Given a Functor F
       (S : Set l)
       where
  open Functor F
  open FunctorProp FProp renaming (ident to fident; comp to fcomp)

  ST₀ : Set l -> Set l
  ST₀ X =  S -> F₀ (X × S)

  STFunctor : Functor
  STFunctor = record
            { F₀ = ST₀
            ; F₁ = λ f x s →  F₁ (map₁ f) (x s) }

  STProps : FunctorProp STFunctor
  STProps = record
          { ident = ext (λ x → ext (λ s → trans (cong-app fident (x s)) refl))
          ; comp = λ f g → ext λ x → ext λ s → -- Do I need Ext?
                  let open ≡-Reasoning in
                    F₁ (map₁ (g ∘ f)) (x s) ≡⟨ refl ⟩
                    F₁ (map₁ g ∘ map₁ f) (x s) ≡⟨ cong-app (fcomp (map₁ f) (map₁ g)) (x s) ⟩
                    (F₁ (map₁ g) ∘ F₁ (map₁ f)) (x s) ≡⟨ refl ⟩
                    F₁ (map₁ g) (F₁ (map₁ f) (x s))  ∎
          }
```

So now we can define a *relators transformer* for |StateT|.

```
module StateTRelatorEq
  {l : Level}
  (F : Functor {l}){FProp : FunctorProp F} -- Given a functor F
  (S : Set l) -- Set of states
  (Γ : Relator F){ ΓProp : RelatorFProp F Γ} -- a relator for F.
  where
  open StateT {l} F {FProp} S
  open RelatorFProp ΓProp renaming (ident to Γident; comp to Γcomp; inv to Γinv; mono to Γmono)

  liftEq : {A B : Set l}
           -> REL A B l
           -> REL (A × S) (B × S) l
  liftEq P (a , s₁) (b , s₂) =  P a b × s₁ ≡ s₂

  -- ∀ s ∈ S, (f s) Γ (R × ≡) (g s)
  STRelator : Relator STFunctor
  STRelator R f g = (s : S) -> Γ (liftEq R) (f s) (g s)

  STRelProps : RelatorFProp STFunctor STRelator
  STRelProps = record
             { ident = λ x y x₁ s → Γmono _≡_ (λ { (a , s₁) (b , s₂) → a ≡ b × s₁ ≡ s₂ })
                     (λ{ a .a refl → refl , refl}) -- _≡_ ⊆ _≡_ × _≡_
                     (x s) (y s) (Γident (x s) (y s) (cong-app x₁ s))
             ; comp = λ{ {R = P} {Q} x z ( STB , RelSTBZ , STRElx) s →
                    Γmono ( liftEq Q ● liftEq P) (λ { (a , s₁) (b , s₂) → (Q ● P) a b × s₁ ≡ s₂ })
                    (λ{ (a , .s₃) (c , s₃) ((b , .s₃) , (fst , refl) , fst₁ , refl) → (b , fst , fst₁) , refl})
                    (x s) (z s) (Γcomp (x s) (z s) (STB s , RelSTBZ s , STRElx s) )
                    }
             ; inv = λ R f g x y x₁ s → Γinv (liftEq R) (map₁ f) (map₁ g) (x s) (y s)
                   (Γmono (liftEq (preR f g R)) (preR (map₁ f) (map₁ g) (liftEq R))
                   (λ a b x₂ → x₂)
                   (x s) (y s) (x₁ s))
             ; mono = λ R P R⊆P Fa Fb FaRFb s
               → Γmono (liftEq R) (liftEq P)
                       (λ{ (a , s₁) (b , .s₁) (Rab , refl) → R⊆P a b Rab , refl})
                       (Fa s) (Fb s) (FaRFb s)
             }

module OuterImprovement
  (F : Functor ){FProp : FunctorProp F} -- Given a functor F
  (Γ : Relator F){ ΓProp : RelatorFProp F Γ} -- a relator for F.
  where
  open import Data.Nat
  open import Data.Nat.Properties
  open StateT F {FProp} ℕ
  open RelatorFProp ΓProp renaming (ident to Γident; comp to Γcomp; inv to Γinv; mono to Γmono)

  private variable l : Level

  liftImp : {A B : Set}
          -> REL A B l
          -> REL (A × ℕ) (B × ℕ) l
  liftImp R (a , n) (b , m) =  R a b × n ≥ m

  OutImp : Relator STFunctor
  OutImp R f g = (n : ℕ) -> Γ (liftImp R) (f n) (g n)

  OutImpProp : RelatorFProp STFunctor OutImp
  OutImpProp = record
             { ident = λ x y x₁ n → Γmono _≡_ (liftImp _≡_)
                       (λ{ (a , n) (.a , .n) refl → refl , ≤-refl})
                       (x n) (y n) (Γident (x n) (y n) (cong-app x₁ n))
             ; comp = λ{ {R = R} {S} x z (FB , fst₁ , snd) n → Γmono ( liftImp S ● liftImp R) (liftImp (S ● R))
                            (λ{ (a , ac) (b , bc) (( z , zc ) , fst₁ , snd) → (z , proj₁ fst₁ , proj₁ snd) , ≤-trans (proj₂ fst₁) (proj₂ snd) })
                            (x n) (z n) (Γcomp (x n) (z n) (FB n , fst₁ n , snd n))}
             ; inv = λ R f g x y x₁ n →  Γinv (liftImp R) (map₁ f) (map₁ g) (x n) (y n) (Γmono (liftImp ( preR f g R)) (preR (map₁ f) (map₁ g) (liftImp R)) (λ a b x₂ → x₂) (x n) (y n) (x₁ n))
             ; mono = λ R S x fa fb x₁ n → Γmono (liftImp R) (liftImp S)
                      (λ{ (fst , snd) (fst₁ , snd₁) (fst₂ , snd₂) → x fst fst₁ fst₂ , snd₂}) (fa n) (fb n) (x₁ n) }
```
