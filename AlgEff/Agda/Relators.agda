module Relators where

-- Proof of Concept: Relators in Agda

open import Level renaming (suc to lsuc)
open import Relation.Binary
open import Relation.Binary.PropositionalEquality

open import Functors
open import Monads

open import Data.Product

-- Relation composition with prod and coprod.
_∘_ : {l : Level} {A B C : Set l} -> REL B C l -> REL A B l -> REL A C l
_∘_ {l} {A} {B} {C} R S a c = Σ B (λ b →  R b c × S a b)

-- preR is a classic relation through function application.
preR : {l : Level} {A B X Y : Set l} -> (X -> A) -> (Y -> B) -> REL A B l -> REL X Y l
preR f g R x y = R (f x) (g y)

-- Relator is a way to lift a relation from ground terms into terms inside F
-- R ⊆ X × Y ⇒ Γ R ⊆ F X × F Y
Relator : {c : Level} (F : Functor {c}) -> Set (lsuc c)
Relator {c} F = let open Functor F in
            {A B : Set c} -> REL A B c -> REL (F₀ A) (F₀ B) c

-- We are working with monotonic relators.
-- Here are the laws
record RelatorFProp {l : Level} (F : Functor) (Γ : Relator F) : Set (lsuc l) where
  open Functor F
  field
    -- Properties and Laws
    -- Identity is lifted properly
    ident : {A : Set l}
            -> (x y : F₀ A) -> x ≡ y
            -> (Γ _≡_) x y
    -- Composition behaves
    comp : {A B C : Set l}
          -> {R : REL A B l} -> {S : REL B C l}
          -> (x : F₀ A) -> (z : F₀ C)
          -> ((Γ S) ∘ (Γ R)) x z -> Γ ( S ∘ R) x z
    -- Map and relator behaves
    inv : {A B X Y : Set l}
          -> (R : REL A B l) -> (f : X -> A) -> (g : Y -> B)
          -> (x : F₀ X) -> (y : F₀ Y)
          -> Γ (preR f g R) x y
          -> (preR (F₁ f) (F₁ g) (Γ R)) x y
    -- And in particular we work with monotonic relators.
    mono : {A B : Set l}
          -> (R S : REL A B l)
          -> ((a : A) -> (b : B) -> R a b -> S a b)
          -> (fa : F₀ A) -> (fb : F₀ B) -> Γ R fa fb
          -> Γ S fa fb

-- Lax Extesion???
record RelatorMonadProp {l : Level}(F : Functor {l})(M : Monad F)(Γ : Relator F) : Set (lsuc l) where
  open Functor F renaming (F₀ to T; F₁ to F)
  open Monad M
  field
    -- R ⊆ (η × η)-1 (Γ R)
    lax-unit : {X Y : Set l}(R : REL X Y l)
             -> (x : X) -> (y : Y) -> R x y
             -> Γ R (η x) (η y)
    -- R ⊆ (f × g)-1 (Γ S) ⇒ Γ R ⊆ (f† × g†) (Γ S)
    lax-bind : {X X' Y Y' : Set l}(R : REL X Y l)(S : REL X' Y' l)
             -> (f : X -> T X') -> (g : Y -> T Y')
             -- R ⊆ (f × g)-1 (Γ S)
             -> ((x : X) -> (y : Y) -> R x y -> (Γ S) (f x) (g y))
             -- u (Γ R) v
             -> (u : T X) -> (v : T Y) -> Γ R u v
             -- ⇒ f† u (Γ S) g† v
             -> Γ S ((f †) u) ((g †) v)
--------------------------------------------------------------------------------
-- DSL
--------------------------------------------------------------------------------
-- Relator Product
-- product : {r : Level}{F : Functor {l}}{G : Functor {r}}
--         -> Relator F -> Relator G -> Relator (F × G)
-- product = ?

-- Relator Intersection
intersection : {l : Level} {F : Functor {l}} -> Relator F -> Relator F -> Relator F
intersection Γ₁ Γ₂ R fa fb = (Γ₁ R fa fb) × (Γ₂ R fa fb)

compIsRelator : {c : Level}{F : Functor {c}}(Γ₁ Γ₂ : Relator F)
              -> RelatorFProp F Γ₁ -> RelatorFProp F Γ₂
              -> RelatorFProp F (intersection {c} {F} Γ₁ Γ₂)
compIsRelator {F = F} Γ₁ Γ₂ P₁ P₂ =
  let open RelatorFProp in
  record
    { ident = λ x y x₁ → ident P₁ x y x₁ , ident P₂ x y x₁
    ; comp = λ{ x z (fst , (fst₁ , snd₁) , fst₂ , snd)
            → (comp P₁ x z (fst , fst₁ , fst₂))
            , comp P₂ x z (fst , snd₁ , snd)}
    ; inv = λ{ R f g x y (x₁ , x₂)
            → inv P₁ R f g x y x₁
            , inv P₂ R f g x y x₂ }
    ; mono = λ{R S x fa fb ( x₁ , x₂ )
            → mono P₁ R S x fa fb x₁
            , mono P₂ R S x fa fb x₂ }
    }

-- Relator Composition
relComp : {l : Level}{F G : Functor {l}}
        -> Relator F -> Relator G -> Relator (FComp F G)
relComp S T R = S (T R)

relCompProp : {c : Level}{F G : Functor {c}}
            -> (Γ₁ : Relator F) -> RelatorFProp F Γ₁
            -> (Γ₂ : Relator G) -> RelatorFProp G Γ₂
            -> RelatorFProp (FComp F G) (relComp {c} {F} {G} Γ₁ Γ₂)
relCompProp {F = F}{G} Γ₁ P₁ Γ₂ P₂ =
            let open RelatorFProp in
            record
            { ident = λ{ x y x₁ →
                        mono P₁ (_≡_) (Γ₂ _≡_) (ident P₂) x y (ident P₁ x y x₁)}
            ; comp = λ{ {R = R} {S} x z w
                    → mono P₁ ((Γ₂ S) ∘ (Γ₂ R)) (Γ₂ (S ∘ R)) (comp P₂) x z
                      (comp P₁ x z w)
                    }
            ; inv = λ{ R f g x y x₁
                    → inv P₁ (Γ₂ R) (Functor.F₁ G f) (Functor.F₁ G g) x y
                      (mono P₁ (Γ₂ (preR f g R))
                        (preR (Functor.F₁ G f) (Functor.F₁ G g) (Γ₂ R))
                            (inv P₂ R f g) x y x₁)}
            ; mono = λ{ R S x fa fb x₁
                    → mono P₁ (Γ₂ R) (Γ₂ S) (mono P₂ R S x) fa fb x₁}
            }
