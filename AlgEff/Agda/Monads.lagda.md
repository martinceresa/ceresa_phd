```
module Monads where
open import Level renaming (suc to lsuc)
open import Functors

open import Function

open import Relation.Binary.PropositionalEquality hiding (Extensionality)

record Monad {l : Level}(F : Functor {l}) : Set (lsuc l) where
  open Functor F renaming (F₀ to T; F₁ to F)
  field
    -- Injection function,
    η : {X : Set l} -> X -> T X
    -- Kleisli lifting
    _† : {X Y : Set l} -> (X -> T Y) -> T X -> T Y

record MonadLaws {l : Level}{F : Functor {l}}(M : Monad F) : Set (lsuc l) where
  open Functor F renaming (F₀ to T; F₁ to F)
  open Monad M
  field
    law₁ : {X Y : Set l} (f : X -> T Y) -> ( f † ) ∘ η ≡ f
    law₂ : {X : Set l} -> ( _† {X} η ) ≡ (λ x -> x)
    law₃ : {X Y Z : Set l}(f : X -> T Y)( g : Y -> T Z)
         -> (g † ∘ f) † ≡ ( g † ∘ f † )
```
