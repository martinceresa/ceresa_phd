module Util where

-- This is also from Aaron Stump

open import Data.Bool
open import Data.Nat using (ℕ ; suc ; _+_  )
open import Data.Integer using (ℤ ; -[1+_] ; +_ ; -_; _⊖_)
open import Relation.Binary.PropositionalEquality
open import Data.Sum
open import Data.Product
open import Data.Empty
import Level

∨₁ : ∀ {b1 b2} → b1 ∨ b2 ≡ false → b1 ≡ false
∨₁ {true} ()
∨₁ {false}{true} ()
∨₁ {false}{false} p = refl

∨₂ : ∀ {b1 b2} → b1 ∨ b2 ≡ false → b2 ≡ false
∨₂ {true} ()
∨₂ {false}{true} ()
∨₂ {false}{false} p = refl

∨₁-cong : ∀ {b1 b1' b2} → b1 ≡ b1' → b1 ∨ b2 ≡ b1' ∨ b2
∨₁-cong p rewrite p = refl

∧e-true : ∀ {b1 b2} → b1 ∧ b2 ≡ true → (b1 ≡ true) × (b2 ≡ true)
∧e-true{true}{true} p = (refl , refl)
∧e-true{false} ()
∧e-true{true}{false} ()

∧₁-true : ∀ {b1 b2} → b1 ≡ true → b2 ≡ false → b1 ∧ b2 ≡ false
∧₁-true {true} p1 p2 = p2
∧₁-true {false} () p2

cong1 : ∀{ℓ ℓ'} { A : Set ℓ}{B : Set ℓ'}{a a' : A}(f : A → B) → a ≡ a' → f a ≡ f a'
cong1 f refl = refl

cong2 : ∀{ℓ ℓ' ℓ''} { A : Set ℓ}{B : Set ℓ'}{ C : Set ℓ''}{a a' : A}{b b' : B} (f : A → B → C) → a ≡ a' → b ≡ b' → f a b ≡ f a' b'
cong2 f refl refl = refl

infixl 5 _≤ℕ_ _=ℕ_

_=ℕ_ : ℕ → ℕ → Bool
0 =ℕ 0 = true
suc x =ℕ suc y = x =ℕ y
_ =ℕ _ = false

=ℕ-refl : ∀ (x : ℕ) → (x =ℕ x) ≡ true
=ℕ-refl 0 = refl
=ℕ-refl (suc x) rewrite (=ℕ-refl x) = refl

=ℕ-sym-true : ∀ (x y : ℕ) → (x =ℕ y) ≡ true → (y =ℕ x) ≡ true
=ℕ-sym-true 0 0 p = refl
=ℕ-sym-true 0 (suc y) ()
=ℕ-sym-true (suc x) 0 ()
=ℕ-sym-true (suc x) (suc y) p rewrite =ℕ-sym-true x y p = refl

=ℕ-sym-false : ∀ (x y : ℕ) → (x =ℕ y) ≡ false → (y =ℕ x) ≡ false
=ℕ-sym-false 0 0 ()  
=ℕ-sym-false 0 (suc y) p = refl
=ℕ-sym-false (suc x) 0 p = refl
=ℕ-sym-false (suc x) (suc y) p = =ℕ-sym-false x y p 

Bool-dec : ∀ (b : Bool) → b ≡ true ⊎ b ≡ false
Bool-dec true = inj₁ refl
Bool-dec false = inj₂ refl

=ℕ-to-equiv : ∀ (x y : ℕ) → (x =ℕ y) ≡ true → x ≡ y
=ℕ-to-equiv 0 0 u = refl
=ℕ-to-equiv (suc x) 0 ()
=ℕ-to-equiv 0 (suc y) ()
=ℕ-to-equiv (suc x) (suc y) u rewrite =ℕ-to-equiv x y u = refl

=ℕ-from-equiv : ∀ (x y : ℕ) → x ≡ y → (x =ℕ y) ≡ true
=ℕ-from-equiv x y u rewrite u = =ℕ-refl y

Bool-contr : true ≡ false → ⊥
Bool-contr ()

=ℕ-equiv-false : ∀ (x y : ℕ) → (x =ℕ y) ≡ false → x ≢ y
=ℕ-equiv-false x y u v rewrite =ℕ-from-equiv x y v = Bool-contr u

_≤ℕ_ : ℕ → ℕ → Bool
0 ≤ℕ y = true
(suc x) ≤ℕ (suc y) = x ≤ℕ y
(suc x) ≤ℕ 0 = false

≤ℕ-refl : ∀ (x : ℕ) → (x ≤ℕ x) ≡ true
≤ℕ-refl 0 = refl
≤ℕ-refl (suc x) = ≤ℕ-refl x

≤ℕ-zero : ∀ (x : ℕ) → (x ≤ℕ 0) ≡ true → x ≡ 0
≤ℕ-zero 0 p = refl
≤ℕ-zero (suc x) () 

≤ℕ-total : ∀ (n m : ℕ) → (n ≤ℕ m) ≡ true ⊎ (suc m ≤ℕ n) ≡ true
≤ℕ-total 0 m = inj₁ refl
≤ℕ-total (suc n) 0 = inj₂ refl
≤ℕ-total (suc n) (suc m) = ≤ℕ-total n m 

≤ℕ-trans : ∀ (x y z : ℕ) → (x ≤ℕ y) ≡ true → (y ≤ℕ z) ≡ true → (x ≤ℕ z) ≡ true
≤ℕ-trans x 0 _ p1 p2 rewrite ≤ℕ-zero x p1 = refl
≤ℕ-trans 0 (suc y) _ p1 p2 = refl
≤ℕ-trans (suc x) (suc y) 0 p1 () 
≤ℕ-trans (suc x) (suc y) (suc z) p1 p2 = ≤ℕ-trans x y z p1 p2

≤ℕ-antisymm : ∀ (x y : ℕ) → (x ≤ℕ y) ≡ true → (y ≤ℕ x) ≡ true → x ≡ y
≤ℕ-antisymm 0 0 p1 p2 = refl
≤ℕ-antisymm 0 (suc y) p1 ()
≤ℕ-antisymm (suc x) 0 () p2
≤ℕ-antisymm (suc x) (suc y) p1 p2 = cong1 suc (≤ℕ-antisymm x y p1 p2)

≤ℕ-suc : ∀ (n : ℕ) → (n ≤ℕ suc n) ≡ true
≤ℕ-suc 0 = refl
≤ℕ-suc (suc n) rewrite ≤ℕ-suc n = refl

≤ℕ-suc-left : ∀ (n m : ℕ) → (suc n ≤ℕ m) ≡ true → (n ≤ℕ m) ≡ true
≤ℕ-suc-left n m p = ≤ℕ-trans n (suc n) m (≤ℕ-suc n) p

suc-inj1 : ∀ (x y : ℕ) → suc x ≡ suc y → x ≡ y
suc-inj1 x y u rewrite (=ℕ-to-equiv x y (=ℕ-from-equiv (suc x) (suc y) u)) = refl

suc-inj2 : ∀ (x y : ℕ) → suc x ≢ suc y → x ≢ y
suc-inj2 x y u v rewrite v = ⊥-elim (u refl)

≤ℕ-strict-decrease : ∀ (x y : ℕ) → x ≢ (suc y) → (x ≤ℕ (suc y)) ≡ true → (x ≤ℕ y) ≡ true
≤ℕ-strict-decrease 0 0 _ _ = refl
≤ℕ-strict-decrease 0 (suc y) _ _ = refl
≤ℕ-strict-decrease (suc x) 0 u v rewrite ≤ℕ-zero x v = ⊥-elim (u refl)
≤ℕ-strict-decrease (suc x) (suc y) u v = ≤ℕ-strict-decrease x y (suc-inj2 x (suc y) u) v

+-right-id : ∀ (x : ℕ) → x + 0 ≡ x
+-right-id 0 = refl
+-right-id (suc n) rewrite +-right-id n = refl

+-right-suc : ∀ (x y : ℕ) → x + (suc y) ≡ suc(x + y)
+-right-suc 0 y = refl
+-right-suc (suc x) y rewrite +-right-suc x y = refl

≤ℕ+-mono-left : ∀ (x y z : ℕ) → (x ≤ℕ y) ≡ true → ((x + z) ≤ℕ (y + z)) ≡ true
≤ℕ+-mono-left x y 0 p rewrite +-right-id x | +-right-id y = p
≤ℕ+-mono-left x y (suc z) p rewrite +-right-suc x z | +-right-suc y z = ≤ℕ+-mono-left x y z p

≤ℕ+-mono-right : ∀ (x y z : ℕ) → (x ≤ℕ y) ≡ true → ((z + x) ≤ℕ (z + y)) ≡ true
≤ℕ+-mono-right x y 0 p = p
≤ℕ+-mono-right x y (suc z) p = ≤ℕ+-mono-right x y z p

infixl 4 _≤ℤ_

_≤ℤ_ : ℤ → ℤ → Bool
-[1+ x ] ≤ℤ + y = true
+ x ≤ℤ -[1+ y ] = false
-[1+ x ] ≤ℤ -[1+ y ] = y ≤ℕ x
+ x ≤ℤ + y = x ≤ℕ y

≤ℤ-refl : ∀ x → (x ≤ℤ x) ≡ true
≤ℤ-refl (+ x) rewrite ≤ℕ-refl x = refl
≤ℤ-refl -[1+ x ] rewrite ≤ℕ-refl x = refl

Increasing : ∀ {ℓ} {A : Set ℓ}
              (f : ℕ → A)
              (_≤_ : A → A → Set ℓ) → Set ℓ
Increasing f _≤_ = ∀ (n : ℕ) → f n ≤ f (suc n)
               
-- integer operations from the library (renamed here so we can use them infix in older versions of Agda
_+ℤ_ = Data.Integer._+_
_*ℤ_ = Data.Integer._*_
_-ℤ_ = Data.Integer._-_
-ℤ_ = Data.Integer.-_

⊖-lem : ∀ (x y : ℕ) → 
        (x ≤ℕ y) ≡ true →
        (y ≤ℕ x) ≡ false →
        (y ⊖ x ≤ℤ + 0) ≡ false
⊖-lem (suc x) 0 () p2
⊖-lem 0 0 p1 ()
⊖-lem 0 (suc y) p1 p2 = refl
⊖-lem (suc x) (suc y) p1 p2 = ⊖-lem x y p1 p2

≤ℕ-subtract : ∀ (x y : ℕ) →
              (x ≤ℕ y) ≡ true →
              (y ≤ℕ x) ≡ false →
              ((+ y) +ℤ (- (+ x)) ≤ℤ (+ 0)) ≡ false
≤ℕ-subtract (suc x) 0 () p2
≤ℕ-subtract 0 0 p1 ()
≤ℕ-subtract 0 (suc y) p1 p2 = refl
≤ℕ-subtract (suc x) (suc y) p1 p2 = ⊖-lem x y p1 p2

≤ℤ-subtract : ∀ (n m : ℤ) → 
              (n ≤ℤ m) ≡ true × (m ≤ℤ n) ≡ false → 
              (m +ℤ (- n) ≤ℤ (+ 0)) ≡ false
≤ℤ-subtract (+ x) -[1+ y ] (p1 , ()) 
≤ℤ-subtract (+ x) (+ y) (p1 , p2) = ≤ℕ-subtract x y p1 p2
≤ℤ-subtract -[1+ x ] -[1+ y ] (p1 , p2) = ⊖-lem y x p1 p2
≤ℤ-subtract -[1+ x ] (+ y) (p1 , p2) rewrite +-right-suc y x = refl
