module InnerCostRelator where



open import Level renaming (suc to lsuc)

open import Relators
open import Functors

open import Data.Product
-- open import Data.Product.Relation.Binary.Pointwise.NonDependent

open import Relation.Binary
-- I need >, dont I?
open import Data.Nat
open import Data.Nat.Properties

-- Needed for proofs
open import Relation.Binary.PropositionalEquality hiding (Extensionality)

private
  variable l : Level

module CostRelator (F : Functor {l})(FProp : FunctorProp F)( Γ : Relator F)( ΓProp : RelatorFProp F Γ) where
  open Functor F
  open FunctorProp FProp renaming (ident to fident; comp to fcomp)
  open RelatorFProp ΓProp renaming (ident to Γident; comp to Γcomp; inv to Γinv; mono to Γmono)

  lApp : {p q : Level} {A B : Set p}{C : Set q}
       -> (A -> B)
       -> (A × C -> B × C)
  lApp f (fst , snd) = f fst , snd

  prodℕ : Functor
  prodℕ = record { F₀ = λ X → F₀ (X × ℕ)
                 ; F₁ = λ f x → F₁ (lApp f) x }

  prodProps : FunctorProp prodℕ
  prodProps = record
            { ident = fident
            ; comp = λ f g → fcomp (lApp f) (lApp g) }

  PointwiseProd : {r : Level} {A B : Set l}
                  {X Y : Set r}
                -> (Rₗ : REL A B l)
                -> (Rᵣ : REL X Y r)
                -> REL (A × X) (B × Y) (Level._⊔_ l r)
  PointwiseProd Rₗ Rᵣ (fst , snd) (fst₁ , snd₁) = Rₗ fst fst₁ × Rᵣ snd snd₁

  Pointwise≡→≡ : {A B : Set l } -> (a b : A × B)
               -> PointwiseProd _≡_ _≡_ a  b -> a ≡ b
  Pointwise≡→≡ (fst₁ , snd₁) (.fst₁ , .snd₁) (refl , refl) = refl

  ΓℕRelator : Relator prodℕ
  ΓℕRelator {A} {B} R = Γ (PointwiseProd {Level.zero} {A} {B} {ℕ} R _≥_)

  ΓℕRelProps : RelatorFProp prodℕ ΓℕRelator
  ΓℕRelProps = record
             { ident = λ{ x .x refl
                       -> Γmono _≡_ (PointwiseProd _≡_ _≥_)
                       (λ{ (fst , snd) .(fst , snd) refl → refl , ≤-refl}) x x -- _≡_ ⊆ Pointwise _≡_ _≥_
                          (Γident x x refl)}
             ; comp = λ{ {R = R }{S} x z ev
                      → Γmono ( ( PointwiseProd S _≥_ ) ∘  PointwiseProd R _≥_)  (PointwiseProd (S ∘ R) _≥_)
                              (λ{ (a₁ , a₂) (b₁ , b₂) ((fst , snd₁) , (fst₁ , snd₂) , fst₂ , snd)
                              → (fst , fst₁ , fst₂) , ≤-trans snd₂ snd})
                              x z
                              (Γcomp x z ev)
                              }
             ; inv = λ R f g x y x₁ → Γinv (PointwiseProd R _≥_) (lApp f) (lApp g) x y
                     (Γmono (PointwiseProd (preR f g R) _≥_) (preR (lApp f) (lApp g) (PointwiseProd R _≥_))
                     (λ a b x₂ → x₂)
                     x y x₁)
             ; mono = λ R S x fa fb x₁ → Γmono (PointwiseProd R _≥_) (PointwiseProd S _≥_)
                      (λ { ( a₁ , a₂ ) (b₁ , b₂ ) (fst , snd) → (x a₁ b₁ fst) , snd } )
                      fa fb x₁
             }
