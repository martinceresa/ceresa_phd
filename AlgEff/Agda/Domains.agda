module Domains where

-- Module based on Aaron Stump's one http://homepage.cs.uiowa.edu/~astump/

open import Level hiding (suc ; _⊔_)
open import Relation.Binary.PropositionalEquality
open import Data.Nat
open import Data.Bool
open import Data.Sum
open import Util

module top(ℓ : Level.Level) where

  ℓ' = Level.suc ℓ

  record Poset (X : Set ℓ) : Set ℓ' where
    field _⊑_ : X → X → Set ℓ
          reflexivity : ∀ (x : X) → x ⊑ x
          transitivity : ∀ (x y z : X) → x ⊑ y → y ⊑ z → x ⊑ z
          antisymmetry : ∀ (x y : X) → x ⊑ y → y ⊑ x → x ≡ y

  record ω-chain{X : Set ℓ}(p : Poset X) : Set ℓ' where
    _⊑_ = Poset._⊑_ p
    field f : ℕ → X
          increasing : Increasing f _⊑_

  upperbound : ∀ {X}{p : Poset X} → X → ω-chain p → Set ℓ
  upperbound{p} x c = let open ω-chain c in
                    ∀ (n : ℕ) → f n ⊑ x

  record Predomain (X : Set ℓ) : Set ℓ' where
    field p : Poset X
    open Poset p
    field ⊔_ : ω-chain p → X
          ⊔-upper : ∀ (c : ω-chain p) → upperbound (⊔ c) c
          ⊔-least : ∀ (c : ω-chain p) (u : X) → upperbound u c → (⊔ c) ⊑ u

  record domain (X : Set ℓ): Set ℓ' where
    field pd : Predomain X
    open Predomain pd
    open Poset p
    field ⊥ : X
          ⊥-least : ∀ (x : X) → ⊥ ⊑ x

  ω-chain-mono : ∀ {X} (p : Poset X)(c : ω-chain p) (n n' : ℕ) → let open ω-chain c in
                 n ≤ℕ n' ≡ true →
                 f n ⊑ f n'
  ω-chain-mono p c n 0 u' rewrite ≤ℕ-zero n u' = let open ω-chain c in let open Poset p in reflexivity (f 0)
  ω-chain-mono p c n (suc n') u' with Bool-dec (n =ℕ (suc n'))
  ω-chain-mono p c n (suc n') u' | inj₁ u rewrite =ℕ-to-equiv n (suc n') u = 
    let open ω-chain c in let open Poset p in reflexivity (f (suc n'))
  ω-chain-mono p c n (suc n') u' | inj₂ u = 
    let open ω-chain c in let open Poset p in 
        transitivity (f n) (f n') (f (suc n'))
            (ω-chain-mono p c n n' (≤ℕ-strict-decrease n n' (=ℕ-equiv-false n (suc n') u) u' ))
            (increasing n')

  Poset-cycles-trivial : {X : Set ℓ} (p : Poset X) → let open Poset p in
                         ∀ (a b c : X) → 
                           a ⊑ b → 
                           b ⊑ c → 
                           c ⊑ a → 
                           b ≡ c
  Poset-cycles-trivial p a b c p1 p2 p3 = let open Poset p in antisymmetry b c p2 (transitivity c a b p3 p1) 
