module Functors where

open import Level
open import Relation.Binary.PropositionalEquality hiding (Extensionality)
open import Axiom.Extensionality.Propositional

open import Data.Product


open import Function

private
  variable l : Level

record Functor : Set (suc l) where
  field
    -- Object functor. In this case Set to Set
    F₀ : Set l -> Set l
    -- Map Functor.
    F₁ : {X Y : Set l} -> (X -> Y) -> F₀ X -> F₀ Y

-- record BiFunctor : Set (suc (l ⊔ r)) where
--   field
--     B₀ : Set l -> Set r -> Set (l ⊔ r)
--     B₁ : {X Y : Set l}{A B : Set r}
--       -> (X -> Y) -> (A -> B)
--       -> B₀ X A -> B₀ Y B

--------------------------------------------------------------------------------
-- | Postulated Extensionality at level l
postulate ext : Extensionality l l
-- Since we are working on Cat Set I do not see any problem.
-- No needded after all
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Functors properties
record FunctorProp (F : Functor {l} ) : Set (suc l) where
  open Functor F
  field
    ident : {A : Set _} -> F₁ {A} (λ x -> x) ≡ (λ x -> x)
    comp  : {A B C : Set _}
          -> (f : A -> B) -> (g : B -> C)
          -> F₁ (g ∘ f) ≡ (F₁ g ∘ F₁ f)

FComp : Functor {l} -> Functor -> Functor
FComp G P =
      let open Functor G renaming ( F₀ to G₀; F₁ to G₁ )
          open Functor P renaming ( F₀ to P₀; F₁ to P₁ )
      in
      record
      { F₀ = λ X → G₀ (P₀ X)
      ; F₁ = λ f x → G₁ (P₁ f) x }

FCompProp : (F G : Functor {l}) -- ^ Given two functors F,G
          -> (FP : FunctorProp F) -- ^ its props.
          -> (GP : FunctorProp G)
          -- | Then its composition is a functor
          -> FunctorProp (FComp G F)
FCompProp F G Fp Gp =
          let open Functor G renaming ( F₀ to G₀; F₁ to G₁)
              open FunctorProp Gp renaming (ident to gident; comp to gcomp)
              open Functor F
              open FunctorProp Fp
          in
          record
            { ident = trans (cong (λ f → G₁ f) ident) gident
            ; comp = λ f g → trans (cong (λ f → G₁ f) (comp f g)) (gcomp (F₁ f) (F₁ g)) }

