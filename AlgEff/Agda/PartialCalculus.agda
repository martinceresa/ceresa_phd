module PartialCalculus where

open import Data.Nat hiding (_≟_)
open import Data.Empty
-- Lets go for the easy solution?
open import Data.String
open import Data.List using (List; _∷_; []; any)
open import Relation.Nullary using (Dec; yes; no; ¬_)
open import Relation.Binary.PropositionalEquality using (_≡_; _≢_; refl)

open import Level renaming (suc to lsuc; zero to lzero)

--------------------------------------------------------------------------------
-- Λ stuff
--------------------------------------------------------------------------------
-- since this is a simple proof of concept, I am going to assume that I am
-- working with closed lambda terms where no variable is used twice.
Symbol : Set
Symbol = String

--------------------
-- Lambda terms
--------------------
infix  4  _⊢_
infix  4  _⊣_
infix  4  _∋_
infixl 5  _,_

-- infix  6  ƛ_
-- infix  6  ′_
-- infixl 7  _·_

data Type : Set where
  ⋆ : Type

data Context : Set where
  ∅ : Context
  _,_ : Context -> Type -> Context

data _∋_ : Context -> Type -> Set where
  Z : ∀ {Γ A} -> Γ , A ∋ A
  S_ : ∀ {Γ A B} -> Γ ∋ A -> Γ , B ∋ A

data _⊢_ : Context -> Type -> Set

data _⊣_ : Context -> Type -> Set where
  abs : ∀ {Γ A} -> Γ , A ⊢ A -> Γ ⊣ A
  var : ∀ {Γ A} -> Γ ∋ A -> Γ ⊣ A

data _⊢_ where
  val : ∀{Γ A} -> Γ ⊣ A -> Γ ⊢ A
  app : ∀ {Γ} -> Γ ⊣ ⋆ -> Γ ⊣ ⋆ -> Γ ⊢ ⋆
  to : ∀ {Γ} -> Γ ⊢ ⋆ -> Γ , ⋆ ⊢ ⋆ -> Γ ⊢ ⋆

count : ∀ {Γ} -> ℕ -> Γ ∋ ⋆
count {r , ⋆} zero = Z
count {r , ⋆} (suc n) = S count n
count {∅} n = ⊥-elim impossible
  where postulate impossible : ⊥

-- This represents the term (var n).
#_ : ∀ {Γ} -> ℕ -> Γ ⊢ ⋆
# n = val (var (count n))

-- Context extention lemma
ext : ∀ {Γ Δ}
    -> (∀ {A} -> Γ ∋ A -> Δ ∋ A) -- Γ ⊆ Δ
    -> (∀ {A B} -> Γ , B ∋ A -> Δ , B ∋ A)
ext p Z = Z
ext p (S n) = S p n

----------------------------------------
-- Renaming
----------------------------------------
-- Term rename
trename : ∀ {Γ Δ}
        -> (∀ {A} -> Γ ∋ A -> Δ ∋ A) -- Γ ⊆ Δ
        -> (∀ {A} -> Γ ⊢ A -> Δ ⊢ A)

-- Value rename
vrename : ∀ {Γ Δ}
        -> (∀ {A} -> Γ ∋ A -> Δ ∋ A) -- Γ ⊆ Δ
        -> (∀ {A} -> Γ ⊣ A -> Δ ⊣ A)
vrename p (abs x) = abs (trename (ext p) x)
vrename p (var x) = var (p x)

trename p (val x) = val (vrename p x)
trename p (app x x₁) = app (vrename p x) (vrename p x₁)
trename p (to t t₁) = to (trename p t) (trename (ext p) t₁)

----------------------------------------
-- Simulataneous Substitution
----------------------------------------

----------------------------------------
-- Extension Lemmas
exts : ∀ {Γ Δ}
     -> (∀ {A} -> Γ ∋ A -> Δ ⊢ A) -- Γ ⊆ Δ
     -> (∀ {A B} -> Γ , B ∋ A -> Δ , B ⊢ A)
exts {Γ} {Δ} p Z = val (var Z)
exts p (S t) = trename S_ (p t)

valExts : ∀ {Γ Δ}
     -> (∀ {A} -> Γ ∋ A -> Δ ⊣ A)
     -> (∀ {A B} -> Γ , B ∋ A -> Δ , B ⊣ A )
valExts {Γ} {Δ} p Z = var Z
valExts {Γ} {Δ} p (S t) = vrename S_ (p t)

----------------------------------------
-- Simultaneous substs
-- Value Substitution comes in two flavours
-- inside term t: t [ n := W ]
-- inside value v: v [ W / n ]
--
--
subst→Val : ∀ {Γ Δ}
          -> (∀ {A} -> Γ ∋ A -> Δ ⊣ A)
          -> (∀ {A} -> Γ ⊢ A -> Δ ⊢ A)

-- v [ p ]
substVal : ∀ {Γ Δ}
         -> (∀ {A} -> Γ ∋ A -> Δ ⊣ A)
         -> (∀ {A} -> Γ ⊣ A -> Δ ⊣ A)
substVal p (abs x) = abs (subst→Val (valExts p) x)
substVal p (var x) = p x

-- Substitution of a variable with a value in a Term
-- t [ p ]
subst→Val p (val x) = val (substVal p x)
subst→Val p (app x x₁) = app (substVal p x) (substVal p x₁)
subst→Val p (to t t₁) = to (subst→Val p t) (subst→Val (valExts p) t₁)

----------
-- Sinlge Substitution

-- either change this variable or no.
subst-Val₀ : ∀ {Γ B} -> (Γ ⊣ B) -> ∀ {A} -> (Γ , B ∋ A) -> Γ ⊣ A
subst-Val₀ W Z = W
subst-Val₀ W (S v) = var v

-- t [ v := W ]

_[_] : ∀ {Γ A B}
     -- Given a term t with variaibles in B , Γ
     -> Γ , B ⊢ A
     -- A value with variables in Γ
     -> Γ ⊣ B
     -------------
     -- Results in a term with variiables in Γ
     -> Γ ⊢ A
_[_] {Γ} {A} {B} t W = subst→Val {Γ , B} {Γ} (subst-Val₀ W) t

--------------------------------------------------------------------------------
-- Evaluation
--------------------------------------------------------------------------------

data _⟶_ : ∀ {Γ A} -> (Γ ⊢ A) -> (Γ ⊢ A) -> Set where
         β : ∀ {Γ} {N : Γ , ⋆ ⊢ ⋆} {M : Γ ⊣ ⋆}
           -> (app (abs N) M) ⟶ (N [ M ] )

data Gas : Set where
  gas : ℕ -> Gas

-- Results is Mabe plus an additional open term...
-- ⊥ ⟶ Nothing
-- t ∈ Λ, v ∈ Var, v ∙ t ⟶ Open v t
-- t ∈ Λ₀, v ∈ V₀, t ⟶ₙ v ✓

-- data Results : (Γ : Context)  -> Set where
--   ✓_ : ∀ {Γ} -> ( Γ ⊣ ⋆ ) -> Results Γ
--   Fail : ∀ {Γ} -> Results Γ
--   Open : ∀ {Γ} -> Γ ⊢ ⋆ -> Results Γ

-- eval : ∀ {Γ} -> ℕ -> Γ ⊢ ⋆ -> Results Γ
-- eval zero t = Fail
-- eval (suc n) (val x) = ✓ x
-- eval (suc n) (app (abs x) x₁) =  eval n ( x [ x₁ ] )
-- eval (suc n) (app (var x) x₁) = Open (app (var x) x₁)
-- eval (suc n) (to t t₁) with eval n t
-- ... | ✓ x = eval n ( t₁ [ x ])
-- ... | Fail = Fail
-- ... | Open x = Open (to x t₁)

open import MaybeRelator

Term₀ : Set
Term₀ = ∅ ⊢ ⋆

Value₀ : Set
Value₀ = ∅ ⊣ ⋆

_⇓_ : Term₀ -> ℕ -> Maybe₀ Value₀
t ⇓ zero = Nothing
val x ⇓ suc n = η x
app (abs x) x₁ ⇓ suc n = (x [ x₁ ])  ⇓ n
to t t₁ ⇓ suc n = ( t ⇓ n ) >>= λ v → ( t₁ [ v ]) ⇓ n

-- Playground
-- Churchs naturals

-- λ f x . x
czero : ( ∅ ⊣ ⋆ )
czero = abs (val (abs ( # 0)))

-- λ n f x . f (n f x)
-- to (n ∙ f) nf. to (nf ∙ x) n'. app f n'
csuc : ( ∅ ⊣ ⋆ )
csuc = abs (val (abs (val (abs -- 2 n, 1 f, 0 x
       (to (app (var ( count 2)) (var ( count 1)))
       -- 3 n, 2 f, 1 x, 0 nf
       (to (app (var (count 0)) (var (count 1)))
       -- 4 n, 3 f, 2 x, 1 nf, 0 n'
       (app (var (count 3)) (var (count 0)))))
       ))))

cone : ( ∅ ⊢ ⋆ )
cone = app csuc czero
