module MaybeRelator where

open import Relation.Binary hiding (Poset)
open import Relation.Binary.PropositionalEquality
open import Level

open import Relators
open import Functors
open import Data.Product

----------------------------------------
-- Maybe Proof of concept
-- Object mappings
data Maybe₀ (X : Set) : Set where
  Nothing : Maybe₀ X
  Just : (x : X) -> Maybe₀ X

-- Morphism mapping
Maybe₁ : {X Y : Set} -> (X -> Y) -> Maybe₀ X -> Maybe₀ Y
Maybe₁ f Nothing = Nothing
Maybe₁ f (Just x) = Just (f x)

-- Maybe functor definition
Maybe : Functor
Maybe = record { F₀ = Maybe₀ ; F₁ = Maybe₁ }

-- Maybe is a functor
maybeFunctor : FunctorProp Maybe
maybeFunctor = record
             { ident = ext λ{ Nothing → refl ; (Just x) → refl}
             ; comp = λ f g →
               ext λ{ Nothing → refl
                   ; (Just x) → refl}
             }

----------------------------------------
-- Some easy defs.
----------------------------------------
η : {X : Set} -> (x : X) -> Maybe₀ X
η = Just

kl : {X Y : Set} -> (f : X -> Maybe₀ Y) -> Maybe₀ X -> Maybe₀ Y
kl f Nothing = Nothing
kl f (Just x) = f x

_>>=_ : {X Y : Set} -> Maybe₀ X -> (X -> Maybe₀ Y) -> Maybe₀ Y
mx >>= f = kl f mx


----------------------------------------
-- Inductively defined Partiality Relator.
data LRel {X Y : Set} (R : REL X Y zero) : REL (Maybe₀ X) (Maybe₀ Y) zero where
  lBot : (my : Maybe₀ Y) -> LRel R Nothing my
  RLift : (x : X) -> (y : Y) -> (R x y) -> LRel R (Just x) (Just y)
----------------------------------------

----------------------------------------
-- Maybe's relator properties
maybeProps : RelatorFProp Maybe LRel
maybeProps = record {
           ident = λ{ Nothing y x₁ → lBot y
                   ; (Just x) .(Just x) refl → RLift x x refl}
         ; comp = λ{ Nothing z x₁ → lBot z
                  ; (Just x) .(Just y₁) (.(Just y) , RLift .y y₁ x₂ , RLift .x y x₁)
                  → RLift x y₁ (y , x₂ , x₁)}
         ; inv = λ{ R f g Nothing y x₁ → lBot (Maybe₁ g y)
                 ; R f g (Just x) .(Just y) (RLift .x y x₁) → RLift (f x) (g y) x₁}
         ; mono = λ{ R S x Nothing fb x₁ → lBot fb
                  ; R S x (Just x₂) .(Just y) (RLift .x₂ y x₁)
                  → RLift x₂ y (x x₂ y x₁)}
         }
----------------------------------------

----------------------------------------
-- Maybe Discrete Order!
open import Domains

open top (Level.zero)

data DiscreteOrder {X : Set} : Maybe₀ X -> Maybe₀ X -> Set where
  lBot : (x : Maybe₀ X) -> DiscreteOrder Nothing x
  eJust : (x : X) -> DiscreteOrder (Just x) (Just x)

maybePoset : {X : Set} -> Poset (Maybe₀ X)
maybePoset = record
           { _⊑_ = DiscreteOrder
           ; reflexivity = λ{ Nothing → lBot Nothing
                           ; (Just x) → eJust x}
           ; transitivity = λ{ .Nothing y z (lBot .y) x₂ → lBot z
                            ; .(Just x) .(Just x) .(Just x) (eJust x) (eJust .x) → eJust x}
           ; antisymmetry = λ{ .Nothing .Nothing (lBot .Nothing) (lBot .Nothing) → refl ; .(Just x) .(Just x) (eJust x) x₂ → refl}
           }

open import Util



maybePredomain : {X : Set} -> Predomain (Maybe₀ X)
maybePredomain = record
                 { p = maybePoset
                 ; ⊔_ = λ{ record
                      { f = f
                      ; increasing = increasing } → {!!}}
                 ; ⊔-upper = λ c n → {!!}
                 ; ⊔-least = λ c u x → {!!} }
