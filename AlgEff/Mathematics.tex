\section{Preliminaries and Language}

In this section, we introduce several mathematical definitions we use
throughout the paper, mainly to fix notation. A more in-depth treatment can be
found in standard
references~\citep{maclane:71,davey_priestley_2002,streicher,manesarbib}.

We assume the reader to be familiar with basic domain theory (\wCPPO, monotone
and continuous functions), basic order theory (lattice, complete lattice) and
basic category theory.
%
In particular, we assume the reader to be familiar with monads as models of
computational effects~\citep{Moggi:Monads} and the notion of \emph{Kleisli} triple~\citep{maclane:71}.

\subsection*{Partially Ordered Set}

%% Partial order, and Poset
% A \emph{partial order} (poset) is a pair \((P, :<_P)\) of a non-empty set \(P\) and a
% reflexive, transitive, and antisymmetric relation \((:<_P)\).

%% Chain
% Let \((P, :<_P)\) be a poset, a subset \(X\) of \(P\) is a chain if
% and only if \(X\) is non-empty and for all \(x, y\) in \(X\)
% \((x :<_P y)\) or \((y :<_P x)\) holds.
% %%
% An \wchain\ is an infinite sequence \(\chain{x}\) of elements of \(P\) such that
% \(x_i :<_P x_j \) whenever \(i \leq j\).

% %% Complete Poset or pre-domain
% A poset \((D, :<_{D})\) is an \(\omega\)-complete partial order (\wCPO ), or \emph{pre-domain}, if any
% \wchain\ in \(D\) has least upper bound (lub) in \(D\). Moreover, a poset
% \((D, :<_{D})\) is an \(\omega\)-complete pointed partial order (\wCPPO ), or
% \emph{domain}, if it is an \wCPO\ with a distinguished element \(\bot_D\) in \(D\),
% such that \(\bot_D\) is the minimum of \(D\) according to \((:<_D)\).
% % In other words, for any \(d\) in \(D\), \(\bot_D :<_D d\).

We note domains as triples \((D , :<_D, \bot_D)\) and omit subscripts when they
can be inferred from context, writing \(:<\) and \(\bot\) for \(:<_D\) and \(\bot_D\),
respectively. Given an \wchain\ \(\chain{x}\) we denote its lub as
\(\bigsqcup_{D} \{ x_n \mathrel{|} n < \omega \}\). We drop the \(D\)
subscript when it can be inferred and write the lub as \(\LUB{x_n}{n}\).

\begin{example}\label{ex:wCPPO}
   Let \(X\)  be a set. Each of the following examples is a domain:
   \begin{itemize}
  \item The flat lifting \(X_\bot\) consists of the set \(X + \{\bot\}\), ordered as
follows:
\begin{displaymath}
  x \sqsubseteq y \iff
  \begin{dcases}
    x = in_r(\bot)\\
    x = in_l(x') \land y = in_l(y') \land x' = y'
  \end{dcases}
\end{displaymath}
The bottom element is \(in_r(\bot)\).
%   \item \mauro{this is exactly the same example as before, with \( X\) specialised to \(X + E\)} Let \(E\) be a set, the disjoint union \({(X + E)}_\bot\) is ordered as follows:
%     \begin{displaymath}
%       x \sqsubseteq y \iff
%       \begin{dcases}
%         x = in_r(\bot) \\
%         x = in_l(in_r(e)) \land y = in_l(in_r(e')) \land e = e' \\
%         x = in_l(in_l(x')) \land y = in_l(in_l(y')) \land x' = y'
%       \end{dcases}
%     \end{displaymath}
% The bottom element is \(in_r(\bot)\).
  \item The set \(\PP(X)\) of all subsets of \(X\) ordered by
inclusion. The least upper bound of a chain is defined as their union, and the
bottom element is the empty set.
  \item The set of sub-distributions with countable support \(\mathcal{D}(X)\) defined as follows:
    \begin{displaymath}
      {\{\mu : X -> [0,1] \mid \supp(\mu) \ \text{countable}, \sum_{x
          \in X} \mu(x) \leq 1\}}
    \end{displaymath}
The set \(\mathcal{D}(X)\) is ordered pointwise:
\begin{displaymath}
\mu \sqsubseteq \nu \iff \forall x \in X, \mu(x) \leq \nu(x)
\end{displaymath}
The least element is the constant zero distribution \((x \mapsto 0)\).
   \end{itemize}
\end{example}

\begin{definition}[Inductive Relation]
  Let \((D, :<, \bot)\) be a domain, \(E\) be a set,
  and \(\RR \subseteq D\times E\) be a relation.
  %
  The relation \(\RR\) is \emph{inductive} if:
  \begin{itemize}
  \item \(\RR\) respects \(\bot\): \(\forall e \in E, \bot \mathrel{\RR} e\),
  \item and for any \wchain\ \(\chain{u}\) in \(D\), and \(v\) in \(E\):
    \[(\forall n < \omega, u_n \mathrel\RR v) \implies (\LUB{u_n}{n})
      \mathrel{\RR} v \]
  \end{itemize}
  \end{definition}

% \begin{definition}[Monotone Function]\label{def:mono:fun}
%   Let \((D, :<_P)\) and \((E, :<_E)\) be posets. A
%   function \(f : D -> E\) is \emph{monotone} iff for all elements \(x, y\) in \(D\),
%   \(x :<_D y\) implies \(f(x) :<_E f(y)\).
% \end{definition}
%

% Note that monotone functions preserve \wchain s.
% %
% That is,
% given a monotone function \(f\) and an \wchain\ \(\chain{x}\), the result of
% applying \(f\) to each element of the chain is again an \wchain\ \(\{f(x_n)\}_{n
% \in \omega}\).

% \begin{definition}[Continuous Function]\label{def:cont:fun}
%   Let \((D, :<_P)\) and \((E, :<_E)\) be pre-domains. We say a
%   function \(f : D -> E\) is \emph{continuous} iff \(f\) preserves lubs. That is, for any
%   \wchain\ \(\chain{x}\) in D:
%   \[
%     f (\bigsqcup\limits_{D} \{ x_n \mid n < \omega \})
%     = \bigsqcup\limits_{E} \{ f(x_n) \mid n < \omega \}
%   \]
% \end{definition}

% Note that continuous functions are also monotone functions, thus they respect
% \wchain s. However, not every monotone function between pre-domains is continuous.
% Continuous functions are functions that

%% We assume the reader already knows about this.
%% A function \(f \colon (P, :<_{P}) \to (Q, :<_{Q})\) between posets is monotone if
%% preserve \wchain s.
%% %
%% While a function \(g \colon (D_{1}, :<_{D_{1}}) \to (D_{2}, <_{D_{2}}))\) between
%% pre-domains is continuous if preserves structure from one pre-domain to another,
%% \(g\) translates every \wchain\ in \(D_{1}\) to an \wchain\ in \(D_{2}\), while
%% also preserving lubs.

% \begin{theorem}[Kleene Fixed Point]\label{thm:kleene:fixpoint}
%  Let \((D, :<, \bot)\) be a domain and \({f:D->D}\) a continuous function. Then
%  the least fixed point of \(f\) exists in \(D\). That is, there exists an element \(\mu f\) in
%  \(D\), such that:
%  \begin{itemize}
%  \item \(\mu f = \bigsqcup_{n < \omega} f^n(\bot)\)
%  \item \(f(\mu f) = \mu f\)
%  \item for any \(x\) in \(D\), if \(f(x) = x\) then \(\mu f :< x\)
%  \end{itemize}
% \end{theorem}

% We use Kleene Fixed Point Theorem to define the evaluation function of the
% language presented in Section~\ref{sec:Calculus}.

% \mauro{The Kleene Fixed point theorem is not even mentioned un section 2.2. May we erase it?}

% \begin{definition}[Lattice]
%   A poset \((P, :<_P)\) is called a \emph{lattice} if every pair of elements of \(P\)
%   has a supremum and an infimum in \(P\).
% \end{definition}

% \begin{definition}[Complete Lattice]
%   A lattice \((L, :<_L)\) is called \emph{complete} if for every subset \(X\) of \(L\),
%   both supremum and infimum of \(X\) exists in \(L\).
% \end{definition}

% Complete lattices also have minimum and maximum elements.
% %
% Let \((L, :<_L)\) be a complete lattice, in particular \(L\) is a
% subset of \(L\), and thus, has a minimum element, noted \(\bot_L\),
% and a maximum element, noted \(\top_L\).

\begin{theorem}[Knaster-Tarski]\label{thm:Knaster:Tarski}
 Let \((L, :<_L)\) be a complete lattice, and let \(f : L -> L\) be a monotone
 function, then the set of fixed points of \(f\) is itself a complete lattice.
\end{theorem}

Let \((L, :<_L)\) be a complete lattice, and \(f : L -> L\) be a monotone
function. Since the set of fixed points of \(f\) is a complete lattice, it has a
minimum element, \(\mu f\), the least fixed point, and a maximum element, \(\nu
f\), the greatest fixed point. In other words, Knaster-Tarski presents us with
two principles:
\begin{description}
\item[Induction:] \(\mu f\) is the least pre-fixed point of the pre-fix points of
\(f\). For any \(x\) of \(L\) such that \(f(x) :<_L x\), then \(\mu f :<_L x\).
\item[Coinduction:] \(\nu f\) is the greatest pos-fixed point of the pos-fix points
of \(f\). For any \(x\) of \(L\), such that \(x :<_L f(x)\), then \(x :<_L \nu
f\).
\end{description}

We use the Knaster-Tarski Theorem to define relations over the complete lattice of
relations over sets.
% as the greatest fixed point of a monotonic operator between relations.
In the following sections, we define relations through order-preserving
functions, and thus, Knaster-Tarski guarantees the existence of fixed points and
gives us the useful principles mentioned above.

% Kleene's Fixed-Point theorem may seem a weaker version of Knaster-Tarski.
% %
% However, the latter requires the existence of a supremum, which does not make
% much sense in the context of program semantics.


\subsection*{Effects}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Category Theory Stuff
We use basic category theory, and in particular \emph{monads}, to provide semantics to effectful
computations.
% , and thus, we start from the notion of a \emph{monad}.

\begin{definition}[Monad]
  Let \(\cat{C}\) be a category.
  %
 A monad \(\textbf{T} = (T , \mu, \eta)\) on category \(\cat{C}\) is an
 endofunctor \(T : \cat{C} -> \cat{C}\) together with two natural
 transformations \(\eta_{X} \colon X \to T (X), \mu_{X} \colon T^{2}(X) \to  T(X)\), such that the following
 diagrams commute:
 \[
\begin{tikzcd}
  & T^3 \ar[r, "T\mu"] \ar[d, swap, "\mu T"]
            & T^2 \ar[d, "\mu"] \\
  & T^2 \ar[r, swap,"\mu"] & T
\end{tikzcd}
\begin{tikzcd}
  & T \ar[r, "T\eta"] \ar[dr, swap, "="]
            & T^2 \ar[d, "\mu"] & T \ar[l, swap, "\eta T"] \ar[dl, "="]\\
  & & T &
\end{tikzcd}
\]
\end{definition}

%% Restricting our world to set, and rel, plus Cat notation.
We denote the category of sets and total functions as \(\Set\).
%
Unless otherwise noted, we work in \(\Set\).
% the category of sets and total functions.
% where triples coincide with the notion of \emph{monads}, and morphisms with functions.
Moreover, we assume endofunctors to be endofunctors on \(\Set\) and use \(\ident_X\) to
denote the identity morphism on a set \(X\).

Since we work in the \(\Set\) category, we can define monads in terms of the
\emph{bind operator} \((>>=)\) instead of \(\mu\).
%
The bind operator takes an element \(c\) of \(T(X)\), together with a
function \(f : X -> T(Y)\) and returns an element \((c >>= f)\) of
\(T(Y)\).
%
The bind operator is defined in terms of \(\mu\) and the underlying functor of
\(T\) as follows: \((c >>= f) \doteq \mu \circ (T(f)(c))\).
%
% The diagrams in the monad definition above can be expressed in terms of the bind operator as follows:
% \begin{align}
%   (\eta(x) >>= f) & = f(x)\\
%   (c >>= \eta) & = c\\
%   ((c >>= f) >>= g) & = c >>= (\lambda y.~f(y) >>= g)
%   \end{align}
% \mauro{Are these equations used anywhere in the paper? If they are not we may erase them}
%
We use the presentation of monads with a bind operator for writing our program examples.

\subsection{Introducing Effects}\label{sec:computational:model}

Operations, such as \(\oplus_r\) in Example~\ref{ex:Prob:Eff}, are introduced by
uninterpreted symbols given by a \emph{signature}\citep{Plotkin:Alg.Ops}.

\begin{definition}[Signature]
A \emph{signature} is a pair \((\Sigma, \alpha)\) of a set of
symbols \(\Sigma\) and an arity function, \(\alpha : \Sigma -> \N\).
  \end{definition}

Operations are given meaning by algebras dictating how operations are
interpreted.

\begin{definition}[\(\Sigma\)-Algebra]
Given a signature \((\Sigma, \alpha)\), a \emph{\(\Sigma\)-algebra} is
a pair \((A,(\cdot)^A)\) consisting of a carrier set \(A\) and a
family of maps \((\cdot)^A\) interpreting each of the symbols in
\(\Sigma\), such that, for each symbol \(\sigma\) in \(\Sigma\) with
arity \(\alpha(\sigma) = n\) there is an n-ary function
\(\sigma^A \colon A^{n} \to A\).
\end{definition}

We refer to a signature \((\Sigma, \alpha)\) as \(\Sigma\) when the arity can be inferred from context.

Once symbols are added, we need to add the appropriate
algebraic structure to interpret such symbols as \emph{suitable functions}.
%
Following the order structures presented before, it makes sense to employ
\(\Sigma\)-algebras carrying domains instead of unordered sets, and consider
the interpretation of symbols as continuous functions.

\begin{definition}[Continuous \(\Sigma\)-algebras~\citep{Goguen:IAS}]
  Given a signature \(\Sigma\)
  % Let \(\Sigma\) be a signature
  and \((A , {(\cdot)}^{A})\) a \(\Sigma\)-algebra.
  We say \((A , (\cdot)^A)\) is a \emph{continuous \(\Sigma\)-algebra} if \(A\) is a
  domain, and for each symbol \(\sigma\) in \(\Sigma\), the function \(\sigma^A\)
  is continuous in each argument.
\end{definition}


\begin{example}\label{ex:exception:alg}
  Let \(E\) be a non-empty set of symbols representing exceptions.
  %
  Define the set \(\Sigma = \{\textbf{raise}_e \mathrel{|} e \in E\}\), and
  \(\alpha({raise_e}) = 0\) for each \(e \in E\), such that \((\Sigma,
  \alpha)\) forms a signature.
  % 
  For each set \(X\), we define a \(\Sigma\)-algebra with carrier set
  \({(X + E)}_{\bot}\), and for each \(e \in E\) define the constant
  interpretation function \({\textbf{raise}_e}^{{(X + E)}_{\bot}} = in_l(in_r(e))\).
  % 
  The above \(\Sigma\)-algebra is continuous
since Example~\ref{ex:wCPPO} shows that the carrier set is a domain,
and each symbol \({\textbf{raise}}_e \in \Sigma\) is interpreted as a
constant function, and thus, trivially continuous.
\end{example}

\begin{example}\label{ex:non-deter:alg}
  Let \(\Sigma = \{ \oplus \}\) be a singleton set.
  %
  The symbol \(\oplus\) represents the binary operation of
  non-deterministic choice, and thus, \(\alpha(\oplus) = 2\).
  % 
  For each set \(X\), we define a \(\Sigma\)-algebra with carrier set
  \( \PP(X) \), and the interpretation function
  \(\oplus^{\PP(X)}(P, Q) = P \cup Q \).
  %
  Example~\ref{ex:wCPPO} shows that for each set \(X\), \(\PP(X)\) is
  a domain.
  %
  The union operator is continuous because the elements
in \(\PP(X)\) are ordered by set inclusion.
\end{example}

\begin{example}\label{ex:prob:alg}
  Let \(\Sigma = \{ {\oplus}_p \mid p \in [0,1]\}\) be a signature
  composed of a family of symbols indexed by elements in the continuous
  segment \([0,1]\).
  % 
  Each symbol \({\oplus}_p\) represents a probabilistic binary
  operator, and thus, \(\alpha({\oplus}_p) = 2\).
  % 
  For each set \(X\), we define a \(\Sigma\)-algebra with carrier set
  \( \mathcal{D}(X) \), and for each symbol
  \({\oplus}_p \in \Sigma\) define the interpretation function
  \( {{\oplus}_p}^{\DD(X)}(\mu,\nu) = x \mapsto p \cdot \mu(x) + (1 - p) \cdot \nu(x) \).
  %
  Example~\ref{ex:wCPPO} shows that the carrier set is in fact a
  domain. Moreover, for each \(p \in [0,1]\) the interpretation function
  \({{\oplus}_p}^{\DD(X)}\) is continuous in both arguments since
  function application and arithmetic operations are
  continuous.
\end{example}

\subsubsection{Algebraic Effects}
%
Each signature \(\Sigma\) describes the set of operations we want to add to our
language.
%
Each operation produces some computational effects that are eventually
interpreted by a monad.
% that are later interpreted by a monad.
% Such operations produce effects as interpreted by a monad.
%
Since operations are interpreted as continuous operations, we are interested in
monads carrying continuous \(\Sigma\)-algebra structure.

\begin{definition}[Order over a Monad]
  Let \(T\) be a monad.
  % Given a monad \(T\),
  An \emph{order over \(T\)} consists of a family of
  relations \((\sqsubseteq)\) and elements \(\bot\) such that for each set \(X\),
  \(\bot_X\in T(X)\),
  \( (\sqsubseteq_X) \subseteq T(X) \times T(X)\) are
  such that \((T(X), \sqsubseteq_X, \bot_X)\) is an \wCPPO\, and the bind operator is continuous in both arguments:
  \begin{align*}
    \lub_{n < \omega}(x >>= f_n) &= x >>= (\lub_{n < \omega} f_n)  \\
    \lub_{n < \omega}(a_n >>= f) &= (\lub_{n < \omega} a_n) >>= f
  \end{align*}
  where \(\chain{f}\) and \(\chain{a}\) are \wchain s, \(f_n : X -> T(Y)\)
and \(a_n : T(X)\) for all \(n < \omega\), \(x : T(X)\), and \(f : X ->
T(Y)\).
%
An \emph{ordered monad} is a monad that has an order.
  \end{definition}

\begin{definition}[\(\Sigma\)-algebra for a monad]
  Let \(\Sigma\) be a signature, and \(T\) be a monad.
  %
  A \(\Sigma\)-algebra for monad \(T\) is a family of
  \(\Sigma\)-algebras \(\Phi\) such that for each set \(X\),
  it equips \(T(X)\) with a \(\Sigma\)-algebra \({\Phi}_{X}\).
\end{definition}

\begin{definition}[\(\Sigma\)-continuous Monad]
  Given a signature \(\Sigma\), a monad \(T\),
  and a \(\Sigma\)-algebra for monad \(T\) \(\Phi\).
%
  % Let \(\Sigma\) be a signature, \(T\) be a monad, and
  % \(\Phi\) a \(\Sigma\)-algebra for monad \(T\).
  %
  We say that \emph{\(T\) is \(\Sigma\)-continuous}, or carries a
continuous \(\Sigma\)-algebra, if \(T\) has an order \((\sqsubseteq)\) such
that for each set \(X\), \(\Phi_X\) is a continuous \(\SigmaAlg\) with
respect to the order \((\sqsubseteq_X)\).
  \end{definition}

In other words, let \(\Sigma\) be a signature, and \(T\) be a
\(\Sigma\)-continuous monad.
%
For each set \(X\), \(T(X)\) is equipped
with a \(\Sigma\)-continuous algebra.
%
Therefore, for each symbol \(\sigma\) in \(\Sigma\) with arity
\(
\alpha(\sigma) \geq n > 0 \), its interpretation in \(T(X)\) is continuous in each argument:
\[
  \sigma^{T(X)}(\ldots, \LUB{a_n}{n}, \ldots) = \LUB{\sigma^{T(X)}(\ldots, a_n, \ldots)}{n}
\]

% \begin{note}[Algebraic]
% For each operation symbol \(\sigma \in \Sigma\), with arity \(\alpha(\sigma) =
% n\) and any set \(X\), we associate its interpretation in monad \(T\),
% \( \sigma^{TX} : (T X)^n -> T X \), such that for any \(f : X -> T Y\),
% \(f^{\dagger}\) is a \(\Sigma\text{-algebra}\). Diagrammatically:
% \begin{tikzcd}
%   & (T X)^n \ar[r, "(f^{\dagger})^n"] \ar[d,swap, "\llbracket op \rrbracket_X"]
%             & (T Y)^n \ar[d, " \llbracket op \rrbracket_Y"] \\
%   & T X \ar[r,swap, "f^{\dagger}"] & T Y
% \end{tikzcd}
% \todi{IDK where I can put this.}
%   \end{note}


\begin{example}\label{ex:monads}
  The monadic structure corresponding to each of the
orders in Example~\ref{ex:wCPPO} is as follows:
  \begin{description}
  \item[Partiality] Partial computations are modeled using the lifting
    monad.
    %
    For any set \(X\), and \(\bot_X\) not in \(X\):
    \begin{align*}
      X_{\bot} & \triangleq X + \{ \bot_X \} \\
      \eta_{\bot}(x) & \triangleq in_l (x) \\
      m >>= f & \triangleq
                 \begin{dcases}
                   f(x) & \text{if } m = in_l(x) \\
                   \bot_{Y} & \text{if } m = in_r(\bot_{X})
                 \end{dcases}
    \end{align*}
    Where \(m \in X_{\bot}\), and \(f : X -> Y_{\bot}\).

    Partiality handles \emph{undefined} values. An element of \(X_\bot\) is
either an element \(x\) of \(X\) noted as \(in_r(x)\), or it is undefined noted
as \(in_l(\bot_X)\).

    % The interpreter presented in Section~\ref{sec:Calculus} handles
    % \emph{non-termination} explicitly, thus partiality monad presents the basic
    % structure, interpreting non-termination as undefined values.

  \item[Exception] Let \(E\) be a set of symbols representing exceptions.
    The set \((X + E)_\bot\) is a monad,
    following the same structure as the lifting monad.

  \item[Powerset] Let \(X\) be a set, the set of subsets of \(X\)
is a monad called the powerset monad.
    \begin{align*}
      P(X)&\triangleq \PP(X)\\
      \eta_{P}(x) &\triangleq \{x\} \\
      mx >>= f & \triangleq \bigcup_{x \in mx}(f(x))
    \end{align*}
    Where \(mx \in \PP(X)\) and \(f : X -> \PP(Y)\).

  \item[Sub-Distribution] The set of sub-distributions is a monad.
    \begin{align*}
      \mathcal{D}(X) & \triangleq \{ \mu : X -> [0 , 1] \mathrel{|}\mathop{supp}(\mu) \text{
                       countable},
                       (\Sigma_{x \in X} \mu(x)) \leq 1 \} \\
      \eta_{\mathcal{D}}(x) & \triangleq y |->
                              \begin{dcases}
                                1 \ \text{if} \ x = y \\
                                0 \ \text{otherwise}
                              \end{dcases} \\
      \mu_X >>= f & \triangleq y |-> {\Sigma_{x \in X} (\mu_x(x) \cdot f(x)(y))}
    \end{align*}
    Where \(\mu_x \in \mathcal{D}(X)\) and \(f : X -> \mathcal{D}(Y)\).
  \end{description}
\end{example}

% Each order in Example~\ref{ex:wCPPO} equips each of the monads presented above
% with an order. Moreover, all bind operations are continuous respecting each
% order, and except for lifting monad, each monad is \(\Sigma\)-continuous
% respecting each \(\Sigma\)-algebra.

We have presented three different algebraic effects:
exceptions (Example~\ref{ex:exception:alg}), non-determinism
(Example~\ref{ex:non-deter:alg}), and probabilistic operators
(Example~\ref{ex:prob:alg}).
%
Each example was introduced as a signature equipped with
a \(\Sigma\)-continuous algebra.
%
We also defined three different \wCPPO s and showed them to be monads
in the example above, and thus, ordered monads.
%
Finally, each monad is a \(\Sigma\)-continuous monad for
its corresponding algebraic effect.
