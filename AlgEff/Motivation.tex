%% \subsection{Improvement in Effectful Languages}\label{sec:Motivation}

Classic improvement theory works on pure languages that may
diverge (in a sense, the definition of purity of the Haskell
language%~\citep{Marlow.Haskell}
). Hence, we consider non-termination as
an explicit effect in all our languages.

Thanks to the developments made by
\citet{Sands:Op:Theories,Sands97improvementtheory,Sands:POPL97}, we know
how to compare pure programs.
%
But suppose our language has effects, \emph{when can we
say that a given program is better than another?}

\begin{example}[Probabilistic Effects]\label{ex:Prob:Eff}
Assume a lambda calculus equipped with a binary probabilistic choice
operation \(\oplus_{r}\) that chooses its first argument with probability \(r\)
and its second argument with probability \(1-r\).
%% Let \(\oplus_{r}\) be a probabilistic choice operation such that it chooses the
%% left side with probability \(r\) and the right side with probability \(1 - r\).
%% When \(r=\frac{1}{2}\), we omit the subscript and simply write \(\oplus\).
%
%% We call probabilistic programs to programs wrote using simple lambda calculus
%% with the probabilistic choice operation added as a new binary term constructor.
%
When can we say that a probabilistic term improves another?
More generally, how do we prove improvements in presence of probabilistic effects?
\end{example}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{example}[Common Subexpression Elimination]\label{ex:CSE}
%   Consider a language with \textit{let} expressions and integer operations.
%   %
%   Let \( M \) be an expression which evaluates to an integer.
%   %
%   Intuitively, a term that sums twice the same expression \(M\) can be improved
%   by first calculating the integer (once) and then calculating the addition.
%   \[
%   (M + M) \cimproved \LLet{w}{M}{w + w}
%   \]

%   Let us abstract the pattern \( ([-] + [-]) \) as a context \(\CC\), and present
%   the same optimization above as:
%   \[
%   \CC [[ M ]] \cimproved \LLet{w}{M}{\CC [[ w ]]}
%   \]

% The above formula is a simple way of describing the improvement obtained by the
% well-known optimization called \emph{Common Subexpression Elimination} (CSE).

% When is such a transformation an improvement?
% %
% It is well known that in absence of effects, that is in \textbf{pure} functional
% programming languages, CSE is a correct compiler
% optimization~\citep{olaf:CSE:lazy,Appel:CPSComp}.

% Is \emph{CSE} an improvement in presence of effects?
% %
% Consider CSE in a language with \(print\) operations and a sequential
% operator '\(,\)' , and let \( M = (print(\text{``Hi!''}), 1) \).
% %
% When we execute \( (M + M) \), we would like to see ``Hi!'' printed twice on the
% screen, not just once as it would happen when \((\LLet{w}{M}{w + w})\) is
% evaluated.
% %
% Therefore, CSE is \emph{not valid} in this case.
% %
% However, when CSE is a valid optimization?
% %
% For example, what if we \emph{ignore} output operations and only perform
% observations in the result of the program execution, is CSE a valid
% optimization then?
%
% We explore some of these questions but we do not have a general answer to them,
% in this article we focus on having a working improvement theory with effects.
% \end{example}

We have posed two different questions that cannot be
answered in a theory of improvement that does not take effects into account:
%
What does improvement mean in effectful languages?
%
How can we prove optimizations?

This paper aims to answer the first question and to explore what may be needed
to answer the second one.
%
We follow a method proposed recently by \citeauthor{Eff:App:Bisim} and the
intuition proposed by~\citeauthor{Sands:Op:Theories}.
%
\citet{Eff:App:Bisim} developed a method to study
congruence relations in the presence of effects which we refine to \emph{expose
intensional analysis}.
%
\citet{Sands:Op:Theories} proposed the intuition
of improvement as a \emph{specialized notion of congruence} where cost between
programs are compared.
%
The main idea of this paper is to connect both works and derive notions of
improvement in presence of effects.
