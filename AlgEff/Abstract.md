Improving Algebraic Effects
================================================================================

So, the idea here is that we can make an improvement theory out of a lambda
calculus with algebraic effects.

In the calculus proposed by Ugo Dal Lago there is a monadic evaluator.
Given a signature Sigma.
Terms(Sigma):
```BNF
M,N ::= return V | V W | M to x.N | op(M,...,M)
V,W ::= x | \x.M
```
For a continuous Sigma-Algebra carrying monad m, evaluation is given by:
```Agda
ev : Term -> m Value
ev (return V) = \nu V
ev ((\x. M) W) = ev (M[x <- W])
ev (M to x.N) = ev M >>= \v -> ev(N[x <- v])
ev (op(o, args)) = oT(map ev args)
```

So my proposal is to keep count the resource use inside *m* with a *Writer
Transformer monad*: WriterT m r a == m (r , a), and threaded through all
computations, checking some monotonic constraints on the continuous
Sigma-Algebra structure.

Tick Op
-------------

There are actually two abstraction levels to implement a *tick* operation.

**Machine Level**

Here, machine level means a monadic operation such as:
```Agda
mtick : T X -> T X
mtick = -- produces a tick?
```

Since what I want to characterize using the monadic evaluator is termination, it
may be easier just to keep count inside the monad directly.

```Agda
mtick : T (X x N) -> T (X x N)
mtick = fmap (second suc)
```

Just using a monadic evaluator and Relators to abstract away approximation
(observational approximation) leaves the door open to use the Writer Monad
Transformer

```Agda
CostT X = T (X x N)

ret : X -> CostT x
ret x = ret_T (x , 0)

addCost : N -> CostT X -> CostT X
addCost n = fmap (second (n+))

tick = addCost 1

(>>=) : CostT a -> (a -> CostT b) -> CostT b
m >>= n = m >>=_T (\(a,ca) . addCost ca (n a))
```

So, everything stands the same. Relators, etc...
But know we want to keep some sort of coherence about how much the program is
spending resources, and so, monotonicity is required. TODO

**Algebraic**

Given a signature Sigma, define Sigma + {tick} = TSigma, with ar(tick) = 1. So
then, we can define for a cont Sigma algebra, we can extend it to a cont TSigma
algebra, simply by replacing each tick operation with a machine's tick.

More or less.
```Agda
talpha : (Sigma (T X) -> (T X)) -> TSigma (T X) -> (T X)
talpha alpha (Left x) = alpha x
talpha alpha (Right (t(e))) = mtick (alpha e) 
```
