\emph{Common Sub-expression Elimination} (CSE) is a term transformation
that removes multiple occurrences of a some shared sub-expression.
%
CSE is performed by binding a shared sub-expression \(M\) to a new
fresh variable \(x\), evaluating the sub-expression \(M\), storing its
result in the variable \(x\), and finally replacing every occurrence of
\(M\) with \(x\).
%
CSE maybe considered an optimization since avoids multiple evaluations of the
same sub-expression reducing the total cost required to evaluate a term.
%
In this example we explore a proper definition of CSE and we prove that it is an
improvement when the only observable effect is termination.

We assume a fixed signature \(\Sigma\) for the rest of the section.
% Assuming a fixed signature, and an appropriate compatible system, the
% previous transformation is translated directly through the use of term
% contexts as:
\begin{statement}[Common Sub-Expression Elimination]\label{stmt:first:cse}
 Let \((\Gamma, T)\) be a \(\Sigma\)-compatible system with \(T\)
commutative and idempotent, \(\CC\) be a term context, \(M\) be a
closed term, and \(x\) a fresh new variable, such that
\( \CC[ M ] \in \KT\):
  \begin{displaymath}
    \tick \CC[\tick M] \gimprov{\Gamma} \LTo{M}{x.\CC[\LRet{x}]}
  \end{displaymath}
\end{statement}
The ticks added are to pay ahead for the \(\LTo{}{}\) binding term and
each \(\LRet{}\) construction required.

While the definition above translates directly from the intuition of
CSE, it presents two problems.
%
First, multiple observable effects produced by the evaluation of \(M\)
may be removed.
%
Second, the evaluation of \(M\) may not required, and thus, CSE may
increase the cost of the evaluation of the whole term.
%
The first problem is solved imposing restrictions on the compatible
system employed to interpret and compare effects, while the second is
solved restricting the term context employed to perform the
transformation.

\subsubsection*{CSE Effects Manipulation}

While CSE prevents multiple evaluations of a shared sub-expression,
it also prevents multiple occurrences of the observable effects
produced by the evaluation of such sub-expression.
%% Re form the concept of sequential evaluation in the mind of the
%% reader.
The order of effects is syntactically introduced through the use of
the explicit binding operator \textbf{to}, and interpreted
sequentially in the same order following the monadic evaluation
presented in Definition~\ref{def:evaluation}.
%
Let \(M,N\) be two terms, and \(x\) a variable, such
that \(\LTo{M}{x. N}\) is a closed term. Following the monadic
evaluation we have:
\begin{displaymath}
  [[ \LTo{M}{x . N} ]] \equiv [[ M ]] >>= \func{m}{[[ N [x := m ] ]]}
\end{displaymath}
%
Therefore, we expect to interpret first the effects produced by the
evaluation of the term \(M\), and then the effects produced by the
evaluation of \([[ N [x := m ] ]]\) where \(m\) is the resulting value
of the evaluation of the term \(M\).

CSE changes the structure of terms, and in consequence, it also
changes two aspects in the interpretation of effects: the number of
times an effect is generated and the order in which the effects are
produced.
%
In order to CSE be a valid transformation the monad employed
has to be able to change the order of effects and remove multiple
apparitions of the same effect.
%
We present two examples showing how CSE changes impact on the effects
observed after evaluation.
% We explore what are the assumptions required to make CSE a legal
% transformation of programs with effects through two minimal examples
% exposing how changes impact on the observations made.
%

The following two examples show messages through standard output, and
we assume that the relator employed observes the output performed by
the evaluation of terms.
%
Suppose there are two nullary operations \(print_{msg}\) and
\(print_{msg'}\) that print the message \(msg\) and \(msg'\)
respectively.
%
The expected behaviour of several calls to \(print_{msg}\) is to print
several messages \(msg\), as well as several calls to \(print_{msg'}\)
print several messages \(msg'\).

\begin{example}
  Let \(M\) be a closed term, define the following term:
  \begin{displaymath}
    TwoMsg \triangleq
    (\LTo{print_{msg}}{x.(\LTo{print_{msg}}{y.M})})
  \end{displaymath}
%
  The expected behaviour when the term \(TwoMsg\) is evaluated is to
  print two messages \(msg\) plus the other observable effects of
  \(M\).
%
  If we were to apply CSE to the term \(TwoMsg\), we would get a new
  term equivalent to:
  \begin{displaymath}
    Msg \triangleq \LTo{print_{msg}}{x. M} 
  \end{displaymath}
%
  But this new term \(Msg\) prints the message \(msg\) only once,
  while \(TwoMsg\) prints the message twice, before exposing the
  shared behaviour produced by \(M\).
%
  Therefore, \(Msg\) has different observable behaviour than
  \(TwoMsg\).
\end{example}

The previous example indicates that in order to apply CSE, two
consecutive evaluations of the same term should generate the same
observable effects.

Additionally, CSE changes where expressions are evaluated.
%
CSE collects a shared sub-expression \(M\), replaces every apparition of
\(M\) with a new fresh variable \(x\), and places a new binding term
before the first appearance of \(M\).
%
Therefore, \(M\) is evaluated \emph{before} every other place where
\(M\) was in the original term, and thus, CSE requires to be able to
change the order of the evaluation of terms.
%
% In other words, the order of effects produced by the evaluation of
% terms should be indistinguishable by the relator employed.

% Assume to have two operations \(print_{msg1}, print_{msg2}\) that show
% the message \(msg1\) and \(msg2\) through standard output
% respectively.

\begin{example}
  Let \(M\) be a closed term, define the following term:
  \begin{displaymath}
    Msgs \triangleq \LTo{print_{msg}}
    {x.(\LTo{print_{msg'}}{y.(\LTo{print_{msg}}{z.M})})} 
  \end{displaymath}
  The expected behaviour of the evaluation of \(Msgs\) is to print
  three messages through standard output: \(msg, msg', msg\), and
  then followed by the expected behaviour of \(M\).
%
  If we were to apply CSE to the term \(Msgs\), identifying
  \(print_{msg}\) as the shared sub-expression, we would get a new
  term equivalent to:
  \begin{displaymath}
    Msgs' \triangleq
    \LTo{print_{msg}}
    {w.(\LTo{print_{msg'}}{y.(\LTo{(\LRet{w})}{z.M})})}
  \end{displaymath}
%
  But this new term \(Msgs'\) prints the following \emph{two}
messages: \(msg, msg'\), and continuous with the expected behaviour of
\(M\).
%
  There is not another message \(msg\) after \(msg'\) as it was in
  the original term evaluation, and thus, \(Msgs\) and \(Msgs'\) have
  different observable behaviour.
\end{example}
%
% Note that the observable effects are determined by the relator
% employed.
%
% For example, if in the example before the standard output were not
% observed by the relator employed then \(Msg\) and \(TwoMsg\) would
% have been comparable.

The evaluation of terms result in a sequence of monadic operations
dictated syntactically by the binding constructor \(\LTo{}{}\).
%
CSE modifies the structure of a term, and thus, changes the sequence
of monadic operations removing shared computations.
%
Atomically, we can see the transformation made by CSE as two steps on
the sequence of monadic operations: removing two consecutive duplicate
computations and swapping two consecutive independent monadic terms.
%
% \begin{enumerate}
%   \item[R-1]\label{op:cse:1}
%     Two consecutive evaluations of the same expression should be
% observably equivalent to one execution.
% %
% Otherwise, we would encounter the same problem described in the
% first example mentioned above.
%   \item[R-2]\label{op:cse:2}
%     The result of moving up the evaluation of closed terms should be
% observably equivalent to not doing so.
%     \martin{Too informal?}
% %
% Otherwise, we would encounter the same problem described in the second
% example mentioned above.
% \end{enumerate}
%
The two atomic steps described before are translated to two concise
monadic properties:
\begin{itemize}
\item removing two consecutive evaluations of the same expression can
  be interpreted as the monad property of \emph{idempotent};
\item while swapping two evaluations of closed terms can be
  interpreted as the monad property of \emph{commutative}.
\end{itemize}

\begin{definition}[Idempotent Monad]
 Let \(M\) be a monad. We say that the monad \(M\) is idempotent iff for
 any two sets \(X,Y\), monadic value \(ma : M(X)\) and function \(f :
 X -> X -> M(Y)\), the following equation holds:
 \begin{displaymath}
   ma >>= \lambda a. ma >>= \lambda b. f(a)(b)
   \equiv
   ma >>= \lambda a. f(a)(a)
 \end{displaymath}
\end{definition}
%
If an idempotent monad is employed to evaluate terms, two consecutive
evaluations of the same term generates exactly the same observable
effect as one evaluation, and thus, one evaluation can be removed.
%
When costs are added to the evaluation is easy to see that
idempotency is in fact an improvement, since removing spurious
computations reduces the total cost required to evaluate a term.
%
However, the cost monad of an idempontent monad is not idempotent.
%
In fact the cost monad should not be idempontent, otherwise multiple
ticks would be removed making the whole calculus useless.

\begin{definition}[Commutative Monad]
  Let \(M\) be a monad.
  %
  We say that the monad \(M\) is commutative if and only if for any
three sets \(X,Y,Z\), monadic values \(ma : M(X), mb : M(Y)\) and
function \(f : X -> Y -> M (Z)\), the following
equation holds:
 \begin{displaymath}
   ma >>= \lambda a. mb >>= \lambda b. f(a)(b) 
   \equiv
   mb >>= \lambda b. ma >>= \lambda a. f(a)(b) 
 \end{displaymath}
\end{definition}
%
Commutative monads enable us to commute the evaluation of closed terms.
%
When costs are added to the evaluation of terms commutativity is an
improvement but the cost required to evaluate the term remains the
same.

At this point the reader should be able to prove a specific CSE
improvement such as the Example~\ref{ex:CSE} for an idempotent and
commutative monad.

\subsubsection{Restrict CSE Contexts}

Statement~\ref{stmt:first:cse} states that for any given term context
CSE is an improvement.
%
However, there are valid term contexts that do not require its hole to
be evaluated rendering the transformation unnecessary.

\begin{example}\label{ex:constantCtx}
  Let \(N,M\) be closed terms, and \(\CC = N\).
  %
  Following the CSE definition proposed so far we would have that:
  \begin{displaymath}
    \tick \CC [ \tick M ] \gimprov{} \LTo{M}{x. \CC [\LRet{x}]}
    \equiv \tick N \gimprov{} \LTo{M}{x. N}
  \end{displaymath}
\end{example}
%
In order to CSE be valid improvement the context employed is required
to use its hole during evaluation.
%
We introduce the concept of \emph{reduction context} as those contexts
that require their hole to be evaluated and restate CSE.

\begin{definition}[Reduction Context]
  Let \(\CC\) be a term context.
  %
  We say \(\CC\) is a reduction context if and only if for all term
  \(M\), \(\CC [ \tick M ] \gimprov{\Gamma} \tick \CC [ M ]\)
  \end{definition}
%
Reduction contexts are contexts that \emph{use} or \emph{require}
their hole to be filled to complete their evaluation.
%
% Reduction Contexts are also called \emph{evaluation contexts}\todo{cite
% where}.

% A trivial example would be:
\begin{example}[Trivial Reduction Context]
  The trivial context \([-]\) is a reduction context.

  Let \(M\) be a term.
  %
  By the hole-filling definition we have that:
  \begin{displaymath}
    [-] [\tick M] \equiv \tick M \equiv \tick ([-] M)
  \end{displaymath}
  and since improvement is reflexive,
  \begin{displaymath}
   [-] [\tick M]  \gimprov{\Gamma} \tick ([-] [M])
  \end{displaymath}
  \end{example}
%

While is easy to see that the context in Example~\ref{ex:constantCtx} 
is not a reduction since \(N\) is not improved by \(\tick[] N\).
  
% We are ready to define a general presentation of CSE.
\begin{definition}[Common Sub-Expression Elimination]
  Let \((\Gamma, T)\) be a compatible system with \(T\) commutative and
idempotent, \(\CC\) be a reduction context, \(M\) be a closed term,
and \(x\) a fresh new variable, such that \( \CC[ M ] \in \KT\):
\begin{displaymath}
  \tick \CC[\tick M] \gimprov{\Gamma} \LTo{M}{x.\CC[\LRet{x}]}
\end{displaymath}
\end{definition}

Intuitively, we ask \(\CC\) to be a reduction context to be sure that
its hole is required in the evaluation of \( \CC [M]\), and thus, that
\(M\) is going to be evaluated in the evaluation of \(\CC [M]\).
%
Since \(T\), the monad employed to evaluate terms, is commutative and
idempotent, the shared sub-expression, \(M\), can be factored out
and evaluated before the evaluation of \(\CC[-]\).
%
A new variable \(x\) is introduced to share the resulting value from
the evaluation of \(M\), and placed instead of \(M\).
%
Ticks are introduced to pay ahead for the new \(\LTo{M}{x.}\)
construction, and for the cost of looking up \(x\).

The amount of pay ahead ticks depend on the definition of the cost
evaluation.
%
In the definition of the cost
evaluation~(Definition~\ref{lmm:cost:evaluation}) the evaluation of a
\(\LRet{}\) and the evaluation of \(\LTo{}{}\) expressions add one
tick to the evaluation of a term, and thus, we added one tick for each
operation.

The proof that CSE is an improvement follows the method described in
the previous sections of this article: proving CSE locally presenting
an improvement simulation, and by Theorem~\ref{thm:sim:ctx}, CSE is an
improvement.
%

%% Reduction ctxs are not modular
While reduction contexts describe very well the contexts where CSE can
be applied, the reduction property is not inherited from contexts to
its sub-contexts, and thus, are complicated to work with.
%
We propose the following simpler lemma.
%
\begin{lemma}[Direct CSE]\label{lmm:direct:cse}
  Let \((\Gamma, T)\) be a \(\Sigma\)-compatible system with \(T\) commutative
and idempotent, \(M\) be a closed term, \(\CC\) a closed context term,
and \(x\) a fresh new variable, such that \( \CC[ M ] \in \KT\):
a fresh new variable:
  \begin{displaymath}
    \mkRel{\LTo{M}{x.\CC[\tick M]}}{\gimprov{\Gamma}}{\LTo{M}{x.\CC[\LRet{x}]}}
  \end{displaymath}
\end{lemma}

Assuming that the evaluation of \(M\) is perform before the evaluation
of the term \(\CC[M]\) enable us to avoid the notion of reduction
context.

\wrule

However, the  there raises another problem related to our proof method.
Applicative simulations are useful to prove local transformations but
they do not carry global or contextual information.
% \martin{Then why don't we present an improvement (which are contextual
% by definition)? Because I am not intelligent enough and I think we
% would be opening another door into another dark room.}

Let \((\Gamma, T)\) be a \(\Sigma\)-compatible system with \(T\) commutative
and idempotent, \(M\) be a closed term, \(\CC\) a closed context term,
and \(x\) a fresh new variable, such that \( \CC[ M ] \in \KT\),
we can show the problem with the following term:
\begin{displaymath}
 \LTo{M}{x. \LRet{\lambda y. \CC [ \tick[] M ]}} 
 \gimprov{\Gamma}
 \LTo{M}{x. \LRet{\lambda y. \CC [ \LRet{x} ]}} 
\end{displaymath}

The problem resides in that we cannot fabricate an improvement
simulation for those terms as is stated, we need more information.
%
When we evaluate the terms stated above, we lose the connection
between the term \(M\) and the resulting value of the evaluation of
\(M\).
 
On the one hand we have:
\begin{displaymath}
  \cost{[[ \LTo{M}{x. \LRet{\lambda y. \CC [ \tick[] M ]}} ]]} 
  = \tick[] \cost{[[ M ]] } >>= \lambda m . \cost{ [[ \LRet{\lambda y. \CC [ \tick[] M ]} ]]}
\end{displaymath}
%
While on the other hand we have:
\begin{displaymath}
  \cost{[[ \LTo{M}{x. \LRet{\lambda y. \CC [ \LRet{x} ]}} ]]}
  = \tick[] \cost{[[ M ]] } >>= \lambda m . \cost{[[ \LRet{\lambda y. \CC [ \LRet{m} ]} ]]}
\end{displaymath}

Following the definition of T-relator, we are required to prove
that for any \(m_1, m_2 \in \KV\) such that \(m_1 \mathrel{\RR_\V}
m_2\), the following expression hold
\begin{align*}
  \cost{ [[ \LRet{\lambda y. \CC [ \tick[] M ]} ]]}
  & \mathrel{\cost{\Gamma} \RR_\V}
  \cost{[[ \LRet{\lambda y. \CC [ \LRet{m_2} ]} ]]} \\
  \tick[] \eta(\lambda y. \CC [ \tick[] M ])
  & \mathrel{\cost{\Gamma} \RR_\V}
  \tick[] \eta(\lambda y. \CC [ \LRet{m_2} ]) \\
\end{align*}
Remember that \(x\) is a new and fresh variable.
%
But we do not have any connection between the closed term \(M\) and
the closed value \(m\).

Moreover, since I cannot prove the general version we decided to
specialized the proof a bit more.

Instead of proving CSE for an idempotent and commutative monad, I
produced a proof for the Partial Computation monad.
%
Why? Because given a closed term M, if its evaluation finishes, it
produces a value, and that value can be shared throughout every
context.
%
Can we do the same for the exception monad? My guess is that we can,
since is essentially the same thing.

\wrule

% The proof is tedious and uninformative, it consists in proving that
% the following \(\lambda\)-term relation is actually an improvement
% simulation, by co-induction and case analysis in \(\CC\).
% \begin{align*}
%   DCSE_\T &\triangleq
%          \{
%          ( \LTo{M}{x. \CC [ \tick M ]}
%          , \LTo{M}{x. \CC [ \LRet{x} ]} )
%          \mid M \in \KT, \CC [ M ] \in \KT \}\\
%   DCSE_\V &\triangleq Id_\V
% \end{align*}

Finally, a sketch of a proof of CSE in presence of effects is
provided.
%
Let \(M\) be a closed term, the proof consists in proving that the
following \(\lambda\)-term relation representing CSE forms an
improvement simulation:
\martin{format}
\begin{align*}
  CSE_\T &\triangleq
         \{
         (\tick[] \CC[ \tick[] M], \LTo{M}{x.\CC[\LRet{x}]}) \\
         &  \mid \CC \text{ is a reduction context},
           \CC[M] \in \KT, x \text{ is fresh} \}\\
         & \cup DCSE_\T \\
  CSE_\V &\triangleq Id_\V
\end{align*}
In this case, we added the applicative simulation employed to prove
the auxiliary direct CSE lemma ~(Lemma~\ref{lmm:direct:cse}).
%
Again the proof is tedious and uninformative, however we show the most
prominent case of the proof: when \(\CC = \LTo{\DD}{y. \EE}\).
% , and
% \(\CC\) is a reduction context.
%
Inherited from \(\CC\), \(\DD\) or \(\EE\) are reduction contexts,
which give us two more cases.
%
%
We show the case when \(\DD\) is a reduction context, and thus, we
need to show that:
\begin{displaymath}
  \cost{[[ \tick[] (\LTo{\DD}{y. \EE})[\tick M] ]]}
  \mathrel{\cost{\Gamma} Id_V}
  \cost{[[ \LTo{M}{x. (\LTo{\DD}{y. \EE})[\LRet{x}]}]]}
\end{displaymath}
%
Evaluating the first term we get
\begin{displaymath}
  \begin{array}{l}
    \cost{[[ \tick[] (\LTo{\DD}{y. \EE})[\tick M] ]]} \\
%%%%%%%%%%%%%%%%%%%%
    \equiv \langle{\text{\(M\) is a closed term}}\rangle \\
%%%%%%%%%%%%%%%%%%%%
    \cost{[[ \tick[] (\LTo{\DD[\tick M]}{y. \EE[\tick M]}) ]]} \\
%%%%%%%%%%%%%%%%%%%%
    \equiv \langle{\text{ \(\tick[]\) interpretation}}\rangle \\
%%%%%%%%%%%%%%%%%%%%
    \tick[] \cost{[[ \LTo{\DD[\tick M]}{y. \EE[\tick M]} ]]} \\
%%%%%%%%%%%%%%%%%%%%
    \equiv \langle{\text{ \(\LTo{}{}\) evaluation }}\rangle \\
%%%%%%%%%%%%%%%%%%%%
    \tick[2] \cost{[[ \DD[\tick M] ]]}
    >>= \lambda d. \cost{ [[ \EE[\tick M][y := d] ]] } \\
%%%%%%%%%%%%%%%%%%%%
    \equiv \langle{\text{\(\tick[]\) interpretation}}\rangle \\
%%%%%%%%%%%%%%%%%%%%
    \tick[] \cost{[[\tick[] \DD[\tick M] ]]}
    >>= \lambda d. \cost{ [[ \EE[\tick M][y := d] ]] }\\
%%%%%%%%%%%%%%%%%%%%
    \equiv \langle{\text{\(M\) is a closed term}}\rangle \\
%%%%%%%%%%%%%%%%%%%%
    \tick[] \cost{[[\tick[] \DD[\tick M] ]]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
  \end{array}
\end{displaymath}
Since \(\DD\) is a reduction context, we know that the pair
\((\tick[] \DD[\tick M], \LTo{M}{x. \DD [\LRet{x}] } )\)
is in \(CSE_T\), and \(\cost{\Gamma}\) is a T-relator, we have:
\begin{displaymath}
  \begin{array}{l}
    \tick[] \cost{[[\tick[] \DD[\tick M] ]]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
 \mathrel{\cost{\Gamma} Id_v} \\
    \tick[] \cost{[[ \LTo{M}{x. \DD[\LRet{x}]} ]]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] }
  \end{array}
\end{displaymath}
Here is where the auxiliary direct CSE lemma comes into play, since we
do not know if \(\EE\) is a reduction context.
%
In order to apply direct CSE on context \(\EE\), we need to fabricate
a binding term \(\LTo{M}{}\) around context \(\EE\).
%
However, we can not use the same \(\LTo{M}{}\) term generated by the
co-inductive hypothesis since its value is used to fill the hole in
context \(\DD\).
%
Therefore, we use a trick derived from the idempotency property of the
underlying monad.
%
We add an independent evaluation of the closed term \(M\) to both
evaluations and prove that the following evaluations are in the
improvement simulation:
% add a new independent evaluation of \(M\) to both terms,
% prove that the new terms are part of the improvement simulation, and
% safely remove the added evaluations of \(M\) it later through
% idempotency.
\begin{displaymath}
  \begin{array}{l}
    \tick[] \cost{ [[ M ]]}
    >>= \lambda m'. \cost{[[ \LTo{M}{x. \DD[\LRet{x}]} ]]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
 \mathrel{\cost{\Gamma} Id_v} \\
    \tick[] \cost{ [[ M ]]}
    >>= \lambda m'. \cost{[[ \LTo{M}{x. \DD[\LRet{x}]} ]]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\LRet{m'}] ]] } \\
  \end{array}
\end{displaymath}

First we move the added evaluation of the term \(M\) closer to the
context \(\EE\), and build a \(\LTo{M}{}\) expression around \(\EE\):
\begin{displaymath}
  \begin{array}{l}
    \tick[2] \cost{ [[ M ]]} >>= \lambda m'.
    \cost{[[ M ]]} >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] }\\
%%%%%%%%%%
    \equiv \langle{\text{Commutativity}}\rangle \\
%%%%%%%%%%
    \tick[2] 
    \cost{[[ M ]]} >>= \lambda m.
    \cost{ [[ M ]]} >>= \lambda m'. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
%%%%%%%%%%
    \equiv \langle{\text{Commutativity}}\rangle \\
%%%%%%%%%%
    \tick[2] 
    \cost{[[ M ]]} >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ M ]]}
    >>= \lambda m'. \cost{ [[ \EE[y := d][\tick M] ]] } \\
%%%%%%%%%%
    \equiv \langle{\text{Tick Commutativity}}\rangle\\
%%%%%%%%%%
    \tick[] 
    \cost{[[ M ]]} >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \tick[] \cost{ [[ M ]]}
    >>=  \lambda m'. \cost{ [[ \EE[y := d][\tick M] ]] }\\
%%%%%%%%%%
    \equiv \langle{\text{\(\LTo{}{}\) evaluation}}\rangle\\
%%%%%%%%%%
    \tick[] 
    \cost{[[ M ]]} >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \LTo{M}{x. \EE[y := d][\tick M]} ]] }
  \end{array}
\end{displaymath}
%
Now we are able to apply direct CSE at the context \(\EE[y := d]\),
note that \(x\) is a fresh variable in \(\EE, d, M\), and that \(d\)
is a closed value, and thus \(\EE[y:=d][\tick M]\) is a closed term.
%
As result we have that:
\begin{displaymath}
  \begin{array}{l}
    \tick[2] \cost{ [[ M ]]} >>= \lambda m'.
    \cost{[[ M ]]} >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
 \mathrel{\cost{\Gamma} Id_v} \\
    \tick[] 
    \cost{[[ M ]]} >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \LTo{M}{x. \EE[y := d][\LRet{x}]} ]] }
  \end{array}
\end{displaymath}
Finally, undoing all the steps made before we get that:
\begin{displaymath}
  \begin{array}{l}
    \tick[2] \cost{ [[ M ]]}
    >>= \lambda m'. \cost{[[ M ]]}
    >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
 \mathrel{\cost{\Gamma} Id_v} \\
    \tick[2] \cost{ [[ M ]]}
    >>= \lambda m'. \cost{[[ M ]]}
    >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\LRet{m'}] ]] }
  \end{array}
\end{displaymath}
By idempotency of the underling monad, we can remove one evaluation of
the term \(M\) and the cost associated with its evaluation to both
terms.
\begin{displaymath}
  \begin{array}{l}
    \tick[2] \cost{[[ M ]]}
    >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
 \mathrel{\cost{\Gamma} Id_v} \\
    \tick[2] \cost{[[ M ]]}
    >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\LRet{m}] ]] }
  \end{array}
\end{displaymath}

Putting all together we have that:
\begin{displaymath}
  \begin{array}{l}
    \tick[] \cost{[[\tick[] \DD[\tick M] ]]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\tick M] ]] } \\
 \mathrel{\cost{\Gamma} Id_v} \\
    \tick[2] \cost{[[ M ]]}
    >>= \lambda m. {\DD[\LRet{m}]}
    >>= \lambda d. \cost{ [[ \EE[y := d][\LRet{m}] ]] }
  \end{array}
\end{displaymath}
Which can be rewritten as:
\begin{displaymath}
  \cost{[[ \tick[] (\LTo{\DD}{y. \EE})[\tick M] ]]}
  \mathrel{\cost{\Gamma} Id_V}
  \cost{[[ \LTo{M}{x. (\LTo{\DD}{y. \EE})[\LRet{x}]}]]}
\end{displaymath}
%
Proving the case when \(\CC = \LTo{\DD}{y. \EE}\).
%
The other cases are similar.

As result we have that CSE is an improvement whenever the monad
employed to interpret the algebraic effects is idempotent and commutative.
%
Sadly idempotent and commutative monads are very particular and
describe monads which do not distinguish effects aside from
termination\todo{Cite something?}.

% We need another lemma derived from the idempotency of the underling
% monad to finally prove CSE.
% \begin{align*}
%   & (ma >>= \lambda m . ma >>= \lambda m'. f(m)(m'))
%   \mathrel{\cost{\Gamma} S}
%   (ma >>= \lambda m . ma >>= \lambda m'. g(m)(m')) \\
%   & \implies
%   (ma >>= \lambda m . f(m)(m))
%   \mathrel{\cost{\Gamma} S}
%   (ma >>= \lambda m . g(m)(m))
%   \end{align*}

T-relators dictate how effects are observed and monadic values are
compared, and we use them to define the notion of improvement as well
as improvement similarity.
%
In the model presented in Section~\ref{sec:Calculus} we required a
system composed by a monad to evaluate terms and a t-relator to
compare the evaluation of terms.
%
In this article we impose properties over the underlying monad
restricting every effect aside from termination.
%
Hence, another approach is to restrict the relator's observations to
define \emph{observable idempotent} and \emph{observable commutative}
terms not directly as equivalent monadic values but as observable
approximations through the relator employed.
%
% Improvements are observable approximations where costs are taken into
% account but they are compared through the observations made by the
% relator employed.
% %

The approach taken in this example implies the restrictions
needed in the relator thanks to the property (Ref-1) of
relators (Definition~\ref{def:fun:relator}).
%
While restricting the observations made by the relator is a more general
approach accepting in principle more monads and algebraic effects,
it also requires the proof to be carried out entirely at the
observation level.
%
In other words, improvement simulations cannot be used but to prove
some simpler or local lemmas.
%
We leave this approach for future work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Old presentation
% In any case \(Gamma\) decides how and what effects are observed, and
% therefore, modify the classic notions of commutativity and
% idempotency. It is also clear that if \(M\) is commutative and
% idempotent, is also \(\Gamma\) commutative and idempotent. For
% example, if standard output is ignored by \(\Gamma\), it does not
% matter how many times \(print_{msg}\) is called, or even called at
% all.


% \(\Sigma\)-system and
% \(\Gamma\) compatible with \(\Sigma\).

% \begin{definition}[Idempotent Observation]
%   We say that the observational approximation relation is idempotent if and only
% if for any two terms \(M,N\) and variable \(x\) such that \(x \notin FV(M)\):
% \begin{displaymath}
%     \LTo{M}{x.\LTo{M}{x'.N}} \rctxImp[\Gamma] \LTo{M}{x.N[x' := x]}
% \end{displaymath}
% \end{definition}

% \begin{definition}[Commutative Observation]
%   We say that the observational approximation relation is commutative if and
% only if for any given terms \(M,N,O\) such that \(x \notin FV(N)\) and \(y
% \notin FV(M)\):
%   \begin{displaymath}
%     \LTo{M}{x.\LTo{N}{y.O}} \rctxImp[\Gamma] \LTo{N}{y.\LTo{M}{x.O}}
%   \end{displaymath}
% \end{definition}

% We assume that for the rest of this example the observational approximation
% relation resulting from the \((T, \Sigma)\)-system is commutative and
% idempotent.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Here is actually true, since evaluation is performed. Can we use this to
%% simplify other proofs?
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% It does not hold that every context is either a reduction context or a value.
% But it could be proved that having defined an open context evaluation, every
% context evaluates to either a reduction or a value context.

% A trivial example would be:
% \begin{example}
%   \(\CC = \IfThenElse{true}{ [ - ]}{(\func{x}{x})}\)
% \end{example}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%
% The observations are defined by the relator employed, and for the sake
% of the argument, assume that such relator observes the output
% performed by the evaluation.
%

% As opposed to \emph{Constant/Value Contexts}.
%
% \begin{definition}[Value Context]
%   Given a context \(\CC\) we say \(\CC\) is a \emph{value context} if \(\forall
%   M \in T, \CC[M] \in \KT, \CC[M] \cimproved \CC[\tick M]\)
%   \end{definition} 
%
% Basically a value context is a context such performs no computation at all(alas
% its name). And such contexts come in the following way, since hole-filling is
% related to terms.
%
% And here is an example of a context that performs no evaluation, which obviously
% does not require its holes filled no reach a value.

% Opposed to reduction context we have contexts which do not require they
% hole to be filled in order to reach a value.

% \begin{example}[Value Context]
%   Let \(\CC\) be a term context, the term context defined as
%   \(\LRet{(\lambda x. \CC)}\) is not a reduction Context.
%   \end{example}
