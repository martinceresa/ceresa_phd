\emph{Common Sub-expression Elimination} (CSE) is a term transformation that
removes multiple occurrences of a shared sub-expression.
%
It consists on binding a shared sub-expression \(M\) to a new fresh
variable \(x\), evaluating the sub-expression \(M\) \textbf{once}, storing its
result in the variable \(x\), and finally replacing every occurrence of \(M\)
with \(x\).
%
It may be considered an optimization since CSE \emph{avoids multiple
evaluations} of the same sub-expression, and thus, the total cost required by
the evaluation is reduced.
%
First, we explore a definition of CSE as an improvement working from intuition
without taking care of effects; and second, we prove CSE is an optimization when
the only observable effect is non-termination.

Intuitively, we can enunciate CSE as follows:
%
given a signature \(\Sigma\), and a \(\Sigma\)-compatible system \((\Gamma, T)\),
for any context \(\CC\), closed term \(M\),
and fresh new variable \(x\), \(\tick \CC[\tick M]\) is improved by
\(\LTo{M}{x.\CC[\LRet{x}]}\)~\citep{Hackett.2019.Clairvoyant}.
%
We add ticks to pay ahead for transforming the original expression: one tick
to pay for binding the shared term \(M\) to
variable \(x\), \((\LTo{M}{x . \, \ldots})\), and one tick to replace each \(M\)
with \((\LRet{x})\) where we can think of this extra tick as paying ahead to retrieve the value
stored in variable \(x\).
%
The amount of pay ahead ticks depends on the definition of the cost evaluation
function. In particular, in Definition~\ref{lmm:cost:evaluation}, the
evaluation of \((\LRet{})\) and \((\LTo{}{})\) expressions add one tick to the
evaluation of a term, and therefore, we added one tick for each new operation added
to the original expression \(M\).

While the above translation of CSE into improvement codifies the intuition
behind CSE, it is too general to be an improvement as it includes contexts where CSE increases the
total cost of evaluation.
%
For example, if the context does not require the evaluation of its
hole, CSE would prematurely evaluate the shared sub-expression
unnecessarily.
%

\begin{example}\label{ex:constantCtx}
  Let \(\Sigma\) be a signature, \((\Gamma,T)\) a compatible
  \(\Sigma\)-system, \(N,M\) be closed terms, and \(\CC \triangleq N\).
  %
  The context \(\CC\) is a context without holes, but following the intuitive
CSE definition proposed so far we would have that:
  %
  \begin{displaymath}
    \tick N [ \tick M ] \gimprov{\Gamma} \LTo{M}{x. N [\LRet{x}]}
  \end{displaymath}
  %
  However, applying hole-filling substitution, we would have that:
  %
  \begin{displaymath}
    \tick N \gimprov{\Gamma} \LTo{M}{x. N}
  \end{displaymath}
  %
  This is a valid improvement only if the evaluation of the term
  \(N\) does not terminate, but it is not a valid improvement otherwise.
\end{example}

From the previous example, we conclude that for CSE to be a valid improvement
contexts need to \textit{use} their hole during evaluation, otherwise, we may be
forcing the evaluation of dead code.
%
We follow the literature and introduce the concept of \emph{reduction contexts},
or \emph{evaluation contexts}~\citep{Felleisen.EvaluationContexts}, as those
contexts that require their hole during evaluation and restate CSE.

\begin{definition}[Reduction Context]
  Let \(\Sigma\) be a signature.
  %
  We define reduction contexts with the following grammar:
  %
  \begin{align*}
    \mathbb{R} &::= [-]
                 \mid (\LTo{\mathbb{R}}{x \mathrel{.} \mathbb{C}})
                 \mid \LApp{(\LAbs{x}{\mathbb{R}})}{\mathbb{D}}
                 \mid \sigma(\mathbb{C}_{1}, \ldots, \mathbb{C}_{n})
    \end{align*}
    Where \(\sigma \in \Sigma, n = \alpha(\sigma)\), and at least one context \(\mathbb{C}_{i}\) is a
reduction context, and \(\mathbb{D}\) is a context but not necessarily a
reduction one.
  \end{definition}
%
Reduction contexts are contexts that \emph{use} or \emph{require} their hole to
be filled to complete their evaluation, in a sense,
the evaluation of a reduction context will get stuck.
%
It is easy to see that the context in Example~\ref{ex:constantCtx}
is not a reduction context since there is no hole in it.
%
Reduction contexts guarantee that the expression that we are placing in the
context's hole is going to be evaluated, exactly what we need to state CSE.
% and that guarantee is what we
% need to state CSE.

\begin{definition}[Common Sub-Expression Elimination]
  Let \(\Sigma\) be a signature, \((\Gamma, T)\) be a
\(\Sigma\)-system, \(\mathbb{R}\) be a reduction context,
CSE is equivalent to prove that for any closed term \(M \in \KT\) be and \(x\) a
fresh new variable:
\begin{displaymath}
  \tick \mathbb{R}[\tick M] \gimprov{\Gamma} \LTo{M}{x.\mathbb{R}[\LRet{x}]}
\end{displaymath}
\end{definition}

%% commented because it was lengthily explained before.
% Intuitively, we ask \(\mathbb{R}\) to be a reduction context to be sure that the
% expression \(M\) is required by the evaluation of \(\mathbb{R} [M]\), and ticks are
% introduced to pay ahead for adding \((\LTo{}{})\) and \((\LRet{})\).


%% Having defined CSE we now turn our focus to what happens when effects are
%% introduced.

CSE is a simple compiler optimization in languages without effects; however, it
has been first proved for a call-by-need calculus employing improvement
theory~\citep{Moran:Sands:Need}, and it has remained as the main example in the
majority of the improvement theories developed since
then~\citep{Schmidt.2015.ImpFunctionalCore,Hackett.2019.Clairvoyant}.
%
However, when effects are added CSE may not be a correct program transformation,
as it may change the observable behaviour of programs.
%
For example, changing the order of two sequential calls to a printing operation
changes the order in which such messages are printed.

Having defined CSE in our setting, we present a CSE proof when \emph{the only
observable effect is non-termination}, i.e., for the partiality system,
as done in the works mentioned above.%positioning our theory of cost in parallel with the others.
%
We first state a simpler lemma encoding the idea of ensuring the evaluation of a
shared subexpression while removing every other apparition indicated by the
context, similarly to reduction contexts.
%
% and that also enable us to provide a modular proof.

\begin{lemma}[Direct CSE]\label{lmm:direct:cse}
  Let \(M\) be a closed term, \(\CC\) a term context,
and \(x\) a fresh new variable, such that \( \CC[ M ] \in \KT\):
  \begin{displaymath}
    \mkRel{\LTo{M}{x.\CC[\tick M]}}{\gimprov{\Gamma_\bot}}{\LTo{M}{x.\CC[\LRet{x}]}}
  \end{displaymath}
\end{lemma}

The previous lemma
% Lemma direct CSE
states that since we already have a value for expression \(M\)
bound to \(x\), we can use that value instead of evaluating \(M\) again, but
more importantly, it reorders the effects produced by the evaluation of terms.
%
In other words, it encapsulates how to restructure effects when it comes to
factor out a common sub-expression, assuming that we already have evaluated it.
%
Both terms in the above lemma can be represented as the reduction
context \((\LTo{[-]}{x . \CC})\) which makes direct CSE a particular case of CSE.
%
A proof of this lemma can be found in~\ref{app:direc_cse_proof}.

The proof of CSE consists of the observation that the evaluation of reduction
contexts leads to the evaluation of a let-binding expression (where direct CSE
can be applied) or it is a hole (where such an improvement is
trivial).
%
This observation is similar to the open uniform computation
lemma where~\citet{Moran:Sands:Need} state that the evaluation of their
contexts reaches either a free variable, a value, or a hole.

Before beginning, note that the partial system does not have any operation, \(\Sigma
= \emptyset\). Hence, there is no operational reduction context.
%
This property of the system and the proof of direct CSE are the only two
places where we use the hypothesis that there are no effectful operations.
%
%We continue this discussion in the future work section.%which discussion?

\begin{lemma}[Partial System CSE]
  Let \(M\) be a closed term, \(\mathbb{R}\) a reduction context, and \(x\) a
fresh new variable.%
%
\[
  \tick[] \mathbb{R}[\tick[] M] \mathrel{\gimprov{\Gamma_{\bot}}} \LTo{M}{x . \mathbb{R}[\LRet{x}]}
\]
%
\begin{proof}
  The proof consists of building an applicative simulation exposing such
  improvement.
  %
  Let \(M\) be a closed term, we define the following \(\lambda\)-term relation:
  %
  \[
    \begin{array}{ll}
    \mathbb{V}_{\T} & \hspace{-0.75em} \doteq \KT \\
                   & \hspace{-0.75em} \cup \, \{ (\tick[] \mathbb{R}[\tick[] M] , \LTo{M}{x . \mathbb{R}[\LRet{x}]}) \mid \mathbb{R} \text{ is a reduction context}, x \text{ is fresh}\} \\
                   & \hspace{-0.75em} \cup \, \{ (\LTo{M}{x . \mathbb{C}[\tick[] M]}, \LTo{M}{x . \mathbb{C}[\LRet{x}]}) \mid \mathbb{C} \text{ is a context }, x \text{ is fresh}\} \\
    \mathbb{V}_{\V} & \hspace{-0.75em} \doteq Id_{\KV}
      \end{array}
  \]
  %
  To prove that \(\mathbb{V}\) is an applicative simulation, we employ
coinduction and case analysis on the structure of reduction contexts.
  %
  Here, we focus just in the CSE part of the simulation, the rest is
shown in the proof of Lemma~\ref{lmm:direct:cse}~(see~\ref{app:direc_cse_proof}).
%
It is easy to see that \(\mathbb{V}_{\V}\) respects values, so we show that the
evaluation of closed terms leads to related values, let \(\mathbb{R}\) be a
reduction context and \(x\) a fresh new variable:
\[
\cost{[[ \tick[] \mathbb{R}[\tick[] M] ]]} \mathrel{\cost{\Gamma_{\bot}} \mathbb{V}_{\V}} \cost{[[ \LTo{M}{x . \mathbb{R}[\LRet{x}]} ]]}
\]
  %
Case analysis in the structure of \(\mathbb{R}\).
  \begin{itemize}
    \item The trivial reduction context: \(\mathbb{R} = [ -  ]\).
    %
      In that case it is trivially an improvement.
    \item Let-binding reduction context:
\(\mathbb{R} = \LTo{\mathbb{S}}{y . \mathbb{C}}\) for some reduction context
\(\mathbb{S}\) and regular context \(\mathbb{C}\).
          %
          \begin{VProof}
            \vpT{\cost{[[ \tick[] (\LTo{\mathbb{S}}{y . \mathbb{C}})[\tick[] M] ]]}}
            \vpJ{\equiv}{Context hole-filling, \(M\) is closed}
            \vpT{\cost{[[ \tick[] (\LTo{\mathbb{S}[\tick[] M]}{y . \mathbb{C}[\tick[] M]}) ]]}}
            \vpJ{\equiv}{Evaluation}
            \vpT{ \tick[2] (\cost{[[ \mathbb{S}[\tick[] M] ]]} \cost{>>=} v |-> \cost{[[ \substT{\mathbb{C}[\tick[] M]}{y}{v} ]]})}
            \vpJ{\equiv}{Monad associativity}
            \vpT{ \tick[2] \cost{[[ \mathbb{S}[\tick[] M] ]]} \cost{>>=} v |-> \cost{[[ \substT{\mathbb{C}[\tick[] M]}{y}{v} ]]}}
            \vpJ{\equiv}{Evaluation}
            \vpT{ \tick[] \cost{[[ \tick[] \mathbb{S}[\tick[] M] ]]} \cost{>>=} v |-> \cost{[[ \substT{\mathbb{C}[\tick[] M]}{y}{v} ]]}}
            \vpJ{\cost{\Gamma_{\bot}}}{Co-Inductive Hypothesis, \(x\) a fresh new variable}
            \vpT{\tick[] \cost{[[ \LTo{M}{x . \mathbb{S}[\LRet{x}]} ]]} \cost{>>=} v |-> \cost{[[ \substT{\mathbb{C}[\tick[] M]}{y}{v} ]]}}
            \vpJ{\equiv}{Evaluation}
            \vpT{\tick[]( \tick[] \cost{[[ M ]]} \cost{>>=} m |-> \cost{[[ \substT{\mathbb{S}[\LRet{x}]}{x}{m} ]]} ) \cost{>>=} v |-> \cost{[[ \substT{\mathbb{C}[\tick[] M]}{y}{v} ]]}}
            \vpJ{\equiv}{Monad associativity}
            \vpT{ \tick[2] \cost{[[ M ]]} \cost{>>=} m |-> \cost{[[ \substT{\mathbb{S}[\LRet{x}]}{x}{m} ]]}  \cost{>>=} v |-> \cost{[[ \substT{\mathbb{C}[\tick[] M]}{y}{v} ]]}}
            \vpJ{\equiv}{Evaluation, several steps}
            \vpT{\cost{[[ \LTo{M}{x . \LTo{\mathbb{S}[\LRet{x}]}{y . \mathbb{C}[\tick[] M]}} ]]}}
            \vpJ{\cost{\Gamma_{\bot}}}{Direct CSE Hypothesis, \(x\) is fresh}
            \vpEnd{\cost{[[ \LTo{M}{x . \LTo{\mathbb{S}[\LRet{x}]}{y. \mathbb{C}[\LRet{x}]}} ]]}}
            \end{VProof}
    \clearpage
    \item Application Reduction Context: \(\mathbb{R}
    = \LApp{(\LAbs{y}{\mathbb{S}})}{\mathbb{V}}\) for some reduction
    context \(\mathbb{S}\), value context \(\mathbb{V}\) and variable \(y\).
    %
          \begin{VProof}
            \vpT{\cost{[[ \tick[] (\LApp{(\LAbs{y}{\mathbb{S}})}{\mathbb{V}})[\tick[] M] ]]}}
            \vpJ{\equiv}{Hole-filling, \(M\) is closed}
            \vpT{\cost{[[ \tick[] (\LApp{(\LAbs{y}{\mathbb{S}[\tick[] M]})}{(\mathbb{V}[\tick[] M])}) ]]}}
            \vpJ{\equiv}{Evaluation}
            \vpT{\tick[2] \cost{[[ \substT{\mathbb{S}[\tick[] M]}{y}{\mathbb{V}[\tick[] M]} ]]}}
            \vpJ{\equiv}{Hole-filling, \(M\) is closed}
            \vpT{\tick[2] \cost{[[ (\substT{\mathbb{S}}{y}{\mathbb{V}})[\tick[] M] ]]}}
            \vpJ{\equiv}{Evaluation}
            \vpT{\tick[] \cost{[[ \tick[] (\substT{\mathbb{S}}{y}{\mathbb{V}})[\tick[] M] ]]}}
            \vpJ{\cost{\Gamma_{\bot}}}{Co-Inductive Hypothesis, \(x\) a fresh new variable}
            \vpT{\tick[] \cost{[[ \LTo{M}{x . (\substT{\mathbb{S}}{y}{\mathbb{V}})[\LRet{x}] }  ]]}}
            \vpJ{\equiv}{Hole-filling, \(M\) is closed }
            \vpT{\tick[] \cost{[[ \LTo{M}{x . (\substT{(\mathbb{S}[\LRet{x}])}{y}{(\mathbb{V}[\LRet{x}])}) }  ]]}}
            \vpJ{\equiv}{Evaluation}
            \vpT{\tick[] \cost{[[ M ]]} \cost{>>=} m |-> \cost{[[ \substT{(\substT{(\mathbb{S}[\LRet{x}])}{y}{(\mathbb{V}[\LRet{x}])})}{x}{m} ]]}}
            \vpJ{\equiv}{Substitution, \(x \notin FV(\mathbb{S},\mathbb{V}), x \neq y\)}
            \vpT{\tick[] \cost{[[ M ]]} \cost{>>=} m |-> \cost{[[ (\substT{(\mathbb{S}[\LRet{m}])}{y}{(\mathbb{V}[\LRet{m}])}) ]]}}
            \vpJ{\equiv}{Tick is a pure lifted operation}
            \vpT{\cost{[[ M ]]} \cost{>>=} m |-> \tick[] \cost{[[ (\substT{(\mathbb{S}[\LRet{m}])}{y}{(\mathbb{V}[\LRet{m}])}) ]]}}
            \vpJ{\equiv}{Evaluation}
            \vpT{\cost{[[ M ]]} \cost{>>=} m |-> \cost{ [[ \LApp{(\LAbs{y}{\mathbb{S}[\LRet{m}]})}{(\mathbb{V}[\LRet{m}])} ]] }}
            \vpJ{\equiv}{Substitution, \(x\) is a fresh variable}
            \vpT{\cost{[ M ]} \cost{>>=} m |-> \cost{ [[ \substT{((\LApp{\LAbs{y}{\mathbb{S}[\LRet{x}]}}){(\mathbb{V}[\LRet{x}])})}{x}{m} ]] }}
            \vpJ{\equiv}{Evaluation}
            \vpT{\cost{[[ \LTo{M}{x . \LApp{(\LAbs{y}{\mathbb{S}[\LRet{x}]})}{(\mathbb{V}[\LRet{x}])}} ]]}}
            \vpJ{\equiv}{Hole-filling, \(x\) is fresh}
            \vpEnd{\cost{[[ \LTo{M}{x. (\LApp{(\LAbs{y}{\mathbb{S}})}{\mathbb{V}})[\LRet{x}]} ]]}}
            \end{VProof}
    \item Since we are not observing any other effect than non-termination,
\(\Sigma\) is empty, and thus, there is no operation reduction context.
    \end{itemize}
  \end{proof}
  \end{lemma}

\subsection{Towards effectful CSE}

We provided a proof of CSE where non-termination is the only observable effect,
but there is still a question to be answered: is CSE an improvement in
presence of other effects?

There are two entities coming into play when comparing effectful programs:
relators and monads.
%
Therefore, we have two ways to prove that some transformations over programs are
improvements:
\begin{itemize}
 \item lifting properties from the underlying monad
 \item tunning our observations through properties over relators
\end{itemize}
%
A CSE transformation involves replacing multiple evaluations of a
shared subterm by just one evaluation. 
%
% CSE transforms the program replacing multiple apparitions of a term \(M\) by
% just one, before any of the other apparitions, and thus, CSE replaces multiple
% apparitions of the effects that would have been generated by the evaluation
% of \(M\) by just one (possible) at a different place.
%
Hence, during a CSE transformation, the effects produced by the evaluation of a shared
sub-expression may be \emph{removed} or \emph{ignored}.
%
We can remove effects if the underlying monad interpreting the effects has some
properties (e.g. idempotency) that ensure equivalence
between terms.
%
We can ignore effects if the observations made through the relator simply
ignore some effects, e.g., if print operations are ignored.
So, effects may be ignored when relators forget structure.
%
% More work is needed to give precise conditions under which we can either remove
% or ignore effects to get a valid transformation.

We do not know yet sufficient and necessary conditions that ensure that CSE is valid, 
but we can prove that idempotency is necessary.
Let \(\Sigma\) be a signature and a
\((T,\Gamma)\) a compatible system such that CSE is an improvement.
%
Then, we can show that the \((T,\Gamma)\)-system is (observationally)
idempotent: for any \(M \in \KT\),
\[
  \LTo{M}{x . \tick[] M} \improv \LTo{M}{x. \LRet{x}} \improv M
\]
%
Therefore, effects that are not \emph{observationally} idempotent cannot have CSE as an
improvement.
%
Also, some weak form of commutativity seems to be necessary.

The partiality monad is commutative and idempotent, and
thus, we can reorder or even remove multiple
% enable us to move the evaluation of terms and to remove multiple
evaluations of the same term, but sadly, there are not many monads with such
properties.
%
However, we can learn something from the proof of CSE shown in this section, we
only need to know that we are proving CSE for the partiality system at the two
places where effects are reordered
\begin{itemize*}
  \item direct CSE
  \item operation reduction context case
\end{itemize*}
%
The rest of the proof is generic in the effects.
%
% Those two properties are where the effects are reordered: direct CSE factors
% out effects already observed, while the operations mix the effects generated by
% its arguments.
%%%%

% \martin{Mauro check this out}
% Note that we can give a similar proof for the exception system: direct CSE has a
% new additional failing case and \(raise\) operations do not receive any
% argument, and thus, they do not define new reduction contexts.
% \mauro{It seems true but I'm not sure. I don't think it adds much. Maybe remove?}

%% One may wonder if the same proof can be employed to the exception
%% system but sadly the answer is \textbf{no} since the exception monad is not
%% commutative:
%% \(
%%   raise_e >>= \lambda \, \_ -> raise_{e'}
%%   \neq
%%   raise_{e'} >>= \lambda \, \_ -> raise_e \).
%
%% In other words, another venue is to explore which properties over observations
%% are required to make CSE a valid improvement.

%% We can reason the other way around, given a signature \(\Sigma\) and a
%% \((T,\Gamma)\)-system, such that CSE is an improvement, we can show easily that such
%% system is (observationally) idempotent.
%% %
%% In symbols, for any \(M \in \KT\):
%% \[
%%   \LTo{M}{x . M} \improv \LTo{M}{x. \LRet{x}} \improv M
%% \]
%% %
%% We leave this discussion as future work.
%% For most effects of interest, except for non-termination, this law does not hold.

%% \mauro{The general case of CSE has been discussed above. It should be discussed in only one place. Maybe remove it from above and put it all here.}

%% Moreover, we can show that for example the exception system cannot accept
%% CSE, following a similar reasoning.

%% \begin{lemma}
%% Let \(E\) be a set with at least two different elements \(e_1,e_2 \in E\).
%% %
%% Assume the \(E\)-exeption system accepts CSE as an improvement, and thus, the we
%% have that it should accept:
%% \[
%% \LTo{raise_{e_1}}{y . (\LTo{raise_{e_2}}{x . raise_{e_2}})}
%% \improv
%% \LTo{raise_{e_2}}{z . \LTo{raise_{e_1}}{y . (\LTo{\LRet{z}}{x . \LRet{z}})}}
%% \]
%% %
%% But sadly this is not true, since the later term does not approximates the former.
%% %
%% \begin{align*}
%% {[[ \LTo{raise_{e_1}}{y . (\LTo{raise_{e_2}}{x . raise_{e_2}})} ]]}_{E} &\equiv& in_l(e_1) \\
%% &\neq&\\
%% {[[ \LTo{raise_{e_2}}{z . \LTo{raise_{e_1}}{y . (\LTo{\LRet{z}}{x . \LRet{z}})}} ]]}_{E} &\equiv& in_l(e_2) \\
%% \end{align*}
%%   \end{lemma}

%% The previous lemma goes into the account that the exception system is not
%% observationally commutative, and this is one of the reason we have choose to
%% prove CSE for the partiality system.
%% %
%% A possible solution is to use a relator that forgets which exception was raised
%% and just note that an exception was raised falling into a similar setting as the
%% partiality system before.
