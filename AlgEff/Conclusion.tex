\section{Conclusion}

Proving optimizations is hard.
%
Given a program transformation two different aspects of computation have to be
compared: semantics are preserved while operational aspects are improved.
%
Improvement theory tries to solve this problem refining a notion of equivalence
adding cost to the equation.
%
It proposes a relational calculus where program's transformations preserve
their meaning (observed through the equivalence relation) while their
computational cost decrease.
%
Improvement theory was employed to prove optimizations in functional
programming languages\citep{Sands:Op:Theories} with different evaluation
strategies, such as call-by-value~\citep{Sands97improvementtheory}, 
call-by-name and lazy~\citep{Moran:Sands:Need}.
%
It also sheds some light into the study of space
consumption~\citep{Gustavsson:Foundation}, and the study of
non-determinism~\citep{MORAN:NonDet}.
%
In particular, \citeauthor{MORAN:NonDet} studied the interaction between
non-determinism and \emph{cost equivalence} relation~\citep{MORAN:NonDet}, where
cost equivalence requires that the evaluation of both terms finishes with the
same cost. In this work we have a notion of cost improvement.
%
Following this line, it would be worth studying a notion of cost equivalence
based on the more refined observations made by
\citeauthor{Lassen.1997.SimBisimNonDet}.

In previous work, given its operational nature, improvement theory involved
working at a low-level setting, such as abstract machines enabling a very granular
reasoning about resource consumption.
%
However, in this article, we follow a more general and abstract framework,
losing specific properties (such as sharing) but gaining generality.
%
In particular, we can interpret (different) algebraic effects and costs.

In the last decades, a lot of work has been made in cost analysis and complexity
theory.
%
However, in general, the efforts are put into obtaining automatic or static reasoning
about cost by embedding cost into the type 
system~\citep{Hoffmann.Multivariate,Hoffmann.Automatic} or into semi-automatic
complexity of program reasoning~\citep{DalLago.Geometry,DalLago.LinearTypes}.
%
We followed a different path where we build a relational cost theory, not by
observing the absolute cost of a program, but by comparing the cost of two programs
in any possible context.

%%%
Another recent work is RelCost~\citep{Cicek.RelCost}, a relational cost analysis
of programs where the authors define a refinement type and effect system mixed
with cost analysis, producing a type system capable of \emph{estimating} the
cost of program executions to compare them.
%
They introduced two relations: one stating that two programs can have a bounded
difference in their execution cost; and another relation, stating the cost of
the execution of a single program.
%
It differs from our approach in several points:
1) we derive notions of improvements from the definition of equivalence, and
thus, proving improvements is proving (correct) optimizations;
2) our language can easily be extended with new effects and operations;
3) we handle algebraic effects directly;
4) we do not require to know the cost of the execution of programs, but that one
is less than the other.

Recently, the fundamental machinery employed by Sands, observational
approximation\citep{FellMatt} and Howe's method\citep{Howe:Cong:Bisim}, was
updated\citep{Eff:App:Bisim} and studied in the abstract enabling us to also
update the relational improvement framework.
%
In this work, we followed the steps made by \citet{Sands:POPL97}
adding a new observation to the equivalence framework that observes the cost of
evaluating a term, and we showed a simple way to get an improvement theory
from effectful approximation.
%
We employed a transformation known in the Haskell community as the writer's
monad transformer and used the accumulative monoid of natural numbers to
represent program's cost.
%
As result of having cost as an observable effect, we derived a notion of
improvement simulation and improvement from effectful equivalence for terms
systematically and generically.
%
Moreover, we have shown that when the only observable effect is non-termination, the notion of
improvement derived matches the traditional notion of improvement presented by
\citet{Sands:Op:Theories}.

We showed two improvements: one is a generalization of dead-code elimination
called unobservable code elimination, and showed that the probabilistic
model accepts it; and second, we defined common-subexpression elimination as an
improvement and proved that the partiality system accepts it.
% when the only observable effect is non-termination and
% discussed possible ways to include other effects.
%
%% We explored the world of probabilistic programming showing that the
%% removal of needless sampling operations is an improvement.
%% %
%% We defined common-subexpression elimination as an improvement, gave a sketch
%% of a proof, and discussed possible ways to include other effects.


We also explored the limits of this recipe.
%
Sometimes the simplest derived notion of improvement theory may not be
expressive enough to discern some terms as we may have expected.
%
However, we also showed that it is possible to define a more granular cost
bookkeeping system, and thus, we began to explore another notion of cost called
outer cost. 
%
We derived an outer cost monad for the exception
system using monad transformers, and we discussed manners in which this notion could be
generalized to other effects.

This work is a starting point for an effectful relational theory of
costs, where we have shown it to be effective and to be expressive enough to prove generic and specific
improvements.
% We focused mainly on having a working theory and we should be able to prove more
% improvements in the future, and happily, 
Moreover, it has opened the door to explore how
cost and different intensional observations interact with each other.
