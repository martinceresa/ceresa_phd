% There are three different ways to approximate terms with non-deterministic
% effect, and thus, we have three different notions of improvement one for each
% way to approximate terms.

Let \(\Sigma_P = \{\oplus\}\) be a signature with one symbol representing the
binary operation of non-deterministic choice and \(T_P\) be the power set monad
presented in Example~\ref{ex:monads}.

There are two different notions of approximation for non-deterministic
computations, which we codify as two different relators.
%
Moreover, we can compose these two relators to get yet another one.
% , and we
% can do the same with their relators to produce a third one.
%
Each of these relators is \emph{inspired} by the different relations over
powerdomains presented in~\citep{NonDet.FunLang}.

More recent work on the study of similarity in the presence of the effect of
{non-determinism} can be found on the work done by
\citeauthor{Lassen.1997.SimBisimNonDet} where they present two relations,
may-converge and may-diverge~\cite[Section~2]{Lassen.1997.SimBisimNonDet},
establishing two observations about the evaluation of terms.
%
However, {non-determinism} is observed following a similar
approach~\citep[Definition~3.1]{Lassen.1997.SimBisimNonDet}, but they combine
such observations to get more detailed similarity relations.
%
In this work, we only focus on how we observe non-determinism through the lens
of a relator, and thus, we focus on the basic observations made
by~\citeauthor{NonDet.FunLang}, which is comparable to the may-converge relation
defined by~\citeauthor{Lassen.1997.SimBisimNonDet}.

In the rest of this subsection, we instantiate the definitions made in
Section~\ref{sec:CostCalculus}, in particular the relations improvement
simulation~(Definition \ref{def:app:cost}) and improvement~(Definition
\ref{def:Improvement}) to three different T-relators of the power set
monad~(Example~\ref{ex:monads}).

\subsubsection{Hoare's Cost Relation}

The Hoare's relator is the most intuitive way to lift a relation from ground
values to subsets of ground values.
%
Let \(R \subset X \times Y\) be a relation, then \(A \subset X, B \subset Y\) are
\(R\)-related whenever every element in \(A\)
is \(R\)-related to an element in \(B\).
%
When employed to compare non-deterministic terms, Hoare's relator says that a
term \(M\) is related to a term \(N\) if every possible value reached by the
evaluation of \(M\) is related to a value reached by the evaluation of \(N\).

\begin{definition}[Hoare's Relator]\label{def:hoare:rel}
  Let \(X,Y\) bet sets, and \(\RR\) a relation between \(X\) and \(Y\).
  %
  Define the relator \(\Gamma^H \RR \subseteq T_P(X) \times T_P(Y)\) as follows:
  \[\mkRel{S}{\Gamma^H\RR}{T} \iff \forall x \in S, \exists y \in T, \mkRel{x}{\RR}{y} \]
  \end{definition}

We add cost as an observable effect (see Section~\ref{sec:CostCalculus}), and
thus, we have an improvement theory for a language with non-deterministic
effect.

From the definition of improvement simulation, a closed
\(\lambda\)-term relation \((\RR_\T, \RR_\T)\) is a Hoare's improvement
simulation if and only if respects values and for any \(M,N\) closed terms:
% such that \(\mkRel{M}{\RR_\T}{N}\):
\begin{displaymath}
  \mkRel{M}{\RR_\T}{N} \implies \mkRel{[[M]]_{\Nat}}{\cost{\Gamma_{H}}\RR_\V}{[[N]]_{\Nat}}
\end{displaymath}
%
%% The only place where the relator plays a role is in the term part of the
%% relation, and thus, we are going to pay attention to how its definition unfolds.
%
By the Definition~\ref{def:cost:rel}:
\[
  \mkRel{M}{\RR_\T}{N} \implies \mkRel{[[M]]_{\Nat}}{\Gamma^{H}\cost{\RR_\V}}{[[N]]_{\Nat}}
\]
Unfolding the definition of the Hoare's relator and the cost relator
definition we have:
\[
  \mkRel{M}{\RR_\T}{N} \implies \forall (m,m_c) \in [[M]]_{\Nat}, \exists (n,n_c) \in
  [[N]]_{\Nat}, \mkRel{m}{\RR_\V}{n} \land \mkRel{m_c}{\geqInf}{n_c}
\]

We say \(N\) is similar to \(M\) if and only if the evaluation of \(M\)
terminates then also does the evaluation of \(N\), and for every possible value
\(m\) in the evaluation of \(M\) there is a value \(n\) in the evaluation of
\(N\), such that \(n\) is similar to \(m\) and the cost required to compute
\(n\) is less than or equal to the cost required to compute \(m\).

Hoare's improvement is defined as instantiating the definition of
improvement with the Hoare's relator:
\[
  \mkRel{M}{\gimprov{\Gamma^H}}{N} \iff (\forall \CC \in Ctx,
  \CC[ M ], \CC [ N ] \in \KT,
  \mkRel{[[ \CC[M] ]]_{\Nat}}{\cost{\Gamma^H}\UU}{[[ \CC[N] ]]_{\Nat}})
\]

We say \(N\) improves \(M\) if and only if for any closing context
\(\CC\), for every a value \(m\) in the evaluation of \(\CC[M]\), there is a
value \(n\) in the evaluation of \(\CC[N]\), such that the cost required to
evaluate to \(n\) is less than or equal to the cost required to evaluate to
\(m\).
% 
Recall that \(\UU\) relates any two values.

\begin{example}
Let \(M \triangleq \tick[5] I\) and \(N \triangleq \tick[7] I \oplus I\).
%
Since both \(M,N\) are closed, we can employ applicative improvement simulation
and evaluate both of them locally.
%
Formally, to prove that \(N\) improves \(M\), building a cost simulation
relation suffices, and in fact, the trivial one works.
%
On the other hand, intuitively, we can see that \(M\) evaluates to the identity
function within \(5\) units of cost, while \(N\) evaluates to the identity
within \(7\) or \(1\) units of cost.
%
However, since there is a possible outcome that is cheaper than \(5\), Hoare's
relator dictates that it is an improvement.
  \end{example}

To summarize, let \(M,N\) be two terms, \(M\) is improved by \(N\) if the best
possible case of \(N\) is better or equal than the best possible case of \(M\).

\subsubsection{Smyth's Cost Relation}

The Smyth's relator, compared to Hoare's relator, turns the relation the other
way around: let \(\RR \subseteq X \times Y\), then \(A \subseteq X, B \subseteq
Y\) are \(\RR\)-related whenever every element in \(B\) is \(\RR\)-related to an
element in \(A\).

\begin{definition}[Smyth's Relator]\label{def:smyth:rel}
  Let \(X,Y\) bet sets, and \(\RR \subseteq X \times Y\) a relation
  between \(X\) and \(Y\).
  %
  Define the relator
  \(\Gamma^S \RR \subseteq T_P X \times T_P Y\) as follows:
\begin{displaymath}
  \mkRel{S}{\Gamma^S\RR}{T} \iff \forall y \in T, \exists x \in S, \mkRel{x}{R}{y} 
\end{displaymath}
  \end{definition}

A closed \(\lambda\)-term relation \((\RR_\T, \RR_\T)\) is a Smyth's
improvement simulation if and only if respects values and for any \(M,N\) closed
terms:
\begin{displaymath}
  \mkRel{M}{\RR_\T}{N} \implies \mkRel{[[M]]_{\Nat}}{\cost{\Gamma^S}\RR_\V}{[[N]]_{\Nat}} \\
\end{displaymath}

Unfolding the definition of Smyth's relator we have:
\[
  \mkRel{M}{\RR_\T}{N} \implies
  \forall (n,n_c) \in [[N]]_{\Nat},
  \exists (m,m_c) \in [[M]]_{\Nat},
  \mkRel{m}{\RR_\V}{n} \land m_c \geqInf n_c
\]

Smyth's improvement is defined as instantiating the definition of
improvement to the Smyth's relator, for any \(M,N\) possible open terms,
\(\mkRel{M}{\gimprov{\Gamma^S}}{N} \) if and only if:
\[
  \forall \CC \in Ctx, \CC[M],\CC[N] \in \KT,
  \mkRel{[[ \CC[M] ]]_{\Nat}}{\cost{\Gamma^S}\UU}{[[ \CC[N] ]]_{\Nat}}
\]
%% Unfolding the definition of the cost relator and \(\Gamma^S\) we have that
%% forn any \(M,N\) possible open terms,
%% \(\mkRel{M}{\gimprov{\Gamma^S}}{N} \) if and only if:
%% \[
%% %% \begin{align*}
%%   %% & \mkRel{M}{\gimprov{\Gamma^S}}{N} \iff \\
%%   \forall \CC \in Ctx, \CC[M],\CC[N] \in \KT,
%%   \forall (n,n_c) \in [[ \CC[N] ]]_{\Nat},
%%   \exists (m,m_c) \in [[ \CC[M] ]]_{\Nat},
%%   m_c \geqInf n_c
%% %% \end{align*}
%% \]

In other words, for every possible value reachable by the evaluation of
\(\CC[N]\), there is a worse value reachable by the evaluation of \(\CC[M]\).

It is easy to see the duality between Hoare's and Smyth's definitions.
%
While Hoare's is satisfied with just one way to improve a term, Smyth's wants to
be sure that for every possible value in the evaluation of \(\CC[N]\) there is
one at least as bad in the evaluation of \(\CC[M]\).

\begin{example}
  Let \(M \triangleq \tick[5] I\) and \(N \triangleq \tick[7] I \oplus I\).
  %
  The term \(M\) is not (Smyth) improved by \(N\) since \(M\) evaluates to the
  identity function within \(5\) units of cost and there is a possible execution
  of \(N\) that takes more units of cost, \(7\).
%%   there is
%%   no value in the set of possible values generated by the evaluation of \(M\) that is
%%   worse than the value \(\tick[7]\) reachable by the evaluation of \(N\).

  However, we can prove that \(N \rctxImp[\Gamma^S] M\) following the same procedure
as Hoare's example.
  %
  %%   Every value reachable by the evaluation of \(M\) has no greater cost than the
  %% value \(\tick[7]\) reachable by the evaluation of \(N\), and thus, \(N \rctxImp[\Gamma^S] M\).
\end{example}

In summary, a term \(M\) is improved by a term \(N\) if and only if in every
context \(\CC\) the worst possible case in the evaluation of \(\CC [ N ]\)
should be better (or equal) than the worst possible case in the evaluation of
\(\CC [ M ]\).

\subsubsection{Plotkin's Cost Relation}

Plotkin's cost relation is the conjunction of Hoare's and Smyth's cost relations.
%
In particular, we obtain a relation where a term \(M\) is improved by a term \(N\)
whenever for any context \(\CC\):
\begin{itemize}
\item for every possible value in the evaluation of \(\CC [ M ]\)
there is a \emph{better} value in the evaluation of \(\CC [ N ]\),
\item and for every possible value in the evaluation of \(\CC [ N ]\)
there is a \emph{worse} value in the evaluation of \(\CC [ M ]\).
\end{itemize}

\begin{definition}[Plotkin's Non-deterministc Relator]\label{def:plotkin:rel}
  Let \(X,Y\) be sets, and \(\RR \subseteq X \times Y\) a relation between \(X\) and \(Y\).
  %
  We define the Plotkin's non-deterministic relator, \(\Gamma^P \RR \subseteq T_P(X) \times T_P(Y)\), as follows:
  \[\mkRel{S}{\Gamma^P\RR}{T} \iff \mkRel{S}{\Gamma^H\RR}{T} \land \mkRel{S}{\Gamma^S\RR}{T}\]
  \end{definition}

A closed \(\lambda\)-term relation \((\RR_\T, \RR_\V)\) is a Plotkin's
improvement simulation if and only if respects values and:
\begin{align*}
  \mkRel{M}{\RR_{\T}}{N} \implies&
      (\forall v_m \in [[ M ]]_{\Nat}, \exists v_n \in [[ N ]]_{\Nat}
      , \mkRel{v_m}{\cost{\RR_\V}}{v_n}) \\
      \land& (\forall u_n \in [[N]]_{\Nat}, \exists u_m \in [[M]]_{\Nat},
      \mkRel{u_m}{\cost{\RR_\V}}{u_n})
\end{align*}

While Plotkin's Improvement is defined as:
\begin{align*}
  \mkRel{M}{\gimprov{\Gamma^P}}{N} \iff & \forall C \in Ctx, C[M],C[N] \in \KT, \\
                      & (\forall (v_m,c_m) \in [[ C[M] ]]_{\Nat}, \exists (v_n,c_n) \in
                      [[ C[N] ]]_{\Nat}, (c_m \geqInf c_n)) \\
  & \land \\
                      & (\forall (u_n,d_n) \in [[ C[N] ]]_{\Nat}, \exists (u_m, d_m)
                      \in [[ C[M] ]]_{\Nat}, (d_n \geqInf d_m))
\end{align*}

In summary, Plotkin's cost relation is the conjunction of Hoare's and Smyth's
cost relations.
%
In other words, a term \(N\) is (Plotkin's) improved by a term \(M\) if and only
if in any context \(\CC\), the best possible case in the evaluation of
\(\CC [ N ]\) is \emph{better} than the best possible case in the evaluation of
\(\CC [ M ]\), while the worst possible case in the evaluation of \(\CC [ M ]\)
is \emph{worse} than the worst possible case in the evaluation of \(\CC [ N ]\).

