\section{Direct CSE Proof}\label{app:direc_cse_proof}

Here we present a proof of direct CSE, where we split it into two parts: first
we build two improvement simulations as auxiliary lemmas, and then, use them to
prove direct CSE.
%
We assume to be working with the partiality system where the only
observable effect is non-termination.

\begin{lemma}[Direct CSE]
  Let \(M\) be a closed term, \(\CC\) a term context,
  and \(x\) a fresh new variable, such that \( \CC[ M ] \in \KT\):
  \begin{displaymath}
    \mkRel{\LTo{M}{x.\CC[\tick M]}}{\gimprov{\Gamma_\bot}}{\LTo{M}{x.\CC[\LRet{x}]}}
  \end{displaymath}
\end{lemma}
\begin{proof}
  Let \(M\) be a closed term.
  %
  Since both terms evaluates \(M\), we proceed by case analysis on the
result of the evaluation of the closed term \(M\):
either \(\cost{[[ M]]}\) reaches a value \((m,m_c) \in \KV \times \N \)
or diverges represented by \(in_r(\bot)\).

\begin{itemize}
  \item The evaluation of \(M\) diverges:
    \( \cost{[[ M ]]} = in_r(\bot)\).
    %
    Since both expressions evaluate the expression \(M\), by definition of the
    partiality monad, both expressions evaluate to \(in_r(\bot)\).
    %
    Define the following closed \(\lambda\)-term relation:
    \begin{align*}
      {DCSE_\bot}_\T & \triangleq Id_{\KT} \\
                    & \cup \{ ({\LTo{M}{x.\CC[\tick M]}},{\LTo{M}{x.\CC[\LRet{x}]}}) \\
                    & \mid \CC \text{ is a closed context}, x \text{ a fresh variable} \} \\
      {DCSE_\bot}_\V &\triangleq Id_{\KV}
    \end{align*}
    
    The \(\lambda\)-term relation \({DCSE_\bot}\) is trivially an improvement
simulation. 

  \item The evaluation of \(M\) reaches a value \(m \in \KV\) with
    cost \(m_c \in \N \): \(\cost{[[ M ]]} = in_l(m, m_c)\).

    We define a closed \(\lambda-\)term relation where we replace each
occurrence of the closed term \(M\) with the term returning the closed value
\(m\).
    %
    \begin{align*}
     \RR_{\T} &\triangleq Id_{\KT} \cup \{ (\CC[ \tick[] M ], \CC[ \LRet{m} ]) \mid \CC \text{ closed term context} \} \\
     \RR_{\V} &\triangleq Id_{\KV} \cup \{ (\VV[ \tick[] M ], \VV[ \LRet{m} ]) \mid \VV \text{ closed value context} \}
    \end{align*}
    %
    Our task is to show that \((\RR_{\T}, \RR_{\V})\) is an improvement
    simulation, i.e. that \((\RR_{\T}, \RR_{\V})\) respects values and that
    every two related terms evaluate to related values through
    relator \(\Gamma_\bot\).
%%     Following the definition of improvement simulation
%% (Definition~\ref{def:app:cost}) we need to prove that the above closed
%% \(\lambda\)-term relation

    Let \(\CC\) be a closed term context such that \((\CC[\tick[] M],
\CC[\LRet{m}]) \in \RR_{\T}\), we prove that
%% their evaluations are related by the relation
\(
\cost{[[ \CC[\tick[] M] ]]}
\mathrel{\Gamma_{\bot} \RR_{\V}}
\cost{[[ \CC[\LRet{m}]) ]]}
\)
.
    %

    The proof proceeds by case analysis over the context \(\CC\) and the
application of the co-induction hypotheses.
    % 
    We show the most informative case where \(\CC = (\LTo{\DD}{x. \EE})\) for
    contexts \(\DD\) and \(\EE\).

    From hypotheses, we have that \(M \in \KT\) and \(\CC\) is a closed term
    context, and thus, \(FV(\DD[M]) = \emptyset\) and
    \(FV(\EE[M]) \subseteq \{ x \}\).

Our goal is to show that
\begin{displaymath}
    \cost{[[ \CC[ \tick[] M ] ]]} \mathrel{\cost{\Gamma_\bot} \RR_\V} \cost{[[ \CC[ \LRet{m} ] ]]}
\end{displaymath}
%
After replacing \(\CC\) by \((\LTo{\DD}{x. \EE})\) and
evaluating both sides of the relation we have that

\begin{displaymath}
  \begin{array}{c}
    \tick \cost{[[ \DD[\tick[] M ] ]]} >>= \lambda d . \cost{[[ \EE[\tick[] M ][x:=d] ]]}\\
    {\cost{\Gamma_\bot} \RR_\V}\\
    \tick \cost{[[ \DD[ \LRet{m} ] ]]} >>= \lambda d . \cost{[[ \EE[ \LRet{m} ][x:=d] ]]}\\
  \end{array}
\end{displaymath}

    We can remove the ticks at the beginning of both terms since tick is
    injective, and by T-relator~(Definition~\ref{def:monad:relator}) we
    split the bind operator:
    \begin{itemize}
    \item \(\cost{[[ \DD[\tick[] M ] ]]} \mathrel{\cost{\Gamma_\bot}
        \RR_\V} \cost{[[ \DD[ \LRet{m} ] ]]}\) follows from the
      fact that \(\DD\) is a closed term context and co-inductive hypothesis.
    \item Let \( d_0, d_1 \in \KV \) such that \( d_0
      \mathrel{\RR_{\V}} d_1 \), we have to show that
      \begin{displaymath}
      \cost{[[ \EE[\tick[] M][x:=d_0] ]]}
      \mathrel{\cost{\Gamma} \RR_{\V}}
      \cost{[[ \EE[\LRet{m}][x:=d_1] ]]}
      \end{displaymath}

      Note that we cannot apply the co-inductive hypothesis here since \(\EE\)
      may not be a closed term context, and thus, our goal is to get a closed
      term context to apply the co-inductive hypothesis.
      %
      From the definition of \(\RR_\V\), \( d_0 \mathrel{\RR_{\V}} d_1 \)
      implies two possible cases: either \(d_0 = d_1\) or there is a value
      context \(\VV\) such that \(d_0 = \VV[\tick[] M]\) and \(d_1
      = \VV[ \LRet{m} ]\).
      %
      Both cases have the same proof structure, and thus, we show one of them
      leaving the other one as an exercise to the reader.
      %
      Let \(\VV\) be a closed value context, and \(d_0,d_1\) as described above.
      %
      \begin{VProof}
      \vpT{\cost{[[ \EE[\tick[] M][x:=d_0] ]]}}
      \vpJ{\equiv}{\(d_0\) definition}
      \vpT{\cost{[[ \EE[\tick[] M][ x:=\VV[\tick[] M] ] ]]}}
      \vpJ{\equiv}{\(M \in \KT\) and hole-filling operation}
      \vpT{\cost{[[ \EE[ x:=\VV ][\tick[] M] ]]}}
      \vpJ{\cost{\Gamma_{\bot}} \RR_{\V}}{\(\EE[x:=\VV]\) is a closed term context and co-inductive hypothesis}
      \vpT{\cost{[[ \EE[ x:=\VV ][ \LRet{m}] ]]}}
      \vpJ{\equiv}{\(M \in \KT\) and hole-filling operation}
      \vpT{\cost{[[ \EE[ \LRet{m} ][ x:=\VV[ \LRet{m} ] ] ]]}}
      \vpJ{\equiv}{\(d_1\) definition}
      \vpEnd{\cost{[[ \EE[ \LRet{m} ][ x:= d_1 ] ]]}}
      \end{VProof}

      %% \begin{displaymath}
      %% \cost{[[ \EE[\tick[] M][x:=d_0] ]]}
      %% \equiv
      %% \cost{[[ \EE[\tick[] M][ x:=\VV[\tick[] M] ] ]]}
      %% \end{displaymath}
      %% , we have that:
      %% \begin{displaymath}
      %% \cost{[[ \EE[\tick[] M][ x:=\VV[\tick[] M] ] ]]}
      %% \equiv
      %% \cost{[[ \EE[ x:=\VV ][\tick[] M] ]]}
      %% \end{displaymath}
      %% Finally, since  we can
      %% apply the co-inductive hypothesis
      %% \begin{displaymath}
      %% \cost{[[ \EE[ x:=\VV ][\tick[] M] ]]}
      %% \mathrel{\cost{\Gamma_{\bot}} \RR_{\V}}
      %% \cost{[[ \EE[ x:=\VV ][ \LRet{m}] ]]}
      %% \end{displaymath}

      %% And thus we can undo all the steps made before proving that

      %% \begin{displaymath}
      %% \cost{[[ \EE[\tick[] M][x:=d_0] ]]}
      %% \mathrel{\cost{\Gamma_{\bot}} \RR_{\V}}
      %% \cost{[[ \EE[ \LRet{m}][ x:=d_1 ] ]]}
      %% \end{displaymath}
    \end{itemize}
    As result we have that
\[
  \begin{array}{c}
    \cost{[[ \DD[\tick[] M ] ]]} >>= \lambda d . \cost{[[ \EE[\tick[] M ][x:=d] ]]}\\
    {\cost{\Gamma_\bot} \RR_\V}\\
    \cost{[[ \DD[ \LRet{m} ] ]]} >>= \lambda d . \cost{[[  \EE[ \LRet{m} ][x:=d] ]]}\\
  \end{array}
\]

The other cases follow a similar structure, swapping substitution with
hole-filling taking advantage of the fact that \(M\) is a closed term and
applying the co-inductive hypothesis as necessary.
\end{itemize}

Finally, we show that relation \(\RR_{\V}\) respects values.
%
Let \(\VV\) be a closed value context such that
\(\VV[ \tick[] M ] \RR_{\V} \VV[ \LRet{m} ] \), and \(d \in \KV\), we
show that
\begin{displaymath}
  (\LApp{\VV[ \tick[] M ]}{d}) \mathrel{\RR_{\T}} (\LApp{\VV[\LRet{m} ]}{d})
\end{displaymath}
%
Which holds since \(d \in \KV\) and \(\VV\) is a closed value
context, and thus, \((\LApp{\VV}{d})\) is a closed term context.
%
Therefore, by definition of hole-filling, we have that
\begin{displaymath}
  (\LApp{\VV}{d})[ \tick[] M ] \mathrel{\RR_{\T}} (\LApp{\VV}{d})[\LRet{m} ]
\end{displaymath}

Consequently of the previous proof, we can conclude that the closed \(\lambda\)-term
relation \((\RR_\T,\RR_\V)\) is an improvement simulation.

Taking a step back, we still have to prove that direct CSE is an
improvement when the evaluation of the closed term \(M\) reaches a value.
%
Define the following \(\lambda\)-term relation:
\begin{align*}
  {DCSE}_\T & \triangleq \RR_\T \\
            & \cup \{ ({\LTo{M}{x.\CC[\tick M]}},{\LTo{M}{x.\CC[\LRet{x}]}}) \\
            & \mid \CC \text{ closed term context}, x \text{ fresh new variable} \} \\
  {DCSE}_\V & \triangleq \RR_\V
\end{align*}
%
To see why the above relation is an improvement simulation, we focus on the new
case added since the other terms are just the auxiliary lemma proved before.
%
Let \(\CC\) be a closed term context, \(x\) be a fresh new
variable, and recall that by hypothesis \(\cost{[[ M ]]} =
in_l(m,m_c)\), we have to show that:
%
\begin{displaymath}
  \cost{[[ \LTo{M}{x.\CC[\tick M]} ]]}
  \mathrel{\cost{\Gamma_\bot} {DCSE}_\V} 
  \cost{[[ {\LTo{M}{x.\CC[\LRet{x}]}} ]]}
\end{displaymath}
Which by evaluation and transitivity of relator is equivalent to show that:
\begin{displaymath}
  \tick[m_c+1] \cost{[[ \CC[\tick M] ]]}
  \mathrel{\cost{\Gamma_\bot} {DCSE}_\V} 
  \tick[m_c+1] \cost{[[ \CC[\LRet{m}] ]]}
\end{displaymath}

% \begin{align*}
% %%%%%%%%%%%%%%%%%%%%
%   & \tick \cost{[[ M ]]} >>= \lambda m. \cost{[[ \CC[\tick M][x := m] ]]}
%   \mathrel{\cost{\Gamma_\bot} {DCSE}_\V} 
%   \tick \cost{[[ M ]]} >>= \lambda m. \cost{[[ \CC[\LRet{x}][x := m] ]]} \\
% %%%%%%%%%%%%%%%%%%%%
%   & \tick[m_c+1] \cost{[[ \CC[\tick M][x := m] ]]}
%   \mathrel{\cost{\Gamma_\bot} {DCSE}_\V} 
%   \tick[m_c+1] \cost{[[ \CC[\LRet{x}][x := m] ]]} \\
% %%%%%%%%%%%%%%%%%%%%
%   & \tick[m_c+1] \cost{[[ \CC[\tick M] ]]}
%   \mathrel{\cost{\Gamma_\bot} {DCSE}_\V} 
%   \tick[m_c+1] \cost{[[ \CC[\LRet{m}] ]]} \\
% %%%%%%%%%%%%%%%%%%%%
% \end{align*}

Since \(\CC[\tick M ] \mathrel{\RR_\T} \CC[\LRet{m}]\) 
and by definition of improvement simulation, we have that:
\begin{displaymath}
  \cost{[[ \CC[\tick M ] ]]}
  \mathrel{\cost{\Gamma_\bot} \RR_\V}
  \cost{[[ \CC[\LRet{m}] ]]}
\end{displaymath}
since \(\RR_{\V} \subseteq {DCSE}_{\V}\) and \(\cost{\Gamma_{\bot}}\) is a relator
implies that:
\begin{displaymath}
  \cost{[[ \CC[\tick M ] ]]}
  \mathrel{\cost{\Gamma_\bot} {DCSE}_\V}
  \cost{[[ \CC[\LRet{m}] ]]}
\end{displaymath}
%
We can add the missing ticks since tick is monotonic, proving that
\(({DCSE}_\T,{DCSE_\V})\) is an improvement simulation.
%

Putting all together, we have shown that direct CSE is an improvement providing
an improvement simulation for each possible observation made in the partiality
system.
\end{proof}
