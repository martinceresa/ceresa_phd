\section{Outer Cost}\label{sec:outercost}

The intensional cost analysis presented in Section~\ref{sec:CostCalculus} yields 
one way of interpreting costs in an effectful language, assigning to every value a corresponding cost.
%
However, one might be interested in taking into account the cost of effects in
some other way, especially when there are terms that do not yield a value, and
thus, do not carry any cost information.
%
For example, in the exception system, the exceptions themselves are interpreted
as distinguished values that do not carry any cost information (they are just
labels), and thus, the notion of improvement derived in
Section~\ref{sec:notion:exceptions} does not compare the cost of the
evaluation of terms ending in exceptions.
%
%% limitation mentioned above
This is caused by the use of the \emph{inner cost monad} presented in
Definition~\ref{def:inner.cost.monad} where for a monad
\(T\) the inner cost monad of \(T\) equips values with costs \emph{inside} of
\(T\), \(\cost{T} \doteq T(X \times \Nat)\).
%
Moreover, we derived a cost analysis guided by a T-relator for \(T\) where we
compare values and costs; however, if the monad \(T\) interprets some effects as
elements without information, as in the case of exceptions, there will be
elements in \(\cost{T}\) without any assigned cost.

\begin{example}\label{ex:raiseraise}
  Let \(E\) be a non-empty set of exceptions and \(e \in E\).
  %
  We can prove 
  \( {raise}_e \gimprov{\Gamma^E} \tick {raise}_e\)
  since the relation
  \(({Id}_\T \cup \{({raise}_e, \tick {raise}_e)\}, {Id}_\V)\)
  of \(\lambda\)-terms is an improvement simulation.
\end{example}

Since the problem arises from equipping each value with a cost, instead of
keeping the cost locally to each value, we can record the total cost required to
evaluate a term.
%
In other words, we may attempt to keep a global counter where ticks are recorded to be
compared at the end of the evaluation.
%
For this, we need to provide a monad and a relator that form a compatible system.
%
We can start by defining the following functor:
%
\begin{definition}[Outer Cost Functor]
  Given an endo-functor \(T\), define:
  \[
    O_T(X) \triangleq (T(X) \times \Nat)
  \]
  \end{definition}

Unfortunately, this is not a monad in general: \emph{given a monad \(T\), \(O_T\) might not be a monad}.
%
However, for the case of the exception effect, we do obtain a monad.
%
This is akin to using the exceptions monad transformer on the cost monad.
%

\begin{definition}[Exception Monad Transformer]
  Let \(E\) be a set of uninterpreted symbols representing exceptions and \(M\)
be a monad.
  %
  The following is a monad capable of interpreting exceptions and effects
produced by \(M\):
\[
  \<ExceptT>(M)(X) \doteq M {( E + X )}_{\bot}
\]
%
where for each \(e \in E\), \(raise_{e}\) is interpreted as follows:
\[
  raise_e \doteq \eta (in_l(e))
\]

Moreover, it has a \(\<lift>\) function that promotes monadic \(M\) computation
into \(\<ExceptT>(M)\) computations.
\[
\<lift> \doteq M \, in_r
\]
\end{definition}

We then can use the cost monad, \(\mathbb{D}(A) \doteq
(A \times \Nat)\)~\citep{Ugo:Norm:Bisim}, to incorporate costs into the exception
system, \(\<ExceptT>(\mathbb{D})\), so we have exceptions and an interpretation of
the tick operation simply as \(\<lift> \, add_{1}\).
%
Unfolding the definition of the exception monad transformer, we can see that it
is equivalent to the outer cost functor, for any set \(E\) of exceptions:
%
\[
\<ExceptT>(\mathbb{D})(X) \doteq (X + E)_{\bot} \times \Nat
\]
%
And thus, a valid relator for such system is the result of the composition of
the cost relator and the exception relator:
\[ \Gamma_{EC} \doteq \cost{\Gamma} \circ {\Gamma}_{E} \]
%
Finally, it is easy to see that \((\<ExceptT>(\mathbb{D}), \Gamma_{EC})\) forms
a \(E\)-exception system, and moreover, we have all the ingredients required to
implement a cost-aware evaluation~(Section~\ref{subsec:costeval}).
%
This new exception system is more fine-grained since it can discern cheap
from expensive exceptions.

\begin{example}[example~\ref{ex:raiseraise} revisited]
Let \(E\) be a non-empty set of exceptions and \(e \in E\).
%
We can show that \( raise_{e} \not\gimprov{\Gamma_{EC}} \tick[] raise_{e} \) simply by
executing them.
%
Assume that \(raise_{e} \gimprov{\Gamma_{EC}} \tick[] raise_{e}\), however evaluating
terms within the trivial context, we have that:
\[
{[[ raise_{e} ]]}_{EC} \equiv \<lift> \, add_{1} \, (in_l(e), 0) \equiv (in_l(e), 1)
\]
%
while
%
\[
{[[ \tick[] raise_{e} ]]}_{EC}
\equiv \<lift> \, add_{1} \, {[[ riase_{e} ]]}_{EC}
\equiv \<lift> \, add_{1} \, (in_l(e), 1)
\equiv (in_l(e), 2)
\]
%
And finally, there is no relation \(R \subseteq X \times Y\) that can be lifted
with relator \(\Gamma\), such that
\((in_l(e),1) \mathrel{{\Gamma}_{EC}(R)} ((in_l(e)),2)\).
\end{example}
% ack cost-aware

It would seem that we can have an outer cost monad by stacking a monad
transformer over the cost monad.
%
Nevertheless, it is not clear how to obtain a compatible system in general.

The exception monad transformer is defined simply as the composition of two
functors, \(\<ExceptT>(M) = M \circ E\), and therefore we obtain a compatible system
almost for free.
%
However, most monad transformers do not fit into this pattern.

An essential characteristic of monad transformers is their ability to lift
computations~\citep{Jaskelioff.2010.MonadTrans,JM:TCS:2010}, and thus, it makes sense to
build the cost (and operational) system around the lift operation.
%
In other words, we would require that the lift operation would be a
continuous operation.
%
Moreover, we can go a step further and build our system starting with the two basic
required effects: divergence and cost, and add new effects by stacking
monad transformers.
%
The problem arrives when we try to define a relator for such a system.
%
It seems likely that we would also require a notion of \emph{relator
transformer} enabling us to compare effectful elements, but it is not clear how
to force them to take into account our restrictions over cost.
%
A clear candidate would be:
\begin{definition}[T-Relator Transformer]
  Let \(M\) be a monad, and \(\hat{T}\) be a monad
transformer.
%
A T-relator transformer \(\Theta\) is an operator that takes a T-relator
\(\Gamma\) for \(M\), and returns a T-relator for \(\hat{T}M\), such that the
operator \(\<lift>\) lifts related values into effectful related values.
%
More formally, for any two sets \(X,Y\), and a relation \(\mathbb{R} \subseteq X \times Y\):
%
\[
  \forall a \in M(X), b \in M(Y),
  a \mathrel{\Gamma \mathbb{R}} b
  \implies
  \<lift>(a) \mathrel{\Theta\Gamma \mathbb{R}} \<lift> (b)
\]
  \end{definition}

But we need to devise a property that guarantees that
\(p \mathrel{\Theta\Gamma} q\) if and only if the cost in \(q\) is less than or
equal to the cost in \(p\).
%
Note that at the semantic level we are covered: if we can approximate terms in
monad \(M\), we can approximate them in monad \(\hat{T}(M)\) since \(\<lift>\)
is continuous.
%
The problem is that the notion of cost is not part of \(\hat{T}\) and hence the
transformer can ignore it. For example, the relator \(\Theta\Gamma\) may relate terms
ignoring their cost.

Another hurdle of this approach is that some monads
do not have a clear monad transformer counterpart, as it is the case of the
sub-probability monad.
%

% However, the study of modular composition of effects is still an open problem,
% and we have other alternatives to the composition of their effects either
% following a monad transformer based
% approach~\citep{Schrijvers.2019.MonadTransformersModEffects}, a different way to
% combine monads using coproducts~\citep{Luth.2002.MonadCoproduct} or a more
% general notion of monads, parameterised monads~\citep{Atkey.2009.Parameterised}.

%% Fortunately, the result of the application of the outer cost functor to the
%% exception monad (Example~\ref{ex:monads}) \emph{yields} a monad capable to
%% observe evaluations' cost.
%% %
%% The idea is similar to evaluate terms while keeping a global account of the
%% ticks observed through the evaluation of a term.

% \begin{example}[Exceptions]
%   In Section~\ref{sec:notion:exceptions} a notion of improvement was presented
%   to a language with exceptions. Such notion was derived from the application of
%   the Inner Cost Monad (Definition~\ref{def:inner.cost.monad}) to the Exception
%   Monad (Definition~\ref{ex:exception}).

%   In this example a tailor-made monad where cost is also introduced to exceptions.
%   The cost applied to exceptions represents the cost needed for an expression to
%   reach an expression.

%% \begin{definition}[Exception Cost Monad]\label{def:out:exceptmonad}
%%   Let \(E\) be a set of symbols, define the following:
%%   \begin{align*}
%%     E_C(X) &\triangleq ({(X + E)}_{\bot} \times \Nat) \\
%%     \eta(x) &\triangleq (in_l(in_l(x)), 0) \\
%%     m >>= f &\triangleq
%%               \begin{dcases}
%%                 (in_r(\bot), n)    & \text{if } m = ( in_r(\bot) , n)  \\
%%                 (in_l(in_r(e)), n) & \text{if } m = (in_l(in_r(e)), n) \\
%%                 plus (f(x), n) & \text{if } m = (in_l(in_l(x)), n) \\
%%               \end{dcases} \\
%%    plus((v,m),n) &\triangleq (v, m + n)
%%   \end{align*}
%% \end{definition}

%% Where for each \(e \in E\), \(\cost{[[ raise_e ]]} = (in_l(in_r(e)), 1)\).

%% \begin{definition}[Exception Cost Relator]
%%   Let \(X,Y\) be set, and \(\RR \subset X \times Y\) a relation.
%%   %
%%   Define the exception cost relator \(\Gamma^{E_C} \RR \subseteq E_C(X) \times
%%   E_C(Y)\) as follows:
%%   \begin{displaymath}
%%     \mkRel{(x,n)}{\Gamma^{E_C}\RR}{(y,m)} \iff \mkRel{x}{\Gamma^E\RR}{y} \land n \geq m
%%   \end{displaymath}
%% \end{definition}

%% The operation \(\Gamma^{E_c}\) is a relator for \(E_C\) since it is the
%% conjunction between the relator \(\Gamma^E\) and \((\geq)\), and
%% moreover, \(\Gamma^{E_C}\) is a T-relator for \(E_C\) since cost can be
%% accumulated through the binding operator.
%% %
%% Finally, \((\Gamma^{E_C}, E_c)\) forms an exception system since the
%% interpretation of each \(raise_e\) operation is related through
%% \(\Gamma^{E_C}\).

%% However, the approach presented in previous sections has the advantage of being
%% systematic: given a signature \(\Sigma\) and a \(\Sigma\)-system \((\Gamma,
%% T)\), we can derive a \(\Sigma\)-system \((\cost{\Gamma}, \cost{T})\) for signature
%% \(\cost{\Sigma}\).

%% The conclusion of this section is that it is possible to present a tailor-made
%% \((\Gamma, T)\) system for a given signature \(\Sigma\), and perform a better
%% or more precise cost analysis but it comes with a price: one should prove
%% that \((\Gamma,T)\) is a system.

% Another solution is to take a bit the monad/relator
% in order to be aware of such information. On the lines of \(T_e X \triangleq X +
% E\) where \(E \triangleq (DivZero \mathbb{N}) + (Err42 \mathbb{N})\), etc.

% Replace raise/catch for a model like evidence/catch. TODO: IDK if something like
% this exists, thinking a bit about this IDK if it is a good idea afterall.
% On the lines of \(T X \triangleq (X +
% E X)\) with \( E X \triangleq n \times X, n \in \mathbb{N}\)

% But anyway this is a \textbf{limitation} and should be mentioned in the final
% version of this work.

% \subsection{Why Writer may not be enough}\label{sec:Ideas:NotEnough}

% \pnote{Nice conclusion, it might be useful at the end.}
% Following the same example as before, 

% Instead of \(T_E = (X + Raise)\) where you have two operations like:
% \{raise : T X, raise = inr(T); catch : T^2 X -> T X\}
% I propose something like \(T_{Ev} \triangleq X + ( Evidence X )\)
% \{ev : T X -> T X,ev x = inr(ev(x))\}

% A possible solution to this somewaht inconvinient is to use a State Monad
% Transformer instead, which will enable us to keep a global counter independent
% of local \emph{ignorant} computation.

% \intuition{why is all right that we cannot observe costs with Exception as it is?
% But it seems that we cannot escape from that, \(StateT s m a \equiv s -> m (a,
% s)\) and so we are in the very same problem. But I think this is good, since we
% are using all this model to compare the results of computing terms \textbf{into
%   values} and if some terms do not compute into values that comparison has no sense.}
%

% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
% Old work on transformers.

%% Following the computation model presented by \citeauthor{DalLago2019}, given a
%% signature \(\Sigma\) describing operations, we need a monad \(T\) that interprets such
%% operations, here we assume that monad \(T\) accepts a monad transformer
%% \(\hat{T}\) such that we can introduce new effects through another monad while
%% still being able to interpret the effects produced by the operations in \(\Sigma\).
%% %
%% In particular, in our case, \(\hat{T}\<DCost>\) is our final monad to
%% interpret \(\Sigma_{\tick}\) operations.

%% \begin{definition}[\(\<DCost>\) is an ordered monad]
%%   Let \(X\) be a set.
%%   %
%%   Define \(\bot_{X} \doteq in_{r}(\bot)\),
%%   and for any \(x, y \in \<DCost>(X)\):
%%   \[
%%     x = in_{l}(a, c) \wedge y = in_{l}(a, d) \wedge c \geq d \implies x \sqsubseteq y
%%   \]
%%   \[
%%     x = \bot_{X} \implies x \sqsubseteq y
%%   \]
%%   \end{definition}

%% Let \(\hat{T}\) be a monad transformer, we require that \(\hat{T}\<DCost>\) be an
%% ordered monad, and also that \(\<lift>\) the order of \(\<DCost>\).
%% %
%% In symbols, for each set \(X\), there is an order \((\hat{T}\<DCost>(X), \sqsubseteq_{X}, \bot_{X})\):
%% %
%% \begin{align*}
%%   & \forall a,b \in \<DCost>(X), a \sqsubseteq_{\<DCost>X} b \implies \<lift>(a) \sqsubseteq_{X} \<lift>(b) \\
%%   & \bot_{X} \equiv \<lift> (\bot_{\<DCost>X})
%% \end{align*}
%% %
%% This is basically to ask that the lifting operator \(\<lift>\) is continuous.
%% %
%% And finally, we also require that \(\hat{T}\<DCost>\) to be a \(\Sigma\)-continuous
%% monad, that is to have a continuous \(\Sigma\)-algebra structure over its order.
%% %
%% As result, we can define a monadic operational semantic as defined in
%% Section~\ref{sec:Calculus}

%% What we need know is to define a relevant relator (and T-relator) for monad
%% \(\hat{T}\<DCost>\).
%% %
%% Remember that a relator is also a lifting mechanism that takes a relation
%% between ground (non-effectful) elements, and lift it, so we can compare
%% effectful ones.
%% %
%% In this case, we need to do the same but following the approach of monad
%% transformer, that is, we need a \emph{relator transformer} that lift a relator
%% for a monad \(M\) and returns a relator for monad \(\hat{T}M\).
%% %
%% We also need that the monad operations are respected, and in addition to that,
%% that the lifing operator respect such transformation too.

%% \begin{definition}[T-Relator Transformer]
%%   Let \(M\) be a monad, and \(\hat{T}\) be a monad
%% transformer.
%% %
%% A t-relator transformer \(\Theta\) is a operator that takes a t-relator \(\Gamma\) for
%% \(M\), and returns a t-relator for \(\hat{T}M\), such that the operator
%% \(\<lift>\) lifts related values into related ones.
%% %
%% In symbols, for any two sets \(X,Y\), and a relation \(\mathbb{R} \subseteq X \times Y\):
%% %
%% \[
%%   \forall a \in M(X), b \in M(Y),
%%   a \mathrel{\Gamma \mathbb{R}} b
%%   \implies
%%   \<lift> a \mathrel{\Theta\Gamma \mathbb{R}} \<lift> b
%% \]
%%   \end{definition}

%% This enable us to build applicative and contextual relation between programs,
%% following Section~\ref{sec:app:bisim}.

%% Now we ask ourselves if that is enough to generate a reasonable cost system, and
%% the answer is \emph{no}, we still need to ask a bit more about the system.

%% The most basic requirements are that ticks can observe omega terms, that is, a
%% term that can improve ignoring ticks is a divergent one,
%% %
%% \[
%%   m \improv* \tick[] m \implies m \equiv \Omega
%% \]
%% %
%%  and that removing ticks is always an optimization:
%% \[
%%   \tick[] m \improv m
%% \]
%% %
%% This last equation is equivalent to showing that:
%% \[
%%   \tick[] \improv \eta(\<unit>)
%% \]

%% We left this line of work as future work.

%% Finally, we can describe the outer exception cost monad
%% (Definition~\ref{def:out:exceptmonad}) simply as \(\<ExceptT>(\<DCost>)\).
