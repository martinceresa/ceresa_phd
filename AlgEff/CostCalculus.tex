\section{Improvement Theory with Effects}\label{sec:CostCalculus}

%% Section's introduction.
%% Main Ideas:
%% + Inner product to introduce cost analysis and Relators Composition
%% + Improvement Simulation = Applicative \Gamma_c-Simulation
%% + Improvement = Observational Approximation (with \Gamma_c)

This section represents the main contribution of this article:
we present a way to perform relational intensional analysis on the
evaluation of terms in the presence of algebraic effects.
%
We make use of effectful applicative similarity and contextual approximation
relations developed in the previous section, while adding a new observable
effect representing the required cost to evaluate a term.
%
As result we get two new relations: \emph{effectful applicative cost similarity}
and \emph{improvement}.
%
Additionally, we show that when the only effect is non-termination we obtain the classical notions
of improvement simulation and improvement presented by
\citet{Sands:Op:Theories}.
%
%We discuss an alternative way of introducing costs in Section~\ref{sec:outercost}.

We introduce costs by adding a new unary operation called tick \((\tick)\) to a
given signature \(\Sigma\), and we call the resulting signature \(\Sigma_{\tick}
\equiv \Sigma \cup \{\tick\}\).
% 
The tick operation represents spending one unit of cost, and thus, enable
us to keep a record of how many units of cost were spent during the evaluation of
a term.

For the rest of the section, we assume a fixed signature \(\Sigma\) and
a \((T, \Gamma)\)-system. 

% The section consists of  three parts: first, we
% build a new \(\Sigma_{\tick}\)-system, second we define a new cost
% aware evaluation relation between terms and effectful values, and finally we
% instantiate effectful applicative similarity and contextual approximation to the
% new \(\Sigma_{\tick}\)-system.

\subsection{Adding Cost}

To keep a record of the cost required to compute values, we pair each element
with a natural number.
%
We first define some internal functions to add costs which we later use to
define the interpretation of the tick operation.

Let \(\Nat\) be the natural numbers with one additional element, \(\infty\),
denoting the supremum element of \(\Nat\).
%
% The distinguished element \(\infty\) is the supremum of \(\Nat\).

\begin{definition}
  Let \(X\) be a set. We define a family of functions called \(add_n\)
  indexed by \(n\) in \(\N\).
  %
  \begin{align*}
   add_n &: (X \times \Nat) -> (X \times \Nat)\\
   add_n (t, c) &\equiv (t, c + n)
  \end{align*}
\end{definition}

For each \(n \in \N\), we can lift the function \(add_n\) into monad \(T\)
and define a new family of operations called tick as follows:
%
\begin{align*}
  \tick[n] &: T (X \times \Nat) -> T (X \times \Nat) \\
  \tick[n] & \equiv T \ add_n
\end{align*}

We define a new monad based on monad \(T\) that equips every element
with a natural number representing the cost required to compute it.
%
Pure values require no cost, but whenever the bind operator is used, it
accumulates the cost required to compute its first argument.

\begin{definition}[Inner Cost Monad]\label{def:inner.cost.monad}
  Define \(\cost{T}\) to be the monad:
  \begin{align*}
    \cost{T}(X) &\equiv T(X \times \Nat) \\
    \eta_{\cost{T}}(x) &\equiv \eta_T(x, 0) \\
    m \cost{>>=} f &\equiv  m >>=_T (v, c) |-> \tick[c] (f(v))
  \end{align*}
  \end{definition}

% The \emph{inner cost monad} assigns a natural number to each element % \emph{locally}
% representing the cost required to compute it.
% %
This way of transforming a monad into another is widely known in the Haskell
community as the writer monad transformer with the additive monoid of natural
numbers~\citep{MonadTrans,JM:TCS:2010}.

Other combinations of transformers or monads can also lead to a
correct account of costs (cf. Section~\ref{sec:outercost}).
%
However, the writer monad transformer has the advantage that it enables us to
perform cost analysis without modifying how effects are interpreted, we are
equipping values with their cost while everything else remains the same.
%
% We begin showing that every the inner cost monad \(\cost{T}\) has an order,
% whenever \(T\) has one.

% \begin{lemma}
%   The inner cost monad of an ordered monad is an ordered monad.
%   %% Every order of monad \(T\) is also an order of the inner cost monad \(\cost{T}\).
% \begin{proof}
%   Let \((\sqsubseteq)\) be an order of \(T\) and therefore for each set \(X\)
% there is an \wCPPO\ \((T(X), \sqsubseteq_{X}, \bot_{X})\).
%   %
%   In particular, for any set \(X\), \((X\times\Nat)\) is also a set, and thus,
% there is an \wCPPO\ \((T(X \times \Nat), \sqsubseteq_{(X\times\Nat)},
% \bot_{(X\times\Nat)})\).
% \end{proof}
% \end{lemma}


% \subsection{Monad Composition/Writer's system}

Since we are building the previous notion of cost around a specific way of
building a monad, i.e. writer transformer monad, we take a step back and give a
general way to derive a \(\Sigma\)-system using the transformation building blocks.
%
In this case, we require a monoid structure with a preorder to define a relator.

\begin{lemma}\label{cost:lmm:monoidrel}
  Given a monoid \((M, \cdot, e)\) and a preorder \((\sqsubseteq) \subseteq M \times M\).
  %
  % The product \(\_ \times M\) is a monad, and more over,
  The pointwise relation product with \(\sqsubseteq\), \( (\mathrel{-} \times \sqsubseteq)\), forms a T-relator for
product monad \((\mathrel{-} \times M)\).
  \end{lemma}

For the rest of this section, we assume that \((M, \cdot, e)\) is a monoid,
\((\sqsubseteq) \) is a preorder relation over the set \(M\), and we note monad
\(T \circ (\mathrel{-} \times M)\) as \(\mathbb{W}_{T,M}\).

The goal of this section is to prove that \(( \mathbb{W}_{T,M}, \Gamma \circ (\mathrel{-} \times \sqsubseteq) )\)
forms a valid \(\Sigma\)-system, in other words, that \(\mathbb{W}_{T,M}\) is an ordered
monad and \(\Gamma \circ (\mathrel{-} \times \sqsubseteq)\) is an inductive T-relator for
\(\mathbb{W}_{T,M}\).

\begin{lemma}
  For every ordered monad \(T\), monad \(\mathbb{W}_{T,M}\) is also an ordered monad.
  % Let \(T\) be an ordered monad then monad \(\mathbb{W}_{T,M}\) is an ordered
% monad.

  \begin{proof}
    Let \((\preceq)\) be an order of \(T\).
    %
    By definition of ordered monad, for each set \(X\) there is an \wCPPO\
\((T(X), \preceq_{X}, \bot_{X})\).
  %
  In particular, for any set \(X\), \((X \times M)\) is also a set, and thus,
there is an \wCPPO\ \((T(X \times M), \preceq_{(X\times M)},
\bot_{(X \times M)})\).
    \end{proof}
  \end{lemma}

\begin{lemma}
  The operator defined in Lemma~\ref{cost:lmm:monoidrel}, \(\Gamma \circ (\mathrel{-} \times \sqsubseteq)\), is a
T-relator for monad \(\mathbb{W}_{T,M}\).

\begin{proof}
First, since relators are closed under composition,
\(\Gamma \circ (\mathrel{-} \times \sqsubseteq)\) is a relator for the
underlying functor of \(\mathbb{W}_{T,M}\).
%
Second, we proceed to show that
\(\Gamma \circ ( \mathrel{-} \times \sqsubseteq )\) respects the monad
operations of \(\mathbb{W}_{T,M}\).

  Let \(X,X', Y, Y'\) be sets,
  \(f \colon X \to \mathbb{W}_{T,M} (X'), g \colon Y \to \mathbb{W}_{T,M} (Y')\) be
  functions, and
  \(\mathbb{R} \subseteq X \times Y, \mathbb{S} \subseteq X' \times Y'\) be two relations.

  \begin{itemize}
    % \item We show that the injection function \(\eta_{TM}\) respects  relation \(\mathbb{R}\)
    \item The relator \({\Gamma \circ ( \mathbb{-} \times \sqsubseteq)}\)
respects the natural transformation \({\eta}_{TM}\).
          %
          Let \(x, y\) be two values, such that \(x \mathrel{\mathbb{R}} y\), we show
that \(\eta_{TM}(x) \mathrel{\Gamma \circ ( \mathbb{R} \times \sqsubseteq)} \eta_{TM}(y)\).
          \begin{VProof}
            \vpT{\eta_{TM}(x)}
            \vpJ{\equiv}{Writer transfomer monad definition}
            \vpT{\eta_{T}(x, e)}
            \vpJ{\Gamma (\mathbb{R} \, \times \sqsubseteq)}{\(\Gamma\) is a T-relator for \(T\) and \((x,e) \mathrel{\mathbb{R} \, \times \sqsubseteq} (y,e)\)}
            \vpT{\eta_{T}(y, e)}
            \vpJ{\equiv}{Write transformer monad definition}
            \vpEnd{\eta_{TM}(y)}
            \end{VProof}
    \item Let \(u \in \mathbb{W}_{T,M}(X), v \in \mathbb{W}_{T,M}(Y)\) such that
\(u \mathrel{\Gamma(\mathbb{R} \, \times \sqsubseteq)} v\) and \(f,g\) forms an homomorphism between
\(\mathbb{R}\) and \(\Gamma(\mathbb{S} \, \times \sqsubseteq)\).
%
          % We show that:
          % \( (u {>>=}_{\mathbb{W}_{T,M}} f) \mathrel{\Gamma(\mathbb{S} \times \sqsubseteq)} (v {>>=}_{\mathbb{W}_{T,M}} g)\).
%
 \begin{VProof}
   \vpT{(u {>>=}_{\mathbb{W}_{T,M}} f) \mathrel{\Gamma(\mathbb{S} \, \times \sqsubseteq)}
     (v {>>=}_{\mathbb{W}_{T,M}} g)}
   \vpJ{\equiv}{Writer transformer monad definition}
   \vpEnd{%
     \begin{array}{l}
       \hspace{1em} (u {>>=}_{T} ( u_X , u_M ) \mapsto T(\cdot) u_M f(u_X)) \\
       \hspace{1em} \mathrel{\Gamma(\mathbb{S} \, \times \sqsubseteq)} \\
       \hspace{1em} (v {>>=}_{T} ( v_Y , v_M ) \mapsto T(\cdot) v_M g(v_Y))
       \end{array}
   }
   \end{VProof}
   %
Since \(\Gamma\) is a T-relator for \(T\), and \(u \mathrel{\Gamma} v\), we only need
to show that for any \((x, m_{1}) \in X \times M\) and \((y, m_{2}) \in Y \times M\) such that
   \((x , m_{1}) \mathrel{\mathbb{R} \, \times \sqsubseteq} (y , m_{2})\),
   \(T(\cdot)(m_{1})(f(x)) \mathrel{\Gamma(\mathbb{S} \, \times \sqsubseteq)} T(\cdot)(m_{2})(g(y))\).
   %
Which follows from the fact that \(\Gamma\) is a T-relator for \(T\),
\(m_{1} \sqsubseteq m_{2}\) and \(f,g\) forms an homomorphism between \(\mathbb{R}\)
and \(\Gamma(\mathbb{S} \, \times \sqsubseteq)\)
\end{itemize}
\end{proof}
\end{lemma}

% \begin{lemma}
%   Let \(T\) be a monad and \(\Gamma\) be an inductive T-relator for \(T\).
%   %
%   The resulting relator \(\Gamma \circ (\_ \times \sqsubseteq)\) is an inductive T-relator for monad \(\mathbb{W}_{T,M}\).
%   %
%   \begin{proof}
%     Let \(X,Y\) be sets and \(\mathbb{R} \subseteq X \times Y\), \(\Gamma(\mathbb{R} \times \sqsubseteq)\) is an
% inductive relation since \(\Gamma\) is an inductive relator.
%     \end{proof}
%   \end{lemma}

Finally, it is easy to see that \(\mathbb{W}_{T,M}\) is \(\Sigma\)-continuous, since
it follows from \(T\) being \(\Sigma\)-continuous.
%
As result, we have that for any given monoid \((M, \cdot, e)\) and preorder
\((\sqsubseteq) \subseteq M \times M\), \((\mathbb{W}_{T,M} , \Gamma \circ (\mathrel{-} \times \sqsubseteq))\) forms a \(\Sigma\)-system.

\subsection{Lifting Cost-aware Relations}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Following the procedure dictated in Section~\ref{sec:app:bisim}, we
require a relator to lift relations between values to relations
between effectful values.
%
% We achieve this in two steps: first we define a simple relator equipping
% relations with the ability to compare natural numbers which we call \emph{cost
% relator}, and second, we use \(\Gamma\) to lift cost-aware relations into effectful
% cost-aware relations between values.
Since we are pairing values with the cost required to compute them, it makes
sense to use the relator defined in Lemma~\ref{cost:lmm:monoidrel} applied to
the additive monoid of natural numbers ordered by \((\geq)\), which yields the
following definition.

\begin{definition}[Cost Relator]\label{def:cost:rel}
  Let \(X,Y\) be sets, and \(\RR \subseteq X \times Y\).
  Define \(\cost{\RR} \subseteq (X \times \Nat) \times (Y \times \Nat)\) to be
  the product relation of relations \(\RR\) and \((\geqInf)\).
  \begin{displaymath}
   (x,m) \mathrel{\cost{\RR}} (y,n) \iff x \mathrel{\RR} y \land m \geqInf n
  \end{displaymath}
\end{definition}

The above definition states how to compare costs when we equip values with their
cost.
%
In this way, the cost relator of relation \(\RR\) is a refined relation that not
only relates terms by \(\RR\) but also takes costs into account.
%
This definition comes from~\citet{Ugo:Norm:Bisim} and encodes the definition of
improvement given by~\citet{Sands97improvementtheory}.

%%%%%%%%%%%%%%%% Old very complicated way to prove that (\cost{T}, \cost{\Gamma}) is
%%%%%%%%%%%%%%%% a \Sigma-system.
%
% Relators are closed under composition, and thus, the result of composing
% \(\Gamma\) with the cost relator is a relator \(\Gamma_{\Nat}\) for
% \(\cost{T}\).

% \begin{definition}\label{def:cost:rel}
%   Let \(X,Y\) be sets, and \(\RR \subseteq X \times Y\).
%   %
%   We define the cost relator \(\cost{\Gamma}\) of \(\Gamma\) for \(T\)
%   as follows:
% \[
%   \mkRel{p}{\cost{\Gamma}\RR}{q} \iff \mkRel{p}{\Gamma (\cost{\RR})}{q}
% \]
% \end{definition}

% We can lift homomorphisms between two relations into homomorphisms between
% lifted relations with \(\Gamma\) and the underlying functor of monad \(T\).
% %
% Remember that an homomorphism between \(R \subset X \times Y\) and \(R' \subset
% X' \times Y'\) is a pair of functions \(f : X -> X'\) and \(f' : Y -> Y'\) such
% that \(\forall x, y, \mkRel{x}{R}{y} \implies \mkRel{f(x)}{R'}{f'(y)}\).

% \begin{lemma}\label{lmm:maplifting}
%  Let \(X,X',Y,Y'\) be sets, \(\RR \subseteq X \times Y\),
%  \(\mathcal{S} \subseteq X' \times Y'\), \(f : X -> X'\),
%  \(g : Y -> Y'\), such that \((f,g)\) forms an homomorphism between \(\RR\) and
%  \(\mathcal{S}\).
%  %
%  Then, for any \(p \in T(X)\) and \(q \in T(Y)\)
%  such that \(p \mathrel{\Gamma\RR} q\),
%  \(T(f)(p) \mathrel{\Gamma\mathcal{S}} T(g)(q)\).
% \end{lemma}

% The proof is easy, and follows from the definition of relators.
% %
% Remember that relators are endo-functors in the category of relations, and thus,
% map homomorphisms between relations to homomorphisms between relations.

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % \martin{Not so sure why I am stating this lemma}
% % \begin{lemma}\label{lmm:tick:mono}
% %   Let \(X\) be a set, for any \(x \in (X \times \Nat)\),
% %   \( x  \mathrel{\cost{Id_X}} \tick x \).
% %  \(\tick \) is monotonic. Revisit later
% % \begin{proof}
% %   By Lemma~\ref{lmm:maplifting} and monotonicity of addition.
% % \end{proof}
% % \end{lemma}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The following lemma states that we can worsen terms' cost by adding cost to
% their computation whenever we add more cost to the already more expensive term.

% \begin{lemma}\label{lmm:worsening}
%   Let \(X,Y\) be sets, \(\RR \subseteq X \times Y\),
%   \(p \in \cost{T}(X), q \in \cost{T}(Y)\) such that \(p
%   \mathrel{\cost{\Gamma}\RR} q\).
%   Then for any two natural numbers \(c_1\) and \(c_2\), such that \(c_1 \geqInf
%   c_2\), \(\tick[c_1] p \mathrel{\cost{\Gamma}\RR} \tick[c_2] q\).
%   \begin{proof}
%     Let \(c_1, c_2 \in \N\), such that \(c_1 \geqInf c_2\).
%     %
%     The functions \(add_{c_1}\) and \(add_{c_2}\) are ordered pointwise:
%     \(add_{c_1} \geq add_{c_2}\), and thus, \((add_{c_1},add_{c_2})\) forms a
%     endo-homomorphism in \(\cost{\RR}\).
%     %
%     By Lemma~\ref{lmm:maplifting}, % and the definition of \(\tick\), we have
%     that for any \(p\) in \(\cost{T}(X)\) and \(q\) in \(\cost{T}(Y)\), such
%     that \(p \mathrel{\cost{\Gamma}\RR} q\), then \(T(add_{c_1})(p)
%     \mathrel{\cost{\Gamma}\RR} T(add_{c_2})(q)\), equivalent to \(\tick[c_1] p
%     \mathrel{\cost{\Gamma} \RR} \tick[c_2] q\) by definition of \(\tick\).
%   \end{proof}
% \end{lemma}

% We show that the relator \(\cost{\Gamma}\) is in fact a T-relator
% for \(\cost{T}\), in other words, it behaves as expected with the bind and
% return operators.

% \begin{lemma}
%   The relator \(\cost{\Gamma}\) is a T-relator for \(\cost{T}\).
% \begin{proof}
%  Let \(X,Y,X',Y'\) be sets, \(f : X -> \cost{T}(X'), g : Y -> \cost{T}(Y')\) be
%  functions, \(\RR \subseteq X \times Y, \mathcal{S} \subseteq X' \times Y' \) be relations.

%  The proof consists in showing that the injection function \(\eta_{\cost{T}}\)
%  and the bind operation of \(\cost{T}\) respect the orders lifted by
%  \(\cost{\Gamma}\), and thus, we split the proof in two:

%  \begin{itemize}
%  \item Let \(x \in X, y \in Y\), such that \(x \mathrel{\RR} y\), we have to
%    show that \(\eta_{\cost{T}}(x) \mathrel{\cost{\Gamma} \RR} \eta_{\cost{T}}(y)\).
%    %
%    \begin{VProof}
%      \vpT{x \mathrel{\RR} y}
%      \vpJ{\implies}{ cost relator definition }
%      \vpT{(x,0) \mathrel{\cost{\RR}} (y,0)}
%      \vpJ{\implies}{\(\Gamma\) is a T-relator for \(T\)}
%      \vpT{\eta_{T}(x,0) \mathrel {\Gamma \cost{\RR}} \eta_{T}(y,0)}
%      \vpJ{\equiv}{by Definition~\ref{def:cost:rel}}
%      \vpT{\eta_{T} (x,0) \mathrel {\cost{\Gamma} \RR} \eta_{T}(y,0)}
%      \vpJ{\equiv}{definition of \(\eta_{\cost{T}}\)}
%      \vpEnd{\eta_{\cost{T}} (x) \mathrel {\cost{\Gamma} \RR} \eta_{\cost{T}}(y)}
%    \end{VProof}
%    %
%  \item Let \(u \in \cost{T}(X), v \in \cost{T}(Y)\) such that \( u
%    \mathrel{\cost{\Gamma}\RR} v\), and \(f,g\) forms an homomorphism between
%    \(\RR\) and \(\cost{\Gamma}\mathcal{S}\).
%    % assume that for any \(x \in X\), \(y \in
%    % Y\), such that \( x \mathrel{\RR} y\), \(f(x)
%    % \mathrel{\cost{\Gamma}\mathcal{S}} g(y)\),
%    We have show that \((u \cost{>>=} f) \mathrel{\cost{\Gamma}\mathcal{S}} (v
%    \cost{>>=} g)\).
%    \begin{VProof}
%      \vpT{(u \cost{>>=} f) \mathrel{\cost{\Gamma}\mathcal{S}} (v \cost{>>=} g)}
%      \vpJ{\equiv}{Definition~\ref{def:cost:rel}}
%      \vpT{(u \cost{>>=} f)
%        \mathrel{\Gamma\cost{\mathcal{S}}} (v \cost{>>=} g)}
%      \vpJ{\equiv}{Definition of \((\cost{>>=})\)}
%      \vpEnd{(u >>= (u', c_1) |-> \tick[c_1] (f(u'))) \mathrel{\Gamma\cost{\mathcal{S}}} (v >>= (v',c_2) |->
%        \tick[c_2] (g(v')))}
%    \end{VProof}

%    Since \(\Gamma\) is a T-relator for \(T\), and \(u
%    \mathrel{\Gamma\cost{\RR}} v\), we need to show that for any \((x,c_1) \in X
%    \times \Nat, (y,c_2) \in Y \times \Nat\), such that \((x,c_1)
%    \mathrel{\cost{\RR}} (y,c_2)\), then it also holds that \(\tick[c_1](f(x))
%    \mathrel{\Gamma\cost{\mathcal{S}}} \tick[c_2](g(y))\).
%    %
%    Let \((x,c_1) \in X \times Nat\) and \((y, c_2) \in Y \times Nat\) such that
%    \((x,c_1) \mathrel{\cost{\RR}} (y,c_2)\), and thus, by definition of the cost
%    relator, \(x \mathrel{\RR} y\) and \(c_1 \geqInf c_2\).
%    %
%    From \(x \mathrel{\RR} y\) we have that \(f(x)
%    \mathrel{\Gamma\cost{\mathcal{S}}} g(y)\), and thus, applying
%    Lemma~\ref{lmm:worsening} and the fact that \(c_1 \geqInf c_2\), we have that
%    \(\tick[c_1] (f(x)) \mathrel{\Gamma\cost{\mathcal{S}}} \tick[c_2] (g(y))\).
%  \end{itemize}
% \end{proof}
% \end{lemma}

% \begin{lemma}
%   The cost T-relator \(\cost{\Gamma}\) for \(\cost{T}\) is inductive.
%   \begin{proof}
%     Let \(X,Y\) be sets, and \(\RR \subseteq X \times Y\), we have to show that
%     \(\cost{\Gamma}\RR\) is an inductive relation.
%     %
%     By Definition~\ref{def:cost:rel} \(\cost{\Gamma}\RR\) is equivalent to
%     \(\Gamma\cost{\RR}\), and since \(\Gamma\) is an inductive relator,
%     \(\Gamma\cost{\RR}\) is an inductive relation.
%   \end{proof}
% \end{lemma}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% It is shown if \(T\) is \(\SigmaCont\) then \(T_C\) is also
% \(\Sigma_{\tick}\text{-continuous}\), and for any \(T\text{-relator}\) \(\Gamma\) we
% present a cost relator \(\Gamma_c\) able to perform intensional analysis,
% presenting a solid foundation to an \emph{Improvement Theory with effects}.


%%
% Writer's monad accepts a transformer
% monad which in this case provides a monad with counting capabilities.
%%
% Intuitively, such a monad first consumes effects generated by evaluating a term
% and with this extension we can also keep count cost.
%%

% Here we presented a concrete relator to handle the cost of evaluation,
% given a term \(M \in \T\) its evaluation would produce an effectful value with a
% certain observable cost, \( T (\V \times \Nat)\).
% \martin{Is this the right place to say this?}
% \begin{note}
% For this presentation \(\Nat\) is enough. However, in general we could
% be able to repeat this idea employing another mathematical entity, different
% from \(\Nat\), in order to perform a different intensional analysis.
% In particular (discrete) cost analysis requires at least a \emph{ring} structure
% where a \emph{tick} operation could be interpreted.
% \end{note}
%
%%%
%%%
%
We note our new system, \((\mathbb{W}_{T,\Nat} , \Gamma \circ \cost{\RR})\), simply as
\((\cost{T},\cost{\Gamma})\), and  prove that we can add \(\tick\) as an operation
so we have a \(\Sigma_{\tick}\)-system.

\begin{lemma}\label{thm:cost:continuous}
  The inner cost monad \(\cost{T}\) is \(\Sigma_{\tick}\)-continuous.
  \begin{proof}
    Recall that \(\Sigma_{\tick} \equiv \Sigma \cup \{\tick\}\), and thus, we
    split the proof in two:
    \begin{itemize}
      \item Monad \(\cost{T}\) is \(\Sigma\)-continuous.
      %
      It follows \(T\) being \(\Sigma\)-continuous.
      %       Since \(T\) is
      % \(\Sigma\)-continuous, for any set \(X\), and for each \(\sigma\) in
      % \(\Sigma\), there is a continuous operation \(\sigma^T :
      % X^{\alpha(\sigma)} -> X\).
      % %
      % In particular, for any set
      % \(X\), \(X \times \Nat\) is also a set, and thus, for each \(\sigma\) in
      % \(\Sigma\) there is a continuous operation \(\sigma^{\cost{T}} : {(X \times
      % \Nat)}^{\alpha(\sigma)} -> (X \times \Nat)\). Therefore, \(\cost{T}\) is
      % \(\SigmaCont\).
    \item The interpretation of the unary tick operation is continuous. 
      %
      The \(\tick\) operator is defined by lifting the function \(add_1\), and
      lifting functions is a continuous operation since monad operations are.
      Therefore, the \(\tick\) function is continuous.
    \end{itemize}
  \end{proof}
\end{lemma}

%% Continuity gives compatibility
% \begin{lemma}
%  The cost relator \(\cost{\Gamma}\) is compatible with \(\Sigma_{\tick}\).
%  \begin{proof}
%   Since the relator \(\Gamma\) is compatible with \(\Sigma\), we
%   only need to prove that the unary tick operation is compatible with
%   \(\cost{\Gamma}\).
%   %
%   Fortunately, this is easy since it is a variation of Lemma~\ref{lmm:maplifting}.
%  \end{proof}
% \end{lemma}

% By the Theorem~\ref{thm:cost:continuous} and previous lemmas, the
% pair

% Putting together the previous lemmas we have the following.
%
\begin{theorem}\label{thm:sigma_tick_system}
The pair \((\cost{T}, \cost{\Gamma})\) is a
  \(\Sigma_{\tick}\)-system.    
\end{theorem}

\subsection{Cost Aware Evaluation}\label{subsec:costeval}

We are ready to define effectful applicative \(\cost{\Sigma}\)-similarity and
observational approximation with cost for \(\lambda\)-terms.

As a guiding example, we present an instrumented evaluation that keeps
count of every step made while evaluating a term into a monadic value.
%
Different ways to keep count may lead to different notions of cost.
%
For example, we may be interested in just counting function
application~\citep{Sands:Op:Theories} or a particular computationally
heavy operation.

\begin{lemma}[Cost Evaluation]\label{lmm:cost:evaluation}
  \normalfont%
  \begin{align*}
    {[[ \LRet{V} ]]}_{\Nat} &= \tick \eta_{\Nat}(V) \\
    {[[ \LApp{(\lambda x . M)}{W} ]]}_{\Nat} &= \tick[] {[[ M[x:=W] ]]}_{\Nat} \\
    {[[ \LTo{M}{x.N} ]]}_{\Nat}  &= \tick[] {[[ M ]]}_{\Nat} {>>=}_{\Nat} (v |-> {[[ N[x:=v] ]]}_{\Nat}) \\
    {[[ \tick[] M ]]}_{\Nat} &= \tick[] [[ M ]]_{\Nat} \\
    {[[ \sigma(M_1 , \ldots, M_n) ]]}_{\Nat} &= \tick[] \sigma^{{T}_{\Nat}}({[[M_1]]}_{\Nat}, \ldots, {[[M_n]]}_{\Nat})
  \end{align*}
\end{lemma}

There are two differences between the evaluation function presented in
Section~\ref{sec:Calculus} and cost evaluation: 1) the evaluation of a ticked
expression is the tick of the evaluation of the expression, avoiding double counting ticks;
2) we add a tick for each step in the evaluation.
%
Moreover, since the evaluation of terms does not depend on the cost of the
computation, both evaluation functions evaluate terms to the same values.

Following the route described in Section~\ref{sec:app:bisim}, we derive two
notions: the definition of effectful applicative simulation and 
observational approximation.
%
The effectful applicative simulation derived from cost analysis
is called \emph{Improvement Simulation}, while in the case of observational
approximation is called \emph{Improvement}~\citep{Sands:Op:Theories}.
%

\subsection{Improvement Simulation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Summary: Improvement Simulation is just applicative simulation with different
%% cost relators.
% This section has a two-fold goal: make the reader remember the process
% presented in Section~\ref{sec:AppSim} and compare the result obtainedo
% with classical results.

Applicative similarity reassembles the idea of Abramsky that two terms are
similar if they are similar modulo evaluation. We defined it coinductively
as the union of all \(\lambda\)-term applicative simulation relations.
%
We revisit the definition of applicative similarity but using
the cost \((\cost{T}, \cost{\Gamma})\)-system and the cost evaluation instead of
the \((T, \Gamma)\)-system and the evaluation defined in
Section~\ref{sec:Calculus}.
%
We call \emph{improvement simulation} to the applicative simulation resulting
from the use of a cost-aware system.
%
Therefore, improvement simulation mixes two observations: Abramsky's
simulation for term comparison and cost comparison.
%% as added in the previous section.

\begin{definition}[Improvement Simulation]\label{def:app:cost}
  A closed \(\lambda\)-term relation \(\RR\)
  % \((\RR_{\T},\RR_{\V})\)
  is an improvement simulation if and only if it respects values and:
  \begin{displaymath}
    M,N \in \KT, \mkRel{M}{\RR_{\T}}{N} \implies \mkRel{{[[ M ]]}_{\Nat}}{\cost{\Gamma} \RR_\V}{{[[ N ]]}_{\Nat}}
  \end{displaymath}
\end{definition}

Let
% \((\RR_\T, \RR_\V)\)
\(\RR\) be an improvement simulation, \(M\) and
\(N\) be closed terms, such that \(\mkRel{M}{\RR_{\T}}{N}\).
%
Unfolding the previous definition we have that the evaluation of \(M\) and
\(N\) are related,
\(\mkRel{{[[ M ]]}_{\Nat}}{\cost{\Gamma} \RR_\V}{{[[ N ]]}_{\Nat}}\).
%
Moreover, by definition of the cost relator (Definition~\ref{def:cost:rel}), we have that
% the evaluation of \(M\) and \(N\)is the same as
\(\mkRel{{[[ M ]]}_{\Nat}}{\Gamma\cost{\RR_\V}}{{[[ N ]]}_{\Nat}}\), in other
words:
\begin{itemize}
\item \({[[ M ]]}_{\Nat}\) and \( {[[ N ]]}_{\Nat}\) have
  %% the same observable behavior
  related effects through \(\Gamma\) in \(\cost{T}\), and
  \item the cost of the evaluation \( {[[ M ]]}_{\Nat}\) is greater
or equal than the cost of the evaluation \( {[[ N ]]}_{\Nat}\).
\end{itemize}

\subsubsection{Partiality Monad}

The partiality monad presents the required mechanism to interpret a
\(\lambda\)-calculus without any observable algebraic effect aside from
non-termination.
%
Following the definition of improvement simulation presented above, and
unfolding the definition of the partiality relator \(\Gamma_\bot\), we have that a
closed \(\lambda\)-term relation \((\mathcal{S}_\T, \mathcal{S}_\V)\) is a
partial improvement simulation whenever respect values and for any two closed
terms \(M\), \(N\):
\begin{displaymath}
  \mkRel{M}{\mathcal{S}_{\T}}{N} \implies
  \begin{dcases}
    {[[M]]}_{\Nat} = in_{r}(\bot_{V \times \N}) \\
    {[[M]]}_{\Nat} = in_{l}(v , n) \implies & {[[N]]}_{\Nat} = in_{l}(w, m) \\
    & \mathrel{\land} (n \geqInf m) \\
    & \mathrel{\land} v \mathrel{\mathcal{S}_\V} w
  \end{dcases}
\end{displaymath}

The result of the improvement simulation for the partiality monad is equivalent
to the improvement simulation presented by~\citet{Sands:Op:Theories}, where the
only difference is that the latter uses a \emph{weak head normal form}
evaluation.

\subsection{Improvement}

Observational approximation relates terms based on how they behave in any given
context.
%
In other words, term \(A\) approximates term \(B\) if there is no context that
can tell the difference between \(A\) and \(B\).
%
In this section, we explore the observational approximation relation
exploiting the information made available by the cost evaluation and
the \((\cost{T}, \cost{\Gamma})\)-system.

We start from the definition of improvement, which is the resulting
observational approximation obtained from the use of the cost system.

\begin{definition}[Improvement]\label{def:Improvement}
  Let \(M\) and \(N\) be terms.
  We say that \(M\) is improved by \(N\) if and only if
  \(\mkRel{M}{\rctxImp[\cost{\Gamma}]}{N}\).
  \end{definition}

We note the improvement relation \((\rctxImp[\cost{\Gamma}])\) as
\((\gimprov{\Gamma})\), removing the cost sub-index.

\subsubsection{Partiality Monad}

In this subsection, we unfold the definitions of the partiality relator and the
definition of improvement.
%
As result of the application of Lemma~\ref{lmm:ctx:comppread}, the relation
\((\gimprov{\Gamma_{\bot}})\) is a compatible and pre-adequate preorder.
%
%% By compatibility,
For any closing context \(\CC\), we have that \( \emptyset |- \CC[M]
\gimprov{\Gamma_{\bot}} \CC[N]\), by compatibility, and thus, by pre-adequacy
\([[ \CC[M] ]] \mathrel{\cost{\Gamma_{\bot}} \UU} [[ \CC[N] ]]\).
%
Moreover, by definition of the cost relator (Definition~\ref{def:cost:rel}) and
unfolding the definition of \(\Gamma_\bot\), we have that
\(M \gimprov{\Gamma_\bot} N\) if and only if for any closing context \(\CC\)
either:
%
% \[
%   \begin{array}{l}
%     - \text{the evaluation of } {C [M]} \text{ diverges, } {\cost{[[ C[M] ]]} = in_{r}(\bot_{\V \times \Nat})} \\
%     - \text{it converges and it is improved by the evaluation of } {C [N]} \text{, }
%     \cost{[[ \CC[M] ]]} = in_{l}(v, c_1) \implies \cost{[[ C[N] ]]} = in_{l}(u,c_{2}) \land c_{1} \geqInf c_{2}\\
%     \end{array}
% \]
\begin{itemize}
\item the evaluation of \(C[M]\) diverges, \(\cost{[[ C[M] ]]} = \bot_{\V \times \Nat}\), or
  \item the evaluation of \(C[M]\) converges and it is improved by the evaluation of \(C[N]\),
        \(
        % \begin{matrix*}[l]
        % \forall v \in \KV, c_1 \in \Nat,
        \cost{[[ \CC[M] ]]}
        % &
        = in_{l}(v, c_1)
        \implies
        % \\
  % \exists u \in \KV, c_2 \in \Nat,
        \cost{[[ \CC[N] ]]}
        % &
        = in_{l}(u,c_2) \land c_1 \geqInf c_2
% \end{matrix*}
        \)
  \end{itemize}
%% \begin{displaymath}
%%   \begin{dcases}
%%     \cost{[[ C[M] ]]} = in_r(\bot_{\V \times \Nat}) \\
%%     \begin{matrix*}[l]
%%     \forall v \in \KV, c_1 \in \Nat, \cost{[[ \CC[M] ]]}
%%     = in_l((v, c_1)) \\
%%     \hspace{1em} \implies \exists u \in \KV, c_2 \in \Nat, \\
%%     \hspace{2em} \cost{[[ \CC[N] ]]} = in_l((u,c_2)) \land c_1 \geqInf c_2
%%     \end{matrix*}
%%   \end{dcases}
%% \end{displaymath}

Let \(M\) and \(N\) be terms, such that \(M \gimprov{\Gamma_\bot} N\), then for
any closing context \(\CC\), whenever the evaluation of \(\CC[M]\) finishes with
cost \(c_1\), the evaluation of \(\CC[N]\) also finishes with cost \(c_2\),
such that \(c_1\) is greater or equal than \(c_2\).
%
Therefore, the notion of improvement for the partiality system is the classic
definition of improvement for a lambda calculus without effects~\citep{Sands97improvementtheory}.

% \subsection{Improvement Simulations implies Improvement}

Finally, by Theorem~\ref{thm:sim:ctx}, improvement is implied by improvement similarity.
%
Therefore, in order to prove that a term \(M\) is improved by another term
\(N\), \(\mkRel{M}{\gimprov{\Gamma}}{N}\), it is enough to show that
\(M\) is similar to \(N\), \(\mkRel{M}{\rappSym[\cost{\Gamma}]}{N}\).
%
In other words, in order to prove improvements quantified for all contexts, it is enough
to prove that we can observe such improvement locally.

In this section, we presented a concise method to define improvement between
effectful terms and a sound rule to prove such improvements, fulfilling the
promises made in the introduction section.
%
The rest of the paper is dedicated to exploring the implications and utility of
this method.
%
In particular, in the following section, we derive an improvement theory
for the language presented in Section~\ref{sec:Calculus} and each of the
algebraic effects presented in Example~\ref{ex:wCPPO}.
