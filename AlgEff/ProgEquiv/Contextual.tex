We dedicate this section to present a Morris-contextual style precongruence
relation on effectful terms.
%
Contextual definitions, while being very expressive, are hard to work with since
they involve universal quantification over all contexts.
%
\citet{Howe:Cong:Bisim} presents a method to build precongruence
relations starting from the notion of \textit{compatibility clauses}.
%
Compatibility clauses present an inductively defined requirement for
an algebraic relation to be modular.
%
This method was recently updated to be treated in abstract form
in~\citep{Levy:Inf:Howe}.

\begin{definition}[Compatible Relator]
  Let \(\Sigma\) be a signature, \(T\) a monad and \(\Gamma\) a T-relator for \(T\).
  %
  We say that \(\Gamma\) is compatible with \(\Sigma\) if and only if
  for any relation \(\RR \subseteq X \times Y\), for each \(\sigma\) in \(\Sigma\) and
  \(n = \alpha(\sigma)\):
  \begin{displaymath}
  (\forall k \leq n, \mkRel{u_k}{\Gamma \RR}{v_k})
  \implies \mkRel{({\sigma}^{T(X)}(u_1, \ldots, u_n))}{\Gamma \RR}{({\sigma}^{T(Y)}(v_1, \ldots, v_n))}
\end{displaymath}
\end{definition}

The above definition states that a T-relator \(\Gamma\) respects the operators of
signature \(\Sigma\).
%
Fortunately for us, since we are working with continuous monads carrying
continuous \(\Sigma\)-algebra structure, for every function \(f \colon X \to T Y\),
\( f^{\dag} \colon T X \to T Y\) is continuous, and thus, \(\Gamma\) respects the operators of \(\Sigma\).

\begin{lemma}[Remark~6 \citep{Eff:App:Bisim}]
  For any signature \(\Sigma\), \((T, \Gamma)\) a \(\Sigma\)-system.
  %
  Relator \(\Gamma\) is compatible with \(\Sigma\).
  \end{lemma}

For the rest of the section, we assume there is a fixed signature \(\Sigma\) and
a \(\Sigma\)-system \((T, \Gamma)\).
% where \(\Gamma\) is compatible
% with \(\Sigma\).
% It is assumed there is a monad \(T\) carrying a continuous \(\SigmaAlg\)
% structure and a relator \(\Gamma\) which is inductive and respects \(\Sigma\).

% Our goal is then build a compatible pre-congruence relation
% , \emph{operator respecting} is
% a fundamental property enabling modular reasoning on terms.
% That is the
% ability to relate terms based on related sub-terms.

\begin{definition}[Open Relation]
  An open relation over terms is a set \(R_\T\) of triples \((\overline{x}, M ,
  N)\) where \(M, N \in \T(\overline{x})\). Similarly, an open relation over
  values is a set \(R_\V\) of triples \((\overline{x}, V , W)\) where \(V, W \in
  \V(\overline{x})\).
\end{definition}

We use a mixfix notation, noting as \( \overline{x} |- M \mathrel{\RR_\T} N\)
instead of \((\overline{x}, M,N) \in \RR_\T\) and \(\overline{x} |- V \mathrel{\RR_\V}
U\) instead of \((\overline{x},V,U) \in \RR_\V\).

\begin{definition}[Compatibility]
  Let \((\RR_\T, \RR_\V)\) be a \(\lambda\)-term relation.
  %
  We say \((\RR_\T, \RR_\V)\) is compatible if and only if for any
  given set of variables \(\overline{x}\) the following clauses hold:
%%%%%%%%%%%%%%%%%%%%
\begin{displaymath}
  \begin{array}{c}
  \inference[\texttt{Comp-Var}]{}
  {\overline{x} |- x \mathrel{\RR_\V} x}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \quad
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \inference[\texttt{Comp-Abs}]{\overline{x} \cup \{x\} |- M \mathrel{\RR_\T} N}%
  {\overline{x} |- (\lambda x. M) \mathrel{\RR_\V} (\lambda x. N)} \\
    \\ %spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \inference[\texttt{Comp-Ret}]{\overline{x} |- V \mathrel{\RR_\V} W}
  {\overline{x} |- \LRet{V} \mathrel{\RR_\T} \LRet{W} } \\
    \\ %spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \inference[\texttt{Comp-App}]{\overline{x} |- \mkRel{V}{\RR_\V}{V'} & \overline{x} |- \mkRel{W}{\RR_\V}{W'}}%
  {\overline{x} |- \mkRel{(\LApp{V}{W})}{\RR_\T}{(\LApp{V'}{W'})}} \\
    \\ %spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \inference[\texttt{Comp-To}]{\overline{x} |- \mkRel{M}{\RR_\T}{M'}
                     & \overline{x} \cup \{x\} |- \mkRel{N}{\RR_\T}{N'}}%
  {\overline{x} |- \mkRel{\LTo{M}{x.N}}{\RR_\T}{\LTo{M'}{x.N'}}} \\
    \\ %spacing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  \inference[\texttt{Comp-}\(\sigma\)]%
  {\overline{x} |- M_1 \mathrel{\RR_\T} N_1 & \ldots & \overline{x} |- M_k \mathrel{\RR_\T} N_k}%
  {\overline{x} |- \sigma(M_1, \ldots, M_k) \mathrel{\RR_\T} \sigma(N_1, \ldots, N_k)}\\
  \end{array}
\end{displaymath}
%%%%%%%%%%%%%%%%%%%%
The rule \texttt{Comp-}\(\sigma\) represents a set of rules, one for each \(\sigma\) in
\(\Sigma\) and \(k = \alpha(\sigma)\).
\end{definition}

The compatibility clauses explicitly exhibit how to \emph{relate} bigger terms
from related sub-terms, i.e. compatible \(\lambda\)-term relations are
modular relations.
%
A compatible relation means the same as being closed under the term constructors
of the language. That is, for any term context \(\CC\) and compatible
relation \(\RR\):
\begin{displaymath}
  \overline{x} |- \mkRel{M}{\RR}{N} \implies
  \overline{y} |- \mkRel{\CC[M]}{\RR}{\CC[N]}
\end{displaymath}
Note that the set of free variables changes from one triplet to the other since
contexts can capture and introduce free variables.

\begin{definition}[Precongruence]
  Let \((\RR_\T, \RR_\V)\) be a \(\lambda\)-term relation. We say \((\RR_\T,
\RR_\V)\) is a \emph{precongruence} if and only if \((\RR_\T, \RR_\V)\) is a \emph{compatible preorder}.
\end{definition}

\begin{definition}[Preadequate]\label{def:preadequate}
  A relation \(\RR\) is \textbf{preadequate} if and only if
  % We say that a relation \(\RR\) is \textbf{preadequate} if and only if
  \[ \mkRel{M}{\RR}{N} \implies [[M]] \Gamma \mathcal{U} [[N]] \]
  where \(M,N \in \KT\), and \(\mathcal{U} = \KV \times \KV\).
  \end{definition}
A relation is preadequate if related closed terms generate related observable
effects after evaluation without taking into account the resulting values of
their evaluation.
%
A \(\lambda\)-term relation \((\RR_\T, \RR_\V)\) is preadequate if and only if
\(\RR_\T\) is.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Panacea 
\begin{definition}\label{def:ctx:preor}
  Let \(\mathbb{CA}\) be the set of relations on terms that are both compatible
  and preadequate. Define \((\geq_\Gamma)\) to be the union of all compatible
  and preadequate relations.
\end{definition}

\begin{lemma}\label{lmm:ctx:comppread}
  The relation \((\geq_\Gamma)\) is a compatible and preadequate preorder.
\end{lemma}

%%
The relation \((\geq_\Gamma)\) is defined co-inductively in a similar way as
\(\Gamma\)-similarity, and thus, it has a co-inductive principle: since we
defined \((\geq_\Gamma)\) as the union of all compatible and preadequate
relations, to prove \(M \mathrel{\geq_\Gamma} N\) we only require to show that
there exists a compatible and preadequate relation.

\begin{example}[Contextual Preorder]
  \begin{description}
  \item[Partiality.]
    The partiality system defines the \(\bot\)-contextual preorder
    \((\geq_\bot)\).
    %
    Unfolding compatibility and preadequate definitions, we say that
\(\mkRel{M}{\rctxImp[\bot]}{N}\) if and only if for any closing context \(\CC\),
either \([[ \CC[M] ]]\) diverges, \([[ \CC[M] ]] = in_r(\bot_{\V})\), or
for any \(v \in \KV\), if \([[ \CC[M] ]] = in_l(v) \) then exists \(u \in \KV\)
such that \( [[ \CC[N] ]] = in_l(u)\).
% we have one of the following cases:
% \[
% \begin{array}{l}
%     - [[ \CC[M] ]] = in_r(\bot_{\V}) \\
%     - \forall v \in \KV, [[ \CC[M] ]] = in_l(v) \implies \exists u \in \KV, [[
%     \CC[N] ]] = in_l(u)
% \end{array}
% \]
% \[
%   \forall  \CC, \CC[M], \CC[N] \in \KT,
%   \begin{dcases}
%     [[ \CC[M] ]] = in_r(\bot_{\V}) \\
%     \forall v \in \KV, [[ \CC[M] ]] = in_l(v) \implies \exists u \in \KV, [[
%     \CC[N] ]] = in_l(u)
%   \end{dcases}
% \]
% for any term context \(\CC\).

The above observational definition is the classic notion of observational
approximation for the untyped \(\lambda\)-calculus~\citep{FellMatt}.
%
In other words, \( M \rctxImp[\bot] N\) if and only if for all closing
context \(\CC\), if  the evaluation of \(\CC[M]\) finishes, then so
does the evaluation of \(\CC[N]\).

\item[Exception.] The exception system defines that
\(\mkRel{M}{\rctxImp[E]}{N}\) if and only if for each closing context \(\CC\)
for \(M\) and \(N\), one of the following cases holds:
\[
\begin{array}{rl}
  \bullet & \forall v \in \KV, [[ \CC[M] ]] = in_l(v) \implies \exists u \in \KV, [[ \CC[N] ]] = in_l(u) \\
  \bullet & [[ \CC[M] ]] = in_r(e) \implies [[ \CC[N] ]] = in_r(e) \\
  \bullet & [[ \CC[M] ]] = \bot_{T_E(V)}\\
  \end{array}
\]
  % \[
  %   \forall \CC, \CC[M],\CC[N] \in \KT,
  %   \begin{dcases}
  %     \forall v \in \KV, [[ \CC[M] ]] = in_l(v) \implies \exists u \in \KV, [[ \CC[N] ]] = in_l(u) \\
  %     [[ \CC[M] ]] = in_r(e) \implies [[ \CC[N] ]] = in_r(e) \\
  %     [[ \CC[M] ]] = \bot_{T_E V} \\
  %   \end{dcases}
  %   \]
    % for any term context \(\CC\).

The reasoning for exceptions is similar to the partiality system.
%
Given \(M,N\) such that \(M \rctxImp[E] N\), when
% the result of the evaluation of \(\CC[M]\) is an exception \(e\),
the evaluation of \(\CC[M]\) raises an exception \(e\),
the evaluation of \(\CC[N]\) should also raise the same
exception \(e\).
\item[Non-Determinism.] The non-deterministic system defines that \(M \rctxImp[\PP] N\) if:
  \begin{displaymath}
   \forall \CC, \CC[M],\CC[N] \in \KT, [[ \CC[M] ]] \neq \emptyset \implies [[ \CC[N]]] \neq \emptyset
  \end{displaymath}

A term \(M\) is improved by a term \(N\), \( M \rctxImp[\PP] N\), if and only if
for all closing context \(\CC\), if the evaluation of \(\CC[M]\) finishes, then
so does the evaluation of \(\CC[N]\).

\item[Sub-Distribution.] The sub-distribution system defines that
\(M \rctxImp[\mathcal{D}] N\) if:
  \begin{displaymath}
   \forall \CC,\CC[M],\CC[N] \in \KT,
   [[ \CC[M] ]](\KV) \leq [[ \CC[N] ]](\KV)
  \end{displaymath}
  % for any term context \(\CC\).

A term \(M\) is improved by a term \(N\), \( M \rctxImp[\mathcal{D}] N \), if
and only if for all closing context \(\CC\), the probability of the evaluation
of \(\CC[M]\) to converge is less or equal than the probability of the
evaluation of \(\CC[N]\) to converge.
\end{description}
\end{example}

Finally, we present a theorem connecting applicative simulation and contextual
preorder. % connecting applicative with contextual reasoning.
%%
On the one hand, observational approximation defines when two terms have a
similar observable behaviour quantified over all possible contexts, and thus,
while such definition is intuitive (and general) in general is hard to work
with.
%%
On the other hand, applicative simulation focuses on the evaluation of terms
comparing their results, providing us with a useful and local way to compare
terms.
%%
The following theorem states that we can complete the missing contexts promoting
an applicative simulation into an observational approximation since locally both
terms behave similarly and that behaviour can be systematically propagated.

\begin{theorem}\label{thm:sim:ctx}
  The relation \(\GammaTxt{-similarity}\) is a pre-congruence and pre-adequate
by definition, and thus, \( (\rappSym) \subseteq (\geq_{\Gamma})\).
\end{theorem}
%
The proof is based on \emph{Howe's method}.
%
The method consists of building a new pre-congruence relation based
on \(\Gamma\)-similarity, and then, showing that is equivalent
to \(\Gamma\)-similarity~\citep{Howe:Cong:Bisim}.

The result of the above theorem is a sound method to prove observational
approximations from applicative simulations.
% \mauro{that observational approximation what?}
%
Let \(M,N\) be terms,
in order to prove that \(M \rctxImp N\), is enough to prove that \(M \rappSym
N\), and moreover, given the co-inductive nature of \((\rappSym)\), is enough to
show that there is a \(\Gamma\)-simulation \((\RR_\T,\RR_\V)\) such that \(M
\mathrel{\RR_\T} N\).
%
Henceforth, we pay attention to the definition of \(\Gamma\)-simulations instead
of the different \(\Gamma\)-similarity relations.
