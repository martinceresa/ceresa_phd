Applicative similarity comes from the idea that lambda values are interpreted as
functions in the lambda calculus, and therefore, they can be compared extensionally
as such.
%
Intuitively, two terms are applicative similar if they both evaluate to
applicative similar values and values are compared extensionally with
applicative similarity.
%
The intuitive definition has two problems: first, we have to be careful not to
fall into cyclic reasoning, since lambda-abstraction values contain terms; and
second, evaluation is monadic, and therefore, we need a way to compare monadic
values.
%
The first problem is solved through the Knaster-Tarski Theorem
(Theorem~\ref{thm:Knaster:Tarski}) and the fact that the set of all relations
ordered by inclusion is a complete lattice, while the second is solved through
the use of \emph{relators}.

\begin{definition}[Relator]\label{def:fun:relator}
  Let \(F\) be an endofunctor on \(\Set\), and \(X,Y\) be sets. A
  \emph{relator} \(\Gamma\) for \(F\) is a map that associates a relation \(R \subseteq
  X \times Y\) with a relation \(\Gamma R \subseteq F (X) \times F (Y)\) such that:
  \begin{align*}
    \ident_{FX} &\subseteq \Gamma (\ident_X)  \tag{Rel-1}\\
    \Gamma R \circ \Gamma S &\subseteq \Gamma (R \circ S) \tag{Rel-2} \\
    \Gamma ({(f \times g)}^{-1} R) &\subseteq {(F f \times F g)}^{-1}\Gamma R \tag{Rel-3} \\
    R \subseteq T &\implies \Gamma R \subseteq \Gamma T \tag{Rel-4}
  \end{align*}

  Where \(R,T \subseteq X \times Y, S \subseteq Y \times Z\), \(f : W \to X, g : Z
  \to Y\) and the inversion operation \({(f \times g)}^{-1} R = \{ (w,z) | f(w) R g(z) \}\).
  \end{definition}
%Inverse images
% We usually refer to relators simply as relators.
%
Relators are closed under composition and intersection, but they are not closed
under union.

\begin{example}[Relators]\label{ex:relators}
  We present a relator for each of the underlying functors of the monads
in Example~\ref{ex:monads}.
  Let \(X,Y\) be sets and \(\RR\) a relation between \(X\) and \(Y\).
  \begin{description}
  \item[Partiality Relator.]
    Define \(\Gamma_{\bot}\RR \subseteq X_\bot \times Y_\bot\) as:
    \[
\mkRel{x}{\Gamma_\bot\RR}{y} \iff
      \begin{dcases}
        x = in_l(u) \implies y = in_l(v) \mathrel{\land} \mkRel{u}{\RR}{v} \\
        x = in_r({\bot_X})
      \end{dcases}
    \]
  \item[Exception Relator.]
    Let \(E\) be a set of exception symbols.
    %
    Define the relator \(\Theta_{E} \RR \subseteq (X + E) \times (Y +
    E)\) as:
    \begin{displaymath}
      x \mathrel{\Theta_{E} \RR} y \iff
      \begin{dcases}
        x = in_l(u) \implies y = in_l(v) \land u \mathrel{\RR} v \\
        x = in_r(e) \implies y = in_r(e') \land e = e'
      \end{dcases}
    \end{displaymath}
    As result we can define \(\Gamma_E\) as the composition of \(\Theta_E\) with the partiality
    relator: \(\Gamma_E \RR = \Gamma_{\bot}(\Theta_e(\RR))\)
  %
  \item[Powerset Relator.]
    Define \(\Gamma_{\PP} \RR \subseteq \PP(X) \times \PP(Y)\) as:
    \[
      P \mathrel{\Gamma_{\PP} \RR} Q \iff \forall x \in P, \exists y \in Q,
      x \mathrel{\RR} y
    \]
  \item [Sub-Distribution Relator.]
    Define \(\Gamma_{\mathcal{D}} \RR \subseteq \mathcal{D} (X) \times
    \mathcal{D} (Y)\) as:
    \[
      \mu \mathrel{\Gamma_{\mathcal{D}} \RR} \nu \iff \forall \, U \subseteq X, \mu(U) \leq \nu(\RR(U))
    \]
    where \(\RR(U) = \{ y \in Y \mid x \mathrel{\RR} y\}\), and
    for any sub-distribution \(\delta\), and a set \(S\), \(\delta(S) = \sum_{s \in S}
    \delta(s)\).
  \end{description}

Each example presented above defines a relator.
%
The proof that the maps for partiality, exception, and power are relators is
easy and left to the reader.
%
Proving that the map sub-distribution is a relator is not trivial and requires
Strassen's Theorem~\citep{Strassen1965StrassenThm}.
%
The powerset monad accepts different relators.
%
We explore (and exploit) some of these alternative relators in
Section~\ref{sec:notions}.
\end{example}

Now that we have relators as lifting mechanisms, we focus our attention into the
definition of applicative simulation.

% \subsubsection{\(\Gamma\)-Simulation}\label{sec:Simulation:GSim}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %% Introduction to Simulations?
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Applicative simulation is defined as follows: Given a relation
% \((||) \subseteq (\T \times \V)\), a simulation is a pair of relations \(\RR =
% (\RR_{\T}, \RR_{\V})\), such that: for any two terms, \(P, Q \in \T\), they are
% related \( \mkRel{P}{\RR_{\T}}{Q}\) if after execute them the results are
% related:
% \[
%   \forall v_p \in \V, P || v_p \implies \exists v_q \in
%   \V Q || v_q \land \mkRel{v_p}{\RR_{\V}}{v_q}
%   \]

% In presence of effects evaluation is performed inside a monad \(T\), therefore
% relation between values \(\V\) should be lifted to values inside the monad
% \(T\), \(T \V\), and that is exactly what relators do.

% In general we can define a simulation through monads as:
\begin{definition}[\(\Gamma\)-Simulation]
  Let \(X,Y\) be sets, \(T\) be a monad, \(\Gamma\) be a relator for \(T\),
  and
  \(\gamma_X \colon X \to T(X), \gamma_Y \colon Y \to T(Y) \) be two functions injecting
elements into \(T\).
  \begin{itemize}
  \item A \(\Gamma\)-simulation is a relation \(\RR \subseteq X \times Y\) such that
    \[  \mkRel{x}{\RR}{y} \implies \mkRel{\gamma_x(x)}{\Gamma \RR}{\gamma_y(y)} \]
  \item \(\Gamma\text{-similarity}\) \((\Gsim{X,Y})\) is the largest \(\Gamma\)-simulation.
  \end{itemize}
\end{definition}

% In other words, a \(\Gamma\)-simulation is a relation \(\RR\) that can be
% lifted into monad \(T\) through \(\gamma_X\), \(\gamma_Y\) and \(\Gamma\).

% Relating this definition to our current setting, we can think of \(\gamma_X\)
% and \(\gamma_Y\) as evaluation functions of terms, and
% \(\Gamma\) as lifting the simulation on ground values \(\V\) into \(T(\V)\).
% In other words, a \(\Gamma\)-simulation is a relation that
% for each related pair of terms, their evaluation result in related values.

%% The relation \(\Gamma\)-similarity is characterized by the following functional.

%% \begin{definition}
%%   Let \(X,Y\) bet sets, \(T\) be a monad, \(\Gamma\) be a relator for \(T\),
%%   \(\gamma_X : X -> T(X)\) and \(\gamma_Y : Y -> T(Y)\) be \(T\)-coalgebras.

%%   Define the functional \(\mathcal{F}^{\Gamma}_{X \times Y} : \mathcal{P}(X
%%   \times Y) -> \mathcal{P}(X \times Y)\) as:
%%   \[
%%     \mathcal{F}^{\Gamma}_{X \times Y}(\RR) = {(\gamma_X \times \gamma_Y)}^{-1}
%%     \Gamma \RR
%%   \]
%% \end{definition}

%% \begin{lemma}
%%   Let \(X,Y\) be sets, \(T\) be a monad, \(\Gamma\) be a relator for
%%   \(T\), and \(\gamma_X : X -> T(X), \gamma_Y : Y -> T(Y) \) be \(T\)-coalgebras.
%%   \begin{itemize}
%%   \item The functional \(\mathcal{F}^{\Gamma}_{X \times Y}\) is monotonic, and by
%% the Knaster-Tarski theorem, \(\nu \mathcal{F}^{\Gamma}_{X \times Y}\) is the
%% greatest fixed point
%% of \(\mathcal{F}\).
%%   \item A relation \(\RR\) is a \(\Gamma\)-simulation if and only if \(\RR\) is a post
%% fixed-point of \(\mathcal{F}^{\Gamma}_{X \times Y}\). Therefore,
%% \(\Gamma\)-similarity coincides with \(\nu \mathcal{F}^{\Gamma}_{X \times Y}\)
%%   \end{itemize}
%% \begin{proof}
%%   First, \(\mathcal{F}^{\Gamma}_{X \times Y}\) monotonicity follows from \(\Gamma\)
%%   monotonicity (Definition~\ref{def:fun:relator}~(Rel-4)).
%%   Moreover, given that \(\mathcal{P}(X \times Y)\) is a complete lattice ordered by
%%   inclusion, and by Knaster-Tarski Theorem (Theorem~\ref{thm:Knaster:Tarski}),
%%   the greatest fixed-point of \(\mathcal{F}^{\Gamma}_{X \times Y}\)
%%   % , \(\nu \mathcal{F}^{\Gamma}_{X \times Y}\),
%%   exists. Second, it is simply the
%%   co-induction principle left by Knaster-Tarski, and together with the fact that
%%   \(\Gamma\)-similarity is a \(\Gamma\)-simulation, implies
%%   that \(\Gamma\)-similarity coincides with \(\nu \mathcal{F}^{\Gamma}_{X \times Y}\).
%%   \end{proof}
%% \end{lemma}

\begin{lemma}\label{lmm:similarity:preorder}
  Let \(X\) be a set, \(T\) be a monad, \(\Gamma\) be a relator for monad \(T\),
  and \(\gamma_X \colon X \to T(X)\) be a function injecting elements into \(T\).
  %
  The relation \((\Gsim{X,X})\) is a preorder.
\begin{proof}
Let \(X\) be a set, \(T\) be a monad, \(\Gamma\) be a relator for monad \(T\),
and \(\gamma_X \colon X \to T(X)\) be a function injecting elements into \(T\).
%
The relation \((\Gsim{X,X})\) is a preorder if it is reflexive and
transitive, we prove both exploiting the coinductive definition of \(\Gamma\)-similarity.
\begin{itemize}
\item First, we prove that \(\Gamma\)-similarity is reflexive by showing
that the identity relation is \(\Gamma\)-similar, and therefore, contained in
\(\Gamma\)-similarity. Let \(x\) be an element of \(X\), from \(x
\mathrel{\ident_{X}} x\) we get that \(\gamma_X(x) \mathrel{\ident_{T(X)}}
\gamma_X(x)\), and by (Rel-1) we conclude that \(\gamma_X(x) \mathrel{\Gamma
  \ident_{T(X)}} \gamma_X(x)\), therefore \(\ident_X\) is a
\(\Gamma\)-simulation.
\item Second, we show that \(\Gamma\)-similarity is transitive.
  Let \(x,y,z\) be elements of \(X\), such that \(x \mathrel{\Gsim{X,X}} y\) and
  \(y \mathrel{\Gsim{X,X}} z\). By definition of \(\Gamma\)-similarity, there
  exist two \(\Gamma\)-simulations, \(R\) and \(S\), such that \(x \mathrel{R}
  y\) and \(y \mathrel{S} z\), and by relation composition \(x \mathrel{(R \circ
    S)} z\). Since \(R\) and \(S\) are \(\Gamma\)-simulation, we have
  \(\gamma_X(x) \mathrel{\Gamma R} \gamma_X(y)\) and \(\gamma_X(y)
  \mathrel{\Gamma S} \gamma_X(z)\), and by relation composition \(\gamma_X(x)
  \mathrel{(\Gamma R \circ \Gamma S)} \gamma_X(z)\).
  %
  By (Rel-2) we have \(\gamma_X(x) \mathrel{\Gamma (R \circ S)} \gamma_X(z)\),
  showing that the composition of \(\Gamma\)-simulations is itself
  a \(\Gamma\)-simulation, and therefore, contained in \(\Gamma\)-similarity
  showing that \(x \Gsim{X,X} z\).
\end{itemize}
\end{proof}
\end{lemma}

We base the evaluation of terms on monads, and thus, relators need to properly
interact with the structure of monads.

\begin{definition}[T-Relator]\label{def:monad:relator}
  Let \(X,X',Y,Y'\) be sets, \(T\) be a monad, \(f : X -> T(X')\) and \(g : Y ->
  T(Y')\) be functions, \(R \subseteq X \times Y\), and \(S \subseteq
  X' \times Y'\).
  %
  We define a T-relator \(\Gamma\) for \(T\) if and only if
  \(\Gamma\) is a relator for \(T\) regarded as a functor, and:
  \begin{align*}
    \forall x \in X, y \in Y, \mkRel{x}{R}{y} & \implies \mkRel{\eta_X(x)}{\Gamma R}{\eta_Y(y)} \\
    \forall  u \in T(X), v \in T(Y), \mkRel{u}{\Gamma R}{v}
             & \implies \mkRel{(u >>= f)}{\Gamma S}{(v >>= g)}, \\
             & \text{whenever } \forall x \in X, y \in Y, \mkRel{x}{R}{y}
         \implies \mkRel{f(x)}{\Gamma S}{g(y)}
  \end{align*}
  \end{definition}

The evaluation of terms is performed through approximations presented as
\wchain s, and since applicative simulation involves term evaluation,
we need relators that properly interact with \wchain s.
% a relator \(\Gamma\) is required to respect bottoms and lubs:

\begin{definition}[Inductive Relator]
  Let \(T\) be a monad ordered by \((\sqsubseteq)\) and \(\Gamma\) be a
  T-relator for \(T\).
  %
  We say that \(\Gamma\) is an inductive relator if and only if for
any two sets \(X,Y\), and relation \(\RR \subseteq X \times Y\),
\(\Gamma \RR\) is an inductive relation for the domain \((T(X),
\sqsubseteq_X, \bot_X)\).
\end{definition}

\begin{definition}[\(\Sigma\)-System]
  Let \(\Sigma\) be a signature. We call a \(\Sigma\)-system to a
pair of a \(\Sigma\)-continuous monad \(T\), and an inductive
T-relator \(\Gamma\) for \(T\).
\end{definition}

\begin{example}
 Each of relator in Example~\ref{ex:relators} paired with their
respective monad and signature in Example~\ref{ex:monads} forms a
\(\Sigma\)-system.
\end{example}

Following \citet{Abramsky:1990:LLC:119830.119834} an Applicative
Transition System (ATS) for effectful simulations is
defined.

\begin{definition}[Applicative \(\Gamma\)-simulation~\citep{Eff:App:Bisim}]
  Let \\ \(\Sigma\) be a signatue,
  % Given a signature \(\Sigma\), a \(\Sigma\)-system \((T,\Gamma)\), two sets \(X,T\), and
  % functions \(\epsilon : X -> T(Y), (\cdot) : Y -> Y -> X\).
  % Let \(\Sigma\) be a signature,
  %
  \((T, \Gamma)\) be a \(\Sigma\)-system,
  \(X,Y\) be sets, and \(\epsilon : X -> T(Y)\), \((\cdot) : Y -> Y -> X\) be functions.
  % An applicative \(\Gamma\)-simulation is
  A pair of relations \(R_X \subseteq X \times X\) and \(R_Y \subseteq Y \times Y\) are a \(\Gamma\)-simulation if:
  % such that:
  \begin{align*}
    \mkRel{x}{R_X}{x'} &\implies \mkRel{\epsilon (x)}{\Gamma R_Y}{\epsilon(x')} \\
    \mkRel{y}{R_Y}{y'} &\implies \forall w \in Y, \mkRel{(y \cdot w)}{R_X}{(y' \cdot w)}
  \end{align*}

  Applicative \(\Gamma\)-simulation exploits the fact that closed values in
lambda calculus are function abstractions, and therefore, the natural way to
compare them is through extensional analysis.
%
In other words, since values are functions, they are similar if they are similar
when applied to the same argument.
  \end{definition}

The applicative \(\Gamma\)-simulation definition induces an operator
\(\BB_{\Gamma} \) on pairs of relations, \(\mathcal{P}(X \times X) \times \mathcal{P}(Y \times
Y)\).
%
Let \(\RR_X \subseteq X \times X\) and \(\RR_Y \subseteq Y \times Y\) be two
relations, we can define the operator \(\BB_{\Gamma}\) as follows:
% defined for a pair of relations \(\RR_X \subseteq X \times X\)
% and \(\RR_Y \subseteq Y \times Y\) as folows:
\begin{align*}
  \BB_{\Gamma}(\RR_X) &= \{ (x,x') ||| \mkRel{\epsilon(x)}{\Gamma \RR_X}{\epsilon(x')}\} \\
  \BB_{\Gamma}(\RR_Y) &= \{ (y,y') ||| \forall w \in Y, \mkRel{(y \cdot w)}{\RR_X}{(y' \cdot w)} \}
  \end{align*}

The operator \(\BB_{\Gamma}\) is monotonoic, since \(\Gamma\) is monotonic, and
thus,
% And since relators are monotone, so is \(\). Hence,
applicative \(\Gamma\)-similarity is well-defined as the greatest fixed point
\(\nu\BB_{\Gamma}\) of \(\BB_{\Gamma}\), by Knaster-Tarksi
Theorem~\ref{thm:Knaster:Tarski} and the fact that the powerset of relations
ordered by inclusion is a complete lattice.
%%%%%%%%%%%%%%%%%%%%
The result of the Knaster-Tarski theorem is a \(\Gamma\)-similarity
co-inductive principle: to prove that two elements are applicative
\(\Gamma\)-similar, it only requires to find \emph{an} applicative
\(\Gamma\)-simulation that includes them.

\begin{lemma}
  Let \(\Sigma\) be a signature and \((T, \Gamma)\) be a \(\Sigma\)-system.
  %
  Applicative \(\Gamma\)-similarity is a preorder.
  \begin{proof}
Following the proof made in Lemma~\ref{lmm:similarity:preorder},
applicative \(\Gamma\)-similarity is reflexive since the pair of the identity
relation is an applicative \(\Gamma\)-simulation, and transitive since
applicative \(\Gamma\)-simulations are closed under composition.
  \end{proof}
\end{lemma}

In particular, following the computational model presented in
Section~\ref{sec:computational:model}, we can define an ATS for closed
\(\lambda\)-terms.

\begin{definition}
  A closed \(\lambda\)-term relation \((\RR_\T, \RR_\V)\) respects values if for all
  values \(V,W\), \(V \mathrel{\RR_V} W\) implies \((\LApp{V}{U})
  \mathrel{\RR_\T} (\LApp{W}{U})\), for any closed value \(U\).
\end{definition}

\begin{definition}
  Let \(\Sigma\) be a signature and \((T, \Gamma)\) be a \(\Sigma\)-system.
  %
  Define the ATS of closed \(\lambda\)-terms as follows:
  \begin{itemize}
  \item The state space is given by the pair \((\KT, \KV)\),
  \item The evaluation function is \([[ \cdot ]] : \KT -> T(\KV)\),
  \item The application function \((\cdot) : \KV -> \KV -> \KT\) is defined as
    term application.
  \end{itemize}
\end{definition}

We can apply the general definition of applicative
\(\Gamma\)-simulation to the ATS of \(\lambda\)-terms.

\begin{definition}[Effectful Applicative \(\GammaTxt{-simulation}\) for \(\lambda\)-terms]\label{def:eff:app:sim}
  % Given a signature \(\Sigma\) and a \(\Sigma\)-system \((T,\Gamma)\).
  Let \\ \(\Sigma\) be a signature and \((T, \Gamma)\) a \(\Sigma\)-system.
  A closed pair of relations \((\RR_{\T},\RR_{\V})\) is an
  \emph{applicative \(\Gamma\)-simulation} if and only if:
  \begin{itemize}
  \item \( \forall M,N \in \KT, M \, \RR_{\T} \, N  \implies \mkRel{[[ M
      ]]}{\Gamma \RR_\V}{[[ N ]]}\)
  \item \(\RR_\V\) respects values.
  \end{itemize}
\end{definition}

Effectful Applicative \(\GammaTxt{-simulation}\) characterizes as similar terms
those which evaluate to similar effectful values and define similar values to
be those that behave as similar terms when they are applied to a third one.
% This
% characteristic is inherited from Abramsky.
% : since values are functions, given two
% similar functions we expect them to remain similar when applied to the same
% argument.
Effectful Applicative \(\Gamma\)-similarity is, therefore, derived for
effectful lambda calculus and noted as \((\rappSym)\).

Since we work with effectful applicative simulations throughout the paper, we
shorten the term to \emph{applicative simulation}.

\begin{example}[Applicative Simulation]
  We give an instance of applicative simulation to each relator presented in
  Example~\ref{ex:relators}.
  \begin{description}
    \item[Partiality] \((\RR_\T, \RR_\V)\) is an applicative \(\Gamma_\bot\)-simulation if and only if:
      \begin{displaymath}
      \mkRel{M}{\RR_\T}{N} \implies
                           \begin{dcases}
                             {[[ M ]]}_{\bot} = \bot_{\V} \\
                             {[[ M ]]}_{\bot} = v \implies {[[ N ]]}_{\bot}
                                               = u \land \mkRel{v}{\RR_\V}{u}
                             \end{dcases} \\
     \end{displaymath}
     and \(\RR_\V\) respects values.
    \item[Exceptions] \((\RR_\T, \RR_\V)\) is an applicative \(\Gamma_E\)-simulation if and only if:
      \begin{displaymath}
      \mkRel{M}{\RR_\T}{N} \implies
                           \begin{dcases}
                             {[[ M ]]}_{E} = in_r(\bot_{\V}) \\
                             {[[ M ]]}_{E} = in_l(in_r(e_1))
                             \implies {[[ N ]]}_{E} =
                             \left(
                               \begin{array}{l}
                                 in_l(in_r(e_2)) \\
                                 \mathrel{\land} e1 = e2
                              \end{array}
                               \right)
                             \\
                             {[[ M ]]}_{E} = in_l(in_l(v))
                             \implies {[[ N ]]}_{E} = in_l(in_l(u))
                             \land \mkRel{v}{\RR_\V}{u}
                             \end{dcases}
      \end{displaymath}
     and \(\RR_\V\) respects values.
      \item[Powerset] \((\RR_\T, \RR_V)\) is an applicative \(\Gamma_{\PP}\)-simulation if and only if:
        \begin{displaymath}
          M \mathrel{\RR_\T} N \implies \forall v \in [[ M ]], \exists w \in  [[ N ]], x \mathrel{\RR_\V} y
        \end{displaymath}
     and \(\RR_\V\) respects values.
      \item[Sub-Distribution] \((\RR_\T, \RR_\V)\) is an applicative \(\Gamma_{\mathcal{D}}\)-simulation if and only if:
        \begin{displaymath}
          \mkRel{M}{\RR_{\T}}{N} \implies \forall U \subseteq \KV, [[ M ]](U) \leq [[N]](\RR_{\V}(U))
        \end{displaymath}
     and \(\RR_\V\) respects values.
  \end{description}
  \end{example}

The applicative \(\Gamma\text{-similarity}\) relation defines a practical way to
compare the behavior of terms of the language presented in
Section~\ref{sec:Calculus}.
%
It follows the spirit of applicative simulation relations presented by Abramsky
while also adding effects and a straightforward mechanism to compare terms
through monadic evaluation.

% \subsection{Normal Form Simulation}

% In an extremely recent work~\citepauthor{Ugo:Norm:Bisim}~\citep{Ugo:Norm:Bisim}.
% They developed new method to get \emph{open simulations} through the same
% computational model proposed in~\citep{Eff:App:Bisim}.

% The main goal is to produced what they called \emph{effectful normal form
%   similarity}. Which is simpler than applicative similarity, but a sound rule
% anyway.

% The idea is that normal form similarity is easier than applicative similarity
% which is easier than contextual relations. As we have noted before, when proving
% contextual properties one has to prove \emph{for all contexts}, while in
% applicative similarity the reasoning is more local and concrete. The problem now
% is that applicative similarity applies to closed terms and whenever one wants to
% give proofs for open terms \emph{for all} quantification reappear. Normal form
% similarity solves this problem performing a sort of structural reasoning.

% The loss is that each simplification is strict. There are contextual proofs that
% cannot be made through applicative similarity, and there are applicative
% similarity relations that cannot be expressed as normal form similarity.

% Anyway this approach may be useful and Ugo Dal Lago also presents a method
% called \emph{up-to-context} where some boiler plate is incorporated into proofs.

% \begin{example}[Curry vs Turing]
%   \begin{align*}
%   Y &\triangleq \func{y}{\LApp{\Delta}{\Delta}}, &\Delta &\triangleq
%     \func{x}{\LApp{y}{(\func{z}{xxz}})} \\
%     Z &\triangleq \LApp{\Theta}{\Theta}, &\Theta &\triangleq \func{x}{\func{y}{\LApp{y}{(\func{z}{xxyz})}}}
%   \end{align*}
%   We can prove given that \(\RR = ( \{ (Y , Z) , (\Delta \Delta z , \Theta \Theta
%   y z) \} , \emptyset )\) is an enf-simulation.
%   \end{example}
