{-# LANGUAGE TypeOperators         #-}

module PatrickLang where

import Modular.Low.DTC

data Val e = Const Int | Pair e e
data Op e  = Mult e e | Fst e | Snd e

type Sig = Op + Val

instance Functor Val where
    fmap _ (Const x) = Const x
    fmap f (Pair x y) = Pair (f x) (f y)

instance Functor Op where
    fmap f (Mult x y) = Mult (f x) (f y)
    fmap f (Fst x) = Fst $ f x
    fmap f (Snd x) = Snd $ f x

instance Traversable Val where
    traverse _ (Const x) = pure $ Const x
    traverse f (Pair x y) = Pair <$> f x <*> f y

instance Traversable Op where
    traverse f (Mult x y) = Mult <$> f x <*> f y
    traverse f (Fst x) = Fst <$> f x
    traverse f (Snd x) = Snd <$> f x

instance Foldable Val where
    foldl _ a (Const _) = a
    foldl f a (Pair x y) = f (f a x) y
    foldr _ b (Const _) = b
    foldr f b (Pair x y) = f x (f y b)

instance Foldable Op where
    foldr f b (Mult x y) = f x (f y b)
    foldr f b (Fst x) = f x b
    foldr f b (Snd x) = f x b
