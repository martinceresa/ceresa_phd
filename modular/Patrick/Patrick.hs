{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE TypeOperators         #-}

module Patrick where

import qualified Control.Applicative as App
import           Data.Foldable
import           Data.Monoid
import           Data.Traversable

data Val e = Const Int | Pair e e
data Op e  = Mult e e | Fst e | Snd e

newtype KFE a b = KFE {unKFE :: a -> a}

newtype KFEDual a b = KFED {unKFED :: a -> a}

-- constant functor.
instance Functor (KFE a) where
    fmap _ (KFE f ) = KFE f

-- constant functor.
instance Functor (KFEDual a) where
    fmap _ (KFED f ) = KFED f

-- Monoid m => Applicative (KF m)
instance Applicative (KFE a) where
    pure _ = KFE id
    (KFE x) <*> (KFE y) = KFE $ x . y

-- Monoid m => Applicative (KF (Dual m))
instance Applicative (KFEDual a) where
    pure _ = KFED id
    (KFED x) <*> (KFED y) = KFED $ y . x

queryRKFE :: (Traversable f) => (Term f -> r) -> (r -> r -> r) -> Term f -> r
queryRKFE q c t = unKFE (traverse rec $ unTerm t) (q t)
    where
        rec x = KFE $ \r -> c (queryRKFE q c x) r

queryLKFE :: Traversable f => (Term f -> r) -> (r -> r -> r) -> Term f -> r
queryLKFE q c t = unKFED (traverse rec (unTerm t)) (q t)
    where
        rec x = KFED $ \y -> c y (queryLKFE q c x)

data (f :+: g) a = Inl (f a) | Inr (g a)

type Sig = Op :+: Val

data Term f = Term {unTerm :: (f (Term f))}

class sub :<: sup where
    inj :: sub a -> sup a
    proj :: sup a -> Maybe (sub a)

instance f :<: f where
    inj = id
    proj = Just

instance f :<: (f :+: g) where
    inj = Inl
    proj (Inl x) = Just x
    proj _ = Nothing

instance {-# OVERLAPPABLE #-} (f :<: h) => f :<: (g :+: h) where
  inj           = Inr . inj
  proj (Inr x)  = proj x
  proj _        = Nothing

inject :: (g :<: f) => g (Term f) -> Term f
inject = Term . inj

project :: (g :<: f) => Term f -> Maybe (g (Term f))
project = proj . unTerm

iMult :: (Op :<: f) => Term f -> Term f -> Term f
iMult x y = inject $ Mult x y

iConst  :: (Val :<: f ) => Int -> Term f
iConst i = inject $ Const i

iPair :: (Val :<: f) => Term f -> Term f -> Term f
iPair x y = inject $ Pair x y

iFst :: (Op :<: f) => Term f -> Term f
iFst x = inject $ Fst x

iSnd :: (Op :<: f) => Term f -> Term f
iSnd x = inject $ Snd x

type Alg f a = f a -> a

class Eval f v where
    evalAlg :: Alg f (Term v)

instance (Eval f v, Eval g v) => Eval (f :+: g) v where
    evalAlg (Inl x) = evalAlg x
    evalAlg (Inr x) = evalAlg x

cata :: Functor f => Alg f a -> Term f -> a
cata f =  f . fmap (cata f) . unTerm

eval :: (Functor f, Eval f v) => Term f -> Term v
eval = cata evalAlg

trans :: Functor f => (Term f -> Term f)
    -> (Term f -> Term f)
trans f = cata $ f . Term

instance Foldable Val where
    foldl _ a (Const _) = a
    foldl f a (Pair x y) = f (f a x) y
    foldr _ b (Const _) = b
    foldr f b (Pair x y) = f x (f y b)

instance Foldable Op where
    foldr f b (Mult x y) = f x (f y b)
    foldr f b (Fst x) = f x b
    foldr f b (Snd x) = f x b

instance (Foldable f, Foldable g) => Foldable (f :+: g) where
    foldr f b (Inl t) = foldr f b t
    foldr f b (Inr t) = foldr f b t

query :: Foldable f => (Term f -> r) -> (r -> r -> r) -> Term f -> r
query q c t = foldl (\ x y -> c x (query q c y)) (q t) (unTerm t)

query' :: Foldable f => (Term f -> r) -> (r -> r -> r) -> Term f -> r
query' q c t = appEndo
    (getDual $ foldMap (Dual . Endo . (flip $ \x y -> c x (query' q c y))) (unTerm t))
    (q t)

query'' :: Traversable f => (Term f -> r) -> (r -> r -> r) -> Term f -> r
query'' q c t = appEndo
    (getDual $ App.getConst $ traverse (App.Const . Dual . Endo . (flip $ \x y -> c x (query' q c y))) (unTerm t))
    (q t)

-- paso 1: usar foldr. Query' es foldr.
-- NB: lo borré, dsp lo comento de nuevo
-- paso 2: foldr -> foldMap
queryR' :: Foldable f => (Term f -> r) -> (r -> r -> r) -> Term f -> r
queryR' q c t = appEndo (foldMap (Endo . rec) (unTerm t)) (q t)
    where
        rec = (\ t r -> c (query' q c t) r)

-- paso 3: foldMap -> traverse
queryR'' :: (Traversable f) => (Term f -> r) -> (r -> r -> r) -> Term f -> r
queryR'' q c t = appEndo (App.getConst . traverse (App.Const . Endo . rec) $ unTerm t) $ q t
    where
        rec = (\ t r -> c (query' q c t) r)

kf :: (Traversable t) => (a -> Endo b) -> t a -> App.Const (Endo b) (t c)
kf f = traverse (App.Const . f)

-- foldlTraduc :: Foldable t => (b -> a -> b) -> b -> t a -> b
-- foldlTraduc f z t = appEndo (getDual $ foldMap (Dual . Endo . (flip f)) t) z

gsize :: Foldable f => Term f -> Int
gsize = query (const 1) (+)

gsize' :: Foldable f => Term f -> Int
gsize' = queryR' (const 1) (+)

test :: (Val :<: f, Op :<: f) => Term f
test = (iConst 1) `iMult` (iConst 2)

instance Functor Val where
    fmap f (Const x) = Const x
    fmap f (Pair x y) = Pair (f x) (f y)

instance Functor Op where
    fmap f (Mult x y) = Mult (f x) (f y)
    fmap f (Fst x) = Fst $ f x
    fmap f (Snd x) = Snd $ f x

instance (Functor f, Functor g) => Functor (f :+: g) where
    fmap f (Inl x) = Inl $ fmap f x
    fmap f (Inr x) = Inr $ fmap f x

instance Traversable Val where
    traverse f (Const x) = pure $ Const x
    traverse f (Pair x y) = Pair <$> f x <*> f y

instance Traversable Op where
    traverse f (Mult x y) = Mult <$> f x <*> f y
    traverse f (Fst x) = Fst <$> f x
    traverse f (Snd x) = Snd <$> f x

instance (Traversable f, Traversable g) => Traversable (f :+: g) where
    traverse f (Inl t) = Inl <$> traverse f t
    traverse f (Inr t) = Inr <$> traverse f t

gsize'' :: Traversable f => Term f -> Int
gsize'' = queryR'' (const 1) (+)
