{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE PatternSynonyms           #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}

module HPatrick where

import           Modular.High.DTC
import           Modular.High.Evaluation
import           Modular.High.FreeMonad
import           Modular.High.GenTerm
import           Modular.High.HFunctor

data Val e a where
    Const_ :: Int -> Val e Int
    Pair_ :: e a -> e b -> Val e (a, b)

data Op e a where
    Mult_ :: e Int -> e Int -> Op e Int
    Fst_ :: e (a , b) -> Op e a
    Snd_ :: e (a , b) -> Op e b

type Sig = Op + Val

-- Patterns
--
pattern Const i <- (proj -> Just (Const_ i))
    where Const i = inj $ Const_ i

pattern Pair l r <- (proj -> Just (Pair_ l r))
    where Pair l r = inj $ Pair_ l r

pattern Mult l r <- (proj -> Just (Mult_ l r))
    where Mult l r = inj $ Mult_ l r

pattern Fst i <- (proj -> Just (Fst_ i))
    where Fst i = inj $ Fst_ i

pattern Snd i <- (proj -> Just (Snd_ i))
    where Snd i = inj $ Snd_ i
-- HFunctor instances.
--
instance HFunctor Val where
    hfmap _ (Const_ x) = Const_ x
    hfmap f (Pair_ l r) = Pair_ (f l) (f r)

instance HFunctor Op where
    hfmap f (Mult_ x y) = Mult_ (f x) (f y)
    hfmap f (Fst_ x) = Fst_ $ f x
    hfmap f (Snd_ x) = Snd_ $ f x

-- HTraversable.
--
instance HTraversable Val where
    htraverse f (Const_ x) = pure $ Const_ x
    htraverse f (Pair_ x y) = Pair_ <$> f x <*> f y

instance HTraversable Op where
    htraverse f (Mult_ x y) = Mult_ <$> f x <*> f y
    htraverse f (Fst_ x) = Fst_ <$> f x
    htraverse f (Snd_ x) = Snd_ <$> f x
--
iConst :: (Val < dom) => Int -> Term dom Int
iConst = Op . Const

iMult :: (Op < dom) => Term dom Int -> Term dom Int -> Term dom Int
iMult n m = Op $ Mult n m

test :: (Val < dom, Op < dom) =>  Term dom Int
test = iMult (iConst 1) (iConst 2)

gsize :: HTraversable dom => Term dom x -> Int
gsize = queryLKFE (const 1) (+)

data A f = forall i. A (f i)

subs :: HTraversable dom => Term dom x -> [ A (Term dom) ]
subs = queryLKFE (\x -> [A x ]) (++)

-- La idea, más allá que subs este ahora con query...
querySubs :: HTraversable dom => (forall x. Term dom x -> r) -> (r -> r -> r) -> Term dom x -> r
querySubs f c x = foldr1 c $ fmap (\(A x) -> f x) $ subs x
