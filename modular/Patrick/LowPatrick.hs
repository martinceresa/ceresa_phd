{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE ViewPatterns          #-}
module LowPatrick where

-- import           Patrick             hiding (cata, query, test, trans)
import PatrickLang

import qualified Control.Applicative as App
import           Data.Foldable       hiding (fold)
import           Data.Monoid
import           Data.Traversable

import Modular.Low.FreeMonad
import Modular.Low.DTC
import Modular.Low.GenTerm
import Modular.Low.Fun

-- newtype Ignore f e a = Ig (f e)
--
-- type Val_ e a = Ignore Val e a
type Expr a = Free Sig a

jMult :: (Op < f) => Free f a -> Free f a -> Free f a
jMult x y = inject $ Mult x y

jConst :: (Val < f) => Int -> Free f a
jConst = inject . Const

iPair :: (Val < f) => Free f a -> Free f a -> Free f a
iPair x y = inject $ Pair x y

iFst :: (Op < f) => Free f a -> Free f a
iFst x = inject $ Fst x

iSnd :: (Op < f) => Free f a -> Free f a
iSnd x = inject $ Snd x

test :: (Val < f, Op < f) => Free f a
test = (jConst 1) `jMult` (jConst 2)

class FShow f where
    showf :: Show a => f a -> String

instance FShow Val where
    showf (Const i) = "( C " ++ show i ++ " )"
    showf (Pair l r) = "(" ++ show l ++ "," ++ show r ++ ")"

instance FShow Op where
    showf (Mult x y) = "(" ++ show x ++ " * " ++ show y ++ ")"
    showf (Fst p) = "Fst " ++ show p
    showf (Snd p) = "Snd" ++ show p

instance (FShow f, FShow g) => FShow (f + g) where
    showf (Inl fx) = showf fx
    showf (Inr gx) = showf gx

instance (Show a, FShow f) => Show (Free f a) where
    show (Return x) = show x
    show (Op t) = showf t

expsize :: Traversable f => Free f a -> Int
expsize = query (const 1) (+)

pattern CConst n <- (project -> Just (Const n))
    where CConst n = inject $ Const n

suma1 :: (Val < dom) => Free dom a -> Free dom a
suma1 (CConst n) = CConst $ n + 1
suma1 x = x
