{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE GADTs          #-}
{-# LANGUAGE KindSignatures #-}
module HAST where

import           Prefixes

data Types = HExp | HSubscript | HCArg | HEqu | HStmt
            | HMod | HArg | HSClass | HDec | HExt | HComp
            | HElem | HCompo | HClassType | HCDef


data MegaAst (l :: Types) where
  -- Class Definition

  ClassDefinition :: MegaAst HClassType -> MegaAst HCDef
-- data ClassType
  DerivedClass :: Ident -> Name -> [Ident] -> (StringComment, Maybe [MegaAst HArg]) -> MegaAst HClassType
  ExtendedClass :: Ident -> Maybe [MegaAst HArg] -> StringComment -> MegaAst HCompo -> MegaAst HClassType
  RegularClass :: Ident -> StringComment -> MegaAst HCompo -> Ident -> MegaAst HClassType
  ShortClass_ :: MegaAst HSClass -> StringComment -> Maybe [MegaAst HArg]  -> MegaAst HClassType

  --Composition
  Composition :: [MegaAst HElem] -> [MegaAst HElem] -> [MegaAst HElem] ->
    [[(MegaAst HEqu, StringComment, Maybe [MegaAst HArg])]] -> [[(MegaAst HStmt, StringComment, Maybe [MegaAst HArg])]]
              -> (Maybe (MegaAst HExt)) -> (Maybe [MegaAst HArg]) -> MegaAst HCompo
  -- Element
  Import :: (StringComment, Maybe [MegaAst HArg]) -> MegaAst HElem
  Extends :: Name -> (Maybe [MegaAst HArg]) -> (Maybe [MegaAst HArg]) -> MegaAst HElem
  ClassDef ::  MegaAst HCDef -> (Maybe (Name, Maybe [MegaAst HArg]) ,(StringComment, Maybe [MegaAst HArg])) -> MegaAst HElem
  ComponentEl ::  MegaAst HComp -> (Maybe (Name, Maybe [MegaAst HArg]),(StringComment, Maybe [MegaAst HArg])) -> MegaAst HElem

  -- Component
  Component :: TypeSpec -> (Maybe [MegaAst HSubscript])
    -> [(MegaAst HDec, Maybe (MegaAst HExp), (StringComment, Maybe [MegaAst HArg]))]
    -> MegaAst HComp
  -- External
  External ::
    Maybe LangSpec
    -> Maybe ((Maybe [(Ident, [MegaAst HSubscript])]), Ident, [MegaAst HExp])
    -> (Maybe [MegaAst HArg]) -> MegaAst HExt
-- Declaration
  Declaration :: Ident -> Maybe [MegaAst HSubscript] -> Maybe (MegaAst HMod) -> MegaAst HDec
  -- ShortClass'
  ShortClass' :: Name -> (Maybe [MegaAst HSubscript]) -> (Maybe [MegaAst HArg]) -> MegaAst HSClass
  EnumShortClass :: Ident -> (Maybe [(Ident, StringComment, Maybe [MegaAst HArg])]) -> MegaAst HSClass

  -- Argument
  ElementMod :: Name -> (Maybe (MegaAst HMod)) -> StringComment -> MegaAst HArg
  ElementRepClass ::
    (MegaAst HSClass , StringComment, Maybe [MegaAst HArg])
    -> Maybe (Name, Maybe [MegaAst HArg]) -> MegaAst HArg
  ElementRepComp ::  (MegaAst HDec, StringComment, Maybe [MegaAst HArg]) -> Maybe (Name, Maybe [MegaAst HArg]) -> MegaAst HArg
  ElementRedClass :: (MegaAst HSClass , StringComment, Maybe [MegaAst HArg]) -> MegaAst HArg
  ElementRedRep  :: MegaAst HArg -> MegaAst HArg

  -- Modification
  ModEqual :: MegaAst HExp -> MegaAst HMod
  ModAssign :: MegaAst HExp -> MegaAst HMod
  ModClass :: [MegaAst HArg] -> (Maybe (MegaAst HExp)) -> MegaAst HMod

  -- Statement
  MBreak :: MegaAst HStmt
  MReturn :: MegaAst HStmt
  -- ValStmt :: HStmt -> MegaAst HStmt
  ForSt :: [(Name, Maybe (MegaAst HExp))] -> [MegaAst HStmt] -> MegaAst HStmt
  WhenSt :: MegaAst HExp -> [MegaAst HStmt] -> [(MegaAst HExp, [MegaAst HStmt])] -> MegaAst HStmt
  Assign :: [(Ident, [MegaAst HSubscript])] -> MegaAst HExp -> MegaAst HStmt
  IfSt   :: MegaAst HExp -> [MegaAst HStmt] -> [(MegaAst HExp, [MegaAst HStmt])] -> [MegaAst HStmt] -> MegaAst HStmt
  CallSt :: [(Ident, [MegaAst HSubscript])] -> [MegaAst HCArg] -> MegaAst HStmt
  OutputSt :: [Maybe (MegaAst HExp)] -> [(Ident, [MegaAst HSubscript])] -> [MegaAst HCArg] -> MegaAst HStmt
  -- Equation
  Equality :: MegaAst HExp -> MegaAst HExp -> MegaAst HEqu
  IfEq :: MegaAst HExp -> [MegaAst HEqu] -> [(MegaAst HExp, [MegaAst HEqu])] -> [MegaAst HEqu] -> MegaAst HEqu
  ForEq :: [(Name, Maybe (MegaAst HExp))] -> [MegaAst HEqu] -> MegaAst HEqu
  WhenEq :: MegaAst HExp -> [MegaAst HEqu] -> [(MegaAst HExp, [MegaAst HEqu])] -> MegaAst HEqu
  CallEq :: Name -> [MegaAst HCArg] -> MegaAst HEqu
  Connect :: [(Ident, [MegaAst HSubscript])] -> [(Ident, [MegaAst HSubscript])] -> MegaAst HEqu

-- Expression
  BExp :: Bool -> MegaAst HExp
  NumExp :: Either Int Double -> MegaAst HExp
  StringExp :: String -> MegaAst HExp
  EndExp :: MegaAst HExp
  -- Recursive
  RefExp :: [(Ident, [MegaAst HSubscript])] -> MegaAst HExp -- Arg cannot be empty?
  CallExp :: Name -> [MegaAst HCArg] -> Maybe [(Name, Maybe (MegaAst HExp))] -> MegaAst HExp
  BinOpExp :: MegaAst HExp -> BOp -> MegaAst HExp -> MegaAst HExp
  UnOpExp :: UOp -> MegaAst HExp -> MegaAst HExp
  RangExp :: MegaAst HExp -> Maybe (MegaAst HExp) -> MegaAst HExp -> MegaAst HExp
  IfExp :: MegaAst HExp -> MegaAst HExp -> [(MegaAst HExp, MegaAst HExp)] -> MegaAst HExp -> MegaAst HExp
  ArrayExp :: [MegaAst HCArg] -> MegaAst HExp
  MatrixExp :: [[MegaAst HExp]] -> MegaAst HExp
  OutExp :: [Maybe (MegaAst HExp)] -> MegaAst HExp

--Call Arguemtn
  ExpArgument :: MegaAst HExp -> MegaAst HCArg
  -- Recursive
  FunArgument :: Name -> [MegaAst HCArg] -> MegaAst HCArg
  NamedArgument :: Ident -> MegaAst HCArg -> MegaAst HCArg
-- MegaAst HSubscript
  All :: MegaAst HSubscript
  ExpSub :: MegaAst HExp -> MegaAst HSubscript
