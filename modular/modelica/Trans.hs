{-# LANGUAGE DataKinds      #-}
{-# LANGUAGE GADTs          #-}
module Trans where

import Prefixes
import AST as A
import HAST as H

htoa_subs :: MegaAst HSubscript -> Subscript
htoa_subs H.All = A.All
htoa_subs (H.ExpSub x) = A.ExpSub (htoa_exp x)

htoa_callArg :: MegaAst HCArg -> CallArgument
htoa_callArg (H.ExpArgument e) = A.ExpArgument (htoa_exp e)
htoa_callArg (H.FunArgument nm cas) = A.FunArgument nm (map htoa_callArg cas)
htoa_callArg (H.NamedArgument i ca) = A.NamedArgument i (htoa_callArg ca)


htoa_exp :: MegaAst HExp -> Expression
htoa_exp (BExp b) = Boolean b
htoa_exp (NumExp e) = Number e
htoa_exp (StringExp s) = String s
htoa_exp EndExp = End
htoa_exp (RefExp r) = Reference (map (\(i,ss) -> (i , map htoa_subs ss)) r)
htoa_exp (CallExp nm cs mb) = Call
                                  nm
                                  (map htoa_callArg cs)
                                  (map (\(nm, mbe) -> (nm, htoa_exp <$> mbe)) <$> mb)
htoa_exp (BinOpExp e1 bop e2) = BinOp (htoa_exp e1) bop (htoa_exp e2)
htoa_exp (UnOpExp u e) = UnOp u (htoa_exp e)
htoa_exp (RangExp e1 me e2) = Range (htoa_exp e1) (htoa_exp <$> me) (htoa_exp e2)
htoa_exp (H.IfExp e1 e2 els e3) = A.IfExp (htoa_exp e1) (htoa_exp e2) (map (\(l,r) -> (htoa_exp l, htoa_exp r)) els) (htoa_exp e3)
htoa_exp (ArrayExp cas) = ArrayCons $ map htoa_callArg cas
htoa_exp (MatrixExp ess) = MatrixCons $ map (map htoa_exp) ess
htoa_exp (H.OutExp mes) = A.OutExp $ map (htoa_exp <$>) mes

htoa_equ :: MegaAst HEqu -> Equation
htoa_equ (H.Equality e1 e2) = A.Equality (htoa_exp e1) (htoa_exp e2)
htoa_equ (H.IfEq e eqs1 els eqs2) = A.IfEq (htoa_exp e)
                                    (map htoa_equ eqs1)
                                    (twoApp htoa_exp htoa_equ els)
                                    (map htoa_equ eqs2)
htoa_equ (H.ForEq is eqs) = A.ForEq (map_Snd htoa_exp is) (map htoa_equ eqs)
htoa_equ (H.WhenEq e eqs els) = A.WhenEq
                                (htoa_exp e)
                                (map htoa_equ eqs)
                                (twoApp htoa_exp htoa_equ els)
htoa_equ (H.CallEq nm cas) = A.CallEq nm $ map htoa_callArg cas
htoa_equ (H.Connect bds1 bds2) = A.Connect
                                 (map_Snd htoa_subs bds1)
                                 (map_Snd htoa_subs bds2)

dualApp :: (Functor g, Functor f, Functor h) =>
  (a -> b) -> (c -> d) -> h (g a, f c) -> h (g b, f d)
dualApp f g = fmap (\(x, y) -> (fmap f x, fmap g y))

twoApp ::(Functor f, Functor h) => (a -> b) -> (c -> d) -> h (a, f c) -> h (b, f d)
twoApp s f = fmap (\(x, y) -> (s x, fmap f y))

map_Snd :: (Functor f, Functor h) => (b -> c) -> h (a, f b) -> h (a, f c)
map_Snd f = twoApp id f

htoa_Stmt :: MegaAst HStmt -> Statement
htoa_Stmt MBreak = Break
htoa_Stmt MReturn = Return
htoa_Stmt (H.ForSt is ss) = A.ForSt (map_Snd htoa_exp is) (map htoa_Stmt ss)
htoa_Stmt (H.WhenSt e ss els) = A.WhenSt
                                (htoa_exp e)
                                (map htoa_Stmt ss)
                                (twoApp htoa_exp htoa_Stmt els)
htoa_Stmt (H.Assign r e) = A.Assign (map_Snd htoa_subs r) $ htoa_exp e
htoa_Stmt (H.IfSt e sts1 els sts2) = A.IfSt
                                     (htoa_exp e)
                                     (map htoa_Stmt sts1)
                                     (twoApp htoa_exp htoa_Stmt els)
                                     (map htoa_Stmt sts2)
htoa_Stmt (H.CallSt r cas) = A.CallSt (map_Snd htoa_subs r) $ map htoa_callArg cas
htoa_Stmt (H.OutputSt mes ihs cas) = A.OutputSt
                                     (map (htoa_exp <$>) mes)
                                     (map_Snd htoa_subs ihs)
                                     (map htoa_callArg cas)

htoa_mod :: MegaAst HMod -> Modification
htoa_mod (H.ModEqual exp) = A.ModEqual $ htoa_exp exp
htoa_mod (H.ModAssign e)  = A.ModAssign $ htoa_exp e
htoa_mod (H.ModClass cm me) = A.ModClass (map htoa_arg cm) (htoa_exp <$> me)

htoa_arg :: MegaAst HArg -> Argument
htoa_arg (H.ElementMod nm mmod strcmt) = A.ElementMod Each Final  nm (htoa_mod <$> mmod) strcmt
htoa_arg (H.ElementRepClass (hclass, strcmt, margs) mopt) =
  A.ElementRepClass
  (htoa_hsclass hclass, (strcmt, (map htoa_arg) <$> margs))
  (map_Snd (map htoa_arg) mopt)
htoa_arg (H.ElementRepComp (hdec, strcmt, margs) mopt) =
  A.ElementRepComp
  ( typePrefix ,(htoa_hdec hdec, (strcmt, (map htoa_arg) <$> margs)))
  (map_Snd (map htoa_arg) mopt)
  where typePrefix = TypePrefix NoVarKind NoVariability NoDirection
htoa_arg (H.ElementRedClass (hsclass, strcmt, margs)) =
  A.ElementRedClass NoEach NoFinal (htoa_hsclass hsclass, (strcmt, map htoa_arg <$> margs))
htoa_arg (H.ElementRedRep harg) = A.ElementRedRep NoEach NoFinal $ htoa_arg harg

tPrefix = TypePrefix NoVarKind NoVariability NoDirection

htoa_hsclass :: MegaAst HSClass -> ShortClass'
htoa_hsclass (H.ShortClass' nm msubs margs) =
  A.ShortClass' tPrefix nm (map htoa_subs <$> msubs) (map htoa_arg <$> margs)
htoa_hsclass (H.EnumShortClass id ms) = A.EnumShortClass id
  (EnumList . (map (\(i,stcmt, margs) -> (i, (stcmt, map htoa_arg <$> margs)) )) <$> ms)

htoa_hdec :: MegaAst HDec -> Declaration
htoa_hdec (H.Declaration i msubs mmod) = A.Declaration i (map htoa_subs <$> msubs) (htoa_mod <$> mmod)

htoa_external :: MegaAst HExt -> External
htoa_external (H.External mlangspec mas margs) =
  A.External mlangspec
  ((\(ref, i, es) -> (map_Snd htoa_subs <$> ref,i,map htoa_exp es)) <$> mas)
  (map htoa_arg <$> margs)

htoa_component :: MegaAst HComp -> Component
htoa_component (H.Component tspec msubs ls) =
  A.Component tPrefix tspec (map htoa_subs <$> msubs)
  (map (\(dec, me, (strcmt, margs)) -> (htoa_hdec dec, htoa_exp <$> me, (strcmt, map htoa_arg <$> margs))) ls)

fixImp' :: Import'
fixImp' = ImportIdent (Ident "cerebro") (Name "c")

htoa_elem :: MegaAst HElem -> Element
htoa_elem (H.Import (strcmp, margs)) = A.Import (fixImp',(strcmp, map htoa_arg <$> margs))
htoa_elem (H.Extends nm margs1 margs2) = A.Extends nm (map htoa_arg <$> margs1) (map htoa_arg <$> margs2)
htoa_elem (H.ClassDef hdef (mnargs, (strcmt, margs))) = A.ClassDef NoReplaceable (htoa_def hdef)
  (map_Snd (map htoa_arg) mnargs, (strcmt, map htoa_arg <$> margs))
htoa_elem (H.ComponentEl hcomp (mnargs, (strcmt, margs))) =
  A.ComponentEl
  NoReplaceable
  (htoa_component hcomp)
  (map_Snd (map htoa_arg) mnargs, (strcmt, map htoa_arg <$> margs))

htoa_comp :: MegaAst HCompo -> Composition
htoa_comp (H.Composition elems1 elems2 elems3 eqsecs algs mext margs) =
  A.Composition
  (map htoa_elem elems1) (map htoa_elem elems2) (map htoa_elem elems3)
  (map ((\x -> (NoInitial, x))
        .
        map (\(eq, strcmt, margs)-> (htoa_equ eq, (strcmt, map htoa_arg <$> margs)))
       )
    eqsecs)
  (map ((\x -> (NoInitial,x)) . map (\(stmt, strcmt, margs) -> (htoa_Stmt stmt, (strcmt, map htoa_arg <$> margs)) )) algs)
  (htoa_external <$> mext) (map htoa_arg <$> margs)

htoa_ctype :: MegaAst HClassType -> ClassType
htoa_ctype (H.DerivedClass ident nm ids (strcmt, margs)) =
  A.DerivedClass ident nm ids (strcmt, map htoa_arg <$> margs)
htoa_ctype (H.ExtendedClass id margs strcmt hcom) =
  A.ExtendedClass id (map htoa_arg <$> margs) strcmt (htoa_comp hcom)
htoa_ctype (H.RegularClass id1 strcmt hcom id2) =
  A.RegularClass id1 strcmt (htoa_comp hcom) id2
htoa_ctype (H.ShortClass_ hsclass strcmt margs) =
  A.ShortClass_ (htoa_hsclass hsclass, (strcmt, map htoa_arg <$> margs))

htoa_def :: MegaAst HCDef -> ClassDefinition
htoa_def (H.ClassDefinition hclass) =
  A.ClassDefinition NoEncapsulated (NoPartial, Class) (htoa_ctype hclass)
