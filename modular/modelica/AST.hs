module AST where
import           Prefixes

data Within
    = NoWithin
    | WithinRoot
    | Within Name
      deriving (Show,Eq)

data ClassDefinition = ClassDefinition {
    encapsulated   :: Encapsulated,
    class_prefixes :: (Partial, ClassPrefix),
    class_         :: ClassType
} deriving (Show, Eq)

data ClassType
    = DerivedClass Ident Name [Ident] Comment
    | ExtendedClass Ident OptClassModification StringComment Composition
    | RegularClass Ident StringComment Composition Ident
    | ShortClass_ ShortClass
      deriving (Show, Eq)

data Composition = Composition {
    elems           :: [Element],
    public_elems    :: [Element],
    protected_elems :: [Element],
    equation_secs   :: [Section Equation],
    algorithm_secs  :: [Section Statement],
    optExternal     :: (Maybe External),
    optAnnotation   :: OptAnnotation
} deriving (Show, Eq)

data Element
    = Import  (Import', Comment)
    | Extends Name OptClassModification OptAnnotation
    | ClassDef  Replaceable ClassDefinition (OptConstraining, Comment)
    | ComponentEl Replaceable Component (OptConstraining, Comment)
      deriving (Show, Eq)

data Component = Component {
    typePrefix   :: TypePrefix,
    typeSpec     :: TypeSpec,
    optSubsType  :: OptSubscripts,
    declarations :: [(Declaration, OptExpression, Comment)]
} deriving (Show, Eq)

data External = External {
    langSpec        :: Maybe LangSpec,
    optExternalCall :: Maybe ((Maybe Ref), Ident, [Expression]),
    optExternalAnot :: OptAnnotation
} deriving (Show, Eq)

data Import'
    = ImportIdent Ident Name
    | ImportList  Name (Either [Ident] String)
      deriving (Show, Eq)

data ShortClass'
    = ShortClass' TypePrefix Name OptSubscripts OptClassModification
    | EnumShortClass Ident (Maybe EnumList)
      deriving (Show, Eq)

data TypePrefix = TypePrefix {
    varKind     :: VarKind,
    variability :: Variability,
    direction   :: Direction
} deriving (Show, Eq)

data Declaration = Declaration {
    name            :: Ident,
    optSubs         :: OptSubscripts,
    optModification :: Maybe Modification
} deriving (Show, Eq)

data Argument
    = ElementMod Each Final Name (Maybe Modification) StringComment
    | ElementRepClass ShortClass OptConstraining
    | ElementRepComp Component' OptConstraining
    | ElementRedClass Each Final ShortClass
    | ElementRedRep Each Final Argument
      deriving (Show, Eq)

data Modification
    = ModEqual  Expression
    | ModAssign Expression
    | ModClass  ClassModification OptExpression
      deriving (Show, Eq)

-- Equation and Statement data types --
data Statement
    = Break
    | Return
    | ForSt Indices Statements
    | WhenSt Expression Statements (Else Statements)
    | While Expression Statements
    | Assign Ref Expression
    | IfSt Expression Statements (Else Statements) Statements
    | CallSt Ref CallArguments
    | OutputSt OutExpression Ref CallArguments
      deriving (Show, Eq)

data Equation
    = Equality Expression Expression
    | IfEq Expression Equations (Else Equations) Equations
    | ForEq Indices Equations
    | WhenEq Expression Equations (Else Equations)
    | CallEq Name CallArguments
    | Connect Ref Ref
      deriving (Show, Eq)

-- Expressions data types --
data Expression
    = Boolean Bool
    | Number (Either Int Double)
    | String String
    | Reference Ref
    | Call Name CallArguments (Maybe Indices)
    | BinOp Expression BOp Expression
    | UnOp UOp Expression
    | Range Expression OptExpression Expression
    | IfExp Expression Expression (Else Expression) Expression
    | ArrayCons CallArguments
    | MatrixCons [[Expression]]
    | OutExp OutExpression
    | End
      deriving (Show, Eq)

data CallArgument
    = ExpArgument Expression
    | FunArgument Name CallArguments
    | NamedArgument Ident CallArgument
      deriving (Show, Eq)

data Subscript
    = All
    | ExpSub Expression
      deriving (Show, Eq)

-- newtype Ident             = Ident String                  deriving (Show, Eq, Ord)
-- newtype Name              = Name String                   deriving (Show, Eq, Ord)
-- newtype StringComment     = Stringcomment (Maybe String)  deriving (Show, Eq)
-- newtype LangSpec          = LangSpec String               deriving (Show, Eq)
newtype EnumList          = EnumList [(Ident, Comment)]   deriving (Show, Eq)
-- newtype TypeSpec          = TypeSpec Name                 deriving (Show, Eq, Ord)

type Indices              = [Index]
type ClassModification    = [Argument]
type CallArguments        = [CallArgument]
type Statements           = [Statement]
type Equations            = [Equation]
type OutExpression        = [OptExpression]

type Comment              = (StringComment, OptAnnotation)
type Constraining         = (Name, OptClassModification)
type ShortClass           = (ShortClass', Comment)
type Component'           = (TypePrefix, Declaration')
type Annotation           = ClassModification
type Ref                  = [(Ident, [Subscript])] -- Non empty list!
type OptSubscripts        = Maybe [Subscript]
type Declaration'         = (Declaration, Comment)
type StoredDefinition     = (Within, [(Final, ClassDefinition)])
type Index                = (Name, OptExpression)

type Section a            = (Initial, [(a, Comment)])
type Else a               = [(Expression, a)]

type OptConstraining      = Maybe Constraining
type OptExpression        = Maybe Expression
type OptClassModification = Maybe ClassModification
type OptAnnotation        = Maybe Annotation
