\documentclass{article}
%\usepackage{}

\usepackage[numbers]{natbib}
\usepackage{placeins}
\usepackage{afterpage}

\usepackage{color}
\usepackage{url}
\usepackage{array}
\usepackage{amssymb}
\usepackage{natbib}
\usepackage[unicode=true, draft=false]{hyperref}
\usepackage{verbatim}

\begin{document}

\title{AST High as F***}
%   \authorinfo{Mart\'in Ceresa}
%              {CIFASIS-CONICET, Argentina}
%              {ceresa@@cifasis-conicet.gov.ar}
%   
%   \authorinfo{Mauro Jaskelioff}
%              {CIFASIS-CONICET, Argentina}
%              {jaskelioff@@cifasis-conicet.gov.ar}

%\newcommand{\cifasis}{\ensuremath{\textrm \textsection}}
\maketitle

\input{abstract}

% \keywords

%   %include introduction.tex
%   
%   %include overview.tex
%   
%   %include related.tex
%   
%   %include conclusions.tex

\bibliographystyle{abbrvnat}
\bibliography{bib/bib}
\end{document}
