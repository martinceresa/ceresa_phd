{-# LANGUAGE GADTs           #-}
{-# LANGUAGE KindSignatures  #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE PolyKinds       #-}
{-# LANGUAGE TypeOperators   #-}

module Syntactic where

import           DTC
import           FreeMonad
import           HFunctor

-- Axelsson's

data AST dom sig where
    Sym :: dom sig -> AST dom sig
    (:$) :: AST dom (a |-> sig)
            -> AST dom (Full a)
            -> AST dom sig

infixl 1 :$

newtype Full a = Full {result :: a}
newtype a |-> b = Partial (a -> b)

infixr |->

data NUM' e a where
    Num_ ::  Int -> NUM' e (Full Int)
    Add_ :: NUM' e (Int |-> Int |-> Full Int)
    Mul_ :: NUM' e (Int |-> Int |-> Full Int)

type NUM = NUM' VoidF

type Expr a = AST NUM (Full a)

num :: Int -> Expr Int
num = Sym . Num_

add :: Expr Int -> Expr Int -> Expr Int
add m n = Sym Add_ :$ m :$ n

mul :: Expr Int -> Expr Int -> Expr Int
mul m n = Sym Mul_ :$ m :$ n

-- Ours approach.
data Exp e sig where
    Exp :: e (a |-> sig) -> e (Full a) -> Exp e sig

type HAst dom sig = HFree Exp dom sig

type HExprB a = HAst NUM a
type HExpr a = HExprB (Full a)

($$) :: HExprB (a |-> b) -> HExprB (Full a) -> HExprB b
f $$ x = Op $ Exp f x

num' :: Int -> HExpr Int
num' n = Return $ Num_ n

add' :: HExpr Int -> HExpr Int -> HExpr Int
add' n m = Return Add_ $$ n $$ m

mul' :: HExpr Int -> HExpr Int -> HExpr Int
mul' n m = Return Mul_ $$ n $$ m

pattern Num n = Return (Num_ n)
pattern Add n m = Op (Exp (Op (Exp (Return Add_) n)) m)
pattern Mul n m = Op (Exp (Op (Exp (Return Mul_) n)) m)

--- Interpreter
--
eval_num :: Expr a -> a
eval_num (Sym (Num_ n))       = n
eval_num (Sym Add_ :$ n :$ m) = (eval_num n) + (eval_num m)
eval_num (Sym Mul_ :$ n :$ m) = (eval_num n) * (eval_num m)
-- not-exhaustive

render_num :: Expr a -> String
render_num (Sym (Num_ n))       = show n
render_num (Sym Add_ :$ n :$ m) = "(" ++ render_num n ++ "+" ++ render_num m ++ ")"
render_num (Sym Mul_ :$ n :$ m) = "(" ++ render_num n ++ "*" ++ render_num m ++ ")"
-- not-exhaustive

eval_num' :: HExpr a -> a
eval_num' (Num n)   = n
eval_num' (Add n m) = (eval_num' n) + (eval_num' m)
eval_num' (Add n m) = (eval_num' n) * (eval_num' m)
-- not-exhaustive

render_num' :: HExpr a -> String
render_num' (Num n)   = show n
render_num' (Add n m) = "(" ++ render_num' n ++ "+" ++ render_num' m ++ ")"
render_num' (Mul n m) = "(" ++ render_num' n ++ "*" ++ render_num' m ++ ")"
-- not-exhaustive
--
--
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------
-- Extensible Languages
-- ------------------------------------------------------------------------------
-- ------------------------------------------------------------------------------

newtype K' (c :: * -> *) (f :: * -> *) (a :: *) = K' {unK' :: c a}

data Logic a where
    Not_ :: Logic (Bool |-> Full Bool)
    Eq_ :: Eq a => Logic (a |-> a |-> Full Bool)

type Logic' = K' Logic

data If a where
    If_ :: If (Bool |-> a |-> a |-> Full a)

type If' = K' If

type Sig = (NUM' + Logic' + If') VoidF

type ExprBool a = HAst Sig a


