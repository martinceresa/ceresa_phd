{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE ViewPatterns          #-}


module HSyntactic where

import           DTC
import           FreeMonad
import           GenTerm
import           HFunctor

import           Evaluation

newtype Full a = Full {result :: a}
newtype a |-> b = Partial (a -> b)

infixr |->

data NUM e a where
    Num_ :: Int -> NUM e (Full Int)
    Mul_ :: NUM e (Int |-> Int |-> Full Int)
    Add_ :: NUM e (Int |-> Int |-> Full Int)

instance HFunctor NUM where
    hfmap _ (Num_ i) = Num_ i
    hfmap _ Mul_     = Mul_
    hfmap _ Add_     = Add_

data Logic e a where
    Not_ :: Logic e (Bool |-> Full Bool)
    Eq_ :: Eq a => Logic e (a |-> a |-> Full Bool)

instance HFunctor Logic where
    hfmap _ Not_ = Not_
    hfmap _ Eq_  = Eq_

data If e a where
    If_ :: If e (Bool |-> a |-> a |-> Full a)

instance HFunctor If where
    hfmap _ If_ = If_

data Axel e a where
    -- Sym :: e a -> Axel e a
    App :: e (b |-> a) -> e (Full b) -> Axel e a

instance HFunctor Axel where
    -- hfmap f (Sym x)   = Sym $ f x
    hfmap f (App h x) = App (f h) (f x)

pattern Num n <- (proj -> Just (Num_ n))
    where Num n = inj $ Num_ n

pattern Mul' <- (proj -> Just (Mul_))
    where Mul' = inj $ Mul_

pattern Add' <- (proj -> Just (Add_))
    where Add' = inj $ Add_

pattern Not' <- (proj -> Just (Not_))
    where Not' = inj $ Not_

pattern Eq' <- (proj -> Just (Eq_))
    where Eq' = inj $ Eq_

pattern If' <- (proj -> Just (If_))
    where If' = inj $ If_

-- pattern Sym' s <- (proj -> Just (Sym s))
--     where Sym' s = inj $ Sym s

pattern App' f x <- (proj -> Just (App f x))
    -- where App' f x = inj $ App' f x

type Sig = (If + (Logic + (NUM + Axel)))

type ExprAxel = Term Sig

-- Helpers

($$) :: (Axel < h) => HFree h f (b |-> a) -> HFree h f (Full b) -> HFree h f a
f $$ x = Op $ inj $ App f x

num ::( NUM < h) => Int -> HFree h f (Full Int)
num i = Op $ Num i

add :: ( NUM < h, Axel < h, HFunctor h) =>
     HFree h f (Full Int) -> HFree h f (Full Int) -> HFree h f (Full Int)
add n m = (inject Add_) $$ n $$ m

mul :: ( NUM < h, Axel < h, HFunctor h) =>
        HFree h f (Full Int) -> HFree h f (Full Int) -> HFree h f (Full Int)
mul n m = (inject Mul_) $$ n $$ m

eq :: (Eq a, Logic < h, Axel < h, HFunctor h) =>
        HFree h f (Full a) -> HFree h f (Full a) -> HFree h f (Full Bool)
eq p q = (inject Eq_) $$ p $$ q

lnot :: ( Logic < h, Axel < h, HFunctor h) =>
    HFree h f (Full Bool) -> HFree h f (Full Bool)
lnot b = (inject Not_) $$ b

lif :: (Eq a, If < h, Axel < h, HFunctor h) => HFree h f (Full Bool) ->
        HFree h f (Full a) -> HFree h f (Full a) -> HFree h f (Full a)
lif c tt ff = (inject If_) $$ c $$ tt $$ ff
--- Esto se parece mucho ya al aplicativo.
--
-- Eval instances

instance Eval NUM Id where
    evalAlg (Num_ i) = Id (Full i)
    evalAlg Add_     = Id $ Partial (\x -> Partial (\y -> Full $ x + y))
    evalAlg Mul_     = Id $ Partial (\x -> Partial (\y -> Full $ x * y))

instance Eval Axel Id where
    -- evalAlg (Sym (Id a))                         = Id a
    evalAlg (App (Id (Partial f)) (Id (Full x))) = Id $ f x

instance Eval Logic Id where
    evalAlg Not_ = Id $ Partial (\b -> Full $ not b)
    evalAlg Eq_  = Id $ Partial (\x -> Partial (\y -> Full $ x == y))

instance Eval If Id where
    evalAlg If_ = Id $ Partial (\c ->
                    Partial (\tt ->
                    Partial (\ff ->
                    Full $ if c then tt else ff)))


eval_total :: Term Sig (Full a) -> a
eval_total = result . runId . eval

------
--
--

ex2 :: HFree Sig f (Full Int)
ex2 = mul (add (num 5) (num 0)) (num 6)

ex2' :: HFree (NUM + Axel) f (Full Int)
ex2' = mul (add (num 5) (num 0)) (num 6)

ex3 :: HFree Sig f (Full Bool)
ex3 = eq ex2 ex2

-----
-- De esto no estoy seguro ni a palos
size  :: HFree Sig f a -> KF Int a
size x = fold (\ _ -> KF 1) alg x
    where
        alg (App' (KF f) (KF x)) = KF $ f + x
        alg _                    = KF 1

countAdd :: Term Sig :-> KF Int
countAdd x = fold (\ _ -> KF 0) alg x
    where
        alg (App' (KF f) (KF x)) = KF $ f + x
        alg Add'                 = KF $ 1
        alg _                    = KF 0

---
--
--
type family Denotation sig where
    Denotation (Full a) = a
    Denotation (a |-> sig) = a -> Denotation sig

-- HAlg NUM Denotation
evalSymNum :: NUM e :-> Denotation
evalSymNum (Num_ i) = i
evalSymNum Add_     = (+)
evalSymNum Mul_     = (*)

data Den a = Den {unD :: Denotation a}

algDen :: (forall e. sig e :-> Denotation) -> HAlg sig Den
algDen or t = Den $ or t

axelDen :: HAlg Axel Den
-- axelDen (Sym x)               = x
axelDen (App (Den f) (Den x)) = Den $ f x

evalG' :: (HFunctor sig) => HAlg sig Den -> Term (sig + Axel) :-> Den
evalG' alg t = cata (caseAlg alg axelDen) t

evalG :: (HFunctor sig) => (forall e. sig e :-> Denotation) -> Term (sig + Axel) a -> Denotation a
evalG al = unD . evalG' (algDen al)

class AxEval expr where
    axeval :: expr e :-> Denotation

instance AxEval NUM where
    axeval (Num_ i) = i
    axeval Add_     = (+)
    axeval Mul_     = (*)

instance AxEval Logic where
    axeval Not_ = not
    axeval Eq_  = (==)

instance AxEval If where
    axeval If_ = (\c tt ff -> if c then tt else ff)
--
--
evaluator :: (HFunctor sig, AxEval sig) => Term (sig + Axel) a -> Denotation a
evaluator = evalG axeval
