{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE GADTs           #-}
{-# LANGUAGE KindSignatures  #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE TypeFamilies    #-}

module SplitHAST where

import           Prefixes

import           HFunctor

data Types = HExp | HSubscript | HCArg | HEqu | HStmt
            | HMod | HArg | HSClass | HDec | HExt | HComp
            | HElem | HCompo | HClassType | HCDef


--- =================================== Paso 1.

type FstClassDefinition = ClassDefinition (Encapsulated, Partial, ClassPrefix)

type FMClassDefinition =  ClassDefinition ClassPrefix

data ClassDefinition tags :: (Types -> *) -> Types -> * where
  ClassDefinition_ :: tags -> e HClassType -> ClassDefinition tags e HCDef

--------------------------------------------------------------------------------
type FstClassType = ClassTypeK Ident

type FMClassType = ClassTypeK (Ident, Ident)

data ClassTypeExt' :: (Types -> *) -> Types -> * where
  ShortClass_ :: e HSClass -> StringComment -> Maybe [e HArg]  -> ClassTypeExt' e HClassType

data ClassTypeExt'' :: (Types -> *) -> Types -> * where
  EnumShortClass :: Ident -> Maybe [(Ident, StringComment, Maybe [e HArg])]
                 -> ClassTypeExt'' e HClassType

data ClassTypeExt :: (Types -> *) -> Types -> * where
  DerivedClass_ :: Ident -> Name -> [Ident] -> (StringComment, Maybe [e HArg]) -> ClassTypeExt e HClassType
  ExtendedClass_ :: Ident -> Maybe [e HArg] -> StringComment -> e HCompo -> ClassTypeExt e HClassType

data ClassTypeK ident :: (Types -> *) -> Types -> * where
   RegularClass_ :: ident -> StringComment -> e HCompo -> ClassTypeK ident e HClassType

--------------------------------------------------------------------------------

data MicroComposition :: (Types -> *) -> Types -> * where
  CompositionTAST_ :: [(e HComp , (StringComment , Maybe [e HArg]))]
                  -> [e HEqu]
                  -> [e HEqu]
                  -> [[(e HStmt , (StringComment, Maybe [e HArg]))]]
                  -> Maybe (e HExt)
                  -> Maybe [e HArg]
                  -> MicroComposition e HCompo

data FlatComposition :: (Types -> *) -> Types -> * where
  CompositionFAST_ :: [(e HComp , (StringComment , Maybe [e HArg]))]
                   -> [(e HComp , (StringComment , Maybe [e HArg]))]
                   -> [e HEqu]
                   -> [e HEqu]
                   -> [(Initial, [(e HStmt , (StringComment, Maybe [e HArg]))])]
                   -> Maybe (e HExt)
                   -> Maybe [e HArg]
                   -> FlatComposition e HCompo

data FirstComposition :: (Types -> *) -> Types -> * where
  CompositionAST_ :: [e HElem]
                  -> [e HElem]
                  -> [(Initial, [(e HEqu , StringComment, Maybe [e HArg])])]
                  -> [(Initial, [(e HStmt, StringComment, Maybe [e HArg])])]
                  -> (Maybe (e HExt)) -> (Maybe [e HArg])
                  -> FirstComposition e HCompo

data Import'
    = ImportIdent Ident Name
    | ImportList  Name (Either [Ident] String)

data Element :: (Types -> *) -> Types -> * where
  Import_      :: Import' -> (StringComment, Maybe [e HArg]) -> Element e HElem
  Extends_     :: Name -> (Maybe [e HArg]) -> (Maybe [e HArg]) -> Element e HElem
  ClassDef_    :: Replaceable -> e HCDef
               -> (Maybe (Name, Maybe [e HArg]) ,(StringComment, Maybe [e HArg]))
               -> Element e HElem
  ComponentEl_ :: Replaceable -> e HComp
               -> (Maybe (Name, Maybe [e HArg]),(StringComment, Maybe [e HArg]))
               -> Element e HElem

--------------------------------------------------------------------------------

type FstTypePrefix = (VarKind,
                      Variability,
                      Direction)

type FstComponent = Component FstTypePrefix

type FlatTypePrefix = (Variability,
                      Direction)

type FlatComponent = Component FlatTypePrefix

type MicroComponent = Component Variability

data Component tags :: (Types -> *) -> Types -> * where
  Component_ :: tags -> TypeSpec
             -> (Maybe [e HSubscript])
             -> [(e HDec, Maybe (e HExp), (StringComment, Maybe [e HArg]))]
             -> Component tags e HComp

--------------------------------------------------------------------------------

type FirstExternal = External []

type FlatExternal = External Id

data External (f :: * -> *) :: (Types -> *) -> Types -> * where
  External_ :: Maybe LangSpec
            -> Maybe ((Maybe (f (Ident, [e HSubscript]))), Ident, [e HExp])
            -> Maybe [e HArg] -> External f e HExt

--------------------------------------------------------------------------------

data ShortClass' :: (Types -> *) -> Types -> * where -- First AST
  ShortClass'_ :: FstTypePrefix
               -> Name
               -> (Maybe [e HSubscript])
               -> (Maybe [e HArg])
               -> ShortClass' e HSClass
  EnumShortClass_ :: Ident -> (Maybe [(Ident, StringComment, Maybe [e HArg])])
                  -> ShortClass' e HSClass

--------------------------------------------------------------------------------

-- Same for all asts.
data Declaration :: (Types -> *) -> Types -> * where
  Declaration_ :: Ident
               -> Maybe [e HSubscript]
               -> Maybe (e HMod) -> Declaration e HDec

--------------------------------------------------------------------------------

-- Argument = ArgumentExt + ArgumentK
data ArgumentExt  :: (Types -> *) -> Types -> * where
  ElementRepClass_ :: (e HSClass , StringComment, Maybe [e HArg])
                   -> Maybe (Name, Maybe [e HArg]) -> ArgumentExt e HArg
  ElementRepComp_  :: FstTypePrefix
                   -> (e HDec, StringComment, Maybe [e HArg])
                   -> Maybe (Name, Maybe [e HArg]) -> ArgumentExt e HArg
  ElementRedClass_ :: Each -> Final
                   -> (e HSClass , StringComment, Maybe [e HArg])
                   -> ArgumentExt e HArg
  ElementRedRep_   :: Each -> Final
                   -> e HArg -> ArgumentExt e HArg

data ArgumentK :: (Types -> *) -> Types -> * where
  ElementMod_ :: Each -> Final
              -> Name -> (Maybe (e HMod))
              -> StringComment -> ArgumentK e HArg

--------------------------------------------------------------------------------

data Modification :: (Types -> *) -> Types -> * where
  ModEqual_ :: e HExp -> Modification e HMod
  ModAssign_ :: e HExp -> Modification e HMod
  ModClass_ :: [e HArg] -> (Maybe (e HExp)) -> Modification e HMod

--------------------------------------------------------------------------------

data FstStmtAst e l = FstStmt
                      (Statement
                        ([e HCArg]
                        , [(Ident, e HCArg)]
                        , Maybe [(Ident, Maybe (e HExp))])
                        [(Ident, [e HSubscript])]
                        e
                        l
                      )
data FlatStmtAst e l = FlatStmt
                       (Statement
                        ([e HCArg]
                        , [(Ident, e HCArg)])
                        (Ident, [e HSubscript])
                        e
                        l
                       )

-- Statement
data Statement cargs ref :: (Types -> *) -> Types -> * where
  MBreak_   :: Statement cargs ref e HStmt
  MReturn_  :: Statement cargs ref e HStmt
  ForSt_    :: [(Ident, Maybe (e HExp))] -> [e HStmt] -> Statement cargs ref e HStmt
  WhenSt_   :: e HExp -> [e HStmt]
            -> [(e HExp, [e HStmt])] -> Statement cargs ref e HStmt
  IfSt_     :: e HExp -> [e HStmt] -> [(e HExp, [e HStmt])]
            -> Maybe [e HStmt] -> Statement cargs ref e HStmt
  Assign_   :: ref -> e HExp -> Statement cargs ref e HStmt
  CallSt_   :: ref -> cargs -> Statement cargs ref e HStmt
  OutputSt_ :: [Maybe (e HExp)] -> ref -> cargs -> Statement cargs ref e HStmt

--------------------------------------------------------------------------------

data FstEquationExt e l = FstEqu (EquationExt
                          ([e HCArg]
                          , [(Ident, e HCArg)]
                          , Maybe [(Ident, Maybe (e HExp))]) e l)

data FlatEquationExt e l = FlatEqu (EquationExt
                          ([e HCArg]
                          , [(Ident, e HCArg)]) e l)

data EquationExt cargs :: (Types -> *) -> Types -> * where
  IfEq_   :: e HExp -> [e HEqu] -> [(e HExp, e HEqu)] -> Maybe [e HEqu]
          -> EquationExt cargs e HEqu
  WhenEq_ :: e HExp -> [e HEqu] -> [(e HExp, [e HEqu])]
          -> EquationExt cargs e HEqu
  CallEq_ :: Name
          -> cargs
          -> EquationExt cargs e HEqu

data EquationExt' :: (Types -> *) -> Types -> * where
  Connect_ :: [(Ident, [e HSubscript])] -> [(Ident, [e HSubscript])]
           -> EquationExt' e HEqu

data Equation :: (Types -> *) -> Types -> * where
  Equality_ :: e HExp -> e HExp
            -> Equation e HEqu
  ForEq_    :: [(Ident , Maybe (e HExp))] -> [e HEqu]
            -> Equation e HEqu

--------------------------------------------------------------------------------

-- Exps = StrExp + Exp
data StrExp :: (Types -> *) -> Types -> * where
  StringExp_ :: String -> StrExp e HExp

data FirstAstExp e l = FstAst (Exp
                                ([e HCArg]
                                , [(Ident, e HCArg)]
                                , Maybe [(Ident, Maybe (e HExp))]) -- cargs
                                [(Ident, [e HSubscript])] -- ref
                                Integer -- num
                                e l)

data FlatAstExp e l = FlatAst (Exp
                                ([e HCArg]
                                , [(Ident, e HCArg)])
                                (Ident, [e HSubscript]) Int e l)

data Exp cargs ref num  :: (Types -> *) -> Types -> * where
  BoolExp_   :: Bool -> Exp cargs ref num e HExp
  NumExp_    :: Either num Double -> Exp cargs ref num e HExp
  End_       :: Exp cargs ref num e HExp
  RefExp_    :: ref -> Exp cargs ref num e HExp
  CallExp_   :: Name
             -> cargs
             -> Exp cargs ref num e HExp
  BinOpExp_  :: e HExp -> BOp -> e HExp -> Exp cargs ref num e HExp
  UnOpExp_   :: UOp -> e HExp -> Exp cargs ref num e HExp
  RangExp_   :: e HExp -> Maybe (e HExp) -> e HExp -> Exp cargs ref num e HExp
  IfExp_     :: e HExp -> e HExp -> [(e HExp, e HExp)] -> Maybe (e HExp)
             -> Exp cargs ref num e HExp
  ArrayExp_  :: cargs
             -> Exp cargs ref num e HExp
  MatrixExp_ :: [[e HExp]] -> Exp cargs ref num e HExp
  OutExp_    :: [Maybe (e HExp)] -> Exp cargs ref num e HExp


--------------------------------------------------------------------------------

data CallArg :: (Types -> *) -> Types -> *  where
  ExpArgument_ :: e HExp -> CallArg e HCArg
  FunArgument_ :: Name -> (Ident, [e HCArg]) -> CallArg e HCArg

-- data NamedArg :: (Types -> *) -> Types -> * where
--   NamedArgument_ :: Ident -> e HCArg -> NamedArg e HCArg

--------------------------------------------------------------------------------

data Subscript :: (Types -> *) -> Types -> * where
  All_ :: Subscript e HSubscript
  ExpSub_ :: e HExp -> Subscript e HSubscript
