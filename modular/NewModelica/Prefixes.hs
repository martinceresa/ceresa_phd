module Prefixes  where

newtype Ident             = Ident {unIdent :: String}                  deriving (Show, Eq, Ord)
newtype Name              = Name String                   deriving (Show, Eq, Ord)
newtype StringComment     = Stringcomment (Maybe String)  deriving (Show, Eq)
newtype LangSpec          = LangSpec String               deriving (Show, Eq)
newtype TypeSpec          = TypeSpec Name                 deriving (Show, Eq, Ord)


-- Prefixes --
data Initial
    = Initial
    | NoInitial
      deriving (Show, Eq)

data Visibility
    = Public
    | Protected
      deriving (Show,Eq)

data Partial
    = Partial
    | NoPartial
      deriving (Show,Eq)

data Redeclare
    = Redelcare
    | NoRedeclare
      deriving (Show,Eq)

data Inner
    = Inner
    | NoInner
      deriving (Show,Eq)

data Outer
    = Outer
    | NoOuter
      deriving (Show,Eq)

data Each
    = Each
    | NoEach
      deriving (Show,Eq)

data Final
    = Final
    | NoFinal
      deriving (Show,Eq)

data Replaceable
    = Replaceable
    | NoReplaceable
      deriving (Show,Eq)

data Expandable
    = Expandable
    | NoExpandable
      deriving (Show,Eq)

data Encapsulated
    = Encapsulated
    | NoEncapsulated
      deriving (Show,Eq)

data Oper
    = OperatorPrefix
    | NoOperatorPrefix
      deriving (Show,Eq)

data Purity
    = Pure
    | Impure
    | NoPurity
      deriving (Show, Eq)

data Direction
    = Input
    | Output
    | NoDirection
      deriving (Show, Eq)

data VarKind
    = Flow
    | Stream
    | NoVarKind
      deriving (Show, Eq)

data Variability
    = Discrete
    | Parameter
    | Constant
    | NoVariability
      deriving (Show, Eq)

data ClassPrefix
    = Class
    | Model
    | Record Oper
    | Block
    | Connector Expandable
    | Type
    | Package
    | Function Purity Oper
    | Operator
      deriving (Show, Eq)

-- Binary operations --
data BOp
    = Pow
    | PowEl
    | Mult
    | MultEl
    | Div
    | DivEl
    | Add
    | AddEl
    | Sub
    | SubEl
    | Lower
    | LowerEq
    | Greater
    | GreaterEq
    | Equal
    | NotEqual
    | And
    | Or
      deriving (Show, Eq)

-- Unary operations --
data UOp
    = Minus
    | Plus
    | Not
      deriving (Show, Eq)
