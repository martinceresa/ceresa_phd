{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE ViewPatterns          #-}

module SumAST where

import           DTC
import           FreeMonad
import           GenTerm
import           HFunctor

import           SplitHAST

(<$$>) :: (Functor f, Functor g) => (a -> b) -> f (g a) -> f (g b)
f <$$> x = (f <$>) <$> x

type FirstAst =
  FstClassDefinition
             + ClassTypeExt' + ClassTypeExt + FstClassType
             + FirstComposition
             + Element
             + FstComponent
             + FirstExternal
             + Declaration
             + ShortClass'
             + ArgumentExt + ArgumentK
             + Modification
             + FstStmtAst
             + EquationExt' + FstEquationExt + Equation
             + Subscript
             + CallArg
             + StrExp
             + FirstAstExp

type FirstTerms = HFree FirstAst VoidF

appCargs f (cargs, idcargs, mbes) = ( map f cargs
      , map (\(i, arg) -> (i , f arg) ) idcargs
      , (map (\(i, mbe) -> (i, f <$> mbe))) <$> mbes
      )

instance HFunctor FirstAstExp where
  hfmap f (FstAst exp) = FstAst $ case exp of
    (BoolExp_ b) -> BoolExp_ b
    (NumExp_ n) -> NumExp_ n
    End_ -> End_
    (RefExp_ xs) -> RefExp_ (map (\(i, hs) -> (i, map f hs)) xs)
    (CallExp_ nm (cargs, idcargs, mbes)) -> CallExp_ nm $
      ( map f cargs
      , map (\(i, arg) -> (i , f arg) ) idcargs
      , (map (\(i, mbe) -> (i, f <$> mbe))) <$> mbes
      )
    (BinOpExp_ le bop re) -> BinOpExp_ (f le) bop (f re)
    (UnOpExp_ uop e) -> UnOpExp_ uop (f e)
    (RangExp_ start step end) -> RangExp_ (f start) (f <$> step) (f end)
    (IfExp_ c te dunno elses) -> IfExp_ (f c) (f te)
                                 (map (\(x,y) -> (f x, f y)) dunno)
                                 (f <$> elses)
    (ArrayExp_ (cargs, idcargs, mbes)) -> ArrayExp_
                                          ( map f cargs
                                          , map (\(i, arg) -> (i , f arg) ) idcargs
                                          , (map (\(i, mbe) -> (i, f <$> mbe))) <$> mbes
                                          )
    (MatrixExp_ ess) -> MatrixExp_ $ map (map f) ess
    OutExp_ ems -> OutExp_ $ map (f <$>) ems

pattern BoolExpF b <- (proj -> Just (FstAst (BoolExp_ b)))
    where BoolExpF b = inj (FstAst (BoolExp_ b))
pattern NumExpF n <- (proj -> Just (FstAst (NumExp_ n)))
     where NumExpF n = inj (FstAst (NumExp_ n))
pattern EndF <- (proj -> Just (FstAst End_))
     where EndF = inj (FstAst End_)
pattern RefExpF r <- (proj -> Just (FstAst (RefExp_ r)))
     where RefExpF r = inj (FstAst (RefExp_ r))
pattern CallExpF name cargs <- (proj -> Just (FstAst (CallExp_ name cargs)))
     where CallExpF name cargs = inj (FstAst (CallExp_ name cargs))
pattern BinOpExpF lexp bop rexp <- (proj -> Just (FstAst (BinOpExp_ lexp bop rexp)))
     where BinOpExpF lexp bop rexp  = inj (FstAst (BinOpExp_ lexp bop rexp))
pattern UnOpExpF uop exp <- (proj -> Just (FstAst (UnOpExp_ uop exp)))
     where UnOpExpF uop exp  = inj (FstAst (UnOpExp_ uop exp))
pattern RangExpF iexp eexp exp <- (proj -> Just (FstAst (RangExp_ iexp eexp exp)))
     where RangExpF iexp eexp exp = inj (FstAst(RangExp_ iexp eexp exp))
pattern IfExpF e1 e2 r me <- (proj -> Just (FstAst (IfExp_ e1 e2 r me)))
     where IfExpF e1 e2 r me = inj (FstAst(IfExp_ e1 e2 r me))
pattern ArrayExpF cargs <- (proj -> Just (FstAst (ArrayExp_ cargs)))
     where ArrayExpF cargs = inj (FstAst(ArrayExp_ cargs))
pattern MatrixExpF esss <- (proj -> Just (FstAst (MatrixExp_ esss)))
     where MatrixExpF esss = inj (FstAst(MatrixExp_ esss))
pattern OutExpF mes <- (proj -> Just (FstAst (OutExp_ mes)))
     where OutExpF mes = inj (FstAst(OutExp_ mes ))

----

instance HFunctor StrExp where
  hfmap _ (StringExp_ str) = StringExp_ str

pattern StringExpF str <- (proj -> Just (StringExp_ str))
     where StringExpF str = inj (StringExp_ str)
----
instance HFunctor CallArg where
  hfmap f (ExpArgument_ e) = ExpArgument_ (f e)
  hfmap f (FunArgument_ nm (ident, args)) = FunArgument_ nm (ident, map f args)

pattern ExpArgumentF e <- (proj -> Just (ExpArgument_ e))
     where ExpArgumentF e = inj (ExpArgument_ e)

pattern FunArgumentF nm iargs <- (proj -> Just (FunArgument_ nm iargs))
     where FunArgumentF nm iargs = inj (FunArgument_ nm iargs)
----

instance HFunctor Subscript where
  hfmap _ All_        = All_
  hfmap f (ExpSub_ e) = ExpSub_ $ f e

pattern AllF <- (proj -> Just All_)
     where AllF = inj All_

pattern ExpSubF e <- (proj -> Just (ExpSub_ e))
     where ExpSubF e = inj (ExpSub_ e)

---

instance HFunctor Equation where
  hfmap f (Equality_ l r) = Equality_ (f l) (f r)
  hfmap f (ForEq_ mbes eqs) = ForEq_
    (map (\(i, me) -> (i, f <$> me)) mbes)
    (map f eqs)

pattern EqualityF l r <- (proj -> Just (Equality_ l r))
     where EqualityF l r = inj $ Equality_ l r

pattern ForEqF mbes eqs <- (proj -> Just (ForEq_ mbes eqs))
     where ForEqF mbes eqs = inj (ForEq_ mbes eqs)

instance HFunctor FstEquationExt where
  hfmap f (FstEqu e) = FstEqu $ case e of
    IfEq_ cond tes elses mbother ->
      IfEq_ (f cond)
            (map f tes)
            (map (\(e,eq) -> (f e, f eq)) elses)
            (map f <$> mbother)
    WhenEq_ cond eqs does -> WhenEq_
      (f cond)
      (f <$> eqs)
      (map (\(e, eqs) -> (f e, map f eqs)) does)
    CallEq_ nm (cargs, idcargs, mbes) -> CallEq_
      nm
      ( map f cargs
      , map (\(i, arg) -> (i , f arg) ) idcargs
      , (map (\(i, mbe) -> (i, f <$> mbe))) <$> mbes
      )

pattern IfEqF cond tes elses mbother <- (proj -> Just (FstEqu (IfEq_ cond tes elses mbother)))
     where IfEqF cond tes elses mbother = inj $ FstEqu $ IfEq_ cond tes elses mbother

pattern WhenEqF cond eqs does <- (proj -> Just (FstEqu (WhenEq_ cond eqs does)))
     where WhenEqF cond eqs does = inj $ FstEqu $ WhenEq_ cond eqs does

pattern CallEqF nm args <- (proj -> Just (FstEqu (CallEq_ nm args )))
     where CallEqF nm args = inj $ FstEqu $ CallEq_ nm args

instance HFunctor EquationExt' where
  hfmap f (Connect_ subsl subsr) = Connect_
    ((\(i,subs) -> (i, f <$> subs)) <$> subsl)
    ((\(i,subs) -> (i, f <$> subs)) <$> subsr)

pattern ConnectF subl subr <- (proj -> Just (Connect_ subl subr ))
     where ConnectF subl subr = inj $ Connect_ subl subr

---

instance HFunctor FstStmtAst where
  hfmap f (FstStmt stmt) = FstStmt $ case stmt of
    MBreak_ -> MBreak_
    MReturn_ -> MReturn_
    ForSt_ ids stmt -> ForSt_ ((\(i, mbe) -> (i, f <$> mbe )) <$> ids) (f <$> stmt)
    WhenSt_ e stmts does -> WhenSt_
      (f e)
      (f <$> stmts)
      ((\(e,sms) -> (f e, f <$> sms)) <$> does)
    IfSt_ cond trues elses mbmore -> IfSt_
      (f cond)
      (f <$> trues)
      ((\(e,sms) -> (f e, f <$> sms)) <$> elses)
      (f <$$> mbmore)
    Assign_ ref  e -> Assign_
      ((\(i, subs) -> (i, f <$> subs)) <$> ref) (f e)
    CallSt_ ref (cargs, idcargs, mbes) -> CallSt_
      ((\(i, subs) -> (i, f <$> subs)) <$> ref)
      ( map f cargs
      , map (\(i, arg) -> (i , f arg) ) idcargs
      , (map (\(i, mbe) -> (i, f <$> mbe))) <$> mbes
      )
    OutputSt_ outes ref (cargs, idcargs, mbes) -> OutputSt_
      (f <$$> outes)
      ((\(i, subs) -> (i, f <$> subs)) <$> ref)
      ( map f cargs
      , map (\(i, arg) -> (i , f arg) ) idcargs
      , (map (\(i, mbe) -> (i, f <$> mbe))) <$> mbes
      )

pattern MBreakF <- (proj -> Just (FstStmt MBreak_))
     where MBreakF = inj $ FstStmt $ MBreak_
pattern MReturnF <- (proj -> Just (FstStmt MReturn_))
     where MReturnF = inj $ FstStmt $ MReturn_

pattern ForStF cond stmts <- (proj -> Just ( FstStmt (ForSt_ cond stmts)))
     where ForStF cond stmts = inj $ FstStmt $ ForSt_ cond stmts

pattern WhenStF cond stmts does <- (proj -> Just (FstStmt (WhenSt_ cond stmts does )))
     where WhenStF cond stmts does = inj $ FstStmt $ WhenSt_ cond stmts does

pattern IfStF cond tt elses mbes <- (proj -> Just (FstStmt (IfSt_ cond tt elses mbes )))
     where IfStF cond tt elses mbes = inj $ FstStmt  $ IfSt_ cond tt elses mbes

pattern AssignF ref ex <- (proj -> Just (FstStmt (Assign_ ref ex )))
     where AssignF ref ex = inj $ FstStmt  $ Assign_ ref ex

pattern CallStF ref args <- (proj -> Just (FstStmt (CallSt_ ref args)))
     where CallStF ref args = inj $ FstStmt  $ CallSt_ ref args

pattern OutputStF ems ref args <- (proj -> Just (FstStmt (OutputSt_ ems ref args)))
     where OutputStF ems ref args = inj $ FstStmt  $ OutputSt_ ems ref args

---

instance HFunctor Modification where
  hfmap f (ModEqual_ e)        = ModEqual_ $ f e
  hfmap f (ModAssign_ e)       = ModAssign_ $ f e
  hfmap f (ModClass_ args mbe) = ModClass_ (f <$> args) (f <$> mbe)

pattern ModEqualF e <- (proj -> Just (ModEqual_ e))
     where ModEqualF e = inj $ ModEqual_ e

pattern ModAssignF e <- (proj -> Just (ModAssign_ e))
     where ModAssignF e = inj $ ModAssign_ e

pattern ModClassF args mbe <- (proj -> Just ( ModClass_ args mbe))
     where ModClassF args mbe = inj $ ModClass_ args mbe

---

instance HFunctor ArgumentK where
  hfmap f (ElementMod_ each final nm mbmod comm) = ElementMod_
    each final nm (f <$> mbmod) comm

pattern ElementModF each final nm mbmod comm <- (proj -> Just ( ElementMod_ each final nm mbmod comm))
     where ElementModF each final nm mbmod comm = inj $ ElementMod_ each final nm mbmod comm

instance HFunctor ArgumentExt where
  hfmap f (ElementRepClass_ (cls, comm, mbargs) mbs) = ElementRepClass_
    (f cls, comm, f <$$> mbargs)
    ((\(nm, es) -> (nm, f <$$> es)) <$> mbs)
  hfmap f (ElementRepComp_ tpref (dec, comm , mbargs) mbs) = ElementRepComp_
    tpref (f dec, comm, f <$$> mbargs)
    ((\(nm, es) -> (nm, f <$$> es)) <$> mbs)
  hfmap f (ElementRedClass_ each final (cls, comm, mbargs)) = ElementRedClass_
    each final (f cls, comm, f <$$> mbargs)
  hfmap f (ElementRedRep_ each final arg) = ElementRedRep_
    each final (f arg)

pattern ElementRepClassF args mbs <- (proj -> Just ( ElementRepClass_ args mbs))
     where ElementRepClassF args mbs = inj $ ElementRepClass_ args mbs

pattern ElementRepCompF tpref args mbs <- (proj -> Just ( ElementRepComp_ tpref args mbs))
     where ElementRepCompF tpref args mbs = inj $ ElementRepComp_ tpref args mbs

pattern ElementRedClassF each final args <- (proj -> Just ( ElementRedClass_ each final args))
     where ElementRedClassF each final args = inj $ ElementRedClass_ each final args

pattern ElementRedRepF each final arg <- (proj -> Just ( ElementRedRep_ each final arg))
     where ElementRedRepF each final arg = inj $ ElementRedRep_ each final arg

---

instance HFunctor Declaration where
  hfmap f (Declaration_ ident msubs mmod) = Declaration_ ident
    ((f <$> ) <$> msubs) (f <$> mmod)

pattern DeclarationF ident msubs mmod <- (proj -> Just ( Declaration_ ident msubs mmod))
     where DeclarationF ident msubs mmod = inj $ Declaration_ ident msubs mmod

instance HFunctor ShortClass' where
  hfmap f (ShortClass'_ tpref nm msubs margs) = ShortClass'_ tpref nm
    (f <$$> msubs) (f <$$> margs)
  hfmap f (EnumShortClass_ iden args) = EnumShortClass_ iden $
    (\(ident, comm, margs) -> (ident, comm, f <$$> margs)) <$$> args

pattern ShortClassF' tpref nm msubs margs <- (proj -> Just ( ShortClass'_ tpref nm msubs margs))
     where ShortClassF' tpref nm msubs margs = inj $ ShortClass'_ tpref nm msubs margs

pattern EnumShortClassF iden args <- (proj -> Just ( EnumShortClass_ iden args))
     where EnumShortClassF iden args = inj $ EnumShortClass_ iden args

---

instance (Functor f) => HFunctor (External f) where
  hfmap f (External_ mbspec mbrefext mbargs) =
    External_ mbspec
      ( (\(subs, ident, es) ->
           ( (\(i, hsubs) -> (i, f <$> hsubs)) <$$> subs
           , ident
           , f <$> es)
        ) <$> mbrefext)
      (f <$$> mbargs)

pattern ExternalF mbspec mbrefext mbargs <- (proj -> Just (External_ mbspec mbrefext mbargs))
  where ExternalF mbspec mbrefext mbargs = inj $ External_ mbspec mbrefext mbargs

---

instance HFunctor (Component tags) where
  hfmap f (Component_ tags tspec msubs comps) = Component_ tags tspec
    (f <$$> msubs)
    ( (\(dec, mbe, (comm, margs)) ->
         (f dec, f <$> mbe
         ,(comm , f <$$> margs))
         ) <$> comps)

pattern ComponentF tags tspec msubs comps <- (proj -> Just (Component_ tags tspec msubs comps))
  where ComponentF tags tspec msubs comps = inj $ Component_ tags tspec msubs comps

---

instance HFunctor Element where
  hfmap f (Import_ im (comm, margs)) = Import_ im (comm, f <$$> margs)
  hfmap f (Extends_ nm mbargs1 mbargs2) = Extends_ nm (f <$$> mbargs1) (f <$$> mbargs2)
  hfmap f (ClassDef_ rep cdef (mbargs1, (comm, mbargs2))) = ClassDef_ rep (f cdef)
    ( (\(nm, margs) -> (nm, f <$$> margs)) <$> mbargs1 , (comm, f <$$> mbargs2))
  hfmap f (ComponentEl_ rep comp (mbargs1, (comm, mbargs2))) = ComponentEl_ rep (f comp)
    ( (\(nm, margs) -> (nm, f <$$> margs)) <$> mbargs1 , (comm, f <$$> mbargs2))

pattern ImportF im args <- (proj -> Just (Import_ im args))
  where ImportF im args = inj $ Import_ im args

pattern ExtendsF im args args' <- (proj -> Just (Extends_ im args args'))
  where ExtendsF im args args' = inj $ Extends_ im args args'

pattern ClassDefF rep cdef args <- (proj -> Just (ClassDef_ rep cdef args))
  where ClassDefF rep cdef args = inj $ ClassDef_ rep cdef args

pattern ComponentElF rep comp args <- (proj -> Just (ComponentEl_ rep comp args))
  where ComponentElF rep comp args = inj $ ComponentEl_ rep comp args

---

instance HFunctor FirstComposition where
  hfmap f (CompositionAST_ es es' eqs stms mbext margs) = CompositionAST_
    (f <$> es) (f <$> es')
    ((\(initial, eqs) -> (initial, (\(eq, comm, margs) -> (f eq ,comm, f <$$> margs)) <$> eqs)) <$> eqs)
    ((\(initial, stmts) -> (initial, (\(stmt, comm, margs) -> (f stmt ,comm, f <$$> margs)) <$> stmts)) <$> stms)
    (f <$> mbext) (f <$$> margs)

pattern CompositionF es es' eqs stms mbext margs <- (proj -> Just (CompositionAST_ es es' eqs stms mbext margs))
  where CompositionF es es' eqs stms mbext margs = inj $ CompositionAST_ es es' eqs stms mbext margs

---

instance HFunctor (ClassTypeK ident) where
  hfmap f (RegularClass_ ident comm comp) = RegularClass_ ident comm (f comp)

pattern RegularClassF ident comm comp <- (proj -> Just (RegularClass_ ident comm comp))
  where RegularClassF ident comm comp = inj $ RegularClass_ ident comm comp

instance HFunctor ClassTypeExt where
  hfmap f (DerivedClass_ iden name ids (comm, mbargs)) = DerivedClass_ iden name ids (comm, f <$$> mbargs)
  hfmap f (ExtendedClass_ iden margs comm comp) = ExtendedClass_ iden (f <$$> margs) comm (f comp)

pattern DerivedClassF ident name ids margs <- (proj -> Just (DerivedClass_ ident name ids margs))
  where DerivedClassF ident name ids margs = inj $ DerivedClass_ ident name ids margs

pattern ExtendedClassF ident margs comm comp <- (proj -> Just (ExtendedClass_ ident margs comm comp))
  where ExtendedClassF ident margs comm comp = inj $ ExtendedClass_ ident margs comm comp


instance HFunctor ClassTypeExt' where
  hfmap f (ShortClass_ scls comm margs) = ShortClass_ (f scls) comm (f <$$> margs)

pattern ShortClassF scls comm margs <- (proj -> Just (ShortClass_ scls comm margs))
  where ShortClassF scls comm margs = inj $ ShortClass_ scls comm margs

instance HFunctor (ClassDefinition tags) where
  hfmap f (ClassDefinition_ ts clst) = ClassDefinition_ ts (f clst)

pattern ClassDefinitionF ts clst <- (proj -> Just (ClassDefinition_ ts clst))
  where ClassDefinitionF ts clst = inj $ ClassDefinition_ ts clst

---

type SndAst = FMClassDefinition
              + ClassTypeExt'' + ClassTypeExt + FMClassType
              + FlatComposition
              + FlatComponent
              + FlatExternal
              + Declaration
              + ArgumentK
              + Modification
              + FlatStmtAst
              + FlatEquationExt + Equation
              + Subscript
              + CallArg
              + StrExp + FlatAstExp

type TrdAst = FMClassDefinition
            + FMClassType
            + MicroComposition
            + MicroComponent
            + FlatExternal
            + Declaration
            + ArgumentK
            + Modification
            + FlatStmtAst
            + Equation
            + FlatAstExp
            + CallArg
            + Subscript
