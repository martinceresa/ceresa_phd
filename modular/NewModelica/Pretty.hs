{-# LANGUAGE RankNTypes,
             TypeOperators,
             KindSignatures,
             MultiParamTypeClasses,
             FlexibleInstances,
             FlexibleContexts,
             ViewPatterns,
             PatternSynonyms,
             DataKinds, PolyKinds,
             GADTs
 #-}

module Pretty where

import SumAST
import SplitHAST
import Prefixes

import Evaluation
import HFunctor as HFun
import FreeMonad
import GenTerm
import DTC

import Text.PrettyPrint as PP

type Section a            = (Initial, [a])

----

toDoc :: (Show a) => a -> Doc
toDoc = text . show

--------------
tClass    = text "class"
tModel    = text "model"
tType     = text "type"
tBlock    = text "block"
tWhen     = text "when"
tEnd      = text "end"
tIn       = text "in"
tEndFor   = text "end for"
tLoop     = text "loop"
tFor      = text "for"
tIf       = text "if"
tElse     = text "else"
tThen     = text "then"
tTrue     = text "true"
tFalse    = text "false"
tConnect  = text "connect"
tEncapsulated = text "encapsulated"
tPartial = text "partial"
tEquation = text "equation"
tInitial = text "initial"

pRef :: (e HSubscript -> Doc) ->  (Ident, [e HSubscript]) -> Doc
pRef r (Ident n, []) = text n
pRef r (Ident n, xs) = text n <> brackets (commaSep r xs)

commaSep = sepBy comma

sepBy :: Doc -> (a -> Doc) -> [a] ->Doc
sepBy sep f []  = PP.empty
sepBy sep f [x] = f _
sepBy sep f (x:xs) = (f x <> sep) <+> sepBy sep f xs

pReference :: (e HSubscript -> Doc) ->  [(Ident, [e HSubscript])]
           -> Doc
pReference toD [] = error "Found empty reference"
pReference toD r  = commaSep (pRef toD) r

pCallArguments :: (e HCArg -> Doc)
               -> ([e HCArg]
                  , [(Ident, e HCArg)]
                  , Maybe [(Ident, Maybe (e HExp))]) -> Doc
pCallArguments f (args, _ , _) = commaSep f args

pBinOp :: BOp -> Doc
pBinOp Add = text "+"
pBinOp Sub = text "-"
pBinOp Mult = text "*"
pBinOp Div = text "/"

pUnOp :: UOp -> Doc
pUnOp Minus = text "-"
pUnOp Not = text "not "
pUnOp Plus = text "+"

optional :: (a -> Doc) -> Maybe a -> Doc
optional f Nothing = PP.empty
optional f (Just a) = f a

endBySemi :: (a->Doc) -> [a] -> Doc
endBySemi = endBy semi

endBy :: Doc -> (a -> Doc) -> [a] ->Doc
endBy sep f [] = PP.empty
endBy sep f (x:xs) = (f x <> sep) $$ endBy sep f xs

pIndex :: (e HExp -> Doc) -> (Ident , Maybe (e HExp)) -> Doc
pIndex f (n, Nothing) = text $ unIdent n
pIndex f (n, Just e) = (text $ unIdent n) <+> tIn <+> f e

ifIsA :: Eq a => a -> Doc -> a -> Doc
ifIsA a d a' | a == a' = d
ifIsA _ _ _ = PP.empty

pClassPrefix :: ClassPrefix -> Doc
pClassPrefix Class = tClass
pClassPrefix Model = tModel
pClassPrefix Block = tBlock
pClassPrefix Type  = tType

pSection :: ([a]-> Doc) -> Doc -> Section a -> Doc
pSection f t (i, elems) = nest (-8) ((ifIsA Initial tInitial i) <+> t $$ (nest 8 (f elems)))

----
instance EvalK StrExp Doc where
  evalKAlg (StringExp_ s) = text s

instance EvalK FirstAstExp Doc where
  evalKAlg (FstAst inner) = case inner of
           BoolExp_ b -> toDoc b
           NumExp_ n  -> toDoc n
           End_ -> text "end"
           RefExp_ xs -> pReference unKF xs
           CallExp_ (Name n) cargs -> text n <> parens (pCallArguments unKF cargs)
           BinOpExp_ le b lr -> (unKF le) <> pBinOp b <> (unKF lr)
           UnOpExp_  uop e -> pUnOp uop <> (unKF e)
           RangExp_  start _ end -> (unKF start) <> colon <> (unKF end)
           IfExp_  cond trueExp _ (Just elseExp) ->
             tIf
             <+> unKF cond
             <+> tThen <+> unKF trueExp
             <+> tElse <+> unKF elseExp
           ArrayExp_ _ -> braces PP.empty
           MatrixExp_ _ -> brackets PP.empty
           OutExp_ xs -> parens $ commaSep (optional unKF) xs

instance Eval CallArg (KF Doc) where
  evalAlg (ExpArgument_ (KF x)) = KF x
  evalAlg (FunArgument_ _ _)  = error "WTF 2"

instance Eval Subscript (KF Doc) where
  evalAlg All_ = KF colon
  evalAlg (ExpSub_ (KF ex)) = KF ex

instance Eval Equation (KF Doc) where
  evalAlg (Equality_ l r) = KF $ unKF l <+> equals <+> unKF r
  evalAlg (ForEq_ ind eqs) = KF $
    tFor <+> (commaSep (pIndex unKF) ind) <+> tLoop
    $$ nest 2 (endBySemi unKF eqs)
    $$ tEndFor

instance Eval EquationExt' (KF Doc) where
  evalAlg (Connect_ l r) =
    let KF l' = evalAlg (FstAst (RefExp_ l))
        KF r' = evalAlg (FstAst (RefExp_ r))
        in KF $ tConnect
                   <> lparen
                   <> l' <> comma <> r'
                   <> rparen

instance Eval FstEquationExt (KF Doc) where
  evalAlg (FstEqu q) = KF $ case q of --  (WhenEq_ cond eqs _)) =
    WhenEq_ cond eqs _ -> tWhen
      <+> unKF cond <+> tThen
      $$ nest 2 ( endBySemi unKF eqs)
      $$ tEnd
      <+> tWhen
    CallEq_ (Name nm) args -> text nm <> parens (pCallArguments unKF args)
    IfEq_ {} -> error "WTF 3"

instance Eval FstClassDefinition (KF Doc) where
  evalAlg (ClassDefinition_ (e, partial, pref) ctype) = KF $
    (ifIsA Encapsulated tEncapsulated e)
    <+> (ifIsA Partial tPartial partial)
    <+> pClassPrefix pref <+> (unKF ctype)

instance Eval Declaration (KF Doc) where
  evalAlg (Declaration_ ident _ _) = KF $ text $ unIdent ident

instance Eval FstComponent (KF Doc) where
  evalAlg (Component_ _ (TypeSpec (Name ts)) _ decls ) = KF $
    text ts
    <+>
    commaSep unKF [a | (a,_,_) <- decls]

instance Eval Element (KF Doc) where
  evalAlg (ComponentEl_ _ comp _) = KF $ unKF comp -- change tag type...
  evalAlg _ = error "WTF 4"

instance Eval FirstComposition (KF Doc) where
  evalAlg (CompositionAST_ pubElems proElems eqs algs _ _) = KF $
    endBySemi unKF pubElems
    $$ vcat (map (pSection (endBySemi (\(x, _ , _) -> unKF x)) tEquation) eqs)

instance Eval FstClassType (KF Doc) where
  evalAlg (RegularClass_ n com comp ) = KF $
    n'
    $$ unKF comp
    $$ (nest (-8) tEnd <+> n')
    where n' = text $ unIdent n

--- Not defined instances

instance Eval ClassTypeExt' (KF Doc) where
  evalAlg _ = error "not defined"

instance Eval ClassTypeExt (KF Doc) where
  evalAlg _ = error "not defined"

instance Eval FstStmtAst (KF Doc) where
  evalAlg _ = error "not defined"

instance Eval Modification (KF Doc) where
  evalAlg _ = error "not defined"

instance Eval ArgumentExt (KF Doc) where
  evalAlg _ = error "not defined"

instance Eval ArgumentK (KF Doc) where
  evalAlg _ = error "not defined"

instance Eval ShortClass' (KF Doc) where
  evalAlg _ = error "not defined"

instance Eval FirstExternal (KF Doc) where
  evalAlg _ = error "not defined"



-- testsssss

-- numExp :: (FirstAstExp < h) =>  Either Integer Double -> HFree h f 'HExp
numExp n1 = Op (NumExpF n1)

-- binOpExp le bop lr = Op (BinOpExpF le bop lr)
-- binOpExp :: (FirstAstExp < h) => HFree h f 'HExp -> BOp -> HFree h f 'HExp -> HFree h f 'HExp
binOpExp le bop lr = Op (BinOpExpF le bop lr)

----

kfrender :: KF Doc e -> String
kfrender = render . unKF

printer :: (HFunctor e, Eval e (KF Doc)) => HFree e VoidF :-> (KF Doc)
printer  = fold HFun.empty evalAlg

----

num5 :: FirstTerms 'HExp
num5 = numExp (Left 5)

num6 :: FirstTerms 'HExp
num6 = numExp (Right 6.0)

num11 :: FirstTerms 'HExp
num11 = binOpExp num5 Add num6
