{-# LANGUAGE RankNTypes,
             TypeOperators,
             KindSignatures,
             MultiParamTypeClasses,
             FlexibleInstances,
             FlexibleContexts,
             ViewPatterns,
             PatternSynonyms,
             DataKinds,
             GADTs,
UndecidableInstances
 #-}

module Sugar where

import Expr
import AST

data Sug e l where
  Neg_ :: e Int -> Sug e Int
  Swap_ :: e (s, t) -> Sug e (t, s)

type Sig' = Sug + Sig

instance (Val < f, Oper < f, HFunctor f) => Eval Sug (Term f) where
  evalAlg (Neg_ x) = mult (i (-1)) x
  evalAlg (Swap_ p) = pair (p2 p) (p1 p)

instance HFunctor Sug where
  hfmap f (Neg_ x) = Neg_ (f x)
  hfmap f (Swap_ p) = Swap_ (f p)

pattern Neg n <- (proj -> Just (Neg_ n))
  where Neg n = inj (Neg_ n)

pattern Swap p <- (proj -> Just (Swap_ p))
  where Swap p = inj (Swap_ p)


evalSug :: (Eval Sig v) => Term Sig' :-> v
evalSug = eval . desug

runSug :: Term Sig' a -> a
runSug = runId . evalSug

neg :: (Sug < h) => HFree h f Int -> HFree h f Int
neg x = Op (Neg x)

swap :: (Sug < h) => HFree h f (s, t) -> HFree h f (t, s)
swap p = Op (Swap p)

test_neg :: Term Sig' Int
test_neg = neg (i 25)

desug' :: Term Sig' :-> Term Sig
desug' = cata (caseAlg evalAlg Op)

test_neg_desug :: Term Sig Int
test_neg_desug = desug test_neg

test_swap :: Term Sig' (Int, Int)
test_swap = swap test

test_swap_desug :: Term Sig (Int, Int)
test_swap_desug = desug test_swap
