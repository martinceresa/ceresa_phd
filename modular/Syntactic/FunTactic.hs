{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}
module FunTactic where

import           LSyntactic

import           Modular.High.DTC
import           Modular.High.Evaluation
import           Modular.High.FreeMonad
import           Modular.High.GenTerm
import           Modular.High.HFunctor

import           Control.Monad.State
import           Control.Monad.Writer

-- Simple Interpretation
--

instance Eval NUM Id where
    evalAlg (Num n) = Id n
    evalAlg (Mul (Id n) (Id m)) = Id $ n * m
    evalAlg (Add (Id n) (Id m)) = Id $ n + m

eval_num :: Expr3 a -> a
eval_num = run

test1exp = (num3 5 `mul3` num3 6)

test1 = eval_num test1exp

instance Eval NUM (KF String) where
    evalAlg (Num n) = KF $ show n
    evalAlg (Mul (KF n) (KF m)) = KF $"(" ++ n ++ " * " ++ m ++ ")"
    evalAlg (Add (KF n) (KF m)) = KF $"(" ++ n ++ " + " ++ m ++ ")"

rendernum :: Expr3 a -> String
rendernum = runK
--
--- Extensible Languages
--
test2 :: (NUM < dom) => HFree dom f Int
test2 = (num 5 `add` num 0) `mul` num 6

-- \> eval_num test2
-- 30 (Fucking work!)
--

test3 :: (Logic < dom, NUM < dom) => HFree dom f Bool
test3 = eq test2 test2

--- Functions over extensible Lang
--
-- we cannot write size? No directly.
--
--
numSize :: NUM (KF Int) :-> KF Int
numSize (Num_ _) = KF 1
numSize (Add_ (KF t1) (KF t2)) = KF $ 1 + t1 + t2
numSize (Mul_ (KF n) (KF m)) = KF $ 1 + n + m

logicSize :: Logic (KF Int) :-> KF Int
logicSize (Not_ (KF t)) = KF $ 1 + t
logicSize (Eq_ (KF t1) (KF t2)) = KF $ 1 + t1 + t2

ifSize ::  If (KF Int) :-> KF Int
ifSize (If_ (KF c) (KF tt) (KF ff)) = KF $ 1 + c + ff + tt

expSize = appKAlg (caseAlg numSize (caseAlg ifSize logicSize))

appKAlg :: (HFunctor dom) => (dom (KF Int) :-> KF Int) -> Term dom x -> Int
appKAlg f t = query' 0 f t

----
-- Awareness functions... Even worse
-- We can do it in just one function...
countAdd' :: (NUM < dom, If < dom, Logic < dom, HFunctor dom) => dom (KF Int) :-> KF Int
countAdd' t = KF $ case t of
                    Num _ -> 0
                    Add (KF t1) (KF t2) -> 1 + t1 + t2
                    Mul (KF t1) (KF t2) -> t1 + t2
                    Not (KF b) -> b
                    Eq (KF t1) (KF t2) -> t1 + t2
                    If (KF c) (KF tt) (KF ff) -> c + tt + ff
                    _ -> 0

countAdd :: (NUM < dom, If < dom, Logic < dom, HFunctor dom) => Term dom x -> Int
countAdd = appKAlg countAdd'

--- Qué onda con Fold?
-- Núnca lo terminamos de implementar... de ver si es correcta o no la
-- definición de la clase...
instance KFoldable NUM where
    kfold c (KF b) (Num_ _) = KF b
    kfold c (KF b) (Mul_ (KF x) (KF y)) = KF $ c (c b x) y
    kfold c (KF b) (Add_ (KF x) (KF y)) = KF $ c (c b x) y

gsize :: Term NUM x -> Int
gsize = queryF (const $ KF 1) (+)

-- Pero esto no lo puedo escribir no?
-- debería tener una especie de listas heterogeneas en el 'x'??
-- subs :: (KFoldable dom, HFunctor dom) => Term dom x -> [Term dom x]
-- subs = queryF (\y -> KF [y]) (++)

--
-- sizedNUM = query foldNUM (const $ KF 1) (\ (KF x) (KF y) -> KF $ x + y)
-- sizedNum = foldNUM (\ x y -> x + (sizedNum y)) 1
sumKF :: Num a => KF a b -> KF a b -> KF a b
sumKF (KF x) (KF y) = KF $ x + y

-- Pattern Matching
--
-------------

optAddTop :: (NUM < dom) => Term dom a -> Term dom a
optAddTop (OAdd a (ONum 0)) = a
optAddTop x = x


-- Generic Interpretation
--- Evaluators are made inside Eval dom Id instance.
-- an evaluated with runId . eval.
--
instance Eval Logic Id where
    evalAlg (Not_ (Id b)) = Id $ Prelude.not b
    evalAlg (Eq_ (Id p) (Id q)) = Id $ p == q

instance Eval If Id where
    evalAlg (If_ (Id c) (Id tt) (Id ff)) = Id $ if c then tt else ff

-- Finding Compositionality
--
instance Eval Logic (KF String) where
    evalAlg (Not_ (KF b)) = KF $ "(not " ++ b ++ ")"
    evalAlg (Eq_ (KF p) (KF q)) = KF $ "(" ++ p ++ " == " ++ q ++ ")"

instance Eval If (KF String) where
    evalAlg (If_ (KF c) (KF tt) (KF ff)) = KF $ "(if " ++ c ++ " then " ++ tt ++ " else " ++ ff

render :: (Eval dom (KF String), HFunctor dom) => Term dom a -> String
render = runK

---------------------------------------
-- Compiler??
--
---------------------------------------
-- Listing 4
type VarId = Integer
type ResultLoc = VarId
type Program = [String]
type CodeMonad = WriterT Program (State VarId)
type CodeGen = ResultLoc -> CodeMonad ()

freshVar :: CodeMonad VarId
freshVar = do
    v <- get
    put (v+1)
    return v

var :: VarId -> String
var v = "v" ++ show v

(=:=) :: VarId -> String -> String
v =:= expr = var v ++ " = " ++ expr

indent :: Program -> Program
indent = map ("  " ++)

-- Listing 5
--
compileArgs :: (NUM < expr, If < expr, Logic < expr) => Term expr :-> KF CodeGen
compileArgs t = KF $ \loc ->
    case t of
        ONum n -> tell [loc =:= (show n)]
        OAdd n m -> do
            [v1,v2] <- replicateM 2 freshVar
            (unKF (compileArgs n) v1)
            (unKF (compileArgs m) v2)
            tell [loc =:= ("(" ++ (var v1) ++ " + " ++ (var v2) ++ ")")]
        OMul n m -> do
            [v1,v2] <- replicateM 2 freshVar
            (unKF (compileArgs n) v1)
            (unKF (compileArgs m) v2)
            tell [loc =:= ("(" ++ (var v1) ++ " * " ++ (var v2) ++ ")")]
        ONot b -> do
            vb <- freshVar
            (unKF (compileArgs b) vb)
            tell [loc =:= ("( not " ++ var vb ++ ")")]
        OEq p q -> do
            [v1,v2] <- replicateM 2 freshVar
            (unKF (compileArgs p) v1)
            (unKF (compileArgs q) v2)
            tell [loc =:= ("(" ++ (var v1) ++ " == " ++ (var v2) ++ ")")]
        OIf c tt ff -> do
            v1 <- freshVar
            (unKF (compileArgs c) v1)
            tProg <- lift $ execWriterT $ (unKF (compileArgs tt) loc)
            fProg <- lift $ execWriterT $ (unKF (compileArgs ff) loc)
            tell $ [unwords ["if", var v1,"then"]]
                    ++ indent tProg
                    ++ ["else"]
                    ++ indent fProg

test4 :: (NUM < expr, If < expr, Logic < expr) => Term expr Int
test4 = cond (eq (num 1) (num 2)) (num 3) test2

compile :: (Term expr :-> KF CodeGen) -> Term expr x -> String
compile com t =   unlines
                $ flip evalState 1
                $ execWriterT
                $ (unKF $ com t) 0

-- λ> putStr $ compile compileArgs (test4 :: Expr Int)
-- v2 = 1
-- v3 = 2
-- v1 = (v2 == v3)
-- if v1 then
--   v0 = 3
-- else
--   v6 = 5
--   v7 = 0
--   v4 = (v6 + v7)
--   v5 = 6
--   v0 = (v4 * v5)
--
--------------------------------------------------------------
-- Implicit and Explicit recursion
--
--
-- fold ya lo tenemos cocinado.
-- instance Eval NUM (KF (Bool) where

equality :: (NUM < dom, Logic < dom, If < dom) => Term dom a -> Term dom b -> Bool
equality (ONum n) (ONum m) = n == m
equality (OAdd n1 n2) (OAdd m1 m2) = and [equality n1 m1, equality n2 m2]
equality (OMul n1 n2) (OMul m1 m2) = and [equality n1 m1, equality n2 m2]
equality (ONot b1) (ONot b2) = equality b1 b2
equality (OEq p1 q1) (OEq p2 q2) = and [equality p1 p2, equality q1 q2]
equality (OIf c1 tt1 ff1) (OIf c2 tt2 ff2) = and [equality c1 c2, equality tt1 tt2, equality ff1 ff2]
equality _ _ = False

-- Pero no tenemos nada modular así...
--
-- num_eqAlg :: HAlg NUM (HAlg NUM (KF Bool))
-- num_eqAlg _ = undefined
--
--
num_eq :: (NUM < dom) => (forall c d. Term dom c -> Term dom d -> Bool) ->  Term dom a -> Term dom b -> Bool
num_eq feq (ONum n) (ONum m) = n == m
num_eq feq (OAdd n1 n2) (OAdd m1 m2) = and [feq n1 m1, feq n2 m2]
num_eq feq (OMul n1 n2) (OMul m1 m2) = and [feq n1 m1, feq n2 m2]
num_eq _ _ _ = False

log_eq :: (Logic < dom) => (forall c d. Term dom c -> Term dom d -> Bool) -> Term dom a -> Term dom b -> Bool
log_eq feq (ONot b1) (ONot b2) = feq b1 b2
log_eq feq (OEq p1 q1) (OEq p2 q2) = and [feq p1 p2, feq q1 q2]
log_eq _ _ _ = False

if_eq :: (If < dom) => (forall c d. Term dom c -> Term dom d -> Bool) -> Term dom a -> Term dom b -> Bool
if_eq feq (OIf c1 tt1 ff1) (OIf c2 tt2 ff2) = and [feq c1 c2, feq tt1 tt2, feq ff1 ff2]
if_eq _ _ _ = False

eqNP :: (NUM < dom, Logic < dom, If < dom) => Term dom a -> Term dom b -> Bool
eqNP t1 t2 = or [num_eq eqNP t1 t2, log_eq eqNP t1 t2, if_eq eqNP t1 t2]

-- class Equality expr where
--     equal :: Term expr a -> Term expr b -> Bool
--
-- instance (Equality dom1, Equality dom2) => Equality (dom1 + dom2) where
--     equal t1 t2 = or [equal t1 t2, equal t1 t2]
--     ver bien cómo indicarle que equal es ^^

-- instance Equality NUM where
--  Esto ^^^^ es lo mismo que hacer el or allá en eqNP

-- Axelsson tiene algo que nosotros no, que es la "app spine", es decir la
-- estructura aplicativa, como el grafo aplicativo de la computación, es decir
-- tiene a mano @ f x. Esto lo lleva a hacer clases para todo, e instanciar que
-- pasa con la aplicación en cada caso.
--

everywhere :: (HFunctor dom) => (Term dom :-> Term dom) -> Term dom :-> Term dom
everywhere = trans

-- *FunTactic
-- λ> render (test3 :: Expr Bool)
-- "(((5 + 0) * 6) == ((5 + 0) * 6))"
-- *FunTactic
-- λ> render $ everywhere optAddTop (test3 :: Expr Bool)
-- "((5 * 6) == (5 * 6))"
--

partialPM :: (HFunctor dom, NUM < dom, If < dom) => Term dom :-> Term dom
partialPM (OIf c ff tt) = OIf (partialPM c) ff tt
partialPM (OAdd e (ONum 0)) = partialPM e
partialPM (Op t) = Op $ hfmap partialPM t

--
-- VS?!!!
--
partialOpt :: (HFunctor dom, NUM < dom, If < dom) => Term dom :-> Term dom
partialOpt t = cata alg t
    where
        alg :: (HFunctor dom, If < dom, NUM < dom) => dom (Term dom) :-> Term dom
        alg (If c tt ff) = OIf (partialOpt c) tt ff
        alg (Add e (ONum 0)) = partialOpt e
        alg t' = Op $ hfmap partialOpt t'

test5 :: Expr Int
test5 = test4 `add` (num 0)

-- queryTastic :: (HFunctor dom) => (dom (Term dom) :-> c) -> Term dom :-> c
-- queryTastic f t = cata _ t
--

---- Mutually Recursie Types
--

type Var = String

data MExpr a where
    MNum :: Int -> MExpr Int
    MAdd :: MExpr Int -> MExpr Int -> MExpr Int
    Exec :: Var -> Stmt -> MExpr a

data Stmt where
    Assign :: Var -> MExpr a -> Stmt
    Seq :: Stmt -> Stmt -> Stmt

data E a
data S

data ExprDom e a where
    NumSym :: Int -> ExprDom e Int
    AddSym :: e Int -> e Int -> ExprDom e Int
    ExecSym :: e Var -> e (StmtDom e b) -> ExprDom e a

data StmtDom e a where
    AssignSym :: e Var -> e (ExprDom e a) -> StmtDom e a
    SeqSym :: e a -> e a -> StmtDom e a
