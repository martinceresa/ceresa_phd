{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE ViewPatterns          #-}

module Expr where

import           AST

--------------------------------------------------
-- An example signature
--------------------------------------------------

--------------------------------------------------------------------------------
data Val e l where
  Const_  :: Int -> Val e Int
  Pair_   :: e s -> e t -> Val e (s,t)

data Oper e l where
  Mult_  ::  e Int ->  e Int -> Oper e Int
  Fst_   ::  e (s, t) -> Oper e s
  Snd_   ::  e (s, t) -> Oper e t

type Sig = Oper + Val

instance HFunctor Val where
  hfmap f (Const_ x)  = Const_ x
  hfmap f (Pair_ p q) = Pair_ (f p) (f q)

instance HFunctor Oper where
    hfmap f (Mult_ a b) = Mult_ (f a) (f b)
    hfmap f (Fst_ pr)   = Fst_ $ f pr
    hfmap f (Snd_ pr)   = Snd_ $ f pr


-- pairast :: (Functor e) => (e ** e) (s,t) -> Val e (s,t)
-- pairast (fl :**: fr) = Pair_ (fmap fst fl) (fmap snd fr)

-- mag_fun :: (HApplicative f) => f q s -> f q t -> f (Val q) (s,t)
-- mag_fun fs ft = undefined

-- instance HTraversable Val where
--   htrav _ (Const_ x)  = hpure $ Const_ x
-- --  Pair l r = Pair <$> f l <*> f r
-- --  ============= Falta poliformismo?
--   htrav f (Pair_ l r) = mag_fun (f l) (f r)

-- -- Simplifying pattern use

-- pair' :: (e *^* e) s t -> Val e (s,t)
-- pair' (l :*^*: r) = Pair_ l r

-- pair'' :: (Functor e) => (e ** e) (s,t) -> Val e (s,t)
-- pair'' a = pair' (joda a fst snd)


-- --- ==============================================================

pattern Const n <- (proj -> Just (Const_ n))
     where Const n = inj (Const_ n)

pattern Pair p q <- (proj -> Just (Pair_ p q))
     where Pair p q = inj (Pair_ p q)

pattern Mult n m <- (proj -> Just (Mult_ n m))
     where Mult n m = inj (Mult_ n m)

pattern Fst e <- (proj -> Just (Fst_ e))
     where Fst e = inj (Fst_ e)

pattern Snd e <- (proj -> Just (Snd_ e))
     where Snd e = inj (Snd_ e)

pair p q = Op (Pair p q)
i n = Op (Const n)
mult p q = Op (Mult p q)

--------------------------------------------------
--------------------------------------------------

instance Eval Val Id where
    evalAlg (Const_ i)           = Id i
    evalAlg (Pair (Id l) (Id r)) = Id (l,r)

instance Eval Oper Id where
  evalAlg (Mult (Id n) (Id m)) = Id (n * m)
  evalAlg (Fst (Id x))         = Id (fst x)
  evalAlg (Snd (Id x))         = Id (snd x)

run :: Term Sig a -> a
run = runId . eval

pattern OConst n <- Op (proj -> Just (Const_ n))

--------------------------------------------------

-- Está claro que acá hay algo más inteligente para hacer...

instance forall a. Show (Term Val a) where
  show (Op (Const n))  = show n
  show (Op (Pair l r)) = '(' : show l ++ " , " ++ show r ++ ")"

instance forall a. Show (Term Oper a) where
  show (Op (Mult p q)) = '(' : show p ++ " * " ++ show q ++ ")"
  show (Op (Fst p))    = "(fst " ++ show p ++ ")"
  show (Op (Snd p))    = "(snd " ++ show p ++ ")"

-- instance (Show (Term h a), Show (Term g a)) => Show (Term (h + g) a) where
--   show ()

instance forall a. Show (Term (Oper + Val) a) where
  show (Op (Const n))  = show n
  show (Op (Pair l r)) = '(' : show l ++ " , " ++ show r ++ ")"
  show (Op (Mult p q)) = '(' : show p ++ " * " ++ show q ++ ")"
  show (Op (Fst p))    = "(fst " ++ show p ++ ")"
  show (Op (Snd p))    = "(snd " ++ show p ++ ")"

p1 p = Op (Fst p)
p2 p = Op (Snd p)

test :: (Val < h, Oper < h) => Term h (Int, Int)
test = pair (mult (i 3) (p2 (pair (i 0) (i 4)))) (i 5)

test2 :: Term Sig Int
test2 = p1 test

testC :: Term Sig Int
testC = i 2

plus_one :: Term Sig :-> Term Sig
plus_one (Op (Const n)) = Op $ Const (n+1)
plus_one x              = x
