{-# LANGUAGE RankNTypes,
             TypeOperators,
             KindSignatures,
             MultiParamTypeClasses,
             FlexibleInstances,
             FlexibleContexts,
             ViewPatterns,
             PatternSynonyms,
             DataKinds,
             GADTs
 #-}

module Annotations where

import Expr
import AST

stripA :: (HFunctor f) => Term (f & ann) :-> Term f
stripA = cata ( Op . remA)

liftA :: (HFunctor f) => (Term f :-> t) -> Term (f & ann) :-> t
liftA f = f . stripA

data Pos = Pos Int Int
  deriving (Eq, Show)

type SigP = (Oper & Pos) + (Val & Pos)

test1 :: Term (Val & Pos) Int
test1 = Op ( Const 1 :&: Pos 1 1)

-- Obj:  Term (Sig' & Pos) -> Term (Sig & Pos)

annotate :: (HFunctor h) => p -> HFree h f :-> HFree (h & p) f
annotate a (Return x) = Return x
annotate a (Op c) = Op $ ( hfmap (annotate a) c) :&: a

chgAnn :: (HFunctor h) => (p -> p') -> HFree (h & p) f :-> HFree (h & p') f
chgAnn fan = fold hreturn (\(t :&: ann) -> Op (t :&: (fan ann)))

mAlg :: (HFunctor h')
     => (forall f. h f :-> HFree h' f)
     -> HAlg (h & ann) (HFree (h' & ann) f)
mAlg fa (t :&: ann) = hjoin (annotate ann (fa t))

liftAnn :: (HFunctor h, HFunctor h')
         => (forall f. h f :-> HFree h' f)
         -> HFree (h & ann) f :-> HFree (h' & ann) f
liftAnn fa = fold hreturn (mAlg fa)
