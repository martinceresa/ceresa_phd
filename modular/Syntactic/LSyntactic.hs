{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE PatternSynonyms  #-}
{-# LANGUAGE TypeOperators    #-}
{-# LANGUAGE ViewPatterns     #-}

module LSyntactic where

import           Modular.High.DTC
import           Modular.High.FreeMonad
import           Modular.High.GenTerm
import           Modular.High.HFunctor

data NUM e a where
    Num_ :: Int -> NUM e Int
    Mul_ :: e Int -> e Int -> NUM e Int
    Add_ :: e Int -> e Int -> NUM e Int

data Logic e a where
    Not_ :: e Bool -> Logic e Bool
    Eq_ :: Eq a => e a -> e a -> Logic e Bool

data If e a where
    If_ :: e Bool -> e a -> e a -> If e a


-- HFunctors
--
instance HFunctor NUM where
    hfmap _ (Num_ n) = Num_ n
    hfmap f (Mul_ n m) = Mul_ (f n ) (f m)
    hfmap f (Add_ n m) = Add_ (f n ) (f m)

instance HFunctor Logic where
    hfmap f (Not_ b) = Not_ (f b)
    hfmap f (Eq_ m n) = Eq_ (f m) (f n)

instance HFunctor If where
    hfmap f (If_ c tt ff) = If_ (f c) (f tt) (f ff)

-- Patterns
--
-- NUM
pattern Num n <- (proj -> Just (Num_ n))
    where Num n = inj $ Num_ n

pattern ONum n <- Op (proj -> Just (Num_ n))
    where ONum n = Op $ inj $ Num_ n

pattern Add n m <- (proj -> Just (Add_ n m))
    where Add n m = inj $ Add_ n m

pattern OAdd n m <- Op (proj -> Just (Add_ n m))
    where OAdd n m = Op $ inj $ Add_ n m

pattern Mul n m <- (proj -> Just (Mul_ n m))
    where Mul n m = inj $ Mul_ n m

pattern OMul n m <- Op (proj -> Just (Mul_ n m))
    where OMul n m = Op $ inj $ Mul_ n m

-- Logic
pattern Not b <- (proj -> Just (Not_ b))
    where Not b = inj $ Not_ b

pattern ONot n <- Op (proj -> Just (Not_ n))
    where ONot n = Op $ inj $ Not_ n

pattern Eq n m <- (proj -> Just (Eq_ n m))
    where Eq n m = inj $ Eq_ n m

pattern OEq n m <- Op (proj -> Just (Eq_ n m))
    where OEq n m = Op $ inj $ Eq_ n m

-- If
pattern If c tt ff <- (proj -> Just (If_ c tt ff))
    where If c tt ff = inj $ If_ c tt ff

pattern OIf c tt ff <- Op (proj -> Just (If_ c tt ff))
    where OIf c tt ff = Op $ inj $ If_ c tt ff

---- Builders
--

type Expr3 a = Term NUM a

num3 :: Int -> Expr3 Int
num3 = Op . Num

add3 :: Expr3 Int -> Expr3 Int -> Expr3 Int
add3 n m = Op $ Add n m

mul3 :: Expr3 Int -> Expr3 Int -> Expr3 Int
mul3 n m = Op $ Mul n m


--
type Expr a = Term (NUM + (If + Logic)) a

not1 :: Expr Bool -> Expr Bool
not1 = Op . Not

eq1 :: Eq a => Expr a -> Expr a -> Expr Bool
eq1 p q = Op $ Eq p q

cond1 :: Expr Bool -> Expr a -> Expr a -> Expr a
cond1 c t f = Op $ If c t f
---
--

num :: (NUM < h) => Int -> HFree h e Int
num = Op . Num

add :: (NUM < h) => HFree h e Int -> HFree h e Int -> HFree h e Int
add n m = Op $ Add n m

mul :: (NUM < h) => HFree h e Int -> HFree h e Int -> HFree h e Int
mul n m = Op $ Mul n m

not :: (Logic < h) => HFree h e Bool -> HFree h e Bool
not = Op . Not

eq :: (Eq a, Logic < h) => HFree h e a -> HFree h e a -> HFree h e Bool
eq p q = Op $ Eq p q

cond :: (If < h) => HFree h e Bool -> HFree h e a -> HFree h e a -> HFree h e a
cond c t f = Op $ If c t f
