module Modular.Low.Fun where

import           Data.Traversable

import Modular.Low.FreeMonad
import Modular.Low.DTC

fold :: Functor f => (a -> b) -> (f b -> b) -> Free f a -> b
fold f h (Return x) = f x
fold f h (Op t) = h $ fmap (fold f h) t

rec :: Functor f => (a -> b) -> (f (b, Free f a ) -> b) -> Free f a -> b
rec f h (Return x) =  f x
rec f h (Op t) = h $ fmap (\l -> (rec f h l, l)) t

subs :: Traversable f => Free f a -> [Free f a]
subs t = rec
            (\x -> [Return x])
            (\l -> (Op $ fmap snd l) : (foldMap id $ fmap fst l)) t

query :: Traversable f => (Free f a -> r) -> (r -> r -> r) ->  Free f a -> r
query q c t = foldl1 c $ map q $ subs t

cata :: Functor f => (f b -> b) -> Free f a -> b
cata _ (Return x) = undefined
cata h (Op t) = h $ fmap (cata h) t

-- everywhere
trans :: Functor f => (Free f a -> Free f a) -> Free f a -> Free f a
trans f = cata (f . Op)
