{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Modular.Low.DTC where

data (f + g) a = Inl (f a) | Inr (g a)

class sub < sup where
    inj :: sub a -> sup a
    proj :: sup a -> Maybe (sub a)

instance f < f where
    inj = id
    proj = Just

instance f < (f + g) where
    inj = Inl
    proj (Inl x) = Just x
    proj _ = Nothing

instance {-# OVERLAPPABLE #-} (f < h) => f < (g + h) where
  inj           = Inr . inj
  proj (Inr x)  = proj x
  proj _        = Nothing

