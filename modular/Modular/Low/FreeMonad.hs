{-# LANGUAGE GADTs                 #-}

module Modular.Low.FreeMonad where

data Free f a where
    Return :: a -> Free f a
    Op :: f (Free f a) -> Free f a

instance Functor f => Functor (Free f) where
    fmap f (Return a) = Return $ f a
    fmap f (Op t) = Op $ fmap (fmap f) t

instance Functor f => Applicative (Free f) where
    pure = Return
    (Return f) <*> t = fmap f t
    (Op f) <*> x = Op $ fmap ( <*> x)  f

instance Functor f => Monad (Free f) where
    return = Return
    (Return x) >>= f =  f x
    (Op g) >>= f = Op $ fmap ( >>= f) g

instance Foldable f => Foldable (Free f) where
    foldMap f (Return a) = f a
    foldMap f (Op t) = foldMap (foldMap f) t

instance Traversable f => Traversable (Free f) where
    traverse f (Return x) = Return <$> f x
    traverse f (Op t) = Op <$> traverse (traverse f) t
