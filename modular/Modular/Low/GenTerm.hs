{-# Language TypeOperators #-}

module Modular.Low.GenTerm where

import Modular.Low.FreeMonad
import Modular.Low.DTC

data Void

type Term f = Free f Void

unTerm :: Term f -> f (Term f)
unTerm (Op t) = t
-- unTerm Return ? , obviously cannot be...

inject :: (g < f) => g (Free f a) -> Free f a
inject = Op . inj

project :: (g < f) => Free f a -> Maybe (g (Free f a))
project (Return _) = Nothing
project (Op t) = proj t
