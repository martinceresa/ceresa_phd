{-# Language TypeOperators #-}
{-# Language StandaloneDeriving #-}
{-# Language KindSignatures #-}
{-# Language PolyKinds #-}
module Modular.High.Functor.Constant where

import Modular.High.HFunctor
import Modular.High.Functor.CoProd

--------------------------------------------------
-- Constant Prod. of HFunctor
--------------------------------------------------

-- data (h & c) (f :: * -> *) x = h f x :&: c

type (h & c) f x = (h * (K c)) f x
newtype K c (f :: u -> *) (a :: u) = K c -- Constant f

newtype KF (f :: *) (r :: u) = KF {unKF :: f}

instance Functor (KF f) where
  fmap _ (KF f) = KF f

instance Monoid f => Applicative (KF f) where
  pure = const (KF mempty)
  (KF f) <*> (KF x) = KF $ mappend f x

deriving instance Show f => Show (KF f u)

remA :: (h & c) f :-> h f
remA (c :*: _) = c

newtype KFE a b = KFE {unKFE :: a -> a}

newtype KFEDual a b = KFED {unKFED :: a -> a}

-- constant functor.
instance Functor (KFE a) where
    fmap _ (KFE f ) = KFE f

-- constant functor.
instance Functor (KFEDual a) where
    fmap _ (KFED f ) = KFED f

-- Monoid m => Applicative (KF m)
instance Applicative (KFE a) where
    pure _ = KFE id
    (KFE x) <*> (KFE y) = KFE $ x . y

-- Monoid m => Applicative (KF (Dual m))
instance Applicative (KFEDual a) where
    pure _ = KFED id
    (KFED x) <*> (KFED y) = KFED $ y . x
