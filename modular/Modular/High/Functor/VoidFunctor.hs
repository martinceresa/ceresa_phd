{-# Language TypeOperators #-}

module Modular.High.HFunctor.VoidFunctor where

import Modular.High.HFunctor

--Void Functor
data VoidF a

empty :: VoidF :-> f
empty = undefined
