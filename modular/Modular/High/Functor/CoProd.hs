{-# Language TypeOperators #-}
{-# Language KindSignatures #-}
{-# Language PolyKinds #-}
{-# Language RankNTypes #-}

module Modular.High.Functor.CoProd where

import Modular.High.HFunctor

--------------------------------------------------
-- Coproducts of HFunctors
--------------------------------------------------

data (h + k) (f :: u -> *) (x :: u) = Inl (h f x) | Inr (k f x)

caseAlg :: HAlg h f -> HAlg k f -> HAlg (h + k) f
caseAlg h _ (Inl x) = h x
caseAlg _ k (Inr x) = k x

instance (HFunctor h, HFunctor k) => HFunctor (h + k) where
  hfmap f (Inl x) = Inl (hfmap f x)
  hfmap f (Inr x) = Inr (hfmap f x)
