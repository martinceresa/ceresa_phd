module Modular.High.Functor.IdFunctor where

import           Modular.High.HFunctor

newtype HIdF f a = HIdF (f a)

newtype Id a = Id {runId :: a}

instance HFunctor HIdF where
  hfmap f (HIdF x) = HIdF $ f x
