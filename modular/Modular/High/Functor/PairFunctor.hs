{-# Language KindSignatures #-}
{-# Language TypeOperators #-}

module Modular.High.HFunctor.PairFunctor where

import Modular.High.HFunctor

--------------------------------------------------
-- Fun prod.
--------------------------------------------------
data (h || f) x = (h x) :|: (f x)

fstF :: h || f :-> h
fstF (x :|: _) = x

sndF :: h || f :-> f
sndF (_ :|: y) = y

data (h * g) (f :: * -> *) (x :: *) = h f x :*: g f x

instance (HFunctor h, HFunctor g) => HFunctor (h * g) where
  hfmap f (l :*: r) = (hfmap f l) :*: (hfmap f r)

