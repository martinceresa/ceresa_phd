{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}

module Modular.High.FreeMonad where

import           Modular.High.DTC
import           Modular.High.HFunctor

-- type TheType (u -> *) -> u -> *

--------------------------------------------------
-- Free Monad
--------------------------------------------------

data HFree :: ((u -> *) -> u -> *) -> (u -> *) -> u -> * where
     Return :: f a -> HFree h f a
     Op     :: h (HFree h f) a -> HFree h f a

class HMonad (m :: (u -> *) -> u -> *)  where
  hreturn :: f :-> m f
  hextend :: (f :-> m g) -> m f :-> m g

instance (HFunctor h) => HMonad (HFree h) where
  hreturn = Return
  hextend k (Return f) = k f
  hextend k (Op t)     = Op (hfmap (hextend k) t)

instance (HFunctor h) => HFunctor (HFree h) where
  hfmap f = hextend $ hreturn . f

inject :: (k < h, HFunctor h) => k f :-> HFree h f
inject = Op . hfmap Return . inj

project :: (k < h) => HFree h f x -> Maybe (k (HFree h f) x)
project (Op t) = proj t
project _      = Nothing

fold :: HFunctor h => (f :-> g) -> HAlg h g -> HFree h f :-> g
fold gen _ (Return x)   = gen x
fold gen alg (Op t)     = alg (hfmap (fold gen alg) t)

-- unApp :: KUniv App ks e -> ks e
-- unApp (App k) = k

-- foldG :: (HHFunctor f) => (forall x. e x -> KUniv ks' e' x)
--       -> (forall y. f ks' e' y -> KUniv ks' e' y) --
--       -> (forall z. KUniv App (HFree (f App) e) z -> KUniv ks' e' z)
-- foldG gen alg (App (Return x)) = gen x
-- foldG gen alg (App (Op t)) = alg $ hhfmap (foldG gen alg) t

-- foldK :: (HHFunctor f) => (forall x. e x -> v)
--       -> (forall y. f (Id v) e y -> v)
--       -> (forall z. HFree (f App) e z -> v)
-- foldK gen alg t = unK1 $ foldG (K1 . gen) (K1 . alg) (App t)

--------------------------------------------------
-- Helpers
--------------------------------------------------

hjoin :: (HMonad m) => m (m h) :-> m h
hjoin = hextend id
