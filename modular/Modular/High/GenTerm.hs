{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}

module Modular.High.GenTerm where

import           Modular.High.DTC
import           Modular.High.Evaluation
import           Modular.High.FreeMonad
import           Modular.High.HFunctor

-- a term has no Returns
type Term sig = HFree sig VoidF

cata :: HFunctor h => HAlg h f -> HFree h VoidF :-> f
cata alg = fold empty alg

eval :: (HFunctor h, Eval h v) => HFree h VoidF :-> v
eval = cata evalAlg

run :: (HFunctor h, Eval h Id) => HFree h VoidF a -> a
run = runId . eval

runK :: (HFunctor h, Eval h (KF b)) => HFree h VoidF a -> b
runK = unKF . eval

instance {-# OVERLAPPABLE #-} (Show a, HFunctor dom, Eval dom (KF String)) => Show (Term dom a) where
    show = unKF . eval

-- recursion

-- Se llama trans porque B&H la llama así.
trans :: HFunctor f => (Term f :-> Term f) -> Term f :-> Term f
trans f =  cata (f . Op)

foldTerm :: HFunctor dom => (forall x. g x) -> HAlg dom g -> Term dom :-> g
foldTerm b = fold (const b)

----- Query
--
-- Playground
query' :: HFunctor dom => a -> (dom (KF a) :-> KF a) -> Term dom x -> a
query' b f t = unKF $ foldTerm (KF b) f t

toFP :: KF r a -> (forall p. KF r p)
toFP (KF x) = KF x

queryF :: (KFoldable dom, HFunctor dom) => (Term dom :-> KF r) -> (r -> r -> r) -> Term dom x -> r
queryF b c t = unKF $ fold undefined (kfold c (toFP $ b t)) t
--

-- queryM :: HTraversable dom => (dom ? -> r) -> (r -> r -> r) -> Term dom x -> r
-- queryM == ?? escribir query utilizando traverse de HFree.

queryRKFE :: HTraversable dom =>
        (forall b. HFree dom VoidF b -> r)
    ->  (r -> r -> r)
    -> HFree dom VoidF a -> r
queryRKFE q c t'@(Op t) = unKFE (htraverse (\t -> KFE $ \r -> c (queryRKFE q c t) r) t) (q t')

queryLKFE :: HTraversable dom =>
        (forall b. HFree dom VoidF b -> r)
    ->  (r -> r -> r)
    -> HFree dom VoidF a -> r
queryLKFE q c t'@(Op t) = unKFED (htraverse (\x -> KFED $ \y -> c y (queryLKFE q c x)) t) (q t')

-- TermHom h h' =  forall f. h f :-> HFree h' f
termHom :: (HFunctor h, HFunctor h') => (forall f. h f :-> HFree h' f) -> Term h :-> Term h'
termHom hom = cata $ hjoin . hom

-- rec :: HFunctor h => (f :-> g) -> ( h (PairF g (HFree h f)) :-> g) -> HFree h f :-> g
-- rec f _ (Return x) = f x
-- rec f h (Op t) = h $ hfmap (\l -> PairF (rec f h l) l ) t
rec :: HFunctor h => (f :-> g)
      -> (forall k . (k :-> g) -> (k :-> HFree h f) -> h k :-> g)
      -> HFree h f :-> g
rec f _ (Return x) = f x
rec f h (Op t) = h (rec f h) id t

data HFreeList h f a = HFreeList { rHFList :: [HFree h f a] }
-- data CCompF f h g a = CCF (f (HFree h g a ))

instance Monoid (HFreeList h f a) where
  mempty = HFreeList []
  mappend (HFreeList xs) (HFreeList ys) = HFreeList $ xs ++ ys

(<:>) :: HFree h f a -> HFreeList h f a -> HFreeList h f a
x <:> (HFreeList xs) = HFreeList $ x : xs

-- terminar de Implementar

-- subs :: (HFunctor h, HTraversable h) => HFree h f a -> HFreeList h f a
-- subs = rec
--   (\x -> HFreeList [Return x])
--   (\f g t -> (Op $ hfmap g t) <:> (foldMapDef _ $ hfmap f t))
