{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}

module Modular.High.Evaluation where

import           Modular.High.DTC
import           Modular.High.FreeMonad
import           Modular.High.HFunctor


--------------------------------------------------
-- Evaluation
--------------------------------------------------

class Eval (h :: (u -> *) -> u -> *) (v :: u -> *) where
  evalAlg :: forall x. h v x -> v x

instance (Eval h v, Eval g v) => Eval (h + g) v where
  evalAlg = caseAlg evalAlg evalAlg

desug :: (HFunctor h, HFunctor h',
          Eval h (HFree h' f))
      => HFree (h + h') f :-> HFree h' f
desug = fold hreturn (caseAlg evalAlg Op)

--------------------------------------------------
-- Evaluation + KF
--------------------------------------------------

class EvalK (h :: (u -> *) -> u -> *) (v :: *) where
  evalKAlg :: forall x. h (KF v) x -> v

-- data HK (h :: (u -> *) -> u -> *) v u = HK (h (KF v) u)

-- class EvalKH (h :: * -> u -> *) (v :: *) where
--   evalKH :: forall x. h v x -> v

-- class HyperC (h :: * -> u -> *) (h' :: (u -> *) -> u -> * ) where
--   hyper :: forall v. h v :-> h' (KF v)

-- (h :: (u -> *) -> u -> *) -> (h' :: * -> u -> *)

-- class DrownerC (h' :: (u -> *) -> u -> *) (h :: * -> u -> *)  where
--   down :: forall v. h' (KF v) :-> h v

-- instance (EvalKH h v , DrownerC g h) => Eval g (KF v) where
--   evalAlg _ = _ -- KF . evalKH . down

-- reduce :: (HFunctor h) => HAlg h (KF a) -> HFree h (KF a) x -> a
-- reduce alg (Return x) = unKF x
-- reduce alg (Op t) = unKF $ alg (hfmap (reduce alg) t)

-- instance {-# OVERLAPPABLE #-} EvalK h v => Eval h (KF v) where
--   evalAlg = KF . evalKAlg

--------------------------------------------------
-- Foldable
--------------------------------------------------

class KFoldable h where
  kfold :: (r -> r -> r) -> (forall p. KF r p) -> HAlg h (KF r)

--------------------------------------------------
-- Traversable
--------------------------------------------------

class HTraversable t where
    htraverse :: Applicative f => (forall y. a y -> f (b y)) -> t a x -> f (t b x)

foldMapDef :: (Monoid m, HTraversable h) => (forall y. g y -> m) -> h g x -> m
foldMapDef f = unKF . htraverse (KF . f)

instance (HTraversable f, HTraversable g) => HTraversable (f + g) where
    htraverse f (Inl t) = Inl <$> htraverse f t
    htraverse f (Inr t) = Inr <$> htraverse f t

-- instance Traversable f  => Traversable (HFree f VoidF)

instance HTraversable f  => HTraversable (HFree f) where
    htraverse f (Return fa) = Return <$> f fa
    htraverse f (Op t) = Op <$> htraverse (htraverse f) t

-- data (f :=> g) a = V (f a -> f b)
-- class (HFunctor m) => HApplicative (m :: (* -> *) -> * -> *) where
--   hpure :: f :-> m f
--   happ  :: m (f :=> g) a -> m f a -> m g a
--
-- data (f :=> g) a = V (forall b. (a -> b) -> f b -> g b)
--
-- ($=>) :: (f :=> g) a -> f a -> g a
-- (V h) $=> fa = h id fa
--
-- uncurryF :: (h :-> (f :=> g)) -> (h ** f :-> g)
-- uncurryF t (h :**: f) = (\(V fg) -> fg id f) (t h)
--
-- curryF :: (Functor h) => ((h ** f) :-> g) -> h :-> (f :=> g)
-- curryF t h = V (\t' fb -> t ((fmap t' h) :**: fb))
--
-- data (f ** g) a = f a :**: g a
--
-- instance (Functor f, Functor g) => Functor (f ** g) where
--   fmap f (l :**: r) = (fmap f l :**: fmap f r)
--
-- pi1 :: (f ** g) :-> f
-- pi1 (l :**: _ ) = l
--
-- pi2 :: (f ** g) :-> g
-- pi2 (_ :**: r ) = r
--
-- data (f *^* g) a b = f a :*^*: g b
--
-- joda :: (Functor f, Functor g) => (f ** g) a -> (a -> c) -> (a -> d) -> (f *^* g) c d
-- joda (l :**: r) h1 h2 = fmap h1 l :*^*: fmap h2 r
--
-- cannotjoda :: (Functor f, Functor g) => (f *^* g) b c -> (b -> a) -> (c -> a) -> (f ** g) a
-- cannotjoda (l :*^*: r) p1 p2 = (fmap p1 l :**:  fmap p2 r)
--
-- class  HTraversable t where
--   htrav :: (HApplicative f) => (p :-> f q) -> t p :-> f (t q)
--   hdist :: (HApplicative f) => t (f q) :-> f (t q)
--   hdist = htrav id
--
-- instance (HTraversable h, HTraversable h') => HTraversable (h + h') where
--   htrav f (Inl x) = hfmap Inl $ htrav f x
--   htrav f (Inr x) = hfmap Inr $ htrav f x
--
--
-- -- Repasar, creo que esto no está bien! Hablar con Mauro!
-- class HFunctor f => HMonoidal f where
--   hunit :: f UnitF a
--   hst  :: f h a -> f h' a -> f (h ** h') a
--
--
-- instance HMonoidal HIdF where
--   hunit = HIdF UnitF
--   hst (HIdF h) (HIdF h') = HIdF ( h :**: h')
--
-- instance HApplicative HIdF where
--   hpure x = HIdF x
--   happ (HIdF (V h)) (HIdF x) = HIdF $ h id x
--
-- instance HTraversable HIdF where
--   htrav f (HIdF x) = hfmap HIdF $ f x
--   hdist (HIdF fq) = hfmap HIdF fq
