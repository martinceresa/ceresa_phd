{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}

module Modular.High.DTC where

import           Modular.High.Functor.CoProd
import           Modular.High.HFunctor

-- --------------------------------------------------
-- DTC for higher-order functors
--------------------------------------------------

class (sub :: (u -> *) -> u -> *) < sup where
   inj   :: sub f :-> sup f
   proj  :: sup f x -> Maybe (sub f x)

instance (f < f) where
  inj   = id
  proj  = Just

instance f < (f + g) where
  inj           = Inl
  proj (Inl x)  = Just x
  proj _        = Nothing


instance {-# OVERLAPPABLE #-} (f < h) => f < (g + h) where
  inj           = Inr . inj
  proj (Inr x)  = proj x
  proj _        = Nothing
