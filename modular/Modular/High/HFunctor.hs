{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module Modular.High.HFunctor where

{- First attempt
  with index in *
  One could restrict this using DataKinds
  to a specific set of types.
-}

--------------------------------------------------
-- Higher-Order Functors
-- (these are higher-order functors from |*| -> *)
-- as we are not asking that instances map functors to functors
--------------------------------------------------
type a :-> b = forall x. a x -> b x

class HFunctor (h :: (i -> *) -> o -> *) where
  hfmap :: (f :-> g) -> h f :-> h g

-- AtKey!
data (:=) :: * -> u -> u -> * where
  V :: a -> (a := k) k

-- class HHFunctor f  where
--   hhfmap :: (KUniv h e :-> KUniv h' e') -> f h e :-> f h' e'

-- Alg def

type HAlg (f :: (u -> *) -> u -> *) (a :: u -> *) = f a :-> a

class SFunctor (ff :: (u -> *) -> *) where
    sfmap :: (f :-> g) -> ff f -> ff g

---
-----

-- type family KUniv ac (f :: u -> *) (t :: u) where
--   KUniv App f t = f t
--   KUniv (Id c) f t = c
--
-- data App
-- data K1

-- newtype instance KUniv App f t = App (f t)
-- newtype instance KUniv (Id c) f t = K1{ unK1 :: c}

-- instance Functor f => Functor (KUniv App f) where
--   fmap f (App l) = App (fmap f l)

-- instance Functor (KUniv (Id c) f) where
--   fmap f (K1 c) = K1 c

