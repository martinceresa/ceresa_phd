{-# LANGUAGE TupleSections #-}
module CostLazy where

import           Control.Arrow
import           LazyEval
import           Prelude       hiding (lookup)
-- A lo guacho

newtype W w m a = W {unW :: m (a, w)}

instance Functor m => Functor (W w m) where
  fmap f = W . fmap (first f) . unW

primTicks :: (Monoid w, Functor m) => w -> m (a , w) -> m (a , w)
primTicks c = fmap (second (mappend c))
ticks :: (Monoid w, Functor m)=> w -> W w m a -> W w m a
ticks c = W . primTicks c . unW

instance Semigroup Int where
  (<>) = (+)
instance Monoid Int where
  mempty = 0
  mappend = (+)

tick :: (Functor m) => W Int m a -> W Int m a
tick = ticks 1

utick :: (Monad m) => W Int m ()
utick = W $ return ((), 1)

instance (Monoid w, Applicative m) => Applicative (W w m) where
  pure = W . pure . (,mempty)
  (W f) <*> (W x) = W $ (\(g,gc)(y,yc) -> (g y, mappend gc yc)) <$> f <*> x
instance (Monoid w, Monad m) => Monad (W w m) where
  return = pure
  (W x) >>= f = W $ x >>= \(y, c) -> primTicks c $ unW (f y)

liftActions :: (Monoid w, Monad m) => m a -> W w m a
liftActions m = W $ fmap (,mempty) m

-- | Count every lookup ?
contCostEva :: Monad m => Var -> W Int (S Heap m) Value
contCostEva s
  = utick >> (liftActions (gets (lookup s)))
  >>= either return costEvaluation

costEvaluation :: Monad m => Term -> W Int (S Heap m) Value
costEvaluation (Ret (Simple s)) = contCostEva s
costEvaluation (Ret v) = return v
costEvaluation (App v t) = case v of
                         Abs s m ->
                           liftActions (modify (insertTerm (s, t)))
                           >> costEvaluation m
                         Simple s ->
                           contCostEva s
                           >>= costEvaluation . flip App t
costEvaluation (Let t s m)
  = liftActions (modify (insertTerm (s, t)))
  >> costEvaluation m
costEvaluation (To t s m)
  = costEvaluation t
  >>= liftActions . modify . insertVal . (s,)
  >> costEvaluation m

evalCost :: Term -> Maybe (Value, Int)
evalCost t = fmap fst $ flip runLazy error $ unW (costEvaluation t)
