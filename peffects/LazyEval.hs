{-# Language TupleSections #-}
module LazyEval where

import           Control.Arrow
import Prelude hiding (lookup, const)

type Var = String
type Heap = Var -> Either Value Term

data Term
  = App Value Term
  | Ret Value
  -- Lazy Let
  | Let Term Var Term
  -- Strict Let
  | To Term Var Term
  deriving Show

data Value
  = Abs Var Term
  | Simple Var
  deriving Show

-- State transformer.
newtype S s m a = S {unS :: s -> m (a , s)}

modify :: Applicative m => (s -> s) -> S s m ()
modify f = S $ \ s -> pure ((), f s)

get :: Applicative m => S s m s
get = S $ \s -> pure (s,s)

gets :: Applicative m => (s -> a) -> S s m a
gets f = S $ \s -> pure (f s , s)

instance Functor m => Functor (S s m) where
  fmap f c = S $ fmap (first f) . unS c
instance Applicative m => Applicative (S s m) where
  pure a = S $ \s -> pure (a,s)
  (<*>) = undefined -- POC, another day
instance Monad m => Monad (S s m) where
  return = pure
  (S c) >>= f = S $ \s -> c s >>= \(v , s') -> unS (f v) s'

liftSt :: Functor m => m a -> S s m a
liftSt c = S $ \s -> fmap (,s) c

-- liftF :: (m a -> m a) -> S s m a -> S s m a
-- liftF f c = S $ \s -> _ (unS c s)
--
updHeap :: (Var, Either Value Term) -> Heap -> Heap
updHeap (nm , t) h = \n -> if nm == n then t else h nm

insertVal :: (Var, Value) -> Heap -> Heap
insertVal = updHeap . (second Left)
insertTerm :: (Var, Term) -> Heap -> Heap
insertTerm = updHeap . (second Right)

lookup :: Var -> Heap -> Either Value Term
lookup v h = h v
---
contEval :: Monad m => Var -> S Heap m Value
contEval s = gets (lookup s)
  >>= either return
             (\ter -> evaluation ter >>= \v' -> modify (insertVal (s,v')) >> return v')
-- Assumptions: There is no repeated variable?
evaluation :: Monad m => Term -> S Heap m Value
evaluation (Ret (Simple s)) = contEval s
evaluation (Ret v) = return v
evaluation (App v t) = case v of
                         Abs s m -> modify (insertTerm (s, t)) >> evaluation m
                         Simple s -> contEval s >>= evaluation . flip App t
evaluation (Let t s m) = modify (insertTerm (s, t)) >> evaluation m
evaluation (To t s m) = evaluation t >>= \v -> modify (insertVal (s,v)) >> evaluation m


runLazy :: Monad m => S Heap m a -> Heap -> m (a , Heap)
runLazy = unS

eval :: Term -> Maybe Value
eval t = fmap fst $ runLazy (evaluation t) (\_ -> undefined)

----------------------------------------
-- Examples
iden :: Var -> Value
iden nm = Abs nm (Ret (Simple nm))

dopIdent :: Term
dopIdent = App (iden "x") (Ret (iden "y"))

delta :: Var -> Value
delta x = Abs x (App (Simple x) (Ret (Simple x)))

omega :: Term
omega = App (delta "w") (Ret $ delta "v")

const :: Value
const = Abs " " (Ret (iden "x"))

lazyTest :: Term
lazyTest = App const omega
