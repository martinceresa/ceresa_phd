{-# Language TupleSections #-}
module ProbEffects where

-- newtype Prob a = P {unP :: a}
newtype P a = D {unD :: [(a, Float)]}
  deriving Show

instance Functor P where
  fmap f = D . map (\(x,p) -> (f x , p)) . unD

instance Applicative P where
  pure x = D [(x,1)]
  fs <*> xs = D [ (f x , fp * xp) | (f,fp) <- unD fs, (x,xp) <- unD xs ]

instance Monad P where
  return = pure
  (D xs) >>= f = D [ (r , fp * xp) | (x, xp) <- xs, (r, fp) <- unD (f x)]

bop :: Float -> a -> a -> P a
bop p x y = D [(x, p), (y,  1 - p)]

-- Clearly I  do not care about performance
uniform :: [a] -> P a
uniform xs = D $ map (, (1 :: Float) / (fromInteger $ toInteger $ length xs)) xs

newtype CostProb a = C { unC :: P (a , Int)}
  deriving Show

instance Functor CostProb where
  fmap f = C . fmap (\(x,c) -> (f x, c)) . unC

instance Applicative CostProb where
  pure = C . pure . (,0)
  (C fs) <*> (C xs) = C $ (fmap (\(f,fc)(x,xc) -> (f x, fc + xc)) fs) <*> xs

tickN :: Int -> CostProb a -> CostProb a
tickN n = C . fmap (\(a,m) -> (a, n + m)) . unC

instance Monad CostProb where
  return = pure
  (C xs) >>= f = tick $ C $ xs >>= \(x, c) -> unC $ tickN c (f x)

tick :: CostProb a -> CostProb a
tick = tickN 1

uniformC :: [a] -> CostProb a
uniformC = C . fmap (,0) . uniform
oplus :: CostProb a -> CostProb a -> CostProb a
oplus x y = x >>= \m -> y >>= \n -> uniformC [m,n]

-- -- OTP!
gen :: CostProb  Bool
gen = tick $ uniformC [True, False]

notB :: CostProb Bool -> CostProb Bool
notB x = x >>= \y -> if y then return False else return True

encode :: Bool -> CostProb Bool -> CostProb Bool
encode x y = if x then notB y else tick y

exp :: CostProb Bool -> CostProb Bool -> CostProb Bool
exp x y = do
  m <- oplus x y
  encode m gen

expFst :: Bool -> Bool -> CostProb Bool
expFst x _ = encode x gen

expSnd :: Bool -> Bool -> CostProb Bool
expSnd _ y = encode y gen

contextDiff :: (Bool -> Bool -> CostProb Bool) -> CostProb Bool
contextDiff c = c True False

