{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module Modular.FreeMonad where

import           Modular.DTC
import           Modular.HFunctor
import           Modular.Functor.IdFunctor

-- type TheType (u -> *) -> u -> *

--------------------------------------------------
-- Free Monad
--------------------------------------------------

data HFree :: ((u -> *) -> u -> *) -> (u -> *) -> u -> * where
     Return :: f a -> HFree h f a
     Op     :: h (HFree h f) a -> HFree h f a

instance (Eq (e t), Eq (h (HFree h e) t)) => Eq (HFree h e t) where
  (Return x) == (Return y) = x == y
  (Op e1) == (Op e2) = e1 == e2
  _ == _ = False

class HMonad (m :: (u -> *) -> u -> *)  where
  hreturn :: f :-> m f
  hextend :: (f :-> m g) -> m f :-> m g

instance (HFunctor h) => HMonad (HFree h) where
  hreturn = Return
  hextend k (Return f) = k f
  hextend k (Op t)     = Op (hfmap (hextend k) t)

instance (HFunctor h) => HFunctor (HFree h) where
  hfmap f = hextend $ hreturn . f

--------------------------------------------------

inject :: (k < h, HFunctor h) => k f :-> HFree h f
inject = Op . hfmap Return . inj

project :: (k < h) => HFree h f x -> Maybe (k (HFree h f) x)
project (Op t) = proj t
project _      = Nothing

fold :: HFunctor h => (f :-> g) -> HAlg h g -> HFree h f :-> g
fold gen _ (Return x)   = gen x
fold gen alg (Op t)     = alg (hfmap (fold gen alg) t)

rec :: HFunctor h
      => (f :-> g)
      -> (forall k .(k :-> g) -> (k :-> HFree h f) -> h k :-> g)
      -> HFree h f :-> g
rec f _ (Return x) = f x
rec f h (Op t) = h (rec f h) id t

-- |Folding over a constant
foldK :: (HFunctor h)
       => (e :=> v)
       -> (h (KF v) :=> v)
       -> HFree h e :=> v
foldK gen alg = unKF . fold (KF . gen) (KF . alg)

--------------------------------------------------
-- Helpers
--------------------------------------------------

hjoin :: (HMonad m) => m (m h) :-> m h
hjoin = hextend id

--------------------------------------------------
-- |Showing HFree
--------------------------------------------------

instance (ShowHFunctor h, HFunctor h) => ShowHFunctor (HFree h)  where
  showHFunctor = foldK showI showHFunctor

instance (ShowI f, ShowHFunctor h, HFunctor h) => ShowI (HFree h f)  where
  showI = foldK showI showHFunctor

instance (ShowI f, ShowHFunctor h, HFunctor h) => Show (HFree h f a)  where
  show = foldK showI showHFunctor
