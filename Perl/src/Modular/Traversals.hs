{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleInstances     #-}

module Modular.Traversals where

import           Modular.Functor.CoProd
import           Modular.FreeMonad
import           Modular.HFunctor
import           Modular.Functor.IdFunctor
import Data.Constraint hiding ((:=>))

--------------------------------------------------
-- Traversable
--------------------------------------------------

class HTraversable t where
    htraverse :: Applicative f => (forall y. a y -> f (b y)) -> t a x -> f (t b x)

class STraversable t where
  straverse :: Applicative f => (forall y. a y -> f (b y)) -> t a -> f (t b)

instance HTraversable h => STraversable ((:=) h t) where
  straverse f (Flip h) = Flip <$> htraverse f h

hmapM :: (Monad m, HTraversable t) => (forall u. a u -> m (b u)) -> t a x -> m (t b x)
hmapM = htraverse

-- Every HTraversable is an HFunctor
tfmap :: HTraversable h => (f :-> g) -> h f :-> h g
tfmap f = runId . htraverse (pure . f)

foldMapDef :: (Monoid m, HTraversable h) => (g :=> m) -> h g :=> m
foldMapDef f = unKF . htraverse (KF . f)


--------------------------------------------------
-- The coproduct of traversals is Traversable
instance (HTraversable f, HTraversable g) => HTraversable (f + g) where
    htraverse f (Inl t) = Inl <$> htraverse f t
    htraverse f (Inr t) = Inr <$> htraverse f t

-- The free monad on a traversable functor is traversable
instance HTraversable f  => HTraversable (HFree f) where
    htraverse f (Return fa) = Return <$> f fa
    htraverse f (Op t) = Op <$> htraverse (htraverse f) t

--------------------------------------------------
-- | Existentials
data E (f :: u -> *) = forall x. Ex (f x)

instance ShowI f => Show (E f) where
  show (Ex f) = "Ex" ++ showI f

runE :: (f :=> b) -> E f -> b
runE f (Ex x) = f x

--------------------------------------------------
-- |Traversal that returns a list of subterms
subs :: (HFunctor h, HTraversable h) => HFree h f :=> [E (HFree h f)] -- HList (HFree h f) '[u]
subs = unKF . rec
   (\x -> KF [ Ex $ Return x])
   (\f g t -> KF $ (Ex $ Op $ hfmap g t) : (foldMapDef unKF (hfmap f t)))

class ToAny (t :: u) where
  type Ctx t (dom :: (u -> *) -> u -> * ) :: Constraint
  cast :: Ctx t dom => HFree dom f :=> Maybe (HFree dom f t)
  toAny :: Ctx t dom => res -> (HFree dom f t -> res) -> (HFree dom f x -> res)
  toAny b f = maybe b f . cast

-- mkT :: (ToAny b, Ctx b dom) => (HFree dom f b -> HFree dom f b) -> HFree dom f :-> HFree dom f
-- mkT f = _

getSubs :: (HFunctor dom, HTraversable dom, ToAny t, Ctx t dom) => HFree dom f p -> [HFree dom f t]
getSubs = foldr (\(Ex x) xs -> case cast x of
                             Just y -> y : xs
                             Nothing -> xs) [] . subs
