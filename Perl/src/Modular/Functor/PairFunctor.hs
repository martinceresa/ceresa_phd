{-# Language KindSignatures #-}
{-# Language TypeOperators #-}
{-# Language PolyKinds #-}

module Modular.Functor.PairFunctor where

import Modular.HFunctor

--------------------------------------------------
-- Fun prod.
--------------------------------------------------
data (h || f) x = (h x) :|: (f x)

fstF :: h || f :-> h
fstF (x :|: _) = x

sndF :: h || f :-> f
sndF (_ :|: y) = y

data (h * g) (f :: u -> *) (x :: u) = h f x :*: g f x

instance (HFunctor h, HFunctor g) => HFunctor (h * g) where
  hfmap f (l :*: r) = (hfmap f l) :*: (hfmap f r)

fstHF :: (h * g) f x -> h f x
fstHF (x :*: _) = x

sndHF :: (h * g) f x -> g f x
sndHF (_ :*: y) = y
