{-# Language TypeOperators #-}
{-# Language KindSignatures #-}
{-# Language PolyKinds #-}
{-# Language RankNTypes #-}

module Modular.Functor.CoProd where

import Modular.HFunctor

--------------------------------------------------
-- |Coproducts of HFunctors
--------------------------------------------------

data (h + k) (f :: u -> *) (x :: u) = Inl (h f x) | Inr (k f x)
infixr 4 +

caseF :: (h f :-> g) -> (k f :-> g) -> (h + k) f :-> g
caseF h _ (Inl x) = h x
caseF _ k (Inr x) = k x

instance (HFunctor h, HFunctor k) => HFunctor (h + k) where
  hfmap f (Inl x) = Inl (hfmap f x)
  hfmap f (Inr x) = Inr (hfmap f x)

instance (Eq (h e t), Eq (k e t)) => (Eq ((h + k) e t)) where
  (Inl x) == (Inl y) = x == y
  (Inr x) == (Inr y) = x == y
  _ == _ = False

--------------------------------------------------
-- |The coproduct of showables is showable

instance (ShowHFunctor f, ShowHFunctor g) => ShowHFunctor (f + g) where
  showHFunctor (Inl x) = showHFunctor x
  showHFunctor (Inr x) = showHFunctor x
