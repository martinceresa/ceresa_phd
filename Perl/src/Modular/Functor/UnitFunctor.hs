module Modular.Functor.UnitFunctor where

data UnitF a = UnitF
  deriving Show

instance Eq (UnitF e) where
  UnitF == UnitF = True

instance Functor UnitF where
  fmap _ UnitF = UnitF

instance Foldable UnitF where
  foldr _ b UnitF = b

instance Traversable UnitF where
  traverse _ UnitF = pure UnitF
