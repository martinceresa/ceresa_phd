{-# Language TypeOperators #-}
{-# Language KindSignatures #-}
{-# Language PolyKinds #-}
{-# Language RankNTypes #-}

module Modular.Functor.VoidFunctor where

import Modular.HFunctor

--Void Functor
data VoidF (a :: t)

instance Eq (VoidF a) where
  _ == _ = True -- ??

empty :: forall (a :: u) (f :: u -> *) (y :: u). VoidF a -> f y
empty = undefined

empty' :: forall (t :: u) a. VoidF t -> a
empty' = undefined

instance ShowI VoidF where
  showI = empty'

