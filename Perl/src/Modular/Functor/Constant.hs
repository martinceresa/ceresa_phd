{-# Language TypeOperators #-}
{-# Language KindSignatures #-}
{-# Language PolyKinds #-}
module Modular.Functor.Constant where

import Modular.HFunctor
import Modular.Functor.PairFunctor
-- import Modular.Functor.CoProd

--------------------------------------------------
-- Constant Prod. of HFunctor
--------------------------------------------------

-- data (h & c) (f :: * -> *) x = h f x :&: c

type (h & c) f x = (h * (K c)) f x

newtype K c (f :: u -> *) (a :: u) = K c -- Constant f

--deriving instance Show f => Show (KF f u)

remA :: (h & c) f :-> h f
remA (c :*: _) = c

newtype KFE a b = KFE {unKFE :: a -> a}

newtype KFEDual a b = KFED {unKFED :: a -> a}

-- constant functor.
instance Functor (KFE a) where
    fmap _ (KFE f ) = KFE f

-- constant functor.
instance Functor (KFEDual a) where
    fmap _ (KFED f ) = KFED f

-- Monoid m => Applicative (KF m)
instance Applicative (KFE a) where
    pure _ = KFE id
    (KFE x) <*> (KFE y) = KFE $ x . y

-- Monoid m => Applicative (KF (Dual m))
instance Applicative (KFEDual a) where
    pure _ = KFED id
    (KFED x) <*> (KFED y) = KFED $ y . x
