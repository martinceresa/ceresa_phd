{-# Language PolyKinds #-}
module Modular.Functor.IdFunctor where

import Modular.HFunctor

--------------------------------------------------
-- | High-order identity functor.
newtype HIdF (f :: u -> *) (a :: u) = HIdF (f a) deriving Eq

instance HFunctor HIdF where
  hfmap f (HIdF x) = HIdF $ f x

--------------------------------------------------
-- | Identity functor.
newtype Id a = Id {runId :: a} deriving Eq

instance Functor Id where
  fmap f = Id . f . runId

instance Applicative Id where
  pure = Id
  f <*> x = Id $ runId f (runId x)
