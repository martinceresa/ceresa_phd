{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}

module Modular.HFunctor where

--------------------------------------------------
-- Higher-Order Functors
-- (these are higher-order functors from |*| -> *)
-- as we are not asking that instances map functors to functors
--------------------------------------------------

-- |Index preserving morphisms
type a :-> b = forall x. a x -> b x

-- |Special case for morphisms into the constant functor
type a :=> b = forall x. a x -> b

class HFunctor (h :: (i -> *) -> o -> *) where
  hfmap :: (f :-> g) -> h f :-> h g

-- Flip
data (:=) (h :: (i -> *) -> i -> *) :: i -> (i -> *) -> * where
  Flip :: h e t -> (h := t) e

instance (Eq (h e t)) => Eq ((:=) h t e) where
  (Flip x) == (Flip y) = x == y

instance HFunctor h => SFunctor ((:=) h t) where
  sfmap tn (Flip h) = Flip $ hfmap tn h

-- | Alg def

type HAlg (f :: (u -> *) -> (u -> *)) (a :: u -> *) = f a :-> a

type HMAlg (m :: * -> *) (h :: (u -> *) -> (u -> *)) (v :: u -> *) = forall t. h v t -> m (v t)

-- | Const Alg Def
type KAlgG (h :: * -> u -> *) (f :: (u -> *) -> (u -> *)) (v :: *) = HAlg f (h v)
-- type KAlg' (f :: (u -> *) -> (u -> *)) (v :: *) = f (KF v) :-> KF v
type KAlg' f v = KAlgG KF f v
-- KAlg' ~ KAlg. Since forall t. KF v t = v
type KAlg (f :: (u -> *) -> (u -> *)) (v :: *) = f (KF v) :=> v

-- | Monadic Algebras(-ish) T-Algebras?
type KHMAlg m h v = HMAlg m h (KF v)

type MAlg (m :: * -> *) (f :: (u -> *) -> (u -> *)) (v :: *) = f (KF v) :=> m v

---
class SFunctor (ff :: (u -> *) -> *) where
    sfmap :: (f :-> g) -> ff f -> ff g

--------------------------------------------------
-- |Constant Functor
-- This is so useful that I put it in this module.
newtype KF (a :: *) (b :: u) = KF {unKF :: a}

instance Functor (KF f) where
  fmap _ (KF f) = KF f

instance Monoid f => Applicative (KF f) where
  pure = const (KF mempty)
  (KF f) <*> (KF x) = KF $ mappend f x

instance Show (KF String a) where
  show (KF xs) = xs

instance Show a => ShowI (KF a) where
  showI (KF a) = show a

instance Show a => ShowT (KF a) where
  showT (KF a) = show a

--------------------------------------------------
-- | Showing HFunctors
class ShowI (f :: u -> *) where
  showI :: f :=> String

-- instance ShowI f => ShowT f where
--   showT = showI

class ShowT e where
  showT :: e (KF String) -> String

class ShowHFunctor (h :: (i -> *) -> i -> *) where
  showHFunctor :: h (KF String) :=> String

class ShowFlipHFunctor (h :: i -> (i -> *) -> *) where
  showFlipHFunctor :: h t (KF String) -> String

instance ShowHFunctor h => ShowFlipHFunctor ((:=) h) where
  showFlipHFunctor (Flip x) = showHFunctor x

instance ShowHFunctor h => ShowT ((:=) h t) where
  showT (Flip xs) = showHFunctor xs
