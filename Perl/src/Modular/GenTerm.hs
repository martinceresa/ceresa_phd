{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Modular.High.GenTerm where

import           Modular.High.Functor.VoidFunctor
import           Modular.High.Functor.IdFunctor
import           Modular.High.Functor.Constant
import           Modular.High.Evaluation
import           Modular.High.FreeMonad
import           Modular.High.HFunctor
import Modular.High.Traversals

import Control.Monad
-- import Data.Traversable

-- a term has no Returns
type Term sig = HFree sig VoidF

data App (m :: * -> *) (f :: u -> *) (t :: u) = App (m (f t))

cata :: forall (h :: (u -> *) -> u -> *)
               (f :: u -> *)
        .
  HFunctor h => HAlg h f -> HFree h VoidF :-> f
cata = fold (empty @u)

-- cataM :: forall (h :: (u -> *) -> (u -> *))
--          (m :: * -> *)
--          (v :: *). (HFunctor h, Monad m, HTraversable h) => MAlg m h v -> HFree h VoidF :=> m v
-- cataM f = f <=< _
cataM ::  forall
          (h :: (u -> *) -> (u -> *))
          (m :: * -> *)
          (f :: u -> *) t.
          (HFunctor h, Monad m, HTraversable h) => HMAlg m h f -> HFree h VoidF t -> m (f t)
cataM _ (Return _) = undefined
cataM alg (Op t) =  join $ fmap alg (hmapM (cataM alg) t)

eval :: forall
        (h :: ( u -> *) -> u -> *)
        (v :: u -> *)
        .(HFunctor h, Eval h v) =>
        HFree h VoidF :-> v
eval = cata evalAlg

evalM :: (HTraversable h, HFunctor h, EvalM m h v) => HFree h VoidF t -> m (v t)
evalM = cataM evalMAlg'

runKM :: (HTraversable h, HFunctor h, EvalM m h (KF v)) => HFree h VoidF :=> m v
runKM t = unKF <$> evalM t

runKM' :: (HTraversable h, HFunctor h, EvalKM m h v) => HFree h VoidF :=> m v
runKM' t = unKF <$> cataM ((return . KF) <=< evalMAlg) t

run :: (HFunctor h, Eval h Id) => HFree h VoidF a -> a
run = runId . eval

runK :: (HFunctor h, Eval h (KF b)) => HFree h VoidF a -> b
runK = unKF . eval

runK' :: (HFunctor h, EvalK h v) => HFree h VoidF t -> v
runK' = unKF . cata (KF . evalKAlg)

-- runKM :: forall (h :: (u -> *) -> (u -> *))
--          (m :: * -> *)
--          (t :: u)
--          (v :: *). (HFunctor h, EvalKM m h v) => HFree h VoidF t -> m v
-- runKM = unKF . fold (empty @u) _
-- recursion

-- Se llama trans porque B&H la llama así.
trans :: HFunctor f => (Term f :-> Term f) -> Term f :-> Term f
trans f =  cata (f . Op)

foldTerm :: HFunctor dom => (forall x. g x) -> HAlg dom g -> Term dom :-> g
foldTerm b = fold (const b)

----- Query
--
-- Playground
query' :: HFunctor dom => a -> (dom (KF a) :-> KF a) -> Term dom x -> a
query' b f t = unKF $ foldTerm (KF b) f t

toFP :: KF r a -> (forall p. KF r p)
toFP (KF x) = KF x

queryF :: (KFoldable dom, HFunctor dom) => (Term dom :-> KF r) -> (r -> r -> r) -> Term dom x -> r
queryF b c t = unKF $ fold undefined (kfold c (toFP $ b t)) t
--

-- queryM :: HTraversable dom => (dom ? -> r) -> (r -> r -> r) -> Term dom x -> r
-- queryM == ?? escribir query utilizando traverse de HFree.

queryRKFE :: HTraversable dom =>
        (forall b. HFree dom VoidF b -> r)
    ->  (r -> r -> r)
    -> HFree dom VoidF a -> r
queryRKFE q c t'@(Op t) = unKFE (htraverse (\t -> KFE $ \r -> c (queryRKFE q c t) r) t) (q t')

queryLKFE :: HTraversable dom =>
        (forall b. HFree dom VoidF b -> r)
    ->  (r -> r -> r)
    -> HFree dom VoidF a -> r
queryLKFE q c t'@(Op t) = unKFED (htraverse (\x -> KFED $ \y -> c y (queryLKFE q c x)) t) (q t')

-- TermHom h h' =  forall f. h f :-> HFree h' f
-- en todo caso es h UnitF t -> HFree h' IdF t. Un constructor de h por varios de h'
-- termHom :: (HFunctor h, HFunctor h') => (forall f. h f :-> HFree h' f) -> Term h :-> Term h'
-- termHom hom = cata $ hjoin . hom
