{-# LANGUAGE DataKinds        #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs            #-}
{-# LANGUAGE KindSignatures   #-}
{-# LANGUAGE PatternSynonyms  #-}
{-# LANGUAGE TypeOperators    #-}
{-# LANGUAGE ViewPatterns     #-}
{-# LANGUAGE RankNTypes #-}

module Modular.Test where

import           Prelude                          hiding (head, succ, tail)

import           Modular.DTC
import           Modular.FreeMonad
import           Modular.Functor.VoidFunctor
import           Modular.HFunctor
import           Modular.Traversals


data Types = TBool | TInt

data STypes (x :: Types) where
  SBool :: STypes 'TBool
  SInt :: STypes 'TInt

data NUM (e :: Types -> *) (a :: Types) where
    Num_ :: Int -> NUM e 'TInt
    Mul_ :: e 'TInt -> e 'TInt -> NUM e 'TInt
    Add_ :: e 'TInt -> e 'TInt -> NUM e 'TInt

pattern Num :: (NUM < sup) => Int -> sup f 'TInt
pattern Num n <- (proj -> Just (Num_ n))
  where Num = inj . Num_

pattern ONum :: (NUM < sup) => Int -> HFree sup f 'TInt
pattern ONum n <- Op (Num n)
  where ONum = Op . Num

pattern Mul :: (NUM < sup) => f 'TInt -> f 'TInt -> sup f 'TInt
pattern Mul x y <- (proj -> Just (Mul_ x y))
  where Mul x y = inj $ Mul_ x y

pattern OMul :: (NUM < sup) => HFree sup f 'TInt -> HFree sup f 'TInt -> HFree sup f 'TInt
pattern OMul x y <- Op (Mul x y)
  where OMul x y = Op $ Mul x y

pattern Add :: (NUM < sup) => f 'TInt -> f 'TInt -> sup f 'TInt
pattern Add x y <- (proj -> Just (Add_ x y))
  where Add x y = inj $ Add_ x y

pattern OAdd :: (NUM < sup) => HFree sup f 'TInt -> HFree sup f 'TInt -> HFree sup f 'TInt
pattern OAdd x y <- Op (Add x y)
  where OAdd x y = Op $ Add x y

instance HFunctor NUM where
  hfmap _ (Num_ n) = Num_ n
  hfmap nat (Mul_ x y) = Mul_ (nat x) (nat y)
  hfmap nat (Add_ x y) = Add_ (nat x) (nat y)

instance ShowHFunctor NUM where
  showHFunctor (Num_ n) = "Num_ " ++ show n
  showHFunctor (Add_ x y) = "(" ++ show x ++ "+" ++ show y ++ ")"
  showHFunctor (Mul_ x y) = "(" ++ show x ++ "*" ++ show y ++ ")"

instance HTraversable NUM where
  htraverse _ (Num_ n) = pure (Num_ n)
  htraverse f (Mul_ x y) = Mul_ <$> f x <*> f y
  htraverse f (Add_ x y) = Add_ <$> f x <*> f y

data Logic (e :: Types -> *) (a :: Types) where
    Not_ :: e 'TBool -> Logic e 'TBool
    Eq_ :: e a -> e a -> Logic e 'TBool

instance HFunctor Logic where
  hfmap nat (Not_ x) = Not_ $ nat x
  hfmap nat (Eq_ x y) = Eq_ (nat x) (nat y)

instance ShowHFunctor Logic where
  showHFunctor (Not_ n) = "Not_ " ++ show n
  showHFunctor (Eq_ x y) = "(" ++ show x ++ "==" ++ show y ++ ")"

term1 :: (NUM < sup, ShowHFunctor sup) => HFree sup f 'TInt
term1 = OMul (ONum 1) (ONum 2)

termshow :: HFree NUM VoidF 'TInt
termshow = term1

succ :: (NUM < sup) => HFree sup f 'TInt -> HFree sup f 'TInt
succ (ONum n) = ONum (n + 1)
succ (OMul x y) = OMul (succ x) (succ y)
succ (OAdd x y) = OAdd (succ x) (succ y)
