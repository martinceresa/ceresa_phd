{-# Language TypeOperators #-}
{-# LANGUAGE RankNTypes            #-}

module Modular.Iso where

import Modular.HFunctor

-- import Modular.Functor.Constant

iso_ConstF1 :: (a :-> (KF b)) -> (a :=> b)
iso_ConstF1 f x = unKF $ f x

iso_ConstF2 ::(a :=> b) -> (a :-> (KF b))
iso_ConstF2 f x = KF $ f x
