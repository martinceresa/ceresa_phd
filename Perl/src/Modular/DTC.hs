{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms       #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TypeOperators         #-}

module Modular.DTC where

import           Modular.Functor.CoProd
import           Modular.HFunctor

-- --------------------------------------------------
-- DTC for higher-order functors
--------------------------------------------------

-- data L (x:: (Types -> *) -> Types -> *) (e :: Types -> *) (t :: Types)
-- data SUP (e :: Types -> *) (t :: Types)

-- type family Lub (x :: (Types -> *) -> Types -> *) (s :: (Types -> *) -> Types -> *) :: Constraint
-- type instance Lub (L x) s = x < s
-- type instance Lub (p * q) s = (Lub p s, q < s)

class (sub :: (u -> *) -> u -> *) < sup where
   inj   :: sub f :-> sup f
   proj  :: sup f x -> Maybe (sub f x)

instance (f < f) where
  inj   = id
  proj  = Just

instance {-# OVERLAPPING #-} f < (f + g) where
  inj           = Inl
  proj (Inl x)  = Just x
  proj _        = Nothing


instance {-# OVERLAPPABLE #-} (f < h) => f < (g + h) where
  inj           = Inr . inj
  proj (Inr x)  = proj x
  proj _        = Nothing
