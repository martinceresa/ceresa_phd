{-# LANGUAGE KindSignatures        #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE AllowAmbiguousTypes#-}

module Modular.High.Testing where

import Modular.High.FreeMonad
import Modular.High.Functor.CoProd
import Modular.High.DTC

import Data.Constraint

import Test.QuickCheck.Gen
import Test.QuickCheck.Arbitrary

class QCDom (dom :: (u -> *) -> u -> *) (t :: u) where
  type Ctx (sup :: (u -> *) -> u -> *) (t :: u) :: Constraint
  gen :: (Ctx sup t, Arbitrary (f t)) => Gen (sup f t)

-- instance QCDom Expression 'TExpression where
--   type Ctx sup 'TExpression = (Expression < sup)
--   gen = BinOp <$> arbitrary <*> arbitrary <*> arbitrary
-- instance QCDom dom t => QCDom (HFree dom) t where
--   type Ctx sup t = (dom < sup)

-- instance (QCDom dom1 t, QCDom dom2 t)  => QCDom (dom1 + dom2) t where
--   gen = oneof [gen, gen]

-- instance QCDom dom t => QCDom (HFree dom) t where
--   gen = Op <$> gen

-- class QCTop (t :: u) where
--   type Ctx t (h :: (u -> *) -> u -> *) :: Constraint
--   arb :: Ctx t h => Gen (HFree h f t)

