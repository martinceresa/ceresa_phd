{-# Language FlexibleContexts #-}
{-# Language InstanceSigs #-}
{-# Language PartialTypeSignatures #-}
{-# Language FlexibleInstances #-}
module Domains where

import           Modular.Traversals
import           Modular.DTC

import Modular.FreeMonad

import Language
import Patterns

import Data.Constraint

-- | Domains definitions
type family IntDom (dom :: (k -> *) -> k -> *) :: Constraint
type instance IntDom dom = (Val < dom -- Const i
                           ,Op < dom) -- Mult x y

-- | Casting definitions

instance ToAny Int where
  type Ctx Int dom = IntDom dom
  cast :: forall t dom f x. Ctx Int dom => HFree dom f x -> Maybe (HFree dom f Int)
  cast (CConst i) = Just $ CConst i
  cast (CMult x y) = Just $ CMult x y
  -- cast (CFst p) = case (cast p :: Maybe (HFree dom f (Int , _))) of
  --                   Just p' -> Just $ CFst p'
  cast (CFst (CPair l r)) = case cast l of
                              (Just l') -> Just $ CFst (CPair l' r)
                              _ -> Nothing
  cast (CSnd (CPair l r)) = case cast r of
                              (Just r') -> Just $ CSnd (CPair l r')
                              _ -> Nothing
  cast _ = Nothing

castL :: (Val < dom, Op < dom, ToAny c, Ctx c dom) => HFree dom e (l , r) -> Maybe (HFree dom e (c , r))
castL (CPair p q) = case (cast p) of
                      Just p' -> Just (CPair p' q)
                      Nothing -> Nothing
castL (CFst p) =:xa

-- | Cleaner this way?
getFst :: (ToAny l , Ctx l dom, Val < dom, Op < dom) => HFree dom e t -> Maybe (HFree dom e l)
getFst (CPair l r) = cast l
-- getFst (CFst p) = _
getFst _ = Nothing

instance (ToAny l , ToAny r) => ToAny (l,r) where
  type Ctx (l,r) dom = (Ctx l dom , Ctx r dom, Val < dom)
  cast (CPair l r) = case (cast l , cast r) of
                       (Just l' ,  Just r') -> Just $ CPair l' r'
                       _ -> Nothing
  cast _ = Nothing

getIntSubExpressions :: Term t -> [Term Int]
getIntSubExpressions = getSubs
