module Test where

import Language
import Patterns

uno :: Term Int
uno = CConst 1

v3 :: Term Int
v3 = CMult (CConst 1) (CConst 2)

t4 :: Term (Int , Int)
t4 = CPair v3 v3

t5 :: Term Int
t5 = CFst t4

t6 :: Term Int
t6 = CFst $ CFlip $ CPair (CConst 1) (CConst 2)

t7 :: Term Int
t7 = CFst $ CFst $ CPair (CPair (CConst 1) (CConst 4)) (CConst 2)
