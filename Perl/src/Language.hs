module Language where

import           Modular.Functor.CoProd
import Modular.FreeMonad
import Modular.Functor.VoidFunctor

import Modular.HFunctor
import Modular.Traversals


data Val e l where
  Const_ :: Int -> Val e Int
  Pair_ :: e s -> e t -> Val e (s,t)

data Op e l where
  Mult_ :: e Int -> e Int -> Op e Int
  Fst_ :: e (s , t) -> Op e s
  Snd_ :: e (s , t) -> Op e t

data H e l where
  Flip_ :: e (s , t) -> H e (t, s)

type Sig = Val + Op + H

type Term = HFree Sig VoidF

instance HFunctor Val where
  hfmap _ (Const_ i) = Const_ i
  hfmap f (Pair_ l r) = Pair_ (f l) (f r)

instance HFunctor Op where
  hfmap f (Mult_ x y) = Mult_ (f x) (f y)
  hfmap f (Fst_ p) = Fst_ (f p)
  hfmap f (Snd_ p) = Snd_ (f p)

instance HFunctor H where
  hfmap f (Flip_ x) = Flip_ (f x)

instance HTraversable Val where
  htraverse _ (Const_ i) = pure $ Const_ i
  htraverse f (Pair_ l r) = Pair_ <$> f l <*> f r

instance HTraversable Op where
  htraverse f (Mult_ x y) = Mult_ <$> f x <*> f y
  htraverse f (Fst_ p) = Fst_ <$> f p
  htraverse f (Snd_ p) = Snd_ <$> f p

instance HTraversable H where
  htraverse f (Flip_ x) = Flip_ <$> f x

instance ShowHFunctor Val where
  showHFunctor (Const_ i) = "C " ++ show i
  showHFunctor (Pair_ l r) = "("++ unKF l ++ "," ++ unKF r ++ ")"

instance ShowHFunctor Op where
  showHFunctor (Mult_ x y) = "(" ++ unKF x ++ " * " ++ unKF y ++ ")"
  showHFunctor (Fst_ p) = "fst " ++ unKF p
  showHFunctor (Snd_ p) = "snd " ++ unKF p

instance ShowHFunctor H where
  showHFunctor (Flip_ p) = "Flip " ++ unKF p

