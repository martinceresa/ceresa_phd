module Patterns where

import           Modular.DTC
import Modular.FreeMonad

import Language

-- | Intelligent Patterns

pattern Const :: (Val < sup) => x ~ Int => Int -> sup f x
pattern Const i <- (proj -> Just (Const_ i))
  where Const = inj . Const_

pattern Pair :: (Val < sup) => x ~ (s,t) =>
             f s -> f t -> sup f x
pattern Pair l r <- (proj -> Just (Pair_ l r))
  where Pair l r = inj $ Pair_ l r

pattern Mult :: (Op < sup) => x ~ Int =>
             e Int -> e Int -> sup e x
pattern Mult x y <- (proj -> Just (Mult_ x y))
  where Mult x y = inj $ Mult_ x y

pattern Fst :: (Op < sup) => x ~ s =>
            e (s , t) -> sup e x
pattern Fst p <- (proj -> Just (Fst_ p))
  where Fst = inj . Fst_

pattern Snd :: (Op < sup) => x ~ t =>
            e (s , t) -> sup e x
pattern Snd p <- (proj -> Just (Snd_ p))
  where Snd = inj . Snd_

pattern Flip :: (H < sup) => x ~ (t , s) =>
             e (s, t) -> sup e x
pattern Flip p <- (proj -> Just (Flip_ p))
  where Flip = inj . Flip_

-- | Free Terms Intelligent Patterns
pattern CConst :: (Val < sup) => x ~ Int =>
               Int -> HFree sup f x
pattern CConst i <- Op (Const i)
  where CConst = Op . Const

pattern CPair :: (Val < sup) => x ~ (s,t) =>
              HFree sup f s -> HFree sup f t -> HFree sup f x
pattern CPair l r <- Op (Pair l r)
  where CPair l r = Op $ Pair l r

pattern CMult :: (Op < sup) => x ~ Int =>
              HFree sup f Int -> HFree sup f Int -> HFree sup f x
pattern CMult x y <- Op (Mult x y)
  where CMult x y = Op $ Mult x y

pattern CFst :: (Op < sup) => x ~ s =>
             HFree sup f (s,t) -> HFree sup f x
pattern CFst p <- Op (Fst p)
  where CFst = Op . Fst

pattern CSnd :: (Op < sup) => x ~ t =>
             HFree sup f (s,t) -> HFree sup f x
pattern CSnd p <- Op (Snd p)
  where CSnd = Op . Snd

pattern CFlip :: (H < sup) => x ~ (t , s) =>
              HFree sup f (s , t) -> HFree sup f x
pattern CFlip p <- Op (Flip p)
  where CFlip = Op . Flip
